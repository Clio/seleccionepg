using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace SalarDb.CodeGen.Common
{
	static class dbSeleccionDbConnection
	{
		internal static IDbConnection GetNewConnection()
		{
			return new SqlConnection(GetConnectionString());
		}

		internal static String GetConnectionString()
		{
			return ConfigurationManager.ConnectionStrings["dbSeleccionConnectionString"].ConnectionString;
		}
	}
}
