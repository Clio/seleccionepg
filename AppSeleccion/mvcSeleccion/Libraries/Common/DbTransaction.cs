using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace SalarDb.CodeGen.Common
{
	public partial class DbTransaction
	{
		public static dbSeleccionDbTransaction BeginTransactiondbSeleccion()
		{
			return dbSeleccionDbTransaction.BeginTransaction();
		}
	}

	public class dbSeleccionDbTransaction : IDisposable
	{
		#region variables
		private IDbTransaction _SqlTransaction;
		private IDbConnection _Connection;
		#endregion

		#region properties
		internal IDbConnection Connection
		{
			get { return _Connection; }
			set { _Connection = value; }
		}
		#endregion

		#region static methods
		public static dbSeleccionDbTransaction BeginTransaction()
		{
			dbSeleccionDbTransaction result = new dbSeleccionDbTransaction();
			result._Connection = dbSeleccionDbProvider.GetNewConnection();

			// Openning transaction connection
			result._Connection.Open();

			// Begin the trasnaction
			result._SqlTransaction = result._Connection.BeginTransaction();

			return result;
		}
		#endregion

		#region public methods
		private dbSeleccionDbTransaction()
		{
			// User shouldn't create
		}

		public void Dispose()
		{
			if (_SqlTransaction != null)
				_SqlTransaction.Dispose();
			if (_Connection != null)
				_Connection.Dispose();
		}

		public void Commit()
		{
			_SqlTransaction.Commit();
		}

		public void Rollback()
		{
			_SqlTransaction.Rollback();
		}
		#endregion

		#region protected methods
		internal SqlCommand GetNewCommand()
		{
			IDbCommand cmd = _SqlTransaction.Connection.CreateCommand();
			cmd.Transaction = _SqlTransaction;
			return (SqlCommand)cmd;
		}
		#endregion
	}
}
