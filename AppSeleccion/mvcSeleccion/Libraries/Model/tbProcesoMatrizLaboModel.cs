using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using SalarDb.CodeGen.Base;
using SalarDb.CodeGen.BLL;

namespace SalarDb.CodeGen.Model
{
	/// <summary>
	/// User custom methods for tbProcesoMatrizLabo
	/// </summary>
	partial class tbProcesoMatrizLaboModel
	{
        public List<tbProcesoMatrizLaboModel> tbProcesoMatrizLaboModelList { get; set; }

        public tbProcesoCnfgLaboModel tbProcesoCnfgLaboModel { get; set; }
	}
}
