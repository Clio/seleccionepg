using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using SalarDb.CodeGen.Base;
using SalarDb.CodeGen.BLL;

namespace SalarDb.CodeGen.Model
{
	/// <summary>
	/// User custom methods for tbAcademico
	/// </summary>
	partial class tbAcademicoModel
	{
        public List<tbAcademicoModel> tbAcademicoModelList { get; set; }

        public bool Cumple { get; set; }

        public string ctCarrera { get; set; }
	}
}
