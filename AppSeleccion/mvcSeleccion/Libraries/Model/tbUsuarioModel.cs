using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using SalarDb.CodeGen.Base;
using SalarDb.CodeGen.BLL;

namespace SalarDb.CodeGen.Model
{
    /// <summary>
    /// User custom methods for tbUsuario
    /// </summary>
    partial class tbUsuarioModel
    {
        public string FullName
        {
            get
            {
                return string.Format("{0} {1}", this.ctNombres, this.ctApellidos);
            }
        }
    }
}
