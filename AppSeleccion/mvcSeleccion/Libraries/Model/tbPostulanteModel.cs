using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using SalarDb.CodeGen.Base;
using SalarDb.CodeGen.BLL;

namespace SalarDb.CodeGen.Model
{
    /// <summary>
    /// User custom methods for tbPostulante
    /// </summary>
    partial class tbPostulanteModel
    {
        private string _NumeroDNI;

        public string NombreCompleto
        {
            get
            {
                return string.Format("{0} {1} {2}", this._ctNombres, this._ctApellidoPaterno, this._ctApellidoMaterno);
            }
        }

        public string Cobertura
        {
            get
            {
                return string.Format("{0}\\{1}", this._ctCobertura, this._ctZona);
            }
        }

        public String NumeroDNI
        {
            get { return _NumeroDNI; }
            set { _NumeroDNI = value; }
        }

        public tbProcesoModel tbProcesoModel { get; set; }

        public tbCandidatoModel tbCandidatoModel { get; set; }

        public List<tbPostulanteDocuModel> tbPostulanteDocuList { get; set; }

        public List<tbAcademicoModel> tbAcademicoModelList { get; set; }

        public List<tbExperienciaModel> tbExperienciaModelList { get; set; }

    }
}
