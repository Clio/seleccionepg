using SalarDb.CodeGen.Base;
using SalarDb.CodeGen.BLL;
using System;
using System.Collections.Generic;
using System.Data;

namespace SalarDb.CodeGen.Model
{
    /// <summary>
    /// Business Model for tbCandidato
    /// </summary>
    public partial class tbCandidatoModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbCandidato
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_Candidato;
            public Int32 ctApellidoPaterno;
            public Int32 ctApellidoMaterno;
            public Int32 ctNombres;
            public Int32 cnID_Sexo;
            public Int32 ctEstadoCivil;
            public Int32 ctLugarNacimiento;
            public Int32 cfFechaNacimiento;
            public Int32 cnID_PaisNacimiento;
            public Int32 ctDireccion;
            public Int32 cnID_Pais;
            public Int32 cnID_Departamento;
            public Int32 cnID_Provincia;
            public Int32 cnID_Distrito;
            public Int32 ctTelefonoCasa;
            public Int32 ctTelefonoMovil;
            public Int32 ctTelefonoOtro;
            public Int32 ctEmail;
            public Int32 ctClaveAcceso;
            public Int32 cfFechaCreacion;
            public Int32 cfFechaActualizacion;
            public Int32 ctIPAcceso;
            public Int32 cnEstadoReg;
            public Int32 cnCodigoRecuperacion;
            public Int32 ctNumeroDNI;
            public Int32 ctImagenPerfil;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_Candidato = dataReader.GetOrdinal("cnID_Candidato");
                ctApellidoPaterno = dataReader.GetOrdinal("ctApellidoPaterno");
                ctApellidoMaterno = dataReader.GetOrdinal("ctApellidoMaterno");
                ctNombres = dataReader.GetOrdinal("ctNombres");
                cnID_Sexo = dataReader.GetOrdinal("cnID_Sexo");
                ctEstadoCivil = dataReader.GetOrdinal("ctEstadoCivil");
                ctLugarNacimiento = dataReader.GetOrdinal("ctLugarNacimiento");
                cfFechaNacimiento = dataReader.GetOrdinal("cfFechaNacimiento");
                cnID_PaisNacimiento = dataReader.GetOrdinal("cnID_PaisNacimiento");
                ctDireccion = dataReader.GetOrdinal("ctDireccion");
                cnID_Pais = dataReader.GetOrdinal("cnID_Pais");
                cnID_Departamento = dataReader.GetOrdinal("cnID_Departamento");
                cnID_Provincia = dataReader.GetOrdinal("cnID_Provincia");
                cnID_Distrito = dataReader.GetOrdinal("cnID_Distrito");
                ctTelefonoCasa = dataReader.GetOrdinal("ctTelefonoCasa");
                ctTelefonoMovil = dataReader.GetOrdinal("ctTelefonoMovil");
                ctTelefonoOtro = dataReader.GetOrdinal("ctTelefonoOtro");
                ctEmail = dataReader.GetOrdinal("ctEmail");
                ctClaveAcceso = dataReader.GetOrdinal("ctClaveAcceso");
                cfFechaCreacion = dataReader.GetOrdinal("cfFechaCreacion");
                cfFechaActualizacion = dataReader.GetOrdinal("cfFechaActualizacion");
                ctIPAcceso = dataReader.GetOrdinal("ctIPAcceso");
                cnEstadoReg = dataReader.GetOrdinal("cnEstadoReg");
                cnCodigoRecuperacion = dataReader.GetOrdinal("cnCodigoRecuperacion");
                //Campos personalizados
                if (DataReaderExtensions.ContainsColumn(dataReader, "ctNumero"))
                    ctNumeroDNI = dataReader.GetOrdinal("ctNumero");
                //Campos personalizados


                if (DataReaderExtensions.ContainsColumn(dataReader, "ctImagenPerfil"))
                    ctImagenPerfil = dataReader.GetOrdinal("ctImagenPerfil");
            }

            #endregion public methods
        }

        #region fields variables

        private Int64 _cnID_Candidato;
        private String _ctApellidoPaterno;
        private String _ctApellidoMaterno;
        private String _ctNombres;
        private Int64 _cnID_Sexo;
        private Int64 _ctEstadoCivil;
        private String _ctLugarNacimiento;
        private DateTime _cfFechaNacimiento;
        private Int32 _cnID_PaisNacimiento;
        private String _ctDireccion;
        private Int32 _cnID_Pais;
        private Int32 _cnID_Departamento;
        private Int32 _cnID_Provincia;
        private Int32 _cnID_Distrito;
        private String _ctTelefonoCasa;
        private String _ctTelefonoMovil;
        private String _ctTelefonoOtro;
        private String _ctEmail;
        private String _ctClaveAcceso;
        private DateTime? _cfFechaCreacion;
        private DateTime? _cfFechaActualizacion;
        private String _ctIPAcceso;
        private Byte _cnEstadoReg;
        private Guid? _cnCodigoRecuperacion;
        private string _ctImagenPerfil;

        private string _ctDescripcion;

        private bool _ctTerminoAceptacion;



        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Candidato
        {
            get { return _cnID_Candidato; }
            set { _cnID_Candidato = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctApellidoPaterno
        {
            get { return _ctApellidoPaterno; }
            set { _ctApellidoPaterno = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctApellidoMaterno
        {
            get { return _ctApellidoMaterno; }
            set { _ctApellidoMaterno = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctNombres
        {
            get { return _ctNombres; }
            set { _ctNombres = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Sexo
        {
            get { return _cnID_Sexo; }
            set { _cnID_Sexo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 ctEstadoCivil
        {
            get { return _ctEstadoCivil; }
            set { _ctEstadoCivil = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctLugarNacimiento
        {
            get { return _ctLugarNacimiento; }
            set { _ctLugarNacimiento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime cfFechaNacimiento
        {
            get { return _cfFechaNacimiento; }
            set { _cfFechaNacimiento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_PaisNacimiento
        {
            get { return _cnID_PaisNacimiento; }
            set { _cnID_PaisNacimiento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctDireccion
        {
            get { return _ctDireccion; }
            set { _ctDireccion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_Pais
        {
            get { return _cnID_Pais; }
            set { _cnID_Pais = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_Departamento
        {
            get { return _cnID_Departamento; }
            set { _cnID_Departamento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_Provincia
        {
            get { return _cnID_Provincia; }
            set { _cnID_Provincia = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_Distrito
        {
            get { return _cnID_Distrito; }
            set { _cnID_Distrito = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctTelefonoCasa
        {
            get { return _ctTelefonoCasa; }
            set { _ctTelefonoCasa = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctTelefonoMovil
        {
            get { return _ctTelefonoMovil; }
            set { _ctTelefonoMovil = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctTelefonoOtro
        {
            get { return _ctTelefonoOtro; }
            set { _ctTelefonoOtro = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctEmail
        {
            get { return _ctEmail; }
            set { _ctEmail = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctClaveAcceso
        {
            get { return _ctClaveAcceso; }
            set { _ctClaveAcceso = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? cfFechaCreacion
        {
            get { return _cfFechaCreacion; }
            set { _cfFechaCreacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? cfFechaActualizacion
        {
            get { return _cfFechaActualizacion; }
            set { _cfFechaActualizacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctIPAcceso
        {
            get { return _ctIPAcceso; }
            set { _ctIPAcceso = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Byte cnEstadoReg
        {
            get { return _cnEstadoReg; }
            set { _cnEstadoReg = value; }
        }

        public Guid? cnCodigoRecuperacion
        {
            get { return _cnCodigoRecuperacion; }
            set { _cnCodigoRecuperacion = value; }
        }

        public string ctImagenPerfil
        {
            get { return _ctImagenPerfil; }
            set { _ctImagenPerfil = value; }
        }

        public String ctDescripcion
        {
            get { return _ctDescripcion; }
            set { _ctDescripcion = value; }
        }

        public bool ctTerminoAceptacion
        {
            get { return _ctTerminoAceptacion; }
            set { _ctTerminoAceptacion = value; }
        }



        #endregion fields property

        #region public methods

        public tbCandidatoModel()
        {
        }

        public tbCandidatoModel(Int64 cnID_Candidato)
        {
            using (tbCandidatoBLL bll = tbCandidatoBLL.CreateInstance())
            {
                if (!bll.GetBycnID_Candidato(cnID_Candidato, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int64 Insert()
        {
            using (tbCandidatoBLL bll = tbCandidatoBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbCandidatoBLL bll = tbCandidatoBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbCandidatoBLL bll = tbCandidatoBLL.CreateInstance())
            {
                bll.Delete(this.cnID_Candidato);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_Candidato = Convert.ToInt64(dataReader[fields.cnID_Candidato]);
            _ctApellidoPaterno = Convert.ToString(dataReader[fields.ctApellidoPaterno]);
            _ctApellidoMaterno = Convert.ToString(dataReader[fields.ctApellidoMaterno]);
            _ctNombres = Convert.ToString(dataReader[fields.ctNombres]);
            _cnID_Sexo = Convert.ToInt64(dataReader[fields.cnID_Sexo]);
            _ctEstadoCivil = Convert.ToInt64(dataReader[fields.ctEstadoCivil]);
            _ctLugarNacimiento = Convert.ToString(dataReader[fields.ctLugarNacimiento]);
            _cfFechaNacimiento = Convert.ToDateTime(dataReader[fields.cfFechaNacimiento]);
            _cnID_PaisNacimiento = Convert.ToInt32(dataReader[fields.cnID_PaisNacimiento]);
            _ctDireccion = Convert.ToString(dataReader[fields.ctDireccion]);
            _cnID_Pais = Convert.ToInt32(dataReader[fields.cnID_Pais]);
            _cnID_Departamento = Convert.ToInt32(dataReader[fields.cnID_Departamento]);
            _cnID_Provincia = Convert.ToInt32(dataReader[fields.cnID_Provincia]);
            _cnID_Distrito = Convert.ToInt32(dataReader[fields.cnID_Distrito]);


            if (dataReader.IsDBNull(fields.ctTelefonoCasa) == false)
                _ctTelefonoCasa = Convert.ToString(dataReader[fields.ctTelefonoCasa]);
            else
                _ctTelefonoCasa = null;

            if (dataReader.IsDBNull(fields.ctTelefonoMovil) == false)
                _ctTelefonoMovil = Convert.ToString(dataReader[fields.ctTelefonoMovil]);
            else
                _ctTelefonoMovil = null;

            if (dataReader.IsDBNull(fields.ctTelefonoOtro) == false)
                _ctTelefonoOtro = Convert.ToString(dataReader[fields.ctTelefonoOtro]);
            else
                _ctTelefonoOtro = null;

            _ctEmail = Convert.ToString(dataReader[fields.ctEmail]);
            _ctClaveAcceso = Convert.ToString(dataReader[fields.ctClaveAcceso]);
            if (dataReader.IsDBNull(fields.cfFechaCreacion) == false)
                _cfFechaCreacion = Convert.ToDateTime(dataReader[fields.cfFechaCreacion]);
            else
                _cfFechaCreacion = null;

            if (dataReader.IsDBNull(fields.cfFechaActualizacion) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader[fields.cfFechaActualizacion]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader[fields.ctIPAcceso]);
            _cnEstadoReg = Convert.ToByte(dataReader[fields.cnEstadoReg]);

            _cnCodigoRecuperacion = (dataReader[fields.cnCodigoRecuperacion] == DBNull.Value) ? Guid.Empty : Guid.Parse(dataReader[fields.cnCodigoRecuperacion].ToString());

            //Campos personalizados
            if (dataReader.IsDBNull(fields.ctNumeroDNI) == false)
                NumeroDNI = Convert.ToString(dataReader[fields.ctNumeroDNI]);
            else
                NumeroDNI = null;
            //Campos personalizados


            if (dataReader.IsDBNull(fields.ctImagenPerfil) == false)
                ctImagenPerfil = Convert.ToString(dataReader[fields.ctImagenPerfil]);
            else
                ctImagenPerfil = string.Empty;
            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_Candidato = Convert.ToInt64(dataReader["cnID_Candidato"]);
            _ctApellidoPaterno = Convert.ToString(dataReader["ctApellidoPaterno"]);
            _ctApellidoMaterno = Convert.ToString(dataReader["ctApellidoMaterno"]);
            _ctNombres = Convert.ToString(dataReader["ctNombres"]);
            _cnID_Sexo = Convert.ToInt64(dataReader["cnID_Sexo"]);
            _ctEstadoCivil = Convert.ToInt64(dataReader["ctEstadoCivil"]);
            _ctLugarNacimiento = Convert.ToString(dataReader["ctLugarNacimiento"]);
            _cfFechaNacimiento = Convert.ToDateTime(dataReader["cfFechaNacimiento"]);
            _cnID_PaisNacimiento = Convert.ToInt32(dataReader["cnID_PaisNacimiento"]);
            _ctDireccion = Convert.ToString(dataReader["ctDireccion"]);
            _cnID_Pais = Convert.ToInt32(dataReader["cnID_Pais"]);
            _cnID_Departamento = Convert.ToInt32(dataReader["cnID_Departamento"]);
            _cnID_Provincia = Convert.ToInt32(dataReader["cnID_Provincia"]);
            _cnID_Distrito = Convert.ToInt32(dataReader["cnID_Distrito"]);

            _ctDescripcion = Convert.ToString(dataReader["ctDescripcion"]);


            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctTelefonoCasa")) == false)
                _ctTelefonoCasa = Convert.ToString(dataReader["ctTelefonoCasa"]);
            else
                _ctTelefonoCasa = null;

            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctTelefonoMovil")) == false)
                _ctTelefonoMovil = Convert.ToString(dataReader["ctTelefonoMovil"]);
            else
                _ctTelefonoMovil = null;

            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctTelefonoOtro")) == false)
                _ctTelefonoOtro = Convert.ToString(dataReader["ctTelefonoOtro"]);
            else
                _ctTelefonoOtro = null;

            _ctEmail = Convert.ToString(dataReader["ctEmail"]);
            _ctClaveAcceso = Convert.ToString(dataReader["ctClaveAcceso"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("cfFechaCreacion")) == false)
                _cfFechaCreacion = Convert.ToDateTime(dataReader["cfFechaCreacion"]);
            else
                _cfFechaCreacion = null;

            if (dataReader.IsDBNull(dataReader.GetOrdinal("cfFechaActualizacion")) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader["cfFechaActualizacion"]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader["ctIPAcceso"]);
            _cnEstadoReg = Convert.ToByte(dataReader["cnEstadoReg"]);

            _cnCodigoRecuperacion = (dataReader["cnCodigoRecuperacion"] == DBNull.Value) ? Guid.Empty : Guid.Parse(dataReader["cnCodigoRecuperacion"].ToString());

            // Model is ready

            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctImagenPerfil")) == false)
                _ctImagenPerfil = Convert.ToString(dataReader["ctImagenPerfil"]);
            else
                _ctImagenPerfil = string.Empty;


            SetDataLoaded(true);
        }

        #endregion public methods

        public List<tbMaestroListasModel> SexoListado { get; set; }

        public List<tbMaestroListasModel> EstadoCivilListado { get; set; }
    }

    /// <summary>
    /// Business Model for tbCnfgSistema
    /// </summary>
    public partial class tbCnfgSistemaModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbCnfgSistema
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_Config;
            public Int32 ctDescripcion;
            public Int32 ctValor;
            public Int32 ctCodigo;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_Config = dataReader.GetOrdinal("cnID_Config");
                ctDescripcion = dataReader.GetOrdinal("ctDescripcion");
                ctValor = dataReader.GetOrdinal("ctValor");
                ctCodigo = dataReader.GetOrdinal("ctCodigo");
            }

            #endregion public methods
        }

        #region fields variables

        private Int32 _cnID_Config;
        private String _ctDescripcion;
        private String _ctValor;
        private String _ctAlias;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_Config
        {
            get { return _cnID_Config; }
            set { _cnID_Config = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctDescripcion
        {
            get { return _ctDescripcion; }
            set { _ctDescripcion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctValor
        {
            get { return _ctValor; }
            set { _ctValor = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctAlias
        {
            get { return _ctAlias; }
            set { _ctAlias = value; }
        }

        #endregion fields property

        #region public methods

        public tbCnfgSistemaModel()
        {
        }

        public tbCnfgSistemaModel(Int32 cnID_Config)
        {
            using (tbCnfgSistemaBLL bll = tbCnfgSistemaBLL.CreateInstance())
            {
                if (!bll.GetBycnID_Config(cnID_Config, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int32 Insert()
        {
            using (tbCnfgSistemaBLL bll = tbCnfgSistemaBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbCnfgSistemaBLL bll = tbCnfgSistemaBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbCnfgSistemaBLL bll = tbCnfgSistemaBLL.CreateInstance())
            {
                bll.Delete(this.cnID_Config);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_Config = Convert.ToInt32(dataReader[fields.cnID_Config]);
            _ctDescripcion = Convert.ToString(dataReader[fields.ctDescripcion]);
            if (dataReader.IsDBNull(fields.ctValor) == false)
                _ctValor = Convert.ToString(dataReader[fields.ctValor]);
            else
                _ctValor = null;

            _ctAlias = Convert.ToString(dataReader[fields.ctCodigo]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_Config = Convert.ToInt32(dataReader["cnID_Config"]);
            _ctDescripcion = Convert.ToString(dataReader["ctDescripcion"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctValor")) == false)
                _ctValor = Convert.ToString(dataReader["ctValor"]);
            else
                _ctValor = null;

            _ctAlias = Convert.ToString(dataReader["ctAlias"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbDepartamento
    /// </summary>
    public partial class tbDepartamentoModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbDepartamento
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_Departamento;
            public Int32 ctDepartamento;
            public Int32 ctCodigoISO;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_Departamento = dataReader.GetOrdinal("cnID_Departamento");
                ctDepartamento = dataReader.GetOrdinal("ctDepartamento");
                ctCodigoISO = dataReader.GetOrdinal("ctCodigoISO");
            }

            #endregion public methods
        }

        #region fields variables

        private Int32 _cnID_Departamento;
        private String _ctDepartamento;
        private String _ctCodigoISO;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_Departamento
        {
            get { return _cnID_Departamento; }
            set { _cnID_Departamento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctDepartamento
        {
            get { return _ctDepartamento; }
            set { _ctDepartamento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctCodigoISO
        {
            get { return _ctCodigoISO; }
            set { _ctCodigoISO = value; }
        }

        #endregion fields property

        #region public methods

        public tbDepartamentoModel()
        {
        }

        public tbDepartamentoModel(Int32 cnID_Departamento)
        {
            using (tbDepartamentoBLL bll = tbDepartamentoBLL.CreateInstance())
            {
                if (!bll.GetBycnID_Departamento(cnID_Departamento, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int32 Insert()
        {
            using (tbDepartamentoBLL bll = tbDepartamentoBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbDepartamentoBLL bll = tbDepartamentoBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbDepartamentoBLL bll = tbDepartamentoBLL.CreateInstance())
            {
                bll.Delete(this.cnID_Departamento);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_Departamento = Convert.ToInt32(dataReader[fields.cnID_Departamento]);
            if (dataReader.IsDBNull(fields.ctDepartamento) == false)
                _ctDepartamento = Convert.ToString(dataReader[fields.ctDepartamento]);
            else
                _ctDepartamento = null;

            if (dataReader.IsDBNull(fields.ctCodigoISO) == false)
                _ctCodigoISO = Convert.ToString(dataReader[fields.ctCodigoISO]);
            else
                _ctCodigoISO = null;

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_Departamento = Convert.ToInt32(dataReader["cnID_Departamento"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctDepartamento")) == false)
                _ctDepartamento = Convert.ToString(dataReader["ctDepartamento"]);
            else
                _ctDepartamento = null;

            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctCodigoISO")) == false)
                _ctCodigoISO = Convert.ToString(dataReader["ctCodigoISO"]);
            else
                _ctCodigoISO = null;

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbDistrito
    /// </summary>
    public partial class tbDistritoModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbDistrito
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_Distrito;
            public Int32 cnID_Departamento;
            public Int32 cnID_Provincia;
            public Int32 ctDistrito;
            public Int32 ctCodigoISO;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_Distrito = dataReader.GetOrdinal("cnID_Distrito");
                cnID_Departamento = dataReader.GetOrdinal("cnID_Departamento");
                cnID_Provincia = dataReader.GetOrdinal("cnID_Provincia");
                ctDistrito = dataReader.GetOrdinal("ctDistrito");
                ctCodigoISO = dataReader.GetOrdinal("ctCodigoISO");
            }

            #endregion public methods
        }

        #region fields variables

        private Int32 _cnID_Distrito;
        private Int32? _cnID_Departamento;
        private Int32 _cnID_Provincia;
        private String _ctDistrito;
        private String _ctCodigoISO;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_Distrito
        {
            get { return _cnID_Distrito; }
            set { _cnID_Distrito = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32? cnID_Departamento
        {
            get { return _cnID_Departamento; }
            set { _cnID_Departamento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_Provincia
        {
            get { return _cnID_Provincia; }
            set { _cnID_Provincia = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctDistrito
        {
            get { return _ctDistrito; }
            set { _ctDistrito = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctCodigoISO
        {
            get { return _ctCodigoISO; }
            set { _ctCodigoISO = value; }
        }

        #endregion fields property

        #region public methods

        public tbDistritoModel()
        {
        }

        public tbDistritoModel(Int32 cnID_Distrito)
        {
            using (tbDistritoBLL bll = tbDistritoBLL.CreateInstance())
            {
                if (!bll.GetBycnID_Distrito(cnID_Distrito, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int32 Insert()
        {
            using (tbDistritoBLL bll = tbDistritoBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbDistritoBLL bll = tbDistritoBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbDistritoBLL bll = tbDistritoBLL.CreateInstance())
            {
                bll.Delete(this.cnID_Distrito);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_Distrito = Convert.ToInt32(dataReader[fields.cnID_Distrito]);
            if (dataReader.IsDBNull(fields.cnID_Departamento) == false)
                _cnID_Departamento = Convert.ToInt32(dataReader[fields.cnID_Departamento]);
            else
                _cnID_Departamento = null;

            _cnID_Provincia = Convert.ToInt32(dataReader[fields.cnID_Provincia]);
            _ctDistrito = Convert.ToString(dataReader[fields.ctDistrito]);
            if (dataReader.IsDBNull(fields.ctCodigoISO) == false)
                _ctCodigoISO = Convert.ToString(dataReader[fields.ctCodigoISO]);
            else
                _ctCodigoISO = null;

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_Distrito = Convert.ToInt32(dataReader["cnID_Distrito"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("cnID_Departamento")) == false)
                _cnID_Departamento = Convert.ToInt32(dataReader["cnID_Departamento"]);
            else
                _cnID_Departamento = null;

            _cnID_Provincia = Convert.ToInt32(dataReader["cnID_Provincia"]);
            _ctDistrito = Convert.ToString(dataReader["ctDistrito"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctCodigoISO")) == false)
                _ctCodigoISO = Convert.ToString(dataReader["ctCodigoISO"]);
            else
                _ctCodigoISO = null;

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbDocIdentidad
    /// </summary>
    public partial class tbDocIdentidadModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbDocIdentidad
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_Candidato;
            public Int32 cnID_TipoDocumento;
            public Int32 ctNumero;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_Candidato = dataReader.GetOrdinal("cnID_Candidato");
                cnID_TipoDocumento = dataReader.GetOrdinal("cnID_TipoDocumento");
                ctNumero = dataReader.GetOrdinal("ctNumero");
            }

            #endregion public methods
        }

        #region fields variables

        private Int64 _cnID_Candidato;
        private Int64 _cnID_TipoDocumento;
        private String _ctNumero;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Candidato
        {
            get { return _cnID_Candidato; }
            set { _cnID_Candidato = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_TipoDocumento
        {
            get { return _cnID_TipoDocumento; }
            set { _cnID_TipoDocumento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctNumero
        {
            get { return _ctNumero; }
            set { _ctNumero = value; }
        }

        #endregion fields property

        #region public methods

        public tbDocIdentidadModel()
        {
        }

        public void Insert()
        {
            using (tbDocIdentidadBLL bll = tbDocIdentidadBLL.CreateInstance())
            {
                bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbDocIdentidadBLL bll = tbDocIdentidadBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbDocIdentidadBLL bll = tbDocIdentidadBLL.CreateInstance())
            {
                bll.Delete(this.cnID_Candidato);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_Candidato = Convert.ToInt64(dataReader[fields.cnID_Candidato]);
            _cnID_TipoDocumento = Convert.ToInt64(dataReader[fields.cnID_TipoDocumento]);
            _ctNumero = Convert.ToString(dataReader[fields.ctNumero]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_Candidato = Convert.ToInt64(dataReader["cnID_Candidato"]);
            _cnID_TipoDocumento = Convert.ToInt64(dataReader["cnID_TipoDocumento"]);
            _ctNumero = Convert.ToString(dataReader["ctNumero"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbDocIdentidad
    /// </summary>
    public partial class tbMailModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbDocIdentidad
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_Candidato;
            public Int32 cnID_TipoEmail;
            public Int32 ctMail;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_Candidato = dataReader.GetOrdinal("cnID_Candidato");
                cnID_TipoEmail = dataReader.GetOrdinal("cnID_TipoEmail");
                ctMail = dataReader.GetOrdinal("ctMail");
            }

            #endregion public methods
        }

        #region fields variables

        private Int64 _cnID_Candidato;
        private Int64 _cnID_TipoEmail;
        private String _ctMail;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Candidato
        {
            get { return _cnID_Candidato; }
            set { _cnID_Candidato = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_TipoEmail
        {
            get { return _cnID_TipoEmail; }
            set { _cnID_TipoEmail = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctMail
        {
            get { return _ctMail; }
            set { _ctMail = value; }
        }

        #endregion fields property

        #region public methods

        public tbMailModel()
        {
        }

        public void Insert()
        {
            using (tbMailBLL bll = tbMailBLL.CreateInstance())
            {
                bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbMailBLL bll = tbMailBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbMailBLL bll = tbMailBLL.CreateInstance())
            {
                bll.Delete(this.cnID_Candidato);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_Candidato = Convert.ToInt64(dataReader[fields.cnID_Candidato]);
            _cnID_TipoEmail = Convert.ToInt64(dataReader[fields.cnID_TipoEmail]);
            _ctMail = Convert.ToString(dataReader[fields.ctMail]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_Candidato = Convert.ToInt64(dataReader["cnID_Candidato"]);
            _cnID_TipoEmail = Convert.ToInt64(dataReader["cnID_TipoEmail"]);
            _ctMail = Convert.ToString(dataReader["ctMail"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbEspecializacion
    /// </summary>
    public partial class tbEspecializacionModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbEspecializacion
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_Especializacion;
            public Int32 cnID_Candidato;
            public Int32 cnID_Especialidad;
            public Int32 ctEspecialidadOtro;
            public Int32 cnID_Nivel;
            public Int32 cnID_Pais;
            public Int32 ctCiudad;
            public Int32 cfFechaInicio;
            public Int32 cfFechaFin;
            public Int32 ctCertificado;
            public Int32 cfFechaCreacion;
            public Int32 cfFechaActualizacion;
            public Int32 ctIPAcceso;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_Especializacion = dataReader.GetOrdinal("cnID_Especializacion");
                cnID_Candidato = dataReader.GetOrdinal("cnID_Candidato");
                cnID_Especialidad = dataReader.GetOrdinal("cnID_Especialidad");
                ctEspecialidadOtro = dataReader.GetOrdinal("ctEspecialidadOtro");
                cnID_Nivel = dataReader.GetOrdinal("cnID_Nivel");
                cnID_Pais = dataReader.GetOrdinal("cnID_Pais");
                ctCiudad = dataReader.GetOrdinal("ctCiudad");
                cfFechaInicio = dataReader.GetOrdinal("cfFechaInicio");
                cfFechaFin = dataReader.GetOrdinal("cfFechaFin");
                ctCertificado = dataReader.GetOrdinal("ctCertificado");
                cfFechaCreacion = dataReader.GetOrdinal("cfFechaCreacion");
                cfFechaActualizacion = dataReader.GetOrdinal("cfFechaActualizacion");
                ctIPAcceso = dataReader.GetOrdinal("ctIPAcceso");
            }

            #endregion public methods
        }

        #region fields variables

        private Int64 _cnID_Especializacion;
        private Int64 _cnID_Candidato;
        private Int64 _cnID_Especialidad;
        private String _ctEspecialidadOtro;
        private Int64 _cnID_Nivel;
        private Int32 _cnID_Pais;
        private String _ctCiudad;
        private DateTime _cfFechaInicio;
        private DateTime _cfFechaFin;
        private String _ctCertificado;
        private DateTime _cfFechaCreacion;
        private DateTime? _cfFechaActualizacion;
        private String _ctIPAcceso;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Especializacion
        {
            get { return _cnID_Especializacion; }
            set { _cnID_Especializacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Candidato
        {
            get { return _cnID_Candidato; }
            set { _cnID_Candidato = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Especialidad
        {
            get { return _cnID_Especialidad; }
            set { _cnID_Especialidad = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctEspecialidadOtro
        {
            get { return _ctEspecialidadOtro; }
            set { _ctEspecialidadOtro = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Nivel
        {
            get { return _cnID_Nivel; }
            set { _cnID_Nivel = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_Pais
        {
            get { return _cnID_Pais; }
            set { _cnID_Pais = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctCiudad
        {
            get { return _ctCiudad; }
            set { _ctCiudad = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime cfFechaInicio
        {
            get { return _cfFechaInicio; }
            set { _cfFechaInicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime cfFechaFin
        {
            get { return _cfFechaFin; }
            set { _cfFechaFin = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctCertificado
        {
            get { return _ctCertificado; }
            set { _ctCertificado = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime cfFechaCreacion
        {
            get { return _cfFechaCreacion; }
            set { _cfFechaCreacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? cfFechaActualizacion
        {
            get { return _cfFechaActualizacion; }
            set { _cfFechaActualizacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctIPAcceso
        {
            get { return _ctIPAcceso; }
            set { _ctIPAcceso = value; }
        }

        #endregion fields property

        #region public methods

        public tbEspecializacionModel()
        {
        }

        public tbEspecializacionModel(Int64 cnID_Especializacion)
        {
            using (tbEspecializacionBLL bll = tbEspecializacionBLL.CreateInstance())
            {
                if (!bll.GetBycnID_Especializacion(cnID_Especializacion, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int64 Insert()
        {
            using (tbEspecializacionBLL bll = tbEspecializacionBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbEspecializacionBLL bll = tbEspecializacionBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbEspecializacionBLL bll = tbEspecializacionBLL.CreateInstance())
            {
                bll.Delete(this.cnID_Especializacion);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_Especializacion = Convert.ToInt64(dataReader[fields.cnID_Especializacion]);
            _cnID_Candidato = Convert.ToInt64(dataReader[fields.cnID_Candidato]);
            _cnID_Especialidad = Convert.ToInt64(dataReader[fields.cnID_Especialidad]);
            if (dataReader.IsDBNull(fields.ctEspecialidadOtro) == false)
                _ctEspecialidadOtro = Convert.ToString(dataReader[fields.ctEspecialidadOtro]);
            else
                _ctEspecialidadOtro = null;

            _cnID_Nivel = Convert.ToInt64(dataReader[fields.cnID_Nivel]);
            _cnID_Pais = Convert.ToInt32(dataReader[fields.cnID_Pais]);
            if (dataReader.IsDBNull(fields.ctCiudad) == false)
                _ctCiudad = Convert.ToString(dataReader[fields.ctCiudad]);
            else
                _ctCiudad = null;

            _cfFechaInicio = Convert.ToDateTime(dataReader[fields.cfFechaInicio]);
            _cfFechaFin = Convert.ToDateTime(dataReader[fields.cfFechaFin]);
            if (dataReader.IsDBNull(fields.ctCertificado) == false)
                _ctCertificado = Convert.ToString(dataReader[fields.ctCertificado]);
            else
                _ctCertificado = null;

            _cfFechaCreacion = Convert.ToDateTime(dataReader[fields.cfFechaCreacion]);
            if (dataReader.IsDBNull(fields.cfFechaActualizacion) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader[fields.cfFechaActualizacion]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader[fields.ctIPAcceso]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_Especializacion = Convert.ToInt64(dataReader["cnID_Especializacion"]);
            _cnID_Candidato = Convert.ToInt64(dataReader["cnID_Candidato"]);
            _cnID_Especialidad = Convert.ToInt64(dataReader["cnID_Especialidad"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctEspecialidadOtro")) == false)
                _ctEspecialidadOtro = Convert.ToString(dataReader["ctEspecialidadOtro"]);
            else
                _ctEspecialidadOtro = null;

            _cnID_Nivel = Convert.ToInt64(dataReader["cnID_Nivel"]);
            _cnID_Pais = Convert.ToInt32(dataReader["cnID_Pais"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctCiudad")) == false)
                _ctCiudad = Convert.ToString(dataReader["ctCiudad"]);
            else
                _ctCiudad = null;

            _cfFechaInicio = Convert.ToDateTime(dataReader["cfFechaInicio"]);
            _cfFechaFin = Convert.ToDateTime(dataReader["cfFechaFin"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctCertificado")) == false)
                _ctCertificado = Convert.ToString(dataReader["ctCertificado"]);
            else
                _ctCertificado = null;

            _cfFechaCreacion = Convert.ToDateTime(dataReader["cfFechaCreacion"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("cfFechaActualizacion")) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader["cfFechaActualizacion"]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader["ctIPAcceso"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbExperiencia
    /// </summary>
    public partial class tbExperienciaModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbExperiencia
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_Experiencia;
            public Int32 cnID_Candidato;
            public Int32 cnID_TipoEntidad;
            public Int32 ctNombreEmpresa;
            public Int32 cnID_Rubro;
            public Int32 cnID_Cargo;
            public Int32 cnID_Area;
            public Int32 cnID_TipoContrato;
            public Int32 cnID_Pais;
            public Int32 cnSubordinados;
            public Int32 ctDescripcionCargo;
            public Int32 cfFechaInicio;
            public Int32 cfFechaFin;
            public Int32 cnTiempoAnios;
            public Int32 cnTiempoMeses;
            public Int32 ctCertificado;
            public Int32 cfFechaCreacion;
            public Int32 cfFechaActualizacion;
            public Int32 ctIPAcceso;
            public Int32 ctCargo;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_Experiencia = dataReader.GetOrdinal("cnID_Experiencia");
                cnID_Candidato = dataReader.GetOrdinal("cnID_Candidato");
                cnID_TipoEntidad = dataReader.GetOrdinal("cnID_TipoEntidad");
                ctNombreEmpresa = dataReader.GetOrdinal("ctNombreEmpresa");
                cnID_Rubro = dataReader.GetOrdinal("cnID_Rubro");
                cnID_Cargo = dataReader.GetOrdinal("cnID_Cargo");
                cnID_Area = dataReader.GetOrdinal("cnID_Area");
                cnID_TipoContrato = dataReader.GetOrdinal("cnID_TipoContrato");
                cnID_Pais = dataReader.GetOrdinal("cnID_Pais");
                cnSubordinados = dataReader.GetOrdinal("cnSubordinados");
                ctDescripcionCargo = dataReader.GetOrdinal("ctDescripcionCargo");
                cfFechaInicio = dataReader.GetOrdinal("cfFechaInicio");
                cfFechaFin = dataReader.GetOrdinal("cfFechaFin");
                cnTiempoAnios = dataReader.GetOrdinal("cnTiempoAnios");
                cnTiempoMeses = dataReader.GetOrdinal("cnTiempoMeses");
                ctCertificado = dataReader.GetOrdinal("ctCertificado");
                cfFechaCreacion = dataReader.GetOrdinal("cfFechaCreacion");
                cfFechaActualizacion = dataReader.GetOrdinal("cfFechaActualizacion");
                ctIPAcceso = dataReader.GetOrdinal("ctIPAcceso");


                ctCargo = dataReader.GetOrdinal("ctCargo");
            }

            #endregion public methods
        }

        #region fields variables

        private Int64 _cnID_Experiencia;
        private Int64 _cnID_Candidato;
        private Int64 _cnID_TipoEntidad;
        private String _ctNombreEmpresa;
        private Int64 _cnID_Rubro;
        private Int64 _cnID_Cargo;
        private Int64 _cnID_Area;
        private Int64 _cnID_TipoContrato;
        private Int32 _cnID_Pais;
        private Int16? _cnSubordinados;
        private String _ctDescripcionCargo;
        private DateTime _cfFechaInicio;
        private DateTime _cfFechaFin;
        private int? _cnTiempoAnios;
        private int? _cnTiempoMeses;
        private String _ctCertificado;
        private DateTime _cfFechaCreacion;
        private DateTime? _cfFechaActualizacion;
        private String _ctIPAcceso;

        private bool _ctAceptaTerminos;



        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Experiencia
        {
            get { return _cnID_Experiencia; }
            set { _cnID_Experiencia = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Candidato
        {
            get { return _cnID_Candidato; }
            set { _cnID_Candidato = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_TipoEntidad
        {
            get { return _cnID_TipoEntidad; }
            set { _cnID_TipoEntidad = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctNombreEmpresa
        {
            get { return _ctNombreEmpresa; }
            set { _ctNombreEmpresa = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Rubro
        {
            get { return _cnID_Rubro; }
            set { _cnID_Rubro = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Cargo
        {
            get { return _cnID_Cargo; }
            set { _cnID_Cargo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Area
        {
            get { return _cnID_Area; }
            set { _cnID_Area = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_TipoContrato
        {
            get { return _cnID_TipoContrato; }
            set { _cnID_TipoContrato = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_Pais
        {
            get { return _cnID_Pais; }
            set { _cnID_Pais = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16? cnSubordinados
        {
            get { return _cnSubordinados; }
            set { _cnSubordinados = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctDescripcionCargo
        {
            get { return _ctDescripcionCargo; }
            set { _ctDescripcionCargo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime cfFechaInicio
        {
            get { return _cfFechaInicio; }
            set { _cfFechaInicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime cfFechaFin
        {
            get { return _cfFechaFin; }
            set { _cfFechaFin = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public int? cnTiempoAnios
        {
            get { return _cnTiempoAnios; }
            set { _cnTiempoAnios = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public int? cnTiempoMeses
        {
            get { return _cnTiempoMeses; }
            set { _cnTiempoMeses = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctCertificado
        {
            get { return _ctCertificado; }
            set { _ctCertificado = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime cfFechaCreacion
        {
            get { return _cfFechaCreacion; }
            set { _cfFechaCreacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? cfFechaActualizacion
        {
            get { return _cfFechaActualizacion; }
            set { _cfFechaActualizacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctIPAcceso
        {
            get { return _ctIPAcceso; }
            set { _ctIPAcceso = value; }
        }


        private string _ctCargo;

        public string ctCargo
        {
            get { return _ctCargo; }
            set { _ctCargo = value; }
        }

        public bool ctAceptaTerminos
        {
            get { return _ctAceptaTerminos; }
            set { _ctAceptaTerminos = value; }
        }



        #endregion fields property

        #region public methods

        public tbExperienciaModel()
        {
        }

        public tbExperienciaModel(Int64 cnID_Experiencia)
        {
            using (tbExperienciaBLL bll = tbExperienciaBLL.CreateInstance())
            {
                if (!bll.GetBycnID_Experiencia(cnID_Experiencia, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int64 Insert()
        {
            using (tbExperienciaBLL bll = tbExperienciaBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbExperienciaBLL bll = tbExperienciaBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbExperienciaBLL bll = tbExperienciaBLL.CreateInstance())
            {
                bll.Delete(this.cnID_Experiencia);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_Experiencia = Convert.ToInt64(dataReader[fields.cnID_Experiencia]);
            _cnID_Candidato = Convert.ToInt64(dataReader[fields.cnID_Candidato]);
            _cnID_TipoEntidad = Convert.ToInt64(dataReader[fields.cnID_TipoEntidad]);
            _ctNombreEmpresa = Convert.ToString(dataReader[fields.ctNombreEmpresa]);
            _cnID_Rubro = Convert.ToInt64(dataReader[fields.cnID_Rubro]);
            _cnID_Cargo = Convert.ToInt64(dataReader[fields.cnID_Cargo]);
            _cnID_Area = Convert.ToInt64(dataReader[fields.cnID_Area]);
            _cnID_TipoContrato = Convert.ToInt64(dataReader[fields.cnID_TipoContrato]);
            _cnID_Pais = Convert.ToInt32(dataReader[fields.cnID_Pais]);
            if (dataReader.IsDBNull(fields.cnSubordinados) == false)
                _cnSubordinados = Convert.ToInt16(dataReader[fields.cnSubordinados]);
            else
                _cnSubordinados = null;

            _ctDescripcionCargo = Convert.ToString(dataReader[fields.ctDescripcionCargo]);
            _cfFechaInicio = Convert.ToDateTime(dataReader[fields.cfFechaInicio]);
            _cfFechaFin = Convert.ToDateTime(dataReader[fields.cfFechaFin]);
            if (dataReader.IsDBNull(fields.cnTiempoAnios) == false)
                _cnTiempoAnios = Convert.ToByte(dataReader[fields.cnTiempoAnios]);
            else
                _cnTiempoAnios = null;

            if (dataReader.IsDBNull(fields.cnTiempoMeses) == false)
                _cnTiempoMeses = Convert.ToByte(dataReader[fields.cnTiempoMeses]);
            else
                _cnTiempoMeses = null;

            if (dataReader.IsDBNull(fields.ctCertificado) == false)
                _ctCertificado = Convert.ToString(dataReader[fields.ctCertificado]);
            else
                _ctCertificado = null;

            _cfFechaCreacion = Convert.ToDateTime(dataReader[fields.cfFechaCreacion]);
            if (dataReader.IsDBNull(fields.cfFechaActualizacion) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader[fields.cfFechaActualizacion]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader[fields.ctIPAcceso]);

            _ctCargo = Convert.ToString(dataReader[fields.ctCargo]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_Experiencia = Convert.ToInt64(dataReader["cnID_Experiencia"]);
            _cnID_Candidato = Convert.ToInt64(dataReader["cnID_Candidato"]);
            _cnID_TipoEntidad = Convert.ToInt64(dataReader["cnID_TipoEntidad"]);
            _ctNombreEmpresa = Convert.ToString(dataReader["ctNombreEmpresa"]);
            _cnID_Rubro = Convert.ToInt64(dataReader["cnID_Rubro"]);
            _cnID_Cargo = Convert.ToInt64(dataReader["cnID_Cargo"]);
            _cnID_Area = Convert.ToInt64(dataReader["cnID_Area"]);
            _cnID_TipoContrato = Convert.ToInt64(dataReader["cnID_TipoContrato"]);
            _cnID_Pais = Convert.ToInt32(dataReader["cnID_Pais"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("cnSubordinados")) == false)
                _cnSubordinados = Convert.ToInt16(dataReader["cnSubordinados"]);
            else
                _cnSubordinados = null;

            _ctDescripcionCargo = Convert.ToString(dataReader["ctDescripcionCargo"]);
            _cfFechaInicio = Convert.ToDateTime(dataReader["cfFechaInicio"]);
            _cfFechaFin = Convert.ToDateTime(dataReader["cfFechaFin"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("cnTiempoAnios")) == false)
                _cnTiempoAnios = Convert.ToByte(dataReader["cnTiempoAnios"]);
            else
                _cnTiempoAnios = null;

            if (dataReader.IsDBNull(dataReader.GetOrdinal("cnTiempoMeses")) == false)
                _cnTiempoMeses = Convert.ToByte(dataReader["cnTiempoMeses"]);
            else
                _cnTiempoMeses = null;

            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctCertificado")) == false)
                _ctCertificado = Convert.ToString(dataReader["ctCertificado"]);
            else
                _ctCertificado = null;

            _cfFechaCreacion = Convert.ToDateTime(dataReader["cfFechaCreacion"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("cfFechaActualizacion")) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader["cfFechaActualizacion"]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader["ctIPAcceso"]);

            if (base.HasColumn(dataReader, "ctCargo"))
                _ctCargo = Convert.ToString(dataReader["ctCargo"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbIdioma
    /// </summary>
    public partial class tbIdiomaModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbIdioma
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_Idioma;
            public Int32 cnID_Candidato;
            public Int32 cnID_Lengua;
            public Int32 ctLenguaOtro;
            public Int32 cnID_Institucion;
            public Int32 cnID_NivelLectura;
            public Int32 cnID_NivelEscritura;
            public Int32 cnID_NivelConversacion;
            public Int32 cnID_Situacion;
            public Int32 ctCertificado;
            public Int32 cfFechaCreacion;
            public Int32 cfFechaActualizacion;
            public Int32 ctIPAcceso;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_Idioma = dataReader.GetOrdinal("cnID_Idioma");
                cnID_Candidato = dataReader.GetOrdinal("cnID_Candidato");
                cnID_Lengua = dataReader.GetOrdinal("cnID_Lengua");
                ctLenguaOtro = dataReader.GetOrdinal("ctLenguaOtro");
                cnID_Institucion = dataReader.GetOrdinal("cnID_Institucion");
                cnID_NivelLectura = dataReader.GetOrdinal("cnID_NivelLectura");
                cnID_NivelEscritura = dataReader.GetOrdinal("cnID_NivelEscritura");
                cnID_NivelConversacion = dataReader.GetOrdinal("cnID_NivelConversacion");
                cnID_Situacion = dataReader.GetOrdinal("cnID_Situacion");
                ctCertificado = dataReader.GetOrdinal("ctCertificado");
                cfFechaCreacion = dataReader.GetOrdinal("cfFechaCreacion");
                cfFechaActualizacion = dataReader.GetOrdinal("cfFechaActualizacion");
                ctIPAcceso = dataReader.GetOrdinal("ctIPAcceso");
            }

            #endregion public methods
        }

        #region fields variables

        private Int64 _cnID_Idioma;
        private Int64 _cnID_Candidato;
        private Int64? _cnID_Lengua;
        private String _ctLenguaOtro;
        private Int64 _cnID_Institucion;
        private Int64 _cnID_NivelLectura;
        private Int64 _cnID_NivelEscritura;
        private Int64 _cnID_NivelConversacion;
        private Int64 _cnID_Situacion;
        private String _ctCertificado;
        private DateTime _cfFechaCreacion;
        private DateTime? _cfFechaActualizacion;
        private String _ctIPAcceso;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Idioma
        {
            get { return _cnID_Idioma; }
            set { _cnID_Idioma = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Candidato
        {
            get { return _cnID_Candidato; }
            set { _cnID_Candidato = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64? cnID_Lengua
        {
            get { return _cnID_Lengua; }
            set { _cnID_Lengua = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctLenguaOtro
        {
            get { return _ctLenguaOtro; }
            set { _ctLenguaOtro = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Institucion
        {
            get { return _cnID_Institucion; }
            set { _cnID_Institucion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_NivelLectura
        {
            get { return _cnID_NivelLectura; }
            set { _cnID_NivelLectura = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_NivelEscritura
        {
            get { return _cnID_NivelEscritura; }
            set { _cnID_NivelEscritura = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_NivelConversacion
        {
            get { return _cnID_NivelConversacion; }
            set { _cnID_NivelConversacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Situacion
        {
            get { return _cnID_Situacion; }
            set { _cnID_Situacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctCertificado
        {
            get { return _ctCertificado; }
            set { _ctCertificado = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime cfFechaCreacion
        {
            get { return _cfFechaCreacion; }
            set { _cfFechaCreacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? cfFechaActualizacion
        {
            get { return _cfFechaActualizacion; }
            set { _cfFechaActualizacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctIPAcceso
        {
            get { return _ctIPAcceso; }
            set { _ctIPAcceso = value; }
        }

        #endregion fields property

        #region public methods

        public tbIdiomaModel()
        {
        }

        public tbIdiomaModel(Int64 cnID_Idioma)
        {
            using (tbIdiomaBLL bll = tbIdiomaBLL.CreateInstance())
            {
                if (!bll.GetBycnID_Idioma(cnID_Idioma, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int64 Insert()
        {
            using (tbIdiomaBLL bll = tbIdiomaBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbIdiomaBLL bll = tbIdiomaBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbIdiomaBLL bll = tbIdiomaBLL.CreateInstance())
            {
                bll.Delete(this.cnID_Idioma);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_Idioma = Convert.ToInt64(dataReader[fields.cnID_Idioma]);
            _cnID_Candidato = Convert.ToInt64(dataReader[fields.cnID_Candidato]);

            if (dataReader.IsDBNull(fields.cnID_Lengua) == false)
                _cnID_Lengua = Convert.ToInt64(dataReader[fields.cnID_Lengua]);
            else
                _cnID_Lengua = null;

            if (dataReader.IsDBNull(fields.ctLenguaOtro) == false)
                _ctLenguaOtro = Convert.ToString(dataReader[fields.ctLenguaOtro]);
            else
                _ctLenguaOtro = null;

            _cnID_Institucion = Convert.ToInt64(dataReader[fields.cnID_Institucion]);
            _cnID_NivelLectura = Convert.ToInt64(dataReader[fields.cnID_NivelLectura]);
            _cnID_NivelEscritura = Convert.ToInt64(dataReader[fields.cnID_NivelEscritura]);
            _cnID_NivelConversacion = Convert.ToInt64(dataReader[fields.cnID_NivelConversacion]);
            _cnID_Situacion = Convert.ToInt64(dataReader[fields.cnID_Situacion]);

            if (dataReader.IsDBNull(fields.ctCertificado) == false)
                _ctCertificado = Convert.ToString(dataReader[fields.ctCertificado]);
            else
                _ctCertificado = null;

            _cfFechaCreacion = Convert.ToDateTime(dataReader[fields.cfFechaCreacion]);

            if (dataReader.IsDBNull(fields.cfFechaActualizacion) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader[fields.cfFechaActualizacion]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader[fields.ctIPAcceso]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_Idioma = Convert.ToInt64(dataReader["cnID_Idioma"]);
            _cnID_Candidato = Convert.ToInt64(dataReader["cnID_Candidato"]);

            if (dataReader.IsDBNull(dataReader.GetOrdinal("cnID_Lengua")) == false)
                _cnID_Lengua = Convert.ToInt64(dataReader["cnID_Lengua"]);
            else
                _cnID_Lengua = null;

            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctLenguaOtro")) == false)
                _ctLenguaOtro = Convert.ToString(dataReader["ctLenguaOtro"]);
            else
                _ctLenguaOtro = null;

            _cnID_Institucion = Convert.ToInt64(dataReader["cnID_Institucion"]);
            _cnID_NivelLectura = Convert.ToInt64(dataReader["cnID_NivelLectura"]);
            _cnID_NivelEscritura = Convert.ToInt64(dataReader["cnID_NivelEscritura"]);
            _cnID_NivelConversacion = Convert.ToInt64(dataReader["cnID_NivelConversacion"]);
            _cnID_Situacion = Convert.ToInt64(dataReader["cnID_Situacion"]);

            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctCertificado")) == false)
                _ctCertificado = Convert.ToString(dataReader["ctCertificado"]);
            else
                _ctCertificado = null;

            _cfFechaCreacion = Convert.ToDateTime(dataReader["cfFechaCreacion"]);

            if (dataReader.IsDBNull(dataReader.GetOrdinal("cfFechaActualizacion")) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader["cfFechaActualizacion"]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader["ctIPAcceso"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbMaestroGrupos
    /// </summary>
    public partial class tbMaestroGruposModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbMaestroGrupos
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_GrupoLista;
            public Int32 ctNombreGrupo;
            public Int32 cnEstado;
            public Int32 ctTipoGrupo;
            public Int32 cnGrupoPadre;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_GrupoLista = dataReader.GetOrdinal("cnID_GrupoLista");
                ctNombreGrupo = dataReader.GetOrdinal("ctNombreGrupo");
                cnEstado = dataReader.GetOrdinal("cnEstado");
                ctTipoGrupo = dataReader.GetOrdinal("ctTipoGrupo");
                cnGrupoPadre = dataReader.GetOrdinal("cnGrupoPadre");
            }

            #endregion public methods
        }

        #region fields variables

        private Int32 _cnID_GrupoLista;
        private String _ctNombreGrupo;
        private int _cnEstado;
        private String _ctTipoGrupo;
        private Int32 _cnGrupoPadre;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_GrupoLista
        {
            get { return _cnID_GrupoLista; }
            set { _cnID_GrupoLista = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctNombreGrupo
        {
            get { return _ctNombreGrupo; }
            set { _ctNombreGrupo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public int cnEstado
        {
            get { return _cnEstado; }
            set { _cnEstado = value; }
        }

        public string ctTipoGrupo
        {
            get { return _ctTipoGrupo; }
            set { _ctTipoGrupo = value; }
        }

        public Int32 cnGrupoPadre
        {
            get { return _cnGrupoPadre; }
            set { _cnGrupoPadre = value; }
        }

        #endregion fields property

        #region public methods

        public tbMaestroGruposModel()
        {
        }

        public tbMaestroGruposModel(Int32 cnID_GrupoLista)
        {
            using (tbMaestroGruposBLL bll = tbMaestroGruposBLL.CreateInstance())
            {
                if (!bll.GetBycnID_GrupoLista(cnID_GrupoLista, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int32 Insert()
        {
            using (tbMaestroGruposBLL bll = tbMaestroGruposBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbMaestroGruposBLL bll = tbMaestroGruposBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbMaestroGruposBLL bll = tbMaestroGruposBLL.CreateInstance())
            {
                bll.Delete(this.cnID_GrupoLista);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_GrupoLista = Convert.ToInt32(dataReader[fields.cnID_GrupoLista]);
            _ctNombreGrupo = Convert.ToString(dataReader[fields.ctNombreGrupo]);
            _cnEstado = Convert.ToByte(dataReader[fields.cnEstado]);

            _ctTipoGrupo = Convert.ToString(dataReader[fields.ctTipoGrupo]);
            if (dataReader[fields.cnGrupoPadre] != DBNull.Value)
                _cnGrupoPadre = Convert.ToInt32(dataReader[fields.cnGrupoPadre]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_GrupoLista = Convert.ToInt32(dataReader["cnID_GrupoLista"]);
            _ctNombreGrupo = Convert.ToString(dataReader["ctNombreGrupo"]);
            _cnEstado = Convert.ToByte(dataReader["cnEstado"]);

            _ctTipoGrupo = Convert.ToString(dataReader["ctTipoGrupo"]);

            if (dataReader["cnGrupoPadre"] != DBNull.Value)
                _cnGrupoPadre = Convert.ToInt32(dataReader["cnGrupoPadre"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbMaestroListas
    /// </summary>
    public partial class tbMaestroListasModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbMaestroListas
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_Lista;
            public Int32 cnID_GrupoLista;
            public Int32 ctElemento;
            public Int32 cnEstado;
            public Int32 cnOrden;
            public Int32 cnObligatorio;
            public Int32 cnChecked;
            public Int32 cnID_ListaPadre;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_Lista = dataReader.GetOrdinal("cnID_Lista");
                cnID_GrupoLista = dataReader.GetOrdinal("cnID_GrupoLista");
                ctElemento = dataReader.GetOrdinal("ctElemento");
                cnEstado = dataReader.GetOrdinal("cnEstado");
                cnOrden = dataReader.GetOrdinal("cnOrden");
                cnObligatorio = dataReader.GetOrdinal("cnObligatorio");


                if (DataReaderExtensions.ContainsColumn(dataReader, "cnChecked"))
                    cnChecked = dataReader.GetOrdinal("cnChecked");

                if (DataReaderExtensions.ContainsColumn(dataReader, "cnID_ListaPadre"))
                    cnID_ListaPadre = dataReader.GetOrdinal("cnID_ListaPadre");


            }

            #endregion public methods
        }

        #region fields variables

        private Int64 _cnID_Lista;
        private Int32 _cnID_GrupoLista;
        private String _ctElemento;
        private Byte _cnEstado;
        private Int32? _cnOrden;
        private bool _cnObligatorio;
        private bool _cnChecked;
        private Int64? _cnID_ListaPadre;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Lista
        {
            get { return _cnID_Lista; }
            set { _cnID_Lista = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_GrupoLista
        {
            get { return _cnID_GrupoLista; }
            set { _cnID_GrupoLista = value; }
        }

        public Int32 cnID_SubGrupoLista { get; set; }

        /// <summary>
        ///
        /// </summary>
        public String ctElemento
        {
            get { return _ctElemento; }
            set { _ctElemento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Byte cnEstado
        {
            get { return _cnEstado; }
            set { _cnEstado = value; }
        }

        public Int32? cnOrden
        {
            get { return _cnOrden; }
            set { _cnOrden = value; }
        }

        public Int64? cnID_ListaPadre
        {
            get
            {
                return _cnID_ListaPadre;
            }
            set
            {
                _cnID_ListaPadre = value;
            }
        }


        public bool cnObligatorio
        {
            get { return _cnObligatorio; }
            set { _cnObligatorio = value; }
        }


        public bool cnChecked
        {
            get { return _cnChecked; }
            set { _cnChecked = value; }
        }



        #endregion fields property

        #region public methods

        public tbMaestroListasModel()
        {
        }

        public tbMaestroListasModel(Int64 cnID_Lista)
        {
            using (tbMaestroListasBLL bll = tbMaestroListasBLL.CreateInstance())
            {
                if (!bll.GetBycnID_Lista(cnID_Lista, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int64 Insert()
        {
            using (tbMaestroListasBLL bll = tbMaestroListasBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbMaestroListasBLL bll = tbMaestroListasBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbMaestroListasBLL bll = tbMaestroListasBLL.CreateInstance())
            {
                bll.Delete(this.cnID_Lista);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_Lista = Convert.ToInt64(dataReader[fields.cnID_Lista]);
            _cnID_GrupoLista = Convert.ToInt32(dataReader[fields.cnID_GrupoLista]);
            _ctElemento = Convert.ToString(dataReader[fields.ctElemento]);
            _cnEstado = Convert.ToByte(dataReader[fields.cnEstado]);
            if (dataReader.IsDBNull(fields.cnOrden) == false)
                _cnOrden = Convert.ToInt32(dataReader[fields.cnOrden]);

            if (dataReader.IsDBNull(fields.cnObligatorio) == false)
                _cnObligatorio = Convert.ToBoolean(dataReader[fields.cnObligatorio]);

            //if (DataReaderExtensions.ContainsColumn(dataReader, "cnChecked"))
            _cnChecked = Convert.ToBoolean(dataReader[fields.cnChecked]);

            //if (DataReaderExtensions.ContainsColumn(dataReader, "cnID_ListaPadre"))
            if (dataReader.IsDBNull(fields.cnID_ListaPadre) == false)
                _cnID_ListaPadre = Convert.ToInt64(dataReader[fields.cnID_ListaPadre]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_Lista = Convert.ToInt64(dataReader["cnID_Lista"]);
            _cnID_GrupoLista = Convert.ToInt32(dataReader["cnID_GrupoLista"]);
            _ctElemento = Convert.ToString(dataReader["ctElemento"]);
            _cnEstado = Convert.ToByte(dataReader["cnEstado"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("cnOrden")) == false)
                _cnOrden = Convert.ToInt32(dataReader["cnOrden"]);

            if (dataReader.IsDBNull(dataReader.GetOrdinal("cnObligatorio")) == false)
                _cnObligatorio = Convert.ToBoolean(dataReader["cnObligatorio"]);

            if (DataReaderExtensions.ContainsColumn(dataReader, "cnChecked"))
                _cnChecked = Convert.ToBoolean(dataReader["cnChecked"]);

            if (DataReaderExtensions.ContainsColumn(dataReader, "cnID_ListaPadre"))
                if (dataReader.IsDBNull(dataReader.GetOrdinal("cnID_ListaPadre")) == false)
                    _cnID_ListaPadre = Convert.ToInt64(dataReader["cnID_ListaPadre"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbMenu
    /// </summary>
    public partial class tbMenuModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbMenu
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_Menu;
            public Int32 cnID_Perfil;
            public Int32 ctTitulo;
            public Int32 ctDescipcion;
            public Int32 ctPagina;
            public Int32 cnOrden;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_Menu = dataReader.GetOrdinal("cnID_Menu");
                cnID_Perfil = dataReader.GetOrdinal("cnID_Perfil");
                ctTitulo = dataReader.GetOrdinal("ctTitulo");
                ctDescipcion = dataReader.GetOrdinal("ctDescipcion");
                ctPagina = dataReader.GetOrdinal("ctPagina");
                cnOrden = dataReader.GetOrdinal("cnOrden");
            }

            #endregion public methods
        }

        #region fields variables

        private Int64 _cnID_Menu;
        private Int32 _cnID_Perfil;
        private String _ctTitulo;
        private String _ctDescipcion;
        private String _ctPagina;
        private Int32 _cnOrden;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Menu
        {
            get { return _cnID_Menu; }
            set { _cnID_Menu = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_Perfil
        {
            get { return _cnID_Perfil; }
            set { _cnID_Perfil = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctTitulo
        {
            get { return _ctTitulo; }
            set { _ctTitulo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctDescipcion
        {
            get { return _ctDescipcion; }
            set { _ctDescipcion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctPagina
        {
            get { return _ctPagina; }
            set { _ctPagina = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnOrden
        {
            get { return _cnOrden; }
            set { _cnOrden = value; }
        }

        #endregion fields property

        #region public methods

        public tbMenuModel()
        {
        }

        public tbMenuModel(Int64 cnID_Menu)
        {
            using (tbMenuBLL bll = tbMenuBLL.CreateInstance())
            {
                if (!bll.GetBycnID_Menu(cnID_Menu, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int64 Insert()
        {
            using (tbMenuBLL bll = tbMenuBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbMenuBLL bll = tbMenuBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbMenuBLL bll = tbMenuBLL.CreateInstance())
            {
                bll.Delete(this.cnID_Menu);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_Menu = Convert.ToInt64(dataReader[fields.cnID_Menu]);
            _cnID_Perfil = Convert.ToInt32(dataReader[fields.cnID_Perfil]);
            _ctTitulo = Convert.ToString(dataReader[fields.ctTitulo]);
            if (dataReader.IsDBNull(fields.ctDescipcion) == false)
                _ctDescipcion = Convert.ToString(dataReader[fields.ctDescipcion]);
            else
                _ctDescipcion = null;

            _ctPagina = Convert.ToString(dataReader[fields.ctPagina]);
            _cnOrden = Convert.ToInt32(dataReader[fields.cnOrden]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_Menu = Convert.ToInt64(dataReader["cnID_Menu"]);
            _cnID_Perfil = Convert.ToInt32(dataReader["cnID_Perfil"]);
            _ctTitulo = Convert.ToString(dataReader["ctTitulo"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctDescipcion")) == false)
                _ctDescipcion = Convert.ToString(dataReader["ctDescipcion"]);
            else
                _ctDescipcion = null;

            _ctPagina = Convert.ToString(dataReader["ctPagina"]);
            _cnOrden = Convert.ToInt32(dataReader["cnOrden"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbPais
    /// </summary>
    public partial class tbPaisModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbPais
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_Pais;
            public Int32 ctPais;
            public Int32 ctCodigoISO;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_Pais = dataReader.GetOrdinal("cnID_Pais");
                ctPais = dataReader.GetOrdinal("ctPais");
                ctCodigoISO = dataReader.GetOrdinal("ctCodigoISO");
            }

            #endregion public methods
        }

        #region fields variables

        private Int32 _cnID_Pais;
        private String _ctPais;
        private String _ctCodigoISO;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_Pais
        {
            get { return _cnID_Pais; }
            set { _cnID_Pais = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctPais
        {
            get { return _ctPais; }
            set { _ctPais = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctCodigoISO
        {
            get { return _ctCodigoISO; }
            set { _ctCodigoISO = value; }
        }

        #endregion fields property

        #region public methods

        public tbPaisModel()
        {
        }

        public tbPaisModel(Int32 cnID_Pais)
        {
            using (tbPaisBLL bll = tbPaisBLL.CreateInstance())
            {
                if (!bll.GetBycnID_Pais(cnID_Pais, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int32 Insert()
        {
            using (tbPaisBLL bll = tbPaisBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbPaisBLL bll = tbPaisBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbPaisBLL bll = tbPaisBLL.CreateInstance())
            {
                bll.Delete(this.cnID_Pais);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_Pais = Convert.ToInt32(dataReader[fields.cnID_Pais]);
            if (dataReader.IsDBNull(fields.ctPais) == false)
                _ctPais = Convert.ToString(dataReader[fields.ctPais]);
            else
                _ctPais = null;

            if (dataReader.IsDBNull(fields.ctCodigoISO) == false)
                _ctCodigoISO = Convert.ToString(dataReader[fields.ctCodigoISO]);
            else
                _ctCodigoISO = null;

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_Pais = Convert.ToInt32(dataReader["cnID_Pais"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctPais")) == false)
                _ctPais = Convert.ToString(dataReader["ctPais"]);
            else
                _ctPais = null;

            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctCodigoISO")) == false)
                _ctCodigoISO = Convert.ToString(dataReader["ctCodigoISO"]);
            else
                _ctCodigoISO = null;

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbPerfilUsuario
    /// </summary>
    public partial class tbPerfilUsuarioModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbPerfilUsuario
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_Perfil;
            public Int32 ctDescripcion;
            public Int32 cnEstadoReg;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_Perfil = dataReader.GetOrdinal("cnID_Perfil");
                ctDescripcion = dataReader.GetOrdinal("ctDescripcion");
                cnEstadoReg = dataReader.GetOrdinal("cnEstadoReg");
            }

            #endregion public methods
        }

        #region fields variables

        private Int32 _cnID_Perfil;
        private String _ctDescripcion;
        private Boolean _cnEstadoReg;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_Perfil
        {
            get { return _cnID_Perfil; }
            set { _cnID_Perfil = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctDescripcion
        {
            get { return _ctDescripcion; }
            set { _ctDescripcion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Boolean cnEstadoReg
        {
            get { return _cnEstadoReg; }
            set { _cnEstadoReg = value; }
        }

        #endregion fields property

        #region public methods

        public tbPerfilUsuarioModel()
        {
        }

        public void Insert()
        {
            using (tbPerfilUsuarioBLL bll = tbPerfilUsuarioBLL.CreateInstance())
            {
                bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbPerfilUsuarioBLL bll = tbPerfilUsuarioBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbPerfilUsuarioBLL bll = tbPerfilUsuarioBLL.CreateInstance())
            {
                bll.Delete(this.cnID_Perfil);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_Perfil = Convert.ToInt32(dataReader[fields.cnID_Perfil]);
            _ctDescripcion = Convert.ToString(dataReader[fields.ctDescripcion]);
            _cnEstadoReg = Convert.ToBoolean(dataReader[fields.cnEstadoReg]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_Perfil = Convert.ToInt32(dataReader["cnID_Perfil"]);
            _ctDescripcion = Convert.ToString(dataReader["ctDescripcion"]);
            _cnEstadoReg = Convert.ToBoolean(dataReader["cnEstadoReg"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbPostulante
    /// </summary>
    public partial class tbPostulanteModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbPostulante
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_Proceso;
            public Int32 cnID_Candidato;
            public Int32 cnID_Postulante;
            public Int32 cnPuntosCriterioLaboral;
            public Int32 cnPuntosCriterioAcademico;
            public Int32 cnPuntosCriterioAdicional;
            public Int32 cfFechaCreacion;
            public Int32 cfFechaActualizacion;
            public Int32 ctIPAcceso;
            public Int32 cnEvalAuto;
            public Int32 cnEvalManual;
            public Int32 cnEstadoReg;
            public Int32 ctNumeroDNI;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_Proceso = dataReader.GetOrdinal("cnID_Proceso");
                cnID_Candidato = dataReader.GetOrdinal("cnID_Candidato");
                cnID_Postulante = dataReader.GetOrdinal("cnID_Postulante");
                cnPuntosCriterioLaboral = dataReader.GetOrdinal("cnPuntosCriterioLaboral");
                cnPuntosCriterioAcademico = dataReader.GetOrdinal("cnPuntosCriterioAcademico");
                cnPuntosCriterioAdicional = dataReader.GetOrdinal("cnPuntosCriterioAdicional");
                cfFechaCreacion = dataReader.GetOrdinal("cfFechaCreacion");
                cfFechaActualizacion = dataReader.GetOrdinal("cfFechaActualizacion");
                ctIPAcceso = dataReader.GetOrdinal("ctIPAcceso");
                cnEvalAuto = dataReader.GetOrdinal("cnEvalAuto");
                cnEvalManual = dataReader.GetOrdinal("cnEvalManual");
                cnEstadoReg = dataReader.GetOrdinal("cnEstadoReg");
                //Campos personalizados
                if (DataReaderExtensions.ContainsColumn(dataReader, "ctNumero"))
                    ctNumeroDNI = dataReader.GetOrdinal("ctNumero");
                //Campos personalizados
            }

            #endregion public methods
        }

        #region fields variables

        private Int64 _cnID_Proceso;
        private Int64 _cnID_Candidato;
        private Int64 _cnID_Postulante;
        private Int32 _cnPuntosCriterioLaboral;
        private Int32 _cnPuntosCriterioAcademico;
        private Int32 _cnPuntosCriterioAdicional;
        private DateTime _cfFechaCreacion;
        private DateTime? _cfFechaActualizacion;
        private String _ctIPAcceso;
        private string _ctApellidoPaterno;
        private string _ctApellidoMaterno;
        private string _ctNombres;
        private string _ctEmail;
        private string _ctTelefonoCasa;
        private string _ctTelefonoMovil;
        private string _ctCobertura;
        private string _ctZona;

        //private Int64 _cnID_Grado;
        private bool _cnEvalAuto;

        private bool _cnEvalManual;
        private Int16 _cnEstadoReg;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Proceso
        {
            get { return _cnID_Proceso; }
            set { _cnID_Proceso = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Candidato
        {
            get { return _cnID_Candidato; }
            set { _cnID_Candidato = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Postulante
        {
            get { return _cnID_Postulante; }
            set { _cnID_Postulante = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnPuntosCriterioLaboral
        {
            get { return _cnPuntosCriterioLaboral; }
            set { _cnPuntosCriterioLaboral = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnPuntosCriterioAcademico
        {
            get { return _cnPuntosCriterioAcademico; }
            set { _cnPuntosCriterioAcademico = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnPuntosCriterioAdicional
        {
            get { return _cnPuntosCriterioAdicional; }
            set { _cnPuntosCriterioAdicional = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime cfFechaCreacion
        {
            get { return _cfFechaCreacion; }
            set { _cfFechaCreacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? cfFechaActualizacion
        {
            get { return _cfFechaActualizacion; }
            set { _cfFechaActualizacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctIPAcceso
        {
            get { return _ctIPAcceso; }
            set { _ctIPAcceso = value; }
        }

        //public Int64 cnID_Grado
        //{
        //    get { return _cnID_Grado; }
        //    set { _cnID_Grado = value; }
        //}

        public bool cnEvalAuto
        {
            get { return _cnEvalAuto; }
            set { _cnEvalAuto = value; }
        }

        public bool cnEvalManual
        {
            get { return _cnEvalManual; }
            set { _cnEvalManual = value; }
        }

        public Int16 cnEstadoReg
        {
            get { return _cnEstadoReg; }
            set { _cnEstadoReg = value; }
        }

        public string ctEmail
        {
            get { return _ctEmail; }
            set { _ctEmail = value; }
        }

        public string ctTelefonoCasa
        {
            get { return _ctTelefonoCasa; }
            set { _ctTelefonoCasa = value; }
        }

        public string ctTelefonoMovil
        {
            get { return _ctTelefonoMovil; }
            set { _ctTelefonoMovil = value; }
        }

        public string ctCobertura
        {
            get { return _ctCobertura; }
            set { _ctCobertura = value; }
        }

        public string ctZona
        {
            get { return _ctZona; }
            set { _ctZona = value; }
        }

        #endregion fields property

        #region public methods

        public tbPostulanteModel()
        {
        }

        public tbPostulanteModel(Int64 cnID_Postulante)
        {
            using (tbPostulanteBLL bll = tbPostulanteBLL.CreateInstance())
            {
                if (!bll.GetBycnID_Postulante(cnID_Postulante, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int64 Insert()
        {
            using (tbPostulanteBLL bll = tbPostulanteBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbPostulanteBLL bll = tbPostulanteBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbPostulanteBLL bll = tbPostulanteBLL.CreateInstance())
            {
                bll.Delete(this.cnID_Postulante);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_Proceso = Convert.ToInt64(dataReader[fields.cnID_Proceso]);
            _cnID_Candidato = Convert.ToInt64(dataReader[fields.cnID_Candidato]);
            _cnID_Postulante = Convert.ToInt64(dataReader[fields.cnID_Postulante]);
            _cnPuntosCriterioLaboral = Convert.ToInt32(dataReader[fields.cnPuntosCriterioLaboral]);
            _cnPuntosCriterioAcademico = Convert.ToInt32(dataReader[fields.cnPuntosCriterioAcademico]);
            _cnPuntosCriterioAdicional = Convert.ToInt32(dataReader[fields.cnPuntosCriterioAdicional]);
            _cfFechaCreacion = Convert.ToDateTime(dataReader[fields.cfFechaCreacion]);
            if (dataReader.IsDBNull(fields.cfFechaActualizacion) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader[fields.cfFechaActualizacion]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader[fields.ctIPAcceso]);

            if (HasColumn(dataReader, "ctApellidoPaterno"))
            {
                _ctApellidoPaterno = dataReader["ctApellidoPaterno"].ToString();
            }

            if (HasColumn(dataReader, "ctApellidoMaterno"))
            {
                _ctApellidoMaterno = dataReader["ctApellidoMaterno"].ToString();
            }

            if (HasColumn(dataReader, "ctNombres"))
            {
                _ctNombres = dataReader["ctNombres"].ToString();
            }

            if (HasColumn(dataReader, "ctEmail"))
            {
                _ctEmail = dataReader["ctEmail"].ToString();
            }

            if (HasColumn(dataReader, "ctTelefonoCasa"))
            {
                _ctTelefonoCasa = dataReader["ctTelefonoCasa"].ToString();
            }

            if (HasColumn(dataReader, "ctTelefonoMovil"))
            {
                _ctTelefonoMovil = dataReader["ctTelefonoMovil"].ToString();
            }

            if (HasColumn(dataReader, "ctCobertura"))
            {
                _ctCobertura = dataReader["ctCobertura"].ToString();
            }

            if (HasColumn(dataReader, "ctZona"))
            {
                _ctZona = dataReader["ctZona"].ToString();
            }

            _cnEvalAuto = Convert.ToBoolean(dataReader[fields.cnEvalAuto]);

            if (dataReader.IsDBNull(fields.cnEstadoReg) == false)
                cnEstadoReg = Convert.ToInt16(dataReader[fields.cnEstadoReg]);

            //Campos personalizados
            if (dataReader.IsDBNull(fields.ctNumeroDNI) == false)
                NumeroDNI = Convert.ToString(dataReader[fields.ctNumeroDNI]);
            else
                NumeroDNI = null;
            //Campos personalizados

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_Proceso = Convert.ToInt64(dataReader["cnID_Proceso"]);
            _cnID_Candidato = Convert.ToInt64(dataReader["cnID_Candidato"]);
            _cnID_Postulante = Convert.ToInt64(dataReader["cnID_Postulante"]);
            _cnPuntosCriterioLaboral = Convert.ToInt32(dataReader["cnPuntosCriterioLaboral"]);
            _cnPuntosCriterioAcademico = Convert.ToInt32(dataReader["cnPuntosCriterioAcademico"]);
            _cnPuntosCriterioAdicional = Convert.ToInt32(dataReader["cnPuntosCriterioAdicional"]);
            _cfFechaCreacion = Convert.ToDateTime(dataReader["cfFechaCreacion"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("cfFechaActualizacion")) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader["cfFechaActualizacion"]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader["ctIPAcceso"]);

            //if (dataReader.IsDBNull(dataReader.GetOrdinal("cnID_Grado")) == false)
            //    _cnID_Grado = Convert.ToInt64(dataReader["cnID_Grado"]);

            _cnEvalAuto = Convert.ToBoolean(dataReader["cnEvalAuto"]);

            if (dataReader.IsDBNull(dataReader.GetOrdinal("cnEstadoReg")) == false)
                cnEstadoReg = Convert.ToInt16(dataReader["cnEstadoReg"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbPostulanteCobe
    /// </summary>
    public partial class tbPostulanteCobeModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbPostulanteCobe
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_PostulanteCobe;
            public Int32 cnID_Postulante;
            public Int32 cnID_Cobertura;
            public Int32 cnID_CoberturaZona;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_PostulanteCobe = dataReader.GetOrdinal("cnID_PostulanteCobe");
                cnID_Postulante = dataReader.GetOrdinal("cnID_Postulante");
                cnID_Cobertura = dataReader.GetOrdinal("cnID_Cobertura");
                cnID_CoberturaZona = dataReader.GetOrdinal("cnID_CoberturaZona");
            }

            #endregion public methods
        }

        #region fields variables

        private Int64 _cnID_PostulanteCobe;
        private Int64 _cnID_Postulante;
        private Int64 _cnID_Cobertura;
        private Int64 _cnID_CoberturaZona;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_PostulanteCobe
        {
            get { return _cnID_PostulanteCobe; }
            set { _cnID_PostulanteCobe = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Postulante
        {
            get { return _cnID_Postulante; }
            set { _cnID_Postulante = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Cobertura
        {
            get { return _cnID_Cobertura; }
            set { _cnID_Cobertura = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_CoberturaZona
        {
            get { return _cnID_CoberturaZona; }
            set { _cnID_CoberturaZona = value; }
        }

        #endregion fields property

        #region public methods

        public tbPostulanteCobeModel()
        {
        }

        public tbPostulanteCobeModel(Int64 cnID_PostulanteCobe)
        {
            using (tbPostulanteCobeBLL bll = tbPostulanteCobeBLL.CreateInstance())
            {
                if (!bll.GetBycnID_PostulanteCobe(cnID_PostulanteCobe, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int64 Insert()
        {
            using (tbPostulanteCobeBLL bll = tbPostulanteCobeBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbPostulanteCobeBLL bll = tbPostulanteCobeBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbPostulanteCobeBLL bll = tbPostulanteCobeBLL.CreateInstance())
            {
                bll.Delete(this.cnID_PostulanteCobe);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_PostulanteCobe = Convert.ToInt64(dataReader[fields.cnID_PostulanteCobe]);
            _cnID_Postulante = Convert.ToInt64(dataReader[fields.cnID_Postulante]);
            _cnID_Cobertura = Convert.ToInt64(dataReader[fields.cnID_Cobertura]);
            _cnID_CoberturaZona = Convert.ToInt64(dataReader[fields.cnID_CoberturaZona]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_PostulanteCobe = Convert.ToInt64(dataReader["cnID_PostulanteCobe"]);
            _cnID_Postulante = Convert.ToInt64(dataReader["cnID_Postulante"]);
            _cnID_Cobertura = Convert.ToInt64(dataReader["cnID_Cobertura"]);
            _cnID_CoberturaZona = Convert.ToInt64(dataReader["cnID_CoberturaZona"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbPostulanteDocu
    /// </summary>
    public partial class tbPostulanteDocuModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbPostulanteDocu
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_PostulanteDocu;
            public Int32 cnID_Postulante;
            public Int32 cnID_Documento;
            public Int32 ctRutaArchivo;
            public Int32 cfFechaCreacion;
            public Int32 cfFechaActualizacion;
            public Int32 ctIPAcceso;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_PostulanteDocu = dataReader.GetOrdinal("cnID_PostulanteDocu");
                cnID_Postulante = dataReader.GetOrdinal("cnID_Postulante");
                cnID_Documento = dataReader.GetOrdinal("cnID_Documento");
                ctRutaArchivo = dataReader.GetOrdinal("ctRutaArchivo");
                //cfFechaCreacion = dataReader.GetOrdinal("cfFechaCreacion");
                //cfFechaActualizacion = dataReader.GetOrdinal("cfFechaActualizacion");
                //ctIPAcceso = dataReader.GetOrdinal("ctIPAcceso");
            }

            #endregion public methods
        }

        #region fields variables

        private Int64 _cnID_PostulanteDocu;
        private Int64 _cnID_Postulante;
        private Int32 _cnID_Documento;
        private String _ctRutaArchivo;
        private DateTime _cfFechaCreacion;
        private DateTime? _cfFechaActualizacion;
        private String _ctIPAcceso;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_PostulanteDocu
        {
            get { return _cnID_PostulanteDocu; }
            set { _cnID_PostulanteDocu = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Postulante
        {
            get { return _cnID_Postulante; }
            set { _cnID_Postulante = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_Documento
        {
            get { return _cnID_Documento; }
            set { _cnID_Documento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctRutaArchivo
        {
            get { return _ctRutaArchivo; }
            set { _ctRutaArchivo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime cfFechaCreacion
        {
            get { return _cfFechaCreacion; }
            set { _cfFechaCreacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? cfFechaActualizacion
        {
            get { return _cfFechaActualizacion; }
            set { _cfFechaActualizacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctIPAcceso
        {
            get { return _ctIPAcceso; }
            set { _ctIPAcceso = value; }
        }

        #endregion fields property

        #region public methods

        public tbPostulanteDocuModel()
        {
        }

        public tbPostulanteDocuModel(Int64 cnID_PostulanteDocu)
        {
            using (tbPostulanteDocuBLL bll = tbPostulanteDocuBLL.CreateInstance())
            {
                if (!bll.GetBycnID_PostulanteDocu(cnID_PostulanteDocu, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int64 Insert()
        {
            using (tbPostulanteDocuBLL bll = tbPostulanteDocuBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbPostulanteDocuBLL bll = tbPostulanteDocuBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbPostulanteDocuBLL bll = tbPostulanteDocuBLL.CreateInstance())
            {
                bll.Delete(this.cnID_PostulanteDocu);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_PostulanteDocu = Convert.ToInt64(dataReader[fields.cnID_PostulanteDocu]);
            _cnID_Postulante = Convert.ToInt64(dataReader[fields.cnID_Postulante]);
            _cnID_Documento = Convert.ToInt32(dataReader[fields.cnID_Documento]);
            if (dataReader.IsDBNull(fields.ctRutaArchivo) == false)
                _ctRutaArchivo = Convert.ToString(dataReader[fields.ctRutaArchivo]);
            else
                _ctRutaArchivo = null;

            //_cfFechaCreacion = Convert.ToDateTime(dataReader[fields.cfFechaCreacion]);
            //if (dataReader.IsDBNull(fields.cfFechaActualizacion) == false)
            //    _cfFechaActualizacion = Convert.ToDateTime(dataReader[fields.cfFechaActualizacion]);
            //else
            //    _cfFechaActualizacion = null;

            //_ctIPAcceso = Convert.ToString(dataReader[fields.ctIPAcceso]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_PostulanteDocu = Convert.ToInt64(dataReader["cnID_PostulanteDocu"]);
            _cnID_Postulante = Convert.ToInt64(dataReader["cnID_Postulante"]);
            _cnID_Documento = Convert.ToInt32(dataReader["cnID_Documento"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctRutaArchivo")) == false)
                _ctRutaArchivo = Convert.ToString(dataReader["ctRutaArchivo"]);
            else
                _ctRutaArchivo = null;

            //_cfFechaCreacion = Convert.ToDateTime(dataReader["cfFechaCreacion"]);
            //if (dataReader.IsDBNull(dataReader.GetOrdinal("cfFechaActualizacion")) == false)
            //    _cfFechaActualizacion = Convert.ToDateTime(dataReader["cfFechaActualizacion"]);
            //else
            //    _cfFechaActualizacion = null;

            //_ctIPAcceso = Convert.ToString(dataReader["ctIPAcceso"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbPostulanteLabo
    /// </summary>
    public partial class tbPostulanteLaboModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbPostulanteLabo
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_PostulanteLabo;
            public Int32 cnID_Postulante;
            public Int32 cnID_Experiencia;
            public Int32 cnTiempo;
            public Int32 cnID_TipoCriterioLabo;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_PostulanteLabo = dataReader.GetOrdinal("cnID_PostulanteLabo");
                cnID_Postulante = dataReader.GetOrdinal("cnID_Postulante");
                cnID_Experiencia = dataReader.GetOrdinal("cnID_Experiencia");
                cnTiempo = dataReader.GetOrdinal("cnTiempoAnios");
                cnID_TipoCriterioLabo = dataReader.GetOrdinal("cnID_TipoCriterioLabo");
            }

            #endregion public methods
        }

        #region fields variables

        private Int64 _cnID_PostulanteLabo;
        private Int64 _cnID_Postulante;
        private Int64 _cnID_Experiencia;
        private Int32 _cnTiempo;
        private Int32 _cnID_TipoCriterioLabo;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_PostulanteLabo
        {
            get { return _cnID_PostulanteLabo; }
            set { _cnID_PostulanteLabo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Postulante
        {
            get { return _cnID_Postulante; }
            set { _cnID_Postulante = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Experiencia
        {
            get { return _cnID_Experiencia; }
            set { _cnID_Experiencia = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnTiempo
        {
            get { return _cnTiempo; }
            set { _cnTiempo = value; }
        }

        public Int32 cnID_TipoCriterioLabo
        {
            get { return _cnID_TipoCriterioLabo; }
            set { _cnID_TipoCriterioLabo = value; }
        }

        #endregion fields property

        #region public methods

        public tbPostulanteLaboModel()
        {
        }

        public tbPostulanteLaboModel(Int64 cnID_PostulanteLabo)
        {
            using (tbPostulanteLaboBLL bll = tbPostulanteLaboBLL.CreateInstance())
            {
                if (!bll.GetBycnID_PostulanteLabo(cnID_PostulanteLabo, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int64 Insert()
        {
            using (tbPostulanteLaboBLL bll = tbPostulanteLaboBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbPostulanteLaboBLL bll = tbPostulanteLaboBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbPostulanteLaboBLL bll = tbPostulanteLaboBLL.CreateInstance())
            {
                bll.Delete(this.cnID_PostulanteLabo);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_PostulanteLabo = Convert.ToInt64(dataReader[fields.cnID_PostulanteLabo]);
            _cnID_Postulante = Convert.ToInt64(dataReader[fields.cnID_Postulante]);
            _cnID_Experiencia = Convert.ToInt64(dataReader[fields.cnID_Experiencia]);
            _cnTiempo = Convert.ToInt32(dataReader[fields.cnTiempo]);

            _cnID_TipoCriterioLabo = Convert.ToInt32(dataReader[fields.cnID_TipoCriterioLabo]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_PostulanteLabo = Convert.ToInt64(dataReader["cnID_PostulanteLabo"]);
            _cnID_Postulante = Convert.ToInt64(dataReader["cnID_Postulante"]);
            _cnID_Experiencia = Convert.ToInt64(dataReader["cnID_Experiencia"]);
            _cnTiempo = Convert.ToInt32(dataReader["cnTiempoAnios"]);

            _cnID_TipoCriterioLabo = Convert.ToInt32(dataReader["cnID_TipoCriterioLabo"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbProceso
    /// </summary>
    public partial class tbProcesoModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbProceso
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_Proceso;
            public Int32 ctTitulo;
            public Int32 ctDescripcion;

            //public Int32 cnID_Tipo;
            // public Int32 cnID_Cobertura;
            public Int32 cfFechaInicio;

            public Int32 cfFechaFin;
            public Int32 cnID_Estado;
            public Int32 ctArchivoTerminos;
            public Int32 ctArchivoProceso;
            public Int32 cnIndEvaluacion;
            public Int32 ctUsuarioCreador;
            public Int32 cfFechaCreacion;
            public Int32 cfFechaActualizacion;
            public Int32 ctIPAcceso;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_Proceso = dataReader.GetOrdinal("cnID_Proceso");
                ctTitulo = dataReader.GetOrdinal("ctTitulo");
                ctDescripcion = dataReader.GetOrdinal("ctDescripcion");
                //cnID_Tipo = dataReader.GetOrdinal("cnID_Tipo");
                ///cnID_Cobertura = dataReader.GetOrdinal("cnID_Cobertura");
                cfFechaInicio = dataReader.GetOrdinal("cfFechaInicio");
                cfFechaFin = dataReader.GetOrdinal("cfFechaFin");
                cnID_Estado = dataReader.GetOrdinal("cnID_Estado");
                ctArchivoTerminos = dataReader.GetOrdinal("ctArchivoTerminos");
                ctArchivoProceso = dataReader.GetOrdinal("ctArchivoProceso");
                cnIndEvaluacion = dataReader.GetOrdinal("cnIndEvaluacion");
                ctUsuarioCreador = dataReader.GetOrdinal("ctUsuarioCreador");
                cfFechaCreacion = dataReader.GetOrdinal("cfFechaCreacion");
                cfFechaActualizacion = dataReader.GetOrdinal("cfFechaActualizacion");
                ctIPAcceso = dataReader.GetOrdinal("ctIPAcceso");
            }

            #endregion public methods
        }

        #region fields variables

        private Int64 _cnID_Proceso;
        private String _ctTitulo;
        private String _ctDescripcion;

        //private Int64 _cnID_Tipo;
        //private Int32 _cnID_Cobertura;
        private DateTime _cfFechaInicio;

        private DateTime _cfFechaFin;
        private Int64 _cnID_Estado;
        private String _ctArchivoTerminos;
        private String _ctArchivoProceso;
        private Boolean _cnIndEvaluacion;
        private String _ctUsuarioCreador;
        private DateTime _cfFechaCreacion;
        private DateTime? _cfFechaActualizacion;
        private String _ctIPAcceso;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Proceso
        {
            get { return _cnID_Proceso; }
            set { _cnID_Proceso = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctTitulo
        {
            get { return _ctTitulo; }
            set { _ctTitulo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctDescripcion
        {
            get { return _ctDescripcion; }
            set { _ctDescripcion = value; }
        }

        ///// <summary>
        /////
        ///// </summary>
        //public Int64 cnID_Tipo
        //{
        //    get { return _cnID_Tipo; }
        //    set { _cnID_Tipo = value; }
        //}

        /// <summary>
        ///
        /// </summary>
        //public Int32 cnID_Cobertura
        //{
        //    get { return _cnID_Cobertura; }
        //    set { _cnID_Cobertura = value; }
        //}

        /// <summary>
        ///
        /// </summary>
        public DateTime cfFechaInicio
        {
            get { return _cfFechaInicio; }
            set { _cfFechaInicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime cfFechaFin
        {
            get { return _cfFechaFin; }
            set { _cfFechaFin = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Estado
        {
            get { return _cnID_Estado; }
            set { _cnID_Estado = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctArchivoTerminos
        {
            get { return _ctArchivoTerminos; }
            set { _ctArchivoTerminos = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctArchivoProceso
        {
            get { return _ctArchivoProceso; }
            set { _ctArchivoProceso = value; }
        }

        public Boolean cnIndEvaluacion { get { return _cnIndEvaluacion; } set { _cnIndEvaluacion = value; } }

        /// <summary>
        ///
        /// </summary>
        public String ctUsuarioCreador
        {
            get { return _ctUsuarioCreador; }
            set { _ctUsuarioCreador = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime cfFechaCreacion
        {
            get { return _cfFechaCreacion; }
            set { _cfFechaCreacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? cfFechaActualizacion
        {
            get { return _cfFechaActualizacion; }
            set { _cfFechaActualizacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctIPAcceso
        {
            get { return _ctIPAcceso; }
            set { _ctIPAcceso = value; }
        }

        #endregion fields property

        #region public methods

        public tbProcesoModel()
        {
        }

        public tbProcesoModel(Int64 cnID_Proceso)
        {
            using (tbProcesoBLL bll = tbProcesoBLL.CreateInstance())
            {
                if (!bll.GetBycnID_Proceso(cnID_Proceso, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int64 Insert()
        {
            using (tbProcesoBLL bll = tbProcesoBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbProcesoBLL bll = tbProcesoBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbProcesoBLL bll = tbProcesoBLL.CreateInstance())
            {
                bll.Delete(this.cnID_Proceso);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_Proceso = Convert.ToInt64(dataReader[fields.cnID_Proceso]);
            _ctTitulo = Convert.ToString(dataReader[fields.ctTitulo]);
            _ctDescripcion = Convert.ToString(dataReader[fields.ctDescripcion]);
            //            _cnID_Tipo = Convert.ToInt64(dataReader[fields.cnID_Tipo]);
            //_cnID_Cobertura = Convert.ToInt32(dataReader[fields.cnID_Cobertura]);
            _cfFechaInicio = Convert.ToDateTime(dataReader[fields.cfFechaInicio]);
            _cfFechaFin = Convert.ToDateTime(dataReader[fields.cfFechaFin]);
            _cnID_Estado = Convert.ToInt64(dataReader[fields.cnID_Estado]);
            if (dataReader.IsDBNull(fields.ctArchivoTerminos) == false)
                _ctArchivoTerminos = Convert.ToString(dataReader[fields.ctArchivoTerminos]);
            else
                _ctArchivoTerminos = null;

            if (dataReader.IsDBNull(fields.ctArchivoProceso) == false)
                _ctArchivoProceso = Convert.ToString(dataReader[fields.ctArchivoProceso]);
            else
                _ctArchivoProceso = null;

            _cnIndEvaluacion = Convert.ToBoolean(dataReader[fields.cnIndEvaluacion]);

            _ctUsuarioCreador = Convert.ToString(dataReader[fields.ctUsuarioCreador]);
            _cfFechaCreacion = Convert.ToDateTime(dataReader[fields.cfFechaCreacion]);
            if (dataReader.IsDBNull(fields.cfFechaActualizacion) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader[fields.cfFechaActualizacion]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader[fields.ctIPAcceso]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_Proceso = Convert.ToInt64(dataReader["cnID_Proceso"]);
            _ctTitulo = Convert.ToString(dataReader["ctTitulo"]);
            _ctDescripcion = Convert.ToString(dataReader["ctDescripcion"]);
            //_cnID_Tipo = Convert.ToInt64(dataReader["cnID_Tipo"]);
            //_cnID_Cobertura = Convert.ToInt32(dataReader["cnID_Cobertura"]);
            _cfFechaInicio = Convert.ToDateTime(dataReader["cfFechaInicio"]);
            _cfFechaFin = Convert.ToDateTime(dataReader["cfFechaFin"]);
            _cnID_Estado = Convert.ToInt64(dataReader["cnID_Estado"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctArchivoTerminos")) == false)
                _ctArchivoTerminos = Convert.ToString(dataReader["ctArchivoTerminos"]);
            else
                _ctArchivoTerminos = null;

            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctArchivoProceso")) == false)
                _ctArchivoProceso = Convert.ToString(dataReader["ctArchivoProceso"]);
            else
                _ctArchivoProceso = null;

            _cnIndEvaluacion = Convert.ToBoolean(dataReader["cnIndEvaluacion"]);

            _ctUsuarioCreador = Convert.ToString(dataReader["ctUsuarioCreador"]);
            _cfFechaCreacion = Convert.ToDateTime(dataReader["cfFechaCreacion"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("cfFechaActualizacion")) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader["cfFechaActualizacion"]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader["ctIPAcceso"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbProcesoCnfgAcad
    /// </summary>
    public partial class tbProcesoCnfgAcadModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbProcesoCnfgAcad
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_CnfgAcademica;
            public Int32 cnID_Proceso;
            public Int32 cnID_Grado;
            public Int32 cnID_Situacion;
            public Int32 cnID_OperadorLogico;
            public Int32 cnGrupoLogico;
            public Int32 cnPuntaje;
            public Int32 ctDescripcion;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_CnfgAcademica = dataReader.GetOrdinal("cnID_CnfgAcademica");
                cnID_Proceso = dataReader.GetOrdinal("cnID_Proceso");
                cnID_Grado = dataReader.GetOrdinal("cnID_Grado");
                cnID_Situacion = dataReader.GetOrdinal("cnID_Situacion");
                cnID_OperadorLogico = dataReader.GetOrdinal("cnID_OperadorLogico");
                cnGrupoLogico = dataReader.GetOrdinal("cnGrupoLogico");
                cnPuntaje = dataReader.GetOrdinal("cnPuntaje");
                ctDescripcion = dataReader.GetOrdinal("ctDescripcion");
            }

            #endregion public methods
        }

        #region fields variables

        private Int32 _cnID_CnfgAcademica;
        private Int64 _cnID_Proceso;
        private Int64 _cnID_Grado;
        private Int64 _cnID_Situacion;
        private Int64? _cnID_OperadorLogico;
        private Int16? _cnGrupoLogico;
        private Int32 _cnPuntaje;
        private string _ctDescripcion;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_CnfgAcademica
        {
            get { return _cnID_CnfgAcademica; }
            set { _cnID_CnfgAcademica = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Proceso
        {
            get { return _cnID_Proceso; }
            set { _cnID_Proceso = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Grado
        {
            get { return _cnID_Grado; }
            set { _cnID_Grado = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Situacion
        {
            get { return _cnID_Situacion; }
            set { _cnID_Situacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64? cnID_OperadorLogico
        {
            get { return _cnID_OperadorLogico; }
            set { _cnID_OperadorLogico = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int16? cnGrupoLogico
        {
            get { return _cnGrupoLogico; }
            set { _cnGrupoLogico = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnPuntaje
        {
            get { return _cnPuntaje; }
            set { _cnPuntaje = value; }
        }

        public string ctDescripcion { get { return _ctDescripcion; } set { _ctDescripcion = value; } }

        #endregion fields property

        #region public methods

        public tbProcesoCnfgAcadModel()
        {
        }

        public tbProcesoCnfgAcadModel(Int32 cnID_CnfgAcademica)
        {
            using (tbProcesoCnfgAcadBLL bll = tbProcesoCnfgAcadBLL.CreateInstance())
            {
                if (!bll.GetBycnID_CnfgAcademica(cnID_CnfgAcademica, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int32 Insert()
        {
            using (tbProcesoCnfgAcadBLL bll = tbProcesoCnfgAcadBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbProcesoCnfgAcadBLL bll = tbProcesoCnfgAcadBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbProcesoCnfgAcadBLL bll = tbProcesoCnfgAcadBLL.CreateInstance())
            {
                bll.Delete(this.cnID_CnfgAcademica);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_CnfgAcademica = Convert.ToInt32(dataReader[fields.cnID_CnfgAcademica]);
            _cnID_Proceso = Convert.ToInt64(dataReader[fields.cnID_Proceso]);
            _cnID_Grado = Convert.ToInt64(dataReader[fields.cnID_Grado]);
            _cnID_Situacion = Convert.ToInt64(dataReader[fields.cnID_Situacion]);
            if (dataReader.IsDBNull(fields.cnID_OperadorLogico) == false)
                _cnID_OperadorLogico = Convert.ToInt64(dataReader[fields.cnID_OperadorLogico]);
            else
                _cnID_OperadorLogico = null;

            if (dataReader.IsDBNull(fields.cnGrupoLogico) == false)
                _cnGrupoLogico = Convert.ToInt16(dataReader[fields.cnGrupoLogico]);
            else
                _cnGrupoLogico = null;

            _cnPuntaje = Convert.ToInt32(dataReader[fields.cnPuntaje]);

            _ctDescripcion = dataReader[fields.ctDescripcion].ToString();

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_CnfgAcademica = Convert.ToInt32(dataReader["cnID_CnfgAcademica"]);
            _cnID_Proceso = Convert.ToInt64(dataReader["cnID_Proceso"]);
            _cnID_Grado = Convert.ToInt64(dataReader["cnID_Grado"]);
            _cnID_Situacion = Convert.ToInt64(dataReader["cnID_Situacion"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("cnID_OperadorLogico")) == false)
                _cnID_OperadorLogico = Convert.ToInt64(dataReader["cnID_OperadorLogico"]);
            else
                _cnID_OperadorLogico = null;

            if (dataReader.IsDBNull(dataReader.GetOrdinal("cnGrupoLogico")) == false)
                _cnGrupoLogico = Convert.ToInt16(dataReader["cnGrupoLogico"]);
            else
                _cnGrupoLogico = null;

            _cnPuntaje = Convert.ToInt32(dataReader["cnPuntaje"]);

            _ctDescripcion = dataReader["ctDescripcion"].ToString();

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbProcesoCnfgLabo
    /// </summary>
    public partial class tbProcesoCnfgLaboModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbProcesoCnfgLabo
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_CnfgLaboral;
            public Int32 cnID_Proceso;
            public Int32 cnID_TipoEntidad;
            public Int32 ctDescripcion;
            public Int32 cnID_TipoCriterioLabo;
            public Int32 cnID_TipoEvaluacion;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_CnfgLaboral = dataReader.GetOrdinal("cnID_CnfgLaboral");
                cnID_Proceso = dataReader.GetOrdinal("cnID_Proceso");
                cnID_TipoEntidad = dataReader.GetOrdinal("cnID_TipoEntidad");
                ctDescripcion = dataReader.GetOrdinal("ctDescripcion");
                cnID_TipoCriterioLabo = dataReader.GetOrdinal("cnID_TipoCriterioLabo");
                cnID_TipoEvaluacion = dataReader.GetOrdinal("cnID_TipoEvaluacion");
            }

            #endregion public methods
        }

        #region fields variables

        private Int32 _cnID_CnfgLaboral;
        private Int64 _cnID_Proceso;
        private Int64 _cnID_TipoEntidad;
        private String _ctDescripcion;
        private Int32 _cnID_TipoCriterioLabo;
        private Int64 _cnID_TipoEvaluacion;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_CnfgLaboral
        {
            get { return _cnID_CnfgLaboral; }
            set { _cnID_CnfgLaboral = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Proceso
        {
            get { return _cnID_Proceso; }
            set { _cnID_Proceso = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_TipoEntidad
        {
            get { return _cnID_TipoEntidad; }
            set { _cnID_TipoEntidad = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctDescripcion
        {
            get { return _ctDescripcion; }
            set { _ctDescripcion = value; }
        }

        public Int32 cnID_TipoCriterioLabo
        {
            get { return _cnID_TipoCriterioLabo; }
            set { _cnID_TipoCriterioLabo = value; }
        }

        public Int64 cnID_TipoEvaluacion
        {
            get { return _cnID_TipoEvaluacion; }
            set { _cnID_TipoEvaluacion = value; }
        }

        #endregion fields property

        #region public methods

        public tbProcesoCnfgLaboModel()
        {
        }

        public tbProcesoCnfgLaboModel(Int32 cnID_CnfgLaboral)
        {
            using (tbProcesoCnfgLaboBLL bll = tbProcesoCnfgLaboBLL.CreateInstance())
            {
                if (!bll.GetBycnID_CnfgLaboral(cnID_CnfgLaboral, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int32 Insert()
        {
            using (tbProcesoCnfgLaboBLL bll = tbProcesoCnfgLaboBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbProcesoCnfgLaboBLL bll = tbProcesoCnfgLaboBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbProcesoCnfgLaboBLL bll = tbProcesoCnfgLaboBLL.CreateInstance())
            {
                bll.Delete(this.cnID_CnfgLaboral);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_CnfgLaboral = Convert.ToInt32(dataReader[fields.cnID_CnfgLaboral]);
            _cnID_Proceso = Convert.ToInt64(dataReader[fields.cnID_Proceso]);
            _cnID_TipoEntidad = Convert.ToInt64(dataReader[fields.cnID_TipoEntidad]);
            _ctDescripcion = Convert.ToString(dataReader[fields.ctDescripcion]);
            _cnID_TipoCriterioLabo = Convert.ToInt32(dataReader[fields.cnID_TipoCriterioLabo]);
            _cnID_TipoEvaluacion = Convert.ToInt32(dataReader[fields.cnID_TipoEvaluacion]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_CnfgLaboral = Convert.ToInt32(dataReader["cnID_CnfgLaboral"]);
            _cnID_Proceso = Convert.ToInt64(dataReader["cnID_Proceso"]);
            _cnID_TipoEntidad = Convert.ToInt64(dataReader["cnID_TipoEntidad"]);
            _ctDescripcion = Convert.ToString(dataReader["ctDescripcion"]);
            _cnID_TipoCriterioLabo = Convert.ToInt32(dataReader["cnID_TipoCriterioLabo"]);
            _cnID_TipoEvaluacion = Convert.ToInt32(dataReader["cnID_TipoEvaluacion"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbProcesoCobertura
    /// </summary>
    public partial class tbProcesoCoberturaModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbProcesoCobertura
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_Cobertura;
            public Int32 cnID_Proceso;
            public Int32 ctNombre;
            //public Int32 ctCobertura;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_Cobertura = dataReader.GetOrdinal("cnID_Cobertura");
                cnID_Proceso = dataReader.GetOrdinal("cnID_Proceso");
                ctNombre = dataReader.GetOrdinal("ctNombre");
                //ctCobertura = dataReader.GetOrdinal("ctCobertura");
            }

            #endregion public methods
        }

        #region fields variables

        private Int32 _cnID_Cobertura;
        private Int64 _cnID_Proceso;
        private String _ctNombre;
        private String _ctCobertura;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_Cobertura
        {
            get { return _cnID_Cobertura; }
            set { _cnID_Cobertura = value; }
        }

        public Int64 cnID_Proceso
        {
            get { return _cnID_Proceso; }
            set { _cnID_Proceso = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctNombre
        {
            get { return _ctNombre; }
            set { _ctNombre = value; }
        }

        ///// <summary>
        /////
        ///// </summary>
        //public String ctCobertura
        //{
        //    get { return _ctCobertura; }
        //    set { _ctCobertura = value; }
        //}

        #endregion fields property

        #region public methods

        public tbProcesoCoberturaModel()
        {
        }

        public tbProcesoCoberturaModel(Int32 cnID_Cobertura)
        {
            using (tbProcesoCoberturaBLL bll = tbProcesoCoberturaBLL.CreateInstance())
            {
                if (!bll.GetBycnID_Cobertura(cnID_Cobertura, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int32 Insert()
        {
            using (tbProcesoCoberturaBLL bll = tbProcesoCoberturaBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbProcesoCoberturaBLL bll = tbProcesoCoberturaBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbProcesoCoberturaBLL bll = tbProcesoCoberturaBLL.CreateInstance())
            {
                bll.Delete(this.cnID_Cobertura);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_Cobertura = Convert.ToInt32(dataReader[fields.cnID_Cobertura]);
            _cnID_Proceso = Convert.ToInt32(dataReader[fields.cnID_Proceso]);

            _ctNombre = Convert.ToString(dataReader[fields.ctNombre]);
            //if (dataReader.IsDBNull(fields.ctCobertura) == false)
            //    _ctCobertura = Convert.ToString(dataReader[fields.ctCobertura]);
            //else
            //    _ctCobertura = null;

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_Cobertura = Convert.ToInt32(dataReader["cnID_Cobertura"]);
            _cnID_Proceso = Convert.ToInt32(dataReader["cnID_Proceso"]);
            _ctNombre = Convert.ToString(dataReader["ctNombre"]);
            //if (dataReader.IsDBNull(dataReader.GetOrdinal("ctCobertura")) == false)
            //    _ctCobertura = Convert.ToString(dataReader["ctCobertura"]);
            //else
            //    _ctCobertura = null;

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbProcesoCriterioAdic
    /// </summary>
    public partial class tbProcesoCriterioAdicModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbProcesoCriterioAdic
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_CriterioAdicional;
            public Int32 cnPuntaje;
            public Int32 ctDescripcion;
            public Int32 cnID_Proceso;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_CriterioAdicional = dataReader.GetOrdinal("cnID_CriterioAdicional");
                cnPuntaje = dataReader.GetOrdinal("cnPuntaje");
                ctDescripcion = dataReader.GetOrdinal("ctDescripcion");
                cnID_Proceso = dataReader.GetOrdinal("cnID_Proceso");
            }

            #endregion public methods
        }

        #region fields variables

        private Int32 _cnID_CriterioAdicional;
        private Int32 _cnPuntaje;
        private String _ctDescripcion;
        private Int64 _cnID_Proceso;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_CriterioAdicional
        {
            get { return _cnID_CriterioAdicional; }
            set { _cnID_CriterioAdicional = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnPuntaje
        {
            get { return _cnPuntaje; }
            set { _cnPuntaje = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctDescripcion
        {
            get { return _ctDescripcion; }
            set { _ctDescripcion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Proceso
        {
            get { return _cnID_Proceso; }
            set { _cnID_Proceso = value; }
        }

        #endregion fields property

        #region public methods

        public tbProcesoCriterioAdicModel()
        {
        }

        public tbProcesoCriterioAdicModel(Int32 cnID_CriterioAdicional)
        {
            using (tbProcesoCriterioAdicBLL bll = tbProcesoCriterioAdicBLL.CreateInstance())
            {
                if (!bll.GetBycnID_CriterioAdicional(cnID_CriterioAdicional, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int32 Insert()
        {
            using (tbProcesoCriterioAdicBLL bll = tbProcesoCriterioAdicBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbProcesoCriterioAdicBLL bll = tbProcesoCriterioAdicBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbProcesoCriterioAdicBLL bll = tbProcesoCriterioAdicBLL.CreateInstance())
            {
                bll.Delete(this.cnID_CriterioAdicional);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_CriterioAdicional = Convert.ToInt32(dataReader[fields.cnID_CriterioAdicional]);
            _cnPuntaje = Convert.ToInt32(dataReader[fields.cnPuntaje]);
            if (dataReader.IsDBNull(fields.ctDescripcion) == false)
                _ctDescripcion = Convert.ToString(dataReader[fields.ctDescripcion]);
            else
                _ctDescripcion = null;

            _cnID_Proceso = Convert.ToInt64(dataReader[fields.cnID_Proceso]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_CriterioAdicional = Convert.ToInt32(dataReader["cnID_CriterioAdicional"]);
            _cnPuntaje = Convert.ToInt32(dataReader["cnPuntaje"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctDescripcion")) == false)
                _ctDescripcion = Convert.ToString(dataReader["ctDescripcion"]);
            else
                _ctDescripcion = null;

            _cnID_Proceso = Convert.ToInt64(dataReader["cnID_Proceso"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbProcesoDocumento
    /// </summary>
    public partial class tbProcesoDocumentoModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbProcesoDocumento
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_Documento;
            public Int32 cnID_Proceso;
            public Int32 ctDescripcion;
            public Int32 cnIndObligatorio;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_Documento = dataReader.GetOrdinal("cnID_Documento");
                cnID_Proceso = dataReader.GetOrdinal("cnID_Proceso");
                ctDescripcion = dataReader.GetOrdinal("ctDescripcion");
                cnIndObligatorio = dataReader.GetOrdinal("cnIndObligatorio");
            }

            #endregion public methods
        }

        #region fields variables

        private Int32 _cnID_Documento;
        private Int64 _cnID_Proceso;
        private String _ctDescripcion;
        private Boolean _cnIndObligatorio;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_Documento
        {
            get { return _cnID_Documento; }
            set { _cnID_Documento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Proceso
        {
            get { return _cnID_Proceso; }
            set { _cnID_Proceso = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctDescripcion
        {
            get { return _ctDescripcion; }
            set { _ctDescripcion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Boolean cnIndObligatorio
        {
            get { return _cnIndObligatorio; }
            set { _cnIndObligatorio = value; }
        }

        #endregion fields property

        #region public methods

        public tbProcesoDocumentoModel()
        {
        }

        public tbProcesoDocumentoModel(Int32 cnID_Documento)
        {
            using (tbProcesoDocumentoBLL bll = tbProcesoDocumentoBLL.CreateInstance())
            {
                if (!bll.GetBycnID_Documento(cnID_Documento, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int32 Insert()
        {
            using (tbProcesoDocumentoBLL bll = tbProcesoDocumentoBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbProcesoDocumentoBLL bll = tbProcesoDocumentoBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbProcesoDocumentoBLL bll = tbProcesoDocumentoBLL.CreateInstance())
            {
                bll.Delete(this.cnID_Documento);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_Documento = Convert.ToInt32(dataReader[fields.cnID_Documento]);
            _cnID_Proceso = Convert.ToInt64(dataReader[fields.cnID_Proceso]);
            _ctDescripcion = Convert.ToString(dataReader[fields.ctDescripcion]);
            _cnIndObligatorio = Convert.ToBoolean(dataReader[fields.cnIndObligatorio]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_Documento = Convert.ToInt32(dataReader["cnID_Documento"]);
            _cnID_Proceso = Convert.ToInt64(dataReader["cnID_Proceso"]);
            _ctDescripcion = Convert.ToString(dataReader["ctDescripcion"]);
            _cnIndObligatorio = Convert.ToBoolean(dataReader["cnIndObligatorio"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbProcesoMatrizLabo
    /// </summary>
    public partial class tbProcesoMatrizLaboModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbProcesoMatrizLabo
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_MatrizLaboral;
            public Int32 cnID_CnfgLaboral;

            //public Int32 cnID_Proceso;
            //public Int32 cnID_TipoEvaluacion;
            public Int32 cnMininmo;

            public Int32 cnMaximo;
            public Int32 cnPuntaje;
            //public Int32 ctDescripcion;
            //public Int32 cnID_TipoCriterioLabo;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_MatrizLaboral = dataReader.GetOrdinal("cnID_MatrizLaboral");
                cnID_CnfgLaboral = dataReader.GetOrdinal("cnID_CnfgLaboral");
                //cnID_Proceso = dataReader.GetOrdinal("cnID_Proceso");
                //cnID_TipoEvaluacion = dataReader.GetOrdinal("cnID_TipoEvaluacion");
                cnMininmo = dataReader.GetOrdinal("cnMininmo");
                cnMaximo = dataReader.GetOrdinal("cnMaximo");
                cnPuntaje = dataReader.GetOrdinal("cnPuntaje");
                //ctDescripcion = dataReader.GetOrdinal("ctDescripcion");
                //cnID_TipoCriterioLabo = dataReader.GetOrdinal("cnID_TipoCriterioLabo");
            }

            #endregion public methods
        }

        #region fields variables

        private Int32 _cnID_MatrizLaboral;
        private Int32 _cnID_CnfgLaboral;
        private Int64 _cnID_Proceso;
        private Int64 _cnID_TipoEvaluacion;
        private Int32 _cnMininmo;
        private Int32 _cnMaximo;
        private Int32 _cnPuntaje;
        private string _ctDescripcion;
        private Int32 _cnID_TipoCriterioLabo;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_MatrizLaboral
        {
            get { return _cnID_MatrizLaboral; }
            set { _cnID_MatrizLaboral = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_CnfgLaboral
        {
            get { return _cnID_CnfgLaboral; }
            set { _cnID_CnfgLaboral = value; }
        }

        ///// <summary>
        /////
        ///// </summary>
        //public Int64 cnID_Proceso
        //{
        //    get { return _cnID_Proceso; }
        //    set { _cnID_Proceso = value; }
        //}

        ///// <summary>
        /////
        ///// </summary>
        //public Int64 cnID_TipoEvaluacion
        //{
        //    get { return _cnID_TipoEvaluacion; }
        //    set { _cnID_TipoEvaluacion = value; }
        //}

        /// <summary>
        ///
        /// </summary>
        public Int32 cnMininmo
        {
            get { return _cnMininmo; }
            set { _cnMininmo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnMaximo
        {
            get { return _cnMaximo; }
            set { _cnMaximo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnPuntaje
        {
            get { return _cnPuntaje; }
            set { _cnPuntaje = value; }
        }

        //public string ctDescripcion
        //{
        //    get { return _ctDescripcion; }
        //    set { _ctDescripcion = value; }
        //}

        //public Int32 cnID_TipoCriterioLabo { get { return _cnID_TipoCriterioLabo; } set { _cnID_TipoCriterioLabo = value; } }

        #endregion fields property

        #region public methods

        public tbProcesoMatrizLaboModel()
        {
        }

        public tbProcesoMatrizLaboModel(Int32 cnID_MatrizLaboral)
        {
            using (tbProcesoMatrizLaboBLL bll = tbProcesoMatrizLaboBLL.CreateInstance())
            {
                if (!bll.GetBycnID_MatrizLaboral(cnID_MatrizLaboral, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int32 Insert()
        {
            using (tbProcesoMatrizLaboBLL bll = tbProcesoMatrizLaboBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbProcesoMatrizLaboBLL bll = tbProcesoMatrizLaboBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbProcesoMatrizLaboBLL bll = tbProcesoMatrizLaboBLL.CreateInstance())
            {
                bll.Delete(this.cnID_MatrizLaboral);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_MatrizLaboral = Convert.ToInt32(dataReader[fields.cnID_MatrizLaboral]);
            _cnID_CnfgLaboral = Convert.ToInt32(dataReader[fields.cnID_CnfgLaboral]);
            //_cnID_Proceso = Convert.ToInt64(dataReader[fields.cnID_Proceso]);
            //_cnID_TipoEvaluacion = Convert.ToInt64(dataReader[fields.cnID_TipoEvaluacion]);
            _cnMininmo = Convert.ToInt32(dataReader[fields.cnMininmo]);
            _cnMaximo = Convert.ToInt32(dataReader[fields.cnMaximo]);
            _cnPuntaje = Convert.ToInt32(dataReader[fields.cnPuntaje]);
            //_ctDescripcion = dataReader[fields.ctDescripcion].ToString();
            //_cnID_TipoCriterioLabo = Convert.ToInt32(dataReader[fields.cnID_TipoCriterioLabo]);
            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_MatrizLaboral = Convert.ToInt32(dataReader["cnID_MatrizLaboral"]);
            _cnID_CnfgLaboral = Convert.ToInt32(dataReader["cnID_CnfgLaboral"]);
            //_cnID_Proceso = Convert.ToInt64(dataReader["cnID_Proceso"]);
            //_cnID_TipoEvaluacion = Convert.ToInt64(dataReader["cnID_TipoEvaluacion"]);
            _cnMininmo = Convert.ToInt32(dataReader["cnMininmo"]);
            _cnMaximo = Convert.ToInt32(dataReader["cnMaximo"]);
            _cnPuntaje = Convert.ToInt32(dataReader["cnPuntaje"]);
            //_ctDescripcion = dataReader["ctDescripcion"].ToString();
            //_cnID_TipoCriterioLabo = Convert.ToInt32(dataReader["cnID_TipoCriterioLabo"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbProvincia
    /// </summary>
    public partial class tbProvinciaModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbProvincia
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_Provincia;
            public Int32 cnID_Departamento;
            public Int32 ctProvincia;
            public Int32 ctCodigoISO;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_Provincia = dataReader.GetOrdinal("cnID_Provincia");
                cnID_Departamento = dataReader.GetOrdinal("cnID_Departamento");
                ctProvincia = dataReader.GetOrdinal("ctProvincia");
                ctCodigoISO = dataReader.GetOrdinal("ctCodigoISO");
            }

            #endregion public methods
        }

        #region fields variables

        private Int32 _cnID_Provincia;
        private Int32 _cnID_Departamento;
        private String _ctProvincia;
        private String _ctCodigoISO;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_Provincia
        {
            get { return _cnID_Provincia; }
            set { _cnID_Provincia = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_Departamento
        {
            get { return _cnID_Departamento; }
            set { _cnID_Departamento = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctProvincia
        {
            get { return _ctProvincia; }
            set { _ctProvincia = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctCodigoISO
        {
            get { return _ctCodigoISO; }
            set { _ctCodigoISO = value; }
        }

        #endregion fields property

        #region public methods

        public tbProvinciaModel()
        {
        }

        public tbProvinciaModel(Int32 cnID_Provincia)
        {
            using (tbProvinciaBLL bll = tbProvinciaBLL.CreateInstance())
            {
                if (!bll.GetBycnID_Provincia(cnID_Provincia, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int32 Insert()
        {
            using (tbProvinciaBLL bll = tbProvinciaBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbProvinciaBLL bll = tbProvinciaBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbProvinciaBLL bll = tbProvinciaBLL.CreateInstance())
            {
                bll.Delete(this.cnID_Provincia);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_Provincia = Convert.ToInt32(dataReader[fields.cnID_Provincia]);
            _cnID_Departamento = Convert.ToInt32(dataReader[fields.cnID_Departamento]);
            if (dataReader.IsDBNull(fields.ctProvincia) == false)
                _ctProvincia = Convert.ToString(dataReader[fields.ctProvincia]);
            else
                _ctProvincia = null;

            if (dataReader.IsDBNull(fields.ctCodigoISO) == false)
                _ctCodigoISO = Convert.ToString(dataReader[fields.ctCodigoISO]);
            else
                _ctCodigoISO = null;

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_Provincia = Convert.ToInt32(dataReader["cnID_Provincia"]);
            _cnID_Departamento = Convert.ToInt32(dataReader["cnID_Departamento"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctProvincia")) == false)
                _ctProvincia = Convert.ToString(dataReader["ctProvincia"]);
            else
                _ctProvincia = null;

            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctCodigoISO")) == false)
                _ctCodigoISO = Convert.ToString(dataReader["ctCodigoISO"]);
            else
                _ctCodigoISO = null;

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbUsuario
    /// </summary>
    public partial class tbUsuarioModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbUsuario
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_Usuario;
            public Int32 cnID_Perfil;
            public Int32 ctNombres;
            public Int32 ctApellidos;
            public Int32 ctLogin;
            public Int32 ctClave;
            public Int32 cnEstadoReg;
            public Int32 cnBloqueado;
            public Int32 cnCodigoRecuperacion;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_Usuario = dataReader.GetOrdinal("cnID_Usuario");
                cnID_Perfil = dataReader.GetOrdinal("cnID_Perfil");
                ctNombres = dataReader.GetOrdinal("ctNombres");
                ctApellidos = dataReader.GetOrdinal("ctApellidos");
                ctLogin = dataReader.GetOrdinal("ctLogin");
                ctClave = dataReader.GetOrdinal("ctClave");
                cnEstadoReg = dataReader.GetOrdinal("cnEstadoReg");
                cnBloqueado = dataReader.GetOrdinal("cnBloqueado");
                cnCodigoRecuperacion = dataReader.GetOrdinal("cnCodigoRecuperacion");
            }

            #endregion public methods
        }

        #region fields variables

        private Int64 _cnID_Usuario;
        private Int32 _cnID_Perfil;
        private String _ctNombres;
        private String _ctApellidos;
        private String _ctLogin;
        private String _ctClave;
        private Boolean _cnEstadoReg;
        private Boolean _cnBloqueado;
        private Guid? _cnCodigoRecuperacion;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Usuario
        {
            get { return _cnID_Usuario; }
            set { _cnID_Usuario = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_Perfil
        {
            get { return _cnID_Perfil; }
            set { _cnID_Perfil = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctNombres
        {
            get { return _ctNombres; }
            set { _ctNombres = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctApellidos
        {
            get { return _ctApellidos; }
            set { _ctApellidos = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctLogin
        {
            get { return _ctLogin; }
            set { _ctLogin = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctClave
        {
            get { return _ctClave; }
            set { _ctClave = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Boolean cnEstadoReg
        {
            get { return _cnEstadoReg; }
            set { _cnEstadoReg = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Boolean cnBloqueado
        {
            get { return _cnBloqueado; }
            set { _cnBloqueado = value; }
        }

        public Guid? cnCodigoRecuperacion
        {
            get { return _cnCodigoRecuperacion; }
            set { _cnCodigoRecuperacion = value; }
        }

        #endregion fields property

        #region public methods

        public tbUsuarioModel()
        {
        }

        public void Insert()
        {
            using (tbUsuarioBLL bll = tbUsuarioBLL.CreateInstance())
            {
                bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbUsuarioBLL bll = tbUsuarioBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbUsuarioBLL bll = tbUsuarioBLL.CreateInstance())
            {
                bll.Delete(this.cnID_Usuario);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_Usuario = Convert.ToInt64(dataReader[fields.cnID_Usuario]);
            _cnID_Perfil = Convert.ToInt32(dataReader[fields.cnID_Perfil]);
            _ctNombres = Convert.ToString(dataReader[fields.ctNombres]);
            _ctApellidos = Convert.ToString(dataReader[fields.ctApellidos]);
            _ctLogin = Convert.ToString(dataReader[fields.ctLogin]);
            _ctClave = Convert.ToString(dataReader[fields.ctClave]);
            _cnEstadoReg = Convert.ToBoolean(dataReader[fields.cnEstadoReg]);
            _cnBloqueado = Convert.ToBoolean(dataReader[fields.cnBloqueado]);
            _cnCodigoRecuperacion = (dataReader[fields.cnCodigoRecuperacion] == DBNull.Value) ? Guid.Empty : Guid.Parse(dataReader[fields.cnCodigoRecuperacion].ToString());

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_Usuario = Convert.ToInt64(dataReader["cnID_Usuario"]);
            _cnID_Perfil = Convert.ToInt32(dataReader["cnID_Perfil"]);
            _ctNombres = Convert.ToString(dataReader["ctNombres"]);
            _ctApellidos = Convert.ToString(dataReader["ctApellidos"]);
            _ctLogin = Convert.ToString(dataReader["ctLogin"]);
            _ctClave = Convert.ToString(dataReader["ctClave"]);
            _cnEstadoReg = Convert.ToBoolean(dataReader["cnEstadoReg"]);
            _cnBloqueado = Convert.ToBoolean(dataReader["cnBloqueado"]);
            //_cnCodigoRecuperacion = (dataReader["cnCodigoRecuperacion"] == DBNull.Value) ? Guid.Empty : Guid.Parse(dataReader["cnCodigoRecuperacion"].ToString());

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbAcademico
    /// </summary>
    public partial class tbAcademicoModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbAcademico
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_Academico;
            public Int32 cnID_Candidato;
            public Int32 cnID_Grado;
            public Int32 cnID_Carrera;
            public Int32 ctCarreraOtro;
            public Int32 cnID_Institucion;
            public Int32 ctInstitucionOtro;
            public Int32 cnID_Situacion;
            public Int32 cnID_Pais;
            public Int32 ctCiudad;
            public Int32 cfFechaInicio;
            public Int32 cfFechaFin;
            public Int32 ctCertificado;
            public Int32 cfFechaCreacion;
            public Int32 cfFechaActualizacion;
            public Int32 ctIPAcceso;
            public Int32 ctGrado;
            public Int32 ctSituacion;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_Academico = dataReader.GetOrdinal("cnID_Academico");
                cnID_Candidato = dataReader.GetOrdinal("cnID_Candidato");
                cnID_Grado = dataReader.GetOrdinal("cnID_Grado");
                cnID_Carrera = dataReader.GetOrdinal("cnID_Carrera");
                ctCarreraOtro = dataReader.GetOrdinal("ctCarreraOtro");
                cnID_Institucion = dataReader.GetOrdinal("cnID_Institucion");
                ctInstitucionOtro = dataReader.GetOrdinal("ctInstitucionOtro");
                cnID_Situacion = dataReader.GetOrdinal("cnID_Situacion");
                cnID_Pais = dataReader.GetOrdinal("cnID_Pais");
                ctCiudad = dataReader.GetOrdinal("ctCiudad");
                cfFechaInicio = dataReader.GetOrdinal("cfFechaInicio");
                cfFechaFin = dataReader.GetOrdinal("cfFechaFin");
                ctCertificado = dataReader.GetOrdinal("ctCertificado");
                cfFechaCreacion = dataReader.GetOrdinal("cfFechaCreacion");
                cfFechaActualizacion = dataReader.GetOrdinal("cfFechaActualizacion");
                ctIPAcceso = dataReader.GetOrdinal("ctIPAcceso");

                ctGrado = dataReader.GetOrdinal("ctGrado");

                ctSituacion = dataReader.GetOrdinal("ctSituacion");


            }

            #endregion public methods
        }

        #region fields variables

        private Int64 _cnID_Academico;
        private Int64 _cnID_Candidato;
        private Int64 _cnID_Grado;
        private Int64 _cnID_Carrera;
        private String _ctCarreraOtro;
        private Int64 _cnID_Institucion;
        private String _ctInstitucionOtro;
        private Int64 _cnID_Situacion;
        private Int32 _cnID_Pais;
        private String _ctCiudad;
        private DateTime _cfFechaInicio;
        private DateTime _cfFechaFin;
        private String _ctCertificado;
        private DateTime _cfFechaCreacion;
        private DateTime? _cfFechaActualizacion;
        private String _ctIPAcceso;

        private String _ctSituacion;
        private String _ctGrado;

        private bool _ctAceptaTerminos;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Academico
        {
            get { return _cnID_Academico; }
            set { _cnID_Academico = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Candidato
        {
            get { return _cnID_Candidato; }
            set { _cnID_Candidato = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Grado
        {
            get { return _cnID_Grado; }
            set { _cnID_Grado = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Carrera
        {
            get { return _cnID_Carrera; }
            set { _cnID_Carrera = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctCarreraOtro
        {
            get { return _ctCarreraOtro; }
            set { _ctCarreraOtro = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Institucion
        {
            get { return _cnID_Institucion; }
            set { _cnID_Institucion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctInstitucionOtro
        {
            get { return _ctInstitucionOtro; }
            set { _ctInstitucionOtro = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Situacion
        {
            get { return _cnID_Situacion; }
            set { _cnID_Situacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_Pais
        {
            get { return _cnID_Pais; }
            set { _cnID_Pais = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctCiudad
        {
            get { return _ctCiudad; }
            set { _ctCiudad = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime cfFechaInicio
        {
            get { return _cfFechaInicio; }
            set { _cfFechaInicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime cfFechaFin
        {
            get { return _cfFechaFin; }
            set { _cfFechaFin = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctCertificado
        {
            get { return _ctCertificado; }
            set { _ctCertificado = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime cfFechaCreacion
        {
            get { return _cfFechaCreacion; }
            set { _cfFechaCreacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? cfFechaActualizacion
        {
            get { return _cfFechaActualizacion; }
            set { _cfFechaActualizacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctIPAcceso
        {
            get { return _ctIPAcceso; }
            set { _ctIPAcceso = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctSituacion
        {
            get { return _ctSituacion; }
            set { _ctSituacion = value; }
        }

        public String ctGrado
        {
            get { return _ctGrado; }
            set { _ctGrado = value; }
        }

        public bool ctAceptaTerminos
        {
            get { return _ctAceptaTerminos; }
            set { _ctAceptaTerminos = value; }
        }

        #endregion fields property

        #region public methods

        public tbAcademicoModel()
        {
        }

        public void Insert()
        {
            using (tbAcademicoBLL bll = tbAcademicoBLL.CreateInstance())
            {
                bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbAcademicoBLL bll = tbAcademicoBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbAcademicoBLL bll = tbAcademicoBLL.CreateInstance())
            {
                bll.Delete(this.cnID_Academico);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_Academico = Convert.ToInt64(dataReader[fields.cnID_Academico]);
            _cnID_Candidato = Convert.ToInt64(dataReader[fields.cnID_Candidato]);
            _cnID_Grado = Convert.ToInt64(dataReader[fields.cnID_Grado]);
            _cnID_Carrera = Convert.ToInt64(dataReader[fields.cnID_Carrera]);

            //_ctAceptaTerminos = Convert.ToString(dataReader["ctAceptaTerminos"]);

            if (dataReader.IsDBNull(fields.ctCarreraOtro) == false)
                _ctCarreraOtro = Convert.ToString(dataReader[fields.ctCarreraOtro]);
            else
                _ctCarreraOtro = null;

            _cnID_Institucion = Convert.ToInt64(dataReader[fields.cnID_Institucion]);
            if (dataReader.IsDBNull(fields.ctInstitucionOtro) == false)
                _ctInstitucionOtro = Convert.ToString(dataReader[fields.ctInstitucionOtro]);
            else
                _ctInstitucionOtro = null;

            _cnID_Situacion = Convert.ToInt64(dataReader[fields.cnID_Situacion]);
            _cnID_Pais = Convert.ToInt32(dataReader[fields.cnID_Pais]);
            if (dataReader.IsDBNull(fields.ctCiudad) == false)
                _ctCiudad = Convert.ToString(dataReader[fields.ctCiudad]);
            else
                _ctCiudad = null;

            _cfFechaInicio = Convert.ToDateTime(dataReader[fields.cfFechaInicio]);
            _cfFechaFin = Convert.ToDateTime(dataReader[fields.cfFechaFin]);
            if (dataReader.IsDBNull(fields.ctCertificado) == false)
                _ctCertificado = Convert.ToString(dataReader[fields.ctCertificado]);
            else
                _ctCertificado = null;

            _cfFechaCreacion = Convert.ToDateTime(dataReader[fields.cfFechaCreacion]);
            if (dataReader.IsDBNull(fields.cfFechaActualizacion) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader[fields.cfFechaActualizacion]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader[fields.ctIPAcceso]);

            if (base.HasColumn(dataReader, "ctSituacion"))
                _ctSituacion = Convert.ToString(dataReader[fields.ctSituacion]);

            if (base.HasColumn(dataReader, "ctGrado"))
                _ctGrado = Convert.ToString(dataReader[fields.ctGrado]);

            if (base.HasColumn(dataReader, "ctCarrera"))
                ctCarrera = Convert.ToString(dataReader["ctCarrera"]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_Academico = Convert.ToInt64(dataReader["cnID_Academico"]);
            _cnID_Candidato = Convert.ToInt64(dataReader["cnID_Candidato"]);
            _cnID_Grado = Convert.ToInt64(dataReader["cnID_Grado"]);
            _cnID_Carrera = Convert.ToInt64(dataReader["cnID_Carrera"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctCarreraOtro")) == false)
                _ctCarreraOtro = Convert.ToString(dataReader["ctCarreraOtro"]);
            else
                _ctCarreraOtro = null;

            _cnID_Institucion = Convert.ToInt64(dataReader["cnID_Institucion"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctInstitucionOtro")) == false)
                _ctInstitucionOtro = Convert.ToString(dataReader["ctInstitucionOtro"]);
            else
                _ctInstitucionOtro = null;

            _cnID_Situacion = Convert.ToInt64(dataReader["cnID_Situacion"]);
            _cnID_Pais = Convert.ToInt32(dataReader["cnID_Pais"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctCiudad")) == false)
                _ctCiudad = Convert.ToString(dataReader["ctCiudad"]);
            else
                _ctCiudad = null;

            _cfFechaInicio = Convert.ToDateTime(dataReader["cfFechaInicio"]);
            _cfFechaFin = Convert.ToDateTime(dataReader["cfFechaFin"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctCertificado")) == false)
                _ctCertificado = Convert.ToString(dataReader["ctCertificado"]);
            else
                _ctCertificado = null;

            _cfFechaCreacion = Convert.ToDateTime(dataReader["cfFechaCreacion"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("cfFechaActualizacion")) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader["cfFechaActualizacion"]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader["ctIPAcceso"]);

            if (base.HasColumn(dataReader, "ctSituacion"))
                _ctSituacion = Convert.ToString(dataReader["ctSituacion"]);

            if (base.HasColumn(dataReader, "ctGrado"))
                _ctGrado = Convert.ToString(dataReader["ctGrado"]);


            if (base.HasColumn(dataReader, "ctCarrera"))
                ctCarrera = Convert.ToString(dataReader["ctCarrera"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods


    }

    /// <summary>
    /// Business Model for tbComplementario
    /// </summary>
    public partial class tbComplementarioModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbComplementario
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_Complementario;
            public Int32 cnID_Candidato;
            public Int32 cnID_TipoEstudio;

            //public Int32 ctTipoEstudioOtro;
            public Int32 cnID_Institucion;

            public Int32 ctInstitucionOtro;
            public Int32 cnID_Situacion;
            public Int32 cnID_Pais;
            public Int32 cfFechaInicio;
            public Int32 cfFechaFin;
            public Int32 ctCertificado;
            public Int32 cfFechaCreacion;
            public Int32 cfFechaActualizacion;
            public Int32 ctIPAcceso;
            public Int32 ctDescripcion;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_Complementario = dataReader.GetOrdinal("cnID_Complementario");
                cnID_Candidato = dataReader.GetOrdinal("cnID_Candidato");
                cnID_TipoEstudio = dataReader.GetOrdinal("cnID_TipoEstudio");
                //ctTipoEstudioOtro = dataReader.GetOrdinal("ctTipoEstudioOtro");
                cnID_Institucion = dataReader.GetOrdinal("cnID_Institucion");
                ctInstitucionOtro = dataReader.GetOrdinal("ctInstitucionOtro");
                cnID_Situacion = dataReader.GetOrdinal("cnID_Situacion");
                cnID_Pais = dataReader.GetOrdinal("cnID_Pais");
                cfFechaInicio = dataReader.GetOrdinal("cfFechaInicio");
                cfFechaFin = dataReader.GetOrdinal("cfFechaFin");
                ctCertificado = dataReader.GetOrdinal("ctCertificado");
                cfFechaCreacion = dataReader.GetOrdinal("cfFechaCreacion");
                cfFechaActualizacion = dataReader.GetOrdinal("cfFechaActualizacion");
                ctIPAcceso = dataReader.GetOrdinal("ctIPAcceso");
                ctDescripcion = dataReader.GetOrdinal("ctDescripcion");
            }

            #endregion public methods
        }

        #region fields variables

        private Int64 _cnID_Complementario;
        private Int64 _cnID_Candidato;
        private Int64 _cnID_TipoEstudio;

        //private String _ctTipoEstudioOtro;
        private Int64? _cnID_Institucion;

        private String _ctInstitucionOtro;
        private Int64? _cnID_Situacion;
        private Int32 _cnID_Pais;
        private DateTime _cfFechaInicio;
        private DateTime _cfFechaFin;
        private String _ctCertificado;
        private DateTime _cfFechaCreacion;
        private DateTime? _cfFechaActualizacion;
        private String _ctIPAcceso;
        private string _ctDescripcion;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Complementario
        {
            get { return _cnID_Complementario; }
            set { _cnID_Complementario = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Candidato
        {
            get { return _cnID_Candidato; }
            set { _cnID_Candidato = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_TipoEstudio
        {
            get { return _cnID_TipoEstudio; }
            set { _cnID_TipoEstudio = value; }
        }

        ///// <summary>
        /////
        ///// </summary>
        //public String ctTipoEstudioOtro
        //{
        //    get { return _ctTipoEstudioOtro; }
        //    set { _ctTipoEstudioOtro = value; }
        //}

        /// <summary>
        ///
        /// </summary>
        public Int64? cnID_Institucion
        {
            get { return _cnID_Institucion; }
            set { _cnID_Institucion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctInstitucionOtro
        {
            get { return _ctInstitucionOtro; }
            set { _ctInstitucionOtro = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64? cnID_Situacion
        {
            get { return _cnID_Situacion; }
            set { _cnID_Situacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_Pais
        {
            get { return _cnID_Pais; }
            set { _cnID_Pais = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime cfFechaInicio
        {
            get { return _cfFechaInicio; }
            set { _cfFechaInicio = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime cfFechaFin
        {
            get { return _cfFechaFin; }
            set { _cfFechaFin = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctCertificado
        {
            get { return _ctCertificado; }
            set { _ctCertificado = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime cfFechaCreacion
        {
            get { return _cfFechaCreacion; }
            set { _cfFechaCreacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? cfFechaActualizacion
        {
            get { return _cfFechaActualizacion; }
            set { _cfFechaActualizacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctIPAcceso
        {
            get { return _ctIPAcceso; }
            set { _ctIPAcceso = value; }
        }

        public string ctDescripcion
        {
            get { return _ctDescripcion; }
            set { _ctDescripcion = value; }
        }

        #endregion fields property

        #region public methods

        public tbComplementarioModel()
        {
        }

        public tbComplementarioModel(Int64 cnID_Complementario)
        {
            using (tbComplementarioBLL bll = tbComplementarioBLL.CreateInstance())
            {
                if (!bll.GetBycnID_Complementario(cnID_Complementario, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int64 Insert()
        {
            using (tbComplementarioBLL bll = tbComplementarioBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbComplementarioBLL bll = tbComplementarioBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbComplementarioBLL bll = tbComplementarioBLL.CreateInstance())
            {
                bll.Delete(this.cnID_Complementario);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_Complementario = Convert.ToInt64(dataReader[fields.cnID_Complementario]);
            _cnID_Candidato = Convert.ToInt64(dataReader[fields.cnID_Candidato]);
            _cnID_TipoEstudio = Convert.ToInt64(dataReader[fields.cnID_TipoEstudio]);
            //if (dataReader.IsDBNull(fields.ctTipoEstudioOtro) == false)
            //    _ctTipoEstudioOtro = Convert.ToString(dataReader[fields.ctTipoEstudioOtro]);
            //else
            //    _ctTipoEstudioOtro = null;

            if (dataReader.IsDBNull(fields.cnID_Institucion) == false)
                _cnID_Institucion = Convert.ToInt64(dataReader[fields.cnID_Institucion]);
            else
                _cnID_Institucion = null;

            if (dataReader.IsDBNull(fields.ctInstitucionOtro) == false)
                _ctInstitucionOtro = Convert.ToString(dataReader[fields.ctInstitucionOtro]);
            else
                _ctInstitucionOtro = null;

            if (dataReader.IsDBNull(fields.cnID_Situacion) == false)
                _cnID_Situacion = Convert.ToInt64(dataReader[fields.cnID_Situacion]);
            else
                _cnID_Situacion = null;

            _cnID_Pais = Convert.ToInt32(dataReader[fields.cnID_Pais]);
            _cfFechaInicio = Convert.ToDateTime(dataReader[fields.cfFechaInicio]);
            _cfFechaFin = Convert.ToDateTime(dataReader[fields.cfFechaFin]);
            if (dataReader.IsDBNull(fields.ctCertificado) == false)
                _ctCertificado = Convert.ToString(dataReader[fields.ctCertificado]);
            else
                _ctCertificado = null;

            _cfFechaCreacion = Convert.ToDateTime(dataReader[fields.cfFechaCreacion]);
            if (dataReader.IsDBNull(fields.cfFechaActualizacion) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader[fields.cfFechaActualizacion]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader[fields.ctIPAcceso]);

            _ctDescripcion = Convert.ToString(dataReader[fields.ctDescripcion]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_Complementario = Convert.ToInt64(dataReader["cnID_Complementario"]);
            _cnID_Candidato = Convert.ToInt64(dataReader["cnID_Candidato"]);
            _cnID_TipoEstudio = Convert.ToInt64(dataReader["cnID_TipoEstudio"]);
            //if (dataReader.IsDBNull(dataReader.GetOrdinal("ctTipoEstudioOtro")) == false)
            //    _ctTipoEstudioOtro = Convert.ToString(dataReader["ctTipoEstudioOtro"]);
            //else
            //    _ctTipoEstudioOtro = null;

            if (dataReader.IsDBNull(dataReader.GetOrdinal("cnID_Institucion")) == false)
                _cnID_Institucion = Convert.ToInt64(dataReader["cnID_Institucion"]);
            else
                _cnID_Institucion = null;

            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctInstitucionOtro")) == false)
                _ctInstitucionOtro = Convert.ToString(dataReader["ctInstitucionOtro"]);
            else
                _ctInstitucionOtro = null;

            if (dataReader.IsDBNull(dataReader.GetOrdinal("cnID_Situacion")) == false)
                _cnID_Situacion = Convert.ToInt64(dataReader["cnID_Situacion"]);
            else
                _cnID_Situacion = null;

            _cnID_Pais = Convert.ToInt32(dataReader["cnID_Pais"]);
            _cfFechaInicio = Convert.ToDateTime(dataReader["cfFechaInicio"]);
            _cfFechaFin = Convert.ToDateTime(dataReader["cfFechaFin"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctCertificado")) == false)
                _ctCertificado = Convert.ToString(dataReader["ctCertificado"]);
            else
                _ctCertificado = null;

            _cfFechaCreacion = Convert.ToDateTime(dataReader["cfFechaCreacion"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("cfFechaActualizacion")) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader["cfFechaActualizacion"]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader["ctIPAcceso"]);

            _ctDescripcion = Convert.ToString(dataReader["ctDescripcion"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbReferencia
    /// </summary>
    public partial class tbReferenciaModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbReferencia
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_Referencia;
            public Int32 cnID_Candidato;
            public Int32 ctNombreCompleto;
            public Int32 ctNombreEmpresa;
            public Int32 ctCargo;
            public Int32 ctTelefono;
            public Int32 cfFechaCreacion;
            public Int32 cfFechaActualizacion;
            public Int32 ctIPAcceso;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_Referencia = dataReader.GetOrdinal("cnID_Referencia");
                cnID_Candidato = dataReader.GetOrdinal("cnID_Candidato");
                ctNombreCompleto = dataReader.GetOrdinal("ctNombreCompleto");
                ctNombreEmpresa = dataReader.GetOrdinal("ctNombreEmpresa");
                ctCargo = dataReader.GetOrdinal("ctCargo");
                ctTelefono = dataReader.GetOrdinal("ctTelefono");
                cfFechaCreacion = dataReader.GetOrdinal("cfFechaCreacion");
                cfFechaActualizacion = dataReader.GetOrdinal("cfFechaActualizacion");
                ctIPAcceso = dataReader.GetOrdinal("ctIPAcceso");
            }

            #endregion public methods
        }

        #region fields variables

        private Int64 _cnID_Referencia;
        private Int64 _cnID_Candidato;
        private String _ctNombreCompleto;
        private String _ctNombreEmpresa;
        private String _ctCargo;
        private String _ctTelefono;
        private DateTime _cfFechaCreacion;
        private DateTime? _cfFechaActualizacion;
        private String _ctIPAcceso;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Referencia
        {
            get { return _cnID_Referencia; }
            set { _cnID_Referencia = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Candidato
        {
            get { return _cnID_Candidato; }
            set { _cnID_Candidato = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctNombreCompleto
        {
            get { return _ctNombreCompleto; }
            set { _ctNombreCompleto = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctNombreEmpresa
        {
            get { return _ctNombreEmpresa; }
            set { _ctNombreEmpresa = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctCargo
        {
            get { return _ctCargo; }
            set { _ctCargo = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctTelefono
        {
            get { return _ctTelefono; }
            set { _ctTelefono = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime cfFechaCreacion
        {
            get { return _cfFechaCreacion; }
            set { _cfFechaCreacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public DateTime? cfFechaActualizacion
        {
            get { return _cfFechaActualizacion; }
            set { _cfFechaActualizacion = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctIPAcceso
        {
            get { return _ctIPAcceso; }
            set { _ctIPAcceso = value; }
        }

        #endregion fields property

        #region public methods

        public tbReferenciaModel()
        {
        }

        public tbReferenciaModel(Int64 cnID_Referencia)
        {
            using (tbReferenciaBLL bll = tbReferenciaBLL.CreateInstance())
            {
                if (!bll.GetBycnID_Referencia(cnID_Referencia, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int64 Insert()
        {
            using (tbReferenciaBLL bll = tbReferenciaBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbReferenciaBLL bll = tbReferenciaBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbReferenciaBLL bll = tbReferenciaBLL.CreateInstance())
            {
                bll.Delete(this.cnID_Referencia);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_Referencia = Convert.ToInt64(dataReader[fields.cnID_Referencia]);
            _cnID_Candidato = Convert.ToInt64(dataReader[fields.cnID_Candidato]);
            _ctNombreCompleto = Convert.ToString(dataReader[fields.ctNombreCompleto]);
            _ctNombreEmpresa = Convert.ToString(dataReader[fields.ctNombreEmpresa]);
            _ctCargo = Convert.ToString(dataReader[fields.ctCargo]);
            _ctTelefono = Convert.ToString(dataReader[fields.ctTelefono]);
            _cfFechaCreacion = Convert.ToDateTime(dataReader[fields.cfFechaCreacion]);
            if (dataReader.IsDBNull(fields.cfFechaActualizacion) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader[fields.cfFechaActualizacion]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader[fields.ctIPAcceso]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_Referencia = Convert.ToInt64(dataReader["cnID_Referencia"]);
            _cnID_Candidato = Convert.ToInt64(dataReader["cnID_Candidato"]);
            _ctNombreCompleto = Convert.ToString(dataReader["ctNombreCompleto"]);
            _ctNombreEmpresa = Convert.ToString(dataReader["ctNombreEmpresa"]);
            _ctCargo = Convert.ToString(dataReader["ctCargo"]);
            _ctTelefono = Convert.ToString(dataReader["ctTelefono"]);
            _cfFechaCreacion = Convert.ToDateTime(dataReader["cfFechaCreacion"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("cfFechaActualizacion")) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader["cfFechaActualizacion"]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader["ctIPAcceso"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbPostulanteCrit
    /// </summary>
    public partial class tbPostulanteCritModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbPostulanteCrit
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_PostulanteCrit;
            public Int32 cnID_Postulante;
            public Int32 cnID_CriterioAdicional;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_PostulanteCrit = dataReader.GetOrdinal("cnID_PostulanteCrit");
                cnID_Postulante = dataReader.GetOrdinal("cnID_Postulante");
                cnID_CriterioAdicional = dataReader.GetOrdinal("cnID_CriterioAdicional");
            }

            #endregion public methods
        }

        #region fields variables

        private Int64 _cnID_PostulanteCrit;
        private Int64? _cnID_Postulante;
        private Int32 _cnID_CriterioAdicional;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_PostulanteCrit
        {
            get { return _cnID_PostulanteCrit; }
            set { _cnID_PostulanteCrit = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64? cnID_Postulante
        {
            get { return _cnID_Postulante; }
            set { _cnID_Postulante = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int32 cnID_CriterioAdicional
        {
            get { return _cnID_CriterioAdicional; }
            set { _cnID_CriterioAdicional = value; }
        }

        #endregion fields property

        #region public methods

        public tbPostulanteCritModel()
        {
        }

        public tbPostulanteCritModel(Int64 cnID_PostulanteCrit)
        {
            using (tbPostulanteCritBLL bll = tbPostulanteCritBLL.CreateInstance())
            {
                if (!bll.GetBycnID_PostulanteCrit(cnID_PostulanteCrit, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int64 Insert()
        {
            using (tbPostulanteCritBLL bll = tbPostulanteCritBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbPostulanteCritBLL bll = tbPostulanteCritBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbPostulanteCritBLL bll = tbPostulanteCritBLL.CreateInstance())
            {
                bll.Delete(this.cnID_PostulanteCrit);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_PostulanteCrit = Convert.ToInt64(dataReader[fields.cnID_PostulanteCrit]);
            if (dataReader.IsDBNull(fields.cnID_Postulante) == false)
                _cnID_Postulante = Convert.ToInt64(dataReader[fields.cnID_Postulante]);
            else
                _cnID_Postulante = null;

            _cnID_CriterioAdicional = Convert.ToInt32(dataReader[fields.cnID_CriterioAdicional]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_PostulanteCrit = Convert.ToInt64(dataReader["cnID_PostulanteCrit"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("cnID_Postulante")) == false)
                _cnID_Postulante = Convert.ToInt64(dataReader["cnID_Postulante"]);
            else
                _cnID_Postulante = null;

            _cnID_CriterioAdicional = Convert.ToInt32(dataReader["cnID_CriterioAdicional"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbCoberturaZona
    /// </summary>
    public partial class tbCoberturaZonaModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbCoberturaZona
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_CoberturaZona;
            public Int32 cnID_Cobertura;
            public Int32 ctZona;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_CoberturaZona = dataReader.GetOrdinal("cnID_CoberturaZona");
                cnID_Cobertura = dataReader.GetOrdinal("cnID_Cobertura");
                ctZona = dataReader.GetOrdinal("ctZona");
            }

            #endregion public methods
        }

        #region fields variables

        private Int64 _cnID_CoberturaZona;
        private Int64 _cnID_Cobertura;
        private String _ctZona;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_CoberturaZona
        {
            get { return _cnID_CoberturaZona; }
            set { _cnID_CoberturaZona = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Cobertura
        {
            get { return _cnID_Cobertura; }
            set { _cnID_Cobertura = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public String ctZona
        {
            get { return _ctZona; }
            set { _ctZona = value; }
        }

        #endregion fields property

        #region public methods

        public tbCoberturaZonaModel()
        {
        }

        public tbCoberturaZonaModel(Int64 cnID_CoberturaZona)
        {
            using (tbCoberturaZonaBLL bll = tbCoberturaZonaBLL.CreateInstance())
            {
                if (!bll.GetBycnID_CoberturaZona(cnID_CoberturaZona, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int64 Insert()
        {
            using (tbCoberturaZonaBLL bll = tbCoberturaZonaBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbCoberturaZonaBLL bll = tbCoberturaZonaBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbCoberturaZonaBLL bll = tbCoberturaZonaBLL.CreateInstance())
            {
                bll.Delete(this.cnID_CoberturaZona);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_CoberturaZona = Convert.ToInt64(dataReader[fields.cnID_CoberturaZona]);
            _cnID_Cobertura = Convert.ToInt64(dataReader[fields.cnID_Cobertura]);
            _ctZona = Convert.ToString(dataReader[fields.ctZona]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_CoberturaZona = Convert.ToInt64(dataReader["cnID_CoberturaZona"]);
            _cnID_Cobertura = Convert.ToInt64(dataReader["cnID_Cobertura"]);
            _ctZona = Convert.ToString(dataReader["ctZona"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbPostulanteAcad
    /// </summary>
    public partial class tbPostulanteAcadModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbPostulanteAcad
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_PostulanteAcad;
            public Int32 cnID_Postulante;
            public Int32 cnID_Academico;
            public Int32 cnID_Grado;
            public Int32 cnID_Situacion;

            #endregion fields ordinal cache

            #region public methods

            public FieldsOrdinal(IDataReader dataReader)
            {
                cnID_PostulanteAcad = dataReader.GetOrdinal("cnID_PostulanteAcad");
                cnID_Postulante = dataReader.GetOrdinal("cnID_Postulante");
                cnID_Academico = dataReader.GetOrdinal("cnID_Academico");
                cnID_Grado = dataReader.GetOrdinal("cnID_Grado");
                cnID_Situacion = dataReader.GetOrdinal("cnID_Situacion");
            }

            #endregion public methods
        }

        #region fields variables

        private Int64 _cnID_PostulanteAcad;
        private Int64 _cnID_Postulante;
        private Int64 _cnID_Academico;
        private Int64 _cnID_Grado;
        private Int64 _cnID_Situacion;

        #endregion fields variables

        #region fields property

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_PostulanteAcad
        {
            get { return _cnID_PostulanteAcad; }
            set { _cnID_PostulanteAcad = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Postulante
        {
            get { return _cnID_Postulante; }
            set { _cnID_Postulante = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Academico
        {
            get { return _cnID_Academico; }
            set { _cnID_Academico = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Grado
        {
            get { return _cnID_Grado; }
            set { _cnID_Grado = value; }
        }

        /// <summary>
        ///
        /// </summary>
        public Int64 cnID_Situacion
        {
            get { return _cnID_Situacion; }
            set { _cnID_Situacion = value; }
        }

        #endregion fields property

        #region public methods

        public tbPostulanteAcadModel()
        {
        }

        public tbPostulanteAcadModel(Int64 cnID_PostulanteAcad)
        {
            using (tbPostulanteAcadBLL bll = tbPostulanteAcadBLL.CreateInstance())
            {
                if (!bll.GetBycnID_PostulanteAcad(cnID_PostulanteAcad, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int64 Insert()
        {
            using (tbPostulanteAcadBLL bll = tbPostulanteAcadBLL.CreateInstance())
            {
                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbPostulanteAcadBLL bll = tbPostulanteAcadBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbPostulanteAcadBLL bll = tbPostulanteAcadBLL.CreateInstance())
            {
                bll.Delete(this.cnID_PostulanteAcad);
            }
        }

        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {
            _cnID_PostulanteAcad = Convert.ToInt64(dataReader[fields.cnID_PostulanteAcad]);
            _cnID_Postulante = Convert.ToInt64(dataReader[fields.cnID_Postulante]);
            _cnID_Academico = Convert.ToInt64(dataReader[fields.cnID_Academico]);
            _cnID_Grado = Convert.ToInt64(dataReader[fields.cnID_Grado]);
            _cnID_Situacion = Convert.ToInt64(dataReader[fields.cnID_Situacion]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {
            _cnID_PostulanteAcad = Convert.ToInt64(dataReader["cnID_PostulanteAcad"]);
            _cnID_Postulante = Convert.ToInt64(dataReader["cnID_Postulante"]);
            _cnID_Academico = Convert.ToInt64(dataReader["cnID_Academico"]);
            _cnID_Grado = Convert.ToInt64(dataReader["cnID_Grado"]);
            _cnID_Situacion = Convert.ToInt64(dataReader["cnID_Situacion"]);

            // Model is ready
            SetDataLoaded(true);
        }

        #endregion public methods
    }

    /// <summary>
    /// Business Model for tbPostulanteEvalManual
    /// </summary>
    public partial class tbPostulanteEvalManualModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbPostulanteEvalManual
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnPostulanteEvalManual;
            public Int32 cnID_Postulante;
            public Int32 cnID_TipoEval;
            public Int32 cnEvaluacion;
            public Int32 ctObservacion;
            public Int32 ctUsuarioEval;
            public Int32 cfFechaEval;
            #endregion

            #region public methods
            public FieldsOrdinal(IDataReader dataReader)
            {

                cnPostulanteEvalManual = dataReader.GetOrdinal("cnPostulanteEvalManual");
                cnID_Postulante = dataReader.GetOrdinal("cnID_Postulante");
                cnID_TipoEval = dataReader.GetOrdinal("cnID_TipoEval");
                cnEvaluacion = dataReader.GetOrdinal("cnEvaluacion");
                ctObservacion = dataReader.GetOrdinal("ctObservacion");
                ctUsuarioEval = dataReader.GetOrdinal("ctUsuarioEval");
                cfFechaEval = dataReader.GetOrdinal("cfFechaEval");
            }
            #endregion
        }

        #region fields variables

        private Int64 _cnPostulanteEvalManual;
        private Int64 _cnID_Postulante;
        private Int64 _cnID_TipoEval;
        private Boolean _cnEvaluacion;
        private String _ctObservacion;
        private String _ctUsuarioEval;
        private DateTime? _cfFechaEval;
        #endregion

        #region fields property

        /// <summary>
        /// 
        /// </summary>
        public Int64 cnPostulanteEvalManual
        {
            get { return _cnPostulanteEvalManual; }
            set { _cnPostulanteEvalManual = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int64 cnID_Postulante
        {
            get { return _cnID_Postulante; }
            set { _cnID_Postulante = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int64 cnID_TipoEval
        {
            get { return _cnID_TipoEval; }
            set { _cnID_TipoEval = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Boolean cnEvaluacion
        {
            get { return _cnEvaluacion; }
            set { _cnEvaluacion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String ctObservacion
        {
            get { return _ctObservacion; }
            set { _ctObservacion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String ctUsuarioEval
        {
            get { return _ctUsuarioEval; }
            set { _ctUsuarioEval = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? cfFechaEval
        {
            get { return _cfFechaEval; }
            set { _cfFechaEval = value; }
        }
        #endregion

        #region public methods
        public tbPostulanteEvalManualModel()
        {
        }


        public tbPostulanteEvalManualModel(Int64 cnPostulanteEvalManual)
        {
            using (tbPostulanteEvalManualBLL bll = tbPostulanteEvalManualBLL.CreateInstance())
            {
                if (!bll.GetBycnPostulanteEvalManual(cnPostulanteEvalManual, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int64 Insert()
        {
            using (tbPostulanteEvalManualBLL bll = tbPostulanteEvalManualBLL.CreateInstance())
            {

                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbPostulanteEvalManualBLL bll = tbPostulanteEvalManualBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbPostulanteEvalManualBLL bll = tbPostulanteEvalManualBLL.CreateInstance())
            {
                bll.Delete(this.cnPostulanteEvalManual);
            }
        }


        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {

            _cnPostulanteEvalManual = Convert.ToInt64(dataReader[fields.cnPostulanteEvalManual]);
            _cnID_Postulante = Convert.ToInt64(dataReader[fields.cnID_Postulante]);
            _cnID_TipoEval = Convert.ToInt64(dataReader[fields.cnID_TipoEval]);
            _cnEvaluacion = Convert.ToBoolean(dataReader[fields.cnEvaluacion]);
            if (dataReader.IsDBNull(fields.ctObservacion) == false)
                _ctObservacion = Convert.ToString(dataReader[fields.ctObservacion]);
            else
                _ctObservacion = null;

            _ctUsuarioEval = Convert.ToString(dataReader[fields.ctUsuarioEval]);
            if (dataReader.IsDBNull(fields.cfFechaEval) == false)
                _cfFechaEval = Convert.ToDateTime(dataReader[fields.cfFechaEval]);
            else
                _cfFechaEval = null;


            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {

            _cnPostulanteEvalManual = Convert.ToInt64(dataReader["cnPostulanteEvalManual"]);
            _cnID_Postulante = Convert.ToInt64(dataReader["cnID_Postulante"]);
            _cnID_TipoEval = Convert.ToInt64(dataReader["cnID_TipoEval"]);
            _cnEvaluacion = Convert.ToBoolean(dataReader["cnEvaluacion"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctObservacion")) == false)
                _ctObservacion = Convert.ToString(dataReader["ctObservacion"]);
            else
                _ctObservacion = null;

            _ctUsuarioEval = Convert.ToString(dataReader["ctUsuarioEval"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("cfFechaEval")) == false)
                _cfFechaEval = Convert.ToDateTime(dataReader["cfFechaEval"]);
            else
                _cfFechaEval = null;


            // Model is ready
            SetDataLoaded(true);
        }
        #endregion
    }

    /// <summary>
    /// Business Model for tbExperienciaDocencia
    /// </summary>
    public partial class tbExperienciaDocenciaModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbExperienciaDocencia
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_ExperienciaDocencia;
            public Int32 cnID_Candidato;
            public Int32 cnID_TipoInstitucion;
            public Int32 ctInstitucion;
            public Int32 cnDocenteInvestigador;
            public Int32 cnID_CategoriaDocente;
            public Int32 cnID_RegimenDedicacion;
            public Int32 cnRegistradoDINA;
            public Int32 ctNumeroRegistroDINA;
            public Int32 cfFechaInicio;
            public Int32 cfFechaCreacion;
            public Int32 cfFechaActualizacion;
            public Int32 ctIPAcceso;
            #endregion

            #region public methods
            public FieldsOrdinal(IDataReader dataReader)
            {

                cnID_ExperienciaDocencia = dataReader.GetOrdinal("cnID_ExperienciaDocencia");
                cnID_Candidato = dataReader.GetOrdinal("cnID_Candidato");
                cnID_TipoInstitucion = dataReader.GetOrdinal("cnID_TipoInstitucion");
                ctInstitucion = dataReader.GetOrdinal("ctInstitucion");
                cnDocenteInvestigador = dataReader.GetOrdinal("cnDocenteInvestigador");
                cnID_CategoriaDocente = dataReader.GetOrdinal("cnID_CategoriaDocente");
                cnID_RegimenDedicacion = dataReader.GetOrdinal("cnID_RegimenDedicacion");
                cnRegistradoDINA = dataReader.GetOrdinal("cnRegistradoDINA");
                ctNumeroRegistroDINA = dataReader.GetOrdinal("ctNumeroRegistroDINA");
                cfFechaInicio = dataReader.GetOrdinal("cfFechaInicio");
                cfFechaCreacion = dataReader.GetOrdinal("cfFechaCreacion");
                cfFechaActualizacion = dataReader.GetOrdinal("cfFechaActualizacion");
                ctIPAcceso = dataReader.GetOrdinal("ctIPAcceso");
            }
            #endregion
        }

        #region fields variables

        private Int64 _cnID_ExperienciaDocencia;
        private Int64 _cnID_Candidato;
        private Int64 _cnID_TipoInstitucion;
        private String _ctInstitucion;
        private Boolean _cnDocenteInvestigador;
        private Int64 _cnID_CategoriaDocente;
        private Int64 _cnID_RegimenDedicacion;
        private Boolean _cnRegistradoDINA;
        private String _ctNumeroRegistroDINA;
        private DateTime _cfFechaInicio;
        private DateTime _cfFechaCreacion;
        private DateTime? _cfFechaActualizacion;
        private String _ctIPAcceso;

        private bool _ctAceptaTerminos;



        #endregion

        #region fields property

        /// <summary>
        /// 
        /// </summary>
        public Int64 cnID_ExperienciaDocencia
        {
            get { return _cnID_ExperienciaDocencia; }
            set { _cnID_ExperienciaDocencia = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int64 cnID_Candidato
        {
            get { return _cnID_Candidato; }
            set { _cnID_Candidato = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int64 cnID_TipoInstitucion
        {
            get { return _cnID_TipoInstitucion; }
            set { _cnID_TipoInstitucion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String ctInstitucion
        {
            get { return _ctInstitucion; }
            set { _ctInstitucion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Boolean cnDocenteInvestigador
        {
            get { return _cnDocenteInvestigador; }
            set { _cnDocenteInvestigador = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int64 cnID_CategoriaDocente
        {
            get { return _cnID_CategoriaDocente; }
            set { _cnID_CategoriaDocente = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int64 cnID_RegimenDedicacion
        {
            get { return _cnID_RegimenDedicacion; }
            set { _cnID_RegimenDedicacion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Boolean cnRegistradoDINA
        {
            get { return _cnRegistradoDINA; }
            set { _cnRegistradoDINA = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String ctNumeroRegistroDINA
        {
            get { return _ctNumeroRegistroDINA; }
            set { _ctNumeroRegistroDINA = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime cfFechaInicio
        {
            get { return _cfFechaInicio; }
            set { _cfFechaInicio = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime cfFechaCreacion
        {
            get { return _cfFechaCreacion; }
            set { _cfFechaCreacion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? cfFechaActualizacion
        {
            get { return _cfFechaActualizacion; }
            set { _cfFechaActualizacion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String ctIPAcceso
        {
            get { return _ctIPAcceso; }
            set { _ctIPAcceso = value; }
        }

        public bool ctAceptaTerminos
        {
            get { return _ctAceptaTerminos; }
            set { _ctAceptaTerminos = value; }
        }

        #endregion

        #region public methods
        public tbExperienciaDocenciaModel()
        {
        }


        public tbExperienciaDocenciaModel(Int64 cnID_ExperienciaDocencia)
        {
            using (tbExperienciaDocenciaBLL bll = tbExperienciaDocenciaBLL.CreateInstance())
            {
                if (!bll.GetBycnID_ExperienciaDocencia(cnID_ExperienciaDocencia, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int64 Insert()
        {
            using (tbExperienciaDocenciaBLL bll = tbExperienciaDocenciaBLL.CreateInstance())
            {

                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbExperienciaDocenciaBLL bll = tbExperienciaDocenciaBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbExperienciaDocenciaBLL bll = tbExperienciaDocenciaBLL.CreateInstance())
            {
                bll.Delete(this.cnID_ExperienciaDocencia);
            }
        }


        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {

            _cnID_ExperienciaDocencia = Convert.ToInt64(dataReader[fields.cnID_ExperienciaDocencia]);
            _cnID_Candidato = Convert.ToInt64(dataReader[fields.cnID_Candidato]);
            _cnID_TipoInstitucion = Convert.ToInt64(dataReader[fields.cnID_TipoInstitucion]);
            _ctInstitucion = Convert.ToString(dataReader[fields.ctInstitucion]);
            _cnDocenteInvestigador = Convert.ToBoolean(dataReader[fields.cnDocenteInvestigador]);
            _cnID_CategoriaDocente = Convert.ToInt64(dataReader[fields.cnID_CategoriaDocente]);
            _cnID_RegimenDedicacion = Convert.ToInt64(dataReader[fields.cnID_RegimenDedicacion]);
            _cnRegistradoDINA = Convert.ToBoolean(dataReader[fields.cnRegistradoDINA]);
            if (dataReader.IsDBNull(fields.ctNumeroRegistroDINA) == false)
                _ctNumeroRegistroDINA = Convert.ToString(dataReader[fields.ctNumeroRegistroDINA]);
            else
                _ctNumeroRegistroDINA = null;

            _cfFechaInicio = Convert.ToDateTime(dataReader[fields.cfFechaInicio]);
            _cfFechaCreacion = Convert.ToDateTime(dataReader[fields.cfFechaCreacion]);
            if (dataReader.IsDBNull(fields.cfFechaActualizacion) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader[fields.cfFechaActualizacion]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader[fields.ctIPAcceso]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {

            _cnID_ExperienciaDocencia = Convert.ToInt64(dataReader["cnID_ExperienciaDocencia"]);
            _cnID_Candidato = Convert.ToInt64(dataReader["cnID_Candidato"]);
            _cnID_TipoInstitucion = Convert.ToInt64(dataReader["cnID_TipoInstitucion"]);
            _ctInstitucion = Convert.ToString(dataReader["ctInstitucion"]);
            _cnDocenteInvestigador = Convert.ToBoolean(dataReader["cnDocenteInvestigador"]);
            _cnID_CategoriaDocente = Convert.ToInt64(dataReader["cnID_CategoriaDocente"]);
            _cnID_RegimenDedicacion = Convert.ToInt64(dataReader["cnID_RegimenDedicacion"]);
            _cnRegistradoDINA = Convert.ToBoolean(dataReader["cnRegistradoDINA"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctNumeroRegistroDINA")) == false)
                _ctNumeroRegistroDINA = Convert.ToString(dataReader["ctNumeroRegistroDINA"]);
            else
                _ctNumeroRegistroDINA = null;

            _cfFechaInicio = Convert.ToDateTime(dataReader["cfFechaInicio"]);
            _cfFechaCreacion = Convert.ToDateTime(dataReader["cfFechaCreacion"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("cfFechaActualizacion")) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader["cfFechaActualizacion"]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader["ctIPAcceso"]);

            // Model is ready
            SetDataLoaded(true);
        }
        #endregion
    }

    /// <summary>
    /// Business Model for tbAreaTematica
    /// </summary>
    public partial class tbAreaTematicaModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbAreaTematica
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_AreaTematica;
            public Int32 cnID_Candidato;
            public Int32 ctDominioEspecifico;
            public Int32 cfFechaCreacion;
            public Int32 cfFechaActualizacion;
            public Int32 ctIPAcceso;
            #endregion

            #region public methods
            public FieldsOrdinal(IDataReader dataReader)
            {

                cnID_AreaTematica = dataReader.GetOrdinal("cnID_AreaTematica");
                cnID_Candidato = dataReader.GetOrdinal("cnID_Candidato");
                ctDominioEspecifico = dataReader.GetOrdinal("ctDominioEspecifico");
                cfFechaCreacion = dataReader.GetOrdinal("cfFechaCreacion");
                cfFechaActualizacion = dataReader.GetOrdinal("cfFechaActualizacion");
                ctIPAcceso = dataReader.GetOrdinal("ctIPAcceso");
            }
            #endregion
        }

        #region fields variables

        private Int64 _cnID_AreaTematica;
        private Int64 _cnID_Candidato;
        private String _ctDominioEspecifico;
        private DateTime _cfFechaCreacion;
        private DateTime? _cfFechaActualizacion;
        private String _ctIPAcceso;
        #endregion

        #region fields property

        /// <summary>
        /// 
        /// </summary>
        public Int64 cnID_AreaTematica
        {
            get { return _cnID_AreaTematica; }
            set { _cnID_AreaTematica = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int64 cnID_Candidato
        {
            get { return _cnID_Candidato; }
            set { _cnID_Candidato = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String ctDominioEspecifico
        {
            get { return _ctDominioEspecifico; }
            set { _ctDominioEspecifico = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime cfFechaCreacion
        {
            get { return _cfFechaCreacion; }
            set { _cfFechaCreacion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? cfFechaActualizacion
        {
            get { return _cfFechaActualizacion; }
            set { _cfFechaActualizacion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String ctIPAcceso
        {
            get { return _ctIPAcceso; }
            set { _ctIPAcceso = value; }
        }
        #endregion

        #region public methods
        public tbAreaTematicaModel()
        {
        }


        public tbAreaTematicaModel(Int64 cnID_AreaTematica)
        {
            using (tbAreaTematicaBLL bll = tbAreaTematicaBLL.CreateInstance())
            {
                if (!bll.GetBycnID_AreaTematica(cnID_AreaTematica, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int64 Insert()
        {
            using (tbAreaTematicaBLL bll = tbAreaTematicaBLL.CreateInstance())
            {

                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbAreaTematicaBLL bll = tbAreaTematicaBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbAreaTematicaBLL bll = tbAreaTematicaBLL.CreateInstance())
            {
                bll.Delete(this.cnID_AreaTematica);
            }
        }


        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {

            _cnID_AreaTematica = Convert.ToInt64(dataReader[fields.cnID_AreaTematica]);
            _cnID_Candidato = Convert.ToInt64(dataReader[fields.cnID_Candidato]);
            if (dataReader.IsDBNull(fields.ctDominioEspecifico) == false)
                _ctDominioEspecifico = Convert.ToString(dataReader[fields.ctDominioEspecifico]);
            else
                _ctDominioEspecifico = null;

            _cfFechaCreacion = Convert.ToDateTime(dataReader[fields.cfFechaCreacion]);
            if (dataReader.IsDBNull(fields.cfFechaActualizacion) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader[fields.cfFechaActualizacion]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader[fields.ctIPAcceso]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {

            _cnID_AreaTematica = Convert.ToInt64(dataReader["cnID_AreaTematica"]);
            _cnID_Candidato = Convert.ToInt64(dataReader["cnID_Candidato"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("ctDominioEspecifico")) == false)
                _ctDominioEspecifico = Convert.ToString(dataReader["ctDominioEspecifico"]);
            else
                _ctDominioEspecifico = null;

            _cfFechaCreacion = Convert.ToDateTime(dataReader["cfFechaCreacion"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("cfFechaActualizacion")) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader["cfFechaActualizacion"]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader["ctIPAcceso"]);

            // Model is ready
            SetDataLoaded(true);
        }
        #endregion
    }

    /// <summary>
    /// Business Model for tbAreaTematicaDet
    /// </summary>
    public partial class tbAreaTematicaDetModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbAreaTematicaDet
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_AreaTematicaDet;
            public Int32 cnID_AreaTematica;
            public Int32 cnID_DominioCurso;
            public Int32 cfFechaCreacion;
            public Int32 cfFechaActualizacion;
            public Int32 ctIPAcceso;
            #endregion

            #region public methods
            public FieldsOrdinal(IDataReader dataReader)
            {

                cnID_AreaTematicaDet = dataReader.GetOrdinal("cnID_AreaTematicaDet");
                cnID_AreaTematica = dataReader.GetOrdinal("cnID_AreaTematica");
                cnID_DominioCurso = dataReader.GetOrdinal("cnID_DominioCurso");
                cfFechaCreacion = dataReader.GetOrdinal("cfFechaCreacion");
                cfFechaActualizacion = dataReader.GetOrdinal("cfFechaActualizacion");
                ctIPAcceso = dataReader.GetOrdinal("ctIPAcceso");
            }
            #endregion
        }

        #region fields variables

        private Int64 _cnID_AreaTematicaDet;
        private Int64 _cnID_AreaTematica;
        private Int64 _cnID_DominioCurso;
        private DateTime _cfFechaCreacion;
        private DateTime? _cfFechaActualizacion;
        private String _ctIPAcceso;
        #endregion

        #region fields property

        /// <summary>
        /// 
        /// </summary>
        public Int64 cnID_AreaTematicaDet
        {
            get { return _cnID_AreaTematicaDet; }
            set { _cnID_AreaTematicaDet = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int64 cnID_AreaTematica
        {
            get { return _cnID_AreaTematica; }
            set { _cnID_AreaTematica = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int64 cnID_DominioCurso
        {
            get { return _cnID_DominioCurso; }
            set { _cnID_DominioCurso = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime cfFechaCreacion
        {
            get { return _cfFechaCreacion; }
            set { _cfFechaCreacion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? cfFechaActualizacion
        {
            get { return _cfFechaActualizacion; }
            set { _cfFechaActualizacion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String ctIPAcceso
        {
            get { return _ctIPAcceso; }
            set { _ctIPAcceso = value; }
        }
        #endregion

        #region public methods
        public tbAreaTematicaDetModel()
        {
        }


        public tbAreaTematicaDetModel(Int64 cnID_AreaTematicaDet)
        {
            using (tbAreaTematicaDetBLL bll = tbAreaTematicaDetBLL.CreateInstance())
            {
                if (!bll.GetBycnID_AreaTematicaDet(cnID_AreaTematicaDet, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int64 Insert()
        {
            using (tbAreaTematicaDetBLL bll = tbAreaTematicaDetBLL.CreateInstance())
            {

                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbAreaTematicaDetBLL bll = tbAreaTematicaDetBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbAreaTematicaDetBLL bll = tbAreaTematicaDetBLL.CreateInstance())
            {
                bll.Delete(this.cnID_AreaTematicaDet);
            }
        }


        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {

            _cnID_AreaTematicaDet = Convert.ToInt64(dataReader[fields.cnID_AreaTematicaDet]);
            _cnID_AreaTematica = Convert.ToInt64(dataReader[fields.cnID_AreaTematica]);
            if (dataReader.IsDBNull(fields.cnID_DominioCurso) == false)
                _cnID_DominioCurso = Convert.ToInt64(dataReader[fields.cnID_DominioCurso]);
            else
                _cnID_DominioCurso = 0;

            _cfFechaCreacion = Convert.ToDateTime(dataReader[fields.cfFechaCreacion]);
            if (dataReader.IsDBNull(fields.cfFechaActualizacion) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader[fields.cfFechaActualizacion]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader[fields.ctIPAcceso]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {

            _cnID_AreaTematicaDet = Convert.ToInt64(dataReader["cnID_AreaTematicaDet"]);
            _cnID_AreaTematica = Convert.ToInt64(dataReader["cnID_AreaTematica"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("cnID_DominioCurso")) == false)
                _cnID_DominioCurso = Convert.ToInt64(dataReader["cnID_DominioCurso"]);
            else
                _cnID_DominioCurso = 0;

            _cfFechaCreacion = Convert.ToDateTime(dataReader["cfFechaCreacion"]);
            if (dataReader.IsDBNull(dataReader.GetOrdinal("cfFechaActualizacion")) == false)
                _cfFechaActualizacion = Convert.ToDateTime(dataReader["cfFechaActualizacion"]);
            else
                _cfFechaActualizacion = null;

            _ctIPAcceso = Convert.ToString(dataReader["ctIPAcceso"]);

            // Model is ready
            SetDataLoaded(true);
        }
        #endregion
    }

    public static class DataReaderExtensions
    {
        /// <summary>
        /// Checks if a column's value is DBNull
        /// </summary>
        /// <param name="dataReader">The data reader</param>
        /// <param name="columnName">The column name</param>
        /// <returns>A bool indicating if the column's value is DBNull</returns>
        public static bool IsDBNull(this IDataReader dataReader, string columnName)
        {
            return dataReader[columnName] == DBNull.Value;
        }

        /// <summary>
        /// Checks if a column exists in a data reader
        /// </summary>
        /// <param name="dataReader">The data reader</param>
        /// <param name="columnName">The column name</param>
        /// <returns>A bool indicating the column exists</returns>
        public static bool ContainsColumn(this IDataReader dataReader, string columnName)
        {
            /// See: http://stackoverflow.com/questions/373230/check-for-column-name-in-a-sqldatareader-object/7248381#7248381
            try
            {
                return dataReader.GetOrdinal(columnName) >= 0;
            }
            catch (IndexOutOfRangeException)
            {
                return false;
            }
        }
    }

    /// <summary>
    /// Business Model for tbExperienciaDocenciaDet
    /// </summary>
    public partial class tbExperienciaDocenciaDetModel : dbSeleccionModelBase
    {
        /// <summary>
        /// Caches fields ordinal order when reading bunch of data from tbExperienciaDocenciaDet
        /// </summary>
        public class FieldsOrdinal
        {
            #region fields ordinal cache

            public Int32 cnID_ExperienciaDocenciaDet;
            public Int32 cnID_ExperienciaDocencia;
            public Int32 cnID_Grado;
            public Int32 cnID_TipoExperiencia;
            public Int32 ctDescripcion;
            public Int32 cnTiempoExperiencia;
            public Int32 cnID_UnidadMedida;
            public Int32 ctCerfificado;
            public Int32 cfFechaCreacion;
            public Int32 cfFechaActualizacion;
            public Int32 ctIPAcceso;
            #endregion

            #region public methods
            public FieldsOrdinal(IDataReader dataReader)
            {

                cnID_ExperienciaDocenciaDet = dataReader.GetOrdinal("cnID_ExperienciaDocenciaDet");
                cnID_ExperienciaDocencia = dataReader.GetOrdinal("cnID_ExperienciaDocencia");
                cnID_Grado = dataReader.GetOrdinal("cnID_Grado");
                cnID_TipoExperiencia = dataReader.GetOrdinal("cnID_TipoExperiencia");
                ctDescripcion = dataReader.GetOrdinal("ctDescripcion");
                cnTiempoExperiencia = dataReader.GetOrdinal("cnTiempoExperiencia");
                cnID_UnidadMedida = dataReader.GetOrdinal("cnID_UnidadMedida");
                ctCerfificado = dataReader.GetOrdinal("ctCerfificado");
                cfFechaCreacion = dataReader.GetOrdinal("cfFechaCreacion");
                cfFechaActualizacion = dataReader.GetOrdinal("cfFechaActualizacion");
                ctIPAcceso = dataReader.GetOrdinal("ctIPAcceso");
            }
            #endregion
        }

        #region fields variables

        private Int64 _cnID_ExperienciaDocenciaDet;
        private Int64 _cnID_ExperienciaDocencia;
        private Int64 _cnID_Grado;
        private Int64 _cnID_TipoExperiencia;
        private String _ctDescripcion;
        private Int32 _cnTiempoExperiencia;
        private Int64 _cnID_UnidadMedida;
        private String _ctCerfificado;
        private DateTime _cfFechaCreacion;
        private DateTime _cfFechaActualizacion;
        private String _ctIPAcceso;
        #endregion

        #region fields property

        /// <summary>
        /// 
        /// </summary>
        public Int64 cnID_ExperienciaDocenciaDet
        {
            get { return _cnID_ExperienciaDocenciaDet; }
            set { _cnID_ExperienciaDocenciaDet = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int64 cnID_ExperienciaDocencia
        {
            get { return _cnID_ExperienciaDocencia; }
            set { _cnID_ExperienciaDocencia = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int64 cnID_Grado
        {
            get { return _cnID_Grado; }
            set { _cnID_Grado = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int64 cnID_TipoExperiencia
        {
            get { return _cnID_TipoExperiencia; }
            set { _cnID_TipoExperiencia = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String ctDescripcion
        {
            get { return _ctDescripcion; }
            set { _ctDescripcion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int32 cnTiempoExperiencia
        {
            get { return _cnTiempoExperiencia; }
            set { _cnTiempoExperiencia = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int64 cnID_UnidadMedida
        {
            get { return _cnID_UnidadMedida; }
            set { _cnID_UnidadMedida = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String ctCerfificado
        {
            get { return _ctCerfificado; }
            set { _ctCerfificado = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime cfFechaCreacion
        {
            get { return _cfFechaCreacion; }
            set { _cfFechaCreacion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime cfFechaActualizacion
        {
            get { return _cfFechaActualizacion; }
            set { _cfFechaActualizacion = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public String ctIPAcceso
        {
            get { return _ctIPAcceso; }
            set { _ctIPAcceso = value; }
        }
        #endregion

        #region public methods
        public tbExperienciaDocenciaDetModel()
        {
        }


        public tbExperienciaDocenciaDetModel(Int64 cnID_ExperienciaDocenciaDet)
        {
            using (tbExperienciaDocenciaDetBLL bll = tbExperienciaDocenciaDetBLL.CreateInstance())
            {
                if (!bll.GetBycnID_ExperienciaDocenciaDet(cnID_ExperienciaDocenciaDet, this))
                {
                    throw new ArgumentException();
                }
            }
        }

        public Int64 Insert()
        {
            using (tbExperienciaDocenciaDetBLL bll = tbExperienciaDocenciaDetBLL.CreateInstance())
            {

                return bll.Insert(this);
            }
        }

        public void Update()
        {
            using (tbExperienciaDocenciaDetBLL bll = tbExperienciaDocenciaDetBLL.CreateInstance())
            {
                bll.Update(this);
            }
        }

        public void Delete()
        {
            using (tbExperienciaDocenciaDetBLL bll = tbExperienciaDocenciaDetBLL.CreateInstance())
            {
                bll.Delete(this.cnID_ExperienciaDocenciaDet);
            }
        }


        /// <summary>
        /// Reads data by specified fields ordinal order cache
        /// </summary>
        public void ReadData(IDataReader dataReader, FieldsOrdinal fields)
        {

            _cnID_ExperienciaDocenciaDet = Convert.ToInt64(dataReader[fields.cnID_ExperienciaDocenciaDet]);
            _cnID_ExperienciaDocencia = Convert.ToInt64(dataReader[fields.cnID_ExperienciaDocencia]);
            _cnID_Grado = Convert.ToInt64(dataReader[fields.cnID_Grado]);
            _cnID_TipoExperiencia = Convert.ToInt64(dataReader[fields.cnID_TipoExperiencia]);
            _ctDescripcion = Convert.ToString(dataReader[fields.ctDescripcion]);
            _cnTiempoExperiencia = Convert.ToInt32(dataReader[fields.cnTiempoExperiencia]);
            _cnID_UnidadMedida = Convert.ToInt64(dataReader[fields.cnID_UnidadMedida]);
            _ctCerfificado = Convert.ToString(dataReader[fields.ctCerfificado]);
            //_cfFechaCreacion = Convert.ToDateTime(dataReader[fields.cfFechaCreacion]);
            //_cfFechaActualizacion = Convert.ToDateTime(dataReader[fields.cfFechaActualizacion]);
            _ctIPAcceso = Convert.ToString(dataReader[fields.ctIPAcceso]);

            // Model is ready
            SetDataLoaded(true);
        }

        /// <summary>
        /// Reads data from data reader
        /// </summary>
        public override void ReadData(IDataReader dataReader)
        {

            _cnID_ExperienciaDocenciaDet = Convert.ToInt64(dataReader["cnID_ExperienciaDocenciaDet"]);
            _cnID_ExperienciaDocencia = Convert.ToInt64(dataReader["cnID_ExperienciaDocencia"]);
            _cnID_Grado = Convert.ToInt64(dataReader["cnID_Grado"]);
            _cnID_TipoExperiencia = Convert.ToInt64(dataReader["cnID_TipoExperiencia"]);
            _ctDescripcion = Convert.ToString(dataReader["ctDescripcion"]);
            _cnTiempoExperiencia = Convert.ToInt32(dataReader["cnTiempoExperiencia"]);
            _cnID_UnidadMedida = Convert.ToInt64(dataReader["cnID_UnidadMedida"]);
            _ctCerfificado = Convert.ToString(dataReader["ctCerfificado"]);
            _cfFechaCreacion = Convert.ToDateTime(dataReader["cfFechaCreacion"]);
            _cfFechaActualizacion = Convert.ToDateTime(dataReader["cfFechaActualizacion"]);
            _ctIPAcceso = Convert.ToString(dataReader["ctIPAcceso"]);

            // Model is ready
            SetDataLoaded(true);
        }
        #endregion
    }
}