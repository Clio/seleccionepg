using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using SalarDb.CodeGen.Base;
using SalarDb.CodeGen.BLL;

namespace SalarDb.CodeGen.Model
{
	/// <summary>
	/// User custom methods for tbComplementario
	/// </summary>
	partial class tbComplementarioModel
	{
        public List<tbComplementarioModel> tbComplementarioModelList { get; set; }
	}
}
