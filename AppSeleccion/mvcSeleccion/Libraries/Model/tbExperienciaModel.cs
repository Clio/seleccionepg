using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using SalarDb.CodeGen.Base;
using SalarDb.CodeGen.BLL;

namespace SalarDb.CodeGen.Model
{
	/// <summary>
	/// User custom methods for tbExperiencia
	/// </summary>
	partial class tbExperienciaModel
	{
        public List<tbExperienciaModel> tbExperienciaModelList { get; set; }

        public bool Cumple { get; set; }
	}
}
