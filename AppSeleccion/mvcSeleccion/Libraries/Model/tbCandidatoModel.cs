using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using SalarDb.CodeGen.Base;
using SalarDb.CodeGen.BLL;

namespace SalarDb.CodeGen.Model
{
    /// <summary>
    /// User custom methods for tbCandidato
    /// </summary>
    partial class tbCandidatoModel
    {
        private string _NumeroDNI;
        public List<tbDocIdentidadModel> tbDocIdentidadModelList { get; set; }

        public List<tbMailModel> tbMailModelList { get; set; }

        public string NombreCompleto
        {
            get
            {
                return string.Format("{0} {1} {2}", this.ctNombres, this.ctApellidoPaterno, this.ctApellidoMaterno);
            }
        }

        public String NumeroDNI
        {
            get { return _NumeroDNI; }
            set { _NumeroDNI = value; }
        }


        public long cnID_TipoEmail { get; set; }
    }
}
