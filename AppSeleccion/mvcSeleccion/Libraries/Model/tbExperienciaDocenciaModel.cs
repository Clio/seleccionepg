﻿using SalarDb.CodeGen.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SalarDb.CodeGen.Model
{
    public partial class tbExperienciaDocenciaModel : dbSeleccionModelBase
    {
        public List<tbExperienciaDocenciaModel> tbExperienciaDocenciaModelList { get; set; }

        public List<tbExperienciaDocenciaDetModel> tbExperienciaDocenciaDetModelList { get; set; }
    }
}