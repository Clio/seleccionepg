using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using SalarDb.CodeGen.Base;
using SalarDb.CodeGen.BLL;

namespace SalarDb.CodeGen.Model
{
	/// <summary>
	/// User custom methods for tbProceso
	/// </summary>
	partial class tbProcesoModel
	{
        public List<tbProcesoCoberturaModel> tbProcesoCoberturaList { get; set; }

        public List<tbProcesoCnfgAcadModel> tbProcesoCnfgAcadModelList { get; set; }

        public List<tbProcesoCriterioAdicModel> tbProcesoCriterioAdicModelList { get; set; }

        public List<tbProcesoDocumentoModel> tbProcesoDocumentoModelList { get; set; }

        public tbProcesoCnfgLaboModel tbProcesoCnfgLabo { get; set; }

        public List<tbProcesoCnfgLaboModel> tbProcesoCnfgLaboList { get; set; }

        public List<tbProcesoMatrizLaboModel> tbProcesoMatrizLaboModelList { get; set; }

        public bool EsPostulante { get; set; }
	}
}
