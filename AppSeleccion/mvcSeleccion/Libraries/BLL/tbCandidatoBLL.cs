//using SalarDb.CodeGen.Base;
using SalarDb.CodeGen.DAL;
using SalarDb.CodeGen.Model;
using System;

namespace SalarDb.CodeGen.BLL
{
    /// <summary>
    /// User custom methods for tbCandidato
    /// </summary>
    partial class tbCandidatoBLL
    {
        public void UpdateEmail(Int64 cnID_Candidato, String ctEmail)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                dal.UpdateEmail(cnID_Candidato, ctEmail);
            }
        }

        public void UpdateClaveAcceso(Int64 cnID_Candidato, String ctClaveAcceso)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                dal.UpdateClaveAcceso(cnID_Candidato, ctClaveAcceso);
            }
        }
    }
}