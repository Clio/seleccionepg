using SalarDb.CodeGen.Base;
using SalarDb.CodeGen.DAL;
using SalarDb.CodeGen.Model;
using System;
using System.Collections.Generic;
using System.Data;
using static SalarDb.CodeGen.DAL.tbAcademicoDAL;

namespace SalarDb.CodeGen.BLL
{
    /// <summary>
    /// Business logic for tbCandidato
    /// </summary>
    public partial class tbCandidatoBLL : dbSeleccionBLLBase
    {
        internal tbCandidatoBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbCandidatoBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbCandidatoBLL"))
                return (tbCandidatoBLL)mngrInstances["tbCandidatoBLL"];
            else
            {
                tbCandidatoBLL objtbCandidatoBLL = new tbCandidatoBLL();
                mngrInstances.Add("tbCandidatoBLL", objtbCandidatoBLL);
                return objtbCandidatoBLL;
            }
        }

        public List<tbCandidatoModel> GetAll()
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbCandidatoModel GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }

        public bool GetBycnID_Candidato(Int64 cnID_Candidato, tbCandidatoModel result)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato, result);
            }
        }

        public void Update(tbCandidatoModel model)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public void UpdateProfile(tbCandidatoModel model)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                dal.UpdateProfile(model);
            }
        }

        public Int64 Insert(tbCandidatoModel model)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int64 cnID_Candidato)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                dal.Delete(cnID_Candidato);
            }
        }

        public tbCandidatoModel GetByctCodigoRecuperacion(String ctCodigoRecuperacion)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetBytcCodigoRecuperacion(ctCodigoRecuperacion);
            }
        }

        public List<tbAcademicoModel> GettbAcademicoBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbAcademicoDAL dal = new tbAcademicoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }

        public List<tbCandidatoModel> GetBycnID_Sexo(Int64 cnID_Sexo)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Sexo(cnID_Sexo);
            }
        }

        public tbMaestroListasModel GettbMaestroListasBycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }

        public List<tbCandidatoModel> GetBycnID_PaisNacimiento(Int32 cnID_PaisNacimiento)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_PaisNacimiento(cnID_PaisNacimiento);
            }
        }

        public tbPaisModel GettbPaisBycnID_Pais(Int32 cnID_Pais)
        {
            using (tbPaisDAL dal = new tbPaisDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Pais(cnID_Pais);
            }
        }

        public List<tbCandidatoModel> GetBycnID_Pais(Int32 cnID_Pais)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Pais(cnID_Pais);
            }
        }

        public tbPaisModel GettbPais_BycnID_Pais(Int32 cnID_Pais)
        {
            using (tbPaisDAL dal = new tbPaisDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Pais(cnID_Pais);
            }
        }

        public List<tbCandidatoModel> GetBycnID_Departamento(Int32 cnID_Departamento)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Departamento(cnID_Departamento);
            }
        }

        public tbDepartamentoModel GettbDepartamentoBycnID_Departamento(Int32 cnID_Departamento)
        {
            using (tbDepartamentoDAL dal = new tbDepartamentoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Departamento(cnID_Departamento);
            }
        }

        public List<tbCandidatoModel> GetBycnID_Provincia(Int32 cnID_Provincia)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Provincia(cnID_Provincia);
            }
        }

        public tbProvinciaModel GettbProvinciaBycnID_Provincia(Int32 cnID_Provincia)
        {
            using (tbProvinciaDAL dal = new tbProvinciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Provincia(cnID_Provincia);
            }
        }

        public List<tbCandidatoModel> GetBycnID_Distrito(Int32 cnID_Distrito)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Distrito(cnID_Distrito);
            }
        }

        public tbDistritoModel GettbDistritoBycnID_Distrito(Int32 cnID_Distrito)
        {
            using (tbDistritoDAL dal = new tbDistritoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Distrito(cnID_Distrito);
            }
        }

        public tbDocIdentidadModel GettbDocIdentidadBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbDocIdentidadDAL dal = new tbDocIdentidadDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }

        public List<tbEspecializacionModel> GettbEspecializacionBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbEspecializacionDAL dal = new tbEspecializacionDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }

        public List<tbExperienciaModel> GettbExperienciaBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbExperienciaDAL dal = new tbExperienciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }

        public List<tbIdiomaModel> GettbIdiomaBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbIdiomaDAL dal = new tbIdiomaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }

        public List<tbPostulanteModel> GettbPostulanteBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbPostulanteDAL dal = new tbPostulanteDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }

        public List<tbCandidatoModel> GetByctApellidoMaterno(String ctApellidoMaterno)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetByctApellidoMaterno(ctApellidoMaterno);
            }
        }

        public List<tbCandidatoModel> GetByctApellidoPaterno(String ctApellidoPaterno)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetByctApellidoPaterno(ctApellidoPaterno);
            }
        }

        public List<tbCandidatoModel> GetByctNombres(String ctNombres)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetByctNombres(ctNombres);
            }
        }

        public tbCandidatoModel GetByctEmail(String ctEmail)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetByctEmail(ctEmail);
            }
        }

        public void DeleteByctEmail(String ctEmail)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                dal.DeleteByctEmail(ctEmail);
            }
        }

        public List<tbCandidatoModel> GetConsultaCandidatos(string ctNombreCompleto, string ctNumeroDNI, string ctEmail)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetConsultaCandidatos(ctNombreCompleto, ctNumeroDNI, ctEmail);
            }
        }

        public DataTable GetConsultaQuery(string query, string columnas)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetConsultaQuery(query, columnas);
            }
        }
    }

    /// <summary>
    /// Business logic for tbCnfgSistema
    /// </summary>
    public partial class tbCnfgSistemaBLL : dbSeleccionBLLBase
    {
        internal tbCnfgSistemaBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbCnfgSistemaBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbCnfgSistemaBLL"))
                return (tbCnfgSistemaBLL)mngrInstances["tbCnfgSistemaBLL"];
            else
            {
                tbCnfgSistemaBLL objtbCnfgSistemaBLL = new tbCnfgSistemaBLL();
                mngrInstances.Add("tbCnfgSistemaBLL", objtbCnfgSistemaBLL);
                return objtbCnfgSistemaBLL;
            }
        }

        public List<tbCnfgSistemaModel> GetAll()
        {
            using (tbCnfgSistemaDAL dal = new tbCnfgSistemaDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbCnfgSistemaModel GetBycnID_Config(Int32 cnID_Config)
        {
            using (tbCnfgSistemaDAL dal = new tbCnfgSistemaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Config(cnID_Config);
            }
        }

        public tbCnfgSistemaModel GetByctCodigo(string ctCodigo)
        {
            using (tbCnfgSistemaDAL dal = new tbCnfgSistemaDAL(Transaction, Connection))
            {
                return dal.GetByctAlias(ctCodigo);
            }
        }

        public bool GetBycnID_Config(Int32 cnID_Config, tbCnfgSistemaModel result)
        {
            using (tbCnfgSistemaDAL dal = new tbCnfgSistemaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Config(cnID_Config, result);
            }
        }

        public void Update(tbCnfgSistemaModel model)
        {
            using (tbCnfgSistemaDAL dal = new tbCnfgSistemaDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int32 Insert(tbCnfgSistemaModel model)
        {
            using (tbCnfgSistemaDAL dal = new tbCnfgSistemaDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int32 cnID_Config)
        {
            using (tbCnfgSistemaDAL dal = new tbCnfgSistemaDAL(Transaction, Connection))
            {
                dal.Delete(cnID_Config);
            }
        }

        public tbCnfgSistemaModel GetByctDescripcion(String ctDescripcion)
        {
            using (tbCnfgSistemaDAL dal = new tbCnfgSistemaDAL(Transaction, Connection))
            {
                return dal.GetByctDescripcion(ctDescripcion);
            }
        }

        public void DeleteByctDescripcion(String ctDescripcion)
        {
            using (tbCnfgSistemaDAL dal = new tbCnfgSistemaDAL(Transaction, Connection))
            {
                dal.DeleteByctDescripcion(ctDescripcion);
            }
        }
    }

    /// <summary>
    /// Business logic for tbDepartamento
    /// </summary>
    public partial class tbDepartamentoBLL : dbSeleccionBLLBase
    {
        internal tbDepartamentoBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbDepartamentoBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbDepartamentoBLL"))
                return (tbDepartamentoBLL)mngrInstances["tbDepartamentoBLL"];
            else
            {
                tbDepartamentoBLL objtbDepartamentoBLL = new tbDepartamentoBLL();
                mngrInstances.Add("tbDepartamentoBLL", objtbDepartamentoBLL);
                return objtbDepartamentoBLL;
            }
        }

        public List<tbDepartamentoModel> GetAll()
        {
            using (tbDepartamentoDAL dal = new tbDepartamentoDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbDepartamentoModel GetBycnID_Departamento(Int32 cnID_Departamento)
        {
            using (tbDepartamentoDAL dal = new tbDepartamentoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Departamento(cnID_Departamento);
            }
        }

        public bool GetBycnID_Departamento(Int32 cnID_Departamento, tbDepartamentoModel result)
        {
            using (tbDepartamentoDAL dal = new tbDepartamentoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Departamento(cnID_Departamento, result);
            }
        }

        public void Update(tbDepartamentoModel model)
        {
            using (tbDepartamentoDAL dal = new tbDepartamentoDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int32 Insert(tbDepartamentoModel model)
        {
            using (tbDepartamentoDAL dal = new tbDepartamentoDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int32 cnID_Departamento)
        {
            using (tbDepartamentoDAL dal = new tbDepartamentoDAL(Transaction, Connection))
            {
                dal.Delete(cnID_Departamento);
            }
        }

        public List<tbCandidatoModel> GettbCandidatoBycnID_Departamento(Int32 cnID_Departamento)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Departamento(cnID_Departamento);
            }
        }

        public List<tbDistritoModel> GettbDistritoBycnID_Departamento(Int32 cnID_Departamento)
        {
            using (tbDistritoDAL dal = new tbDistritoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Departamento(cnID_Departamento);
            }
        }

        public List<tbProvinciaModel> GettbProvinciaBycnID_Departamento(Int32 cnID_Departamento)
        {
            using (tbProvinciaDAL dal = new tbProvinciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Departamento(cnID_Departamento);
            }
        }
    }

    /// <summary>
    /// Business logic for tbDistrito
    /// </summary>
    public partial class tbDistritoBLL : dbSeleccionBLLBase
    {
        internal tbDistritoBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbDistritoBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbDistritoBLL"))
                return (tbDistritoBLL)mngrInstances["tbDistritoBLL"];
            else
            {
                tbDistritoBLL objtbDistritoBLL = new tbDistritoBLL();
                mngrInstances.Add("tbDistritoBLL", objtbDistritoBLL);
                return objtbDistritoBLL;
            }
        }

        public List<tbDistritoModel> GetAll()
        {
            using (tbDistritoDAL dal = new tbDistritoDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbDistritoModel GetBycnID_Distrito(Int32 cnID_Distrito)
        {
            using (tbDistritoDAL dal = new tbDistritoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Distrito(cnID_Distrito);
            }
        }

        public bool GetBycnID_Distrito(Int32 cnID_Distrito, tbDistritoModel result)
        {
            using (tbDistritoDAL dal = new tbDistritoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Distrito(cnID_Distrito, result);
            }
        }

        public void Update(tbDistritoModel model)
        {
            using (tbDistritoDAL dal = new tbDistritoDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int32 Insert(tbDistritoModel model)
        {
            using (tbDistritoDAL dal = new tbDistritoDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int32 cnID_Distrito)
        {
            using (tbDistritoDAL dal = new tbDistritoDAL(Transaction, Connection))
            {
                dal.Delete(cnID_Distrito);
            }
        }

        public List<tbCandidatoModel> GettbCandidatoBycnID_Distrito(Int32 cnID_Distrito)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Distrito(cnID_Distrito);
            }
        }

        public List<tbDistritoModel> GetBycnID_Departamento(Int32 cnID_Departamento)
        {
            using (tbDistritoDAL dal = new tbDistritoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Departamento(cnID_Departamento);
            }
        }

        public tbDepartamentoModel GettbDepartamentoBycnID_Departamento(Int32 cnID_Departamento)
        {
            using (tbDepartamentoDAL dal = new tbDepartamentoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Departamento(cnID_Departamento);
            }
        }

        public List<tbDistritoModel> GetBycnID_Provincia(Int32 cnID_Provincia)
        {
            using (tbDistritoDAL dal = new tbDistritoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Provincia(cnID_Provincia);
            }
        }

        public tbProvinciaModel GettbProvinciaBycnID_Provincia(Int32 cnID_Provincia)
        {
            using (tbProvinciaDAL dal = new tbProvinciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Provincia(cnID_Provincia);
            }
        }
    }

    /// <summary>
    /// Business logic for tbDocIdentidad
    /// </summary>
    public partial class tbDocIdentidadBLL : dbSeleccionBLLBase
    {
        internal tbDocIdentidadBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbDocIdentidadBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbDocIdentidadBLL"))
                return (tbDocIdentidadBLL)mngrInstances["tbDocIdentidadBLL"];
            else
            {
                tbDocIdentidadBLL objtbDocIdentidadBLL = new tbDocIdentidadBLL();
                mngrInstances.Add("tbDocIdentidadBLL", objtbDocIdentidadBLL);
                return objtbDocIdentidadBLL;
            }
        }

        public List<tbDocIdentidadModel> GetAll()
        {
            using (tbDocIdentidadDAL dal = new tbDocIdentidadDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public List<tbDocIdentidadModel> GetBycnID_CandidatoList(Int64 cnID_Candidato)
        {
            using (tbDocIdentidadDAL dal = new tbDocIdentidadDAL(Transaction, Connection))
            {
                return dal.GetBycnID_CandidatoList(cnID_Candidato);
            }
        }

        public tbDocIdentidadModel GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbDocIdentidadDAL dal = new tbDocIdentidadDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }

        public bool GetBycnID_Candidato(Int64 cnID_Candidato, tbDocIdentidadModel result)
        {
            using (tbDocIdentidadDAL dal = new tbDocIdentidadDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato, result);
            }
        }

        public void Update(tbDocIdentidadModel model)
        {
            using (tbDocIdentidadDAL dal = new tbDocIdentidadDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public void Insert(tbDocIdentidadModel model)
        {
            using (tbDocIdentidadDAL dal = new tbDocIdentidadDAL(Transaction, Connection))
            {
                dal.Insert(model);
            }
        }

        public void Delete(Int64 cnID_Candidato)
        {
            using (tbDocIdentidadDAL dal = new tbDocIdentidadDAL(Transaction, Connection))
            {
                dal.Delete(cnID_Candidato);
            }
        }

        public tbCandidatoModel GettbCandidatoBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }

        public tbMaestroListasModel GettbMaestroListasBycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }

        public List<tbDocIdentidadModel> GetByctNumero(String ctNumero)
        {
            using (tbDocIdentidadDAL dal = new tbDocIdentidadDAL(Transaction, Connection))
            {
                return dal.GetByctNumero(ctNumero);
            }
        }
    }


    /// <summary>
    /// Business logic for tbMail
    /// </summary>
    public partial class tbMailBLL : dbSeleccionBLLBase
    {
        internal tbMailBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbMailBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbMaildBLL"))
                return (tbMailBLL)mngrInstances["tbMaildBLL"];
            else
            {
                tbMailBLL objtbMaildBLL = new tbMailBLL();
                mngrInstances.Add("tbMaildBLL", objtbMaildBLL);
                return objtbMaildBLL;
            }
        }

        public List<tbMailModel> GetAll()
        {
            using (tbMailDAL dal = new tbMailDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public List<tbMailModel> GetBycnID_CandidatoList(Int64 cnID_Candidato)
        {
            using (tbMailDAL dal = new tbMailDAL(Transaction, Connection))
            {
                return dal.GetBycnID_CandidatoList(cnID_Candidato);
            }
        }

        public tbMailModel GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbMailDAL dal = new tbMailDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }

        public bool GetBycnID_Candidato(Int64 cnID_Candidato, tbMailModel result)
        {
            using (tbMailDAL dal = new tbMailDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato, result);
            }
        }

        public void Update(tbMailModel model)
        {
            using (tbMailDAL dal = new tbMailDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public void Insert(tbMailModel model)
        {
            using (tbMailDAL dal = new tbMailDAL(Transaction, Connection))
            {
                dal.Insert(model);
            }
        }

        public void Delete(Int64 cnID_Candidato)
        {
            using (tbMailDAL dal = new tbMailDAL(Transaction, Connection))
            {
                dal.Delete(cnID_Candidato);
            }
        }

        public tbMailModel GettbCandidatoBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbMailDAL dal = new tbMailDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }


        public List<tbMailModel> GetByctNumero(String ctMail)
        {
            using (tbMailDAL dal = new tbMailDAL(Transaction, Connection))
            {
                return dal.GetByctMail(ctMail);
            }
        }
    }

    /// <summary>
    /// Business logic for tbEspecializacion
    /// </summary>
    public partial class tbEspecializacionBLL : dbSeleccionBLLBase
    {
        internal tbEspecializacionBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbEspecializacionBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbEspecializacionBLL"))
                return (tbEspecializacionBLL)mngrInstances["tbEspecializacionBLL"];
            else
            {
                tbEspecializacionBLL objtbEspecializacionBLL = new tbEspecializacionBLL();
                mngrInstances.Add("tbEspecializacionBLL", objtbEspecializacionBLL);
                return objtbEspecializacionBLL;
            }
        }

        public List<tbEspecializacionModel> GetAll()
        {
            using (tbEspecializacionDAL dal = new tbEspecializacionDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbEspecializacionModel GetBycnID_Especializacion(Int64 cnID_Especializacion)
        {
            using (tbEspecializacionDAL dal = new tbEspecializacionDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Especializacion(cnID_Especializacion);
            }
        }

        public bool GetBycnID_Especializacion(Int64 cnID_Especializacion, tbEspecializacionModel result)
        {
            using (tbEspecializacionDAL dal = new tbEspecializacionDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Especializacion(cnID_Especializacion, result);
            }
        }

        public void Update(tbEspecializacionModel model)
        {
            using (tbEspecializacionDAL dal = new tbEspecializacionDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int64 Insert(tbEspecializacionModel model)
        {
            using (tbEspecializacionDAL dal = new tbEspecializacionDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int64 cnID_Especializacion)
        {
            using (tbEspecializacionDAL dal = new tbEspecializacionDAL(Transaction, Connection))
            {
                dal.Delete(cnID_Especializacion);
            }
        }

        public List<tbEspecializacionModel> GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbEspecializacionDAL dal = new tbEspecializacionDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }

        public tbCandidatoModel GettbCandidatoBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }

        public List<tbEspecializacionModel> GetBycnID_Especialidad(Int64 cnID_Especialidad)
        {
            using (tbEspecializacionDAL dal = new tbEspecializacionDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Especialidad(cnID_Especialidad);
            }
        }

        public tbMaestroListasModel GettbMaestroListasBycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }

        public List<tbEspecializacionModel> GetBycnID_Nivel(Int64 cnID_Nivel)
        {
            using (tbEspecializacionDAL dal = new tbEspecializacionDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Nivel(cnID_Nivel);
            }
        }

        public tbMaestroListasModel GettbMaestroListas_BycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }

        public List<tbEspecializacionModel> GetBycnID_Pais(Int32 cnID_Pais)
        {
            using (tbEspecializacionDAL dal = new tbEspecializacionDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Pais(cnID_Pais);
            }
        }

        public tbPaisModel GettbPaisBycnID_Pais(Int32 cnID_Pais)
        {
            using (tbPaisDAL dal = new tbPaisDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Pais(cnID_Pais);
            }
        }
    }

    /// <summary>
    /// Business logic for tbExperiencia
    /// </summary>
    public partial class tbExperienciaBLL : dbSeleccionBLLBase
    {
        internal tbExperienciaBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbExperienciaBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbExperienciaBLL"))
                return (tbExperienciaBLL)mngrInstances["tbExperienciaBLL"];
            else
            {
                tbExperienciaBLL objtbExperienciaBLL = new tbExperienciaBLL();
                mngrInstances.Add("tbExperienciaBLL", objtbExperienciaBLL);
                return objtbExperienciaBLL;
            }
        }

        public List<tbExperienciaModel> GetAll()
        {
            using (tbExperienciaDAL dal = new tbExperienciaDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbExperienciaModel GetBycnID_Experiencia(Int64 cnID_Experiencia)
        {
            using (tbExperienciaDAL dal = new tbExperienciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Experiencia(cnID_Experiencia);
            }
        }

        public bool GetBycnID_Experiencia(Int64 cnID_Experiencia, tbExperienciaModel result)
        {
            using (tbExperienciaDAL dal = new tbExperienciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Experiencia(cnID_Experiencia, result);
            }
        }

        public void Update(tbExperienciaModel model)
        {
            using (tbExperienciaDAL dal = new tbExperienciaDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int64 Insert(tbExperienciaModel model)
        {
            using (tbExperienciaDAL dal = new tbExperienciaDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public Int64 InsertBloque(tbExperienciaModel model, int idbloque)
        {
            using (tbExperienciaDAL dal = new tbExperienciaDAL(Transaction, Connection))
            {
                return dal.InsertBloque(model,idbloque);
            }
        }

        public void Delete(Int64 cnID_Experiencia)
        {
            using (tbExperienciaDAL dal = new tbExperienciaDAL(Transaction, Connection))
            {
                dal.Delete(cnID_Experiencia);
            }
        }

        public void DeleteByCandidato(Int64 cnID_Experiencia)
        {
            using (tbExperienciaDAL dal = new tbExperienciaDAL(Transaction, Connection))
            {
                dal.DeleteByIdCandidato(cnID_Experiencia);
            }
        }

        public List<tbExperienciaModel> GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbExperienciaDAL dal = new tbExperienciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }

        public tbCandidatoModel GettbCandidatoBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }

        public List<tbExperienciaModel> GetBycnID_TipoEntidad(Int64 cnID_TipoEntidad)
        {
            using (tbExperienciaDAL dal = new tbExperienciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_TipoEntidad(cnID_TipoEntidad);
            }
        }

        public tbMaestroListasModel GettbMaestroListasBycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }

        public List<tbExperienciaModel> GetBycnID_Rubro(Int64 cnID_Rubro)
        {
            using (tbExperienciaDAL dal = new tbExperienciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Rubro(cnID_Rubro);
            }
        }

        public tbMaestroListasModel GettbMaestroListas_BycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }

        public List<tbExperienciaModel> GetBycnID_Cargo(Int64 cnID_Cargo)
        {
            using (tbExperienciaDAL dal = new tbExperienciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Cargo(cnID_Cargo);
            }
        }

        public tbMaestroListasModel GettbMaestroListas_1BycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }

        public List<tbExperienciaModel> GetBycnID_Area(Int64 cnID_Area)
        {
            using (tbExperienciaDAL dal = new tbExperienciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Area(cnID_Area);
            }
        }

        public tbMaestroListasModel GettbMaestroListas_2BycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }

        public List<tbExperienciaModel> GetBycnID_TipoContrato(Int64 cnID_TipoContrato)
        {
            using (tbExperienciaDAL dal = new tbExperienciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_TipoContrato(cnID_TipoContrato);
            }
        }

        public tbMaestroListasModel GettbMaestroListas_3BycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }

        public List<tbExperienciaModel> GetBycnID_Pais(Int32 cnID_Pais)
        {
            using (tbExperienciaDAL dal = new tbExperienciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Pais(cnID_Pais);
            }
        }

        public tbPaisModel GettbPaisBycnID_Pais(Int32 cnID_Pais)
        {
            using (tbPaisDAL dal = new tbPaisDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Pais(cnID_Pais);
            }
        }
    }

    /// <summary>
    /// Business logic for tbIdioma
    /// </summary>
    public partial class tbIdiomaBLL : dbSeleccionBLLBase
    {
        internal tbIdiomaBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbIdiomaBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbIdiomaBLL"))
                return (tbIdiomaBLL)mngrInstances["tbIdiomaBLL"];
            else
            {
                tbIdiomaBLL objtbIdiomaBLL = new tbIdiomaBLL();
                mngrInstances.Add("tbIdiomaBLL", objtbIdiomaBLL);
                return objtbIdiomaBLL;
            }
        }

        public List<tbIdiomaModel> GetAll()
        {
            using (tbIdiomaDAL dal = new tbIdiomaDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbIdiomaModel GetBycnID_Idioma(Int64 cnID_Idioma)
        {
            using (tbIdiomaDAL dal = new tbIdiomaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Idioma(cnID_Idioma);
            }
        }

        public bool GetBycnID_Idioma(Int64 cnID_Idioma, tbIdiomaModel result)
        {
            using (tbIdiomaDAL dal = new tbIdiomaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Idioma(cnID_Idioma, result);
            }
        }

        public void Update(tbIdiomaModel model)
        {
            using (tbIdiomaDAL dal = new tbIdiomaDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int64 Insert(tbIdiomaModel model)
        {
            using (tbIdiomaDAL dal = new tbIdiomaDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int64 cnID_Idioma)
        {
            using (tbIdiomaDAL dal = new tbIdiomaDAL(Transaction, Connection))
            {
                dal.Delete(cnID_Idioma);
            }
        }

        public List<tbIdiomaModel> GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbIdiomaDAL dal = new tbIdiomaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }

        public tbCandidatoModel GettbCandidatoBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }

        public List<tbIdiomaModel> GetBycnID_Lengua(Int64 cnID_Lengua)
        {
            using (tbIdiomaDAL dal = new tbIdiomaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lengua(cnID_Lengua);
            }
        }

        public tbMaestroListasModel GettbMaestroListasBycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }

        public List<tbIdiomaModel> GetBycnID_Institucion(Int64 cnID_Institucion)
        {
            using (tbIdiomaDAL dal = new tbIdiomaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Institucion(cnID_Institucion);
            }
        }

        public tbMaestroListasModel GettbMaestroListas_BycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }

        public List<tbIdiomaModel> GetBycnID_NivelLectura(Int64 cnID_NivelLectura)
        {
            using (tbIdiomaDAL dal = new tbIdiomaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_NivelLectura(cnID_NivelLectura);
            }
        }

        public tbMaestroListasModel GettbMaestroListas_1BycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }

        public List<tbIdiomaModel> GetBycnID_NivelEscritura(Int64 cnID_NivelEscritura)
        {
            using (tbIdiomaDAL dal = new tbIdiomaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_NivelEscritura(cnID_NivelEscritura);
            }
        }

        public tbMaestroListasModel GettbMaestroListas_2BycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }

        public List<tbIdiomaModel> GetBycnID_NivelConversacion(Int64 cnID_NivelConversacion)
        {
            using (tbIdiomaDAL dal = new tbIdiomaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_NivelConversacion(cnID_NivelConversacion);
            }
        }

        public tbMaestroListasModel GettbMaestroListas_3BycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }

        public List<tbIdiomaModel> GetBycnID_Situacion(Int64 cnID_Situacion)
        {
            using (tbIdiomaDAL dal = new tbIdiomaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Situacion(cnID_Situacion);
            }
        }

        public tbMaestroListasModel GettbMaestroListas_4BycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }
    }

    /// <summary>
    /// Business logic for tbMaestroGrupos
    /// </summary>
    public partial class tbMaestroGruposBLL : dbSeleccionBLLBase
    {
        internal tbMaestroGruposBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbMaestroGruposBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbMaestroGruposBLL"))
                return (tbMaestroGruposBLL)mngrInstances["tbMaestroGruposBLL"];
            else
            {
                tbMaestroGruposBLL objtbMaestroGruposBLL = new tbMaestroGruposBLL();
                mngrInstances.Add("tbMaestroGruposBLL", objtbMaestroGruposBLL);
                return objtbMaestroGruposBLL;
            }
        }

        public List<tbMaestroGruposModel> GetAll()
        {
            using (tbMaestroGruposDAL dal = new tbMaestroGruposDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        //
        public List<tbMaestroGruposModel> GetByctTipoGrupo(string ctTipoGrupo)
        {
            using (tbMaestroGruposDAL dal = new tbMaestroGruposDAL(Transaction, Connection))
            {
                return dal.GetByctTipoGrupo(ctTipoGrupo);
            }
        }


        public tbMaestroGruposModel GetBycnID_GrupoLista(Int32 cnID_GrupoLista)
        {
            using (tbMaestroGruposDAL dal = new tbMaestroGruposDAL(Transaction, Connection))
            {
                return dal.GetBycnID_GrupoLista(cnID_GrupoLista);
            }
        }

        public bool GetBycnID_GrupoLista(Int32 cnID_GrupoLista, tbMaestroGruposModel result)
        {
            using (tbMaestroGruposDAL dal = new tbMaestroGruposDAL(Transaction, Connection))
            {
                return dal.GetBycnID_GrupoLista(cnID_GrupoLista, result);
            }
        }

        public void Update(tbMaestroGruposModel model)
        {
            using (tbMaestroGruposDAL dal = new tbMaestroGruposDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int32 Insert(tbMaestroGruposModel model)
        {
            using (tbMaestroGruposDAL dal = new tbMaestroGruposDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int32 cnID_GrupoLista)
        {
            using (tbMaestroGruposDAL dal = new tbMaestroGruposDAL(Transaction, Connection))
            {
                dal.Delete(cnID_GrupoLista);
            }
        }

        public List<tbMaestroListasModel> GettbMaestroListasBycnID_GrupoLista(Int32 cnID_GrupoLista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_GrupoLista(cnID_GrupoLista);
            }
        }
    }

    /// <summary>
    /// Business logic for tbMaestroListas
    /// </summary>
    public partial class tbMaestroListasBLL : dbSeleccionBLLBase
    {
        internal tbMaestroListasBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbMaestroListasBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbMaestroListasBLL"))
                return (tbMaestroListasBLL)mngrInstances["tbMaestroListasBLL"];
            else
            {
                tbMaestroListasBLL objtbMaestroListasBLL = new tbMaestroListasBLL();
                mngrInstances.Add("tbMaestroListasBLL", objtbMaestroListasBLL);
                return objtbMaestroListasBLL;
            }
        }

        public List<tbMaestroListasModel> GetAll()
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbMaestroListasModel GetBycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }

        public bool GetBycnID_Lista(Int64 cnID_Lista, tbMaestroListasModel result)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista, result);
            }
        }

        public void Update(tbMaestroListasModel model)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int64 Insert(tbMaestroListasModel model)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                dal.Delete(cnID_Lista);
            }
        }

        public List<tbAcademicoModel> GettbAcademicoBycnID_Grado(Int64 cnID_Grado)
        {
            using (tbAcademicoDAL dal = new tbAcademicoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Grado(cnID_Grado);
            }
        }

        public List<tbAcademicoModel> GettbAcademico_BycnID_Carrera(Int64 cnID_Carrera)
        {
            using (tbAcademicoDAL dal = new tbAcademicoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Carrera(cnID_Carrera);
            }
        }

        public List<tbAcademicoModel> GettbAcademico_1BycnID_Institucion(Int64 cnID_Institucion)
        {
            using (tbAcademicoDAL dal = new tbAcademicoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Institucion(cnID_Institucion);
            }
        }

        public List<tbAcademicoModel> GettbAcademico_2BycnID_Situacion(Int64 cnID_Situacion)
        {
            using (tbAcademicoDAL dal = new tbAcademicoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Situacion(cnID_Situacion);
            }
        }

        public List<tbCandidatoModel> GettbCandidatoBycnID_Sexo(Int64 cnID_Sexo)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Sexo(cnID_Sexo);
            }
        }

        //TODO: Revisar
        //public tbDocIdentidadModel GettbDocIdentidadBycnID_TipoDocumento(Int64 cnID_TipoDocumento)
        //{
        //    using (tbDocIdentidadDAL dal = new tbDocIdentidadDAL(Transaction, Connection))
        //    {
        //        return dal.___GetBycnID_TipoDocumento((long)cnID_TipoDocumento);
        //    }
        //}
        public List<tbEspecializacionModel> GettbEspecializacionBycnID_Especialidad(Int64 cnID_Especialidad)
        {
            using (tbEspecializacionDAL dal = new tbEspecializacionDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Especialidad(cnID_Especialidad);
            }
        }

        public List<tbEspecializacionModel> GettbEspecializacion_BycnID_Nivel(Int64 cnID_Nivel)
        {
            using (tbEspecializacionDAL dal = new tbEspecializacionDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Nivel(cnID_Nivel);
            }
        }

        public List<tbExperienciaModel> GettbExperienciaBycnID_TipoEntidad(Int64 cnID_TipoEntidad)
        {
            using (tbExperienciaDAL dal = new tbExperienciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_TipoEntidad(cnID_TipoEntidad);
            }
        }

        public List<tbExperienciaModel> GettbExperiencia_BycnID_Rubro(Int64 cnID_Rubro)
        {
            using (tbExperienciaDAL dal = new tbExperienciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Rubro(cnID_Rubro);
            }
        }

        public List<tbExperienciaModel> GettbExperiencia_1BycnID_Cargo(Int64 cnID_Cargo)
        {
            using (tbExperienciaDAL dal = new tbExperienciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Cargo(cnID_Cargo);
            }
        }

        public List<tbExperienciaModel> GettbExperiencia_2BycnID_Area(Int64 cnID_Area)
        {
            using (tbExperienciaDAL dal = new tbExperienciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Area(cnID_Area);
            }
        }

        public List<tbExperienciaModel> GettbExperiencia_3BycnID_TipoContrato(Int64 cnID_TipoContrato)
        {
            using (tbExperienciaDAL dal = new tbExperienciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_TipoContrato(cnID_TipoContrato);
            }
        }

        public List<tbIdiomaModel> GettbIdiomaBycnID_Lengua(Int64 cnID_Lengua)
        {
            using (tbIdiomaDAL dal = new tbIdiomaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lengua(cnID_Lengua);
            }
        }

        public List<tbIdiomaModel> GettbIdioma_BycnID_Institucion(Int64 cnID_Institucion)
        {
            using (tbIdiomaDAL dal = new tbIdiomaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Institucion(cnID_Institucion);
            }
        }

        public List<tbIdiomaModel> GettbIdioma_1BycnID_NivelLectura(Int64 cnID_NivelLectura)
        {
            using (tbIdiomaDAL dal = new tbIdiomaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_NivelLectura(cnID_NivelLectura);
            }
        }

        public List<tbIdiomaModel> GettbIdioma_2BycnID_NivelEscritura(Int64 cnID_NivelEscritura)
        {
            using (tbIdiomaDAL dal = new tbIdiomaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_NivelEscritura(cnID_NivelEscritura);
            }
        }

        public List<tbIdiomaModel> GettbIdioma_3BycnID_NivelConversacion(Int64 cnID_NivelConversacion)
        {
            using (tbIdiomaDAL dal = new tbIdiomaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_NivelConversacion(cnID_NivelConversacion);
            }
        }

        public List<tbIdiomaModel> GettbIdioma_4BycnID_Situacion(Int64 cnID_Situacion)
        {
            using (tbIdiomaDAL dal = new tbIdiomaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Situacion(cnID_Situacion);
            }
        }

        public List<tbMaestroListasModel> GetBycnID_GrupoLista(Int32 cnID_GrupoLista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_GrupoLista(cnID_GrupoLista);
            }
        }

        public List<tbMaestroListasModel> GetBycnID_ListaPadre(Int32 cnID_ListaPadre)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_ListaPadre(cnID_ListaPadre);
            }
        }


        public List<tbMaestroListasModel> GetAreaTematicaNivelUltimo(Int32 cnID_ListaPadre, long cnID_Candidato)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetAreaTematicaNivelUltimo(cnID_ListaPadre, cnID_Candidato);
            }
        }

        public tbMaestroGruposModel GettbMaestroGruposBycnID_GrupoLista(Int32 cnID_GrupoLista)
        {
            using (tbMaestroGruposDAL dal = new tbMaestroGruposDAL(Transaction, Connection))
            {
                return dal.GetBycnID_GrupoLista(cnID_GrupoLista);
            }
        }

        public List<tbProcesoModel> GettbProcesoBycnID_Tipo(Int64 cnID_Tipo)
        {
            using (tbProcesoDAL dal = new tbProcesoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Tipo(cnID_Tipo);
            }
        }

        public List<tbProcesoModel> GettbProceso_BycnID_Estado(Int64 cnID_Estado)
        {
            using (tbProcesoDAL dal = new tbProcesoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Estado(cnID_Estado);
            }
        }

        public List<tbProcesoCnfgAcadModel> GettbProcesoCnfgAcadBycnID_Grado(Int64 cnID_Grado)
        {
            using (tbProcesoCnfgAcadDAL dal = new tbProcesoCnfgAcadDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Grado(cnID_Grado);
            }
        }

        public List<tbProcesoCnfgAcadModel> GettbProcesoCnfgAcad_BycnID_Situacion(Int64 cnID_Situacion)
        {
            using (tbProcesoCnfgAcadDAL dal = new tbProcesoCnfgAcadDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Situacion(cnID_Situacion);
            }
        }

        public List<tbProcesoCnfgLaboModel> GettbProcesoCnfgLaboBycnID_Tipo(Int64 cnID_Tipo)
        {
            using (tbProcesoCnfgLaboDAL dal = new tbProcesoCnfgLaboDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Tipo(cnID_Tipo);
            }
        }

        public List<tbProcesoMatrizLaboModel> GettbProcesoMatrizLaboBycnID_TipoEvaluacion(Int64 cnID_TipoEvaluacion)
        {
            using (tbProcesoMatrizLaboDAL dal = new tbProcesoMatrizLaboDAL(Transaction, Connection))
            {
                return dal.GetBycnID_TipoEvaluacion(cnID_TipoEvaluacion);
            }
        }
    }

    /// <summary>
    /// Business logic for tbMenu
    /// </summary>
    public partial class tbMenuBLL : dbSeleccionBLLBase
    {
        internal tbMenuBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbMenuBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbMenuBLL"))
                return (tbMenuBLL)mngrInstances["tbMenuBLL"];
            else
            {
                tbMenuBLL objtbMenuBLL = new tbMenuBLL();
                mngrInstances.Add("tbMenuBLL", objtbMenuBLL);
                return objtbMenuBLL;
            }
        }

        public List<tbMenuModel> GetAll()
        {
            using (tbMenuDAL dal = new tbMenuDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbMenuModel GetBycnID_Menu(Int64 cnID_Menu)
        {
            using (tbMenuDAL dal = new tbMenuDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Menu(cnID_Menu);
            }
        }

        public bool GetBycnID_Menu(Int64 cnID_Menu, tbMenuModel result)
        {
            using (tbMenuDAL dal = new tbMenuDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Menu(cnID_Menu, result);
            }
        }

        public void Update(tbMenuModel model)
        {
            using (tbMenuDAL dal = new tbMenuDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int64 Insert(tbMenuModel model)
        {
            using (tbMenuDAL dal = new tbMenuDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int64 cnID_Menu)
        {
            using (tbMenuDAL dal = new tbMenuDAL(Transaction, Connection))
            {
                dal.Delete(cnID_Menu);
            }
        }

        public List<tbMenuModel> GetBycnID_Perfil(Int32 cnID_Perfil)
        {
            using (tbMenuDAL dal = new tbMenuDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Perfil(cnID_Perfil);
            }
        }

        public tbPerfilUsuarioModel GettbPerfilUsuarioBycnID_Perfil(Int32 cnID_Perfil)
        {
            using (tbPerfilUsuarioDAL dal = new tbPerfilUsuarioDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Perfil(cnID_Perfil);
            }
        }
    }

    /// <summary>
    /// Business logic for tbPais
    /// </summary>
    public partial class tbPaisBLL : dbSeleccionBLLBase
    {
        internal tbPaisBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbPaisBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbPaisBLL"))
                return (tbPaisBLL)mngrInstances["tbPaisBLL"];
            else
            {
                tbPaisBLL objtbPaisBLL = new tbPaisBLL();
                mngrInstances.Add("tbPaisBLL", objtbPaisBLL);
                return objtbPaisBLL;
            }
        }

        public List<tbPaisModel> GetAll()
        {
            using (tbPaisDAL dal = new tbPaisDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbPaisModel GetBycnID_Pais(Int32 cnID_Pais)
        {
            using (tbPaisDAL dal = new tbPaisDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Pais(cnID_Pais);
            }
        }

        public bool GetBycnID_Pais(Int32 cnID_Pais, tbPaisModel result)
        {
            using (tbPaisDAL dal = new tbPaisDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Pais(cnID_Pais, result);
            }
        }

        public void Update(tbPaisModel model)
        {
            using (tbPaisDAL dal = new tbPaisDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int32 Insert(tbPaisModel model)
        {
            using (tbPaisDAL dal = new tbPaisDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int32 cnID_Pais)
        {
            using (tbPaisDAL dal = new tbPaisDAL(Transaction, Connection))
            {
                dal.Delete(cnID_Pais);
            }
        }

        public List<tbAcademicoModel> GettbAcademicoBycnID_Pais(Int32 cnID_Pais)
        {
            using (tbAcademicoDAL dal = new tbAcademicoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Pais(cnID_Pais);
            }
        }

        public List<tbCandidatoModel> GettbCandidatoBycnID_PaisNacimiento(Int32 cnID_PaisNacimiento)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_PaisNacimiento(cnID_PaisNacimiento);
            }
        }

        public List<tbCandidatoModel> GettbCandidato_BycnID_Pais(Int32 cnID_Pais)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Pais(cnID_Pais);
            }
        }

        public List<tbEspecializacionModel> GettbEspecializacionBycnID_Pais(Int32 cnID_Pais)
        {
            using (tbEspecializacionDAL dal = new tbEspecializacionDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Pais(cnID_Pais);
            }
        }

        public List<tbExperienciaModel> GettbExperienciaBycnID_Pais(Int32 cnID_Pais)
        {
            using (tbExperienciaDAL dal = new tbExperienciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Pais(cnID_Pais);
            }
        }
    }

    /// <summary>
    /// Business logic for tbPerfilUsuario
    /// </summary>
    public partial class tbPerfilUsuarioBLL : dbSeleccionBLLBase
    {
        internal tbPerfilUsuarioBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbPerfilUsuarioBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbPerfilUsuarioBLL"))
                return (tbPerfilUsuarioBLL)mngrInstances["tbPerfilUsuarioBLL"];
            else
            {
                tbPerfilUsuarioBLL objtbPerfilUsuarioBLL = new tbPerfilUsuarioBLL();
                mngrInstances.Add("tbPerfilUsuarioBLL", objtbPerfilUsuarioBLL);
                return objtbPerfilUsuarioBLL;
            }
        }

        public List<tbPerfilUsuarioModel> GetAll()
        {
            using (tbPerfilUsuarioDAL dal = new tbPerfilUsuarioDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbPerfilUsuarioModel GetBycnID_Perfil(Int32 cnID_Perfil)
        {
            using (tbPerfilUsuarioDAL dal = new tbPerfilUsuarioDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Perfil(cnID_Perfil);
            }
        }

        public bool GetBycnID_Perfil(Int32 cnID_Perfil, tbPerfilUsuarioModel result)
        {
            using (tbPerfilUsuarioDAL dal = new tbPerfilUsuarioDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Perfil(cnID_Perfil, result);
            }
        }

        public void Update(tbPerfilUsuarioModel model)
        {
            using (tbPerfilUsuarioDAL dal = new tbPerfilUsuarioDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public void Insert(tbPerfilUsuarioModel model)
        {
            using (tbPerfilUsuarioDAL dal = new tbPerfilUsuarioDAL(Transaction, Connection))
            {
                dal.Insert(model);
            }
        }

        public void Delete(Int32 cnID_Perfil)
        {
            using (tbPerfilUsuarioDAL dal = new tbPerfilUsuarioDAL(Transaction, Connection))
            {
                dal.Delete(cnID_Perfil);
            }
        }

        public List<tbMenuModel> GettbMenuBycnID_Perfil(Int32 cnID_Perfil)
        {
            using (tbMenuDAL dal = new tbMenuDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Perfil(cnID_Perfil);
            }
        }

        public List<tbUsuarioModel> GettbUsuarioBycnID_Perfil(Int32 cnID_Perfil)
        {
            using (tbUsuarioDAL dal = new tbUsuarioDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Perfil(cnID_Perfil);
            }
        }
    }

    /// <summary>
    /// Business logic for tbPostulante
    /// </summary>
    public partial class tbPostulanteBLL : dbSeleccionBLLBase
    {
        internal tbPostulanteBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbPostulanteBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbPostulanteBLL"))
                return (tbPostulanteBLL)mngrInstances["tbPostulanteBLL"];
            else
            {
                tbPostulanteBLL objtbPostulanteBLL = new tbPostulanteBLL();
                mngrInstances.Add("tbPostulanteBLL", objtbPostulanteBLL);
                return objtbPostulanteBLL;
            }
        }

        public List<tbPostulanteModel> GetAll()
        {
            using (tbPostulanteDAL dal = new tbPostulanteDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbPostulanteModel GetBycnID_Postulante(Int64 cnID_Postulante)
        {
            using (tbPostulanteDAL dal = new tbPostulanteDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Postulante(cnID_Postulante);
            }
        }

        public bool GetBycnID_Postulante(Int64 cnID_Postulante, tbPostulanteModel result)
        {
            using (tbPostulanteDAL dal = new tbPostulanteDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Postulante(cnID_Postulante, result);
            }
        }

        public void Update(tbPostulanteModel model)
        {
            using (tbPostulanteDAL dal = new tbPostulanteDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int64 Insert(tbPostulanteModel model)
        {
            using (tbPostulanteDAL dal = new tbPostulanteDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int64 cnID_Postulante)
        {
            using (tbPostulanteDAL dal = new tbPostulanteDAL(Transaction, Connection))
            {
                dal.Delete(cnID_Postulante);
            }
        }

        public List<tbPostulanteModel> GetBycnID_Proceso(Int64 cnID_Proceso)
        {
            using (tbPostulanteDAL dal = new tbPostulanteDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Proceso(cnID_Proceso);
            }
        }

        public List<tbPostulanteModel> GetConsultaPostulantes(Int64 cnID_Proceso, Int32 cnTipoResultado,
                                                                string ctNumeroDNI, string ctEmail,
                                                                Int64 cnCobertura, Int64 cnZona)
        {
            using (tbPostulanteDAL dal = new tbPostulanteDAL(Transaction, Connection))
            {
                return dal.GetConsultaPostulantes(cnID_Proceso, cnTipoResultado, ctNumeroDNI, ctEmail, cnCobertura, cnZona);
            }
        }

        public void AnularPostulacion(Int64 cnID_Candidato, Int64 cnID_Proceso)
        {
            using (tbPostulanteDAL dal = new tbPostulanteDAL(Transaction, Connection))
            {
                dal.AnularPostulacion(cnID_Candidato, cnID_Proceso);
            }
        }

        public tbProcesoModel GettbProcesoBycnID_Proceso(Int64 cnID_Proceso)
        {
            using (tbProcesoDAL dal = new tbProcesoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Proceso(cnID_Proceso);
            }
        }

        public List<tbPostulanteModel> GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbPostulanteDAL dal = new tbPostulanteDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }

        public tbCandidatoModel GettbCandidatoBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }

        public List<tbPostulanteCobeModel> GettbPostulanteCobeBycnID_Postulante(Int64 cnID_Postulante)
        {
            using (tbPostulanteCobeDAL dal = new tbPostulanteCobeDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Postulante(cnID_Postulante);
            }
        }

        public List<tbPostulanteDocuModel> GettbPostulanteDocuBycnID_Postulante(Int64 cnID_Postulante)
        {
            using (tbPostulanteDocuDAL dal = new tbPostulanteDocuDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Postulante(cnID_Postulante);
            }
        }

        public List<tbPostulanteLaboModel> GettbPostulanteLaboBycnID_Postulante(Int64 cnID_Postulante)
        {
            using (tbPostulanteLaboDAL dal = new tbPostulanteLaboDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Postulante(cnID_Postulante);
            }
        }
    }

    /// <summary>
    /// Business logic for tbPostulanteCobe
    /// </summary>
    public partial class tbPostulanteCobeBLL : dbSeleccionBLLBase
    {
        internal tbPostulanteCobeBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbPostulanteCobeBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbPostulanteCobeBLL"))
                return (tbPostulanteCobeBLL)mngrInstances["tbPostulanteCobeBLL"];
            else
            {
                tbPostulanteCobeBLL objtbPostulanteCobeBLL = new tbPostulanteCobeBLL();
                mngrInstances.Add("tbPostulanteCobeBLL", objtbPostulanteCobeBLL);
                return objtbPostulanteCobeBLL;
            }
        }

        public List<tbPostulanteCobeModel> GetAll()
        {
            using (tbPostulanteCobeDAL dal = new tbPostulanteCobeDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbPostulanteCobeModel GetBycnID_PostulanteCobe(Int64 cnID_PostulanteCobe)
        {
            using (tbPostulanteCobeDAL dal = new tbPostulanteCobeDAL(Transaction, Connection))
            {
                return dal.GetBycnID_PostulanteCobe(cnID_PostulanteCobe);
            }
        }

        public bool GetBycnID_PostulanteCobe(Int64 cnID_PostulanteCobe, tbPostulanteCobeModel result)
        {
            using (tbPostulanteCobeDAL dal = new tbPostulanteCobeDAL(Transaction, Connection))
            {
                return dal.GetBycnID_PostulanteCobe(cnID_PostulanteCobe, result);
            }
        }

        public void Update(tbPostulanteCobeModel model)
        {
            using (tbPostulanteCobeDAL dal = new tbPostulanteCobeDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int64 Insert(tbPostulanteCobeModel model)
        {
            using (tbPostulanteCobeDAL dal = new tbPostulanteCobeDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int64 cnID_PostulanteCobe)
        {
            using (tbPostulanteCobeDAL dal = new tbPostulanteCobeDAL(Transaction, Connection))
            {
                dal.Delete(cnID_PostulanteCobe);
            }
        }

        public List<tbPostulanteCobeModel> GetBycnID_Postulante(Int64 cnID_Postulante)
        {
            using (tbPostulanteCobeDAL dal = new tbPostulanteCobeDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Postulante(cnID_Postulante);
            }
        }

        public tbPostulanteModel GettbPostulanteBycnID_Postulante(Int64 cnID_Postulante)
        {
            using (tbPostulanteDAL dal = new tbPostulanteDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Postulante(cnID_Postulante);
            }
        }
    }

    /// <summary>
    /// Business logic for tbPostulanteDocu
    /// </summary>
    public partial class tbPostulanteDocuBLL : dbSeleccionBLLBase
    {
        internal tbPostulanteDocuBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbPostulanteDocuBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbPostulanteDocuBLL"))
                return (tbPostulanteDocuBLL)mngrInstances["tbPostulanteDocuBLL"];
            else
            {
                tbPostulanteDocuBLL objtbPostulanteDocuBLL = new tbPostulanteDocuBLL();
                mngrInstances.Add("tbPostulanteDocuBLL", objtbPostulanteDocuBLL);
                return objtbPostulanteDocuBLL;
            }
        }

        public List<tbPostulanteDocuModel> GetAll()
        {
            using (tbPostulanteDocuDAL dal = new tbPostulanteDocuDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbPostulanteDocuModel GetBycnID_PostulanteDocu(Int64 cnID_PostulanteDocu)
        {
            using (tbPostulanteDocuDAL dal = new tbPostulanteDocuDAL(Transaction, Connection))
            {
                return dal.GetBycnID_PostulanteDocu(cnID_PostulanteDocu);
            }
        }

        public bool GetBycnID_PostulanteDocu(Int64 cnID_PostulanteDocu, tbPostulanteDocuModel result)
        {
            using (tbPostulanteDocuDAL dal = new tbPostulanteDocuDAL(Transaction, Connection))
            {
                return dal.GetBycnID_PostulanteDocu(cnID_PostulanteDocu, result);
            }
        }

        public void Update(tbPostulanteDocuModel model)
        {
            using (tbPostulanteDocuDAL dal = new tbPostulanteDocuDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int64 Insert(tbPostulanteDocuModel model)
        {
            using (tbPostulanteDocuDAL dal = new tbPostulanteDocuDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int64 cnID_PostulanteDocu)
        {
            using (tbPostulanteDocuDAL dal = new tbPostulanteDocuDAL(Transaction, Connection))
            {
                dal.Delete(cnID_PostulanteDocu);
            }
        }

        public List<tbPostulanteDocuModel> GetBycnID_Postulante(Int64 cnID_Postulante)
        {
            using (tbPostulanteDocuDAL dal = new tbPostulanteDocuDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Postulante(cnID_Postulante);
            }
        }

        public tbPostulanteModel GettbPostulanteBycnID_Postulante(Int64 cnID_Postulante)
        {
            using (tbPostulanteDAL dal = new tbPostulanteDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Postulante(cnID_Postulante);
            }
        }
    }

    /// <summary>
    /// Business logic for tbPostulanteLabo
    /// </summary>
    public partial class tbPostulanteLaboBLL : dbSeleccionBLLBase
    {
        internal tbPostulanteLaboBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbPostulanteLaboBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbPostulanteLaboBLL"))
                return (tbPostulanteLaboBLL)mngrInstances["tbPostulanteLaboBLL"];
            else
            {
                tbPostulanteLaboBLL objtbPostulanteLaboBLL = new tbPostulanteLaboBLL();
                mngrInstances.Add("tbPostulanteLaboBLL", objtbPostulanteLaboBLL);
                return objtbPostulanteLaboBLL;
            }
        }

        public List<tbPostulanteLaboModel> GetAll()
        {
            using (tbPostulanteLaboDAL dal = new tbPostulanteLaboDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbPostulanteLaboModel GetBycnID_PostulanteLabo(Int64 cnID_PostulanteLabo)
        {
            using (tbPostulanteLaboDAL dal = new tbPostulanteLaboDAL(Transaction, Connection))
            {
                return dal.GetBycnID_PostulanteLabo(cnID_PostulanteLabo);
            }
        }

        public bool GetBycnID_PostulanteLabo(Int64 cnID_PostulanteLabo, tbPostulanteLaboModel result)
        {
            using (tbPostulanteLaboDAL dal = new tbPostulanteLaboDAL(Transaction, Connection))
            {
                return dal.GetBycnID_PostulanteLabo(cnID_PostulanteLabo, result);
            }
        }

        public void Update(tbPostulanteLaboModel model)
        {
            using (tbPostulanteLaboDAL dal = new tbPostulanteLaboDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int64 Insert(tbPostulanteLaboModel model)
        {
            using (tbPostulanteLaboDAL dal = new tbPostulanteLaboDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int64 cnID_PostulanteLabo)
        {
            using (tbPostulanteLaboDAL dal = new tbPostulanteLaboDAL(Transaction, Connection))
            {
                dal.Delete(cnID_PostulanteLabo);
            }
        }

        public List<tbPostulanteLaboModel> GetBycnID_Postulante(Int64 cnID_Postulante)
        {
            using (tbPostulanteLaboDAL dal = new tbPostulanteLaboDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Postulante(cnID_Postulante);
            }
        }

        public tbPostulanteModel GettbPostulanteBycnID_Postulante(Int64 cnID_Postulante)
        {
            using (tbPostulanteDAL dal = new tbPostulanteDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Postulante(cnID_Postulante);
            }
        }
    }

    /// <summary>
    /// Business logic for tbProceso
    /// </summary>
    public partial class tbProcesoBLL : dbSeleccionBLLBase
    {
        internal tbProcesoBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbProcesoBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbProcesoBLL"))
                return (tbProcesoBLL)mngrInstances["tbProcesoBLL"];
            else
            {
                tbProcesoBLL objtbProcesoBLL = new tbProcesoBLL();
                mngrInstances.Add("tbProcesoBLL", objtbProcesoBLL);
                return objtbProcesoBLL;
            }
        }

        public List<tbProcesoModel> GetAll()
        {
            using (tbProcesoDAL dal = new tbProcesoDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public List<tbProcesoModel> GetProcesosVigentes()
        {
            using (tbProcesoDAL dal = new tbProcesoDAL(Transaction, Connection))
            {
                return dal.GetProcesosVigentes();
            }
        }

        public tbProcesoModel GetBycnID_Proceso(Int64 cnID_Proceso)
        {
            using (tbProcesoDAL dal = new tbProcesoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Proceso(cnID_Proceso);
            }
        }

        public bool GetBycnID_Proceso(Int64 cnID_Proceso, tbProcesoModel result)
        {
            using (tbProcesoDAL dal = new tbProcesoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Proceso(cnID_Proceso, result);
            }
        }

        public void Update(tbProcesoModel model)
        {
            using (tbProcesoDAL dal = new tbProcesoDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int64 Insert(tbProcesoModel model)
        {
            using (tbProcesoDAL dal = new tbProcesoDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int64 cnID_Proceso)
        {
            using (tbProcesoDAL dal = new tbProcesoDAL(Transaction, Connection))
            {
                dal.Delete(cnID_Proceso);
            }
        }

        public List<tbPostulanteModel> GettbPostulanteBycnID_Proceso(Int64 cnID_Proceso)
        {
            using (tbPostulanteDAL dal = new tbPostulanteDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Proceso(cnID_Proceso);
            }
        }

        public List<tbProcesoModel> GetBycnID_Tipo(Int64 cnID_Tipo)
        {
            using (tbProcesoDAL dal = new tbProcesoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Tipo(cnID_Tipo);
            }
        }

        public tbMaestroListasModel GettbMaestroListasBycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }

        public List<tbProcesoModel> GetBycnID_Cobertura(Int32 cnID_Cobertura)
        {
            using (tbProcesoDAL dal = new tbProcesoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Cobertura(cnID_Cobertura);
            }
        }

        public tbProcesoCoberturaModel GettbProcesoCoberturaBycnID_Cobertura(Int32 cnID_Cobertura)
        {
            using (tbProcesoCoberturaDAL dal = new tbProcesoCoberturaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Cobertura(cnID_Cobertura);
            }
        }

        public List<tbProcesoModel> GetBycnID_Estado(Int64 cnID_Estado)
        {
            using (tbProcesoDAL dal = new tbProcesoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Estado(cnID_Estado);
            }
        }

        public tbMaestroListasModel GettbMaestroListas_BycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }

        public List<tbProcesoCnfgAcadModel> GettbProcesoCnfgAcadBycnID_Proceso(Int64 cnID_Proceso)
        {
            using (tbProcesoCnfgAcadDAL dal = new tbProcesoCnfgAcadDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Proceso(cnID_Proceso);
            }
        }

        public List<tbProcesoCnfgLaboModel> GettbProcesoCnfgLaboBycnID_Proceso(Int64 cnID_Proceso)
        {
            using (tbProcesoCnfgLaboDAL dal = new tbProcesoCnfgLaboDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Proceso(cnID_Proceso);
            }
        }

        public List<tbProcesoCriterioAdicModel> GettbProcesoCriterioAdicBycnID_Proceso(Int64 cnID_Proceso)
        {
            using (tbProcesoCriterioAdicDAL dal = new tbProcesoCriterioAdicDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Proceso(cnID_Proceso);
            }
        }

        public List<tbProcesoDocumentoModel> GettbProcesoDocumentoBycnID_Proceso(Int64 cnID_Proceso)
        {
            using (tbProcesoDocumentoDAL dal = new tbProcesoDocumentoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Proceso(cnID_Proceso);
            }
        }

        public List<tbProcesoMatrizLaboModel> GettbProcesoMatrizLaboBycnID_Proceso(Int64 cnID_Proceso)
        {
            using (tbProcesoMatrizLaboDAL dal = new tbProcesoMatrizLaboDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Proceso(cnID_Proceso);
            }
        }

        public List<tbProcesoModel> GetBycfFechaInicio(DateTime cfFechaInicio)
        {
            using (tbProcesoDAL dal = new tbProcesoDAL(Transaction, Connection))
            {
                return dal.GetBycfFechaInicio(cfFechaInicio);
            }
        }

        public List<tbProcesoModel> GetBycfFechaFin(DateTime cfFechaFin)
        {
            using (tbProcesoDAL dal = new tbProcesoDAL(Transaction, Connection))
            {
                return dal.GetBycfFechaFin(cfFechaFin);
            }
        }
    }

    /// <summary>
    /// Business logic for tbProcesoCnfgAcad
    /// </summary>
    public partial class tbProcesoCnfgAcadBLL : dbSeleccionBLLBase
    {
        internal tbProcesoCnfgAcadBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbProcesoCnfgAcadBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbProcesoCnfgAcadBLL"))
                return (tbProcesoCnfgAcadBLL)mngrInstances["tbProcesoCnfgAcadBLL"];
            else
            {
                tbProcesoCnfgAcadBLL objtbProcesoCnfgAcadBLL = new tbProcesoCnfgAcadBLL();
                mngrInstances.Add("tbProcesoCnfgAcadBLL", objtbProcesoCnfgAcadBLL);
                return objtbProcesoCnfgAcadBLL;
            }
        }

        public List<tbProcesoCnfgAcadModel> GetAll()
        {
            using (tbProcesoCnfgAcadDAL dal = new tbProcesoCnfgAcadDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbProcesoCnfgAcadModel GetBycnID_CnfgAcademica(Int32 cnID_CnfgAcademica)
        {
            using (tbProcesoCnfgAcadDAL dal = new tbProcesoCnfgAcadDAL(Transaction, Connection))
            {
                return dal.GetBycnID_CnfgAcademica(cnID_CnfgAcademica);
            }
        }

        public bool GetBycnID_CnfgAcademica(Int32 cnID_CnfgAcademica, tbProcesoCnfgAcadModel result)
        {
            using (tbProcesoCnfgAcadDAL dal = new tbProcesoCnfgAcadDAL(Transaction, Connection))
            {
                return dal.GetBycnID_CnfgAcademica(cnID_CnfgAcademica, result);
            }
        }

        public void Update(tbProcesoCnfgAcadModel model)
        {
            using (tbProcesoCnfgAcadDAL dal = new tbProcesoCnfgAcadDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int32 Insert(tbProcesoCnfgAcadModel model)
        {
            using (tbProcesoCnfgAcadDAL dal = new tbProcesoCnfgAcadDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int32 cnID_CnfgAcademica)
        {
            using (tbProcesoCnfgAcadDAL dal = new tbProcesoCnfgAcadDAL(Transaction, Connection))
            {
                dal.Delete(cnID_CnfgAcademica);
            }
        }

        public List<tbProcesoCnfgAcadModel> GetBycnID_Proceso(Int64 cnID_Proceso)
        {
            using (tbProcesoCnfgAcadDAL dal = new tbProcesoCnfgAcadDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Proceso(cnID_Proceso);
            }
        }

        public tbProcesoModel GettbProcesoBycnID_Proceso(Int64 cnID_Proceso)
        {
            using (tbProcesoDAL dal = new tbProcesoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Proceso(cnID_Proceso);
            }
        }

        public List<tbProcesoCnfgAcadModel> GetBycnID_Grado(Int64 cnID_Grado)
        {
            using (tbProcesoCnfgAcadDAL dal = new tbProcesoCnfgAcadDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Grado(cnID_Grado);
            }
        }

        public tbMaestroListasModel GettbMaestroListasBycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }

        public List<tbProcesoCnfgAcadModel> GetBycnID_Situacion(Int64 cnID_Situacion)
        {
            using (tbProcesoCnfgAcadDAL dal = new tbProcesoCnfgAcadDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Situacion(cnID_Situacion);
            }
        }

        public tbMaestroListasModel GettbMaestroListas_BycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }
    }

    /// <summary>
    /// Business logic for tbProcesoCnfgLabo
    /// </summary>
    public partial class tbProcesoCnfgLaboBLL : dbSeleccionBLLBase
    {
        internal tbProcesoCnfgLaboBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbProcesoCnfgLaboBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbProcesoCnfgLaboBLL"))
                return (tbProcesoCnfgLaboBLL)mngrInstances["tbProcesoCnfgLaboBLL"];
            else
            {
                tbProcesoCnfgLaboBLL objtbProcesoCnfgLaboBLL = new tbProcesoCnfgLaboBLL();
                mngrInstances.Add("tbProcesoCnfgLaboBLL", objtbProcesoCnfgLaboBLL);
                return objtbProcesoCnfgLaboBLL;
            }
        }

        public List<tbProcesoCnfgLaboModel> GetAll()
        {
            using (tbProcesoCnfgLaboDAL dal = new tbProcesoCnfgLaboDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbProcesoCnfgLaboModel GetBycnID_CnfgLaboral(Int32 cnID_CnfgLaboral)
        {
            using (tbProcesoCnfgLaboDAL dal = new tbProcesoCnfgLaboDAL(Transaction, Connection))
            {
                return dal.GetBycnID_CnfgLaboral(cnID_CnfgLaboral);
            }
        }

        public bool GetBycnID_CnfgLaboral(Int32 cnID_CnfgLaboral, tbProcesoCnfgLaboModel result)
        {
            using (tbProcesoCnfgLaboDAL dal = new tbProcesoCnfgLaboDAL(Transaction, Connection))
            {
                return dal.GetBycnID_CnfgLaboral(cnID_CnfgLaboral, result);
            }
        }

        public void Update(tbProcesoCnfgLaboModel model)
        {
            using (tbProcesoCnfgLaboDAL dal = new tbProcesoCnfgLaboDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int32 Insert(tbProcesoCnfgLaboModel model)
        {
            using (tbProcesoCnfgLaboDAL dal = new tbProcesoCnfgLaboDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int32 cnID_CnfgLaboral)
        {
            using (tbProcesoCnfgLaboDAL dal = new tbProcesoCnfgLaboDAL(Transaction, Connection))
            {
                dal.Delete(cnID_CnfgLaboral);
            }
        }

        public List<tbProcesoCnfgLaboModel> GetBycnID_Proceso(Int64 cnID_Proceso)
        {
            using (tbProcesoCnfgLaboDAL dal = new tbProcesoCnfgLaboDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Proceso(cnID_Proceso);
            }
        }

        public tbProcesoModel GettbProcesoBycnID_Proceso(Int64 cnID_Proceso)
        {
            using (tbProcesoDAL dal = new tbProcesoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Proceso(cnID_Proceso);
            }
        }

        public List<tbProcesoCnfgLaboModel> GetBycnID_Tipo(Int64 cnID_Tipo)
        {
            using (tbProcesoCnfgLaboDAL dal = new tbProcesoCnfgLaboDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Tipo(cnID_Tipo);
            }
        }

        public tbMaestroListasModel GettbMaestroListasBycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }

        public List<tbProcesoMatrizLaboModel> GettbProcesoMatrizLaboBycnID_CnfgLaboral(Int32 cnID_CnfgLaboral)
        {
            using (tbProcesoMatrizLaboDAL dal = new tbProcesoMatrizLaboDAL(Transaction, Connection))
            {
                return dal.GetBycnID_CnfgLaboral(cnID_CnfgLaboral);
            }
        }
    }

    /// <summary>
    /// Business logic for tbProcesoCobertura
    /// </summary>
    public partial class tbProcesoCoberturaBLL : dbSeleccionBLLBase
    {
        internal tbProcesoCoberturaBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbProcesoCoberturaBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbProcesoCoberturaBLL"))
                return (tbProcesoCoberturaBLL)mngrInstances["tbProcesoCoberturaBLL"];
            else
            {
                tbProcesoCoberturaBLL objtbProcesoCoberturaBLL = new tbProcesoCoberturaBLL();
                mngrInstances.Add("tbProcesoCoberturaBLL", objtbProcesoCoberturaBLL);
                return objtbProcesoCoberturaBLL;
            }
        }

        public List<tbProcesoCoberturaModel> GetAll()
        {
            using (tbProcesoCoberturaDAL dal = new tbProcesoCoberturaDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public List<tbProcesoCoberturaModel> GetBycnID_Proceso(long cnID_Proceso)
        {
            using (tbProcesoCoberturaDAL dal = new tbProcesoCoberturaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Proceso(cnID_Proceso);
            }
        }

        public tbProcesoCoberturaModel GetBycnID_Cobertura(Int32 cnID_Cobertura)
        {
            using (tbProcesoCoberturaDAL dal = new tbProcesoCoberturaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Cobertura(cnID_Cobertura);
            }
        }

        public bool GetBycnID_Cobertura(Int32 cnID_Cobertura, tbProcesoCoberturaModel result)
        {
            using (tbProcesoCoberturaDAL dal = new tbProcesoCoberturaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Cobertura(cnID_Cobertura, result);
            }
        }

        public void Update(tbProcesoCoberturaModel model)
        {
            using (tbProcesoCoberturaDAL dal = new tbProcesoCoberturaDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int32 Insert(tbProcesoCoberturaModel model)
        {
            using (tbProcesoCoberturaDAL dal = new tbProcesoCoberturaDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int32 cnID_Cobertura)
        {
            using (tbProcesoCoberturaDAL dal = new tbProcesoCoberturaDAL(Transaction, Connection))
            {
                dal.Delete(cnID_Cobertura);
            }
        }

        public List<tbProcesoModel> GettbProcesoBycnID_Cobertura(Int32 cnID_Cobertura)
        {
            using (tbProcesoDAL dal = new tbProcesoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Cobertura(cnID_Cobertura);
            }
        }
    }

    /// <summary>
    /// Business logic for tbProcesoCriterioAdic
    /// </summary>
    public partial class tbProcesoCriterioAdicBLL : dbSeleccionBLLBase
    {
        internal tbProcesoCriterioAdicBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbProcesoCriterioAdicBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbProcesoCriterioAdicBLL"))
                return (tbProcesoCriterioAdicBLL)mngrInstances["tbProcesoCriterioAdicBLL"];
            else
            {
                tbProcesoCriterioAdicBLL objtbProcesoCriterioAdicBLL = new tbProcesoCriterioAdicBLL();
                mngrInstances.Add("tbProcesoCriterioAdicBLL", objtbProcesoCriterioAdicBLL);
                return objtbProcesoCriterioAdicBLL;
            }
        }

        public List<tbProcesoCriterioAdicModel> GetAll()
        {
            using (tbProcesoCriterioAdicDAL dal = new tbProcesoCriterioAdicDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbProcesoCriterioAdicModel GetBycnID_CriterioAdicional(Int32 cnID_CriterioAdicional)
        {
            using (tbProcesoCriterioAdicDAL dal = new tbProcesoCriterioAdicDAL(Transaction, Connection))
            {
                return dal.GetBycnID_CriterioAdicional(cnID_CriterioAdicional);
            }
        }

        public bool GetBycnID_CriterioAdicional(Int32 cnID_CriterioAdicional, tbProcesoCriterioAdicModel result)
        {
            using (tbProcesoCriterioAdicDAL dal = new tbProcesoCriterioAdicDAL(Transaction, Connection))
            {
                return dal.GetBycnID_CriterioAdicional(cnID_CriterioAdicional, result);
            }
        }

        public void Update(tbProcesoCriterioAdicModel model)
        {
            using (tbProcesoCriterioAdicDAL dal = new tbProcesoCriterioAdicDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int32 Insert(tbProcesoCriterioAdicModel model)
        {
            using (tbProcesoCriterioAdicDAL dal = new tbProcesoCriterioAdicDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int32 cnID_CriterioAdicional)
        {
            using (tbProcesoCriterioAdicDAL dal = new tbProcesoCriterioAdicDAL(Transaction, Connection))
            {
                dal.Delete(cnID_CriterioAdicional);
            }
        }

        public List<tbProcesoCriterioAdicModel> GetBycnID_Proceso(Int64 cnID_Proceso)
        {
            using (tbProcesoCriterioAdicDAL dal = new tbProcesoCriterioAdicDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Proceso(cnID_Proceso);
            }
        }

        public tbProcesoModel GettbProcesoBycnID_Proceso(Int64 cnID_Proceso)
        {
            using (tbProcesoDAL dal = new tbProcesoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Proceso(cnID_Proceso);
            }
        }

        public List<tbProcesoCriterioAdicModel> GetByctDescripcion(String ctDescripcion)
        {
            using (tbProcesoCriterioAdicDAL dal = new tbProcesoCriterioAdicDAL(Transaction, Connection))
            {
                return dal.GetByctDescripcion(ctDescripcion);
            }
        }
    }

    /// <summary>
    /// Business logic for tbProcesoDocumento
    /// </summary>
    public partial class tbProcesoDocumentoBLL : dbSeleccionBLLBase
    {
        internal tbProcesoDocumentoBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbProcesoDocumentoBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbProcesoDocumentoBLL"))
                return (tbProcesoDocumentoBLL)mngrInstances["tbProcesoDocumentoBLL"];
            else
            {
                tbProcesoDocumentoBLL objtbProcesoDocumentoBLL = new tbProcesoDocumentoBLL();
                mngrInstances.Add("tbProcesoDocumentoBLL", objtbProcesoDocumentoBLL);
                return objtbProcesoDocumentoBLL;
            }
        }

        public List<tbProcesoDocumentoModel> GetAll()
        {
            using (tbProcesoDocumentoDAL dal = new tbProcesoDocumentoDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbProcesoDocumentoModel GetBycnID_Documento(Int32 cnID_Documento)
        {
            using (tbProcesoDocumentoDAL dal = new tbProcesoDocumentoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Documento(cnID_Documento);
            }
        }

        public bool GetBycnID_Documento(Int32 cnID_Documento, tbProcesoDocumentoModel result)
        {
            using (tbProcesoDocumentoDAL dal = new tbProcesoDocumentoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Documento(cnID_Documento, result);
            }
        }

        public void Update(tbProcesoDocumentoModel model)
        {
            using (tbProcesoDocumentoDAL dal = new tbProcesoDocumentoDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int32 Insert(tbProcesoDocumentoModel model)
        {
            using (tbProcesoDocumentoDAL dal = new tbProcesoDocumentoDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int32 cnID_Documento)
        {
            using (tbProcesoDocumentoDAL dal = new tbProcesoDocumentoDAL(Transaction, Connection))
            {
                dal.Delete(cnID_Documento);
            }
        }

        public List<tbProcesoDocumentoModel> GetBycnID_Proceso(Int64 cnID_Proceso)
        {
            using (tbProcesoDocumentoDAL dal = new tbProcesoDocumentoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Proceso(cnID_Proceso);
            }
        }

        public tbProcesoModel GettbProcesoBycnID_Proceso(Int64 cnID_Proceso)
        {
            using (tbProcesoDAL dal = new tbProcesoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Proceso(cnID_Proceso);
            }
        }
    }

    /// <summary>
    /// Business logic for tbProcesoMatrizLabo
    /// </summary>
    public partial class tbProcesoMatrizLaboBLL : dbSeleccionBLLBase
    {
        internal tbProcesoMatrizLaboBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbProcesoMatrizLaboBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbProcesoMatrizLaboBLL"))
                return (tbProcesoMatrizLaboBLL)mngrInstances["tbProcesoMatrizLaboBLL"];
            else
            {
                tbProcesoMatrizLaboBLL objtbProcesoMatrizLaboBLL = new tbProcesoMatrizLaboBLL();
                mngrInstances.Add("tbProcesoMatrizLaboBLL", objtbProcesoMatrizLaboBLL);
                return objtbProcesoMatrizLaboBLL;
            }
        }

        public List<tbProcesoMatrizLaboModel> GetAll()
        {
            using (tbProcesoMatrizLaboDAL dal = new tbProcesoMatrizLaboDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbProcesoMatrizLaboModel GetBycnID_MatrizLaboral(Int32 cnID_MatrizLaboral)
        {
            using (tbProcesoMatrizLaboDAL dal = new tbProcesoMatrizLaboDAL(Transaction, Connection))
            {
                return dal.GetBycnID_MatrizLaboral(cnID_MatrizLaboral);
            }
        }

        public bool GetBycnID_MatrizLaboral(Int32 cnID_MatrizLaboral, tbProcesoMatrizLaboModel result)
        {
            using (tbProcesoMatrizLaboDAL dal = new tbProcesoMatrizLaboDAL(Transaction, Connection))
            {
                return dal.GetBycnID_MatrizLaboral(cnID_MatrizLaboral, result);
            }
        }

        public void Update(tbProcesoMatrizLaboModel model)
        {
            using (tbProcesoMatrizLaboDAL dal = new tbProcesoMatrizLaboDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int32 Insert(tbProcesoMatrizLaboModel model)
        {
            using (tbProcesoMatrizLaboDAL dal = new tbProcesoMatrizLaboDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int32 cnID_MatrizLaboral)
        {
            using (tbProcesoMatrizLaboDAL dal = new tbProcesoMatrizLaboDAL(Transaction, Connection))
            {
                dal.Delete(cnID_MatrizLaboral);
            }
        }

        public List<tbProcesoMatrizLaboModel> GetBycnID_CnfgLaboral(Int32 cnID_CnfgLaboral)
        {
            using (tbProcesoMatrizLaboDAL dal = new tbProcesoMatrizLaboDAL(Transaction, Connection))
            {
                return dal.GetBycnID_CnfgLaboral(cnID_CnfgLaboral);
            }
        }

        public tbProcesoCnfgLaboModel GettbProcesoCnfgLaboBycnID_CnfgLaboral(Int32 cnID_CnfgLaboral)
        {
            using (tbProcesoCnfgLaboDAL dal = new tbProcesoCnfgLaboDAL(Transaction, Connection))
            {
                return dal.GetBycnID_CnfgLaboral(cnID_CnfgLaboral);
            }
        }

        public List<tbProcesoMatrizLaboModel> GetBycnID_Proceso(Int64 cnID_Proceso)
        {
            using (tbProcesoMatrizLaboDAL dal = new tbProcesoMatrizLaboDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Proceso(cnID_Proceso);
            }
        }

        public tbProcesoModel GettbProcesoBycnID_Proceso(Int64 cnID_Proceso)
        {
            using (tbProcesoDAL dal = new tbProcesoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Proceso(cnID_Proceso);
            }
        }

        public List<tbProcesoMatrizLaboModel> GetBycnID_TipoEvaluacion(Int64 cnID_TipoEvaluacion)
        {
            using (tbProcesoMatrizLaboDAL dal = new tbProcesoMatrizLaboDAL(Transaction, Connection))
            {
                return dal.GetBycnID_TipoEvaluacion(cnID_TipoEvaluacion);
            }
        }

        public tbMaestroListasModel GettbMaestroListasBycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }
    }

    /// <summary>
    /// Business logic for tbProvincia
    /// </summary>
    public partial class tbProvinciaBLL : dbSeleccionBLLBase
    {
        internal tbProvinciaBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbProvinciaBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbProvinciaBLL"))
                return (tbProvinciaBLL)mngrInstances["tbProvinciaBLL"];
            else
            {
                tbProvinciaBLL objtbProvinciaBLL = new tbProvinciaBLL();
                mngrInstances.Add("tbProvinciaBLL", objtbProvinciaBLL);
                return objtbProvinciaBLL;
            }
        }

        public List<tbProvinciaModel> GetAll()
        {
            using (tbProvinciaDAL dal = new tbProvinciaDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbProvinciaModel GetBycnID_Provincia(Int32 cnID_Provincia)
        {
            using (tbProvinciaDAL dal = new tbProvinciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Provincia(cnID_Provincia);
            }
        }

        public bool GetBycnID_Provincia(Int32 cnID_Provincia, tbProvinciaModel result)
        {
            using (tbProvinciaDAL dal = new tbProvinciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Provincia(cnID_Provincia, result);
            }
        }

        public void Update(tbProvinciaModel model)
        {
            using (tbProvinciaDAL dal = new tbProvinciaDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int32 Insert(tbProvinciaModel model)
        {
            using (tbProvinciaDAL dal = new tbProvinciaDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int32 cnID_Provincia)
        {
            using (tbProvinciaDAL dal = new tbProvinciaDAL(Transaction, Connection))
            {
                dal.Delete(cnID_Provincia);
            }
        }

        public List<tbCandidatoModel> GettbCandidatoBycnID_Provincia(Int32 cnID_Provincia)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Provincia(cnID_Provincia);
            }
        }

        public List<tbDistritoModel> GettbDistritoBycnID_Provincia(Int32 cnID_Provincia)
        {
            using (tbDistritoDAL dal = new tbDistritoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Provincia(cnID_Provincia);
            }
        }

        public List<tbProvinciaModel> GetBycnID_Departamento(Int32 cnID_Departamento)
        {
            using (tbProvinciaDAL dal = new tbProvinciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Departamento(cnID_Departamento);
            }
        }

        public tbDepartamentoModel GettbDepartamentoBycnID_Departamento(Int32 cnID_Departamento)
        {
            using (tbDepartamentoDAL dal = new tbDepartamentoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Departamento(cnID_Departamento);
            }
        }
    }

    /// <summary>
    /// Business logic for tbUsuario
    /// </summary>
    public partial class tbUsuarioBLL : dbSeleccionBLLBase
    {
        internal tbUsuarioBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbUsuarioBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbUsuarioBLL"))
                return (tbUsuarioBLL)mngrInstances["tbUsuarioBLL"];
            else
            {
                tbUsuarioBLL objtbUsuarioBLL = new tbUsuarioBLL();
                mngrInstances.Add("tbUsuarioBLL", objtbUsuarioBLL);
                return objtbUsuarioBLL;
            }
        }

        public List<tbUsuarioModel> GetAll()
        {
            using (tbUsuarioDAL dal = new tbUsuarioDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbUsuarioModel GetBycnID_Usuario(Int64 cnID_Usuario)
        {
            using (tbUsuarioDAL dal = new tbUsuarioDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Usuario(cnID_Usuario);
            }
        }

        public bool GetBycnID_Usuario(Int64 cnID_Usuario, tbUsuarioModel result)
        {
            using (tbUsuarioDAL dal = new tbUsuarioDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Usuario(cnID_Usuario, result);
            }
        }

        public void Update(tbUsuarioModel model)
        {
            using (tbUsuarioDAL dal = new tbUsuarioDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int32 Insert(tbUsuarioModel model)
        {
            using (tbUsuarioDAL dal = new tbUsuarioDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int64 cnID_Usuario)
        {
            using (tbUsuarioDAL dal = new tbUsuarioDAL(Transaction, Connection))
            {
                dal.Delete(cnID_Usuario);
            }
        }

        public List<tbUsuarioModel> GetBycnID_Perfil(Int32 cnID_Perfil)
        {
            using (tbUsuarioDAL dal = new tbUsuarioDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Perfil(cnID_Perfil);
            }
        }

        public tbPerfilUsuarioModel GettbPerfilUsuarioBycnID_Perfil(Int32 cnID_Perfil)
        {
            using (tbPerfilUsuarioDAL dal = new tbPerfilUsuarioDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Perfil(cnID_Perfil);
            }
        }

        public tbUsuarioModel GetByctLogin(String ctLogin)
        {
            using (tbUsuarioDAL dal = new tbUsuarioDAL(Transaction, Connection))
            {
                return dal.GetByctLogin(ctLogin);
            }
        }

        public tbUsuarioModel GetByctCodigoRecuperacion(String tcCodigoRecuperacion)
        {
            using (tbUsuarioDAL dal = new tbUsuarioDAL(Transaction, Connection))
            {
                return dal.GetBytcCodigoRecuperacion(tcCodigoRecuperacion);
            }
        }

        public void DeleteByctLogin(String ctLogin)
        {
            using (tbUsuarioDAL dal = new tbUsuarioDAL(Transaction, Connection))
            {
                dal.DeleteByctLogin(ctLogin);
            }
        }
    }

    /// <summary>
    /// Business logic for tbAcademico
    /// </summary>
    public partial class tbAcademicoBLL : dbSeleccionBLLBase
    {
        internal tbAcademicoBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbAcademicoBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbAcademicoBLL"))
                return (tbAcademicoBLL)mngrInstances["tbAcademicoBLL"];
            else
            {
                tbAcademicoBLL objtbAcademicoBLL = new tbAcademicoBLL();
                mngrInstances.Add("tbAcademicoBLL", objtbAcademicoBLL);
                return objtbAcademicoBLL;
            }
        }

        public List<tbAcademicoModel> GetAll()
        {
            using (tbAcademicoDAL dal = new tbAcademicoDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbAcademicoModel GetBycnID_Academico(Int64 cnID_Academico)
        {
            using (tbAcademicoDAL dal = new tbAcademicoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Academico(cnID_Academico);
            }
        }

        public bool GetBycnID_Academico(Int64 cnID_Academico, tbAcademicoModel result)
        {
            using (tbAcademicoDAL dal = new tbAcademicoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Academico(cnID_Academico, result);
            }
        }

        public void Update(tbAcademicoModel model)
        {
            using (tbAcademicoDAL dal = new tbAcademicoDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public void UpdateBloque(tbAcademicoModel model)
        {
            using (tbAcademicoDAL dal = new tbAcademicoDAL(Transaction, Connection))
            {
                dal.UpdateBloque(model);
            }
        }

        public int Insert(tbAcademicoModel model)
        {
            using (tbAcademicoDAL dal = new tbAcademicoDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public int InsertBloque(tbAcademicoModel model, int idbloque)
        {
            using (tbAcademicoDAL dal = new tbAcademicoDAL(Transaction, Connection))
            {
                return dal.InsertBloque(model,idbloque);
            }
        }

        public Documents ListDoceumnts(int Candidato, int Bloque)
        {
            using (tbAcademicoDAL dal = new tbAcademicoDAL(Transaction, Connection))
            {
                return dal.ListDocument(Candidato, Bloque);
            }
        }

        public void Delete(Int64 cnID_Academico)
        {
            using (tbAcademicoDAL dal = new tbAcademicoDAL(Transaction, Connection))
            {
                dal.Delete(cnID_Academico);
            }
        }

        public void DeleteByCandidato(Int64 cnID_Candidato)
        {
            using (tbAcademicoDAL dal = new tbAcademicoDAL(Transaction, Connection))
            {
                dal.DeleteByIdCandidato(cnID_Candidato);
            }
        }

        public List<tbAcademicoModel> GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbAcademicoDAL dal = new tbAcademicoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }

        public List<tbAcademicoModel> GetBycnID_CandidatoMasivo(Int64 cnID_Candidato)
        {
            using (tbAcademicoDAL dal = new tbAcademicoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_CandidatoMasivo(cnID_Candidato);
            }
        }

        public tbCandidatoModel GettbCandidatoBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbCandidatoDAL dal = new tbCandidatoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }

        public List<tbAcademicoModel> GetBycnID_Grado(Int64 cnID_Grado)
        {
            using (tbAcademicoDAL dal = new tbAcademicoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Grado(cnID_Grado);
            }
        }

        public tbMaestroListasModel GettbMaestroListasBycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }

        public List<tbAcademicoModel> GetBycnID_Carrera(Int64 cnID_Carrera)
        {
            using (tbAcademicoDAL dal = new tbAcademicoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Carrera(cnID_Carrera);
            }
        }

        public tbMaestroListasModel GettbMaestroListas_BycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }

        public List<tbAcademicoModel> GetBycnID_Institucion(Int64 cnID_Institucion)
        {
            using (tbAcademicoDAL dal = new tbAcademicoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Institucion(cnID_Institucion);
            }
        }

        public tbMaestroListasModel GettbMaestroListas_1BycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }

        public List<tbAcademicoModel> GetBycnID_Situacion(Int64 cnID_Situacion)
        {
            using (tbAcademicoDAL dal = new tbAcademicoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Situacion(cnID_Situacion);
            }
        }

        public tbMaestroListasModel GettbMaestroListas_2BycnID_Lista(Int64 cnID_Lista)
        {
            using (tbMaestroListasDAL dal = new tbMaestroListasDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Lista(cnID_Lista);
            }
        }

        public List<tbAcademicoModel> GetBycnID_Pais(Int32 cnID_Pais)
        {
            using (tbAcademicoDAL dal = new tbAcademicoDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Pais(cnID_Pais);
            }
        }

        public tbPaisModel GettbPaisBycnID_Pais(Int32 cnID_Pais)
        {
            using (tbPaisDAL dal = new tbPaisDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Pais(cnID_Pais);
            }
        }
    }

    /// <summary>
    /// Business logic for tbComplementario
    /// </summary>
    public partial class tbComplementarioBLL : dbSeleccionBLLBase
    {
        internal tbComplementarioBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbComplementarioBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbComplementarioBLL"))
                return (tbComplementarioBLL)mngrInstances["tbComplementarioBLL"];
            else
            {
                tbComplementarioBLL objtbComplementarioBLL = new tbComplementarioBLL();
                mngrInstances.Add("tbComplementarioBLL", objtbComplementarioBLL);
                return objtbComplementarioBLL;
            }
        }

        public List<tbComplementarioModel> GetAll()
        {
            using (tbComplementarioDAL dal = new tbComplementarioDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbComplementarioModel GetBycnID_Complementario(Int64 cnID_Complementario)
        {
            using (tbComplementarioDAL dal = new tbComplementarioDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Complementario(cnID_Complementario);
            }
        }

        public bool GetBycnID_Complementario(Int64 cnID_Complementario, tbComplementarioModel result)
        {
            using (tbComplementarioDAL dal = new tbComplementarioDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Complementario(cnID_Complementario, result);
            }
        }

        public void Update(tbComplementarioModel model)
        {
            using (tbComplementarioDAL dal = new tbComplementarioDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int64 Insert(tbComplementarioModel model)
        {
            using (tbComplementarioDAL dal = new tbComplementarioDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int64 cnID_Complementario)
        {
            using (tbComplementarioDAL dal = new tbComplementarioDAL(Transaction, Connection))
            {
                dal.Delete(cnID_Complementario);
            }
        }

        public List<tbComplementarioModel> GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbComplementarioDAL dal = new tbComplementarioDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }
    }

    /// <summary>
    /// Business logic for tbReferencia
    /// </summary>
    public partial class tbReferenciaBLL : dbSeleccionBLLBase
    {
        internal tbReferenciaBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbReferenciaBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbReferenciaBLL"))
                return (tbReferenciaBLL)mngrInstances["tbReferenciaBLL"];
            else
            {
                tbReferenciaBLL objtbReferenciaBLL = new tbReferenciaBLL();
                mngrInstances.Add("tbReferenciaBLL", objtbReferenciaBLL);
                return objtbReferenciaBLL;
            }
        }

        public List<tbReferenciaModel> GetAll()
        {
            using (tbReferenciaDAL dal = new tbReferenciaDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbReferenciaModel GetBycnID_Referencia(Int64 cnID_Referencia)
        {
            using (tbReferenciaDAL dal = new tbReferenciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Referencia(cnID_Referencia);
            }
        }

        public bool GetBycnID_Referencia(Int64 cnID_Referencia, tbReferenciaModel result)
        {
            using (tbReferenciaDAL dal = new tbReferenciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Referencia(cnID_Referencia, result);
            }
        }

        public void Update(tbReferenciaModel model)
        {
            using (tbReferenciaDAL dal = new tbReferenciaDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int64 Insert(tbReferenciaModel model)
        {
            using (tbReferenciaDAL dal = new tbReferenciaDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int64 cnID_Referencia)
        {
            using (tbReferenciaDAL dal = new tbReferenciaDAL(Transaction, Connection))
            {
                dal.Delete(cnID_Referencia);
            }
        }

        public List<tbReferenciaModel> GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbReferenciaDAL dal = new tbReferenciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }
    }

    /// <summary>
    /// Business logic for tbPostulanteCrit
    /// </summary>
    public partial class tbPostulanteCritBLL : dbSeleccionBLLBase
    {
        internal tbPostulanteCritBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbPostulanteCritBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbPostulanteCritBLL"))
                return (tbPostulanteCritBLL)mngrInstances["tbPostulanteCritBLL"];
            else
            {
                tbPostulanteCritBLL objtbPostulanteCritBLL = new tbPostulanteCritBLL();
                mngrInstances.Add("tbPostulanteCritBLL", objtbPostulanteCritBLL);
                return objtbPostulanteCritBLL;
            }
        }

        public List<tbPostulanteCritModel> GetAll()
        {
            using (tbPostulanteCritDAL dal = new tbPostulanteCritDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbPostulanteCritModel GetBycnID_PostulanteCrit(Int64 cnID_PostulanteCrit)
        {
            using (tbPostulanteCritDAL dal = new tbPostulanteCritDAL(Transaction, Connection))
            {
                return dal.GetBycnID_PostulanteCrit(cnID_PostulanteCrit);
            }
        }

        public bool GetBycnID_PostulanteCrit(Int64 cnID_PostulanteCrit, tbPostulanteCritModel result)
        {
            using (tbPostulanteCritDAL dal = new tbPostulanteCritDAL(Transaction, Connection))
            {
                return dal.GetBycnID_PostulanteCrit(cnID_PostulanteCrit, result);
            }
        }

        public void Update(tbPostulanteCritModel model)
        {
            using (tbPostulanteCritDAL dal = new tbPostulanteCritDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int64 Insert(tbPostulanteCritModel model)
        {
            using (tbPostulanteCritDAL dal = new tbPostulanteCritDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int64 cnID_PostulanteCrit)
        {
            using (tbPostulanteCritDAL dal = new tbPostulanteCritDAL(Transaction, Connection))
            {
                dal.Delete(cnID_PostulanteCrit);
            }
        }
    }

    /// <summary>
    /// Business logic for tbCoberturaZona
    /// </summary>
    public partial class tbCoberturaZonaBLL : dbSeleccionBLLBase
    {
        internal tbCoberturaZonaBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbCoberturaZonaBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbCoberturaZonaBLL"))
                return (tbCoberturaZonaBLL)mngrInstances["tbCoberturaZonaBLL"];
            else
            {
                tbCoberturaZonaBLL objtbCoberturaZonaBLL = new tbCoberturaZonaBLL();
                mngrInstances.Add("tbCoberturaZonaBLL", objtbCoberturaZonaBLL);
                return objtbCoberturaZonaBLL;
            }
        }

        public List<tbCoberturaZonaModel> GetAll()
        {
            using (tbCoberturaZonaDAL dal = new tbCoberturaZonaDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public List<tbCoberturaZonaModel> GetBycnID_Cobertura(Int64 cnID_Cobertura)
        {
            using (tbCoberturaZonaDAL dal = new tbCoberturaZonaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Cobertura(cnID_Cobertura);
            }
        }


        public tbCoberturaZonaModel GetBycnID_CoberturaZona(Int64 cnID_CoberturaZona)
        {
            using (tbCoberturaZonaDAL dal = new tbCoberturaZonaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_CoberturaZona(cnID_CoberturaZona);
            }
        }

        public bool GetBycnID_CoberturaZona(Int64 cnID_CoberturaZona, tbCoberturaZonaModel result)
        {
            using (tbCoberturaZonaDAL dal = new tbCoberturaZonaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_CoberturaZona(cnID_CoberturaZona, result);
            }
        }

        public void Update(tbCoberturaZonaModel model)
        {
            using (tbCoberturaZonaDAL dal = new tbCoberturaZonaDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int64 Insert(tbCoberturaZonaModel model)
        {
            using (tbCoberturaZonaDAL dal = new tbCoberturaZonaDAL(Transaction, Connection))
            {
                return dal.Insert(model);
            }
        }

        public void Delete(Int64 cnID_CoberturaZona)
        {
            using (tbCoberturaZonaDAL dal = new tbCoberturaZonaDAL(Transaction, Connection))
            {
                dal.Delete(cnID_CoberturaZona);
            }
        }
    }

    /// <summary>
    /// Business logic for tbPostulanteAcad
    /// </summary>
    public partial class tbPostulanteAcadBLL : dbSeleccionBLLBase
    {
        internal tbPostulanteAcadBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbPostulanteAcadBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbPostulanteAcadBLL"))
                return (tbPostulanteAcadBLL)mngrInstances["tbPostulanteAcadBLL"];
            else
            {
                tbPostulanteAcadBLL objtbPostulanteAcadBLL = new tbPostulanteAcadBLL();
                mngrInstances.Add("tbPostulanteAcadBLL", objtbPostulanteAcadBLL);
                return objtbPostulanteAcadBLL;
            }
        }

        public List<tbPostulanteAcadModel> GetAll()
        {
            using (tbPostulanteAcadDAL dal = new tbPostulanteAcadDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbPostulanteAcadModel GetBycnID_PostulanteAcad(Int64 cnID_PostulanteAcad)
        {
            using (tbPostulanteAcadDAL dal = new tbPostulanteAcadDAL(Transaction, Connection))
            {
                return dal.GetBycnID_PostulanteAcad(cnID_PostulanteAcad);
            }
        }

        public List<tbPostulanteAcadModel> GetBycnID_Postulante(Int64 cnID_Postulante)
        {
            using (tbPostulanteAcadDAL dal = new tbPostulanteAcadDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Postulante(cnID_Postulante);
            }
        }

        public bool GetBycnID_PostulanteAcad(Int64 cnID_PostulanteAcad, tbPostulanteAcadModel result)
        {
            using (tbPostulanteAcadDAL dal = new tbPostulanteAcadDAL(Transaction, Connection))
            {
                return dal.GetBycnID_PostulanteAcad(cnID_PostulanteAcad, result);
            }
        }

        public void Update(tbPostulanteAcadModel model)
        {
            using (tbPostulanteAcadDAL dal = new tbPostulanteAcadDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int64 Insert(tbPostulanteAcadModel model)
        {
            using (tbPostulanteAcadDAL dal = new tbPostulanteAcadDAL(Transaction, Connection))
            {

                return dal.Insert(model);
            }
        }

        public void Delete(Int64 cnID_PostulanteAcad)
        {
            using (tbPostulanteAcadDAL dal = new tbPostulanteAcadDAL(Transaction, Connection))
            {
                dal.Delete(cnID_PostulanteAcad);
            }
        }



    }

    /// <summary>
    /// Business logic for tbPostulanteEvalManual
    /// </summary>
    public partial class tbPostulanteEvalManualBLL : dbSeleccionBLLBase
    {
        internal tbPostulanteEvalManualBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbPostulanteEvalManualBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbPostulanteEvalManualBLL"))
                return (tbPostulanteEvalManualBLL)mngrInstances["tbPostulanteEvalManualBLL"];
            else
            {
                tbPostulanteEvalManualBLL objtbPostulanteEvalManualBLL = new tbPostulanteEvalManualBLL();
                mngrInstances.Add("tbPostulanteEvalManualBLL", objtbPostulanteEvalManualBLL);
                return objtbPostulanteEvalManualBLL;
            }
        }

        public List<tbPostulanteEvalManualModel> GetAll()
        {
            using (tbPostulanteEvalManualDAL dal = new tbPostulanteEvalManualDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public List<tbPostulanteEvalManualModel> GetBycnPostulanteEvalManual_GetBycnID_Postulante(long cnID_Postulante)
        {
            using (tbPostulanteEvalManualDAL dal = new tbPostulanteEvalManualDAL(Transaction, Connection))
            {
                return dal.GetBycnPostulanteEvalManual_GetBycnID_Postulante(cnID_Postulante);
            }
        }



        public tbPostulanteEvalManualModel GetBycnPostulanteEvalManual(Int64 cnPostulanteEvalManual)
        {
            using (tbPostulanteEvalManualDAL dal = new tbPostulanteEvalManualDAL(Transaction, Connection))
            {
                return dal.GetBycnPostulanteEvalManual(cnPostulanteEvalManual);
            }
        }

        public bool GetBycnPostulanteEvalManual(Int64 cnPostulanteEvalManual, tbPostulanteEvalManualModel result)
        {
            using (tbPostulanteEvalManualDAL dal = new tbPostulanteEvalManualDAL(Transaction, Connection))
            {
                return dal.GetBycnPostulanteEvalManual(cnPostulanteEvalManual, result);
            }
        }

        public void Update(tbPostulanteEvalManualModel model)
        {
            using (tbPostulanteEvalManualDAL dal = new tbPostulanteEvalManualDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int64 Insert(tbPostulanteEvalManualModel model)
        {
            using (tbPostulanteEvalManualDAL dal = new tbPostulanteEvalManualDAL(Transaction, Connection))
            {

                return dal.Insert(model);
            }
        }

        public void Delete(Int64 cnPostulanteEvalManual)
        {
            using (tbPostulanteEvalManualDAL dal = new tbPostulanteEvalManualDAL(Transaction, Connection))
            {
                dal.Delete(cnPostulanteEvalManual);
            }
        }



    }

    /// <summary>
    /// Business logic for tbExperienciaDocencia
    /// </summary>
    public partial class tbExperienciaDocenciaBLL : dbSeleccionBLLBase
    {
        internal tbExperienciaDocenciaBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbExperienciaDocenciaBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbExperienciaDocenciaBLL"))
                return (tbExperienciaDocenciaBLL)mngrInstances["tbExperienciaDocenciaBLL"];
            else
            {
                tbExperienciaDocenciaBLL objtbExperienciaDocenciaBLL = new tbExperienciaDocenciaBLL();
                mngrInstances.Add("tbExperienciaDocenciaBLL", objtbExperienciaDocenciaBLL);
                return objtbExperienciaDocenciaBLL;
            }
        }

        public List<tbExperienciaDocenciaModel> GetAll()
        {
            using (tbExperienciaDocenciaDAL dal = new tbExperienciaDocenciaDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public List<tbExperienciaDocenciaModel> GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbExperienciaDocenciaDAL dal = new tbExperienciaDocenciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }

        public tbExperienciaDocenciaModel GetBycnID_ExperienciaDocencia(Int64 cnID_ExperienciaDocencia)
        {
            using (tbExperienciaDocenciaDAL dal = new tbExperienciaDocenciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_ExperienciaDocencia(cnID_ExperienciaDocencia);
            }
        }

        public bool GetBycnID_ExperienciaDocencia(Int64 cnID_ExperienciaDocencia, tbExperienciaDocenciaModel result)
        {
            using (tbExperienciaDocenciaDAL dal = new tbExperienciaDocenciaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_ExperienciaDocencia(cnID_ExperienciaDocencia, result);
            }
        }

        public void Update(tbExperienciaDocenciaModel model)
        {
            using (tbExperienciaDocenciaDAL dal = new tbExperienciaDocenciaDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int64 Insert(tbExperienciaDocenciaModel model)
        {
            using (tbExperienciaDocenciaDAL dal = new tbExperienciaDocenciaDAL(Transaction, Connection))
            {

                return dal.Insert(model);
            }
        }

        public Int64 InsertBloque(tbExperienciaDocenciaModel model, string filename, int idbloque)
        {
            using (tbExperienciaDocenciaDAL dal = new tbExperienciaDocenciaDAL(Transaction, Connection))
            {

                return dal.InsertBloque(model, filename, idbloque);
            }
        }

        public void Delete(Int64 cnID_ExperienciaDocencia)
        {
            using (tbExperienciaDocenciaDAL dal = new tbExperienciaDocenciaDAL(Transaction, Connection))
            {
                dal.Delete(cnID_ExperienciaDocencia);
            }
        }

        public void DeleteByCandidato(Int64 cnID_Candidato)
        {
            using (tbExperienciaDocenciaDAL dal = new tbExperienciaDocenciaDAL(Transaction, Connection))
            {
                dal.DeleteByIdCandidato(cnID_Candidato);
            }
        }



    }

    /// <summary>
    /// Business logic for tbAreaTematica
    /// </summary>
    public partial class tbAreaTematicaBLL : dbSeleccionBLLBase
    {
        internal tbAreaTematicaBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbAreaTematicaBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbAreaTematicaBLL"))
                return (tbAreaTematicaBLL)mngrInstances["tbAreaTematicaBLL"];
            else
            {
                tbAreaTematicaBLL objtbAreaTematicaBLL = new tbAreaTematicaBLL();
                mngrInstances.Add("tbAreaTematicaBLL", objtbAreaTematicaBLL);
                return objtbAreaTematicaBLL;
            }
        }

        public List<tbAreaTematicaModel> GetAll()
        {
            using (tbAreaTematicaDAL dal = new tbAreaTematicaDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbAreaTematicaModel GetBycnID_AreaTematica(Int64 cnID_AreaTematica)
        {
            using (tbAreaTematicaDAL dal = new tbAreaTematicaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_AreaTematica(cnID_AreaTematica);
            }
        }

        public tbAreaTematicaModel GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            using (tbAreaTematicaDAL dal = new tbAreaTematicaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_Candidato(cnID_Candidato);
            }
        }


        public bool GetBycnID_AreaTematica(Int64 cnID_AreaTematica, tbAreaTematicaModel result)
        {
            using (tbAreaTematicaDAL dal = new tbAreaTematicaDAL(Transaction, Connection))
            {
                return dal.GetBycnID_AreaTematica(cnID_AreaTematica, result);
            }
        }

        public void Update(tbAreaTematicaModel model)
        {
            using (tbAreaTematicaDAL dal = new tbAreaTematicaDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int64 Insert(tbAreaTematicaModel model)
        {
            using (tbAreaTematicaDAL dal = new tbAreaTematicaDAL(Transaction, Connection))
            {

                return dal.Insert(model);
            }
        }

        public void Delete(Int64 cnID_AreaTematica)
        {
            using (tbAreaTematicaDAL dal = new tbAreaTematicaDAL(Transaction, Connection))
            {
                dal.Delete(cnID_AreaTematica);
            }
        }



    }

    /// <summary>
    /// Business logic for tbAreaTematicaDet
    /// </summary>
    public partial class tbAreaTematicaDetBLL : dbSeleccionBLLBase
    {
        internal tbAreaTematicaDetBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbAreaTematicaDetBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbAreaTematicaDetBLL"))
                return (tbAreaTematicaDetBLL)mngrInstances["tbAreaTematicaDetBLL"];
            else
            {
                tbAreaTematicaDetBLL objtbAreaTematicaDetBLL = new tbAreaTematicaDetBLL();
                mngrInstances.Add("tbAreaTematicaDetBLL", objtbAreaTematicaDetBLL);
                return objtbAreaTematicaDetBLL;
            }
        }

        public List<tbAreaTematicaDetModel> GetAll()
        {
            using (tbAreaTematicaDetDAL dal = new tbAreaTematicaDetDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public tbAreaTematicaDetModel GetBycnID_AreaTematicaDet(Int64 cnID_AreaTematicaDet)
        {
            using (tbAreaTematicaDetDAL dal = new tbAreaTematicaDetDAL(Transaction, Connection))
            {
                return dal.GetBycnID_AreaTematicaDet(cnID_AreaTematicaDet);
            }
        }

        public bool GetBycnID_AreaTematicaDet(Int64 cnID_AreaTematicaDet, tbAreaTematicaDetModel result)
        {
            using (tbAreaTematicaDetDAL dal = new tbAreaTematicaDetDAL(Transaction, Connection))
            {
                return dal.GetBycnID_AreaTematicaDet(cnID_AreaTematicaDet, result);
            }
        }

        public void Update(tbAreaTematicaDetModel model)
        {
            using (tbAreaTematicaDetDAL dal = new tbAreaTematicaDetDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int64 Insert(tbAreaTematicaDetModel model)
        {
            using (tbAreaTematicaDetDAL dal = new tbAreaTematicaDetDAL(Transaction, Connection))
            {

                return dal.Insert(model);
            }
        }

        public void Delete(Int64 cnID_AreaTematicaDet)
        {
            using (tbAreaTematicaDetDAL dal = new tbAreaTematicaDetDAL(Transaction, Connection))
            {
                dal.Delete(cnID_AreaTematicaDet);
            }
        }


        public void DeleteByNivel(Int64 cnID_candidato, Int64 cnID_AreaTematicaNivel)
        {
            using (tbAreaTematicaDetDAL dal = new tbAreaTematicaDetDAL(Transaction, Connection))
            {
                dal.DeleteByNivel(cnID_candidato, cnID_AreaTematicaNivel);
            }
        }

    }

    /// <summary>
    /// Business logic for tbExperienciaDocenciaDet
    /// </summary>
    public partial class tbExperienciaDocenciaDetBLL : dbSeleccionBLLBase
    {
        internal tbExperienciaDocenciaDetBLL()
        {
            objGuid = Guid.NewGuid();
        }

        public static tbExperienciaDocenciaDetBLL CreateInstance()
        {
            if (mngrInstances.ContainsKey("tbExperienciaDocenciaDetBLL"))
                return (tbExperienciaDocenciaDetBLL)mngrInstances["tbExperienciaDocenciaDetBLL"];
            else
            {
                tbExperienciaDocenciaDetBLL objtbExperienciaDocenciaDetBLL = new tbExperienciaDocenciaDetBLL();
                mngrInstances.Add("tbExperienciaDocenciaDetBLL", objtbExperienciaDocenciaDetBLL);
                return objtbExperienciaDocenciaDetBLL;
            }
        }

        public List<tbExperienciaDocenciaDetModel> GetAll()
        {
            using (tbExperienciaDocenciaDetDAL dal = new tbExperienciaDocenciaDetDAL(Transaction, Connection))
            {
                return dal.GetAll();
            }
        }

        public List<tbExperienciaDocenciaDetModel> GetBycnID_ExperienciaDocencia(Int64 cnID_ExperienciaDocencia)
        {
            using (tbExperienciaDocenciaDetDAL dal = new tbExperienciaDocenciaDetDAL(Transaction, Connection))
            {
                return dal.GetBycnID_ExperienciaDocencia(cnID_ExperienciaDocencia);
            }
        }


        public tbExperienciaDocenciaDetModel GetBycnID_ExperienciaDocenciaDet(Int64 cnID_ExperienciaDocenciaDet)
        {
            using (tbExperienciaDocenciaDetDAL dal = new tbExperienciaDocenciaDetDAL(Transaction, Connection))
            {
                return dal.GetBycnID_ExperienciaDocenciaDet(cnID_ExperienciaDocenciaDet);
            }
        }

        public bool GetBycnID_ExperienciaDocenciaDet(Int64 cnID_ExperienciaDocenciaDet, tbExperienciaDocenciaDetModel result)
        {
            using (tbExperienciaDocenciaDetDAL dal = new tbExperienciaDocenciaDetDAL(Transaction, Connection))
            {
                return dal.GetBycnID_ExperienciaDocenciaDet(cnID_ExperienciaDocenciaDet, result);
            }
        }

        public void Update(tbExperienciaDocenciaDetModel model)
        {
            using (tbExperienciaDocenciaDetDAL dal = new tbExperienciaDocenciaDetDAL(Transaction, Connection))
            {
                dal.Update(model);
            }
        }

        public Int64 Insert(tbExperienciaDocenciaDetModel model)
        {
            using (tbExperienciaDocenciaDetDAL dal = new tbExperienciaDocenciaDetDAL(Transaction, Connection))
            {

                return dal.Insert(model);
            }
        }

        public void Delete(Int64 cnID_ExperienciaDocenciaDet)
        {
            using (tbExperienciaDocenciaDetDAL dal = new tbExperienciaDocenciaDetDAL(Transaction, Connection))
            {
                dal.Delete(cnID_ExperienciaDocenciaDet);
            }
        }



    }


}