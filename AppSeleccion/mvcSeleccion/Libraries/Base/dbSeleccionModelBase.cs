using System;
using System.Collections;
using System.Data;

namespace SalarDb.CodeGen.Base
{
    public partial class dbSeleccionModelBase : IDisposable
    {
        #region variables

        private Hashtable _Items;
        private bool _DataLoaded = false;

        #endregion variables

        #region properties

        public object this[string name]
        {
            get
            {
                if (_Items == null)
                    return null;

                return _Items[name];
            }
            set
            {
                if (_Items == null)
                    _Items = new Hashtable();
                _Items[name] = value;
            }
        }

        public bool DataLoaded
        {
            get { return _DataLoaded; }
        }

        #endregion properties

        #region public methods

        public virtual void ReadData(IDataReader dataReader)
        {
        }

        public void Dispose()
        {
        }

        #endregion public methods

        #region protected methods

        protected void SetDataLoaded(bool loaded)
        {
            _DataLoaded = loaded;
        }

        public bool HasColumn(IDataRecord dr, string columnName)
        {
            for (int i = 0; i < dr.FieldCount; i++)
            {
                if (dr.GetName(i).Equals(columnName, StringComparison.InvariantCultureIgnoreCase))
                    return true;
            }
            return false;
        }

        #endregion protected methods
    }
}