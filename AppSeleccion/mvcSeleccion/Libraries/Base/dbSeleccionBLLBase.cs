using SalarDb.CodeGen.Common;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;

namespace SalarDb.CodeGen.Base
{
    public partial class dbSeleccionBLLBase : IDisposable
    {
        #region variables

        private dbSeleccionDbTransaction _Transaction = null;
        private IDbConnection _Connection = null;

        #endregion variables

        #region properties

        public dbSeleccionDbTransaction Transaction
        {
            get { return _Transaction; }
            set { _Transaction = value; }
        }

        public IDbConnection Connection
        {
            get { return _Connection; }
            set { _Connection = value; }
        }

        #endregion properties

        #region public methods

        public void Dispose()
        {
        }

        #endregion public methods

        #region methods singleton

        public Guid objGuid { get; set; }

        protected static ControllerDictionary<string, dbSeleccionBLLBase> mngrInstances = new ControllerDictionary<string, dbSeleccionBLLBase>();

        public event EventHandler OnMessageEvent;

        #endregion methods singleton
    }

    public class ControllerDictionary<TKey, TValue> : Dictionary<string, dbSeleccionBLLBase>, INotifyCollectionChanged
    {
        public ControllerDictionary()
            : base()
        {
        }

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public event EventHandler OnMessageEvent;

        public new bool ContainsKey(string key)
        {
            if (OnMessageEvent != null)
                OnMessageEvent(null, null);
            return base.ContainsKey(key);
        }

        public new void Add(string key, dbSeleccionBLLBase value)
        {
            base.Add(key, value);
            value.OnMessageEvent += new System.EventHandler(MessageEvent);
            OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, key, 0));
        }

        private void MessageEvent(object sender, System.EventArgs e)
        {
            if (OnMessageEvent != null)
                OnMessageEvent(sender, null);
        }

        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (CollectionChanged != null)
            {
                CollectionChanged(this, e);
            }
        }
    }
}