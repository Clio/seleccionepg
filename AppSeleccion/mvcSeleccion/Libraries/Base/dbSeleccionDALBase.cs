using SalarDb.CodeGen.Common;
using System;
using System.Data;
using System.Data.SqlClient;

namespace SalarDb.CodeGen.Base
{
    public class dbSeleccionDALBase : IDisposable
    {
        #region variables

        private dbSeleccionDbTransaction _Transaction = null;
        private IDbConnection _Connection = null;

        #endregion variables

        #region properties

        public dbSeleccionDbTransaction Transaction
        {
            get { return _Transaction; }
            set { _Transaction = value; }
        }

        public IDbConnection Connection
        {
            get { return _Connection; }
            set { _Connection = value; }
        }

        #endregion properties

        #region public methods

        public void Dispose()
        {
        }

        #endregion public methods

        #region protected methods

        protected SqlCommand GetNewCommand()
        {
            IDbConnection conn = _Connection;
            if (conn == null)
            {
                if (_Transaction != null && _Transaction.Connection != null)
                {
                    // Getting command from transaction
                    return _Transaction.GetNewCommand();
                }
                else
                    conn = dbSeleccionDbProvider.GetNewConnection();
            }
            return (SqlCommand)conn.CreateCommand();
        }

        #endregion protected methods
    }
}