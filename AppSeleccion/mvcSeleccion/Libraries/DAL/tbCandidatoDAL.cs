using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using SalarDb.CodeGen.Model;
using SalarDb.CodeGen.Common;

namespace SalarDb.CodeGen.DAL
{
	/// <summary>
	/// User custom methods for tbCandidato
	/// </summary>
	partial class tbCandidatoDAL
	{
        public void UpdateEmail(Int64 cnID_Candidato, String ctEmail)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCandidato_UpdateEmail";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);
                    cmd.Parameters.AddWithValue("@ctEmail", ctEmail);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void UpdateClaveAcceso(Int64 cnID_Candidato, String ctClaveAcceso)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCandidato_UpdateClaveAcceso";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);
                    cmd.Parameters.AddWithValue("@ctClaveAcceso", ctClaveAcceso);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
	}
}
