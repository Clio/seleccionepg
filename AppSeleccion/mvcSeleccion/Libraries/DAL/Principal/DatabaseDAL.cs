using SalarDb.CodeGen.Base;
using SalarDb.CodeGen.Common;
using SalarDb.CodeGen.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace SalarDb.CodeGen.DAL
{
    /// <summary>
    /// Data access layer for tbCandidato
    /// </summary>
    public partial class tbCandidatoDAL : dbSeleccionDALBase
    {
        internal tbCandidatoDAL()
        {
        }

        internal tbCandidatoDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbCandidatoModel> GetAll()
        {
            List<tbCandidatoModel> result = new List<tbCandidatoModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbCandidato_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbCandidatoModel.FieldsOrdinal fields = new tbCandidatoModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbCandidatoModel model = new tbCandidatoModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbCandidatoModel GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCandidato_GetBycnID_Candidato";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbCandidatoModel result = new tbCandidatoModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_Candidato(Int64 cnID_Candidato, tbCandidatoModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCandidato_GetBycnID_Candidato";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbCandidatoModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCandidato_Update";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    cmd.Parameters.AddWithValue("@ctApellidoPaterno", model.ctApellidoPaterno);
                    cmd.Parameters.AddWithValue("@ctApellidoMaterno", model.ctApellidoMaterno);
                    cmd.Parameters.AddWithValue("@ctNombres", model.ctNombres);
                    cmd.Parameters.AddWithValue("@cnID_Sexo", model.cnID_Sexo);
                    cmd.Parameters.AddWithValue("@ctEstadoCivil", model.ctEstadoCivil);
                    cmd.Parameters.AddWithValue("@ctLugarNacimiento", model.ctLugarNacimiento);
                    cmd.Parameters.AddWithValue("@cfFechaNacimiento", model.cfFechaNacimiento);
                    cmd.Parameters.AddWithValue("@cnID_PaisNacimiento", model.cnID_PaisNacimiento);
                    cmd.Parameters.AddWithValue("@ctDireccion", model.ctDireccion);

                    if (model.cnID_Pais > 0)
                        cmd.Parameters.AddWithValue("@cnID_Pais", model.cnID_Pais);

                    if (model.cnID_Departamento > 0)
                        cmd.Parameters.AddWithValue("@cnID_Departamento", model.cnID_Departamento);

                    if (model.cnID_Provincia > 0)
                        cmd.Parameters.AddWithValue("@cnID_Provincia", model.cnID_Provincia);

                    if (model.cnID_Distrito > 0)
                        cmd.Parameters.AddWithValue("@cnID_Distrito", model.cnID_Distrito);

                    if (model.ctTelefonoCasa != null)
                        cmd.Parameters.AddWithValue("@ctTelefonoCasa", model.ctTelefonoCasa);

                    if (model.ctTelefonoMovil != null)
                        cmd.Parameters.AddWithValue("@ctTelefonoMovil", model.ctTelefonoMovil);

                    if (model.ctTelefonoOtro != null)
                        cmd.Parameters.AddWithValue("@ctTelefonoOtro", model.ctTelefonoOtro);

                    //cmd.Parameters.AddWithValue("@ctEmail", model.ctEmail);
                    //cmd.Parameters.AddWithValue("@ctClaveAcceso", model.ctClaveAcceso);
                    if (model.cfFechaCreacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion.Value);

                    if (model.cfFechaActualizacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);
                    cmd.Parameters.AddWithValue("@cnEstadoReg", model.cnEstadoReg);

                    cmd.Parameters.AddWithValue("@cnCodigoRecuperacion", model.cnCodigoRecuperacion);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void UpdateProfile(tbCandidatoModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCandidato_UpdateProfile";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);

                    cmd.Parameters.AddWithValue("@ctEmail", model.ctEmail);
                    cmd.Parameters.AddWithValue("@ctClaveAcceso", model.ctClaveAcceso);

                    cmd.Parameters.AddWithValue("@ctImagenPerfil", model.ctImagenPerfil);

                    cmd.Parameters.AddWithValue("@ctDescripcion", model.ctDescripcion);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int64 Insert(tbCandidatoModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCandidato_Insert";

                    cmd.Parameters.AddWithValue("@ctApellidoPaterno", model.ctApellidoPaterno);
                    cmd.Parameters.AddWithValue("@ctApellidoMaterno", model.ctApellidoMaterno);
                    cmd.Parameters.AddWithValue("@ctNombres", model.ctNombres);
                    cmd.Parameters.AddWithValue("@cnID_Sexo", model.cnID_Sexo);
                    cmd.Parameters.AddWithValue("@ctEstadoCivil", model.ctEstadoCivil);
                    cmd.Parameters.AddWithValue("@ctLugarNacimiento", model.ctLugarNacimiento);
                    cmd.Parameters.AddWithValue("@cfFechaNacimiento", model.cfFechaNacimiento);
                    cmd.Parameters.AddWithValue("@cnID_PaisNacimiento", model.cnID_PaisNacimiento);
                    cmd.Parameters.AddWithValue("@ctDireccion", model.ctDireccion);
                    cmd.Parameters.AddWithValue("@cnID_Pais", model.cnID_Pais);
                    cmd.Parameters.AddWithValue("@cnID_Departamento", model.cnID_Departamento);
                    cmd.Parameters.AddWithValue("@cnID_Provincia", model.cnID_Provincia);
                    cmd.Parameters.AddWithValue("@cnID_Distrito", model.cnID_Distrito);
                    int valueTerm = model.ctTerminoAceptacion == true ? 1 : 0;
                    cmd.Parameters.AddWithValue("@ctTerminoAceptacion", valueTerm);
                    if (model.ctTelefonoCasa != null)
                        cmd.Parameters.AddWithValue("@ctTelefonoCasa", model.ctTelefonoCasa);

                    if (model.ctTelefonoMovil != null)
                        cmd.Parameters.AddWithValue("@ctTelefonoMovil", model.ctTelefonoMovil);

                    if (model.ctTelefonoOtro != null)
                        cmd.Parameters.AddWithValue("@ctTelefonoOtro", model.ctTelefonoOtro);

                    cmd.Parameters.AddWithValue("@ctEmail", model.ctEmail);
                    cmd.Parameters.AddWithValue("@ctClaveAcceso", model.ctClaveAcceso);
                    if (model.cfFechaCreacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion.Value);

                    if (model.cfFechaActualizacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);
                    cmd.Parameters.AddWithValue("@cnEstadoReg", model.cnEstadoReg);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt64(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnID_Candidato)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCandidato_DeleteBycnID_Candidato";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbCandidatoModel> GetBycnID_Sexo(Int64 cnID_Sexo)
        {
            List<tbCandidatoModel> result = new List<tbCandidatoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCandidato_GetBycnID_Sexo";

                    cmd.Parameters.AddWithValue("@cnID_Sexo", cnID_Sexo);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbCandidatoModel.FieldsOrdinal fields = new tbCandidatoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbCandidatoModel model = new tbCandidatoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbCandidatoModel> GetBycnID_PaisNacimiento(Int32 cnID_PaisNacimiento)
        {
            List<tbCandidatoModel> result = new List<tbCandidatoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCandidato_GetBycnID_PaisNacimiento";

                    cmd.Parameters.AddWithValue("@cnID_PaisNacimiento", cnID_PaisNacimiento);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbCandidatoModel.FieldsOrdinal fields = new tbCandidatoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbCandidatoModel model = new tbCandidatoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbCandidatoModel> GetBycnID_Pais(Int32 cnID_Pais)
        {
            List<tbCandidatoModel> result = new List<tbCandidatoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCandidato_GetBycnID_Pais";

                    cmd.Parameters.AddWithValue("@cnID_Pais", cnID_Pais);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbCandidatoModel.FieldsOrdinal fields = new tbCandidatoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbCandidatoModel model = new tbCandidatoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbCandidatoModel> GetBycnID_Departamento(Int32 cnID_Departamento)
        {
            List<tbCandidatoModel> result = new List<tbCandidatoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCandidato_GetBycnID_Departamento";

                    cmd.Parameters.AddWithValue("@cnID_Departamento", cnID_Departamento);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbCandidatoModel.FieldsOrdinal fields = new tbCandidatoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbCandidatoModel model = new tbCandidatoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbCandidatoModel> GetBycnID_Provincia(Int32 cnID_Provincia)
        {
            List<tbCandidatoModel> result = new List<tbCandidatoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCandidato_GetBycnID_Provincia";

                    cmd.Parameters.AddWithValue("@cnID_Provincia", cnID_Provincia);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbCandidatoModel.FieldsOrdinal fields = new tbCandidatoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbCandidatoModel model = new tbCandidatoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbCandidatoModel> GetBycnID_Distrito(Int32 cnID_Distrito)
        {
            List<tbCandidatoModel> result = new List<tbCandidatoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCandidato_GetBycnID_Distrito";

                    cmd.Parameters.AddWithValue("@cnID_Distrito", cnID_Distrito);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbCandidatoModel.FieldsOrdinal fields = new tbCandidatoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbCandidatoModel model = new tbCandidatoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbCandidatoModel> Ff___GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            List<tbCandidatoModel> result = new List<tbCandidatoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCandidato_GetBycnID_Candidato";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbCandidatoModel.FieldsOrdinal fields = new tbCandidatoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbCandidatoModel model = new tbCandidatoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public tbCandidatoModel GetBytcCodigoRecuperacion(String ctCodigoRecuperacion)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCandidato_GetBytcCodigoRecuperacion";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnCodigoRecuperacion", ctCodigoRecuperacion);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbCandidatoModel result = new tbCandidatoModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbCandidatoModel> GetByctApellidoMaterno(String ctApellidoMaterno)
        {
            List<tbCandidatoModel> result = new List<tbCandidatoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCandidato_GetByctApellidoMaterno";

                    cmd.Parameters.AddWithValue("@ctApellidoMaterno", ctApellidoMaterno);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbCandidatoModel.FieldsOrdinal fields = new tbCandidatoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbCandidatoModel model = new tbCandidatoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbCandidatoModel> GetByctApellidoPaterno(String ctApellidoPaterno)
        {
            List<tbCandidatoModel> result = new List<tbCandidatoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCandidato_GetByctApellidoPaterno";

                    cmd.Parameters.AddWithValue("@ctApellidoPaterno", ctApellidoPaterno);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbCandidatoModel.FieldsOrdinal fields = new tbCandidatoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbCandidatoModel model = new tbCandidatoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbCandidatoModel> GetByctNombres(String ctNombres)
        {
            List<tbCandidatoModel> result = new List<tbCandidatoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCandidato_GetByctNombres";

                    cmd.Parameters.AddWithValue("@ctNombres", ctNombres);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbCandidatoModel.FieldsOrdinal fields = new tbCandidatoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbCandidatoModel model = new tbCandidatoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public tbCandidatoModel GetByctEmail(String ctEmail)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCandidato_GetByctEmail";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@ctEmail", ctEmail);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbCandidatoModel result = new tbCandidatoModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteByctEmail(String ctEmail)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCandidato_DeleteByctEmail";

                    cmd.Parameters.AddWithValue("@ctEmail", ctEmail);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbCandidatoModel> GetConsultaCandidatos(string ctNombreCompleto, string ctNumeroDNI, string ctEmail)
        {
            List<tbCandidatoModel> result = new List<tbCandidatoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCandidato_GetConsultaCandidatos";
                    if (ctNombreCompleto.Trim().Length > 0)
                        cmd.Parameters.AddWithValue("@ctNombreCompleto", ctNombreCompleto);
                    if (ctNumeroDNI.Trim().Length > 0)
                        cmd.Parameters.AddWithValue("@ctNumeroDNI", ctNumeroDNI);
                    if (ctEmail.Trim().Length > 0)
                        cmd.Parameters.AddWithValue("@ctEmail", ctEmail);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbCandidatoModel.FieldsOrdinal fields = new tbCandidatoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbCandidatoModel model = new tbCandidatoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetConsultaQuery(string query, string columnas)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCandidatoGetAllInfo";

                    cmd.Parameters.AddWithValue("@query", query);
                    cmd.Parameters.AddWithValue("@columnas", columnas);

                    try
                    {
                        cmd.Connection.Open();

                        SqlDataReader dr = cmd.ExecuteReader();

                        var tb = new DataTable();
                        tb.Load(dr);

                        cmd.Connection.Close();
                        return tb;
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                    }
                }
            }
            catch (Exception)
            {
            }

            return null;
        }
    }

    /// <summary>
    /// Data access layer for tbCnfgSistema
    /// </summary>
    public partial class tbCnfgSistemaDAL : dbSeleccionDALBase
    {
        internal tbCnfgSistemaDAL()
        {
        }

        internal tbCnfgSistemaDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbCnfgSistemaModel> GetAll()
        {
            List<tbCnfgSistemaModel> result = new List<tbCnfgSistemaModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbCnfgSistema_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbCnfgSistemaModel.FieldsOrdinal fields = new tbCnfgSistemaModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbCnfgSistemaModel model = new tbCnfgSistemaModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbCnfgSistemaModel GetBycnID_Config(Int32 cnID_Config)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCnfgSistema_GetBycnID_Config";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Config", cnID_Config);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbCnfgSistemaModel result = new tbCnfgSistemaModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public tbCnfgSistemaModel GetByctAlias(string ctAlias)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCnfgSistema_GetByctAlias";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@ctAlias", ctAlias);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbCnfgSistemaModel result = new tbCnfgSistemaModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_Config(Int32 cnID_Config, tbCnfgSistemaModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCnfgSistema_GetBycnID_Config";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Config", cnID_Config);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbCnfgSistemaModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCnfgSistema_Update";

                    cmd.Parameters.AddWithValue("@cnID_Config", model.cnID_Config);
                    cmd.Parameters.AddWithValue("@ctDescripcion", model.ctDescripcion);
                    if (model.ctValor != null)
                        cmd.Parameters.AddWithValue("@ctValor", model.ctValor);

                    cmd.Parameters.AddWithValue("@ctCodigo", model.ctAlias);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int32 Insert(tbCnfgSistemaModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCnfgSistema_Insert";

                    cmd.Parameters.AddWithValue("@ctDescripcion", model.ctDescripcion);
                    if (model.ctValor != null)
                        cmd.Parameters.AddWithValue("@ctValor", model.ctValor);

                    cmd.Parameters.AddWithValue("@ctCodigo", model.ctAlias);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt32(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int32 cnID_Config)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCnfgSistema_DeleteBycnID_Config";

                    cmd.Parameters.AddWithValue("@cnID_Config", cnID_Config);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public tbCnfgSistemaModel GetByctDescripcion(String ctDescripcion)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCnfgSistema_GetByctDescripcion";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@ctDescripcion", ctDescripcion);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbCnfgSistemaModel result = new tbCnfgSistemaModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteByctDescripcion(String ctDescripcion)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCnfgSistema_DeleteByctDescripcion";

                    cmd.Parameters.AddWithValue("@ctDescripcion", ctDescripcion);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbDepartamento
    /// </summary>
    public partial class tbDepartamentoDAL : dbSeleccionDALBase
    {
        internal tbDepartamentoDAL()
        {
        }

        internal tbDepartamentoDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbDepartamentoModel> GetAll()
        {
            List<tbDepartamentoModel> result = new List<tbDepartamentoModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbDepartamento_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbDepartamentoModel.FieldsOrdinal fields = new tbDepartamentoModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbDepartamentoModel model = new tbDepartamentoModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbDepartamentoModel GetBycnID_Departamento(Int32 cnID_Departamento)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDepartamento_GetBycnID_Departamento";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Departamento", cnID_Departamento);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbDepartamentoModel result = new tbDepartamentoModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_Departamento(Int32 cnID_Departamento, tbDepartamentoModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDepartamento_GetBycnID_Departamento";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Departamento", cnID_Departamento);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbDepartamentoModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDepartamento_Update";

                    cmd.Parameters.AddWithValue("@cnID_Departamento", model.cnID_Departamento);
                    if (model.ctDepartamento != null)
                        cmd.Parameters.AddWithValue("@ctDepartamento", model.ctDepartamento);

                    if (model.ctCodigoISO != null)
                        cmd.Parameters.AddWithValue("@ctCodigoISO", model.ctCodigoISO);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int32 Insert(tbDepartamentoModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDepartamento_Insert";

                    if (model.ctDepartamento != null)
                        cmd.Parameters.AddWithValue("@ctDepartamento", model.ctDepartamento);

                    if (model.ctCodigoISO != null)
                        cmd.Parameters.AddWithValue("@ctCodigoISO", model.ctCodigoISO);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt32(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int32 cnID_Departamento)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDepartamento_DeleteBycnID_Departamento";

                    cmd.Parameters.AddWithValue("@cnID_Departamento", cnID_Departamento);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbDistrito
    /// </summary>
    public partial class tbDistritoDAL : dbSeleccionDALBase
    {
        internal tbDistritoDAL()
        {
        }

        internal tbDistritoDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbDistritoModel> GetAll()
        {
            List<tbDistritoModel> result = new List<tbDistritoModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbDistrito_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbDistritoModel.FieldsOrdinal fields = new tbDistritoModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbDistritoModel model = new tbDistritoModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbDistritoModel GetBycnID_Distrito(Int32 cnID_Distrito)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDistrito_GetBycnID_Distrito";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Distrito", cnID_Distrito);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbDistritoModel result = new tbDistritoModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_Distrito(Int32 cnID_Distrito, tbDistritoModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDistrito_GetBycnID_Distrito";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Distrito", cnID_Distrito);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbDistritoModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDistrito_Update";

                    cmd.Parameters.AddWithValue("@cnID_Distrito", model.cnID_Distrito);
                    if (model.cnID_Departamento.HasValue)
                        cmd.Parameters.AddWithValue("@cnID_Departamento", model.cnID_Departamento.Value);

                    cmd.Parameters.AddWithValue("@cnID_Provincia", model.cnID_Provincia);
                    cmd.Parameters.AddWithValue("@ctDistrito", model.ctDistrito);
                    if (model.ctCodigoISO != null)
                        cmd.Parameters.AddWithValue("@ctCodigoISO", model.ctCodigoISO);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int32 Insert(tbDistritoModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDistrito_Insert";

                    if (model.cnID_Departamento.HasValue)
                        cmd.Parameters.AddWithValue("@cnID_Departamento", model.cnID_Departamento.Value);

                    cmd.Parameters.AddWithValue("@cnID_Provincia", model.cnID_Provincia);
                    cmd.Parameters.AddWithValue("@ctDistrito", model.ctDistrito);
                    if (model.ctCodigoISO != null)
                        cmd.Parameters.AddWithValue("@ctCodigoISO", model.ctCodigoISO);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt32(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int32 cnID_Distrito)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDistrito_DeleteBycnID_Distrito";

                    cmd.Parameters.AddWithValue("@cnID_Distrito", cnID_Distrito);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbDistritoModel> GetBycnID_Departamento(Int32 cnID_Departamento)
        {
            List<tbDistritoModel> result = new List<tbDistritoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDistrito_GetBycnID_Departamento";

                    cmd.Parameters.AddWithValue("@cnID_Departamento", cnID_Departamento);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbDistritoModel.FieldsOrdinal fields = new tbDistritoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbDistritoModel model = new tbDistritoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbDistritoModel> GetBycnID_Provincia(Int32 cnID_Provincia)
        {
            List<tbDistritoModel> result = new List<tbDistritoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDistrito_GetBycnID_Provincia";

                    cmd.Parameters.AddWithValue("@cnID_Provincia", cnID_Provincia);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbDistritoModel.FieldsOrdinal fields = new tbDistritoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbDistritoModel model = new tbDistritoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbDocIdentidad
    /// </summary>
    public partial class tbDocIdentidadDAL : dbSeleccionDALBase
    {
        internal tbDocIdentidadDAL()
        {
        }

        internal tbDocIdentidadDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbDocIdentidadModel> GetAll()
        {
            List<tbDocIdentidadModel> result = new List<tbDocIdentidadModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbDocIdentidad_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbDocIdentidadModel.FieldsOrdinal fields = new tbDocIdentidadModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbDocIdentidadModel model = new tbDocIdentidadModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public List<tbDocIdentidadModel> GetBycnID_CandidatoList(Int64 cnID_Candidato)
        {
            List<tbDocIdentidadModel> result = new List<tbDocIdentidadModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbDocIdentidad_GetAllByCandidato";

                IDataReader reader = null;
                try
                {
                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbDocIdentidadModel.FieldsOrdinal fields = new tbDocIdentidadModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbDocIdentidadModel model = new tbDocIdentidadModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbDocIdentidadModel GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDocIdentidad_GetBycnID_Candidato";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbDocIdentidadModel result = new tbDocIdentidadModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_Candidato(Int64 cnID_Candidato, tbDocIdentidadModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDocIdentidad_GetBycnID_Candidato";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbDocIdentidadModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDocIdentidad_Update";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    cmd.Parameters.AddWithValue("@cnID_TipoDocumento", model.cnID_TipoDocumento);
                    cmd.Parameters.AddWithValue("@ctNumero", model.ctNumero);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Insert(tbDocIdentidadModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDocIdentidad_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    cmd.Parameters.AddWithValue("@cnID_TipoDocumento", model.cnID_TipoDocumento);
                    cmd.Parameters.AddWithValue("@ctNumero", model.ctNumero);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnID_Candidato)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDocIdentidad_DeleteBycnID_Candidato";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbDocIdentidadModel> ___GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            List<tbDocIdentidadModel> result = new List<tbDocIdentidadModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDocIdentidad_GetBycnID_Candidato";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbDocIdentidadModel.FieldsOrdinal fields = new tbDocIdentidadModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbDocIdentidadModel model = new tbDocIdentidadModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbDocIdentidadModel> ___GetBycnID_TipoDocumento(Int64 cnID_TipoDocumento)
        {
            List<tbDocIdentidadModel> result = new List<tbDocIdentidadModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDocIdentidad_GetBycnID_TipoDocumento";

                    cmd.Parameters.AddWithValue("@cnID_TipoDocumento", cnID_TipoDocumento);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbDocIdentidadModel.FieldsOrdinal fields = new tbDocIdentidadModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbDocIdentidadModel model = new tbDocIdentidadModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbDocIdentidadModel> GetByctNumero(String ctNumero)
        {
            List<tbDocIdentidadModel> result = new List<tbDocIdentidadModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDocIdentidad_GetByctNumero";

                    cmd.Parameters.AddWithValue("@ctNumero", ctNumero);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbDocIdentidadModel.FieldsOrdinal fields = new tbDocIdentidadModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbDocIdentidadModel model = new tbDocIdentidadModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbDocIdentidad
    /// </summary>
    public partial class tbMailDAL : dbSeleccionDALBase
    {
        internal tbMailDAL()
        {
        }

        internal tbMailDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbMailModel> GetAll()
        {
            List<tbMailModel> result = new List<tbMailModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbMail_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbMailModel.FieldsOrdinal fields = new tbMailModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbMailModel model = new tbMailModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public List<tbMailModel> GetBycnID_CandidatoList(Int64 cnID_Candidato)
        {
            List<tbMailModel> result = new List<tbMailModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbMail_GetAllByCandidato";

                IDataReader reader = null;
                try
                {
                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbMailModel.FieldsOrdinal fields = new tbMailModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbMailModel model = new tbMailModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbMailModel GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMail_GetBycnID_Candidato";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbMailModel result = new tbMailModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_Candidato(Int64 cnID_Candidato, tbMailModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMail_GetBycnID_Candidato";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbMailModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMail_Update";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    cmd.Parameters.AddWithValue("@cnID_TipoEmail", model.cnID_TipoEmail);
                    cmd.Parameters.AddWithValue("@ctMail", model.ctMail);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Insert(tbMailModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMail_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    cmd.Parameters.AddWithValue("@cnID_TipoEmail", model.cnID_TipoEmail);
                    cmd.Parameters.AddWithValue("@ctMail", model.ctMail);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnID_Candidato)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMail_DeleteBycnID_Candidato";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbMailModel> ___GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            List<tbMailModel> result = new List<tbMailModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMail_GetBycnID_Candidato";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbMailModel.FieldsOrdinal fields = new tbMailModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbMailModel model = new tbMailModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbMailModel> ___GetBycnID_TipoEmail(Int64 cnID_TipoEmail)
        {
            List<tbMailModel> result = new List<tbMailModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMail_GetBycnID_TipoEmail";

                    cmd.Parameters.AddWithValue("@cnID_TipoEmail", cnID_TipoEmail);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbMailModel.FieldsOrdinal fields = new tbMailModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbMailModel model = new tbMailModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbMailModel> GetByctMail(String ctMail)
        {
            List<tbMailModel> result = new List<tbMailModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMail_GetByctMail";

                    cmd.Parameters.AddWithValue("@ctMail", ctMail);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbMailModel.FieldsOrdinal fields = new tbMailModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbMailModel model = new tbMailModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbEspecializacion
    /// </summary>
    public partial class tbEspecializacionDAL : dbSeleccionDALBase
    {
        internal tbEspecializacionDAL()
        {
        }

        internal tbEspecializacionDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbEspecializacionModel> GetAll()
        {
            List<tbEspecializacionModel> result = new List<tbEspecializacionModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbEspecializacion_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbEspecializacionModel.FieldsOrdinal fields = new tbEspecializacionModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbEspecializacionModel model = new tbEspecializacionModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbEspecializacionModel GetBycnID_Especializacion(Int64 cnID_Especializacion)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbEspecializacion_GetBycnID_Especializacion";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Especializacion", cnID_Especializacion);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbEspecializacionModel result = new tbEspecializacionModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_Especializacion(Int64 cnID_Especializacion, tbEspecializacionModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbEspecializacion_GetBycnID_Especializacion";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Especializacion", cnID_Especializacion);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbEspecializacionModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbEspecializacion_Update";

                    cmd.Parameters.AddWithValue("@cnID_Especializacion", model.cnID_Especializacion);
                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    cmd.Parameters.AddWithValue("@cnID_Especialidad", model.cnID_Especialidad);
                    if (model.ctEspecialidadOtro != null)
                        cmd.Parameters.AddWithValue("@ctEspecialidadOtro", model.ctEspecialidadOtro);

                    cmd.Parameters.AddWithValue("@cnID_Nivel", model.cnID_Nivel);
                    cmd.Parameters.AddWithValue("@cnID_Pais", model.cnID_Pais);
                    if (model.ctCiudad != null)
                        cmd.Parameters.AddWithValue("@ctCiudad", model.ctCiudad);

                    cmd.Parameters.AddWithValue("@cfFechaInicio", model.cfFechaInicio);
                    cmd.Parameters.AddWithValue("@cfFechaFin", model.cfFechaFin);
                    if (model.ctCertificado != null)
                        cmd.Parameters.AddWithValue("@ctCertificado", model.ctCertificado);

                    cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    if (model.cfFechaActualizacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int64 Insert(tbEspecializacionModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbEspecializacion_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    cmd.Parameters.AddWithValue("@cnID_Especialidad", model.cnID_Especialidad);
                    if (model.ctEspecialidadOtro != null)
                        cmd.Parameters.AddWithValue("@ctEspecialidadOtro", model.ctEspecialidadOtro);

                    cmd.Parameters.AddWithValue("@cnID_Nivel", model.cnID_Nivel);
                    cmd.Parameters.AddWithValue("@cnID_Pais", model.cnID_Pais);
                    if (model.ctCiudad != null)
                        cmd.Parameters.AddWithValue("@ctCiudad", model.ctCiudad);

                    cmd.Parameters.AddWithValue("@cfFechaInicio", model.cfFechaInicio);
                    cmd.Parameters.AddWithValue("@cfFechaFin", model.cfFechaFin);
                    if (model.ctCertificado != null)
                        cmd.Parameters.AddWithValue("@ctCertificado", model.ctCertificado);

                    cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    if (model.cfFechaActualizacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt64(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnID_Especializacion)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbEspecializacion_DeleteBycnID_Especializacion";

                    cmd.Parameters.AddWithValue("@cnID_Especializacion", cnID_Especializacion);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbEspecializacionModel> GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            List<tbEspecializacionModel> result = new List<tbEspecializacionModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbEspecializacion_GetBycnID_Candidato";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbEspecializacionModel.FieldsOrdinal fields = new tbEspecializacionModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbEspecializacionModel model = new tbEspecializacionModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbEspecializacionModel> GetBycnID_Especialidad(Int64 cnID_Especialidad)
        {
            List<tbEspecializacionModel> result = new List<tbEspecializacionModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbEspecializacion_GetBycnID_Especialidad";

                    cmd.Parameters.AddWithValue("@cnID_Especialidad", cnID_Especialidad);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbEspecializacionModel.FieldsOrdinal fields = new tbEspecializacionModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbEspecializacionModel model = new tbEspecializacionModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbEspecializacionModel> GetBycnID_Nivel(Int64 cnID_Nivel)
        {
            List<tbEspecializacionModel> result = new List<tbEspecializacionModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbEspecializacion_GetBycnID_Nivel";

                    cmd.Parameters.AddWithValue("@cnID_Nivel", cnID_Nivel);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbEspecializacionModel.FieldsOrdinal fields = new tbEspecializacionModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbEspecializacionModel model = new tbEspecializacionModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbEspecializacionModel> GetBycnID_Pais(Int32 cnID_Pais)
        {
            List<tbEspecializacionModel> result = new List<tbEspecializacionModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbEspecializacion_GetBycnID_Pais";

                    cmd.Parameters.AddWithValue("@cnID_Pais", cnID_Pais);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbEspecializacionModel.FieldsOrdinal fields = new tbEspecializacionModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbEspecializacionModel model = new tbEspecializacionModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbExperiencia
    /// </summary>
    public partial class tbExperienciaDAL : dbSeleccionDALBase
    {
        internal tbExperienciaDAL()
        {
        }

        internal tbExperienciaDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbExperienciaModel> GetAll()
        {
            List<tbExperienciaModel> result = new List<tbExperienciaModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbExperiencia_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbExperienciaModel.FieldsOrdinal fields = new tbExperienciaModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbExperienciaModel model = new tbExperienciaModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbExperienciaModel GetBycnID_Experiencia(Int64 cnID_Experiencia)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperiencia_GetBycnID_Experiencia";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Experiencia", cnID_Experiencia);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbExperienciaModel result = new tbExperienciaModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_Experiencia(Int64 cnID_Experiencia, tbExperienciaModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperiencia_GetBycnID_Experiencia";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Experiencia", cnID_Experiencia);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbExperienciaModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperiencia_Update";

                    cmd.Parameters.AddWithValue("@cnID_Experiencia", model.cnID_Experiencia);
                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    cmd.Parameters.AddWithValue("@cnID_TipoEntidad", model.cnID_TipoEntidad);
                    cmd.Parameters.AddWithValue("@ctNombreEmpresa", model.ctNombreEmpresa);
                    cmd.Parameters.AddWithValue("@cnID_Rubro", model.cnID_Rubro);
                    cmd.Parameters.AddWithValue("@cnID_Cargo", model.cnID_Cargo);
                    cmd.Parameters.AddWithValue("@cnID_Area", model.cnID_Area);
                    cmd.Parameters.AddWithValue("@cnID_TipoContrato", model.cnID_TipoContrato);
                    cmd.Parameters.AddWithValue("@cnID_Pais", model.cnID_Pais);
                    if (model.cnSubordinados.HasValue)
                        cmd.Parameters.AddWithValue("@cnSubordinados", model.cnSubordinados.Value);

                    cmd.Parameters.AddWithValue("@ctDescripcionCargo", model.ctDescripcionCargo);
                    cmd.Parameters.AddWithValue("@cfFechaInicio", model.cfFechaInicio);
                    cmd.Parameters.AddWithValue("@cfFechaFin", model.cfFechaFin);
                    if (model.cnTiempoAnios.HasValue)
                        cmd.Parameters.AddWithValue("@cnTiempoAnios", model.cnTiempoAnios.Value);

                    if (model.cnTiempoMeses.HasValue)
                        cmd.Parameters.AddWithValue("@cnTiempoMeses", model.cnTiempoMeses.Value);

                    if (model.ctCertificado != null)
                        cmd.Parameters.AddWithValue("@ctCertificado", model.ctCertificado);

                    cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    if (model.cfFechaActualizacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int64 Insert(tbExperienciaModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperiencia_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    cmd.Parameters.AddWithValue("@cnID_TipoEntidad", model.cnID_TipoEntidad);
                    cmd.Parameters.AddWithValue("@ctNombreEmpresa", model.ctNombreEmpresa);
                    cmd.Parameters.AddWithValue("@cnID_Rubro", model.cnID_Rubro);
                    cmd.Parameters.AddWithValue("@cnID_Cargo", model.cnID_Cargo);
                    cmd.Parameters.AddWithValue("@cnID_Area", model.cnID_Area);
                    cmd.Parameters.AddWithValue("@cnID_TipoContrato", model.cnID_TipoContrato);
                    cmd.Parameters.AddWithValue("@cnID_Pais", model.cnID_Pais);
                    if (model.cnSubordinados.HasValue)
                        cmd.Parameters.AddWithValue("@cnSubordinados", model.cnSubordinados.Value);

                    cmd.Parameters.AddWithValue("@ctDescripcionCargo", model.ctDescripcionCargo);
                    cmd.Parameters.AddWithValue("@cfFechaInicio", model.cfFechaInicio);
                    cmd.Parameters.AddWithValue("@cfFechaFin", model.cfFechaFin);
                    if (model.cnTiempoAnios.HasValue)
                        cmd.Parameters.AddWithValue("@cnTiempoAnios", model.cnTiempoAnios.Value);

                    if (model.cnTiempoMeses.HasValue)
                        cmd.Parameters.AddWithValue("@cnTiempoMeses", model.cnTiempoMeses.Value);

                    if (model.ctCertificado != null)
                        cmd.Parameters.AddWithValue("@ctCertificado", model.ctCertificado);

                    cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    if (model.cfFechaActualizacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt64(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int64 InsertBloque(tbExperienciaModel model, int idbloque)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDocumentsAcademic_insert";


                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    
                    if (model.ctCertificado != null)
                        cmd.Parameters.AddWithValue("@ctCertificado", model.ctCertificado);



                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);
                    cmd.Parameters.AddWithValue("@ctIDBloque", idbloque);

                    cmd.Parameters.AddWithValue("@ctAceptaTerminos", model.ctAceptaTerminos);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt32(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnID_Experiencia)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperiencia_DeleteBycnID_Experiencia";

                    cmd.Parameters.AddWithValue("@cnID_Experiencia", cnID_Experiencia);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteByIdCandidato(Int64 cnID_Candidato)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperiencia_DeleteBycnID_Candidato_Fecha";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbExperienciaModel> GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            List<tbExperienciaModel> result = new List<tbExperienciaModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperiencia_GetBycnID_Candidato";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbExperienciaModel.FieldsOrdinal fields = new tbExperienciaModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbExperienciaModel model = new tbExperienciaModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbExperienciaModel> GetBycnID_TipoEntidad(Int64 cnID_TipoEntidad)
        {
            List<tbExperienciaModel> result = new List<tbExperienciaModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperiencia_GetBycnID_TipoEntidad";

                    cmd.Parameters.AddWithValue("@cnID_TipoEntidad", cnID_TipoEntidad);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbExperienciaModel.FieldsOrdinal fields = new tbExperienciaModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbExperienciaModel model = new tbExperienciaModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbExperienciaModel> GetBycnID_Rubro(Int64 cnID_Rubro)
        {
            List<tbExperienciaModel> result = new List<tbExperienciaModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperiencia_GetBycnID_Rubro";

                    cmd.Parameters.AddWithValue("@cnID_Rubro", cnID_Rubro);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbExperienciaModel.FieldsOrdinal fields = new tbExperienciaModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbExperienciaModel model = new tbExperienciaModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbExperienciaModel> GetBycnID_Cargo(Int64 cnID_Cargo)
        {
            List<tbExperienciaModel> result = new List<tbExperienciaModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperiencia_GetBycnID_Cargo";

                    cmd.Parameters.AddWithValue("@cnID_Cargo", cnID_Cargo);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbExperienciaModel.FieldsOrdinal fields = new tbExperienciaModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbExperienciaModel model = new tbExperienciaModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbExperienciaModel> GetBycnID_Area(Int64 cnID_Area)
        {
            List<tbExperienciaModel> result = new List<tbExperienciaModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperiencia_GetBycnID_Area";

                    cmd.Parameters.AddWithValue("@cnID_Area", cnID_Area);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbExperienciaModel.FieldsOrdinal fields = new tbExperienciaModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbExperienciaModel model = new tbExperienciaModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbExperienciaModel> GetBycnID_TipoContrato(Int64 cnID_TipoContrato)
        {
            List<tbExperienciaModel> result = new List<tbExperienciaModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperiencia_GetBycnID_TipoContrato";

                    cmd.Parameters.AddWithValue("@cnID_TipoContrato", cnID_TipoContrato);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbExperienciaModel.FieldsOrdinal fields = new tbExperienciaModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbExperienciaModel model = new tbExperienciaModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbExperienciaModel> GetBycnID_Pais(Int32 cnID_Pais)
        {
            List<tbExperienciaModel> result = new List<tbExperienciaModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperiencia_GetBycnID_Pais";

                    cmd.Parameters.AddWithValue("@cnID_Pais", cnID_Pais);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbExperienciaModel.FieldsOrdinal fields = new tbExperienciaModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbExperienciaModel model = new tbExperienciaModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbIdioma
    /// </summary>
    public partial class tbIdiomaDAL : dbSeleccionDALBase
    {
        internal tbIdiomaDAL()
        {
        }

        internal tbIdiomaDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbIdiomaModel> GetAll()
        {
            List<tbIdiomaModel> result = new List<tbIdiomaModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbIdioma_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbIdiomaModel.FieldsOrdinal fields = new tbIdiomaModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbIdiomaModel model = new tbIdiomaModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbIdiomaModel GetBycnID_Idioma(Int64 cnID_Idioma)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbIdioma_GetBycnID_Idioma";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Idioma", cnID_Idioma);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbIdiomaModel result = new tbIdiomaModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_Idioma(Int64 cnID_Idioma, tbIdiomaModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbIdioma_GetBycnID_Idioma";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Idioma", cnID_Idioma);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbIdiomaModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbIdioma_Update";

                    cmd.Parameters.AddWithValue("@cnID_Idioma", model.cnID_Idioma);
                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    if (model.cnID_Lengua.HasValue)
                        cmd.Parameters.AddWithValue("@cnID_Lengua", model.cnID_Lengua.Value);
                    if (model.ctLenguaOtro != null)
                        cmd.Parameters.AddWithValue("@ctLenguaOtro", model.ctLenguaOtro);

                    cmd.Parameters.AddWithValue("@cnID_Institucion", model.cnID_Institucion);
                    cmd.Parameters.AddWithValue("@cnID_NivelLectura", model.cnID_NivelLectura);
                    cmd.Parameters.AddWithValue("@cnID_NivelEscritura", model.cnID_NivelEscritura);
                    cmd.Parameters.AddWithValue("@cnID_NivelConversacion", model.cnID_NivelConversacion);
                    cmd.Parameters.AddWithValue("@cnID_Situacion", model.cnID_Situacion);
                    if (model.ctCertificado != null)
                        cmd.Parameters.AddWithValue("@ctCertificado", model.ctCertificado);

                    //cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    if (model.cfFechaActualizacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int64 Insert(tbIdiomaModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbIdioma_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    if (model.cnID_Lengua.HasValue)
                        cmd.Parameters.AddWithValue("@cnID_Lengua", model.cnID_Lengua.Value);
                    if (model.ctLenguaOtro != null)
                        cmd.Parameters.AddWithValue("@ctLenguaOtro", model.ctLenguaOtro);

                    cmd.Parameters.AddWithValue("@cnID_Institucion", model.cnID_Institucion);
                    cmd.Parameters.AddWithValue("@cnID_NivelLectura", model.cnID_NivelLectura);
                    cmd.Parameters.AddWithValue("@cnID_NivelEscritura", model.cnID_NivelEscritura);
                    cmd.Parameters.AddWithValue("@cnID_NivelConversacion", model.cnID_NivelConversacion);
                    cmd.Parameters.AddWithValue("@cnID_Situacion", model.cnID_Situacion);
                    if (model.ctCertificado != null)
                        cmd.Parameters.AddWithValue("@ctCertificado", model.ctCertificado);

                    cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    if (model.cfFechaActualizacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt64(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnID_Idioma)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbIdioma_DeleteBycnID_Idioma";

                    cmd.Parameters.AddWithValue("@cnID_Idioma", cnID_Idioma);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbIdiomaModel> GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            List<tbIdiomaModel> result = new List<tbIdiomaModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbIdioma_GetBycnID_Candidato";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbIdiomaModel.FieldsOrdinal fields = new tbIdiomaModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbIdiomaModel model = new tbIdiomaModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbIdiomaModel> GetBycnID_Lengua(Int64 cnID_Lengua)
        {
            List<tbIdiomaModel> result = new List<tbIdiomaModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbIdioma_GetBycnID_Lengua";

                    cmd.Parameters.AddWithValue("@cnID_Lengua", cnID_Lengua);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbIdiomaModel.FieldsOrdinal fields = new tbIdiomaModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbIdiomaModel model = new tbIdiomaModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbIdiomaModel> GetBycnID_Institucion(Int64 cnID_Institucion)
        {
            List<tbIdiomaModel> result = new List<tbIdiomaModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbIdioma_GetBycnID_Institucion";

                    cmd.Parameters.AddWithValue("@cnID_Institucion", cnID_Institucion);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbIdiomaModel.FieldsOrdinal fields = new tbIdiomaModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbIdiomaModel model = new tbIdiomaModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbIdiomaModel> GetBycnID_NivelLectura(Int64 cnID_NivelLectura)
        {
            List<tbIdiomaModel> result = new List<tbIdiomaModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbIdioma_GetBycnID_NivelLectura";

                    cmd.Parameters.AddWithValue("@cnID_NivelLectura", cnID_NivelLectura);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbIdiomaModel.FieldsOrdinal fields = new tbIdiomaModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbIdiomaModel model = new tbIdiomaModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbIdiomaModel> GetBycnID_NivelEscritura(Int64 cnID_NivelEscritura)
        {
            List<tbIdiomaModel> result = new List<tbIdiomaModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbIdioma_GetBycnID_NivelEscritura";

                    cmd.Parameters.AddWithValue("@cnID_NivelEscritura", cnID_NivelEscritura);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbIdiomaModel.FieldsOrdinal fields = new tbIdiomaModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbIdiomaModel model = new tbIdiomaModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbIdiomaModel> GetBycnID_NivelConversacion(Int64 cnID_NivelConversacion)
        {
            List<tbIdiomaModel> result = new List<tbIdiomaModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbIdioma_GetBycnID_NivelConversacion";

                    cmd.Parameters.AddWithValue("@cnID_NivelConversacion", cnID_NivelConversacion);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbIdiomaModel.FieldsOrdinal fields = new tbIdiomaModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbIdiomaModel model = new tbIdiomaModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbIdiomaModel> GetBycnID_Situacion(Int64 cnID_Situacion)
        {
            List<tbIdiomaModel> result = new List<tbIdiomaModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbIdioma_GetBycnID_Situacion";

                    cmd.Parameters.AddWithValue("@cnID_Situacion", cnID_Situacion);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbIdiomaModel.FieldsOrdinal fields = new tbIdiomaModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbIdiomaModel model = new tbIdiomaModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbMaestroGrupos
    /// </summary>
    public partial class tbMaestroGruposDAL : dbSeleccionDALBase
    {
        internal tbMaestroGruposDAL()
        {
        }

        internal tbMaestroGruposDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbMaestroGruposModel> GetAll()
        {
            List<tbMaestroGruposModel> result = new List<tbMaestroGruposModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbMaestroGrupos_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbMaestroGruposModel.FieldsOrdinal fields = new tbMaestroGruposModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbMaestroGruposModel model = new tbMaestroGruposModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public List<tbMaestroGruposModel> GetByctTipoGrupo(string ctTipoGrupo)
        {
            List<tbMaestroGruposModel> result = new List<tbMaestroGruposModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbMaestroGrupos_GetByctTipoGrupo";

                IDataReader reader = null;
                try
                {
                    cmd.Parameters.AddWithValue("@ctTipoGrupo", ctTipoGrupo);
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbMaestroGruposModel.FieldsOrdinal fields = new tbMaestroGruposModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbMaestroGruposModel model = new tbMaestroGruposModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }


        public tbMaestroGruposModel GetBycnID_GrupoLista(Int32 cnID_GrupoLista)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMaestroGrupos_GetBycnID_GrupoLista";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_GrupoLista", cnID_GrupoLista);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbMaestroGruposModel result = new tbMaestroGruposModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

      

        public bool GetBycnID_GrupoLista(Int32 cnID_GrupoLista, tbMaestroGruposModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMaestroGrupos_GetBycnID_GrupoLista";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_GrupoLista", cnID_GrupoLista);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbMaestroGruposModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMaestroGrupos_Update";

                    cmd.Parameters.AddWithValue("@cnID_GrupoLista", model.cnID_GrupoLista);
                    cmd.Parameters.AddWithValue("@ctNombreGrupo", model.ctNombreGrupo);
                    cmd.Parameters.AddWithValue("@cnEstado", model.cnEstado);

                    cmd.Parameters.AddWithValue("@ctTipoGrupo", model.ctTipoGrupo);
                    cmd.Parameters.AddWithValue("@cnGrupoPadre", model.cnGrupoPadre);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int32 Insert(tbMaestroGruposModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMaestroGrupos_Insert";

                    cmd.Parameters.AddWithValue("@ctNombreGrupo", model.ctNombreGrupo);
                    cmd.Parameters.AddWithValue("@cnEstado", model.cnEstado);

                    cmd.Parameters.AddWithValue("@ctTipoGrupo", model.ctTipoGrupo);

                    cmd.Parameters.AddWithValue("@cnGrupoPadre", model.cnGrupoPadre);

                    

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt32(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int32 cnID_GrupoLista)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMaestroGrupos_DeleteBycnID_GrupoLista";

                    cmd.Parameters.AddWithValue("@cnID_GrupoLista", cnID_GrupoLista);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbMaestroListas
    /// </summary>
    public partial class tbMaestroListasDAL : dbSeleccionDALBase
    {
        internal tbMaestroListasDAL()
        {
        }

        internal tbMaestroListasDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbMaestroListasModel> GetAll()
        {
            List<tbMaestroListasModel> result = new List<tbMaestroListasModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbMaestroListas_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbMaestroListasModel.FieldsOrdinal fields = new tbMaestroListasModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbMaestroListasModel model = new tbMaestroListasModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbMaestroListasModel GetBycnID_Lista(Int64 cnID_Lista)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMaestroListas_GetBycnID_Lista";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Lista", cnID_Lista);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbMaestroListasModel result = new tbMaestroListasModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_Lista(Int64 cnID_Lista, tbMaestroListasModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMaestroListas_GetBycnID_Lista";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Lista", cnID_Lista);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbMaestroListasModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMaestroListas_Update";

                    cmd.Parameters.AddWithValue("@cnID_Lista", model.cnID_Lista);
                    cmd.Parameters.AddWithValue("@cnID_GrupoLista", model.cnID_GrupoLista);
                    cmd.Parameters.AddWithValue("@cnID_ListaPadre", model.cnID_ListaPadre);
                    cmd.Parameters.AddWithValue("@ctElemento", model.ctElemento);
                    cmd.Parameters.AddWithValue("@cnEstado", model.cnEstado);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int64 Insert(tbMaestroListasModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMaestroListas_Insert";

                    cmd.Parameters.AddWithValue("@cnID_GrupoLista", model.cnID_GrupoLista);
                    cmd.Parameters.AddWithValue("@cnID_ListaPadre", model.cnID_ListaPadre);
                    cmd.Parameters.AddWithValue("@ctElemento", model.ctElemento);
                    cmd.Parameters.AddWithValue("@cnEstado", model.cnEstado);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt64(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnID_Lista)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMaestroListas_DeleteBycnID_Lista";

                    cmd.Parameters.AddWithValue("@cnID_Lista", cnID_Lista);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbMaestroListasModel> Ff___GetBycnID_Lista(Int64 cnID_Lista)
        {
            List<tbMaestroListasModel> result = new List<tbMaestroListasModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMaestroListas_GetBycnID_Lista";

                    cmd.Parameters.AddWithValue("@cnID_Lista", cnID_Lista);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbMaestroListasModel.FieldsOrdinal fields = new tbMaestroListasModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbMaestroListasModel model = new tbMaestroListasModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbMaestroListasModel> GetBycnID_GrupoLista(Int32 cnID_GrupoLista)
        {
            List<tbMaestroListasModel> result = new List<tbMaestroListasModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMaestroListas_GetBycnID_GrupoLista";

                    cmd.Parameters.AddWithValue("@cnID_GrupoLista", cnID_GrupoLista);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbMaestroListasModel.FieldsOrdinal fields = new tbMaestroListasModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbMaestroListasModel model = new tbMaestroListasModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbMaestroListasModel> GetBycnID_ListaPadre(Int32 cnID_ListaPadre)
        {
            List<tbMaestroListasModel> result = new List<tbMaestroListasModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMaestroListas_GetBycnID_ListaPadre";

                    cmd.Parameters.AddWithValue("@cnID_ListaPadre", cnID_ListaPadre);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbMaestroListasModel.FieldsOrdinal fields = new tbMaestroListasModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbMaestroListasModel model = new tbMaestroListasModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbMaestroListasModel> GetAreaTematicaNivelUltimo(Int32 cnID_ListaPadre, long cnID_Candidato)
        {
            List<tbMaestroListasModel> result = new List<tbMaestroListasModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMaestroListas_GetAreaTematicaNivelUltimo";

                    cmd.Parameters.AddWithValue("@cnID_ListaPadre", cnID_ListaPadre);
                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbMaestroListasModel.FieldsOrdinal fields = new tbMaestroListasModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbMaestroListasModel model = new tbMaestroListasModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbMenu
    /// </summary>
    public partial class tbMenuDAL : dbSeleccionDALBase
    {
        internal tbMenuDAL()
        {
        }

        internal tbMenuDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbMenuModel> GetAll()
        {
            List<tbMenuModel> result = new List<tbMenuModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbMenu_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbMenuModel.FieldsOrdinal fields = new tbMenuModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbMenuModel model = new tbMenuModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbMenuModel GetBycnID_Menu(Int64 cnID_Menu)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMenu_GetBycnID_Menu";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Menu", cnID_Menu);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbMenuModel result = new tbMenuModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_Menu(Int64 cnID_Menu, tbMenuModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMenu_GetBycnID_Menu";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Menu", cnID_Menu);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbMenuModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMenu_Update";

                    cmd.Parameters.AddWithValue("@cnID_Menu", model.cnID_Menu);
                    cmd.Parameters.AddWithValue("@cnID_Perfil", model.cnID_Perfil);
                    cmd.Parameters.AddWithValue("@ctTitulo", model.ctTitulo);
                    if (model.ctDescipcion != null)
                        cmd.Parameters.AddWithValue("@ctDescipcion", model.ctDescipcion);

                    cmd.Parameters.AddWithValue("@ctPagina", model.ctPagina);
                    cmd.Parameters.AddWithValue("@cnOrden", model.cnOrden);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int64 Insert(tbMenuModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMenu_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Perfil", model.cnID_Perfil);
                    cmd.Parameters.AddWithValue("@ctTitulo", model.ctTitulo);
                    if (model.ctDescipcion != null)
                        cmd.Parameters.AddWithValue("@ctDescipcion", model.ctDescipcion);

                    cmd.Parameters.AddWithValue("@ctPagina", model.ctPagina);
                    cmd.Parameters.AddWithValue("@cnOrden", model.cnOrden);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt64(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnID_Menu)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMenu_DeleteBycnID_Menu";

                    cmd.Parameters.AddWithValue("@cnID_Menu", cnID_Menu);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbMenuModel> GetBycnID_Perfil(Int32 cnID_Perfil)
        {
            List<tbMenuModel> result = new List<tbMenuModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbMenu_GetBycnID_Perfil";

                    cmd.Parameters.AddWithValue("@cnID_Perfil", cnID_Perfil);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbMenuModel.FieldsOrdinal fields = new tbMenuModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbMenuModel model = new tbMenuModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbPais
    /// </summary>
    public partial class tbPaisDAL : dbSeleccionDALBase
    {
        internal tbPaisDAL()
        {
        }

        internal tbPaisDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbPaisModel> GetAll()
        {
            List<tbPaisModel> result = new List<tbPaisModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbPais_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbPaisModel.FieldsOrdinal fields = new tbPaisModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbPaisModel model = new tbPaisModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbPaisModel GetBycnID_Pais(Int32 cnID_Pais)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPais_GetBycnID_Pais";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Pais", cnID_Pais);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbPaisModel result = new tbPaisModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_Pais(Int32 cnID_Pais, tbPaisModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPais_GetBycnID_Pais";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Pais", cnID_Pais);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbPaisModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPais_Update";

                    cmd.Parameters.AddWithValue("@cnID_Pais", model.cnID_Pais);
                    if (model.ctPais != null)
                        cmd.Parameters.AddWithValue("@ctPais", model.ctPais);

                    if (model.ctCodigoISO != null)
                        cmd.Parameters.AddWithValue("@ctCodigoISO", model.ctCodigoISO);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int32 Insert(tbPaisModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPais_Insert";

                    if (model.ctPais != null)
                        cmd.Parameters.AddWithValue("@ctPais", model.ctPais);

                    if (model.ctCodigoISO != null)
                        cmd.Parameters.AddWithValue("@ctCodigoISO", model.ctCodigoISO);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt32(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int32 cnID_Pais)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPais_DeleteBycnID_Pais";

                    cmd.Parameters.AddWithValue("@cnID_Pais", cnID_Pais);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbPerfilUsuario
    /// </summary>
    public partial class tbPerfilUsuarioDAL : dbSeleccionDALBase
    {
        internal tbPerfilUsuarioDAL()
        {
        }

        internal tbPerfilUsuarioDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbPerfilUsuarioModel> GetAll()
        {
            List<tbPerfilUsuarioModel> result = new List<tbPerfilUsuarioModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbPerfilUsuario_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbPerfilUsuarioModel.FieldsOrdinal fields = new tbPerfilUsuarioModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbPerfilUsuarioModel model = new tbPerfilUsuarioModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbPerfilUsuarioModel GetBycnID_Perfil(Int32 cnID_Perfil)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPerfilUsuario_GetBycnID_Perfil";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Perfil", cnID_Perfil);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbPerfilUsuarioModel result = new tbPerfilUsuarioModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_Perfil(Int32 cnID_Perfil, tbPerfilUsuarioModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPerfilUsuario_GetBycnID_Perfil";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Perfil", cnID_Perfil);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbPerfilUsuarioModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPerfilUsuario_Update";

                    cmd.Parameters.AddWithValue("@cnID_Perfil", model.cnID_Perfil);
                    cmd.Parameters.AddWithValue("@ctDescripcion", model.ctDescripcion);
                    cmd.Parameters.AddWithValue("@cnEstadoReg", model.cnEstadoReg);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Insert(tbPerfilUsuarioModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPerfilUsuario_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Perfil", model.cnID_Perfil);
                    cmd.Parameters.AddWithValue("@ctDescripcion", model.ctDescripcion);
                    cmd.Parameters.AddWithValue("@cnEstadoReg", model.cnEstadoReg);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int32 cnID_Perfil)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPerfilUsuario_DeleteBycnID_Perfil";

                    cmd.Parameters.AddWithValue("@cnID_Perfil", cnID_Perfil);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbPostulante
    /// </summary>
    public partial class tbPostulanteDAL : dbSeleccionDALBase
    {
        internal tbPostulanteDAL()
        {
        }

        internal tbPostulanteDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbPostulanteModel> GetAll()
        {
            List<tbPostulanteModel> result = new List<tbPostulanteModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbPostulante_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbPostulanteModel.FieldsOrdinal fields = new tbPostulanteModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbPostulanteModel model = new tbPostulanteModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbPostulanteModel GetBycnID_Postulante(Int64 cnID_Postulante)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulante_GetBycnID_Postulante";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Postulante", cnID_Postulante);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbPostulanteModel result = new tbPostulanteModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_Postulante(Int64 cnID_Postulante, tbPostulanteModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulante_GetBycnID_Postulante";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Postulante", cnID_Postulante);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbPostulanteModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulante_Update";

                    cmd.Parameters.AddWithValue("@cnID_Proceso", model.cnID_Proceso);
                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    cmd.Parameters.AddWithValue("@cnID_Postulante", model.cnID_Postulante);
                    cmd.Parameters.AddWithValue("@cnPuntosCriterioLaboral", model.cnPuntosCriterioLaboral);
                    cmd.Parameters.AddWithValue("@cnPuntosCriterioAcademico", model.cnPuntosCriterioAcademico);
                    cmd.Parameters.AddWithValue("@cnPuntosCriterioAdicional", model.cnPuntosCriterioAdicional);
                    cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    if (model.cfFechaActualizacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    cmd.Parameters.AddWithValue("@cnCalifica", model.cnEvalAuto);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int64 Insert(tbPostulanteModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulante_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Proceso", model.cnID_Proceso);
                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    cmd.Parameters.AddWithValue("@cnPuntosCriterioLaboral", model.cnPuntosCriterioLaboral);
                    cmd.Parameters.AddWithValue("@cnPuntosCriterioAcademico", model.cnPuntosCriterioAcademico);
                    cmd.Parameters.AddWithValue("@cnPuntosCriterioAdicional", model.cnPuntosCriterioAdicional);
                    cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    if (model.cfFechaActualizacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    cmd.Parameters.AddWithValue("@cnEstadoReg", model.cnEstadoReg);

                    //if (model.cnID_Grado > 0)
                    //    cmd.Parameters.AddWithValue("@cnID_Grado", model.cnID_Grado);

                    cmd.Parameters.AddWithValue("@cnEvalAuto", model.cnEvalAuto);
                    cmd.Parameters.AddWithValue("@cnEvalManual", model.cnEvalManual);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt64(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void AnularPostulacion(Int64 cnID_Candidato, Int64 cnID_Proceso)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulante_AnularPostulacion";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                    cmd.Parameters.AddWithValue("@cnID_Proceso", cnID_Proceso);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnID_Postulante)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulante_DeleteBycnID_Postulante";

                    cmd.Parameters.AddWithValue("@cnID_Postulante", cnID_Postulante);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbPostulanteModel> GetBycnID_Proceso(Int64 cnID_Proceso)
        {
            List<tbPostulanteModel> result = new List<tbPostulanteModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulante_GetBycnID_Proceso";

                    cmd.Parameters.AddWithValue("@cnID_Proceso", cnID_Proceso);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbPostulanteModel.FieldsOrdinal fields = new tbPostulanteModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbPostulanteModel model = new tbPostulanteModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbPostulanteModel> GetConsultaPostulantes(Int64 cnID_Proceso, Int32 cnTipoResultado,
                                                                string ctNumeroDNI, string ctEmail,
                                                                Int64 cnCobertura, Int64 cnZona)
        {
            List<tbPostulanteModel> result = new List<tbPostulanteModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulante_GetConsultaPostulantes";

                    cmd.Parameters.AddWithValue("@cnID_Proceso", cnID_Proceso);

                    if (ctEmail.Trim().Length > 0)
                        cmd.Parameters.AddWithValue("@ctEmail", ctEmail);

                    if (ctNumeroDNI.Trim().Length > 0)
                        cmd.Parameters.AddWithValue("@ctNumeroDNI", ctNumeroDNI);

                    if (cnTipoResultado > 0)
                        cmd.Parameters.AddWithValue("@cnTipoResultado", cnTipoResultado);

                    if (cnCobertura > 0)
                        cmd.Parameters.AddWithValue("@cnCobertura", cnCobertura);

                    if (cnZona > 0)
                        cmd.Parameters.AddWithValue("@cnZona", cnZona);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbPostulanteModel.FieldsOrdinal fields = new tbPostulanteModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbPostulanteModel model = new tbPostulanteModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbPostulanteModel> GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            List<tbPostulanteModel> result = new List<tbPostulanteModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulante_GetBycnID_Candidato";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbPostulanteModel.FieldsOrdinal fields = new tbPostulanteModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbPostulanteModel model = new tbPostulanteModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbPostulanteCobe
    /// </summary>
    public partial class tbPostulanteCobeDAL : dbSeleccionDALBase
    {
        internal tbPostulanteCobeDAL()
        {
        }

        internal tbPostulanteCobeDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbPostulanteCobeModel> GetAll()
        {
            List<tbPostulanteCobeModel> result = new List<tbPostulanteCobeModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbPostulanteCobe_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbPostulanteCobeModel.FieldsOrdinal fields = new tbPostulanteCobeModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbPostulanteCobeModel model = new tbPostulanteCobeModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbPostulanteCobeModel GetBycnID_PostulanteCobe(Int64 cnID_PostulanteCobe)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteCobe_GetBycnID_PostulanteCobe";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_PostulanteCobe", cnID_PostulanteCobe);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbPostulanteCobeModel result = new tbPostulanteCobeModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_PostulanteCobe(Int64 cnID_PostulanteCobe, tbPostulanteCobeModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteCobe_GetBycnID_PostulanteCobe";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_PostulanteCobe", cnID_PostulanteCobe);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbPostulanteCobeModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteCobe_Update";

                    cmd.Parameters.AddWithValue("@cnID_PostulanteCobe", model.cnID_PostulanteCobe);
                    cmd.Parameters.AddWithValue("@cnID_Postulante", model.cnID_Postulante);
                    cmd.Parameters.AddWithValue("@cnID_Cobertura", model.cnID_Cobertura);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int64 Insert(tbPostulanteCobeModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteCobe_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Postulante", model.cnID_Postulante);
                    cmd.Parameters.AddWithValue("@cnID_Cobertura", model.cnID_Cobertura);
                    cmd.Parameters.AddWithValue("@cnID_CoberturaZona", model.cnID_CoberturaZona);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt64(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnID_PostulanteCobe)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteCobe_DeleteBycnID_PostulanteCobe";

                    cmd.Parameters.AddWithValue("@cnID_PostulanteCobe", cnID_PostulanteCobe);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbPostulanteCobeModel> GetBycnID_Postulante(Int64 cnID_Postulante)
        {
            List<tbPostulanteCobeModel> result = new List<tbPostulanteCobeModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteCobe_GetBycnID_Postulante";

                    cmd.Parameters.AddWithValue("@cnID_Postulante", cnID_Postulante);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbPostulanteCobeModel.FieldsOrdinal fields = new tbPostulanteCobeModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbPostulanteCobeModel model = new tbPostulanteCobeModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbPostulanteDocu
    /// </summary>
    public partial class tbPostulanteDocuDAL : dbSeleccionDALBase
    {
        internal tbPostulanteDocuDAL()
        {
        }

        internal tbPostulanteDocuDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbPostulanteDocuModel> GetAll()
        {
            List<tbPostulanteDocuModel> result = new List<tbPostulanteDocuModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbPostulanteDocu_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbPostulanteDocuModel.FieldsOrdinal fields = new tbPostulanteDocuModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbPostulanteDocuModel model = new tbPostulanteDocuModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbPostulanteDocuModel GetBycnID_PostulanteDocu(Int64 cnID_PostulanteDocu)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteDocu_GetBycnID_PostulanteDocu";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_PostulanteDocu", cnID_PostulanteDocu);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbPostulanteDocuModel result = new tbPostulanteDocuModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_PostulanteDocu(Int64 cnID_PostulanteDocu, tbPostulanteDocuModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteDocu_GetBycnID_PostulanteDocu";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_PostulanteDocu", cnID_PostulanteDocu);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbPostulanteDocuModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteDocu_Update";

                    cmd.Parameters.AddWithValue("@cnID_PostulanteDocu", model.cnID_PostulanteDocu);
                    cmd.Parameters.AddWithValue("@cnID_Postulante", model.cnID_Postulante);
                    cmd.Parameters.AddWithValue("@cnID_Documento", model.cnID_Documento);
                    if (model.ctRutaArchivo != null)
                        cmd.Parameters.AddWithValue("@ctRutaArchivo", model.ctRutaArchivo);

                    //cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    //if (model.cfFechaActualizacion.HasValue)
                    //    cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    //cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int64 Insert(tbPostulanteDocuModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteDocu_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Postulante", model.cnID_Postulante);
                    cmd.Parameters.AddWithValue("@cnID_Documento", model.cnID_Documento);
                    if (model.ctRutaArchivo != null)
                        cmd.Parameters.AddWithValue("@ctRutaArchivo", model.ctRutaArchivo);

                    //cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    //if (model.cfFechaActualizacion.HasValue)
                    //    cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    //cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt64(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnID_PostulanteDocu)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteDocu_DeleteBycnID_PostulanteDocu";

                    cmd.Parameters.AddWithValue("@cnID_PostulanteDocu", cnID_PostulanteDocu);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbPostulanteDocuModel> GetBycnID_Postulante(Int64 cnID_Postulante)
        {
            List<tbPostulanteDocuModel> result = new List<tbPostulanteDocuModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteDocu_GetBycnID_Postulante";

                    cmd.Parameters.AddWithValue("@cnID_Postulante", cnID_Postulante);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbPostulanteDocuModel.FieldsOrdinal fields = new tbPostulanteDocuModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbPostulanteDocuModel model = new tbPostulanteDocuModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbPostulanteLabo
    /// </summary>
    public partial class tbPostulanteLaboDAL : dbSeleccionDALBase
    {
        internal tbPostulanteLaboDAL()
        {
        }

        internal tbPostulanteLaboDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbPostulanteLaboModel> GetAll()
        {
            List<tbPostulanteLaboModel> result = new List<tbPostulanteLaboModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbPostulanteLabo_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbPostulanteLaboModel.FieldsOrdinal fields = new tbPostulanteLaboModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbPostulanteLaboModel model = new tbPostulanteLaboModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbPostulanteLaboModel GetBycnID_PostulanteLabo(Int64 cnID_PostulanteLabo)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteLabo_GetBycnID_PostulanteLabo";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_PostulanteLabo", cnID_PostulanteLabo);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbPostulanteLaboModel result = new tbPostulanteLaboModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_PostulanteLabo(Int64 cnID_PostulanteLabo, tbPostulanteLaboModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteLabo_GetBycnID_PostulanteLabo";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_PostulanteLabo", cnID_PostulanteLabo);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbPostulanteLaboModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteLabo_Update";

                    cmd.Parameters.AddWithValue("@cnID_PostulanteLabo", model.cnID_PostulanteLabo);
                    cmd.Parameters.AddWithValue("@cnID_Postulante", model.cnID_Postulante);
                    cmd.Parameters.AddWithValue("@cnID_Experiencia", model.cnID_Experiencia);
                    cmd.Parameters.AddWithValue("@cnTiempo", model.cnTiempo);
                    cmd.Parameters.AddWithValue("@cnID_TipoCriterioLabo", model.cnID_TipoCriterioLabo);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int64 Insert(tbPostulanteLaboModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteLabo_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Postulante", model.cnID_Postulante);
                    cmd.Parameters.AddWithValue("@cnID_Experiencia", model.cnID_Experiencia);
                    cmd.Parameters.AddWithValue("@cnTiempoAnios", model.cnTiempo);

                    cmd.Parameters.AddWithValue("@cnID_TipoCriterioLabo", model.cnID_TipoCriterioLabo);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt64(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnID_PostulanteLabo)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteLabo_DeleteBycnID_PostulanteLabo";

                    cmd.Parameters.AddWithValue("@cnID_PostulanteLabo", cnID_PostulanteLabo);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbPostulanteLaboModel> GetBycnID_Postulante(Int64 cnID_Postulante)
        {
            List<tbPostulanteLaboModel> result = new List<tbPostulanteLaboModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteLabo_GetBycnID_Postulante";

                    cmd.Parameters.AddWithValue("@cnID_Postulante", cnID_Postulante);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbPostulanteLaboModel.FieldsOrdinal fields = new tbPostulanteLaboModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbPostulanteLaboModel model = new tbPostulanteLaboModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbProceso
    /// </summary>
    public partial class tbProcesoDAL : dbSeleccionDALBase
    {
        internal tbProcesoDAL()
        {
        }

        internal tbProcesoDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbProcesoModel> GetAll()
        {
            List<tbProcesoModel> result = new List<tbProcesoModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbProceso_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbProcesoModel.FieldsOrdinal fields = new tbProcesoModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbProcesoModel model = new tbProcesoModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        //
        public List<tbProcesoModel> GetProcesosVigentes()
        {
            List<tbProcesoModel> result = new List<tbProcesoModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbProceso_GetProcesosVigentes";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbProcesoModel.FieldsOrdinal fields = new tbProcesoModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbProcesoModel model = new tbProcesoModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbProcesoModel GetBycnID_Proceso(Int64 cnID_Proceso)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProceso_GetBycnID_Proceso";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Proceso", cnID_Proceso);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbProcesoModel result = new tbProcesoModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_Proceso(Int64 cnID_Proceso, tbProcesoModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProceso_GetBycnID_Proceso";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Proceso", cnID_Proceso);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbProcesoModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProceso_Update";

                    cmd.Parameters.AddWithValue("@cnID_Proceso", model.cnID_Proceso);
                    cmd.Parameters.AddWithValue("@ctTitulo", model.ctTitulo);
                    cmd.Parameters.AddWithValue("@ctDescripcion", model.ctDescripcion);
                    //cmd.Parameters.AddWithValue("@cnID_Tipo", model.cnID_Tipo);
                    //cmd.Parameters.AddWithValue("@cnID_Cobertura", model.cnID_Cobertura);
                    cmd.Parameters.AddWithValue("@cfFechaInicio", model.cfFechaInicio);
                    cmd.Parameters.AddWithValue("@cfFechaFin", model.cfFechaFin);
                    cmd.Parameters.AddWithValue("@cnID_Estado", model.cnID_Estado);
                    if (model.ctArchivoTerminos != null)
                        cmd.Parameters.AddWithValue("@ctArchivoTerminos", model.ctArchivoTerminos);

                    if (model.ctArchivoProceso != null)
                        cmd.Parameters.AddWithValue("@ctArchivoProceso", model.ctArchivoProceso);

                    cmd.Parameters.AddWithValue("@cnIndEvaluacion", model.cnIndEvaluacion);

                    cmd.Parameters.AddWithValue("@ctUsuarioCreador", model.ctUsuarioCreador);
                    cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    if (model.cfFechaActualizacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int64 Insert(tbProcesoModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProceso_Insert";

                    cmd.Parameters.AddWithValue("@ctTitulo", model.ctTitulo);
                    cmd.Parameters.AddWithValue("@ctDescripcion", model.ctDescripcion);
                    //cmd.Parameters.AddWithValue("@cnID_Tipo", model.cnID_Tipo);
                    //cmd.Parameters.AddWithValue("@cnID_Cobertura", model.cnID_Cobertura);
                    cmd.Parameters.AddWithValue("@cfFechaInicio", model.cfFechaInicio);
                    cmd.Parameters.AddWithValue("@cfFechaFin", model.cfFechaFin);
                    cmd.Parameters.AddWithValue("@cnID_Estado", model.cnID_Estado);
                    if (model.ctArchivoTerminos != null)
                        cmd.Parameters.AddWithValue("@ctArchivoTerminos", model.ctArchivoTerminos);

                    if (model.ctArchivoProceso != null)
                        cmd.Parameters.AddWithValue("@ctArchivoProceso", model.ctArchivoProceso);

                    cmd.Parameters.AddWithValue("@cnIndEvaluacion", model.cnIndEvaluacion);

                    cmd.Parameters.AddWithValue("@ctUsuarioCreador", model.ctUsuarioCreador);
                    cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    if (model.cfFechaActualizacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt64(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnID_Proceso)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProceso_DeleteBycnID_Proceso";

                    cmd.Parameters.AddWithValue("@cnID_Proceso", cnID_Proceso);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbProcesoModel> GetBycnID_Tipo(Int64 cnID_Tipo)
        {
            List<tbProcesoModel> result = new List<tbProcesoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProceso_GetBycnID_Tipo";

                    cmd.Parameters.AddWithValue("@cnID_Tipo", cnID_Tipo);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbProcesoModel.FieldsOrdinal fields = new tbProcesoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbProcesoModel model = new tbProcesoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbProcesoModel> GetBycnID_Cobertura(Int32 cnID_Cobertura)
        {
            List<tbProcesoModel> result = new List<tbProcesoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProceso_GetBycnID_Cobertura";

                    cmd.Parameters.AddWithValue("@cnID_Cobertura", cnID_Cobertura);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbProcesoModel.FieldsOrdinal fields = new tbProcesoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbProcesoModel model = new tbProcesoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbProcesoModel> GetBycnID_Estado(Int64 cnID_Estado)
        {
            List<tbProcesoModel> result = new List<tbProcesoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProceso_GetBycnID_Estado";

                    cmd.Parameters.AddWithValue("@cnID_Estado", cnID_Estado);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbProcesoModel.FieldsOrdinal fields = new tbProcesoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbProcesoModel model = new tbProcesoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbProcesoModel> GetBycfFechaInicio(DateTime cfFechaInicio)
        {
            List<tbProcesoModel> result = new List<tbProcesoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProceso_GetBycfFechaInicio";

                    cmd.Parameters.AddWithValue("@cfFechaInicio", cfFechaInicio);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbProcesoModel.FieldsOrdinal fields = new tbProcesoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbProcesoModel model = new tbProcesoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbProcesoModel> GetBycfFechaFin(DateTime cfFechaFin)
        {
            List<tbProcesoModel> result = new List<tbProcesoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProceso_GetBycfFechaFin";

                    cmd.Parameters.AddWithValue("@cfFechaFin", cfFechaFin);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbProcesoModel.FieldsOrdinal fields = new tbProcesoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbProcesoModel model = new tbProcesoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbProcesoCnfgAcad
    /// </summary>
    public partial class tbProcesoCnfgAcadDAL : dbSeleccionDALBase
    {
        internal tbProcesoCnfgAcadDAL()
        {
        }

        internal tbProcesoCnfgAcadDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbProcesoCnfgAcadModel> GetAll()
        {
            List<tbProcesoCnfgAcadModel> result = new List<tbProcesoCnfgAcadModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbProcesoCnfgAcad_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbProcesoCnfgAcadModel.FieldsOrdinal fields = new tbProcesoCnfgAcadModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbProcesoCnfgAcadModel model = new tbProcesoCnfgAcadModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbProcesoCnfgAcadModel GetBycnID_CnfgAcademica(Int32 cnID_CnfgAcademica)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCnfgAcad_GetBycnID_CnfgAcademica";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_CnfgAcademica", cnID_CnfgAcademica);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbProcesoCnfgAcadModel result = new tbProcesoCnfgAcadModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_CnfgAcademica(Int32 cnID_CnfgAcademica, tbProcesoCnfgAcadModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCnfgAcad_GetBycnID_CnfgAcademica";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_CnfgAcademica", cnID_CnfgAcademica);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbProcesoCnfgAcadModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCnfgAcad_Update";

                    cmd.Parameters.AddWithValue("@cnID_CnfgAcademica", model.cnID_CnfgAcademica);
                    cmd.Parameters.AddWithValue("@cnID_Proceso", model.cnID_Proceso);
                    cmd.Parameters.AddWithValue("@cnID_Grado", model.cnID_Grado);
                    cmd.Parameters.AddWithValue("@cnID_Situacion", model.cnID_Situacion);
                    if (model.cnID_OperadorLogico.HasValue)
                        cmd.Parameters.AddWithValue("@cnID_OperadorLogico", model.cnID_OperadorLogico.Value);

                    if (model.cnGrupoLogico.HasValue)
                        cmd.Parameters.AddWithValue("@cnGrupoLogico", model.cnGrupoLogico.Value);

                    cmd.Parameters.AddWithValue("@cnPuntaje", model.cnPuntaje);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int32 Insert(tbProcesoCnfgAcadModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCnfgAcad_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Proceso", model.cnID_Proceso);
                    cmd.Parameters.AddWithValue("@cnID_Grado", model.cnID_Grado);
                    cmd.Parameters.AddWithValue("@cnID_Situacion", model.cnID_Situacion);
                    if (model.cnID_OperadorLogico.HasValue)
                        cmd.Parameters.AddWithValue("@cnID_OperadorLogico", model.cnID_OperadorLogico.Value);

                    if (model.cnGrupoLogico.HasValue)
                        cmd.Parameters.AddWithValue("@cnGrupoLogico", model.cnGrupoLogico.Value);

                    cmd.Parameters.AddWithValue("@cnPuntaje", model.cnPuntaje);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt32(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int32 cnID_CnfgAcademica)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCnfgAcad_DeleteBycnID_CnfgAcademica";

                    cmd.Parameters.AddWithValue("@cnID_CnfgAcademica", cnID_CnfgAcademica);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbProcesoCnfgAcadModel> GetBycnID_Proceso(Int64 cnID_Proceso)
        {
            List<tbProcesoCnfgAcadModel> result = new List<tbProcesoCnfgAcadModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCnfgAcad_GetBycnID_Proceso";

                    cmd.Parameters.AddWithValue("@cnID_Proceso", cnID_Proceso);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbProcesoCnfgAcadModel.FieldsOrdinal fields = new tbProcesoCnfgAcadModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbProcesoCnfgAcadModel model = new tbProcesoCnfgAcadModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbProcesoCnfgAcadModel> GetBycnID_Grado(Int64 cnID_Grado)
        {
            List<tbProcesoCnfgAcadModel> result = new List<tbProcesoCnfgAcadModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCnfgAcad_GetBycnID_Grado";

                    cmd.Parameters.AddWithValue("@cnID_Grado", cnID_Grado);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbProcesoCnfgAcadModel.FieldsOrdinal fields = new tbProcesoCnfgAcadModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbProcesoCnfgAcadModel model = new tbProcesoCnfgAcadModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbProcesoCnfgAcadModel> GetBycnID_Situacion(Int64 cnID_Situacion)
        {
            List<tbProcesoCnfgAcadModel> result = new List<tbProcesoCnfgAcadModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCnfgAcad_GetBycnID_Situacion";

                    cmd.Parameters.AddWithValue("@cnID_Situacion", cnID_Situacion);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbProcesoCnfgAcadModel.FieldsOrdinal fields = new tbProcesoCnfgAcadModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbProcesoCnfgAcadModel model = new tbProcesoCnfgAcadModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbProcesoCnfgLabo
    /// </summary>
    public partial class tbProcesoCnfgLaboDAL : dbSeleccionDALBase
    {
        internal tbProcesoCnfgLaboDAL()
        {
        }

        internal tbProcesoCnfgLaboDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbProcesoCnfgLaboModel> GetAll()
        {
            List<tbProcesoCnfgLaboModel> result = new List<tbProcesoCnfgLaboModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbProcesoCnfgLabo_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbProcesoCnfgLaboModel.FieldsOrdinal fields = new tbProcesoCnfgLaboModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbProcesoCnfgLaboModel model = new tbProcesoCnfgLaboModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbProcesoCnfgLaboModel GetBycnID_CnfgLaboral(Int32 cnID_CnfgLaboral)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCnfgLabo_GetBycnID_CnfgLaboral";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_CnfgLaboral", cnID_CnfgLaboral);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbProcesoCnfgLaboModel result = new tbProcesoCnfgLaboModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_CnfgLaboral(Int32 cnID_CnfgLaboral, tbProcesoCnfgLaboModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCnfgLabo_GetBycnID_CnfgLaboral";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_CnfgLaboral", cnID_CnfgLaboral);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbProcesoCnfgLaboModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCnfgLabo_Update";

                    cmd.Parameters.AddWithValue("@cnID_CnfgLaboral", model.cnID_CnfgLaboral);
                    cmd.Parameters.AddWithValue("@cnID_Proceso", model.cnID_Proceso);
                    cmd.Parameters.AddWithValue("@cnID_TipoEntidad", model.cnID_TipoEntidad);
                    cmd.Parameters.AddWithValue("@ctDescripcion", model.ctDescripcion);
                    cmd.Parameters.AddWithValue("@cnID_TipoCriterioLabo", model.cnID_TipoCriterioLabo);
                    cmd.Parameters.AddWithValue("@cnID_TipoEvaluacion", model.cnID_TipoEvaluacion);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int32 Insert(tbProcesoCnfgLaboModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCnfgLabo_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Proceso", model.cnID_Proceso);
                    cmd.Parameters.AddWithValue("@cnID_TipoEntidad", model.cnID_TipoEntidad);
                    cmd.Parameters.AddWithValue("@ctDescripcion", model.ctDescripcion);
                    cmd.Parameters.AddWithValue("@cnID_TipoCriterioLabo", model.cnID_TipoCriterioLabo);
                    cmd.Parameters.AddWithValue("@cnID_TipoEvaluacion", model.cnID_TipoEvaluacion);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt32(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int32 cnID_CnfgLaboral)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCnfgLabo_DeleteBycnID_CnfgLaboral";

                    cmd.Parameters.AddWithValue("@cnID_CnfgLaboral", cnID_CnfgLaboral);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbProcesoCnfgLaboModel> GetBycnID_Proceso(Int64 cnID_Proceso)
        {
            List<tbProcesoCnfgLaboModel> result = new List<tbProcesoCnfgLaboModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCnfgLabo_GetBycnID_Proceso";

                    cmd.Parameters.AddWithValue("@cnID_Proceso", cnID_Proceso);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbProcesoCnfgLaboModel.FieldsOrdinal fields = new tbProcesoCnfgLaboModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbProcesoCnfgLaboModel model = new tbProcesoCnfgLaboModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbProcesoCnfgLaboModel> GetBycnID_Tipo(Int64 cnID_Tipo)
        {
            List<tbProcesoCnfgLaboModel> result = new List<tbProcesoCnfgLaboModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCnfgLabo_GetBycnID_Tipo";

                    cmd.Parameters.AddWithValue("@cnID_Tipo", cnID_Tipo);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbProcesoCnfgLaboModel.FieldsOrdinal fields = new tbProcesoCnfgLaboModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbProcesoCnfgLaboModel model = new tbProcesoCnfgLaboModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbProcesoCobertura
    /// </summary>
    public partial class tbProcesoCoberturaDAL : dbSeleccionDALBase
    {
        internal tbProcesoCoberturaDAL()
        {
        }

        internal tbProcesoCoberturaDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbProcesoCoberturaModel> GetAll()
        {
            List<tbProcesoCoberturaModel> result = new List<tbProcesoCoberturaModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbProcesoCobertura_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbProcesoCoberturaModel.FieldsOrdinal fields = new tbProcesoCoberturaModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbProcesoCoberturaModel model = new tbProcesoCoberturaModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbProcesoCoberturaModel GetBycnID_Cobertura(Int32 cnID_Cobertura)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCobertura_GetBycnID_Cobertura";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Cobertura", cnID_Cobertura);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbProcesoCoberturaModel result = new tbProcesoCoberturaModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbProcesoCoberturaModel> GetBycnID_Proceso(long cnID_Proceso)
        {
            List<tbProcesoCoberturaModel> result = new List<tbProcesoCoberturaModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbProcesoCobertura_GetBycnID_Proceso";

                IDataReader reader = null;
                try
                {
                    cmd.Parameters.AddWithValue("@cnID_Proceso", cnID_Proceso);

                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbProcesoCoberturaModel.FieldsOrdinal fields = new tbProcesoCoberturaModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbProcesoCoberturaModel model = new tbProcesoCoberturaModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public bool GetBycnID_Cobertura(Int32 cnID_Cobertura, tbProcesoCoberturaModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCobertura_GetBycnID_Cobertura";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Cobertura", cnID_Cobertura);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbProcesoCoberturaModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCobertura_Update";

                    cmd.Parameters.AddWithValue("@cnID_Cobertura", model.cnID_Cobertura);
                    cmd.Parameters.AddWithValue("@cnID_Proceso", model.cnID_Proceso);
                    cmd.Parameters.AddWithValue("@ctNombre", model.ctNombre);
                    //if (model.ctCobertura != null)
                    //    cmd.Parameters.AddWithValue("@ctCobertura", model.ctCobertura);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int32 Insert(tbProcesoCoberturaModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCobertura_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Proceso", model.cnID_Proceso);
                    cmd.Parameters.AddWithValue("@ctNombre", model.ctNombre);
                    //if (model.ctCobertura != null)
                    //    cmd.Parameters.AddWithValue("@ctCobertura", model.ctCobertura);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt32(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int32 cnID_Cobertura)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCobertura_DeleteBycnID_Cobertura";

                    cmd.Parameters.AddWithValue("@cnID_Cobertura", cnID_Cobertura);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbProcesoCriterioAdic
    /// </summary>
    public partial class tbProcesoCriterioAdicDAL : dbSeleccionDALBase
    {
        internal tbProcesoCriterioAdicDAL()
        {
        }

        internal tbProcesoCriterioAdicDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbProcesoCriterioAdicModel> GetAll()
        {
            List<tbProcesoCriterioAdicModel> result = new List<tbProcesoCriterioAdicModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbProcesoCriterioAdic_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbProcesoCriterioAdicModel.FieldsOrdinal fields = new tbProcesoCriterioAdicModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbProcesoCriterioAdicModel model = new tbProcesoCriterioAdicModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbProcesoCriterioAdicModel GetBycnID_CriterioAdicional(Int32 cnID_CriterioAdicional)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCriterioAdic_GetBycnID_CriterioAdicional";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_CriterioAdicional", cnID_CriterioAdicional);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbProcesoCriterioAdicModel result = new tbProcesoCriterioAdicModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_CriterioAdicional(Int32 cnID_CriterioAdicional, tbProcesoCriterioAdicModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCriterioAdic_GetBycnID_CriterioAdicional";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_CriterioAdicional", cnID_CriterioAdicional);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbProcesoCriterioAdicModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCriterioAdic_Update";

                    cmd.Parameters.AddWithValue("@cnID_CriterioAdicional", model.cnID_CriterioAdicional);
                    cmd.Parameters.AddWithValue("@cnPuntaje", model.cnPuntaje);
                    if (model.ctDescripcion != null)
                        cmd.Parameters.AddWithValue("@ctDescripcion", model.ctDescripcion);

                    cmd.Parameters.AddWithValue("@cnID_Proceso", model.cnID_Proceso);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int32 Insert(tbProcesoCriterioAdicModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCriterioAdic_Insert";

                    cmd.Parameters.AddWithValue("@cnPuntaje", model.cnPuntaje);
                    if (model.ctDescripcion != null)
                        cmd.Parameters.AddWithValue("@ctDescripcion", model.ctDescripcion);

                    cmd.Parameters.AddWithValue("@cnID_Proceso", model.cnID_Proceso);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt32(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int32 cnID_CriterioAdicional)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCriterioAdic_DeleteBycnID_CriterioAdicional";

                    cmd.Parameters.AddWithValue("@cnID_CriterioAdicional", cnID_CriterioAdicional);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbProcesoCriterioAdicModel> GetBycnID_Proceso(Int64 cnID_Proceso)
        {
            List<tbProcesoCriterioAdicModel> result = new List<tbProcesoCriterioAdicModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCriterioAdic_GetBycnID_Proceso";

                    cmd.Parameters.AddWithValue("@cnID_Proceso", cnID_Proceso);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbProcesoCriterioAdicModel.FieldsOrdinal fields = new tbProcesoCriterioAdicModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbProcesoCriterioAdicModel model = new tbProcesoCriterioAdicModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbProcesoCriterioAdicModel> GetByctDescripcion(String ctDescripcion)
        {
            List<tbProcesoCriterioAdicModel> result = new List<tbProcesoCriterioAdicModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoCriterioAdic_GetByctDescripcion";

                    cmd.Parameters.AddWithValue("@ctDescripcion", ctDescripcion);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbProcesoCriterioAdicModel.FieldsOrdinal fields = new tbProcesoCriterioAdicModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbProcesoCriterioAdicModel model = new tbProcesoCriterioAdicModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbProcesoDocumento
    /// </summary>
    public partial class tbProcesoDocumentoDAL : dbSeleccionDALBase
    {
        internal tbProcesoDocumentoDAL()
        {
        }

        internal tbProcesoDocumentoDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbProcesoDocumentoModel> GetAll()
        {
            List<tbProcesoDocumentoModel> result = new List<tbProcesoDocumentoModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbProcesoDocumento_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbProcesoDocumentoModel.FieldsOrdinal fields = new tbProcesoDocumentoModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbProcesoDocumentoModel model = new tbProcesoDocumentoModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbProcesoDocumentoModel GetBycnID_Documento(Int32 cnID_Documento)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoDocumento_GetBycnID_Documento";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Documento", cnID_Documento);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbProcesoDocumentoModel result = new tbProcesoDocumentoModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_Documento(Int32 cnID_Documento, tbProcesoDocumentoModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoDocumento_GetBycnID_Documento";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Documento", cnID_Documento);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbProcesoDocumentoModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoDocumento_Update";

                    cmd.Parameters.AddWithValue("@cnID_Documento", model.cnID_Documento);
                    cmd.Parameters.AddWithValue("@cnID_Proceso", model.cnID_Proceso);
                    cmd.Parameters.AddWithValue("@ctDescripcion", model.ctDescripcion);
                    cmd.Parameters.AddWithValue("@cnIndObligatorio", model.cnIndObligatorio);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int32 Insert(tbProcesoDocumentoModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoDocumento_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Proceso", model.cnID_Proceso);
                    cmd.Parameters.AddWithValue("@ctDescripcion", model.ctDescripcion);
                    cmd.Parameters.AddWithValue("@cnIndObligatorio", model.cnIndObligatorio);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt32(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int32 cnID_Documento)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoDocumento_DeleteBycnID_Documento";

                    cmd.Parameters.AddWithValue("@cnID_Documento", cnID_Documento);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbProcesoDocumentoModel> GetBycnID_Proceso(Int64 cnID_Proceso)
        {
            List<tbProcesoDocumentoModel> result = new List<tbProcesoDocumentoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoDocumento_GetBycnID_Proceso";

                    cmd.Parameters.AddWithValue("@cnID_Proceso", cnID_Proceso);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbProcesoDocumentoModel.FieldsOrdinal fields = new tbProcesoDocumentoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbProcesoDocumentoModel model = new tbProcesoDocumentoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbProcesoMatrizLabo
    /// </summary>
    public partial class tbProcesoMatrizLaboDAL : dbSeleccionDALBase
    {
        internal tbProcesoMatrizLaboDAL()
        {
        }

        internal tbProcesoMatrizLaboDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbProcesoMatrizLaboModel> GetAll()
        {
            List<tbProcesoMatrizLaboModel> result = new List<tbProcesoMatrizLaboModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbProcesoMatrizLabo_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbProcesoMatrizLaboModel.FieldsOrdinal fields = new tbProcesoMatrizLaboModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbProcesoMatrizLaboModel model = new tbProcesoMatrizLaboModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbProcesoMatrizLaboModel GetBycnID_MatrizLaboral(Int32 cnID_MatrizLaboral)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoMatrizLabo_GetBycnID_MatrizLaboral";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_MatrizLaboral", cnID_MatrizLaboral);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbProcesoMatrizLaboModel result = new tbProcesoMatrizLaboModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_MatrizLaboral(Int32 cnID_MatrizLaboral, tbProcesoMatrizLaboModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoMatrizLabo_GetBycnID_MatrizLaboral";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_MatrizLaboral", cnID_MatrizLaboral);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbProcesoMatrizLaboModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoMatrizLabo_Update";

                    cmd.Parameters.AddWithValue("@cnID_MatrizLaboral", model.cnID_MatrizLaboral);
                    cmd.Parameters.AddWithValue("@cnID_CnfgLaboral", model.cnID_CnfgLaboral);
                    //cmd.Parameters.AddWithValue("@cnID_Proceso", model.cnID_Proceso);
                    //cmd.Parameters.AddWithValue("@cnID_TipoEvaluacion", model.cnID_TipoEvaluacion);
                    cmd.Parameters.AddWithValue("@cnMininmo", model.cnMininmo);
                    cmd.Parameters.AddWithValue("@cnMaximo", model.cnMaximo);
                    cmd.Parameters.AddWithValue("@cnPuntaje", model.cnPuntaje);
                    //cmd.Parameters.AddWithValue("@ctDescripcion", model.ctDescripcion);
                    //cmd.Parameters.AddWithValue("@cnID_TipoCriterioLabo", model.cnID_TipoCriterioLabo);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int32 Insert(tbProcesoMatrizLaboModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoMatrizLabo_Insert";

                    cmd.Parameters.AddWithValue("@cnID_CnfgLaboral", model.cnID_CnfgLaboral);
                    //cmd.Parameters.AddWithValue("@cnID_Proceso", model.cnID_Proceso);
                    //cmd.Parameters.AddWithValue("@cnID_TipoEvaluacion", model.cnID_TipoEvaluacion);
                    cmd.Parameters.AddWithValue("@cnMininmo", model.cnMininmo);
                    cmd.Parameters.AddWithValue("@cnMaximo", model.cnMaximo);
                    cmd.Parameters.AddWithValue("@cnPuntaje", model.cnPuntaje);
                    //cmd.Parameters.AddWithValue("@ctDescripcion", model.ctDescripcion);
                    //cmd.Parameters.AddWithValue("@cnID_TipoCriterioLabo", model.cnID_TipoCriterioLabo);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt32(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int32 cnID_MatrizLaboral)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoMatrizLabo_DeleteBycnID_MatrizLaboral";

                    cmd.Parameters.AddWithValue("@cnID_MatrizLaboral", cnID_MatrizLaboral);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbProcesoMatrizLaboModel> GetBycnID_CnfgLaboral(Int32 cnID_CnfgLaboral)
        {
            List<tbProcesoMatrizLaboModel> result = new List<tbProcesoMatrizLaboModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoMatrizLabo_GetBycnID_CnfgLaboral";

                    cmd.Parameters.AddWithValue("@cnID_CnfgLaboral", cnID_CnfgLaboral);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbProcesoMatrizLaboModel.FieldsOrdinal fields = new tbProcesoMatrizLaboModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbProcesoMatrizLaboModel model = new tbProcesoMatrizLaboModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbProcesoMatrizLaboModel> GetBycnID_Proceso(Int64 cnID_Proceso)
        {
            List<tbProcesoMatrizLaboModel> result = new List<tbProcesoMatrizLaboModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoMatrizLabo_GetBycnID_Proceso";

                    cmd.Parameters.AddWithValue("@cnID_Proceso", cnID_Proceso);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbProcesoMatrizLaboModel.FieldsOrdinal fields = new tbProcesoMatrizLaboModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbProcesoMatrizLaboModel model = new tbProcesoMatrizLaboModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbProcesoMatrizLaboModel> GetBycnID_TipoEvaluacion(Int64 cnID_TipoEvaluacion)
        {
            List<tbProcesoMatrizLaboModel> result = new List<tbProcesoMatrizLaboModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProcesoMatrizLabo_GetBycnID_TipoEvaluacion";

                    cmd.Parameters.AddWithValue("@cnID_TipoEvaluacion", cnID_TipoEvaluacion);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbProcesoMatrizLaboModel.FieldsOrdinal fields = new tbProcesoMatrizLaboModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbProcesoMatrizLaboModel model = new tbProcesoMatrizLaboModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbProvincia
    /// </summary>
    public partial class tbProvinciaDAL : dbSeleccionDALBase
    {
        internal tbProvinciaDAL()
        {
        }

        internal tbProvinciaDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbProvinciaModel> GetAll()
        {
            List<tbProvinciaModel> result = new List<tbProvinciaModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbProvincia_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbProvinciaModel.FieldsOrdinal fields = new tbProvinciaModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbProvinciaModel model = new tbProvinciaModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbProvinciaModel GetBycnID_Provincia(Int32 cnID_Provincia)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProvincia_GetBycnID_Provincia";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Provincia", cnID_Provincia);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbProvinciaModel result = new tbProvinciaModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_Provincia(Int32 cnID_Provincia, tbProvinciaModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProvincia_GetBycnID_Provincia";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Provincia", cnID_Provincia);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbProvinciaModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProvincia_Update";

                    cmd.Parameters.AddWithValue("@cnID_Provincia", model.cnID_Provincia);
                    cmd.Parameters.AddWithValue("@cnID_Departamento", model.cnID_Departamento);
                    if (model.ctProvincia != null)
                        cmd.Parameters.AddWithValue("@ctProvincia", model.ctProvincia);

                    if (model.ctCodigoISO != null)
                        cmd.Parameters.AddWithValue("@ctCodigoISO", model.ctCodigoISO);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int32 Insert(tbProvinciaModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProvincia_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Departamento", model.cnID_Departamento);
                    if (model.ctProvincia != null)
                        cmd.Parameters.AddWithValue("@ctProvincia", model.ctProvincia);

                    if (model.ctCodigoISO != null)
                        cmd.Parameters.AddWithValue("@ctCodigoISO", model.ctCodigoISO);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt32(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int32 cnID_Provincia)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProvincia_DeleteBycnID_Provincia";

                    cmd.Parameters.AddWithValue("@cnID_Provincia", cnID_Provincia);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbProvinciaModel> GetBycnID_Departamento(Int32 cnID_Departamento)
        {
            List<tbProvinciaModel> result = new List<tbProvinciaModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbProvincia_GetBycnID_Departamento";

                    cmd.Parameters.AddWithValue("@cnID_Departamento", cnID_Departamento);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbProvinciaModel.FieldsOrdinal fields = new tbProvinciaModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbProvinciaModel model = new tbProvinciaModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbUsuario
    /// </summary>
    public partial class tbUsuarioDAL : dbSeleccionDALBase
    {
        internal tbUsuarioDAL()
        {
        }

        internal tbUsuarioDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbUsuarioModel> GetAll()
        {
            List<tbUsuarioModel> result = new List<tbUsuarioModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbUsuario_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbUsuarioModel.FieldsOrdinal fields = new tbUsuarioModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbUsuarioModel model = new tbUsuarioModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbUsuarioModel GetBycnID_Usuario(Int64 cnID_Usuario)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbUsuario_GetBycnID_Usuario";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Usuario", cnID_Usuario);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbUsuarioModel result = new tbUsuarioModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_Usuario(Int64 cnID_Usuario, tbUsuarioModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbUsuario_GetBycnID_Usuario";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Usuario", cnID_Usuario);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbUsuarioModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbUsuario_Update";

                    cmd.Parameters.AddWithValue("@cnID_Usuario", model.cnID_Usuario);
                    cmd.Parameters.AddWithValue("@cnID_Perfil", model.cnID_Perfil);
                    cmd.Parameters.AddWithValue("@ctNombres", model.ctNombres);
                    cmd.Parameters.AddWithValue("@ctApellidos", model.ctApellidos);
                    cmd.Parameters.AddWithValue("@ctLogin", model.ctLogin);
                    cmd.Parameters.AddWithValue("@ctClave", model.ctClave);
                    cmd.Parameters.AddWithValue("@cnEstadoReg", model.cnEstadoReg);
                    cmd.Parameters.AddWithValue("@cnBloqueado", model.cnBloqueado);

                    cmd.Parameters.AddWithValue("@cnCodigoRecuperacion", model.cnCodigoRecuperacion);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int32 Insert(tbUsuarioModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbUsuario_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Usuario", model.cnID_Usuario);
                    cmd.Parameters.AddWithValue("@cnID_Perfil", model.cnID_Perfil);
                    cmd.Parameters.AddWithValue("@ctNombres", model.ctNombres);
                    cmd.Parameters.AddWithValue("@ctApellidos", model.ctApellidos);
                    cmd.Parameters.AddWithValue("@ctLogin", model.ctLogin);
                    cmd.Parameters.AddWithValue("@ctClave", model.ctClave);
                    cmd.Parameters.AddWithValue("@cnEstadoReg", model.cnEstadoReg);
                    cmd.Parameters.AddWithValue("@cnBloqueado", model.cnBloqueado);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt32(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnID_Usuario)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbUsuario_DeleteBycnID_Usuario";

                    cmd.Parameters.AddWithValue("@cnID_Usuario", cnID_Usuario);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbUsuarioModel> GetBycnID_Perfil(Int32 cnID_Perfil)
        {
            List<tbUsuarioModel> result = new List<tbUsuarioModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbUsuario_GetBycnID_Perfil";

                    cmd.Parameters.AddWithValue("@cnID_Perfil", cnID_Perfil);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbUsuarioModel.FieldsOrdinal fields = new tbUsuarioModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbUsuarioModel model = new tbUsuarioModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public tbUsuarioModel GetByctLogin(String ctLogin)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbUsuario_GetByctLogin";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@ctLogin", ctLogin);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbUsuarioModel result = new tbUsuarioModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public tbUsuarioModel GetBytcCodigoRecuperacion(String tcCodigoRecuperacion)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbUsuario_GetBytcCodigoRecuperacion";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnCodigoRecuperacion", tcCodigoRecuperacion);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbUsuarioModel result = new tbUsuarioModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteByctLogin(String ctLogin)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbUsuario_DeleteByctLogin";

                    cmd.Parameters.AddWithValue("@ctLogin", ctLogin);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbAcademico
    /// </summary>
    public partial class tbAcademicoDAL : dbSeleccionDALBase
    {
        internal tbAcademicoDAL()
        {
        }

        internal tbAcademicoDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbAcademicoModel> GetAll()
        {
            List<tbAcademicoModel> result = new List<tbAcademicoModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbAcademico_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbAcademicoModel.FieldsOrdinal fields = new tbAcademicoModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbAcademicoModel model = new tbAcademicoModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbAcademicoModel GetBycnID_Academico(Int64 cnID_Academico)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAcademico_GetBycnID_Academico";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Academico", cnID_Academico);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbAcademicoModel result = new tbAcademicoModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_Academico(Int64 cnID_Academico, tbAcademicoModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAcademico_GetBycnID_Academico";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Academico", cnID_Academico);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbAcademicoModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAcademico_Update";

                    cmd.Parameters.AddWithValue("@cnID_Academico", model.cnID_Academico);
                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    cmd.Parameters.AddWithValue("@cnID_Grado", model.cnID_Grado);
                    cmd.Parameters.AddWithValue("@cnID_Carrera", model.cnID_Carrera);
                    if (model.ctCarreraOtro != null)
                        cmd.Parameters.AddWithValue("@ctCarreraOtro", model.ctCarreraOtro);

                    cmd.Parameters.AddWithValue("@cnID_Institucion", model.cnID_Institucion);
                    if (model.ctInstitucionOtro != null)
                        cmd.Parameters.AddWithValue("@ctInstitucionOtro", model.ctInstitucionOtro);

                    cmd.Parameters.AddWithValue("@cnID_Situacion", model.cnID_Situacion);
                    cmd.Parameters.AddWithValue("@cnID_Pais", model.cnID_Pais);
                    if (model.ctCiudad != null)
                        cmd.Parameters.AddWithValue("@ctCiudad", model.ctCiudad);

                    cmd.Parameters.AddWithValue("@cfFechaInicio", model.cfFechaInicio);
                    cmd.Parameters.AddWithValue("@cfFechaFin", model.cfFechaFin);
                    if (model.ctCertificado != null)
                        cmd.Parameters.AddWithValue("@ctCertificado", model.ctCertificado);

                    cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    if (model.cfFechaActualizacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void UpdateBloque(tbAcademicoModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDocumentAcademic_update";

                    cmd.Parameters.AddWithValue("@ctIDCertificado", model.cnID_Academico);
                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    
                    if (model.ctCertificado != null)
                        cmd.Parameters.AddWithValue("@ctCertificado", model.ctCertificado);

                    

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int Insert(tbAcademicoModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAcademico_Insert";

                    //cmd.Parameters.AddWithValue("@cnID_Academico", model.cnID_Academico);
                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    cmd.Parameters.AddWithValue("@cnID_Grado", model.cnID_Grado);
                    cmd.Parameters.AddWithValue("@cnID_Carrera", model.cnID_Carrera);
                    if (model.ctCarreraOtro != null)
                        cmd.Parameters.AddWithValue("@ctCarreraOtro", model.ctCarreraOtro);

                    cmd.Parameters.AddWithValue("@cnID_Institucion", model.cnID_Institucion);
                    if (model.ctInstitucionOtro != null)
                        cmd.Parameters.AddWithValue("@ctInstitucionOtro", model.ctInstitucionOtro);

                    cmd.Parameters.AddWithValue("@cnID_Situacion", model.cnID_Situacion);
                    cmd.Parameters.AddWithValue("@cnID_Pais", model.cnID_Pais);
                    if (model.ctCiudad != null)
                        cmd.Parameters.AddWithValue("@ctCiudad", model.ctCiudad);

                    cmd.Parameters.AddWithValue("@cfFechaInicio", model.cfFechaInicio);
                    cmd.Parameters.AddWithValue("@cfFechaFin", model.cfFechaFin);
                    if (model.ctCertificado != null)
                        cmd.Parameters.AddWithValue("@ctCertificado", model.ctCertificado);

                    cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    if (model.cfFechaActualizacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt32(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int InsertBloque(tbAcademicoModel model, int idbloque)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDocumentsAcademic_insert";

                    //cmd.Parameters.AddWithValue("@cnID_Academico", model.cnID_Academico);
                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    //cmd.Parameters.AddWithValue("@cnID_Grado", model.cnID_Grado);
                    //cmd.Parameters.AddWithValue("@cnID_Carrera", model.cnID_Carrera);
                    //if (model.ctCarreraOtro != null)
                    //    cmd.Parameters.AddWithValue("@ctCarreraOtro", model.ctCarreraOtro);

                    //cmd.Parameters.AddWithValue("@cnID_Institucion", model.cnID_Institucion);
                    //if (model.ctInstitucionOtro != null)
                    //    cmd.Parameters.AddWithValue("@ctInstitucionOtro", model.ctInstitucionOtro);

                    //cmd.Parameters.AddWithValue("@cnID_Situacion", model.cnID_Situacion);
                    //cmd.Parameters.AddWithValue("@cnID_Pais", model.cnID_Pais);
                    //if (model.ctCiudad != null)
                    //    cmd.Parameters.AddWithValue("@ctCiudad", model.ctCiudad);

                    //cmd.Parameters.AddWithValue("@cfFechaInicio", model.cfFechaInicio);
                    //cmd.Parameters.AddWithValue("@cfFechaFin", model.cfFechaFin);
                    if (model.ctCertificado != null)
                        cmd.Parameters.AddWithValue("@ctCertificado", model.ctCertificado);

                    //cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    //if (model.cfFechaActualizacion.HasValue)
                    //    cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);
                    cmd.Parameters.AddWithValue("@ctIDBloque", idbloque);
                    cmd.Parameters.AddWithValue("@ctAceptaTerminos", model.ctAceptaTerminos);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt32(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteByIdCandidato(Int64 cnID_Candidato)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAcademico_DeleteBycnID_Candidato_Fecha";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public class Documents
        {
            public int Id { get; set; }
            public string Documento { get; set; }
            public int Candidato { get; set; }
            public int Bloque { get; set; }
            public DateTime Creacion { get; set; }
            public DateTime Actualizacion { get; set; }
        }

        public Documents ListDocument(int Candidato, int Bloque)
        {
            try
            {
                Documents documents = null;
                SqlCommand cmd = GetNewCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbDocumentsAcademic_list";
                cmd.Parameters.AddWithValue("@cnID_Candidato", Candidato);
                cmd.Parameters.AddWithValue("@ctIDBloque", Bloque);

                IDataReader data = dbSeleccionDbProvider.ExecuteReader(cmd);

                while (data.Read() == true)
                {
                    documents = new Documents
                    {
                        Id = Convert.ToInt32(data["ctIDCertificado"]),
                        Documento = Convert.ToString(data["ctCertificado"]),
                        Candidato = Convert.ToInt32(data["cnID_Candidato"]),
                        Bloque = Convert.ToInt32(data["ctIDBloque"]),
                        Actualizacion = Convert.ToDateTime(data["cfFechaActualizacion"]),
                        Creacion = Convert.ToDateTime(data["cfFechacreacion"]),
                    };


                };

                return documents;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public void Delete(Int64 cnID_Academico)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAcademico_DeleteBycnID_Academico";

                    cmd.Parameters.AddWithValue("@cnID_Academico", cnID_Academico);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbAcademicoModel> GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            List<tbAcademicoModel> result = new List<tbAcademicoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAcademico_GetBycnID_Candidato";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbAcademicoModel.FieldsOrdinal fields = new tbAcademicoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbAcademicoModel model = new tbAcademicoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbAcademicoModel> GetBycnID_CandidatoMasivo(Int64 cnID_Candidato)
        {
            List<tbAcademicoModel> result = new List<tbAcademicoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDocumentsAcademic_GetBycnID_Candidato";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbAcademicoModel.FieldsOrdinal fields = new tbAcademicoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbAcademicoModel model = new tbAcademicoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbAcademicoModel> GetBycnID_Grado(Int64 cnID_Grado)
        {
            List<tbAcademicoModel> result = new List<tbAcademicoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAcademico_GetBycnID_Grado";

                    cmd.Parameters.AddWithValue("@cnID_Grado", cnID_Grado);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbAcademicoModel.FieldsOrdinal fields = new tbAcademicoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbAcademicoModel model = new tbAcademicoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbAcademicoModel> GetBycnID_Carrera(Int64 cnID_Carrera)
        {
            List<tbAcademicoModel> result = new List<tbAcademicoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAcademico_GetBycnID_Carrera";

                    cmd.Parameters.AddWithValue("@cnID_Carrera", cnID_Carrera);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbAcademicoModel.FieldsOrdinal fields = new tbAcademicoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbAcademicoModel model = new tbAcademicoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbAcademicoModel> GetBycnID_Institucion(Int64 cnID_Institucion)
        {
            List<tbAcademicoModel> result = new List<tbAcademicoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAcademico_GetBycnID_Institucion";

                    cmd.Parameters.AddWithValue("@cnID_Institucion", cnID_Institucion);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbAcademicoModel.FieldsOrdinal fields = new tbAcademicoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbAcademicoModel model = new tbAcademicoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbAcademicoModel> GetBycnID_Situacion(Int64 cnID_Situacion)
        {
            List<tbAcademicoModel> result = new List<tbAcademicoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAcademico_GetBycnID_Situacion";

                    cmd.Parameters.AddWithValue("@cnID_Situacion", cnID_Situacion);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbAcademicoModel.FieldsOrdinal fields = new tbAcademicoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbAcademicoModel model = new tbAcademicoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbAcademicoModel> GetBycnID_Pais(Int32 cnID_Pais)
        {
            List<tbAcademicoModel> result = new List<tbAcademicoModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAcademico_GetBycnID_Pais";

                    cmd.Parameters.AddWithValue("@cnID_Pais", cnID_Pais);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbAcademicoModel.FieldsOrdinal fields = new tbAcademicoModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbAcademicoModel model = new tbAcademicoModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbComplementario
    /// </summary>
    public partial class tbComplementarioDAL : dbSeleccionDALBase
    {
        internal tbComplementarioDAL()
        {
        }

        internal tbComplementarioDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbComplementarioModel> GetAll()
        {
            List<tbComplementarioModel> result = new List<tbComplementarioModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbComplementario_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbComplementarioModel.FieldsOrdinal fields = new tbComplementarioModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbComplementarioModel model = new tbComplementarioModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbComplementarioModel GetBycnID_Complementario(Int64 cnID_Complementario)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbComplementario_GetBycnID_Complementario";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Complementario", cnID_Complementario);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbComplementarioModel result = new tbComplementarioModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_Complementario(Int64 cnID_Complementario, tbComplementarioModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbComplementario_GetBycnID_Complementario";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Complementario", cnID_Complementario);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbComplementarioModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbComplementario_Update";

                    cmd.Parameters.AddWithValue("@cnID_Complementario", model.cnID_Complementario);
                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    cmd.Parameters.AddWithValue("@cnID_TipoEstudio", model.cnID_TipoEstudio);
                    //if (model.ctTipoEstudioOtro != null)
                    //    cmd.Parameters.AddWithValue("@ctTipoEstudioOtro", model.ctTipoEstudioOtro);

                    if (model.cnID_Institucion.HasValue)
                        cmd.Parameters.AddWithValue("@cnID_Institucion", model.cnID_Institucion.Value);

                    if (model.ctInstitucionOtro != null)
                        cmd.Parameters.AddWithValue("@ctInstitucionOtro", model.ctInstitucionOtro);

                    if (model.cnID_Situacion.HasValue)
                        cmd.Parameters.AddWithValue("@cnID_Situacion", model.cnID_Situacion.Value);

                    cmd.Parameters.AddWithValue("@cnID_Pais", model.cnID_Pais);
                    cmd.Parameters.AddWithValue("@cfFechaInicio", model.cfFechaInicio);
                    cmd.Parameters.AddWithValue("@cfFechaFin", model.cfFechaFin);
                    if (model.ctCertificado != null)
                        cmd.Parameters.AddWithValue("@ctCertificado", model.ctCertificado);

                    cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    if (model.cfFechaActualizacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    cmd.Parameters.AddWithValue("@ctDescripcion", model.ctDescripcion);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int64 Insert(tbComplementarioModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbComplementario_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    cmd.Parameters.AddWithValue("@cnID_TipoEstudio", model.cnID_TipoEstudio);
                    //if (model.ctTipoEstudioOtro != null)
                    //    cmd.Parameters.AddWithValue("@ctTipoEstudioOtro", model.ctTipoEstudioOtro);

                    if (model.cnID_Institucion.HasValue)
                        cmd.Parameters.AddWithValue("@cnID_Institucion", model.cnID_Institucion.Value);

                    if (model.ctInstitucionOtro != null)
                        cmd.Parameters.AddWithValue("@ctInstitucionOtro", model.ctInstitucionOtro);

                    if (model.cnID_Situacion.HasValue)
                        cmd.Parameters.AddWithValue("@cnID_Situacion", model.cnID_Situacion.Value);

                    cmd.Parameters.AddWithValue("@cnID_Pais", model.cnID_Pais);
                    cmd.Parameters.AddWithValue("@cfFechaInicio", model.cfFechaInicio);
                    cmd.Parameters.AddWithValue("@cfFechaFin", model.cfFechaFin);
                    if (model.ctCertificado != null)
                        cmd.Parameters.AddWithValue("@ctCertificado", model.ctCertificado);

                    cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    if (model.cfFechaActualizacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    cmd.Parameters.AddWithValue("@ctDescripcion", model.ctDescripcion);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt64(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnID_Complementario)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbComplementario_DeleteBycnID_Complementario";

                    cmd.Parameters.AddWithValue("@cnID_Complementario", cnID_Complementario);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbComplementarioModel> GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            List<tbComplementarioModel> result = new List<tbComplementarioModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbComplementario_GetBycnID_Candidato";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbComplementarioModel.FieldsOrdinal fields = new tbComplementarioModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbComplementarioModel model = new tbComplementarioModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbReferencia
    /// </summary>
    public partial class tbReferenciaDAL : dbSeleccionDALBase
    {
        internal tbReferenciaDAL()
        {
        }

        internal tbReferenciaDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbReferenciaModel> GetAll()
        {
            List<tbReferenciaModel> result = new List<tbReferenciaModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbReferencia_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbReferenciaModel.FieldsOrdinal fields = new tbReferenciaModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbReferenciaModel model = new tbReferenciaModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbReferenciaModel GetBycnID_Referencia(Int64 cnID_Referencia)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbReferencia_GetBycnID_Referencia";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Referencia", cnID_Referencia);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbReferenciaModel result = new tbReferenciaModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_Referencia(Int64 cnID_Referencia, tbReferenciaModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbReferencia_GetBycnID_Referencia";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Referencia", cnID_Referencia);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbReferenciaModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbReferencia_Update";

                    cmd.Parameters.AddWithValue("@cnID_Referencia", model.cnID_Referencia);
                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    cmd.Parameters.AddWithValue("@ctNombreCompleto", model.ctNombreCompleto);
                    cmd.Parameters.AddWithValue("@ctNombreEmpresa", model.ctNombreEmpresa);
                    cmd.Parameters.AddWithValue("@ctCargo", model.ctCargo);
                    cmd.Parameters.AddWithValue("@ctTelefono", model.ctTelefono);
                    cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    if (model.cfFechaActualizacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int64 Insert(tbReferenciaModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbReferencia_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    cmd.Parameters.AddWithValue("@ctNombreCompleto", model.ctNombreCompleto);
                    cmd.Parameters.AddWithValue("@ctNombreEmpresa", model.ctNombreEmpresa);
                    cmd.Parameters.AddWithValue("@ctCargo", model.ctCargo);
                    cmd.Parameters.AddWithValue("@ctTelefono", model.ctTelefono);
                    cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    if (model.cfFechaActualizacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt64(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnID_Referencia)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbReferencia_DeleteBycnID_Referencia";

                    cmd.Parameters.AddWithValue("@cnID_Referencia", cnID_Referencia);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbReferenciaModel> GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            List<tbReferenciaModel> result = new List<tbReferenciaModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbReferencia_GetBycnID_Candidato";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbReferenciaModel.FieldsOrdinal fields = new tbReferenciaModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbReferenciaModel model = new tbReferenciaModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbPostulanteCrit
    /// </summary>
    public partial class tbPostulanteCritDAL : dbSeleccionDALBase
    {
        internal tbPostulanteCritDAL()
        {
        }

        internal tbPostulanteCritDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbPostulanteCritModel> GetAll()
        {
            List<tbPostulanteCritModel> result = new List<tbPostulanteCritModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbPostulanteCrit_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbPostulanteCritModel.FieldsOrdinal fields = new tbPostulanteCritModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbPostulanteCritModel model = new tbPostulanteCritModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbPostulanteCritModel GetBycnID_PostulanteCrit(Int64 cnID_PostulanteCrit)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteCrit_GetBycnID_PostulanteCrit";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_PostulanteCrit", cnID_PostulanteCrit);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbPostulanteCritModel result = new tbPostulanteCritModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_PostulanteCrit(Int64 cnID_PostulanteCrit, tbPostulanteCritModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteCrit_GetBycnID_PostulanteCrit";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_PostulanteCrit", cnID_PostulanteCrit);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbPostulanteCritModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteCrit_Update";

                    cmd.Parameters.AddWithValue("@cnID_PostulanteCrit", model.cnID_PostulanteCrit);
                    if (model.cnID_Postulante.HasValue)
                        cmd.Parameters.AddWithValue("@cnID_Postulante", model.cnID_Postulante.Value);

                    cmd.Parameters.AddWithValue("@cnID_CriterioAdicional", model.cnID_CriterioAdicional);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int64 Insert(tbPostulanteCritModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteCrit_Insert";

                    if (model.cnID_Postulante.HasValue)
                        cmd.Parameters.AddWithValue("@cnID_Postulante", model.cnID_Postulante.Value);

                    cmd.Parameters.AddWithValue("@cnID_CriterioAdicional", model.cnID_CriterioAdicional);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt64(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnID_PostulanteCrit)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteCrit_DeleteBycnID_PostulanteCrit";

                    cmd.Parameters.AddWithValue("@cnID_PostulanteCrit", cnID_PostulanteCrit);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbCoberturaZona
    /// </summary>
    public partial class tbCoberturaZonaDAL : dbSeleccionDALBase
    {
        internal tbCoberturaZonaDAL()
        {
        }

        internal tbCoberturaZonaDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbCoberturaZonaModel> GetAll()
        {
            List<tbCoberturaZonaModel> result = new List<tbCoberturaZonaModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbCoberturaZona_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbCoberturaZonaModel.FieldsOrdinal fields = new tbCoberturaZonaModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbCoberturaZonaModel model = new tbCoberturaZonaModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public List<tbCoberturaZonaModel> GetBycnID_Cobertura(Int64 cnID_Cobertura)
        {
            List<tbCoberturaZonaModel> result = new List<tbCoberturaZonaModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbCoberturaZona_GetBycnID_Cobertura";

                IDataReader reader = null;
                try
                {
                    cmd.Parameters.AddWithValue("@cnID_Cobertura", cnID_Cobertura);

                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbCoberturaZonaModel.FieldsOrdinal fields = new tbCoberturaZonaModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbCoberturaZonaModel model = new tbCoberturaZonaModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbCoberturaZonaModel GetBycnID_CoberturaZona(Int64 cnID_CoberturaZona)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCoberturaZona_GetBycnID_CoberturaZona";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_CoberturaZona", cnID_CoberturaZona);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbCoberturaZonaModel result = new tbCoberturaZonaModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_CoberturaZona(Int64 cnID_CoberturaZona, tbCoberturaZonaModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCoberturaZona_GetBycnID_CoberturaZona";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_CoberturaZona", cnID_CoberturaZona);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbCoberturaZonaModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCoberturaZona_Update";

                    cmd.Parameters.AddWithValue("@cnID_CoberturaZona", model.cnID_CoberturaZona);
                    cmd.Parameters.AddWithValue("@cnID_Cobertura", model.cnID_Cobertura);
                    cmd.Parameters.AddWithValue("@ctZona", model.ctZona);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int64 Insert(tbCoberturaZonaModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCoberturaZona_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Cobertura", model.cnID_Cobertura);
                    cmd.Parameters.AddWithValue("@ctZona", model.ctZona);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt64(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnID_CoberturaZona)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbCoberturaZona_DeleteBycnID_CoberturaZona";

                    cmd.Parameters.AddWithValue("@cnID_CoberturaZona", cnID_CoberturaZona);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbPostulanteAcad
    /// </summary>
    public partial class tbPostulanteAcadDAL : dbSeleccionDALBase
    {
        internal tbPostulanteAcadDAL()
        {
        }

        internal tbPostulanteAcadDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbPostulanteAcadModel> GetAll()
        {
            List<tbPostulanteAcadModel> result = new List<tbPostulanteAcadModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbPostulanteAcad_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbPostulanteAcadModel.FieldsOrdinal fields = new tbPostulanteAcadModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbPostulanteAcadModel model = new tbPostulanteAcadModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbPostulanteAcadModel GetBycnID_PostulanteAcad(Int64 cnID_PostulanteAcad)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteAcad_GetBycnID_PostulanteAcad";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_PostulanteAcad", cnID_PostulanteAcad);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbPostulanteAcadModel result = new tbPostulanteAcadModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbPostulanteAcadModel> GetBycnID_Postulante(Int64 cnID_Postulante)
        {
            List<tbPostulanteAcadModel> result = new List<tbPostulanteAcadModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbPostulanteAcad_GetBycnID_Postulante";

                IDataReader reader = null;
                try
                {
                    cmd.Parameters.AddWithValue("@cnID_Postulante", cnID_Postulante);
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbPostulanteAcadModel.FieldsOrdinal fields = new tbPostulanteAcadModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbPostulanteAcadModel model = new tbPostulanteAcadModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public bool GetBycnID_PostulanteAcad(Int64 cnID_PostulanteAcad, tbPostulanteAcadModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteAcad_GetBycnID_PostulanteAcad";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_PostulanteAcad", cnID_PostulanteAcad);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbPostulanteAcadModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteAcad_Update";

                    cmd.Parameters.AddWithValue("@cnID_PostulanteAcad", model.cnID_PostulanteAcad);
                    cmd.Parameters.AddWithValue("@cnID_Postulante", model.cnID_Postulante);
                    cmd.Parameters.AddWithValue("@cnID_Academico", model.cnID_Academico);
                    cmd.Parameters.AddWithValue("@cnID_Grado", model.cnID_Grado);
                    cmd.Parameters.AddWithValue("@cnID_Situacion", model.cnID_Situacion);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int64 Insert(tbPostulanteAcadModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteAcad_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Postulante", model.cnID_Postulante);
                    cmd.Parameters.AddWithValue("@cnID_Academico", model.cnID_Academico);
                    cmd.Parameters.AddWithValue("@cnID_Grado", model.cnID_Grado);
                    cmd.Parameters.AddWithValue("@cnID_Situacion", model.cnID_Situacion);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt64(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnID_PostulanteAcad)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteAcad_DeleteBycnID_PostulanteAcad";

                    cmd.Parameters.AddWithValue("@cnID_PostulanteAcad", cnID_PostulanteAcad);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbPostulanteEvalManual
    /// </summary>
    public partial class tbPostulanteEvalManualDAL : dbSeleccionDALBase
    {
        internal tbPostulanteEvalManualDAL()
        {
        }

        internal tbPostulanteEvalManualDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbPostulanteEvalManualModel> GetAll()
        {
            List<tbPostulanteEvalManualModel> result = new List<tbPostulanteEvalManualModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbPostulanteEvalManual_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbPostulanteEvalManualModel.FieldsOrdinal fields = new tbPostulanteEvalManualModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbPostulanteEvalManualModel model = new tbPostulanteEvalManualModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public List<tbPostulanteEvalManualModel> GetBycnPostulanteEvalManual_GetBycnID_Postulante(long cnID_Postulante)
        {
            List<tbPostulanteEvalManualModel> result = new List<tbPostulanteEvalManualModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbPostulanteEvalManual_GetBycnID_Postulante";

                IDataReader reader = null;
                try
                {
                    cmd.Parameters.AddWithValue("@cnID_Postulante", cnID_Postulante);
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbPostulanteEvalManualModel.FieldsOrdinal fields = new tbPostulanteEvalManualModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbPostulanteEvalManualModel model = new tbPostulanteEvalManualModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbPostulanteEvalManualModel GetBycnPostulanteEvalManual(Int64 cnPostulanteEvalManual)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteEvalManual_GetBycnPostulanteEvalManual";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnPostulanteEvalManual", cnPostulanteEvalManual);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbPostulanteEvalManualModel result = new tbPostulanteEvalManualModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnPostulanteEvalManual(Int64 cnPostulanteEvalManual, tbPostulanteEvalManualModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteEvalManual_GetBycnPostulanteEvalManual";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnPostulanteEvalManual", cnPostulanteEvalManual);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbPostulanteEvalManualModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteEvalManual_Update";

                    cmd.Parameters.AddWithValue("@cnPostulanteEvalManual", model.cnPostulanteEvalManual);
                    cmd.Parameters.AddWithValue("@cnID_Postulante", model.cnID_Postulante);

                    if (model.cnID_TipoEval > 0)
                        cmd.Parameters.AddWithValue("@cnID_TipoEval", model.cnID_TipoEval);

                    cmd.Parameters.AddWithValue("@cnEvaluacion", model.cnEvaluacion);
                    if (model.ctObservacion != null)
                        cmd.Parameters.AddWithValue("@ctObservacion", model.ctObservacion);

                    cmd.Parameters.AddWithValue("@ctUsuarioEval", model.ctUsuarioEval);
                    if (model.cfFechaEval.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaEval", model.cfFechaEval.Value);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int64 Insert(tbPostulanteEvalManualModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteEvalManual_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Postulante", model.cnID_Postulante);

                    if (model.cnID_TipoEval > 0)
                        cmd.Parameters.AddWithValue("@cnID_TipoEval", model.cnID_TipoEval);

                    cmd.Parameters.AddWithValue("@cnEvaluacion", model.cnEvaluacion);
                    if (model.ctObservacion != null)
                        cmd.Parameters.AddWithValue("@ctObservacion", model.ctObservacion);

                    cmd.Parameters.AddWithValue("@ctUsuarioEval", model.ctUsuarioEval);
                    if (model.cfFechaEval.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaEval", model.cfFechaEval.Value);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt64(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnPostulanteEvalManual)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbPostulanteEvalManual_DeleteBycnPostulanteEvalManual";

                    cmd.Parameters.AddWithValue("@cnPostulanteEvalManual", cnPostulanteEvalManual);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbExperienciaDocencia
    /// </summary>
    public partial class tbExperienciaDocenciaDAL : dbSeleccionDALBase
    {
        internal tbExperienciaDocenciaDAL()
        {
        }

        internal tbExperienciaDocenciaDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbExperienciaDocenciaModel> GetAll()
        {
            List<tbExperienciaDocenciaModel> result = new List<tbExperienciaDocenciaModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbExperienciaDocencia_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbExperienciaDocenciaModel.FieldsOrdinal fields = new tbExperienciaDocenciaModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbExperienciaDocenciaModel model = new tbExperienciaDocenciaModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbExperienciaDocenciaModel GetBycnID_ExperienciaDocencia(Int64 cnID_ExperienciaDocencia)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperienciaDocencia_GetBycnID_ExperienciaDocencia";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_ExperienciaDocencia", cnID_ExperienciaDocencia);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbExperienciaDocenciaModel result = new tbExperienciaDocenciaModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_ExperienciaDocencia(Int64 cnID_ExperienciaDocencia, tbExperienciaDocenciaModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperienciaDocencia_GetBycnID_ExperienciaDocencia";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_ExperienciaDocencia", cnID_ExperienciaDocencia);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbExperienciaDocenciaModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperienciaDocencia_Update";

                    cmd.Parameters.AddWithValue("@cnID_ExperienciaDocencia", model.cnID_ExperienciaDocencia);
                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    cmd.Parameters.AddWithValue("@cnID_TipoInstitucion", model.cnID_TipoInstitucion);
                    cmd.Parameters.AddWithValue("@ctInstitucion", model.ctInstitucion);
                    cmd.Parameters.AddWithValue("@cnDocenteInvestigador", model.cnDocenteInvestigador);
                    cmd.Parameters.AddWithValue("@cnID_CategoriaDocente", model.cnID_CategoriaDocente);
                    cmd.Parameters.AddWithValue("@cnID_RegimenDedicacion", model.cnID_RegimenDedicacion);
                    cmd.Parameters.AddWithValue("@cnRegistradoDINA", model.cnRegistradoDINA);
                    if (model.ctNumeroRegistroDINA != null)
                        cmd.Parameters.AddWithValue("@ctNumeroRegistroDINA", model.ctNumeroRegistroDINA);

                    cmd.Parameters.AddWithValue("@cfFechaInicio", model.cfFechaInicio);
                    cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    if (model.cfFechaActualizacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int64 Insert(tbExperienciaDocenciaModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperienciaDocencia_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    cmd.Parameters.AddWithValue("@cnID_TipoInstitucion", model.cnID_TipoInstitucion);
                    cmd.Parameters.AddWithValue("@ctInstitucion", model.ctInstitucion);
                    cmd.Parameters.AddWithValue("@cnDocenteInvestigador", model.cnDocenteInvestigador);
                    cmd.Parameters.AddWithValue("@cnID_CategoriaDocente", model.cnID_CategoriaDocente);
                    cmd.Parameters.AddWithValue("@cnID_RegimenDedicacion", model.cnID_RegimenDedicacion);
                    cmd.Parameters.AddWithValue("@cnRegistradoDINA", model.cnRegistradoDINA);
                    if (model.ctNumeroRegistroDINA != null)
                        cmd.Parameters.AddWithValue("@ctNumeroRegistroDINA", model.ctNumeroRegistroDINA);

                    cmd.Parameters.AddWithValue("@cfFechaInicio", model.cfFechaInicio);
                    cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    if (model.cfFechaActualizacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt64(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int64 InsertBloque(tbExperienciaDocenciaModel model, string filename, int idbloque)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbDocumentsAcademic_insert";


                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);

                    if (filename != null)
                        cmd.Parameters.AddWithValue("@ctCertificado", filename);



                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);
                    cmd.Parameters.AddWithValue("@ctIDBloque", idbloque);

                    cmd.Parameters.AddWithValue("@ctAceptaTerminos", model.ctAceptaTerminos);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt32(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnID_ExperienciaDocencia)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperienciaDocencia_DeleteBycnID_ExperienciaDocencia";

                    cmd.Parameters.AddWithValue("@cnID_ExperienciaDocencia", cnID_ExperienciaDocencia);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteByIdCandidato(Int64 cnID_Candidato)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperienciaDocencia_DeleteBycnID_Candidato_Fecha";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbExperienciaDocenciaModel> GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            List<tbExperienciaDocenciaModel> result = new List<tbExperienciaDocenciaModel>();

            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperienciaDocencia_GetBycnID_Candidato";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                    IDataReader reader = null;
                    try
                    {
                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        tbExperienciaDocenciaModel.FieldsOrdinal fields = new tbExperienciaDocenciaModel.FieldsOrdinal(reader);
                        while (reader.Read())
                        {
                            tbExperienciaDocenciaModel model = new tbExperienciaDocenciaModel();
                            model.ReadData(reader, fields);
                            result.Add(model);
                        }
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbAreaTematica
    /// </summary>
    public partial class tbAreaTematicaDAL : dbSeleccionDALBase
    {
        internal tbAreaTematicaDAL()
        {
        }

        internal tbAreaTematicaDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbAreaTematicaModel> GetAll()
        {
            List<tbAreaTematicaModel> result = new List<tbAreaTematicaModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbAreaTematica_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbAreaTematicaModel.FieldsOrdinal fields = new tbAreaTematicaModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbAreaTematicaModel model = new tbAreaTematicaModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbAreaTematicaModel GetBycnID_AreaTematica(Int64 cnID_AreaTematica)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAreaTematica_GetBycnID_AreaTematica";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_AreaTematica", cnID_AreaTematica);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbAreaTematicaModel result = new tbAreaTematicaModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public tbAreaTematicaModel GetBycnID_Candidato(Int64 cnID_Candidato)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAreaTematica_GetBycnID_Candidato";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbAreaTematicaModel result = new tbAreaTematicaModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_AreaTematica(Int64 cnID_AreaTematica, tbAreaTematicaModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAreaTematica_GetBycnID_AreaTematica";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_AreaTematica", cnID_AreaTematica);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbAreaTematicaModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAreaTematica_Update";

                    cmd.Parameters.AddWithValue("@cnID_AreaTematica", model.cnID_AreaTematica);
                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    if (model.ctDominioEspecifico != null)
                        cmd.Parameters.AddWithValue("@ctDominioEspecifico", model.ctDominioEspecifico);

                    //cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    //if (model.cfFechaActualizacion.HasValue)
                    //    cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int64 Insert(tbAreaTematicaModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAreaTematica_Insert";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", model.cnID_Candidato);
                    if (model.ctDominioEspecifico != null)
                        cmd.Parameters.AddWithValue("@ctDominioEspecifico", model.ctDominioEspecifico);

                    //cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    if (model.cfFechaActualizacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt64(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnID_AreaTematica)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAreaTematica_DeleteBycnID_AreaTematica";

                    cmd.Parameters.AddWithValue("@cnID_AreaTematica", cnID_AreaTematica);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbAreaTematicaDet
    /// </summary>
    public partial class tbAreaTematicaDetDAL : dbSeleccionDALBase
    {
        internal tbAreaTematicaDetDAL()
        {
        }

        internal tbAreaTematicaDetDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbAreaTematicaDetModel> GetAll()
        {
            List<tbAreaTematicaDetModel> result = new List<tbAreaTematicaDetModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbAreaTematicaDet_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbAreaTematicaDetModel.FieldsOrdinal fields = new tbAreaTematicaDetModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbAreaTematicaDetModel model = new tbAreaTematicaDetModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbAreaTematicaDetModel GetBycnID_AreaTematicaDet(Int64 cnID_AreaTematicaDet)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAreaTematicaDet_GetBycnID_AreaTematicaDet";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_AreaTematicaDet", cnID_AreaTematicaDet);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbAreaTematicaDetModel result = new tbAreaTematicaDetModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetBycnID_AreaTematicaDet(Int64 cnID_AreaTematicaDet, tbAreaTematicaDetModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAreaTematicaDet_GetBycnID_AreaTematicaDet";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_AreaTematicaDet", cnID_AreaTematicaDet);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbAreaTematicaDetModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAreaTematicaDet_Update";

                    cmd.Parameters.AddWithValue("@cnID_AreaTematicaDet", model.cnID_AreaTematicaDet);
                    cmd.Parameters.AddWithValue("@cnID_AreaTematica", model.cnID_AreaTematica);
                    if (model.cnID_DominioCurso != null)
                        cmd.Parameters.AddWithValue("@cnID_DominioCurso", model.cnID_DominioCurso);

                    cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    if (model.cfFechaActualizacion.HasValue)
                        cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int64 Insert(tbAreaTematicaDetModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAreaTematicaDet_Insert";

                    cmd.Parameters.AddWithValue("@cnID_AreaTematica", model.cnID_AreaTematica);

                    cmd.Parameters.AddWithValue("@cnID_DominioCurso", model.cnID_DominioCurso);

                    //cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    //if (model.cfFechaActualizacion.HasValue)
                    //    cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion.Value);

                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt64(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnID_AreaTematicaDet)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAreaTematicaDet_DeleteBycnID_AreaTematicaDet";

                    cmd.Parameters.AddWithValue("@cnID_AreaTematicaDet", cnID_AreaTematicaDet);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DeleteByNivel(Int64 cnID_Candidato, Int64 cnID_AreaTematicaNivel)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbAreaTematicaDet_DeleteByNivel";

                    cmd.Parameters.AddWithValue("@cnID_Candidato", cnID_Candidato);

                    cmd.Parameters.AddWithValue("@cnID_AreaTematicaNivel", cnID_AreaTematicaNivel);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /// <summary>
    /// Data access layer for tbExperienciaDocenciaDet
    /// </summary>
    public partial class tbExperienciaDocenciaDetDAL : dbSeleccionDALBase
    {
        internal tbExperienciaDocenciaDetDAL()
        {

        }
        internal tbExperienciaDocenciaDetDAL(dbSeleccionDbTransaction transaction, IDbConnection connection)
        {
            Connection = connection;
            Transaction = transaction;
        }

        public List<tbExperienciaDocenciaDetModel> GetAll()
        {
            List<tbExperienciaDocenciaDetModel> result = new List<tbExperienciaDocenciaDetModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbExperienciaDocenciaDet_GetAll";

                IDataReader reader = null;
                try
                {
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbExperienciaDocenciaDetModel.FieldsOrdinal fields = new tbExperienciaDocenciaDetModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbExperienciaDocenciaDetModel model = new tbExperienciaDocenciaDetModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;
        }

        public tbExperienciaDocenciaDetModel GetBycnID_ExperienciaDocenciaDet(Int64 cnID_ExperienciaDocenciaDet)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperienciaDocenciaDet_GetBycnID_ExperienciaDocenciaDet";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_ExperienciaDocenciaDet", cnID_ExperienciaDocenciaDet);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            tbExperienciaDocenciaDetModel result = new tbExperienciaDocenciaDetModel();
                            result.ReadData(reader);
                            return result;
                        }
                        else
                            return null;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<tbExperienciaDocenciaDetModel> GetBycnID_ExperienciaDocencia(Int64 cnID_ExperienciaDocencia)
        {

            List<tbExperienciaDocenciaDetModel> result = new List<tbExperienciaDocenciaDetModel>();

            using (SqlCommand cmd = GetNewCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "tbExperienciaDocenciaDet_GetBycnID_ExperienciaDocencia";

                IDataReader reader = null;
                try
                {
                    cmd.Parameters.AddWithValue("@cnID_ExperienciaDocencia", cnID_ExperienciaDocencia);
                    reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                    tbExperienciaDocenciaDetModel.FieldsOrdinal fields = new tbExperienciaDocenciaDetModel.FieldsOrdinal(reader);
                    while (reader.Read())
                    {
                        tbExperienciaDocenciaDetModel model = new tbExperienciaDocenciaDetModel();
                        model.ReadData(reader, fields);
                        result.Add(model);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (reader != null)
                        reader.Close();
                }
            }
            return result;

        }

        public bool GetBycnID_ExperienciaDocenciaDet(Int64 cnID_ExperienciaDocenciaDet, tbExperienciaDocenciaDetModel result)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperienciaDocenciaDet_GetBycnID_ExperienciaDocenciaDet";

                    IDataReader reader = null;
                    try
                    {
                        cmd.Parameters.AddWithValue("@cnID_ExperienciaDocenciaDet", cnID_ExperienciaDocenciaDet);

                        reader = dbSeleccionDbProvider.ExecuteReader(cmd);
                        if (reader.Read())
                        {
                            result.ReadData(reader);
                            return true;
                        }
                        else
                            return false;
                    }
                    finally
                    {
                        if (reader != null)
                            reader.Close();
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Update(tbExperienciaDocenciaDetModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperienciaDocenciaDet_Update";


                    cmd.Parameters.AddWithValue("@cnID_ExperienciaDocenciaDet", model.cnID_ExperienciaDocenciaDet);
                    cmd.Parameters.AddWithValue("@cnID_ExperienciaDocencia", model.cnID_ExperienciaDocencia);
                    cmd.Parameters.AddWithValue("@cnID_Grado", model.cnID_Grado);
                    cmd.Parameters.AddWithValue("@cnID_TipoExperiencia", model.cnID_TipoExperiencia);
                    cmd.Parameters.AddWithValue("@ctDescripcion", model.ctDescripcion);
                    cmd.Parameters.AddWithValue("@cnTiempoExperiencia", model.cnTiempoExperiencia);
                    cmd.Parameters.AddWithValue("@cnID_UnidadMedida", model.cnID_UnidadMedida);
                    cmd.Parameters.AddWithValue("@ctCerfificado", model.ctCerfificado);
                    //cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    //cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion);
                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Int64 Insert(tbExperienciaDocenciaDetModel model)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperienciaDocenciaDet_Insert";


                    cmd.Parameters.AddWithValue("@cnID_ExperienciaDocencia", model.cnID_ExperienciaDocencia);
                    cmd.Parameters.AddWithValue("@cnID_Grado", model.cnID_Grado);
                    cmd.Parameters.AddWithValue("@cnID_TipoExperiencia", model.cnID_TipoExperiencia);
                    cmd.Parameters.AddWithValue("@ctDescripcion", model.ctDescripcion);
                    cmd.Parameters.AddWithValue("@cnTiempoExperiencia", model.cnTiempoExperiencia);
                    cmd.Parameters.AddWithValue("@cnID_UnidadMedida", model.cnID_UnidadMedida);
                    cmd.Parameters.AddWithValue("@ctCerfificado", "0");
                    //cmd.Parameters.AddWithValue("@cfFechaCreacion", model.cfFechaCreacion);
                    //cmd.Parameters.AddWithValue("@cfFechaActualizacion", model.cfFechaActualizacion);
                    cmd.Parameters.AddWithValue("@ctIPAcceso", model.ctIPAcceso);


                    Object result = null;
                    result = dbSeleccionDbProvider.ExecuteScalar(cmd);
                    if (result == null || Convert.IsDBNull(result))
                        throw new Exception("Can not get inserted auto number from db.");
                    else
                        return Convert.ToInt64(result);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void Delete(Int64 cnID_ExperienciaDocenciaDet)
        {
            try
            {
                using (SqlCommand cmd = GetNewCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "tbExperienciaDocenciaDet_DeleteBycnID_ExperienciaDocenciaDet";

                    cmd.Parameters.AddWithValue("@cnID_ExperienciaDocenciaDet", cnID_ExperienciaDocenciaDet);

                    dbSeleccionDbProvider.ExecuteNonQuery(cmd);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }



    }
}