USE [master]
GO
/****** Object:  Database [dbSeleccion]    Script Date: 29/06/2015 23:40:29 ******/
CREATE DATABASE [dbSeleccion]
-- CONTAINMENT = NONE
-- ON  PRIMARY 
--( NAME = N'dbSeleccion', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\dbSeleccion.mdf' , SIZE = 4160KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
-- LOG ON 
--( NAME = N'dbSeleccion_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\dbSeleccion_log.ldf' , SIZE = 1040KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
--GO
--ALTER DATABASE [dbSeleccion] SET COMPATIBILITY_LEVEL = 110
GO
--IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
--begin
--EXEC [dbSeleccion].[dbo].[sp_fulltext_database] @action = 'enable'
--end
--GO
--ALTER DATABASE [dbSeleccion] SET ANSI_NULL_DEFAULT OFF 
--GO
--ALTER DATABASE [dbSeleccion] SET ANSI_NULLS OFF 
--GO
--ALTER DATABASE [dbSeleccion] SET ANSI_PADDING OFF 
--GO
--ALTER DATABASE [dbSeleccion] SET ANSI_WARNINGS OFF 
--GO
--ALTER DATABASE [dbSeleccion] SET ARITHABORT OFF 
--GO
--ALTER DATABASE [dbSeleccion] SET AUTO_CLOSE ON 
--GO
--ALTER DATABASE [dbSeleccion] SET AUTO_CREATE_STATISTICS ON 
--GO
--ALTER DATABASE [dbSeleccion] SET AUTO_SHRINK OFF 
--GO
--ALTER DATABASE [dbSeleccion] SET AUTO_UPDATE_STATISTICS ON 
--GO
--ALTER DATABASE [dbSeleccion] SET CURSOR_CLOSE_ON_COMMIT OFF 
--GO
--ALTER DATABASE [dbSeleccion] SET CURSOR_DEFAULT  GLOBAL 
--GO
--ALTER DATABASE [dbSeleccion] SET CONCAT_NULL_YIELDS_NULL OFF 
--GO
--ALTER DATABASE [dbSeleccion] SET NUMERIC_ROUNDABORT OFF 
--GO
--ALTER DATABASE [dbSeleccion] SET QUOTED_IDENTIFIER OFF 
--GO
--ALTER DATABASE [dbSeleccion] SET RECURSIVE_TRIGGERS OFF 
--GO
--ALTER DATABASE [dbSeleccion] SET  ENABLE_BROKER 
--GO
--ALTER DATABASE [dbSeleccion] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
--GO
--ALTER DATABASE [dbSeleccion] SET DATE_CORRELATION_OPTIMIZATION OFF 
--GO
--ALTER DATABASE [dbSeleccion] SET TRUSTWORTHY OFF 
--GO
--ALTER DATABASE [dbSeleccion] SET ALLOW_SNAPSHOT_ISOLATION OFF 
--GO
--ALTER DATABASE [dbSeleccion] SET PARAMETERIZATION SIMPLE 
--GO
--ALTER DATABASE [dbSeleccion] SET READ_COMMITTED_SNAPSHOT OFF 
--GO
--ALTER DATABASE [dbSeleccion] SET HONOR_BROKER_PRIORITY OFF 
--GO
--ALTER DATABASE [dbSeleccion] SET RECOVERY SIMPLE 
--GO
--ALTER DATABASE [dbSeleccion] SET  MULTI_USER 
--GO
--ALTER DATABASE [dbSeleccion] SET PAGE_VERIFY CHECKSUM  
--GO
--ALTER DATABASE [dbSeleccion] SET DB_CHAINING OFF 
--GO
--ALTER DATABASE [dbSeleccion] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
--GO
--ALTER DATABASE [dbSeleccion] SET TARGET_RECOVERY_TIME = 0 SECONDS 
--GO
USE [dbSeleccion]
GO
/****** Object:  StoredProcedure [dbo].[tbAcademico_DeleteBycnID_Academico]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbAcademico_DeleteBycnID_Academico
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbAcademico table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbAcademico_DeleteBycnID_Academico]
	(
	@cnID_Academico bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbAcademico WHERE [cnID_Academico]=@cnID_Academico




GO
/****** Object:  StoredProcedure [dbo].[tbAcademico_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbAcademico_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbAcademico table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbAcademico_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbAcademico




GO
/****** Object:  StoredProcedure [dbo].[tbAcademico_GetBycnID_Academico]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbAcademico_GetBycnID_Academico
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbAcademico table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbAcademico_GetBycnID_Academico]
	(
	@cnID_Academico bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbAcademico WHERE [cnID_Academico]=@cnID_Academico




GO
/****** Object:  StoredProcedure [dbo].[tbAcademico_GetBycnID_Candidato]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[tbAcademico_GetBycnID_Candidato]
@cnID_Candidato int
as
begin

SELECT 
	tbAcademico.* ,
	tbSituacion.ctElemento as ctSituacion,
	tbGrado.ctElemento as ctGrado,
	CASE WHEN tbOtros.cnID_Lista = tbAcademico.cnID_Carrera THEN tbAcademico.ctCarreraOtro ELSE tbCarrera.ctElemento END AS ctCarrera
FROM 
tbAcademico 
INNER JOIN tbMaestroListas tbSituacion 
ON tbSituacion.cnID_Lista = tbAcademico.cnID_Situacion AND tbSituacion.cnID_GrupoLista IN (12,26,27,28)
INNER JOIN tbMaestroListas tbGrado 
ON tbGrado.cnID_Lista = tbAcademico.cnID_Grado AND tbGrado.cnID_GrupoLista = 5
LEFT JOIN tbMaestroListas tbCarrera ON tbCarrera.cnID_Lista = tbAcademico.cnID_Carrera AND tbCarrera.cnID_GrupoLista = 3 
LEFT JOIN tbMaestroListas tbOtros ON tbOtros.cnID_Lista = 1634
WHERE 
cnID_Candidato = @cnID_Candidato
end


GO
/****** Object:  StoredProcedure [dbo].[tbAcademico_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbAcademico_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for inserting values to tbAcademico table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbAcademico_Insert]
(
	
	--@cnID_Academico bigint,
	@cnID_Candidato bigint,
	@cnID_Grado bigint,
	@cnID_Institucion bigint,
	@ctInstitucionOtro varchar(200) = NULL,
	@cnID_Carrera bigint,
	@ctCarreraOtro varchar(200) = NULL,
	@cnID_Situacion bigint,
	@cnID_Pais int,
	@ctCiudad varchar(150) = NULL,
	@cfFechaInicio date,
	@cfFechaFin date,
	@ctCertificado varchar(150) = NULL,
	@cfFechaCreacion datetime,
	@cfFechaActualizacion datetime = NULL,
	@ctIPAcceso varchar(15)
)
AS
	SET NOCOUNT ON

	INSERT INTO tbAcademico
			([cnID_Candidato] ,[cnID_Grado] ,[cnID_Institucion] ,[ctInstitucionOtro] ,[cnID_Carrera] ,[ctCarreraOtro] ,[cnID_Situacion] ,[cnID_Pais] ,[ctCiudad] ,[cfFechaInicio] ,[cfFechaFin] ,[ctCertificado] ,[cfFechaCreacion] ,[cfFechaActualizacion] ,[ctIPAcceso] )
			--([cnID_Academico] ,[cnID_Candidato] ,[cnID_Grado] ,[cnID_Institucion] ,[ctInstitucionOtro] ,[cnID_Carrera] ,[ctCarreraOtro] ,[cnID_Situacion] ,[cnID_Pais] ,[ctCiudad] ,[cfFechaInicio] ,[cfFechaFin] ,[ctCertificado] ,[cfFechaCreacion] ,[cfFechaActualizacion] ,[ctIPAcceso] )
	VALUES	(@cnID_Candidato ,@cnID_Grado ,@cnID_Institucion ,@ctInstitucionOtro ,@cnID_Carrera ,@ctCarreraOtro ,@cnID_Situacion ,@cnID_Pais ,@ctCiudad ,@cfFechaInicio ,@cfFechaFin ,@ctCertificado ,@cfFechaCreacion ,@cfFechaActualizacion ,@ctIPAcceso )
	--VALUES	(@cnID_Academico ,@cnID_Candidato ,@cnID_Grado ,@cnID_Institucion ,@ctInstitucionOtro ,@cnID_Carrera ,@ctCarreraOtro ,@cnID_Situacion ,@cnID_Pais ,@ctCiudad ,@cfFechaInicio ,@cfFechaFin ,@ctCertificado ,@cfFechaCreacion ,@cfFechaActualizacion ,@ctIPAcceso )
    
	SELECT  Scope_Identity() AS [cnID_Candidato];




GO
/****** Object:  StoredProcedure [dbo].[tbAcademico_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbAcademico_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for updating 	tbAcademico table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbAcademico_Update]
(
	
	@cnID_Academico bigint,
	@cnID_Candidato bigint,
	@cnID_Grado bigint,
	@cnID_Institucion bigint,
	@ctInstitucionOtro varchar(200) = NULL,
	@cnID_Carrera bigint,
	@ctCarreraOtro varchar(200) = NULL,
	@cnID_Situacion bigint,
	@cnID_Pais int,
	@ctCiudad varchar(150) = NULL,
	@cfFechaInicio date,
	@cfFechaFin date,
	@ctCertificado varchar(150) = NULL,
	@cfFechaCreacion datetime =null,
	@cfFechaActualizacion datetime = NULL,
	@ctIPAcceso varchar(15)
)
AS
	SET NOCOUNT ON

	UPDATE tbAcademico SET 

		--[cnID_Academico] = @cnID_Academico ,
		[cnID_Candidato] = @cnID_Candidato ,
		[cnID_Grado] = @cnID_Grado ,
		[cnID_Institucion] = @cnID_Institucion ,
		[ctInstitucionOtro] = @ctInstitucionOtro ,
		[cnID_Carrera] = @cnID_Carrera ,
		[ctCarreraOtro] = @ctCarreraOtro ,
		[cnID_Situacion] = @cnID_Situacion ,
		[cnID_Pais] = @cnID_Pais ,
		[ctCiudad] = @ctCiudad ,
		[cfFechaInicio] = @cfFechaInicio ,
		[cfFechaFin] = @cfFechaFin ,
		[ctCertificado] = @ctCertificado ,
		--[cfFechaCreacion] = @cfFechaCreacion ,
		[cfFechaActualizacion] = getdate() ,
		[ctIPAcceso] = @ctIPAcceso 
	WHERE [cnID_Academico]=@cnID_Academico




GO
/****** Object:  StoredProcedure [dbo].[tbCandidato_DeleteBycnID_Candidato]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbCandidato_DeleteBycnID_Candidato
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbCandidato table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCandidato_DeleteBycnID_Candidato]
	(
	@cnID_Candidato bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbCandidato WHERE [cnID_Candidato]=@cnID_Candidato




GO
/****** Object:  StoredProcedure [dbo].[tbCandidato_DeleteByctEmail]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbCandidato_DeleteByctEmail
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbCandidato table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCandidato_DeleteByctEmail]
	(
	@ctEmail char(100)
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbCandidato WHERE [ctEmail]=@ctEmail




GO
/****** Object:  StoredProcedure [dbo].[tbCandidato_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbCandidato_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbCandidato table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCandidato_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbCandidato




GO
/****** Object:  StoredProcedure [dbo].[tbCandidato_GetBycnID_Candidato]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbCandidato_GetBycnID_Candidato
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbCandidato table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCandidato_GetBycnID_Candidato]
	(
	@cnID_Candidato bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbCandidato WHERE [cnID_Candidato]=@cnID_Candidato




GO
/****** Object:  StoredProcedure [dbo].[tbCandidato_GetByctApellidoMaterno]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbCandidato_GetByctApellidoMaterno
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbCandidato table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCandidato_GetByctApellidoMaterno]
	(
	@ctApellidoMaterno varchar(200)
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbCandidato WHERE [ctApellidoMaterno]=@ctApellidoMaterno




GO
/****** Object:  StoredProcedure [dbo].[tbCandidato_GetByctApellidoPaterno]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbCandidato_GetByctApellidoPaterno
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbCandidato table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCandidato_GetByctApellidoPaterno]
	(
	@ctApellidoPaterno varchar(200)
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbCandidato WHERE [ctApellidoPaterno]=@ctApellidoPaterno




GO
/****** Object:  StoredProcedure [dbo].[tbCandidato_GetByctEmail]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbCandidato_GetByctEmail
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbCandidato table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCandidato_GetByctEmail]
	(
	@ctEmail char(100)
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbCandidato WHERE [ctEmail]=@ctEmail




GO
/****** Object:  StoredProcedure [dbo].[tbCandidato_GetByctNombres]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbCandidato_GetByctNombres
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbCandidato table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCandidato_GetByctNombres]
	(
	@ctNombres varchar(200)
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbCandidato WHERE [ctNombres]=@ctNombres




GO
/****** Object:  StoredProcedure [dbo].[tbCandidato_GetBytcCodigoRecuperacion]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbUsuario_GetBycnID_Usuario
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	22/04/2015 19:45:05
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbUsuario table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCandidato_GetBytcCodigoRecuperacion]
	(
	@cnCodigoRecuperacion uniqueidentifier
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbCandidato WHERE cnCodigoRecuperacion = @cnCodigoRecuperacion


	



GO
/****** Object:  StoredProcedure [dbo].[tbCandidato_GetHojaVidaAcademico]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[tbCandidato_GetHojaVidaAcademico]
	@cnID_Candidato INT
AS
BEGIN
	
	SELECT 
		tbGrado.ctElemento AS ctGrado,		
		CASE WHEN tbOtros.cnID_Lista = tbAcademico.cnID_Carrera THEN tbAcademico.ctCarreraOtro ELSE tbCarrera.ctElemento END AS ctCarrera,
		tbAcademico.ctCarreraOtro,
		CASE WHEN tbOtros.cnID_Lista = tbAcademico.cnID_Institucion THEN tbAcademico.ctInstitucionOtro ELSE  ISNULL(tbUniversidad.ctElemento,tbInstituto.ctElemento) END AS ctInstitucion,
		tbAcademico.ctInstitucionOtro,
		tbSituacion.ctElemento AS ctSituacion,
		tbPais.ctPais AS ctPais,
		tbAcademico.ctCiudad,
		tbAcademico.cfFechaInicio,
		tbAcademico.cfFechaFin
	FROM 
		tbAcademico  LEFT JOIN
		tbMaestroListas tbGrado ON tbGrado.cnID_Lista = tbAcademico.cnID_Grado AND tbGrado.cnID_GrupoLista = 5 LEFT JOIN
		tbMaestroListas tbCarrera ON tbCarrera.cnID_Lista = tbAcademico.cnID_Carrera AND tbCarrera.cnID_GrupoLista = 3 LEFT JOIN
		tbMaestroListas tbUniversidad ON tbUniversidad.cnID_Lista = tbAcademico.cnID_Institucion AND tbUniversidad.cnID_GrupoLista = 18 LEFT JOIN
		tbMaestroListas tbInstituto ON tbInstituto.cnID_Lista = tbAcademico.cnID_Institucion AND tbInstituto.cnID_GrupoLista = 7 LEFT JOIN
		tbMaestroListas tbSituacion ON tbSituacion.cnID_Lista = tbAcademico.cnID_Situacion AND tbSituacion.cnID_GrupoLista  IN (12,26,27,28) LEFT JOIN
		tbMaestroListas tbOtros ON tbOtros.cnID_Lista = 1634 LEFT JOIN
		tbPais on tbPais.cnID_Pais = tbAcademico.cnID_Pais 
	WHERE
		tbAcademico.cnID_Candidato =  @cnID_Candidato

	
END






GO
/****** Object:  StoredProcedure [dbo].[tbCandidato_GetHojaVidaComplementario]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[tbCandidato_GetHojaVidaComplementario]
	@cnID_Candidato INT
AS
BEGIN
	
	SELECT 
		tbTipoEstudio.ctElemento ctTipoEstudio,
		--tbComplementario.ctTipoEstudioOtro,
		ISNULL(tbUniversidad.ctElemento,tbInstituto.ctElemento) AS ctInstitucion,
		tbComplementario.ctInstitucionOtro,
		tbSituacion.ctElemento AS ctSituacion,
		tbPais.ctPais AS ctPais,
		tbComplementario.cfFechaInicio,
		tbComplementario.cfFechaFin
	FROM 
		tbComplementario LEFT JOIN
		tbMaestroListas tbTipoEstudio ON tbTipoEstudio.cnID_Lista = tbComplementario.cnID_TipoEstudio AND tbTipoEstudio.cnID_GrupoLista = 22 LEFT JOIN
		tbMaestroListas tbUniversidad ON tbUniversidad.cnID_Lista = tbComplementario.cnID_Institucion AND tbUniversidad.cnID_GrupoLista = 18 LEFT JOIN
		tbMaestroListas tbInstituto ON tbInstituto.cnID_Lista = tbComplementario.cnID_Institucion AND tbInstituto.cnID_GrupoLista = 7 LEFT JOIN
		tbMaestroListas tbSituacion ON tbSituacion.cnID_Lista = tbComplementario.cnID_Situacion AND tbSituacion.cnID_GrupoLista = 12 LEFT JOIN
		tbPais on tbPais.cnID_Pais = tbComplementario.cnID_Pais 
	WHERE
		tbComplementario.cnID_Candidato =  @cnID_Candidato

	
END





GO
/****** Object:  StoredProcedure [dbo].[tbCandidato_GetHojaVidaDatosPersonales]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--tbCandidato_GetHojaVidaDatosPersonales 14
CREATE PROCEDURE [dbo].[tbCandidato_GetHojaVidaDatosPersonales]
	@cnID_Candidato INT
AS
BEGIN
	SELECT 
		tbCandidato.cnID_Candidato,
		tbCandidato.ctApellidoPaterno,
		tbCandidato.ctApellidoMaterno,
		tbCandidato.ctNombres,
		tbSexo.ctElemento AS ctSexo,
		tbestadoCivil.ctElemento AS ctEstadoCivil,
		tbCandidato.ctLugarNacimiento,
		tbCandidato.cfFechaNacimiento,
		tbPaisNacimiento.ctPais ctPaisNacimiento,
		tbCandidato.ctDireccion,
		tbPaisDomicilio.ctPais ctPaisDomicilio,
		tbDepartamento.ctDepartamento,
		tbProvincia.ctProvincia,
		tbDistrito.ctDistrito,
		tbCandidato.ctTelefonoCasa,
		tbCandidato.ctTelefonoMovil,
		tbCandidato.ctTelefonoOtro,
		tbCandidato.ctEmail,
		ISNULL(tbDocIdentidadDNI.ctNumero,'') AS ctDNI,
		ISNULL(tbDocIdentidadRUC.ctNumero,'') as ctRUC
	FROM 
		tbCandidato LEFT JOIN
		tbMaestroListas tbSexo ON tbSexo.cnID_Lista = tbCandidato.cnID_Sexo AND tbSexo.cnID_GrupoLista = 11 LEFT JOIN
		tbMaestroListas tbestadoCivil ON tbestadoCivil.cnID_Lista = tbCandidato.ctEstadoCivil AND tbestadoCivil.cnID_GrupoLista = 20 LEFT JOIN
		tbPais tbPaisNacimiento on tbPaisNacimiento.cnID_Pais = tbCandidato.cnID_PaisNacimiento LEFT JOIN
		tbPais tbPaisDomicilio on tbPaisDomicilio.cnID_Pais = tbCandidato.cnID_Pais LEFT JOIN
		tbDepartamento on tbDepartamento.cnID_Departamento = tbCandidato.cnID_Departamento LEFT JOIN
		tbProvincia on tbProvincia.cnID_Provincia = tbCandidato.cnID_Provincia LEFT JOIN
		tbDistrito on tbDistrito.cnID_Distrito = tbCandidato.cnID_Distrito LEFT JOIN
		tbDocIdentidad tbDocIdentidadDNI ON tbDocIdentidadDNI.cnID_Candidato = tbCandidato.cnID_Candidato AND tbDocIdentidadDNI.cnID_TipoDocumento = 237 LEFT JOIN
		tbDocIdentidad tbDocIdentidadRUC ON tbDocIdentidadRUC.cnID_Candidato = tbCandidato.cnID_Candidato AND tbDocIdentidadRUC.cnID_TipoDocumento = 238
	WHERE
		tbCandidato.cnID_Candidato =  @cnID_Candidato

	
END







GO
/****** Object:  StoredProcedure [dbo].[tbCandidato_GetHojaVidaExperiencia]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[tbCandidato_GetHojaVidaExperiencia]
	@cnID_Candidato INT
AS
BEGIN
	

	SELECT
		--tbExperiencia.cnID_TipoEntidad,
		tbExperiencia.ctNombreEmpresa,
		--tbExperiencia.cnID_Rubro,
		tbCargo.ctElemento ctCargo,
		--tbExperiencia.cnID_Area,
		--tbExperiencia.cnID_TipoContrato,
		--tbExperiencia.cnID_Pais,
		tbExperiencia.cnSubordinados,
		tbExperiencia.ctDescripcionCargo,
		tbExperiencia.cfFechaInicio,
		tbExperiencia.cfFechaFin,
		tbExperiencia.cnTiempoAnios,
		tbExperiencia.cnTiempoMeses,
		LTRIM(RTRIM(STR(tbExperiencia.cnTiempoAnios))) +  '.' + LTRIM(RTRIM(STR(tbExperiencia.cnTiempoMeses))) AS cnTiempos
	FROM
		tbExperiencia LEFT JOIN
		tbMaestroListas tbCargo ON tbCargo.cnID_Lista = tbExperiencia.cnID_Cargo AND tbCargo.cnID_GrupoLista = 2
	WHERE
		tbExperiencia.cnID_Candidato =  @cnID_Candidato
END





GO
/****** Object:  StoredProcedure [dbo].[tbCandidato_GetHojaVidaIdioma]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[tbCandidato_GetHojaVidaIdioma]
	@cnID_Candidato INT
AS
BEGIN
	
	
	SELECT 
		CASE WHEN tbOtros.cnID_Lista = tbIdioma.cnID_Lengua THEN tbIdioma.ctLenguaOtro ELSE tbLengua.ctElemento END AS ctLengua,		
		ISNULL(tbUniversidad.ctElemento,tbInstituto.ctElemento) AS ctInstitucion,
		tbNivelLectura.ctElemento ctNivelLectura,
		tbNivelEscritura.ctElemento ctNivelEscritura,
		tbNivelConversacion.ctElemento ctNivelConversacion,
		tbSituacion.ctElemento AS ctSituacion
	FROM 
		tbIdioma LEFT JOIN
		tbMaestroListas tbLengua ON tbLengua.cnID_Lista = tbIdioma.cnID_Lengua AND tbLengua.cnID_GrupoLista = 6 LEFT JOIN
		tbMaestroListas tbUniversidad ON tbUniversidad.cnID_Lista = tbIdioma.cnID_Institucion AND tbUniversidad.cnID_GrupoLista = 18 LEFT JOIN
		tbMaestroListas tbInstituto ON tbInstituto.cnID_Lista = tbIdioma.cnID_Institucion AND tbInstituto.cnID_GrupoLista IN (7,29) LEFT JOIN
		tbMaestroListas tbNivelLectura ON tbNivelLectura.cnID_Lista = tbIdioma.cnID_NivelLectura AND tbNivelLectura.cnID_GrupoLista = 8 LEFT JOIN
		tbMaestroListas tbNivelEscritura ON tbNivelEscritura.cnID_Lista = tbIdioma.cnID_NivelEscritura AND tbNivelEscritura.cnID_GrupoLista = 8 LEFT JOIN
		tbMaestroListas tbNivelConversacion ON tbNivelConversacion.cnID_Lista = tbIdioma.cnID_NivelConversacion AND tbNivelConversacion.cnID_GrupoLista = 8 LEFT JOIN
		tbMaestroListas tbSituacion ON tbSituacion.cnID_Lista = tbIdioma.cnID_Situacion AND tbSituacion.cnID_GrupoLista IN (12,26,27,28,29) LEFT JOIN
		tbMaestroListas tbOtros ON tbOtros.cnID_Lista = 1634
	WHERE
		tbIdioma.cnID_Candidato =  @cnID_Candidato

END





GO
/****** Object:  StoredProcedure [dbo].[tbCandidato_GetHojaVidaReferencia]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[tbCandidato_GetHojaVidaReferencia]
	@cnID_Candidato INT
AS
BEGIN
	

	SELECT
		ctNombreCompleto,
		ctNombreEmpresa,
		ctCargo,
		ctTelefono
	FROM
		tbReferencia
	WHERE
		tbReferencia.cnID_Candidato =  @cnID_Candidato
END





GO
/****** Object:  StoredProcedure [dbo].[tbCandidato_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbCandidato_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for inserting values to tbCandidato table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCandidato_Insert]
(
	
	@ctApellidoPaterno varchar(200),
	@ctApellidoMaterno varchar(200),
	@ctNombres varchar(200),
	@cnID_Sexo bigint,
	@ctEstadoCivil bigint,
	@ctLugarNacimiento varchar(100),
	@cfFechaNacimiento date,
	@cnID_PaisNacimiento int,
	@ctDireccion varchar(300),
	@cnID_Pais int,
	@cnID_Departamento int,
	@cnID_Provincia int,
	@cnID_Distrito int,
	@ctTelefonoCasa varchar(15) = NULL,
	@ctTelefonoMovil varchar(15) = NULL,
	@ctTelefonoOtro varchar(15) = NULL,
	@ctEmail char(100),
	@ctClaveAcceso varchar(150),
	@cnEstadoReg tinyint,
	@cnCodigoRecuperacion uniqueidentifier = NULL,
	@cfFechaCreacion datetime = NULL,
	@cfFechaActualizacion datetime = NULL,
	@ctIPAcceso varchar(15)
)
AS
	SET NOCOUNT ON

	INSERT INTO tbCandidato
			([ctApellidoPaterno] ,[ctApellidoMaterno] ,[ctNombres] ,[cnID_Sexo] ,[ctEstadoCivil] ,[ctLugarNacimiento] ,[cfFechaNacimiento] ,[cnID_PaisNacimiento] ,[ctDireccion] ,[cnID_Pais] ,[cnID_Departamento] ,[cnID_Provincia] ,[cnID_Distrito] ,[ctTelefonoCasa] ,[ctTelefonoMovil] ,[ctTelefonoOtro] ,[ctEmail] ,[ctClaveAcceso] ,[cnEstadoReg] ,[cnCodigoRecuperacion] ,[cfFechaCreacion] ,[cfFechaActualizacion] ,[ctIPAcceso] )
	VALUES	(@ctApellidoPaterno ,@ctApellidoMaterno ,@ctNombres ,@cnID_Sexo ,@ctEstadoCivil ,@ctLugarNacimiento ,@cfFechaNacimiento ,@cnID_PaisNacimiento ,@ctDireccion ,@cnID_Pais ,@cnID_Departamento ,@cnID_Provincia ,@cnID_Distrito ,@ctTelefonoCasa ,@ctTelefonoMovil ,@ctTelefonoOtro ,@ctEmail ,@ctClaveAcceso ,@cnEstadoReg ,@cnCodigoRecuperacion ,GETDATE() ,NULL ,@ctIPAcceso )
    

	SELECT  Scope_Identity() AS [cnID_Candidato];




GO
/****** Object:  StoredProcedure [dbo].[tbCandidato_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbCandidato_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for updating 	tbCandidato table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCandidato_Update]
(
	
	@cnID_Candidato bigint,
	@ctApellidoPaterno varchar(200),
	@ctApellidoMaterno varchar(200),
	@ctNombres varchar(200),
	@cnID_Sexo bigint,
	@ctEstadoCivil bigint,
	@ctLugarNacimiento varchar(100),
	@cfFechaNacimiento date,
	@cnID_PaisNacimiento int,
	@ctDireccion varchar(300),
	@cnID_Pais int,
	@cnID_Departamento int,
	@cnID_Provincia int,
	@cnID_Distrito int,
	@ctTelefonoCasa varchar(15) = NULL,
	@ctTelefonoMovil varchar(15) = NULL,
	@ctTelefonoOtro varchar(15) = NULL,
	@ctEmail char(100),
	@ctClaveAcceso varchar(150),
	@cnEstadoReg tinyint,
	@cnCodigoRecuperacion uniqueidentifier = NULL,
	@cfFechaCreacion datetime = NULL,
	@cfFechaActualizacion datetime = NULL,
	@ctIPAcceso varchar(15)
)
AS
	SET NOCOUNT ON

	UPDATE tbCandidato SET 

		[ctApellidoPaterno] = @ctApellidoPaterno ,
		[ctApellidoMaterno] = @ctApellidoMaterno ,
		[ctNombres] = @ctNombres ,
		[cnID_Sexo] = @cnID_Sexo ,
		[ctEstadoCivil] = @ctEstadoCivil ,
		[ctLugarNacimiento] = @ctLugarNacimiento ,
		[cfFechaNacimiento] = @cfFechaNacimiento ,
		[cnID_PaisNacimiento] = @cnID_PaisNacimiento ,
		[ctDireccion] = @ctDireccion ,
		[cnID_Pais] = @cnID_Pais ,
		[cnID_Departamento] = @cnID_Departamento ,
		[cnID_Provincia] = @cnID_Provincia ,
		[cnID_Distrito] = @cnID_Distrito ,
		[ctTelefonoCasa] = @ctTelefonoCasa ,
		[ctTelefonoMovil] = @ctTelefonoMovil ,
		[ctTelefonoOtro] = @ctTelefonoOtro ,
		[ctEmail] = @ctEmail ,
		[ctClaveAcceso] =CASE WHEN LTRIM(RTRIM(@ctClaveAcceso)) = '' THEN ctClaveAcceso ELSE @ctClaveAcceso END,
		[cnEstadoReg] = @cnEstadoReg ,
		[cnCodigoRecuperacion] = @cnCodigoRecuperacion ,
		--[cfFechaCreacion] = @cfFechaCreacion ,
		[cfFechaActualizacion] = GETDATE(),
		[ctIPAcceso] = @ctIPAcceso 
	WHERE [cnID_Candidato]=@cnID_Candidato

GO

/****** Object:  StoredProcedure [dbo].[tbCandidato_UpdateEmail]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tbCandidato_UpdateEmail]
(	
	@cnID_Candidato bigint,
	@ctEmail char(100)
)
AS
	SET NOCOUNT ON

	UPDATE tbCandidato SET
		[ctEmail] = @ctEmail
	WHERE [cnID_Candidato]=@cnID_Candidato

GO

/****** Object:  StoredProcedure [dbo].[tbCandidato_UpdateClaveAcceso]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tbCandidato_UpdateClaveAcceso]
(	
	@cnID_Candidato bigint,
	@ctClaveAcceso varchar(150)
)
AS
	SET NOCOUNT ON

	UPDATE tbCandidato SET
		[ctClaveAcceso] = @ctClaveAcceso
	WHERE [cnID_Candidato]=@cnID_Candidato

GO

/****** Object:  StoredProcedure [dbo].[tbPostulante_GetConsultaCandidatos]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tbCandidato_GetConsultaCandidatos]
	@ctNombreCompleto VARCHAR(600) = NULL,
	@ctNumeroDNI VARCHAR(16) = NULL,
	@ctEmail VARCHAR(100) = NULL
AS
BEGIN
	 
	SELECT 
		tbCandidato.*,
		tbDocIdentidad.ctNumero
	FROM 
		tbCandidato 
			LEFT JOIN tbDocIdentidad 
				ON tbCandidato.cnID_Candidato = tbDocIdentidad.cnID_Candidato 
					AND tbDocIdentidad.cnID_TipoDocumento  = 237 --DNI
	WHERE
		(@ctNombreCompleto IS NULL OR RTRIM(ctNombres) + ' ' + RTRIM(ctApellidoPaterno) + ' ' + RTRIM(ctApellidoMaterno) LIKE '%' + @ctNombreCompleto + '%')
		AND (@ctEmail IS NULL OR tbCandidato.ctEmail = @ctEmail)
		AND (@ctNumeroDNI IS NULL OR tbDocIdentidad.ctNumero  = @ctNumeroDNI)
END

GO


/****** Object:  StoredProcedure [dbo].[tbCnfgSistema_DeleteBycnID_Config]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbCnfgSistema_DeleteBycnID_Config
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbCnfgSistema table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCnfgSistema_DeleteBycnID_Config]
	(
	@cnID_Config int
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbCnfgSistema WHERE [cnID_Config]=@cnID_Config




GO
/****** Object:  StoredProcedure [dbo].[tbCnfgSistema_DeleteByctAlias]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbCnfgSistema_DeleteByctAlias
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbCnfgSistema table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCnfgSistema_DeleteByctAlias]
	(
	@ctAlias varchar(10)
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbCnfgSistema WHERE [ctAlias]=@ctAlias




GO
/****** Object:  StoredProcedure [dbo].[tbCnfgSistema_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbCnfgSistema_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbCnfgSistema table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCnfgSistema_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbCnfgSistema




GO
/****** Object:  StoredProcedure [dbo].[tbCnfgSistema_GetBycnID_Config]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbCnfgSistema_GetBycnID_Config
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbCnfgSistema table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCnfgSistema_GetBycnID_Config]
	(
	@cnID_Config int
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbCnfgSistema WHERE [cnID_Config]=@cnID_Config




GO
/****** Object:  StoredProcedure [dbo].[tbCnfgSistema_GetByctAlias]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbCnfgSistema_GetByctAlias
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbCnfgSistema table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCnfgSistema_GetByctAlias]
	(
	@ctAlias varchar(10)
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbCnfgSistema WHERE [ctAlias]=@ctAlias




GO
/****** Object:  StoredProcedure [dbo].[tbCnfgSistema_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbCnfgSistema_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for inserting values to tbCnfgSistema table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCnfgSistema_Insert]
(
	
	@ctDescripcion varchar(50),
	@ctAlias varchar(10),
	@ctValor varchar(10)
)
AS
	SET NOCOUNT ON

	INSERT INTO tbCnfgSistema
			([ctDescripcion] ,[ctAlias] ,[ctValor] )
	VALUES	(@ctDescripcion ,@ctAlias ,@ctValor )
    

	SELECT  Scope_Identity() AS [cnID_Config];




GO
/****** Object:  StoredProcedure [dbo].[tbCnfgSistema_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbCnfgSistema_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for updating 	tbCnfgSistema table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCnfgSistema_Update]
(
	
	@cnID_Config int,
	@ctDescripcion varchar(50),
	@ctAlias varchar(10),
	@ctValor varchar(10)
)
AS
	SET NOCOUNT ON

	UPDATE tbCnfgSistema SET 

		[ctDescripcion] = @ctDescripcion ,
		[ctAlias] = @ctAlias ,
		[ctValor] = @ctValor 
	WHERE [cnID_Config]=@cnID_Config




GO
/****** Object:  StoredProcedure [dbo].[tbCoberturaZona_DeleteBycnID_CoberturaZona]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbCoberturaZona_DeleteBycnID_CoberturaZona
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbCoberturaZona table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCoberturaZona_DeleteBycnID_CoberturaZona]
	(
	@cnID_CoberturaZona bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbCoberturaZona WHERE [cnID_CoberturaZona]=@cnID_CoberturaZona




GO
/****** Object:  StoredProcedure [dbo].[tbCoberturaZona_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbCoberturaZona_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbCoberturaZona table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCoberturaZona_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbCoberturaZona




GO
/****** Object:  StoredProcedure [dbo].[tbCoberturaZona_GetBycnID_Cobertura]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbCoberturaZona_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	27/06/2015 11:56:40
-- Description:	This stored procedure is intended for selecting all rows from tbCoberturaZona table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCoberturaZona_GetBycnID_Cobertura]
	@cnID_Cobertura bigint
AS
	SET NOCOUNT ON

	SELECT *  FROM tbCoberturaZona
	WHERE cnID_Cobertura = @cnID_Cobertura



GO
/****** Object:  StoredProcedure [dbo].[tbCoberturaZona_GetBycnID_CoberturaZona]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbCoberturaZona_GetBycnID_CoberturaZona
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbCoberturaZona table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCoberturaZona_GetBycnID_CoberturaZona]
	(
	@cnID_CoberturaZona bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbCoberturaZona WHERE [cnID_CoberturaZona]=@cnID_CoberturaZona




GO
/****** Object:  StoredProcedure [dbo].[tbCoberturaZona_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbCoberturaZona_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for inserting values to tbCoberturaZona table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCoberturaZona_Insert]
(
	
	@cnID_Cobertura bigint,
	@ctZona varchar(500)
)
AS
	SET NOCOUNT ON

	INSERT INTO tbCoberturaZona
			([cnID_Cobertura] ,[ctZona] )
	VALUES	(@cnID_Cobertura ,@ctZona )
    

	SELECT  Scope_Identity() AS [cnID_CoberturaZona];




GO
/****** Object:  StoredProcedure [dbo].[tbCoberturaZona_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbCoberturaZona_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for updating 	tbCoberturaZona table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbCoberturaZona_Update]
(
	
	@cnID_CoberturaZona bigint,
	@cnID_Cobertura bigint,
	@ctZona varchar(500)
)
AS
	SET NOCOUNT ON

	UPDATE tbCoberturaZona SET 

		[cnID_Cobertura] = @cnID_Cobertura ,
		[ctZona] = @ctZona 
	WHERE [cnID_CoberturaZona]=@cnID_CoberturaZona




GO
/****** Object:  StoredProcedure [dbo].[tbComplementario_DeleteBycnID_Complementario]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbComplementario_DeleteBycnID_Complementario
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbComplementario table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbComplementario_DeleteBycnID_Complementario]
	(
	@cnID_Complementario bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbComplementario WHERE [cnID_Complementario]=@cnID_Complementario




GO
/****** Object:  StoredProcedure [dbo].[tbComplementario_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbComplementario_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbComplementario table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbComplementario_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbComplementario




GO
/****** Object:  StoredProcedure [dbo].[tbComplementario_GetBycnID_Candidato]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[tbComplementario_GetBycnID_Candidato]
@cnID_Candidato int
as
begin

SELECT * FROM 
tbComplementario
WHERE cnID_Candidato = @cnID_Candidato
end





GO
/****** Object:  StoredProcedure [dbo].[tbComplementario_GetBycnID_Complementario]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbComplementario_GetBycnID_Complementario
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbComplementario table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbComplementario_GetBycnID_Complementario]
	(
	@cnID_Complementario bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbComplementario WHERE [cnID_Complementario]=@cnID_Complementario




GO
/****** Object:  StoredProcedure [dbo].[tbComplementario_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbComplementario_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for inserting values to tbComplementario table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbComplementario_Insert]
(
	
	@cnID_Candidato bigint,
	@cnID_TipoEstudio bigint,
	@ctDescripcion varchar(20) = NULL,
	@cnID_Institucion bigint = NULL,
	@ctInstitucionOtro varchar(200) = NULL,
	@cnID_Situacion bigint = NULL,
	@cfFechaInicio datetime,
	@cfFechaFin datetime,
	@cnID_Pais int,
	@ctCertificado varchar(150) = NULL,
	@cfFechaCreacion datetime,
	@cfFechaActualizacion datetime = NULL,
	@ctIPAcceso varchar(15)
)
AS
	SET NOCOUNT ON

	INSERT INTO tbComplementario
			([cnID_Candidato] ,[cnID_TipoEstudio] ,[ctDescripcion] ,[cnID_Institucion] ,[ctInstitucionOtro] ,[cnID_Situacion] ,[cfFechaInicio] ,[cfFechaFin] ,[cnID_Pais] ,[ctCertificado] ,[cfFechaCreacion] ,[cfFechaActualizacion] ,[ctIPAcceso] )
	VALUES	(@cnID_Candidato ,@cnID_TipoEstudio ,@ctDescripcion ,@cnID_Institucion ,@ctInstitucionOtro ,@cnID_Situacion ,@cfFechaInicio ,@cfFechaFin ,@cnID_Pais ,@ctCertificado ,GETDATE() ,NULL,@ctIPAcceso )
    

	SELECT  Scope_Identity() AS [cnID_Complementario];




GO
/****** Object:  StoredProcedure [dbo].[tbComplementario_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbComplementario_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for updating 	tbComplementario table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbComplementario_Update]
(
	
	@cnID_Complementario bigint,
	@cnID_Candidato bigint,
	@cnID_TipoEstudio bigint,
	@ctDescripcion varchar(20) = NULL,
	@cnID_Institucion bigint = NULL,
	@ctInstitucionOtro varchar(200) = NULL,
	@cnID_Situacion bigint = NULL,
	@cfFechaInicio datetime,
	@cfFechaFin datetime,
	@cnID_Pais int,
	@ctCertificado varchar(150) = NULL,
	@cfFechaCreacion datetime,
	@cfFechaActualizacion datetime = NULL,
	@ctIPAcceso varchar(15)
)
AS
	SET NOCOUNT ON

	UPDATE tbComplementario SET 

		[cnID_Candidato] = @cnID_Candidato ,
		[cnID_TipoEstudio] = @cnID_TipoEstudio ,
		[ctDescripcion] = @ctDescripcion ,
		[cnID_Institucion] = @cnID_Institucion ,
		[ctInstitucionOtro] = @ctInstitucionOtro ,
		[cnID_Situacion] = @cnID_Situacion ,
		[cfFechaInicio] = @cfFechaInicio ,
		[cfFechaFin] = @cfFechaFin ,
		[cnID_Pais] = @cnID_Pais ,
		[ctCertificado] = @ctCertificado ,
		--[cfFechaCreacion] = @cfFechaCreacion ,
		[cfFechaActualizacion] = GETDATE(),
		[ctIPAcceso] = @ctIPAcceso 
	WHERE [cnID_Complementario]=@cnID_Complementario




GO
/****** Object:  StoredProcedure [dbo].[tbDepartamento_DeleteBycnID_Departamento]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbDepartamento_DeleteBycnID_Departamento
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbDepartamento table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbDepartamento_DeleteBycnID_Departamento]
	(
	@cnID_Departamento int
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbDepartamento WHERE [cnID_Departamento]=@cnID_Departamento




GO
/****** Object:  StoredProcedure [dbo].[tbDepartamento_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbDepartamento_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbDepartamento table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbDepartamento_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbDepartamento




GO
/****** Object:  StoredProcedure [dbo].[tbDepartamento_GetBycnID_Departamento]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbDepartamento_GetBycnID_Departamento
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbDepartamento table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbDepartamento_GetBycnID_Departamento]
	(
	@cnID_Departamento int
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbDepartamento WHERE [cnID_Departamento]=@cnID_Departamento




GO
/****** Object:  StoredProcedure [dbo].[tbDepartamento_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbDepartamento_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for inserting values to tbDepartamento table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbDepartamento_Insert]
(
	
	@ctDepartamento varchar(150) = NULL,
	@ctCodigoISO char(2) = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO tbDepartamento
			([ctDepartamento] ,[ctCodigoISO] )
	VALUES	(@ctDepartamento ,@ctCodigoISO )
    

	SELECT  Scope_Identity() AS [cnID_Departamento];




GO
/****** Object:  StoredProcedure [dbo].[tbDepartamento_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbDepartamento_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for updating 	tbDepartamento table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbDepartamento_Update]
(
	
	@cnID_Departamento int,
	@ctDepartamento varchar(150) = NULL,
	@ctCodigoISO char(2) = NULL
)
AS
	SET NOCOUNT ON

	UPDATE tbDepartamento SET 

		[ctDepartamento] = @ctDepartamento ,
		[ctCodigoISO] = @ctCodigoISO 
	WHERE [cnID_Departamento]=@cnID_Departamento




GO
/****** Object:  StoredProcedure [dbo].[tbDistrito_DeleteBycnID_Distrito]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbDistrito_DeleteBycnID_Distrito
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbDistrito table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbDistrito_DeleteBycnID_Distrito]
	(
	@cnID_Distrito int
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbDistrito WHERE [cnID_Distrito]=@cnID_Distrito




GO
/****** Object:  StoredProcedure [dbo].[tbDistrito_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbDistrito_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbDistrito table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbDistrito_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbDistrito




GO
/****** Object:  StoredProcedure [dbo].[tbDistrito_GetBycnID_Distrito]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbDistrito_GetBycnID_Distrito
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbDistrito table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbDistrito_GetBycnID_Distrito]
	(
	@cnID_Distrito int
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbDistrito WHERE [cnID_Distrito]=@cnID_Distrito




GO
/****** Object:  StoredProcedure [dbo].[tbDistrito_GetBycnID_Provincia]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbDistrito_GetBycnID_Distrito
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	22/04/2015 19:45:04
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbDistrito table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbDistrito_GetBycnID_Provincia]
	(
	@cnID_Provincia int
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbDistrito WHERE cnID_Provincia=@cnID_Provincia




GO
/****** Object:  StoredProcedure [dbo].[tbDistrito_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbDistrito_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for inserting values to tbDistrito table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbDistrito_Insert]
(
	
	@cnID_Departamento int = NULL,
	@cnID_Provincia int,
	@ctDistrito varchar(150),
	@ctCodigoISO varchar(6) = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO tbDistrito
			([cnID_Departamento] ,[cnID_Provincia] ,[ctDistrito] ,[ctCodigoISO] )
	VALUES	(@cnID_Departamento ,@cnID_Provincia ,@ctDistrito ,@ctCodigoISO )
    

	SELECT  Scope_Identity() AS [cnID_Distrito];




GO
/****** Object:  StoredProcedure [dbo].[tbDistrito_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbDistrito_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for updating 	tbDistrito table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbDistrito_Update]
(
	
	@cnID_Distrito int,
	@cnID_Departamento int = NULL,
	@cnID_Provincia int,
	@ctDistrito varchar(150),
	@ctCodigoISO varchar(6) = NULL
)
AS
	SET NOCOUNT ON

	UPDATE tbDistrito SET 

		[cnID_Departamento] = @cnID_Departamento ,
		[cnID_Provincia] = @cnID_Provincia ,
		[ctDistrito] = @ctDistrito ,
		[ctCodigoISO] = @ctCodigoISO 
	WHERE [cnID_Distrito]=@cnID_Distrito




GO
/****** Object:  StoredProcedure [dbo].[tbDocIdentidad_DeleteBycnID_Candidato]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbDocIdentidad_DeleteBycnID_Candidato
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	22/04/2015 19:45:04
-- Description:	This stored procedure is intended for deleting a specific row from tbDocIdentidad table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbDocIdentidad_DeleteBycnID_Candidato]
	(
	@cnID_Candidato bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbDocIdentidad WHERE [cnID_Candidato]=@cnID_Candidato




GO
/****** Object:  StoredProcedure [dbo].[tbDocIdentidad_DeleteBycnID_DocIdentidad]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbDocIdentidad_DeleteBycnID_DocIdentidad
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbDocIdentidad table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbDocIdentidad_DeleteBycnID_DocIdentidad]
	(
	@cnID_DocIdentidad bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbDocIdentidad WHERE [cnID_DocIdentidad]=@cnID_DocIdentidad




GO
/****** Object:  StoredProcedure [dbo].[tbDocIdentidad_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbDocIdentidad_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbDocIdentidad table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbDocIdentidad_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbDocIdentidad




GO
/****** Object:  StoredProcedure [dbo].[tbDocIdentidad_GetAllByCandidato]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- ==========================================================================================
-- Entity Name:	tbDocIdentidad_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	22/04/2015 19:45:04
-- Description:	This stored procedure is intended for selecting all rows from tbDocIdentidad table
-- ==========================================================================================
create PROCEDURE [dbo].[tbDocIdentidad_GetAllByCandidato]
@cnID_Candidato int
AS
	SET NOCOUNT ON

	SELECT *  FROM tbDocIdentidad
	where cnID_Candidato = @cnID_Candidato





GO
/****** Object:  StoredProcedure [dbo].[tbDocIdentidad_GetBycnID_DocIdentidad]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbDocIdentidad_GetBycnID_DocIdentidad
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbDocIdentidad table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbDocIdentidad_GetBycnID_DocIdentidad]
	(
	@cnID_DocIdentidad bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbDocIdentidad WHERE [cnID_DocIdentidad]=@cnID_DocIdentidad




GO
/****** Object:  StoredProcedure [dbo].[tbDocIdentidad_GetByctNumero]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbDocIdentidad_GetByctNumero
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbDocIdentidad table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbDocIdentidad_GetByctNumero]
	(
	@ctNumero varchar(16)
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbDocIdentidad WHERE [ctNumero]=@ctNumero




GO
/****** Object:  StoredProcedure [dbo].[tbDocIdentidad_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbDocIdentidad_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for inserting values to tbDocIdentidad table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbDocIdentidad_Insert]
(
	
	@cnID_Candidato bigint,
	@cnID_TipoDocumento bigint,
	@ctNumero varchar(16)
)
AS
	SET NOCOUNT ON

	INSERT INTO tbDocIdentidad
			([cnID_Candidato] ,[cnID_TipoDocumento] ,[ctNumero] )
	VALUES	(@cnID_Candidato ,@cnID_TipoDocumento ,@ctNumero )
    

	SELECT  Scope_Identity() AS [cnID_DocIdentidad];




GO
/****** Object:  StoredProcedure [dbo].[tbDocIdentidad_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbDocIdentidad_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for updating 	tbDocIdentidad table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbDocIdentidad_Update]
(
	
	@cnID_DocIdentidad bigint,
	@cnID_Candidato bigint,
	@cnID_TipoDocumento bigint,
	@ctNumero varchar(16)
)
AS
	SET NOCOUNT ON

	UPDATE tbDocIdentidad SET 

		[cnID_Candidato] = @cnID_Candidato ,
		[cnID_TipoDocumento] = @cnID_TipoDocumento ,
		[ctNumero] = @ctNumero 
	WHERE [cnID_DocIdentidad]=@cnID_DocIdentidad




GO
/****** Object:  StoredProcedure [dbo].[tbExperiencia_DeleteBycnID_Experiencia]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbExperiencia_DeleteBycnID_Experiencia
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbExperiencia table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbExperiencia_DeleteBycnID_Experiencia]
	(
	@cnID_Experiencia bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbExperiencia WHERE [cnID_Experiencia]=@cnID_Experiencia




GO
/****** Object:  StoredProcedure [dbo].[tbExperiencia_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbExperiencia_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbExperiencia table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbExperiencia_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbExperiencia




GO
/****** Object:  StoredProcedure [dbo].[tbExperiencia_GetBycnID_Candidato]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[tbExperiencia_GetBycnID_Candidato]
@cnID_Candidato int
as
begin

SELECT 
	tbExperiencia.* ,
	tbCargo.ctElemento as ctCargo
FROM 
	tbExperiencia INNER JOIN
	tbMaestroListas tbCargo ON  tbCargo.cnID_Lista = tbExperiencia.cnID_Cargo AND tbCargo.cnID_GrupoLista = 2
WHERE 
	cnID_Candidato = @cnID_Candidato
end



GO
/****** Object:  StoredProcedure [dbo].[tbExperiencia_GetBycnID_Experiencia]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbExperiencia_GetBycnID_Experiencia
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbExperiencia table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbExperiencia_GetBycnID_Experiencia]
	(
	@cnID_Experiencia bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbExperiencia WHERE [cnID_Experiencia]=@cnID_Experiencia




GO
/****** Object:  StoredProcedure [dbo].[tbExperiencia_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbExperiencia_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for inserting values to tbExperiencia table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbExperiencia_Insert]
(
	
	@cnID_Candidato bigint,
	@cnID_TipoEntidad bigint,
	@ctNombreEmpresa varchar(200),
	@cnID_Rubro bigint,
	@cnID_Cargo bigint,
	@ctCargoOtro varchar(50) = NULL,
	@cnID_Area bigint,
	@cnID_Pais int,
	@cnID_TipoContrato bigint,
	@cnSubordinados smallint = NULL,
	@ctDescripcionCargo varchar(500),
	@cfFechaInicio date,
	@cfFechaFin date,
	@cnTiempoAnios tinyint = NULL,
	@cnTiempoMeses tinyint = NULL,
	@ctCertificado varchar(150) = NULL,
	@cfFechaCreacion datetime,
	@cfFechaActualizacion datetime = NULL,
	@ctIPAcceso varchar(15)
)
AS
	SET NOCOUNT ON

	INSERT INTO tbExperiencia
			([cnID_Candidato] ,[cnID_TipoEntidad] ,[ctNombreEmpresa] ,[cnID_Rubro] ,[cnID_Cargo] ,[ctCargoOtro] ,[cnID_Area] ,[cnID_Pais] ,[cnID_TipoContrato] ,[cnSubordinados] ,[ctDescripcionCargo] ,[cfFechaInicio] ,[cfFechaFin] ,[cnTiempoAnios] ,[cnTiempoMeses] ,[ctCertificado] ,[cfFechaCreacion] ,[cfFechaActualizacion] ,[ctIPAcceso] )
	VALUES	(@cnID_Candidato ,@cnID_TipoEntidad ,@ctNombreEmpresa ,@cnID_Rubro ,@cnID_Cargo ,@ctCargoOtro ,@cnID_Area ,@cnID_Pais ,@cnID_TipoContrato ,@cnSubordinados ,@ctDescripcionCargo ,@cfFechaInicio ,@cfFechaFin ,@cnTiempoAnios ,@cnTiempoMeses ,@ctCertificado ,GETDATE() ,NULL ,@ctIPAcceso )
    

	SELECT  Scope_Identity() AS [cnID_Experiencia];




GO
/****** Object:  StoredProcedure [dbo].[tbExperiencia_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbExperiencia_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for updating 	tbExperiencia table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbExperiencia_Update]
(
	
	@cnID_Experiencia bigint,
	@cnID_Candidato bigint,
	@cnID_TipoEntidad bigint,
	@ctNombreEmpresa varchar(200),
	@cnID_Rubro bigint,
	@cnID_Cargo bigint,
	@ctCargoOtro varchar(50) = NULL,
	@cnID_Area bigint,
	@cnID_Pais int,
	@cnID_TipoContrato bigint,
	@cnSubordinados smallint = NULL,
	@ctDescripcionCargo varchar(500),
	@cfFechaInicio date,
	@cfFechaFin date,
	@cnTiempoAnios tinyint = NULL,
	@cnTiempoMeses tinyint = NULL,
	@ctCertificado varchar(150) = NULL,
	@cfFechaCreacion datetime,
	@cfFechaActualizacion datetime = NULL,
	@ctIPAcceso varchar(15)
)
AS
	SET NOCOUNT ON

	UPDATE tbExperiencia SET 

		[cnID_Candidato] = @cnID_Candidato ,
		[cnID_TipoEntidad] = @cnID_TipoEntidad ,
		[ctNombreEmpresa] = @ctNombreEmpresa ,
		[cnID_Rubro] = @cnID_Rubro ,
		[cnID_Cargo] = @cnID_Cargo ,
		[ctCargoOtro] = @ctCargoOtro ,
		[cnID_Area] = @cnID_Area ,
		[cnID_Pais] = @cnID_Pais ,
		[cnID_TipoContrato] = @cnID_TipoContrato ,
		[cnSubordinados] = @cnSubordinados ,
		[ctDescripcionCargo] = @ctDescripcionCargo ,
		[cfFechaInicio] = @cfFechaInicio ,
		[cfFechaFin] = @cfFechaFin ,
		[cnTiempoAnios] = @cnTiempoAnios ,
		[cnTiempoMeses] = @cnTiempoMeses ,
		[ctCertificado] = @ctCertificado ,
		--[cfFechaCreacion] = @cfFechaCreacion ,
		[cfFechaActualizacion] = GETDATE(),
		[ctIPAcceso] = @ctIPAcceso 
	WHERE [cnID_Experiencia]=@cnID_Experiencia




GO
/****** Object:  StoredProcedure [dbo].[tbIdioma_DeleteBycnID_Idioma]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbIdioma_DeleteBycnID_Idioma
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbIdioma table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbIdioma_DeleteBycnID_Idioma]
	(
	@cnID_Idioma bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbIdioma WHERE [cnID_Idioma]=@cnID_Idioma




GO
/****** Object:  StoredProcedure [dbo].[tbIdioma_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbIdioma_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbIdioma table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbIdioma_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbIdioma




GO
/****** Object:  StoredProcedure [dbo].[tbIdioma_GetBycnID_Candidato]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[tbIdioma_GetBycnID_Candidato]
	@cnID_Candidato INT
AS
BEGIN

SELECT 
*
FROM 
	tbIdioma
WHERE cnID_Candidato = @cnID_Candidato

END



GO
/****** Object:  StoredProcedure [dbo].[tbIdioma_GetBycnID_Idioma]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbIdioma_GetBycnID_Idioma
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbIdioma table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbIdioma_GetBycnID_Idioma]
	(
	@cnID_Idioma bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbIdioma WHERE [cnID_Idioma]=@cnID_Idioma




GO
/****** Object:  StoredProcedure [dbo].[tbIdioma_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbIdioma_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for inserting values to tbIdioma table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbIdioma_Insert]
(
	
	@cnID_Candidato bigint,
	@cnID_Lengua bigint,
	@ctLenguaOtro varchar(20) = NULL,
	@cnID_Institucion bigint,
	@cnID_NivelLectura bigint,
	@cnID_NivelEscritura bigint,
	@cnID_NivelConversacion bigint,
	@cnID_Situacion bigint,
	@ctCertificado varchar(150) = NULL,
	@cfFechaCreacion datetime,
	@cfFechaActualizacion datetime = NULL,
	@ctIPAcceso varchar(15)
)
AS
	SET NOCOUNT ON

	INSERT INTO tbIdioma
			([cnID_Candidato] ,[cnID_Lengua] ,[ctLenguaOtro] ,[cnID_Institucion] ,[cnID_NivelLectura] ,[cnID_NivelEscritura] ,[cnID_NivelConversacion] ,[cnID_Situacion] ,[ctCertificado] ,[cfFechaCreacion] ,[cfFechaActualizacion] ,[ctIPAcceso] )
	VALUES	(@cnID_Candidato ,@cnID_Lengua ,@ctLenguaOtro ,@cnID_Institucion ,@cnID_NivelLectura ,@cnID_NivelEscritura ,@cnID_NivelConversacion ,@cnID_Situacion ,@ctCertificado ,GETDATE() ,NULL ,@ctIPAcceso )
    

	SELECT  Scope_Identity() AS [cnID_Idioma];




GO
/****** Object:  StoredProcedure [dbo].[tbIdioma_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbIdioma_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:48 a.m.
-- Description:	This stored procedure is intended for updating 	tbIdioma table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbIdioma_Update]
(
	
	@cnID_Idioma bigint,
	@cnID_Candidato bigint,
	@cnID_Lengua bigint,
	@ctLenguaOtro varchar(20) = NULL,
	@cnID_Institucion bigint,
	@cnID_NivelLectura bigint,
	@cnID_NivelEscritura bigint,
	@cnID_NivelConversacion bigint,
	@cnID_Situacion bigint,
	@ctCertificado varchar(150) = NULL,
	@cfFechaCreacion datetime =null,
	@cfFechaActualizacion datetime = NULL,
	@ctIPAcceso varchar(15)
)
AS
	SET NOCOUNT ON

	UPDATE tbIdioma SET 

		[cnID_Candidato] = @cnID_Candidato ,
		[cnID_Lengua] = @cnID_Lengua ,
		[ctLenguaOtro] = @ctLenguaOtro ,
		[cnID_Institucion] = @cnID_Institucion ,
		[cnID_NivelLectura] = @cnID_NivelLectura ,
		[cnID_NivelEscritura] = @cnID_NivelEscritura ,
		[cnID_NivelConversacion] = @cnID_NivelConversacion ,
		[cnID_Situacion] = @cnID_Situacion ,
		[ctCertificado] = @ctCertificado ,
		--[cfFechaCreacion] = @cfFechaCreacion ,
		[cfFechaActualizacion] = GETDATE(),
		[ctIPAcceso] = @ctIPAcceso 
	WHERE [cnID_Idioma]=@cnID_Idioma




GO
/****** Object:  StoredProcedure [dbo].[tbMaestroGrupos_DeleteBycnID_GrupoLista]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbMaestroGrupos_DeleteBycnID_GrupoLista
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbMaestroGrupos table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbMaestroGrupos_DeleteBycnID_GrupoLista]
	(
	@cnID_GrupoLista int
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbMaestroGrupos WHERE [cnID_GrupoLista]=@cnID_GrupoLista




GO
/****** Object:  StoredProcedure [dbo].[tbMaestroGrupos_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbMaestroGrupos_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbMaestroGrupos table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbMaestroGrupos_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbMaestroGrupos




GO
/****** Object:  StoredProcedure [dbo].[tbMaestroGrupos_GetBycnID_GrupoLista]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbMaestroGrupos_GetBycnID_GrupoLista
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbMaestroGrupos table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbMaestroGrupos_GetBycnID_GrupoLista]
	(
	@cnID_GrupoLista int
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbMaestroGrupos WHERE [cnID_GrupoLista]=@cnID_GrupoLista




GO
/****** Object:  StoredProcedure [dbo].[tbMaestroGrupos_GetByctAlias]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbMaestroGrupos_GetByctAlias
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbMaestroGrupos table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbMaestroGrupos_GetByctAlias]
	(
	@ctAlias varchar(10)
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbMaestroGrupos WHERE [ctAlias]=@ctAlias




GO
/****** Object:  StoredProcedure [dbo].[tbMaestroGrupos_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbMaestroGrupos_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for inserting values to tbMaestroGrupos table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbMaestroGrupos_Insert]
(
	
	@ctNombreGrupo varchar(100),
	@ctAlias varchar(10) = NULL,
	@cnEstado tinyint
)
AS
	SET NOCOUNT ON

	INSERT INTO tbMaestroGrupos
			([ctNombreGrupo] ,[ctAlias] ,[cnEstado] )
	VALUES	(@ctNombreGrupo ,@ctAlias ,@cnEstado )
    

	SELECT  Scope_Identity() AS [cnID_GrupoLista];




GO
/****** Object:  StoredProcedure [dbo].[tbMaestroGrupos_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbMaestroGrupos_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for updating 	tbMaestroGrupos table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbMaestroGrupos_Update]
(
	
	@cnID_GrupoLista int,
	@ctNombreGrupo varchar(100),
	@ctAlias varchar(10) = NULL,
	@cnEstado tinyint
)
AS
	SET NOCOUNT ON

	UPDATE tbMaestroGrupos SET 

		[ctNombreGrupo] = @ctNombreGrupo ,
		[ctAlias] = @ctAlias ,
		[cnEstado] = @cnEstado 
	WHERE [cnID_GrupoLista]=@cnID_GrupoLista




GO
/****** Object:  StoredProcedure [dbo].[tbMaestroListas_DeleteBycnID_Lista]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbMaestroListas_DeleteBycnID_Lista
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbMaestroListas table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbMaestroListas_DeleteBycnID_Lista]
	(
	@cnID_Lista bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbMaestroListas WHERE [cnID_Lista]=@cnID_Lista




GO
/****** Object:  StoredProcedure [dbo].[tbMaestroListas_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbMaestroListas_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbMaestroListas table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbMaestroListas_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbMaestroListas




GO
/****** Object:  StoredProcedure [dbo].[tbMaestroListas_GetBycnID_GrupoLista]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbMaestroListas_GetBycnID_Lista
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	22/04/2015 19:45:04
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbMaestroListas table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbMaestroListas_GetBycnID_GrupoLista]
	(
	@cnID_GrupoLista bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbMaestroListas 
	WHERE [cnID_GrupoLista]=@cnID_GrupoLista
		AND [cnEstado]=1



GO
/****** Object:  StoredProcedure [dbo].[tbMaestroListas_GetBycnID_Lista]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbMaestroListas_GetBycnID_Lista
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbMaestroListas table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbMaestroListas_GetBycnID_Lista]
	(
	@cnID_Lista bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbMaestroListas WHERE [cnID_Lista]=@cnID_Lista




GO
/****** Object:  StoredProcedure [dbo].[tbMaestroListas_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbMaestroListas_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for inserting values to tbMaestroListas table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbMaestroListas_Insert]
(
	
	@cnID_GrupoLista int,
	@ctElemento varchar(150),
	@cnEstado tinyint,
	@cnOrden int = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO tbMaestroListas
			([cnID_GrupoLista] ,[ctElemento] ,[cnEstado] ,[cnOrden] )
	VALUES	(@cnID_GrupoLista ,@ctElemento ,@cnEstado ,@cnOrden )
    

	SELECT  Scope_Identity() AS [cnID_Lista];




GO
/****** Object:  StoredProcedure [dbo].[tbMaestroListas_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbMaestroListas_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for updating 	tbMaestroListas table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbMaestroListas_Update]
(
	
	@cnID_Lista bigint,
	@cnID_GrupoLista int,
	@ctElemento varchar(150),
	@cnEstado tinyint,
	@cnOrden int = NULL
)
AS
	SET NOCOUNT ON

	UPDATE tbMaestroListas SET 

		[cnID_GrupoLista] = @cnID_GrupoLista ,
		[ctElemento] = @ctElemento ,
		[cnEstado] = @cnEstado ,
		[cnOrden] = @cnOrden 
	WHERE [cnID_Lista]=@cnID_Lista




GO
/****** Object:  StoredProcedure [dbo].[tbMenu_DeleteBycnID_Menu]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbMenu_DeleteBycnID_Menu
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbMenu table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbMenu_DeleteBycnID_Menu]
	(
	@cnID_Menu bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbMenu WHERE [cnID_Menu]=@cnID_Menu




GO
/****** Object:  StoredProcedure [dbo].[tbMenu_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbMenu_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbMenu table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbMenu_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbMenu




GO
/****** Object:  StoredProcedure [dbo].[tbMenu_GetBycnID_Menu]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbMenu_GetBycnID_Menu
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbMenu table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbMenu_GetBycnID_Menu]
	(
	@cnID_Menu bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbMenu WHERE [cnID_Menu]=@cnID_Menu




GO
/****** Object:  StoredProcedure [dbo].[tbMenu_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbMenu_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for inserting values to tbMenu table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbMenu_Insert]
(
	
	@cnID_Perfil int,
	@ctTitulo varchar(25),
	@ctDescipcion varchar(150) = NULL,
	@ctPagina varchar(20),
	@cnOrden int
)
AS
	SET NOCOUNT ON

	INSERT INTO tbMenu
			([cnID_Perfil] ,[ctTitulo] ,[ctDescipcion] ,[ctPagina] ,[cnOrden] )
	VALUES	(@cnID_Perfil ,@ctTitulo ,@ctDescipcion ,@ctPagina ,@cnOrden )
    

	SELECT  Scope_Identity() AS [cnID_Menu];




GO
/****** Object:  StoredProcedure [dbo].[tbMenu_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbMenu_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for updating 	tbMenu table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbMenu_Update]
(
	
	@cnID_Menu bigint,
	@cnID_Perfil int,
	@ctTitulo varchar(25),
	@ctDescipcion varchar(150) = NULL,
	@ctPagina varchar(20),
	@cnOrden int
)
AS
	SET NOCOUNT ON

	UPDATE tbMenu SET 

		[cnID_Perfil] = @cnID_Perfil ,
		[ctTitulo] = @ctTitulo ,
		[ctDescipcion] = @ctDescipcion ,
		[ctPagina] = @ctPagina ,
		[cnOrden] = @cnOrden 
	WHERE [cnID_Menu]=@cnID_Menu




GO
/****** Object:  StoredProcedure [dbo].[tbPais_DeleteBycnID_Pais]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPais_DeleteBycnID_Pais
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbPais table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPais_DeleteBycnID_Pais]
	(
	@cnID_Pais int
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbPais WHERE [cnID_Pais]=@cnID_Pais




GO
/****** Object:  StoredProcedure [dbo].[tbPais_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPais_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbPais table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPais_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbPais




GO
/****** Object:  StoredProcedure [dbo].[tbPais_GetBycnID_Pais]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbPais_GetBycnID_Pais
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbPais table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPais_GetBycnID_Pais]
	(
	@cnID_Pais int
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbPais WHERE [cnID_Pais]=@cnID_Pais




GO
/****** Object:  StoredProcedure [dbo].[tbPais_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPais_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for inserting values to tbPais table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPais_Insert]
(
	
	@ctPais varchar(150) = NULL,
	@ctCodigoISO char(3) = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO tbPais
			([ctPais] ,[ctCodigoISO] )
	VALUES	(@ctPais ,@ctCodigoISO )
    

	SELECT  Scope_Identity() AS [cnID_Pais];




GO
/****** Object:  StoredProcedure [dbo].[tbPais_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbPais_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for updating 	tbPais table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPais_Update]
(
	
	@cnID_Pais int,
	@ctPais varchar(150) = NULL,
	@ctCodigoISO char(3) = NULL
)
AS
	SET NOCOUNT ON

	UPDATE tbPais SET 

		[ctPais] = @ctPais ,
		[ctCodigoISO] = @ctCodigoISO 
	WHERE [cnID_Pais]=@cnID_Pais




GO
/****** Object:  StoredProcedure [dbo].[tbPerfilUsuario_DeleteBycnID_Perfil]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPerfilUsuario_DeleteBycnID_Perfil
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbPerfilUsuario table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPerfilUsuario_DeleteBycnID_Perfil]
	(
	@cnID_Perfil int
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbPerfilUsuario WHERE [cnID_Perfil]=@cnID_Perfil




GO
/****** Object:  StoredProcedure [dbo].[tbPerfilUsuario_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPerfilUsuario_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbPerfilUsuario table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPerfilUsuario_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbPerfilUsuario




GO
/****** Object:  StoredProcedure [dbo].[tbPerfilUsuario_GetBycnID_Perfil]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbPerfilUsuario_GetBycnID_Perfil
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbPerfilUsuario table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPerfilUsuario_GetBycnID_Perfil]
	(
	@cnID_Perfil int
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbPerfilUsuario WHERE [cnID_Perfil]=@cnID_Perfil




GO
/****** Object:  StoredProcedure [dbo].[tbPerfilUsuario_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPerfilUsuario_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for inserting values to tbPerfilUsuario table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPerfilUsuario_Insert]
(
	
	@cnID_Perfil int,
	@ctDescripcion varchar(100),
	@cnEstadoReg bit
)
AS
	SET NOCOUNT ON

	INSERT INTO tbPerfilUsuario
			([cnID_Perfil] ,[ctDescripcion] ,[cnEstadoReg] )
	VALUES	(@cnID_Perfil ,@ctDescripcion ,@cnEstadoReg )
    





GO
/****** Object:  StoredProcedure [dbo].[tbPerfilUsuario_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbPerfilUsuario_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for updating 	tbPerfilUsuario table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPerfilUsuario_Update]
(
	
	@cnID_Perfil int,
	@ctDescripcion varchar(100),
	@cnEstadoReg bit
)
AS
	SET NOCOUNT ON

	UPDATE tbPerfilUsuario SET 

		[cnID_Perfil] = @cnID_Perfil ,
		[ctDescripcion] = @ctDescripcion ,
		[cnEstadoReg] = @cnEstadoReg 
	WHERE [cnID_Perfil]=@cnID_Perfil




GO
/****** Object:  StoredProcedure [dbo].[tbPostulante_AnularPostulacion]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[tbPostulante_AnularPostulacion]
	@cnID_Candidato bigint,
	@cnID_Proceso bigint
AS
BEGIN

	UPDATE tbPostulante
		SET tbPostulante.cnEstadoReg = 0
	FROM 
		tbPostulante
	WHERE cnID_Candidato = @cnID_Candidato
	AND cnID_Proceso = @cnID_Proceso
	AND tbPostulante.cnEstadoReg = 1

END


GO
/****** Object:  StoredProcedure [dbo].[tbPostulante_DeleteBycnID_Postulante]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulante_DeleteBycnID_Postulante
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbPostulante table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulante_DeleteBycnID_Postulante]
	(
	@cnID_Postulante bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbPostulante WHERE [cnID_Postulante]=@cnID_Postulante




GO
/****** Object:  StoredProcedure [dbo].[tbPostulante_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulante_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbPostulante table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulante_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbPostulante




GO
/****** Object:  StoredProcedure [dbo].[tbPostulante_GetBycnID_Candidato]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulante_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	22/04/2015 19:45:04
-- Description:	This stored procedure is intended for selecting all rows from tbPostulante table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulante_GetBycnID_Candidato]
	@cnID_Candidato bigint
AS
	SET NOCOUNT ON

	SELECT *  FROM tbPostulante
	WHERE cnID_Candidato = @cnID_Candidato



GO
/****** Object:  StoredProcedure [dbo].[tbPostulante_GetBycnID_Postulante]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbPostulante_GetBycnID_Postulante
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbPostulante table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulante_GetBycnID_Postulante]
	(
	@cnID_Postulante bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbPostulante WHERE [cnID_Postulante]=@cnID_Postulante




GO
/****** Object:  StoredProcedure [dbo].[tbPostulante_GetConsultaPostulantes]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[tbPostulante_GetConsultaPostulantes]
	@cnID_Proceso BIGINT  = NULL,
	@ctEmail VARCHAR(100) = NULL,
	@ctNumeroDNI VARCHAR(16) = NULL,
	@cnTipoResultado INT  = NULL,
	@cnCobertura INT = NULL,
	@cnZona INT = NULL
AS
BEGIN
	DECLARE @cnEvalAuto BIT
	if @cnTipoResultado is not null
		SET @cnEvalAuto  = (CASE WHEN @cnTipoResultado = 1635 THEN 1 ELSE 0 END)
	/*
	cnID_Lista	cnID_GrupoLista	ctElemento
	1635		25				Aprobado
	1636		25				Desaprobado
	*/
	 
	SELECT 
		tbPostulante.*,
		tbCandidato.ctApellidoPaterno,
		tbCandidato.ctApellidoMaterno,
		tbCandidato.ctNombres ,
		tbCandidato.ctEmail,
		tbCandidato.ctTelefonoCasa,
		tbCandidato.ctTelefonoMovil,
		tbDocIdentidad.ctNumero,
		tbProcesoCobertura.ctNombre AS [ctCobertura],
		tbCoberturaZona.ctZona
	FROM 
		tbPostulante 
			INNER JOIN tbCandidato 
				ON tbPostulante.cnID_Candidato = tbCandidato.cnID_Candidato 
			LEFT JOIN tbDocIdentidad 
				ON tbCandidato.cnID_Candidato = tbDocIdentidad.cnID_Candidato 
					AND tbDocIdentidad.cnID_TipoDocumento  = 237 
			LEFT JOIN tbPostulanteCobe 
				ON tbPostulanteCobe.cnID_Postulante = tbPostulante.cnID_Postulante 
			LEFT JOIN tbProcesoCobertura
				ON tbProcesoCobertura.cnID_Cobertura = tbPostulanteCobe.cnID_Cobertura
					AND tbProcesoCobertura.cnID_Proceso = tbPostulante.cnID_Proceso
			LEFT JOIN tbCoberturaZona
				ON tbCoberturaZona.cnID_CoberturaZona = tbPostulanteCobe.cnID_CoberturaZona
					AND tbCoberturaZona.cnID_Cobertura = tbPostulanteCobe.cnID_Cobertura
	WHERE
		tbPostulante.cnID_Proceso = @cnID_Proceso
		AND (@ctEmail IS NULL OR tbCandidato.ctEmail = @ctEmail)
		AND (@ctNumeroDNI IS NULL OR tbDocIdentidad.ctNumero  = @ctNumeroDNI)
		AND (@cnEvalAuto IS NULL OR tbPostulante.cnEvalAuto = @cnEvalAuto )
		AND (@cnCobertura IS NULL OR tbPostulanteCobe.cnID_Cobertura = @cnCobertura)
		AND (@cnZona IS NULL OR tbCoberturaZona.cnID_CoberturaZona = @cnZona)
		AND tbPostulante.cnEstadoReg = 1
END






GO
/****** Object:  StoredProcedure [dbo].[tbPostulante_GetEvaluacionManual]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tbPostulante_GetEvaluacionManual]
	@cnID_Postulante bigint
AS
SELECT 
	(CASE cnID_TipoEval WHEN 1 THEN 'INFORMACIÓN ACADÉMICA' WHEN 2 THEN 'INFORMACIÓN LABORAL' ELSE 'DOCUMENTOS' END) AS cnTipo,
	(CASE WHEN cnEvaluacion = 1 THEN 'SI' ELSE 'NO' END ) AS cnResultado,
	ctObservacion
FROM
	tbPostulanteEvalManual
WHERE
	cnID_Postulante = @cnID_Postulante


GO
/****** Object:  StoredProcedure [dbo].[tbPostulante_GetHojaVidaAcademico]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[tbPostulante_GetHojaVidaAcademico] @cnID_Postulante = 63
CREATE PROCEDURE [dbo].[tbPostulante_GetHojaVidaAcademico]
	@cnID_Postulante bigint
AS
DECLARE @cnID_Candidato bigint
DECLARE @cnID_Proceso bigint
DECLARE @cnID_GradoMaxPostulante bigint
DECLARE @cnCumpleGradoProceso bit

SELECT  @cnID_Candidato = cnID_Candidato, @cnID_Proceso = cnID_Proceso FROM tbPostulante WHERE cnID_Postulante = @cnID_Postulante


SELECT @cnID_GradoMaxPostulante = cnID_Grado FROM tbPostulanteAcad WHERE cnID_Postulante = @cnID_Postulante



--SELECT @cnID_GradoMaxPostulante --252
SELECT 
	@cnCumpleGradoProceso = (CASE WHEN tbProcesoCnfgAcad.cnID_CnfgAcademica IS NULL THEN 0 ELSE 1 END)
FROM 
	tbProcesoCnfgAcad INNER JOIN
	tbMaestroListas ON tbMaestroListas.cnID_GrupoLista = 5 AND tbMaestroListas.cnID_Lista = tbProcesoCnfgAcad.cnID_Grado
	AND tbProcesoCnfgAcad.cnID_Grado = @cnID_GradoMaxPostulante
WHERE
	tbProcesoCnfgAcad.cnID_Proceso = @cnID_Proceso


IF NOT EXISTS(SELECT * FROM tbProcesoCnfgAcad WHERE cnID_Grado = @cnID_GradoMaxPostulante AND cnID_Proceso = @cnID_Proceso)
BEGIN
	
	DECLARE @ctOrdenGradoPostulante bigint
	DECLARE @ctOrdenGradoProceso bigint
	SELECT TOP 1 @ctOrdenGradoProceso = cnOrden FROM tbProcesoCnfgAcad INNER JOIN tbMaestroListas ON tbMaestroListas.cnID_Lista = tbProcesoCnfgAcad.cnID_Grado AND tbMaestroListas.cnID_GrupoLista = 5 ORDER BY cnOrden DESC
	SELECT TOP 1 @ctOrdenGradoPostulante = cnOrden FROM tbPostulanteAcad INNER JOIN tbMaestroListas ON tbMaestroListas.cnID_Lista = @cnID_GradoMaxPostulante AND tbMaestroListas.cnID_GrupoLista = 5 ORDER BY cnOrden DESC

	IF @ctOrdenGradoPostulante > @ctOrdenGradoProceso
		SET @cnCumpleGradoProceso = 1

END

SELECT 
	tbMaestroListas.ctElemento AS ctDescripcion,	
	CASE WHEN @cnCumpleGradoProceso = 1 THEN 'Cumple' ELSE 'No cumple' END AS ctResultado
FROM	
	tbMaestroListas 
WHERE
	tbMaestroListas.cnID_Lista = @cnID_GradoMaxPostulante 
	AND tbMaestroListas.cnID_GrupoLista = 5 






GO
/****** Object:  StoredProcedure [dbo].[tbPostulante_GetHojaVidaAdicional]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tbPostulante_GetHojaVidaAdicional]
	@cnID_Postulante bigint
AS
DECLARE @cnID_Proceso bigint

SELECT @cnID_Proceso = cnID_Proceso FROM tbPostulante WHERE cnID_Postulante = @cnID_Postulante

SELECT 
	tbProcesoCriterioAdic.ctDescripcion,
	(CASE WHEN tbPostulanteCrit.cnID_PostulanteCrit IS NULL THEN  'No Cumple' ELSE 'Cumple' END) AS ctResultado
FROM 
	tbProcesoCriterioAdic LEFT JOIN
	tbPostulanteCrit  ON tbPostulanteCrit.cnID_CriterioAdicional = tbProcesoCriterioAdic.cnID_CriterioAdicional AND tbPostulanteCrit.cnID_Postulante = @cnID_Postulante
WHERE
	tbProcesoCriterioAdic.cnID_Proceso = @cnID_Proceso


GO
/****** Object:  StoredProcedure [dbo].[tbPostulante_GetHojaVidaCobertura]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tbPostulante_GetHojaVidaCobertura]
	@cnID_Postulante bigint
AS
SELECT 
	tbProcesoCobertura.ctNombre,
	tbCoberturaZona.ctZona  AS ctCobertura	
FROM 
	tbPostulanteCobe INNER JOIN
	tbProcesoCobertura ON tbPostulanteCobe.cnID_Cobertura = tbProcesoCobertura.cnID_Cobertura  LEFT JOIN
	tbPostulante ON tbPostulante.cnID_Postulante = tbPostulanteCobe.cnID_Postulante INNER JOIN
	tbCoberturaZona ON tbCoberturaZona.cnID_CoberturaZona = tbPostulanteCobe.cnID_CoberturaZona
WHERE
	tbPostulanteCobe.cnID_Postulante = @cnID_Postulante
	AND tbPostulante.cnEstadoReg = 1



GO
/****** Object:  StoredProcedure [dbo].[tbPostulante_GetHojaVidaDatosPersonales]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[tbPostulante_GetHojaVidaDatosPersonales]
	@cnID_Postulante bigint
AS
SELECT 
	tbCandidato.ctNombres,
	tbCandidato.ctApellidoPaterno,
	tbCandidato.ctApellidoMaterno	
FROM 
	tbPostulante INNER JOIN
	tbCandidato ON tbPostulante.cnID_Candidato = tbCandidato.cnID_Candidato
WHERE 
	tbPostulante.cnID_Postulante = @cnID_Postulante


GO
/****** Object:  StoredProcedure [dbo].[tbPostulante_GetHojaVidaLaboral]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--tbPostulante_GetHojaVidaLaboral @cnID_Postulante = 70
CREATE PROCEDURE [dbo].[tbPostulante_GetHojaVidaLaboral]
	@cnID_Postulante bigint
AS
DECLARE @cnID_Candidato bigint
DECLARE @cnID_Proceso bigint

SELECT  @cnID_Candidato = cnID_Candidato, @cnID_Proceso = cnID_Proceso FROM tbPostulante WHERE cnID_Postulante = @cnID_Postulante

SELECT 
	tbProcesoCnfgLabo.ctDescripcion,
	LTRIM(RTRIM(STR(SUM(ISNULL(tbPostulanteLabo.cnTiempo,0)))))  as ctTiempo,
	(CASE WHEN ISNULL(SUM(tbPostulanteLabo.cnPuntosCriterioLaboral),0) = 0 THEN 'No Cumple' ELSE 'Cumple' END ) AS ctResultado
FROM 
	tbProcesoCnfgLabo LEFT JOIN	
	(
		SELECT 
			tbPostulanteLabo.cnID_TipoCriterioLabo,
			tbPostulanteLabo.cnTiempoAnios cnTiempo,
			tbPostulante.cnPuntosCriterioLaboral
		FROM 
			tbPostulanteLabo INNER JOIN
			tbPostulante ON tbPostulante.cnID_Postulante = tbPostulanteLabo.cnID_Postulante 
		WHERE 
			tbPostulante.cnEstadoReg = 1
	)tbPostulanteLabo ON tbProcesoCnfgLabo.cnID_TipoCriterioLabo = tbPostulanteLabo.cnID_TipoCriterioLabo
WHERE 
	tbProcesoCnfgLabo.cnID_Proceso = @cnID_Proceso
	
GROUP BY
	tbProcesoCnfgLabo.ctDescripcion,
	tbProcesoCnfgLabo.cnID_TipoCriterioLabo	






GO
/****** Object:  StoredProcedure [dbo].[tbPostulante_GetHojaVidaLaboralEspecif]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[tbPostulante_GetHojaVidaLaboralEspecif] @cnID_Postulante = 45
CREATE PROCEDURE [dbo].[tbPostulante_GetHojaVidaLaboralEspecif]
	@cnID_Postulante bigint
AS
DECLARE @cnID_Candidato bigint
DECLARE @cnID_Proceso bigint


SELECT
		tbExperiencia.ctNombreEmpresa,
		tbCargo.ctElemento ctCargo,
		tbExperiencia.cnSubordinados,
		tbExperiencia.ctDescripcionCargo,
		tbExperiencia.cfFechaInicio,
		tbExperiencia.cfFechaFin,
		tbExperiencia.cnTiempoAnios,
		tbExperiencia.cnTiempoMeses,
		LTRIM(RTRIM(STR(tbExperiencia.cnTiempoAnios))) +  '.' + LTRIM(RTRIM(STR(tbExperiencia.cnTiempoMeses))) AS cnTiempos
	FROM
		tbExperiencia LEFT JOIN
		tbMaestroListas tbCargo ON tbCargo.cnID_Lista = tbExperiencia.cnID_Cargo AND tbCargo.cnID_GrupoLista = 2 INNER JOIN
		tbPostulanteLabo ON tbPostulanteLabo.cnID_Experiencia = tbExperiencia.cnID_Experiencia  LEFT JOIN
		tbPostulante ON tbPostulante.cnID_Postulante = tbPostulanteLabo.cnID_Postulante
	WHERE
		tbPostulanteLabo.cnID_Postulante = @cnID_Postulante
		AND tbPostulanteLabo.cnID_TipoCriterioLabo = 236
		AND tbPostulante.cnEstadoReg = 1




GO
/****** Object:  StoredProcedure [dbo].[tbPostulante_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulante_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for inserting values to tbPostulante table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulante_Insert]
(
	
	@cnID_Proceso bigint,
	@cnID_Candidato bigint,
	@cnPuntosCriterioAcademico int,
	@cnPuntosCriterioLaboral int,
	@cnPuntosCriterioAdicional int,
	@cnEvalAuto bit = NULL,
	@cnEvalManual bit,
	@cnEstadoReg tinyint = NULL,
	@cfFechaCreacion datetime,
	@cfFechaActualizacion datetime = NULL,
	@ctIPAcceso varchar(15)
)
AS
	SET NOCOUNT ON

	INSERT INTO tbPostulante
			([cnID_Proceso] ,[cnID_Candidato] ,[cnPuntosCriterioAcademico] ,[cnPuntosCriterioLaboral] ,[cnPuntosCriterioAdicional] ,[cnEvalAuto] ,[cnEvalManual] ,[cnEstadoReg] ,[cfFechaCreacion] ,[cfFechaActualizacion] ,[ctIPAcceso] )
	VALUES	(@cnID_Proceso ,@cnID_Candidato ,@cnPuntosCriterioAcademico ,@cnPuntosCriterioLaboral ,@cnPuntosCriterioAdicional ,@cnEvalAuto ,@cnEvalManual ,@cnEstadoReg ,@cfFechaCreacion ,@cfFechaActualizacion ,@ctIPAcceso )
    

	SELECT  Scope_Identity() AS [cnID_Postulante];




GO
/****** Object:  StoredProcedure [dbo].[tbPostulante_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbPostulante_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for updating 	tbPostulante table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulante_Update]
(
	
	@cnID_Postulante bigint,
	@cnID_Proceso bigint,
	@cnID_Candidato bigint,
	@cnPuntosCriterioAcademico int,
	@cnPuntosCriterioLaboral int,
	@cnPuntosCriterioAdicional int,
	@cnEvalAuto bit = NULL,
	@cnEvalManual bit,
	@cnEstadoReg tinyint = NULL,
	@cfFechaCreacion datetime,
	@cfFechaActualizacion datetime = NULL,
	@ctIPAcceso varchar(15)
)
AS
	SET NOCOUNT ON

	UPDATE tbPostulante SET 

		[cnID_Proceso] = @cnID_Proceso ,
		[cnID_Candidato] = @cnID_Candidato ,
		[cnPuntosCriterioAcademico] = @cnPuntosCriterioAcademico ,
		[cnPuntosCriterioLaboral] = @cnPuntosCriterioLaboral ,
		[cnPuntosCriterioAdicional] = @cnPuntosCriterioAdicional ,
		[cnEvalAuto] = @cnEvalAuto ,
		[cnEvalManual] = @cnEvalManual ,
		[cnEstadoReg] = @cnEstadoReg ,
		[cfFechaCreacion] = @cfFechaCreacion ,
		[cfFechaActualizacion] = @cfFechaActualizacion ,
		[ctIPAcceso] = @ctIPAcceso 
	WHERE [cnID_Postulante]=@cnID_Postulante




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteAcad_DeleteBycnID_PostulanteAcad]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulanteAcad_DeleteBycnID_PostulanteAcad
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbPostulanteAcad table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteAcad_DeleteBycnID_PostulanteAcad]
	(
	@cnID_PostulanteAcad bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbPostulanteAcad WHERE [cnID_PostulanteAcad]=@cnID_PostulanteAcad




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteAcad_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulanteAcad_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbPostulanteAcad table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteAcad_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbPostulanteAcad




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteAcad_GetBycnID_Postulante]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulanteAcad_DeleteBycnID_PostulanteAcad
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbPostulanteAcad table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteAcad_GetBycnID_Postulante]
	(
	@cnID_Postulante bigint
	)
AS
	SET NOCOUNT ON

	select * FROM tbPostulanteAcad WHERE [cnID_Postulante]=@cnID_Postulante




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteAcad_GetBycnID_PostulanteAcad]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbPostulanteAcad_GetBycnID_PostulanteAcad
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbPostulanteAcad table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteAcad_GetBycnID_PostulanteAcad]
	(
	@cnID_PostulanteAcad bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbPostulanteAcad WHERE [cnID_PostulanteAcad]=@cnID_PostulanteAcad




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteAcad_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulanteAcad_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for inserting values to tbPostulanteAcad table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteAcad_Insert]
(
	
	@cnID_Postulante bigint,
	@cnID_Academico bigint,
	@cnID_Grado bigint,
	@cnID_Situacion bigint
)
AS
	SET NOCOUNT ON

	INSERT INTO tbPostulanteAcad
			([cnID_Postulante] ,[cnID_Academico] ,[cnID_Grado] ,[cnID_Situacion] )
	VALUES	(@cnID_Postulante ,@cnID_Academico ,@cnID_Grado ,@cnID_Situacion )
    

	SELECT  Scope_Identity() AS [cnID_PostulanteAcad];




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteAcad_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbPostulanteAcad_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for updating 	tbPostulanteAcad table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteAcad_Update]
(
	
	@cnID_PostulanteAcad bigint,
	@cnID_Postulante bigint,
	@cnID_Academico bigint,
	@cnID_Grado bigint,
	@cnID_Situacion bigint
)
AS
	SET NOCOUNT ON

	UPDATE tbPostulanteAcad SET 

		[cnID_Postulante] = @cnID_Postulante ,
		[cnID_Academico] = @cnID_Academico ,
		[cnID_Grado] = @cnID_Grado ,
		[cnID_Situacion] = @cnID_Situacion 
	WHERE [cnID_PostulanteAcad]=@cnID_PostulanteAcad




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteCobe_DeleteBycnID_PostulanteCobe]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulanteCobe_DeleteBycnID_PostulanteCobe
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbPostulanteCobe table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteCobe_DeleteBycnID_PostulanteCobe]
	(
	@cnID_PostulanteCobe bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbPostulanteCobe WHERE [cnID_PostulanteCobe]=@cnID_PostulanteCobe




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteCobe_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulanteCobe_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbPostulanteCobe table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteCobe_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbPostulanteCobe




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteCobe_GetBycnID_Postulante]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbPostulanteCobe_GetBycnID_Postulante
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	22/04/2015 19:45:04
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbPostulanteCobe table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteCobe_GetBycnID_Postulante]
	(
	@cnID_Postulante bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbPostulanteCobe WHERE [cnID_Postulante]=@cnID_Postulante



GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteCobe_GetBycnID_PostulanteCobe]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbPostulanteCobe_GetBycnID_PostulanteCobe
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbPostulanteCobe table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteCobe_GetBycnID_PostulanteCobe]
	(
	@cnID_PostulanteCobe bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbPostulanteCobe WHERE [cnID_PostulanteCobe]=@cnID_PostulanteCobe




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteCobe_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulanteCobe_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for inserting values to tbPostulanteCobe table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteCobe_Insert]
(
	
	@cnID_Postulante bigint,
	@cnID_Cobertura bigint,
	@cnID_CoberturaZona bigint
)
AS
	SET NOCOUNT ON

	INSERT INTO tbPostulanteCobe
			([cnID_Postulante] ,[cnID_Cobertura] ,[cnID_CoberturaZona] )
	VALUES	(@cnID_Postulante ,@cnID_Cobertura ,@cnID_CoberturaZona )
    

	SELECT  Scope_Identity() AS [cnID_PostulanteCobe];




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteCobe_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbPostulanteCobe_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for updating 	tbPostulanteCobe table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteCobe_Update]
(
	
	@cnID_PostulanteCobe bigint,
	@cnID_Postulante bigint,
	@cnID_Cobertura bigint,
	@cnID_CoberturaZona bigint
)
AS
	SET NOCOUNT ON

	UPDATE tbPostulanteCobe SET 

		[cnID_Postulante] = @cnID_Postulante ,
		[cnID_Cobertura] = @cnID_Cobertura ,
		[cnID_CoberturaZona] = @cnID_CoberturaZona 
	WHERE [cnID_PostulanteCobe]=@cnID_PostulanteCobe




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteCrit_DeleteBycnID_PostulanteCrit]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulanteCrit_DeleteBycnID_PostulanteCrit
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbPostulanteCrit table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteCrit_DeleteBycnID_PostulanteCrit]
	(
	@cnID_PostulanteCrit bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbPostulanteCrit WHERE [cnID_PostulanteCrit]=@cnID_PostulanteCrit




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteCrit_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulanteCrit_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbPostulanteCrit table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteCrit_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbPostulanteCrit




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteCrit_GetBycnID_PostulanteCrit]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbPostulanteCrit_GetBycnID_PostulanteCrit
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbPostulanteCrit table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteCrit_GetBycnID_PostulanteCrit]
	(
	@cnID_PostulanteCrit bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbPostulanteCrit WHERE [cnID_PostulanteCrit]=@cnID_PostulanteCrit




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteCrit_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulanteCrit_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for inserting values to tbPostulanteCrit table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteCrit_Insert]
(
	
	@cnID_Postulante bigint,
	@cnID_CriterioAdicional int
)
AS
	SET NOCOUNT ON

	INSERT INTO tbPostulanteCrit
			([cnID_Postulante] ,[cnID_CriterioAdicional] )
	VALUES	(@cnID_Postulante ,@cnID_CriterioAdicional )
    

	SELECT  Scope_Identity() AS [cnID_PostulanteCrit];




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteCrit_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbPostulanteCrit_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for updating 	tbPostulanteCrit table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteCrit_Update]
(
	
	@cnID_PostulanteCrit bigint,
	@cnID_Postulante bigint,
	@cnID_CriterioAdicional int
)
AS
	SET NOCOUNT ON

	UPDATE tbPostulanteCrit SET 

		[cnID_Postulante] = @cnID_Postulante ,
		[cnID_CriterioAdicional] = @cnID_CriterioAdicional 
	WHERE [cnID_PostulanteCrit]=@cnID_PostulanteCrit




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteDocu_DeleteBycnID_PostulanteDocu]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulanteDocu_DeleteBycnID_PostulanteDocu
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbPostulanteDocu table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteDocu_DeleteBycnID_PostulanteDocu]
	(
	@cnID_PostulanteDocu bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbPostulanteDocu WHERE [cnID_PostulanteDocu]=@cnID_PostulanteDocu




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteDocu_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulanteDocu_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbPostulanteDocu table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteDocu_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbPostulanteDocu




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteDocu_GetBycnID_Postulante]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulanteDocu_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	22/04/2015 19:45:04
-- Description:	This stored procedure is intended for selecting all rows from tbPostulanteDocu table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteDocu_GetBycnID_Postulante]
	@cnID_Postulante bigint
AS
	SET NOCOUNT ON

	SELECT *  FROM tbPostulanteDocu
	WHERE cnID_Postulante = @cnID_Postulante


GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteDocu_GetBycnID_PostulanteDocu]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbPostulanteDocu_GetBycnID_PostulanteDocu
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbPostulanteDocu table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteDocu_GetBycnID_PostulanteDocu]
	(
	@cnID_PostulanteDocu bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbPostulanteDocu WHERE [cnID_PostulanteDocu]=@cnID_PostulanteDocu




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteDocu_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulanteDocu_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for inserting values to tbPostulanteDocu table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteDocu_Insert]
(
	
	@cnID_Postulante bigint,
	@cnID_Documento bigint,
	@ctRutaArchivo varchar(150) = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO tbPostulanteDocu
			([cnID_Postulante] ,[cnID_Documento] ,[ctRutaArchivo] )
	VALUES	(@cnID_Postulante ,@cnID_Documento ,@ctRutaArchivo )
    

	SELECT  Scope_Identity() AS [cnID_PostulanteDocu];




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteDocu_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbPostulanteDocu_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for updating 	tbPostulanteDocu table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteDocu_Update]
(
	
	@cnID_PostulanteDocu bigint,
	@cnID_Postulante bigint,
	@cnID_Documento bigint,
	@ctRutaArchivo varchar(150) = NULL
)
AS
	SET NOCOUNT ON

	UPDATE tbPostulanteDocu SET 

		[cnID_Postulante] = @cnID_Postulante ,
		[cnID_Documento] = @cnID_Documento ,
		[ctRutaArchivo] = @ctRutaArchivo 
	WHERE [cnID_PostulanteDocu]=@cnID_PostulanteDocu




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteEvalManual_DeleteBycnPostulanteEvalManual]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulanteEvalManual_DeleteBycnPostulanteEvalManual
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	28/06/2015 19:17:54
-- Description:	This stored procedure is intended for deleting a specific row from tbPostulanteEvalManual table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteEvalManual_DeleteBycnPostulanteEvalManual]
	(
	@cnPostulanteEvalManual bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbPostulanteEvalManual WHERE [cnPostulanteEvalManual]=@cnPostulanteEvalManual



GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteEvalManual_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulanteEvalManual_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	28/06/2015 19:17:54
-- Description:	This stored procedure is intended for selecting all rows from tbPostulanteEvalManual table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteEvalManual_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbPostulanteEvalManual



GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteEvalManual_GetBycnID_Postulante]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulanteEvalManual_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	28/06/2015 19:17:54
-- Description:	This stored procedure is intended for selecting all rows from tbPostulanteEvalManual table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteEvalManual_GetBycnID_Postulante]
	@cnID_Postulante bigint
AS
	SET NOCOUNT ON

	SELECT *  FROM tbPostulanteEvalManual
	where cnID_Postulante = @cnID_Postulante


GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteEvalManual_GetBycnPostulanteEvalManual]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbPostulanteEvalManual_GetBycnPostulanteEvalManual
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	28/06/2015 19:17:54
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbPostulanteEvalManual table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteEvalManual_GetBycnPostulanteEvalManual]
	(
	@cnPostulanteEvalManual bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbPostulanteEvalManual WHERE [cnPostulanteEvalManual]=@cnPostulanteEvalManual



GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteEvalManual_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulanteEvalManual_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	28/06/2015 19:17:54
-- Description:	This stored procedure is intended for inserting values to tbPostulanteEvalManual table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteEvalManual_Insert]
(
	
	@cnID_Postulante bigint,
	@cnID_TipoEval bigint = null,
	@cnEvaluacion bit,
	@ctObservacion varchar(800) = NULL,
	@ctUsuarioEval char(15),
	@cfFechaEval datetime = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO tbPostulanteEvalManual
			([cnID_Postulante] ,[cnID_TipoEval] ,[cnEvaluacion] ,[ctObservacion] ,[ctUsuarioEval] ,[cfFechaEval] )
	VALUES	(@cnID_Postulante ,@cnID_TipoEval ,@cnEvaluacion ,@ctObservacion ,@ctUsuarioEval ,GETDATE() )
    

	SELECT  Scope_Identity() AS [cnPostulanteEvalManual];



GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteEvalManual_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbPostulanteEvalManual_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	28/06/2015 19:17:54
-- Description:	This stored procedure is intended for updating 	tbPostulanteEvalManual table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteEvalManual_Update]
(
	
	@cnPostulanteEvalManual bigint,
	@cnID_Postulante bigint,
	@cnID_TipoEval bigint = null,
	@cnEvaluacion bit,
	@ctObservacion varchar(800) = NULL,
	@ctUsuarioEval char(15),
	@cfFechaEval datetime = NULL
)
AS
	SET NOCOUNT ON

	UPDATE tbPostulanteEvalManual SET 

		[cnID_Postulante] = @cnID_Postulante ,
		[cnID_TipoEval] = @cnID_TipoEval ,
		[cnEvaluacion] = @cnEvaluacion ,
		[ctObservacion] = @ctObservacion ,
		[ctUsuarioEval] = @ctUsuarioEval ,
		[cfFechaEval] = GEtDATE() 
	WHERE [cnPostulanteEvalManual]=@cnPostulanteEvalManual



GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteLabo_DeleteBycnID_PostulanteLabo]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulanteLabo_DeleteBycnID_PostulanteLabo
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbPostulanteLabo table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteLabo_DeleteBycnID_PostulanteLabo]
	(
	@cnID_PostulanteLabo bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbPostulanteLabo WHERE [cnID_PostulanteLabo]=@cnID_PostulanteLabo




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteLabo_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulanteLabo_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbPostulanteLabo table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteLabo_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbPostulanteLabo




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteLabo_GetBycnID_Postulante]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulanteLabo_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbPostulanteLabo table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteLabo_GetBycnID_Postulante]
	@cnID_Postulante bigint
AS
	SET NOCOUNT ON

	SELECT *  FROM tbPostulanteLabo
	WHERE cnID_Postulante = @cnID_Postulante



GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteLabo_GetBycnID_PostulanteLabo]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbPostulanteLabo_GetBycnID_PostulanteLabo
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbPostulanteLabo table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteLabo_GetBycnID_PostulanteLabo]
	(
	@cnID_PostulanteLabo bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbPostulanteLabo WHERE [cnID_PostulanteLabo]=@cnID_PostulanteLabo




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteLabo_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbPostulanteLabo_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for inserting values to tbPostulanteLabo table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteLabo_Insert]
(
	
	@cnID_Postulante bigint,
	@cnID_Experiencia bigint = NULL,
	@cnID_TipoCriterioLabo int,
	@cnTiempoAnios tinyint,
	@cnTiempoMeses tinyint = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO tbPostulanteLabo
			([cnID_Postulante] ,[cnID_Experiencia] ,[cnID_TipoCriterioLabo] ,[cnTiempoAnios] ,[cnTiempoMeses] )
	VALUES	(@cnID_Postulante ,@cnID_Experiencia ,@cnID_TipoCriterioLabo ,@cnTiempoAnios ,@cnTiempoMeses )
    

	SELECT  Scope_Identity() AS [cnID_PostulanteLabo];




GO
/****** Object:  StoredProcedure [dbo].[tbPostulanteLabo_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbPostulanteLabo_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for updating 	tbPostulanteLabo table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbPostulanteLabo_Update]
(
	
	@cnID_PostulanteLabo bigint,
	@cnID_Postulante bigint,
	@cnID_Experiencia bigint = NULL,
	@cnID_TipoCriterioLabo int,
	@cnTiempoAnios tinyint,
	@cnTiempoMeses tinyint = NULL
)
AS
	SET NOCOUNT ON

	UPDATE tbPostulanteLabo SET 

		[cnID_Postulante] = @cnID_Postulante ,
		[cnID_Experiencia] = @cnID_Experiencia ,
		[cnID_TipoCriterioLabo] = @cnID_TipoCriterioLabo ,
		[cnTiempoAnios] = @cnTiempoAnios ,
		[cnTiempoMeses] = @cnTiempoMeses 
	WHERE [cnID_PostulanteLabo]=@cnID_PostulanteLabo




GO
/****** Object:  StoredProcedure [dbo].[tbProceso_DeleteBycnID_Proceso]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProceso_DeleteBycnID_Proceso
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbProceso table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProceso_DeleteBycnID_Proceso]
	(
	@cnID_Proceso bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbProceso WHERE [cnID_Proceso]=@cnID_Proceso




GO
/****** Object:  StoredProcedure [dbo].[tbProceso_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProceso_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbProceso table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProceso_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProceso




GO
/****** Object:  StoredProcedure [dbo].[tbProceso_GetBycfFechaFin]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbProceso_GetBycfFechaFin
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbProceso table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProceso_GetBycfFechaFin]
	(
	@cfFechaFin datetime
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProceso WHERE [cfFechaFin]=@cfFechaFin




GO
/****** Object:  StoredProcedure [dbo].[tbProceso_GetBycfFechaInicio]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbProceso_GetBycfFechaInicio
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbProceso table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProceso_GetBycfFechaInicio]
	(
	@cfFechaInicio datetime
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProceso WHERE [cfFechaInicio]=@cfFechaInicio




GO
/****** Object:  StoredProcedure [dbo].[tbProceso_GetBycnID_Proceso]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbProceso_GetBycnID_Proceso
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbProceso table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProceso_GetBycnID_Proceso]
	(
	@cnID_Proceso bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProceso WHERE [cnID_Proceso]=@cnID_Proceso




GO
/****** Object:  StoredProcedure [dbo].[tbProceso_GetProcesosVigentes]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProceso_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	22/04/2015 19:45:04
-- Description:	This stored procedure is intended for selecting all rows from tbProceso table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProceso_GetProcesosVigentes]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProceso
	WHERE cnID_Estado = 1632
	AND GETDATE() >= cfFechaInicio
	AND GETDATE() < cfFechaFin






GO
/****** Object:  StoredProcedure [dbo].[tbProceso_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProceso_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for inserting values to tbProceso table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProceso_Insert]
(
	
	@ctTitulo varchar(150),
	@ctDescripcion varchar(250),
	@cfFechaInicio datetime,
	@cfFechaFin datetime,
	@cnID_Estado bigint,
	@ctArchivoTerminos varchar(200) = NULL,
	@ctArchivoProceso varchar(200) = NULL,
	@cnIndEvaluacion bit,
	@ctUsuarioCreador char(15),
	@cfFechaCreacion datetime,
	@cfFechaActualizacion datetime = NULL,
	@ctIPAcceso varchar(15)
)
AS
	SET NOCOUNT ON

	INSERT INTO tbProceso
			([ctTitulo] ,[ctDescripcion] ,[cfFechaInicio] ,[cfFechaFin] ,[cnID_Estado] ,[ctArchivoTerminos] ,[ctArchivoProceso] ,[cnIndEvaluacion] ,[ctUsuarioCreador] ,[cfFechaCreacion] ,[cfFechaActualizacion] ,[ctIPAcceso] )
	VALUES	(@ctTitulo ,@ctDescripcion ,@cfFechaInicio ,@cfFechaFin ,@cnID_Estado ,@ctArchivoTerminos ,@ctArchivoProceso ,@cnIndEvaluacion ,@ctUsuarioCreador ,@cfFechaCreacion ,@cfFechaActualizacion ,@ctIPAcceso )
    

	SELECT  Scope_Identity() AS [cnID_Proceso];




GO
/****** Object:  StoredProcedure [dbo].[tbProceso_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbProceso_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for updating 	tbProceso table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProceso_Update]
(
	
	@cnID_Proceso bigint,
	@ctTitulo varchar(150),
	@ctDescripcion varchar(250),
	@cfFechaInicio datetime,
	@cfFechaFin datetime,
	@cnID_Estado bigint,
	@ctArchivoTerminos varchar(200) = NULL,
	@ctArchivoProceso varchar(200) = NULL,
	@cnIndEvaluacion bit,
	@ctUsuarioCreador char(15),
	@cfFechaCreacion datetime,
	@cfFechaActualizacion datetime = NULL,
	@ctIPAcceso varchar(15)
)
AS
	SET NOCOUNT ON

	UPDATE tbProceso SET 

		[ctTitulo] = @ctTitulo ,
		[ctDescripcion] = @ctDescripcion ,
		[cfFechaInicio] = @cfFechaInicio ,
		[cfFechaFin] = @cfFechaFin ,
		[cnID_Estado] = @cnID_Estado ,
		[ctArchivoTerminos] = @ctArchivoTerminos ,
		[ctArchivoProceso] = @ctArchivoProceso ,
		[cnIndEvaluacion] = @cnIndEvaluacion ,
		[ctUsuarioCreador] = @ctUsuarioCreador ,
		[cfFechaCreacion] = @cfFechaCreacion ,
		[cfFechaActualizacion] = @cfFechaActualizacion ,
		[ctIPAcceso] = @ctIPAcceso 
	WHERE [cnID_Proceso]=@cnID_Proceso




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCnfgAcad_DeleteBycnID_CnfgAcademica]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoCnfgAcad_DeleteBycnID_CnfgAcademica
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbProcesoCnfgAcad table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCnfgAcad_DeleteBycnID_CnfgAcademica]
	(
	@cnID_CnfgAcademica bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbProcesoCnfgAcad WHERE [cnID_CnfgAcademica]=@cnID_CnfgAcademica




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCnfgAcad_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoCnfgAcad_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbProcesoCnfgAcad table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCnfgAcad_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProcesoCnfgAcad




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCnfgAcad_GetBycnID_CnfgAcademica]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbProcesoCnfgAcad_GetBycnID_CnfgAcademica
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbProcesoCnfgAcad table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCnfgAcad_GetBycnID_CnfgAcademica]
	(
	@cnID_CnfgAcademica bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProcesoCnfgAcad WHERE [cnID_CnfgAcademica]=@cnID_CnfgAcademica




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCnfgAcad_GetBycnID_Proceso]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoCnfgAcad_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	22/04/2015 19:45:04
-- Description:	This stored procedure is intended for selecting all rows from tbProcesoCnfgAcad table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCnfgAcad_GetBycnID_Proceso]
	@cnID_Proceso bigint
AS
	SET NOCOUNT ON

	SELECT 
		tbProcesoCnfgAcad.cnID_CnfgAcademica,
		tbProcesoCnfgAcad.cnID_Proceso,
		tbProcesoCnfgAcad.cnID_Grado,
		tbProcesoCnfgAcad.cnID_Situacion,
		tbProcesoCnfgAcad.cnID_OperadorLogico,
		tbProcesoCnfgAcad.cnGrupoLogico,
		tbProcesoCnfgAcad.cnPuntaje,
		tbGrado.ctElemento + ' : ' + tbSituacion.ctElemento  AS ctDescripcion

	FROM 
		tbProcesoCnfgAcad LEFT JOIN 
		tbMaestroListas tbGrado ON tbGrado.cnID_Lista = tbProcesoCnfgAcad.cnID_Grado AND tbGrado.cnID_GrupoLista = 5 LEFT JOIN
		tbMaestroListas tbSituacion ON tbSituacion.cnID_Lista = tbProcesoCnfgAcad.cnID_Situacion AND tbSituacion.cnID_GrupoLista  IN (12,26,27,28)
	WHERE 
		tbProcesoCnfgAcad.cnID_Proceso = @cnID_Proceso



GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCnfgAcad_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoCnfgAcad_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for inserting values to tbProcesoCnfgAcad table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCnfgAcad_Insert]
(
	
	@cnID_Proceso bigint,
	@cnID_Grado bigint,
	@cnID_Situacion bigint,
	@cnID_OperadorLogico bigint = NULL,
	@cnGrupoLogico smallint = NULL,
	@cnPuntaje int
)
AS
	SET NOCOUNT ON

	INSERT INTO tbProcesoCnfgAcad
			([cnID_Proceso] ,[cnID_Grado] ,[cnID_Situacion] ,[cnID_OperadorLogico] ,[cnGrupoLogico] ,[cnPuntaje] )
	VALUES	(@cnID_Proceso ,@cnID_Grado ,@cnID_Situacion ,@cnID_OperadorLogico ,@cnGrupoLogico ,@cnPuntaje )
    

	SELECT  Scope_Identity() AS [cnID_CnfgAcademica];




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCnfgAcad_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbProcesoCnfgAcad_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for updating 	tbProcesoCnfgAcad table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCnfgAcad_Update]
(
	
	@cnID_CnfgAcademica bigint,
	@cnID_Proceso bigint,
	@cnID_Grado bigint,
	@cnID_Situacion bigint,
	@cnID_OperadorLogico bigint = NULL,
	@cnGrupoLogico smallint = NULL,
	@cnPuntaje int
)
AS
	SET NOCOUNT ON

	UPDATE tbProcesoCnfgAcad SET 

		[cnID_Proceso] = @cnID_Proceso ,
		[cnID_Grado] = @cnID_Grado ,
		[cnID_Situacion] = @cnID_Situacion ,
		[cnID_OperadorLogico] = @cnID_OperadorLogico ,
		[cnGrupoLogico] = @cnGrupoLogico ,
		[cnPuntaje] = @cnPuntaje 
	WHERE [cnID_CnfgAcademica]=@cnID_CnfgAcademica




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCnfgLabo_DeleteBycnID_CnfgLaboral]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoCnfgLabo_DeleteBycnID_CnfgLaboral
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbProcesoCnfgLabo table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCnfgLabo_DeleteBycnID_CnfgLaboral]
	(
	@cnID_CnfgLaboral bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbProcesoCnfgLabo WHERE [cnID_CnfgLaboral]=@cnID_CnfgLaboral




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCnfgLabo_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoCnfgLabo_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbProcesoCnfgLabo table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCnfgLabo_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProcesoCnfgLabo




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCnfgLabo_GetBycnID_CnfgLaboral]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbProcesoCnfgLabo_GetBycnID_CnfgLaboral
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbProcesoCnfgLabo table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCnfgLabo_GetBycnID_CnfgLaboral]
	(
	@cnID_CnfgLaboral bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProcesoCnfgLabo WHERE [cnID_CnfgLaboral]=@cnID_CnfgLaboral




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCnfgLabo_GetBycnID_Proceso]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoCnfgLabo_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	22/04/2015 19:45:04
-- Description:	This stored procedure is intended for selecting all rows from tbProcesoCnfgLabo table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCnfgLabo_GetBycnID_Proceso]
	@cnID_Proceso bigint
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProcesoCnfgLabo
	WHERE cnID_Proceso = @cnID_Proceso



GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCnfgLabo_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoCnfgLabo_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for inserting values to tbProcesoCnfgLabo table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCnfgLabo_Insert]
(
	
	@cnID_Proceso bigint,
	@cnID_TipoEntidad bigint,
	@cnID_TipoCriterioLabo bigint = NULL,
	@cnID_TipoEvaluacion bigint,
	@ctDescripcion varchar(300)
)
AS
	SET NOCOUNT ON

	INSERT INTO tbProcesoCnfgLabo
			([cnID_Proceso] ,[cnID_TipoEntidad] ,[cnID_TipoCriterioLabo] ,[cnID_TipoEvaluacion] ,[ctDescripcion] )
	VALUES	(@cnID_Proceso ,@cnID_TipoEntidad ,@cnID_TipoCriterioLabo ,@cnID_TipoEvaluacion ,@ctDescripcion )
    

	SELECT  Scope_Identity() AS [cnID_CnfgLaboral];




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCnfgLabo_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbProcesoCnfgLabo_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for updating 	tbProcesoCnfgLabo table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCnfgLabo_Update]
(
	
	@cnID_CnfgLaboral bigint,
	@cnID_Proceso bigint,
	@cnID_TipoEntidad bigint,
	@cnID_TipoCriterioLabo bigint = NULL,
	@cnID_TipoEvaluacion bigint,
	@ctDescripcion varchar(300)
)
AS
	SET NOCOUNT ON

	UPDATE tbProcesoCnfgLabo SET 

		[cnID_Proceso] = @cnID_Proceso ,
		[cnID_TipoEntidad] = @cnID_TipoEntidad ,
		[cnID_TipoCriterioLabo] = @cnID_TipoCriterioLabo ,
		[cnID_TipoEvaluacion] = @cnID_TipoEvaluacion ,
		[ctDescripcion] = @ctDescripcion 
	WHERE [cnID_CnfgLaboral]=@cnID_CnfgLaboral




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCobertura_DeleteBycnID_Cobertura]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoCobertura_DeleteBycnID_Cobertura
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbProcesoCobertura table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCobertura_DeleteBycnID_Cobertura]
	(
	@cnID_Cobertura bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbProcesoCobertura WHERE [cnID_Cobertura]=@cnID_Cobertura




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCobertura_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoCobertura_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbProcesoCobertura table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCobertura_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProcesoCobertura




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCobertura_GetBycnID_Cobertura]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbProcesoCobertura_GetBycnID_Cobertura
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbProcesoCobertura table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCobertura_GetBycnID_Cobertura]
	(
	@cnID_Cobertura bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProcesoCobertura WHERE [cnID_Cobertura]=@cnID_Cobertura




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCobertura_GetBycnID_Proceso]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoCobertura_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	22/04/2015 19:45:04
-- Description:	This stored procedure is intended for selecting all rows from tbProcesoCobertura table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCobertura_GetBycnID_Proceso]
	@cnID_Proceso         bigint  = NULL 
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProcesoCobertura
	WHERE cnID_Proceso = @cnID_Proceso



GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCobertura_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoCobertura_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for inserting values to tbProcesoCobertura table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCobertura_Insert]
(
	
	@cnID_Proceso bigint,
	@ctNombre varchar(100)
)
AS
	SET NOCOUNT ON

	INSERT INTO tbProcesoCobertura
			([cnID_Proceso] ,[ctNombre] )
	VALUES	(@cnID_Proceso ,@ctNombre )
    

	SELECT  Scope_Identity() AS [cnID_Cobertura];




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCobertura_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbProcesoCobertura_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for updating 	tbProcesoCobertura table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCobertura_Update]
(
	
	@cnID_Cobertura bigint,
	@cnID_Proceso bigint,
	@ctNombre varchar(100)
)
AS
	SET NOCOUNT ON

	UPDATE tbProcesoCobertura SET 

		[cnID_Proceso] = @cnID_Proceso ,
		[ctNombre] = @ctNombre 
	WHERE [cnID_Cobertura]=@cnID_Cobertura




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCriterioAdic_DeleteBycnID_CriterioAdicional]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoCriterioAdic_DeleteBycnID_CriterioAdicional
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbProcesoCriterioAdic table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCriterioAdic_DeleteBycnID_CriterioAdicional]
	(
	@cnID_CriterioAdicional bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbProcesoCriterioAdic WHERE [cnID_CriterioAdicional]=@cnID_CriterioAdicional




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCriterioAdic_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoCriterioAdic_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbProcesoCriterioAdic table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCriterioAdic_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProcesoCriterioAdic




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCriterioAdic_GetBycnID_CriterioAdicional]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbProcesoCriterioAdic_GetBycnID_CriterioAdicional
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbProcesoCriterioAdic table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCriterioAdic_GetBycnID_CriterioAdicional]
	(
	@cnID_CriterioAdicional bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProcesoCriterioAdic WHERE [cnID_CriterioAdicional]=@cnID_CriterioAdicional




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCriterioAdic_GetBycnID_Proceso]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoCriterioAdic_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	22/04/2015 19:45:04
-- Description:	This stored procedure is intended for selecting all rows from tbProcesoCriterioAdic table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCriterioAdic_GetBycnID_Proceso]
	@cnID_Proceso bigint
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProcesoCriterioAdic
	WHERE cnID_Proceso = @cnID_Proceso



GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCriterioAdic_GetByctDescripcion]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbProcesoCriterioAdic_GetByctDescripcion
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbProcesoCriterioAdic table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCriterioAdic_GetByctDescripcion]
	(
	@ctDescripcion varchar(150)
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProcesoCriterioAdic WHERE [ctDescripcion]=@ctDescripcion




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCriterioAdic_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoCriterioAdic_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for inserting values to tbProcesoCriterioAdic table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCriterioAdic_Insert]
(
	
	@cnID_Proceso bigint,
	@ctDescripcion varchar(150) = NULL,
	@cnPuntaje int
)
AS
	SET NOCOUNT ON

	INSERT INTO tbProcesoCriterioAdic
			([cnID_Proceso] ,[ctDescripcion] ,[cnPuntaje] )
	VALUES	(@cnID_Proceso ,@ctDescripcion ,@cnPuntaje )
    

	SELECT  Scope_Identity() AS [cnID_CriterioAdicional];




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoCriterioAdic_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbProcesoCriterioAdic_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for updating 	tbProcesoCriterioAdic table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoCriterioAdic_Update]
(
	
	@cnID_CriterioAdicional bigint,
	@cnID_Proceso bigint,
	@ctDescripcion varchar(150) = NULL,
	@cnPuntaje int
)
AS
	SET NOCOUNT ON

	UPDATE tbProcesoCriterioAdic SET 

		[cnID_Proceso] = @cnID_Proceso ,
		[ctDescripcion] = @ctDescripcion ,
		[cnPuntaje] = @cnPuntaje 
	WHERE [cnID_CriterioAdicional]=@cnID_CriterioAdicional




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoDocumento_DeleteBycnID_Documento]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoDocumento_DeleteBycnID_Documento
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbProcesoDocumento table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoDocumento_DeleteBycnID_Documento]
	(
	@cnID_Documento bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbProcesoDocumento WHERE [cnID_Documento]=@cnID_Documento




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoDocumento_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoDocumento_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbProcesoDocumento table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoDocumento_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProcesoDocumento




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoDocumento_GetBycnID_Documento]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbProcesoDocumento_GetBycnID_Documento
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbProcesoDocumento table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoDocumento_GetBycnID_Documento]
	(
	@cnID_Documento bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProcesoDocumento WHERE [cnID_Documento]=@cnID_Documento




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoDocumento_GetBycnID_Proceso]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoDocumento_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	22/04/2015 19:45:04
-- Description:	This stored procedure is intended for selecting all rows from tbProcesoDocumento table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoDocumento_GetBycnID_Proceso]
	@cnID_Proceso bigint
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProcesoDocumento
	WHERE cnID_Proceso = @cnID_Proceso



GO
/****** Object:  StoredProcedure [dbo].[tbProcesoDocumento_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoDocumento_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for inserting values to tbProcesoDocumento table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoDocumento_Insert]
(
	
	@cnID_Proceso bigint,
	@ctDescripcion varchar(150),
	@cnIndObligatorio bit
)
AS
	SET NOCOUNT ON

	INSERT INTO tbProcesoDocumento
			([cnID_Proceso] ,[ctDescripcion] ,[cnIndObligatorio] )
	VALUES	(@cnID_Proceso ,@ctDescripcion ,@cnIndObligatorio )
    

	SELECT  Scope_Identity() AS [cnID_Documento];




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoDocumento_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbProcesoDocumento_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for updating 	tbProcesoDocumento table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoDocumento_Update]
(
	
	@cnID_Documento bigint,
	@cnID_Proceso bigint,
	@ctDescripcion varchar(150),
	@cnIndObligatorio bit
)
AS
	SET NOCOUNT ON

	UPDATE tbProcesoDocumento SET 

		[cnID_Proceso] = @cnID_Proceso ,
		[ctDescripcion] = @ctDescripcion ,
		[cnIndObligatorio] = @cnIndObligatorio 
	WHERE [cnID_Documento]=@cnID_Documento




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoMatrizLabo_DeleteBycnID_MatrizLaboral]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoMatrizLabo_DeleteBycnID_MatrizLaboral
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbProcesoMatrizLabo table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoMatrizLabo_DeleteBycnID_MatrizLaboral]
	(
	@cnID_MatrizLaboral bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbProcesoMatrizLabo WHERE [cnID_MatrizLaboral]=@cnID_MatrizLaboral




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoMatrizLabo_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoMatrizLabo_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbProcesoMatrizLabo table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoMatrizLabo_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProcesoMatrizLabo




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoMatrizLabo_GetBycnID_CnfgLaboral]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbProcesoMatrizLabo_GetBycnID_MatrizLaboral
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	22/04/2015 19:45:04
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbProcesoMatrizLabo table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoMatrizLabo_GetBycnID_CnfgLaboral]
	(
	@cnID_CnfgLaboral int
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProcesoMatrizLabo WHERE cnID_CnfgLaboral=@cnID_CnfgLaboral




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoMatrizLabo_GetBycnID_MatrizLaboral]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbProcesoMatrizLabo_GetBycnID_MatrizLaboral
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbProcesoMatrizLabo table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoMatrizLabo_GetBycnID_MatrizLaboral]
	(
	@cnID_MatrizLaboral bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProcesoMatrizLabo WHERE [cnID_MatrizLaboral]=@cnID_MatrizLaboral




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoMatrizLabo_GetBycnID_Proceso]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoMatrizLabo_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	22/04/2015 19:45:04
-- Description:	This stored procedure is intended for selecting all rows from tbProcesoMatrizLabo table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoMatrizLabo_GetBycnID_Proceso]
	@cnID_Proceso bigint
AS
	SET NOCOUNT ON

	SELECT tbProcesoMatrizLabo.*  
	FROM 
		tbProcesoMatrizLabo INNER JOIN 
		tbProcesoCnfgLabo ON tbProcesoCnfgLabo.cnID_CnfgLaboral = tbProcesoMatrizLabo.cnID_CnfgLaboral
	WHERE tbProcesoCnfgLabo.cnID_Proceso = @cnID_Proceso



GO
/****** Object:  StoredProcedure [dbo].[tbProcesoMatrizLabo_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProcesoMatrizLabo_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for inserting values to tbProcesoMatrizLabo table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoMatrizLabo_Insert]
(
	
	@cnID_CnfgLaboral bigint,
	@cnMininmo int,
	@cnMaximo int,
	@cnPuntaje int
)
AS
	SET NOCOUNT ON

	INSERT INTO tbProcesoMatrizLabo
			([cnID_CnfgLaboral] ,[cnMininmo] ,[cnMaximo] ,[cnPuntaje] )
	VALUES	(@cnID_CnfgLaboral ,@cnMininmo ,@cnMaximo ,@cnPuntaje )
    

	SELECT  Scope_Identity() AS [cnID_MatrizLaboral];




GO
/****** Object:  StoredProcedure [dbo].[tbProcesoMatrizLabo_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbProcesoMatrizLabo_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for updating 	tbProcesoMatrizLabo table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProcesoMatrizLabo_Update]
(
	
	@cnID_MatrizLaboral bigint,
	@cnID_CnfgLaboral bigint,
	@cnMininmo int,
	@cnMaximo int,
	@cnPuntaje int
)
AS
	SET NOCOUNT ON

	UPDATE tbProcesoMatrizLabo SET 

		[cnID_CnfgLaboral] = @cnID_CnfgLaboral ,
		[cnMininmo] = @cnMininmo ,
		[cnMaximo] = @cnMaximo ,
		[cnPuntaje] = @cnPuntaje 
	WHERE [cnID_MatrizLaboral]=@cnID_MatrizLaboral




GO
/****** Object:  StoredProcedure [dbo].[tbProvincia_DeleteBycnID_Provincia]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProvincia_DeleteBycnID_Provincia
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbProvincia table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProvincia_DeleteBycnID_Provincia]
	(
	@cnID_Provincia int
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbProvincia WHERE [cnID_Provincia]=@cnID_Provincia




GO
/****** Object:  StoredProcedure [dbo].[tbProvincia_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProvincia_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:49 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbProvincia table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProvincia_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProvincia




GO
/****** Object:  StoredProcedure [dbo].[tbProvincia_GetBycnID_Departamento]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbProvincia_GetBycnID_Provincia
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	22/04/2015 19:45:04
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbProvincia table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProvincia_GetBycnID_Departamento]
	(
	@cnID_Departamento int
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProvincia WHERE cnID_Departamento=@cnID_Departamento




GO
/****** Object:  StoredProcedure [dbo].[tbProvincia_GetBycnID_Provincia]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbProvincia_GetBycnID_Provincia
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbProvincia table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProvincia_GetBycnID_Provincia]
	(
	@cnID_Provincia int
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbProvincia WHERE [cnID_Provincia]=@cnID_Provincia




GO
/****** Object:  StoredProcedure [dbo].[tbProvincia_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbProvincia_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for inserting values to tbProvincia table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProvincia_Insert]
(
	
	@cnID_Departamento int,
	@ctProvincia varchar(150) = NULL,
	@ctCodigoISO varchar(4) = NULL
)
AS
	SET NOCOUNT ON

	INSERT INTO tbProvincia
			([cnID_Departamento] ,[ctProvincia] ,[ctCodigoISO] )
	VALUES	(@cnID_Departamento ,@ctProvincia ,@ctCodigoISO )
    

	SELECT  Scope_Identity() AS [cnID_Provincia];




GO
/****** Object:  StoredProcedure [dbo].[tbProvincia_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbProvincia_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for updating 	tbProvincia table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbProvincia_Update]
(
	
	@cnID_Provincia int,
	@cnID_Departamento int,
	@ctProvincia varchar(150) = NULL,
	@ctCodigoISO varchar(4) = NULL
)
AS
	SET NOCOUNT ON

	UPDATE tbProvincia SET 

		[cnID_Departamento] = @cnID_Departamento ,
		[ctProvincia] = @ctProvincia ,
		[ctCodigoISO] = @ctCodigoISO 
	WHERE [cnID_Provincia]=@cnID_Provincia




GO
/****** Object:  StoredProcedure [dbo].[tbReferencia_DeleteBycnID_Referencia]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbReferencia_DeleteBycnID_Referencia
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbReferencia table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbReferencia_DeleteBycnID_Referencia]
	(
	@cnID_Referencia bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbReferencia WHERE [cnID_Referencia]=@cnID_Referencia




GO
/****** Object:  StoredProcedure [dbo].[tbReferencia_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbReferencia_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbReferencia table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbReferencia_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbReferencia




GO
/****** Object:  StoredProcedure [dbo].[tbReferencia_GetBycnID_Candidato]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[tbReferencia_GetBycnID_Candidato]
@cnID_Candidato int
as
begin

SELECT * FROM 
tbReferencia
WHERE cnID_Candidato = @cnID_Candidato
end





GO
/****** Object:  StoredProcedure [dbo].[tbReferencia_GetBycnID_Referencia]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbReferencia_GetBycnID_Referencia
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbReferencia table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbReferencia_GetBycnID_Referencia]
	(
	@cnID_Referencia bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbReferencia WHERE [cnID_Referencia]=@cnID_Referencia




GO
/****** Object:  StoredProcedure [dbo].[tbReferencia_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbReferencia_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for inserting values to tbReferencia table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbReferencia_Insert]
(
	
	@cnID_Candidato bigint,
	@ctNombreCompleto varchar(200),
	@ctNombreEmpresa varchar(200),
	@ctCargo varchar(150),
	@ctTelefono varchar(15),
	@cfFechaCreacion datetime,
	@cfFechaActualizacion datetime = NULL,
	@ctIPAcceso varchar(15)
)
AS
	SET NOCOUNT ON

	INSERT INTO tbReferencia
			([cnID_Candidato] ,[ctNombreCompleto] ,[ctNombreEmpresa] ,[ctCargo] ,[ctTelefono] ,[cfFechaCreacion] ,[cfFechaActualizacion] ,[ctIPAcceso] )
	VALUES	(@cnID_Candidato ,@ctNombreCompleto ,@ctNombreEmpresa ,@ctCargo ,@ctTelefono ,GETDATE() ,NULL,@ctIPAcceso )
    

	SELECT  Scope_Identity() AS [cnID_Referencia];




GO
/****** Object:  StoredProcedure [dbo].[tbReferencia_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbReferencia_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for updating 	tbReferencia table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbReferencia_Update]
(
	
	@cnID_Referencia bigint,
	@cnID_Candidato bigint,
	@ctNombreCompleto varchar(200),
	@ctNombreEmpresa varchar(200),
	@ctCargo varchar(150),
	@ctTelefono varchar(15),
	@cfFechaCreacion datetime,
	@cfFechaActualizacion datetime = NULL,
	@ctIPAcceso varchar(15)
)
AS
	SET NOCOUNT ON

	UPDATE tbReferencia SET 

		[cnID_Candidato] = @cnID_Candidato ,
		[ctNombreCompleto] = @ctNombreCompleto ,
		[ctNombreEmpresa] = @ctNombreEmpresa ,
		[ctCargo] = @ctCargo ,
		[ctTelefono] = @ctTelefono ,
		--[cfFechaCreacion] = @cfFechaCreacion ,
		[cfFechaActualizacion] = GETDATE(),
		[ctIPAcceso] = @ctIPAcceso 
	WHERE [cnID_Referencia]=@cnID_Referencia




GO
/****** Object:  StoredProcedure [dbo].[tbUsuario_DeleteBycnID_Usuario]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbUsuario_DeleteBycnID_Usuario
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbUsuario table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbUsuario_DeleteBycnID_Usuario]
	(
	@cnID_Usuario bigint
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbUsuario WHERE [cnID_Usuario]=@cnID_Usuario




GO
/****** Object:  StoredProcedure [dbo].[tbUsuario_DeleteByctLogin]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbUsuario_DeleteByctLogin
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for deleting a specific row from tbUsuario table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbUsuario_DeleteByctLogin]
	(
	@ctLogin char(15)
	)
AS
	SET NOCOUNT ON

	DELETE FROM tbUsuario WHERE [ctLogin]=@ctLogin




GO
/****** Object:  StoredProcedure [dbo].[tbUsuario_GetALL]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbUsuario_GetALL
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for selecting all rows from tbUsuario table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbUsuario_GetALL]
AS
	SET NOCOUNT ON

	SELECT *  FROM tbUsuario




GO
/****** Object:  StoredProcedure [dbo].[tbUsuario_GetBycnID_Usuario]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbUsuario_GetBycnID_Usuario
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbUsuario table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbUsuario_GetBycnID_Usuario]
	(
	@cnID_Usuario bigint
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbUsuario WHERE [cnID_Usuario]=@cnID_Usuario




GO
/****** Object:  StoredProcedure [dbo].[tbUsuario_GetByctLogin]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbUsuario_GetByctLogin
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for selecting a row by specified primary key from tbUsuario table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbUsuario_GetByctLogin]
	(
	@ctLogin char(15)
	)
AS
	SET NOCOUNT ON

	SELECT *  FROM tbUsuario WHERE [ctLogin]=@ctLogin




GO
/****** Object:  StoredProcedure [dbo].[tbUsuario_Insert]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Entity Name:	tbUsuario_Insert
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for inserting values to tbUsuario table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbUsuario_Insert]
(
	
	@cnID_Usuario bigint,
	@cnID_Perfil int,
	@ctNombres varchar(200),
	@ctApellidos varchar(200),
	@ctLogin char(15),
	@ctClave char(20),
	@cnBloqueado bit,
	@cnEstadoReg bit
)
AS
	SET NOCOUNT ON

	INSERT INTO tbUsuario
			([cnID_Usuario] ,[cnID_Perfil] ,[ctNombres] ,[ctApellidos] ,[ctLogin] ,[ctClave] ,[cnBloqueado] ,[cnEstadoReg] )
	VALUES	(@cnID_Usuario ,@cnID_Perfil ,@ctNombres ,@ctApellidos ,@ctLogin ,@ctClave ,@cnBloqueado ,@cnEstadoReg )
    





GO
/****** Object:  StoredProcedure [dbo].[tbUsuario_Update]    Script Date: 03/07/2015 23:31:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Entity Name:	tbUsuario_Update
-- Generator:	Salar dotNET DbCodeGenerator
-- Create date:	23/06/2015 12:16:50 a.m.
-- Description:	This stored procedure is intended for updating 	tbUsuario table
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tbUsuario_Update]
(
	
	@cnID_Usuario bigint,
	@cnID_Perfil int,
	@ctNombres varchar(200),
	@ctApellidos varchar(200),
	@ctLogin char(15),
	@ctClave char(20),
	@cnBloqueado bit,
	@cnEstadoReg bit
)
AS
	SET NOCOUNT ON

	UPDATE tbUsuario SET 

		[cnID_Usuario] = @cnID_Usuario ,
		[cnID_Perfil] = @cnID_Perfil ,
		[ctNombres] = @ctNombres ,
		[ctApellidos] = @ctApellidos ,
		[ctLogin] = @ctLogin ,
		[ctClave] = @ctClave ,
		[cnBloqueado] = @cnBloqueado ,
		[cnEstadoReg] = @cnEstadoReg 
	WHERE [cnID_Usuario]=@cnID_Usuario




GO
/****** Object:  Table [dbo].[tbAcademico]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbAcademico](
	[cnID_Academico] [bigint] IDENTITY(1,1) NOT NULL,
	[cnID_Candidato] [bigint] NOT NULL,
	[cnID_Grado] [bigint] NOT NULL,
	[cnID_Institucion] [bigint] NOT NULL,
	[ctInstitucionOtro] [varchar](200) NULL,
	[cnID_Carrera] [bigint] NOT NULL,
	[ctCarreraOtro] [varchar](200) NULL,
	[cnID_Situacion] [bigint] NOT NULL,
	[cnID_Pais] [int] NOT NULL,
	[ctCiudad] [varchar](150) NULL,
	[cfFechaInicio] [date] NOT NULL,
	[cfFechaFin] [date] NOT NULL,
	[ctCertificado] [varchar](150) NULL,
	[cfFechaCreacion] [datetime] NOT NULL,
	[cfFechaActualizacion] [datetime] NULL,
	[ctIPAcceso] [varchar](15) NOT NULL,
 CONSTRAINT [Pk_tbAcademico] PRIMARY KEY CLUSTERED 
(
	[cnID_Academico] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbCandidato]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbCandidato](
	[cnID_Candidato] [bigint] IDENTITY(1,1) NOT NULL,
	[ctApellidoPaterno] [varchar](200) NOT NULL,
	[ctApellidoMaterno] [varchar](200) NOT NULL,
	[ctNombres] [varchar](200) NOT NULL,
	[cnID_Sexo] [bigint] NOT NULL,
	[ctEstadoCivil] [bigint] NOT NULL,
	[ctLugarNacimiento] [varchar](100) NOT NULL,
	[cfFechaNacimiento] [date] NOT NULL,
	[cnID_PaisNacimiento] [int] NOT NULL,
	[ctDireccion] [varchar](300) NOT NULL,
	[cnID_Pais] [int] NOT NULL,
	[cnID_Departamento] [int] NOT NULL,
	[cnID_Provincia] [int] NOT NULL,
	[cnID_Distrito] [int] NOT NULL,
	[ctTelefonoCasa] [varchar](15) NULL,
	[ctTelefonoMovil] [varchar](15) NULL,
	[ctTelefonoOtro] [varchar](15) NULL,
	[ctEmail] [char](100) NOT NULL,
	[ctClaveAcceso] [varchar](50) NOT NULL,
	[cnEstadoReg] [tinyint] NOT NULL,
	[cnCodigoRecuperacion] [uniqueidentifier] NULL,
	[cfFechaCreacion] [datetime] NULL,
	[cfFechaActualizacion] [datetime] NULL,
	[ctIPAcceso] [varchar](15) NOT NULL,
 CONSTRAINT [Pk_tbCandidato] PRIMARY KEY CLUSTERED 
(
	[cnID_Candidato] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbCnfgSistema]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbCnfgSistema](
	[cnID_Config] [int] IDENTITY(1,1) NOT NULL,
	[ctDescripcion] [varchar](50) NOT NULL,
	[ctAlias] [varchar](10) NOT NULL,
	[ctValor] [varchar](10) NOT NULL,
 CONSTRAINT [Pk_tbCnfgSistema] PRIMARY KEY CLUSTERED 
(
	[cnID_Config] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbCoberturaZona]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbCoberturaZona](
	[cnID_CoberturaZona] [bigint] IDENTITY(1,1) NOT NULL,
	[cnID_Cobertura] [bigint] NOT NULL,
	[ctZona] [varchar](500) NOT NULL,
 CONSTRAINT [Pk_tbCoberturaZona] PRIMARY KEY CLUSTERED 
(
	[cnID_CoberturaZona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbComplementario]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbComplementario](
	[cnID_Complementario] [bigint] IDENTITY(1,1) NOT NULL,
	[cnID_Candidato] [bigint] NOT NULL,
	[cnID_TipoEstudio] [bigint] NOT NULL,
	[ctDescripcion] [varchar](20) NULL,
	[cnID_Institucion] [bigint] NULL,
	[ctInstitucionOtro] [varchar](200) NULL,
	[cnID_Situacion] [bigint] NULL,
	[cfFechaInicio] [datetime] NOT NULL,
	[cfFechaFin] [datetime] NOT NULL,
	[cnID_Pais] [int] NOT NULL,
	[ctCertificado] [varchar](150) NULL,
	[cfFechaCreacion] [datetime] NOT NULL,
	[cfFechaActualizacion] [datetime] NULL,
	[ctIPAcceso] [varchar](15) NOT NULL,
 CONSTRAINT [Pk_tbEspecializacion] PRIMARY KEY CLUSTERED 
(
	[cnID_Complementario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbDepartamento]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbDepartamento](
	[cnID_Departamento] [int] IDENTITY(1,1) NOT NULL,
	[ctDepartamento] [varchar](150) NULL,
	[ctCodigoISO] [char](2) NULL,
 CONSTRAINT [Pk_tbDepartamento] PRIMARY KEY CLUSTERED 
(
	[cnID_Departamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbDistrito]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbDistrito](
	[cnID_Distrito] [int] IDENTITY(1,1) NOT NULL,
	[cnID_Departamento] [int] NULL,
	[cnID_Provincia] [int] NOT NULL,
	[ctDistrito] [varchar](150) NOT NULL,
	[ctCodigoISO] [varchar](6) NULL,
 CONSTRAINT [Pk_tbDistrito] PRIMARY KEY CLUSTERED 
(
	[cnID_Distrito] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbDocIdentidad]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbDocIdentidad](
	[cnID_DocIdentidad] [bigint] IDENTITY(1,1) NOT NULL,
	[cnID_Candidato] [bigint] NOT NULL,
	[cnID_TipoDocumento] [bigint] NOT NULL,
	[ctNumero] [varchar](16) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbExperiencia]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbExperiencia](
	[cnID_Experiencia] [bigint] IDENTITY(1,1) NOT NULL,
	[cnID_Candidato] [bigint] NOT NULL,
	[cnID_TipoEntidad] [bigint] NOT NULL,
	[ctNombreEmpresa] [varchar](200) NOT NULL,
	[cnID_Rubro] [bigint] NOT NULL,
	[cnID_Cargo] [bigint] NOT NULL,
	[ctCargoOtro] [varchar](50) NULL,
	[cnID_Area] [bigint] NOT NULL,
	[cnID_Pais] [int] NOT NULL,
	[cnID_TipoContrato] [bigint] NOT NULL,
	[cnSubordinados] [smallint] NULL,
	[ctDescripcionCargo] [varchar](500) NOT NULL,
	[cfFechaInicio] [date] NOT NULL,
	[cfFechaFin] [date] NOT NULL,
	[cnTiempoAnios] [tinyint] NULL,
	[cnTiempoMeses] [tinyint] NULL,
	[ctCertificado] [varchar](150) NULL,
	[cfFechaCreacion] [datetime] NOT NULL,
	[cfFechaActualizacion] [datetime] NULL,
	[ctIPAcceso] [varchar](15) NOT NULL,
 CONSTRAINT [Pk_tbExperiencia] PRIMARY KEY CLUSTERED 
(
	[cnID_Experiencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbIdioma]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbIdioma](
	[cnID_Idioma] [bigint] IDENTITY(1,1) NOT NULL,
	[cnID_Candidato] [bigint] NOT NULL,
	[cnID_Lengua] [bigint] NOT NULL,
	[ctLenguaOtro] [varchar](20) NULL,	
	[cnID_NivelLectura] [bigint] NOT NULL,
	[cnID_NivelEscritura] [bigint] NOT NULL,
	[cnID_NivelConversacion] [bigint] NOT NULL,
	[cnID_Institucion] [bigint] NULL,
	[cnID_Situacion] [bigint] NULL,
	[ctCertificado] [varchar](150) NULL,
	[cfFechaCreacion] [datetime] NOT NULL,
	[cfFechaActualizacion] [datetime] NULL,
	[ctIPAcceso] [varchar](15) NOT NULL,
 CONSTRAINT [Pk_tbIdioma] PRIMARY KEY CLUSTERED 
(
	[cnID_Idioma] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbMaestroGrupos]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbMaestroGrupos](
	[cnID_GrupoLista] [int] IDENTITY(1,1) NOT NULL,
	[ctNombreGrupo] [varchar](100) NOT NULL,
	[ctAlias] [varchar](10) NULL,
	[cnEstado] [tinyint] NOT NULL,
 CONSTRAINT [Pk_tbMaestroGrupos] PRIMARY KEY CLUSTERED 
(
	[cnID_GrupoLista] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbMaestroListas]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbMaestroListas](
	[cnID_Lista] [bigint] IDENTITY(1,1) NOT NULL,
	[cnID_GrupoLista] [int] NOT NULL,
	[ctElemento] [varchar](150) NOT NULL,
	[cnOrden] [int] NULL,
	[cnEstado] [tinyint] NOT NULL,
 CONSTRAINT [Pk_tbMaestroListas] PRIMARY KEY CLUSTERED 
(
	[cnID_Lista] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbMenu]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbMenu](
	[cnID_Menu] [bigint] IDENTITY(1,1) NOT NULL,
	[cnID_Perfil] [int] NOT NULL,
	[ctTitulo] [varchar](25) NOT NULL,
	[ctDescipcion] [varchar](150) NULL,
	[ctPagina] [varchar](20) NOT NULL,
	[cnOrden] [int] NOT NULL,
 CONSTRAINT [Pk_tbMenu] PRIMARY KEY CLUSTERED 
(
	[cnID_Menu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbPais]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbPais](
	[cnID_Pais] [int] IDENTITY(1,1) NOT NULL,
	[ctPais] [varchar](150) NULL,
	[ctCodigoISO] [char](3) NULL,
 CONSTRAINT [Pk_tbPais] PRIMARY KEY CLUSTERED 
(
	[cnID_Pais] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbPerfilUsuario]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbPerfilUsuario](
	[cnID_Perfil] [int] NOT NULL,
	[ctDescripcion] [varchar](100) NOT NULL,
	[cnEstadoReg] [bit] NOT NULL,
 CONSTRAINT [Pk_tbPerfilUsuario] PRIMARY KEY CLUSTERED 
(
	[cnID_Perfil] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbPostulante]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbPostulante](
	[cnID_Postulante] [bigint] IDENTITY(1,1) NOT NULL,
	[cnID_Proceso] [bigint] NOT NULL,
	[cnID_Candidato] [bigint] NOT NULL,
	[cnPuntosCriterioAcademico] [int] NOT NULL,
	[cnPuntosCriterioLaboral] [int] NOT NULL,
	[cnPuntosCriterioAdicional] [int] NOT NULL,
	[cnEvalAuto] [bit] NULL,
	[cnEvalManual] [bit] NOT NULL,
	[cnEstadoReg] [tinyint] NULL,
	[cfFechaCreacion] [datetime] NOT NULL,
	[cfFechaActualizacion] [datetime] NULL,
	[ctIPAcceso] [varchar](15) NOT NULL,
 CONSTRAINT [Pk_tbPostulante] PRIMARY KEY CLUSTERED 
(
	[cnID_Postulante] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbPostulanteAcad]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbPostulanteAcad](
	[cnID_PostulanteAcad] [bigint] IDENTITY(1,1) NOT NULL,
	[cnID_Postulante] [bigint] NOT NULL,
	[cnID_Academico] [bigint] NOT NULL,
	[cnID_Grado] [bigint] NOT NULL,
	[cnID_Situacion] [bigint] NOT NULL,
 CONSTRAINT [Pk_tbPostulanteAcad] PRIMARY KEY CLUSTERED 
(
	[cnID_PostulanteAcad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbPostulanteCobe]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbPostulanteCobe](
	[cnID_PostulanteCobe] [bigint] IDENTITY(1,1) NOT NULL,
	[cnID_Postulante] [bigint] NOT NULL,
	[cnID_Cobertura] [bigint] NOT NULL,
	[cnID_CoberturaZona] [bigint] NOT NULL,
 CONSTRAINT [Pk_tbPostulanteCobe] PRIMARY KEY CLUSTERED 
(
	[cnID_PostulanteCobe] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbPostulanteCrit]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbPostulanteCrit](
	[cnID_PostulanteCrit] [bigint] IDENTITY(1,1) NOT NULL,
	[cnID_Postulante] [bigint] NOT NULL,
	[cnID_CriterioAdicional] [int] NOT NULL,
 CONSTRAINT [Pk_tbPostulanteCrit] PRIMARY KEY CLUSTERED 
(
	[cnID_PostulanteCrit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbPostulanteDocu]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbPostulanteDocu](
	[cnID_PostulanteDocu] [bigint] IDENTITY(1,1) NOT NULL,
	[cnID_Postulante] [bigint] NOT NULL,
	[cnID_Documento] [bigint] NOT NULL,
	[ctRutaArchivo] [varchar](150) NULL,
 CONSTRAINT [Pk_tbPostulanteDocu] PRIMARY KEY CLUSTERED 
(
	[cnID_PostulanteDocu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbPostulanteEvalManual]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbPostulanteEvalManual](
	[cnPostulanteEvalManual] [bigint] IDENTITY(1,1) NOT NULL,
	[cnID_Postulante] [bigint] NOT NULL,
	[cnID_TipoEval] [bigint] NOT NULL,
	[cnEvaluacion] [bit] NOT NULL,
	[ctObservacion] [varchar](800) NULL,
	[ctUsuarioEval] [char](15) NOT NULL,
	[cfFechaEval] [datetime] NULL,
 CONSTRAINT [Pk_tbPostulaEvalManual] PRIMARY KEY CLUSTERED 
(
	[cnPostulanteEvalManual] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbPostulanteLabo]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbPostulanteLabo](
	[cnID_PostulanteLabo] [bigint] IDENTITY(1,1) NOT NULL,
	[cnID_Postulante] [bigint] NOT NULL,
	[cnID_Experiencia] [bigint] NULL,
	[cnID_TipoCriterioLabo] [int] NOT NULL,
	[cnTiempoAnios] [tinyint] NOT NULL,
	[cnTiempoMeses] [tinyint] NULL,
 CONSTRAINT [Pk_tbPostulanteLabo] PRIMARY KEY CLUSTERED 
(
	[cnID_PostulanteLabo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbProceso]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbProceso](
	[cnID_Proceso] [bigint] IDENTITY(1,1) NOT NULL,
	[ctTitulo] [varchar](150) NOT NULL,
	[ctDescripcion] [varchar](250) NOT NULL,
	[cfFechaInicio] [datetime] NOT NULL,
	[cfFechaFin] [datetime] NOT NULL,
	[cnID_Estado] [bigint] NOT NULL,
	[ctArchivoTerminos] [varchar](200) NULL,
	[ctArchivoProceso] [varchar](200) NULL,
	[cnIndEvaluacion] [bit] NOT NULL,
	[ctUsuarioCreador] [char](15) NOT NULL,
	[cfFechaCreacion] [datetime] NOT NULL,
	[cfFechaActualizacion] [datetime] NULL,
	[ctIPAcceso] [varchar](15) NOT NULL,
 CONSTRAINT [Pk_tbProceso] PRIMARY KEY CLUSTERED 
(
	[cnID_Proceso] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbProcesoCnfgAcad]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbProcesoCnfgAcad](
	[cnID_CnfgAcademica] [bigint] IDENTITY(1,1) NOT NULL,
	[cnID_Proceso] [bigint] NOT NULL,
	[cnID_Grado] [bigint] NOT NULL,
	[cnID_Situacion] [bigint] NOT NULL,
	[cnID_OperadorLogico] [bigint] NULL,
	[cnGrupoLogico] [smallint] NULL,
	[cnPuntaje] [int] NOT NULL,
 CONSTRAINT [Pk_tbProcesoCnfgAcad] PRIMARY KEY CLUSTERED 
(
	[cnID_CnfgAcademica] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbProcesoCnfgLabo]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbProcesoCnfgLabo](
	[cnID_CnfgLaboral] [bigint] IDENTITY(1,1) NOT NULL,
	[cnID_Proceso] [bigint] NOT NULL,
	[cnID_TipoEntidad] [bigint] NOT NULL,
	[cnID_TipoCriterioLabo] [bigint] NULL,
	[cnID_TipoEvaluacion] [bigint] NOT NULL,
	[ctDescripcion] [varchar](300) NOT NULL,
 CONSTRAINT [Pk_tbProcesoCnfgLabo] PRIMARY KEY CLUSTERED 
(
	[cnID_CnfgLaboral] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbProcesoCobertura]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbProcesoCobertura](
	[cnID_Cobertura] [bigint] IDENTITY(1,1) NOT NULL,
	[cnID_Proceso] [bigint] NOT NULL,
	[ctNombre] [varchar](100) NOT NULL,
 CONSTRAINT [Pk_tbProcesoCobertura] PRIMARY KEY CLUSTERED 
(
	[cnID_Cobertura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbProcesoCriterioAdic]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbProcesoCriterioAdic](
	[cnID_CriterioAdicional] [bigint] IDENTITY(1,1) NOT NULL,
	[cnID_Proceso] [bigint] NOT NULL,
	[ctDescripcion] [varchar](150) NULL,
	[cnPuntaje] [int] NOT NULL,
 CONSTRAINT [Pk_tbCriterioAdicional] PRIMARY KEY CLUSTERED 
(
	[cnID_CriterioAdicional] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbProcesoDocumento]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbProcesoDocumento](
	[cnID_Documento] [bigint] IDENTITY(1,1) NOT NULL,
	[cnID_Proceso] [bigint] NOT NULL,
	[ctDescripcion] [varchar](150) NOT NULL,
	[cnIndObligatorio] [bit] NOT NULL,
 CONSTRAINT [Pk_tbProcesoDocumento] PRIMARY KEY CLUSTERED 
(
	[cnID_Documento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbProcesoMatrizLabo]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbProcesoMatrizLabo](
	[cnID_MatrizLaboral] [bigint] IDENTITY(1,1) NOT NULL,
	[cnID_CnfgLaboral] [bigint] NOT NULL,
	[cnMininmo] [int] NOT NULL,
	[cnMaximo] [int] NOT NULL,
	[cnPuntaje] [int] NOT NULL,
 CONSTRAINT [Pk_tbProcesoMatrizLabo] PRIMARY KEY CLUSTERED 
(
	[cnID_MatrizLaboral] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbProvincia]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbProvincia](
	[cnID_Provincia] [int] IDENTITY(1,1) NOT NULL,
	[cnID_Departamento] [int] NOT NULL,
	[ctProvincia] [varchar](150) NULL,
	[ctCodigoISO] [varchar](4) NULL,
 CONSTRAINT [Pk_tbProvincia] PRIMARY KEY CLUSTERED 
(
	[cnID_Provincia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbReferencia]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbReferencia](
	[cnID_Referencia] [bigint] IDENTITY(1,1) NOT NULL,
	[cnID_Candidato] [bigint] NOT NULL,
	[ctNombreCompleto] [varchar](200) NOT NULL,
	[ctNombreEmpresa] [varchar](200) NOT NULL,
	[ctCargo] [varchar](150) NOT NULL,
	[ctTelefono] [varchar](15) NOT NULL,
	[cfFechaCreacion] [datetime] NOT NULL,
	[cfFechaActualizacion] [datetime] NULL,
	[ctIPAcceso] [varchar](15) NOT NULL,
 CONSTRAINT [Pk_tbReferencia] PRIMARY KEY CLUSTERED 
(
	[cnID_Referencia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbUsuario]    Script Date: 29/06/2015 23:40:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbUsuario](
	[cnID_Usuario] [bigint] IDENTITY(1,1) NOT NULL,
	[cnID_Perfil] [int] NOT NULL,
	[ctNombres] [varchar](200) NOT NULL,
	[ctApellidos] [varchar](200) NOT NULL,
	[ctLogin] [char](15) NOT NULL,
	[ctClave] [varchar](50) NOT NULL,
	[cnBloqueado] [bit] NOT NULL,
	[cnEstadoReg] [bit] NOT NULL,
 CONSTRAINT [Pk_tbUsuario] PRIMARY KEY CLUSTERED 
(
	[cnID_Usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

-- SET IDENTITY_INSERT [dbo].[tbAcademico] ON
-- INSERT [dbo].[tbAcademico] ([cnID_Academico], [cnID_Candidato], [cnID_Grado], [cnID_Institucion], [ctInstitucionOtro], [cnID_Carrera], [ctCarreraOtro], [cnID_Situacion], [cnID_Pais], [ctCiudad], [cfFechaInicio], [cfFechaFin], [ctCertificado], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (2, 1, 251, 103, N'ASDASD', 1310, N'ASDASD', 1637, 14, NULL, CAST(0x063A0B00 AS Date), CAST(0x263A0B00 AS Date), NULL, CAST(0x0000A4C501465E38 AS DateTime), CAST(0x0000A4C60180C20A AS DateTime), N'::1')
-- SET IDENTITY_INSERT [dbo].[tbAcademico] OFF
-- SET IDENTITY_INSERT [dbo].[tbCandidato] ON 

-- INSERT [dbo].[tbCandidato] ([cnID_Candidato], [ctApellidoPaterno], [ctApellidoMaterno], [ctNombres], [cnID_Sexo], [ctEstadoCivil], [ctLugarNacimiento], [cfFechaNacimiento], [cnID_PaisNacimiento], [ctDireccion], [cnID_Pais], [cnID_Departamento], [cnID_Provincia], [cnID_Distrito], [ctTelefonoCasa], [ctTelefonoMovil], [ctTelefonoOtro], [ctEmail], [ctClaveAcceso], [cnEstadoReg], [cnCodigoRecuperacion], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (1, N'ROQUE', N'GAMARRA', N'ALBERTO', 227, 1621, N'LIMA', CAST(0x6F110B00 AS Date), 177, N'Garcia Noblejas 51', 177, 15, 112, 869, N'644405685', N'644405685', N'644405685', N'alroqueg@hotmail.com                                                                                ', N'e10adc3949ba59abbe56e057f20f883e', 0, NULL, CAST(0x0000A4C2015C2D46 AS DateTime), CAST(0x0000A4C501353FE9 AS DateTime), N'::1')
-- INSERT [dbo].[tbCandidato] ([cnID_Candidato], [ctApellidoPaterno], [ctApellidoMaterno], [ctNombres], [cnID_Sexo], [ctEstadoCivil], [ctLugarNacimiento], [cfFechaNacimiento], [cnID_PaisNacimiento], [ctDireccion], [cnID_Pais], [cnID_Departamento], [cnID_Provincia], [cnID_Distrito], [ctTelefonoCasa], [ctTelefonoMovil], [ctTelefonoOtro], [ctEmail], [ctClaveAcceso], [cnEstadoReg], [cnCodigoRecuperacion], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (6, N'CFG', N'DFGDFg', N'DFGDFG', 227, 1624, N'DFGDFG', CAST(0x143A0B00 AS Date), 12, N'DFGDFGDFG', 15, 26, 196, 1840, N'4534534', N'', N'', N'dsfombre@dominio.com                                                                                ', N'e10adc3949ba59abbe56e057f20f883e', 0, NULL, CAST(0x0000A4C2015FD5BA AS DateTime), NULL, N'::1')
-- SET IDENTITY_INSERT [dbo].[tbCandidato] OFF

 SET IDENTITY_INSERT [dbo].[tbCnfgSistema] ON 
 INSERT [dbo].[tbCnfgSistema] ([cnID_Config], [ctDescripcion], [ctAlias], [ctValor]) VALUES (1, N'Tamaño de archivo permitido (PDF)', N'FileSize', N'3145728')
 SET IDENTITY_INSERT [dbo].[tbCnfgSistema] OFF

-- SET IDENTITY_INSERT [dbo].[tbCoberturaZona] ON 
-- INSERT [dbo].[tbCoberturaZona] ([cnID_CoberturaZona], [cnID_Cobertura], [ctZona]) VALUES (3, 1, N'PUNO, MAÑAZO, TIQUILLACA, VILQUE')
-- SET IDENTITY_INSERT [dbo].[tbCoberturaZona] OFF
-- SET IDENTITY_INSERT [dbo].[tbComplementario] ON 

-- INSERT [dbo].[tbComplementario] ([cnID_Complementario], [cnID_Candidato], [cnID_TipoEstudio], [ctDescripcion], [cnID_Institucion], [ctInstitucionOtro], [cnID_Situacion], [cfFechaInicio], [cfFechaFin], [cnID_Pais], [ctCertificado], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (1, 1, 1626, N'SDF76799', 96, N'DFDGF', 229, CAST(0x0000A4BB00000000 AS DateTime), CAST(0x0000A4C400000000 AS DateTime), 13, NULL, CAST(0x0000A4C400074EEF AS DateTime), NULL, N'::1')
-- SET IDENTITY_INSERT [dbo].[tbComplementario] OFF

SET IDENTITY_INSERT [dbo].[tbDepartamento] ON 
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (1, N'AMAZONAS', N'01')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (2, N'ANCASH', N'02')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (3, N'APURIMAC', N'03')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (4, N'AREQUIPA', N'04')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (5, N'AYACUCHO', N'05')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (6, N'CAJAMARCA', N'06')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (7, N'CALLAO', N'07')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (8, N'CUZCO', N'08')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (9, N'HUANCAVELICA', N'09')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (10, N'HUANUCO', N'10')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (11, N'ICA', N'11')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (12, N'JUNIN', N'12')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (13, N'LA LIBERTAD', N'13')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (14, N'LAMBAYEQUE', N'14')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (15, N'LIMA', N'15')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (16, N'LORETO', N'16')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (17, N'MADRE DE DIOS', N'17')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (18, N'MOQUEGUA', N'18')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (19, N'PASCO', N'19')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (20, N'PIURA', N'20')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (21, N'PUNO', N'21')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (22, N'SAN MARTIN', N'22')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (23, N'TACNA', N'23')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (24, N'TUMBES', N'24')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (25, N'UCAYALI', N'25')
INSERT [dbo].[tbDepartamento] ([cnID_Departamento], [ctDepartamento], [ctCodigoISO]) VALUES (26, N'OTROS', N'00')
SET IDENTITY_INSERT [dbo].[tbDepartamento] OFF

SET IDENTITY_INSERT [dbo].[tbDistrito] ON 
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1, 3, 1, N'ABANCAY', N'030101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (2, 2, 23, N'ABELARDO PARDO LEZAMETA', N'020302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (3, 4, 36, N'ACARI', N'040402')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (4, 2, 128, N'ACAS', N'022001')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (5, 7, 139, N'ACCHA', N'071002')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (6, 5, 187, N'ACCOMARCA', N'050903')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (7, 21, 17, N'ACHAYA', N'200202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (8, 4, 43, N'ACHOMA', N'040202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (9, 2, 62, N'ACO', N'020602')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (10, 12, 55, N'ACO', N'110202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (11, 2, 171, N'ACOBAMBA', N'021407')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (12, 9, 2, N'ACOBAMBA', N'080201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (13, 12, 179, N'ACOBAMBA', N'110502')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (14, 9, 84, N'ACOBAMBILLA', N'080102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (15, 2, 13, N'ACOCHACA', N'021802')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (16, 5, 80, N'ACOCRO', N'050111')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (17, 12, 100, N'ACOLLA', N'110302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (18, 7, 3, N'ACOMAYO', N'070201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (19, 2, 38, N'ACOPAMPA', N'020402')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (20, 7, 3, N'ACOPIA', N'070202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (21, 21, 150, N'ACORA', N'200102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (22, 9, 84, N'ACORIA', N'080103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (23, 7, 3, N'ACOS', N'070203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (24, 5, 80, N'ACOS VINCHOS', N'050102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (25, 9, 180, N'ACOSTAMBO', N'080502')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (26, 9, 180, N'ACRAQUIA', N'080503')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (27, 2, 11, N'ACZO', N'021602')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (28, 13, 129, N'AGALLPAMPA', N'120402')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (29, 22, 69, N'AGUA BLANCA', N'211002')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (30, 24, 195, N'AGUAS VERDES', N'230304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (31, 12, 54, N'AHUAC', N'110902')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (32, 9, 180, N'AHUAYCHA', N'080504')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (33, 2, 4, N'AIJA', N'020201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (34, 21, 36, N'AJOYANI', N'200302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (35, 22, 159, N'ALBERTO LEVEAU', N'210602')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (36, 4, 105, N'ALCA', N'040802')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (37, 5, 186, N'ALCAMENCA', N'050702')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (38, 2, 171, N'ALFONSO UGARTE', N'021402')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (39, 15, 192, N'ALIS', N'140702')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (40, 15, 192, N'ALLAUCA', N'140703')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (41, 22, 107, N'ALONSO DE ALVARADO', N'210315')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (42, 22, 21, N'ALTO BIAVO', N'210704')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (43, 23, 174, N'ALTO DE LA ALIANZA', N'220111')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (44, 21, 165, N'ALTO INAMBARI', N'200811')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (45, 11, 49, N'ALTO LARAN', N'100209')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (46, 16, 122, N'ALTO NANAY', N'150102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (47, 7, 71, N'ALTO PICHIGUA', N'070808')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (48, 22, 78, N'ALTO SAPOSOA', N'210205')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (49, 4, 12, N'ALTO SELVA ALEGRE', N'040128')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (50, 16, 154, N'ALTO TAPICHE', N'150402')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (51, 21, 150, N'AMANTANI', N'200115')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (52, 10, 86, N'AMARILIS', N'090110')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (53, 2, 38, N'AMASHCA', N'020403')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (54, 15, 93, N'AMBAR', N'140502')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (55, 10, 6, N'AMBO', N'090201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (56, 20, 135, N'AMOTAPE', N'190502')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (57, 21, 157, N'ANANEA', N'201104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (58, 21, 194, N'ANAPIA', N'201003')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (59, 7, 9, N'ANCAHUASI', N'070309')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (60, 9, 8, N'ANCHONGA', N'080302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (61, 5, 105, N'ANCO', N'050402')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (62, 9, 55, N'ANCO', N'080702')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (63, 3, 50, N'ANCO HUALLO', N'030705')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (64, 15, 112, N'ANCON', N'140102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (65, 6, 167, N'ANDABAMBA', N'060910')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (66, 9, 2, N'ANDABAMBA', N'080203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (67, 4, 41, N'ANDAGUA', N'040502')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (68, 3, 7, N'ANDAHUAYLAS', N'030301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (69, 7, 152, N'ANDAHUAYLILLAS', N'071202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (70, 15, 130, N'ANDAJES', N'141004')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (71, 12, 55, N'ANDAMARCA', N'110203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (72, 3, 7, N'ANDARAPA', N'030302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (73, 4, 57, N'ANDARAY', N'040602')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (74, 16, 66, N'ANDOAS', N'150702')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (75, 13, 168, N'ANGASMARCA', N'120708')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (76, 6, 51, N'ANGUIA', N'060602')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (77, 2, 90, N'ANRA', N'020816')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (78, 2, 38, N'ANTA', N'020404')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (79, 7, 9, N'ANTA', N'070301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (80, 9, 2, N'ANTA', N'080202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (81, 3, 10, N'ANTABAMBA', N'030401')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (82, 21, 123, N'ANTAUTA', N'200702')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (83, 15, 91, N'ANTIOQUIA', N'140602')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (84, 2, 23, N'ANTONIO RAIMONDI', N'020321')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (85, 10, 190, N'APARICIO POMARES', N'091102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (86, 12, 100, N'APATA', N'110303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (87, 4, 41, N'APLAO', N'040501')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (88, 5, 186, N'APONGO', N'050703')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (89, 2, 23, N'AQUIA', N'020304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (90, 15, 34, N'ARAHUAY', N'140302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (91, 1, 19, N'ARAMANGO', N'010202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (92, 10, 78, N'ARANCAY', N'090402')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (93, 21, 17, N'ARAPA', N'200203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (94, 20, 135, N'ARENAL', N'190503')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (95, 4, 12, N'AREQUIPA', N'040101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (96, 9, 42, N'ARMA', N'080402')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (97, 9, 84, N'ASCENSION', N'080119')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (98, 13, 13, N'ASCOPE', N'120801')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (99, 15, 35, N'ASIA', N'140416')
GO
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (100, 21, 17, N'ASILLO', N'200204')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (101, 5, 186, N'ASQUIPATA', N'050715')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (102, 1, 45, N'ASUNCION', N'010102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (103, 6, 26, N'ASUNCION', N'060102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (104, 2, 38, N'ATAQUERO', N'020405')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (105, 12, 100, N'ATAURA', N'110304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (106, 15, 88, N'ATAVILLOS ALTO', N'140802')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (107, 15, 88, N'ATAVILLOS BAJO', N'140803')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (108, 15, 112, N'ATE', N'140103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (109, 4, 36, N'ATICO', N'040403')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (110, 4, 36, N'ATIQUIPA', N'040404')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (111, 21, 150, N'ATUNCOLLA', N'200103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (112, 15, 88, N'AUCALLAMA', N'140804')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (113, 5, 114, N'AUCARA', N'050502')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (114, 9, 42, N'AURAHUA', N'080403')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (115, 22, 155, N'AWAJUN', N'210509')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (116, 20, 16, N'AYABACA', N'190201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (117, 5, 80, N'AYACUCHO', N'050101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (118, 5, 86, N'AYAHUANCO', N'050302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (119, 21, 36, N'AYAPATA', N'200303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (120, 9, 94, N'AYAVI', N'080601')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (121, 15, 192, N'AYAVIRI', N'140704')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (122, 21, 123, N'AYAVIRI', N'200701')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (123, 5, 105, N'AYNA', N'050403')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (124, 4, 41, N'AYO', N'040503')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (125, 15, 192, N'AZANGARO', N'140705')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (126, 21, 17, N'AZANGARO', N'200201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (127, 1, 19, N'BAGUA', N'010205')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (128, 1, 186, N'BAGUA GRANDE', N'010701')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (129, 22, 21, N'BAJO BIAVO', N'210706')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (130, 16, 5, N'BALSAPUERTO', N'150202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (131, 1, 45, N'BALSAS', N'010103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (132, 6, 77, N'BAMBAMARCA', N'060701')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (133, 13, 21, N'BAMBAMARCA', N'120202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (134, 2, 62, N'BAMBAS', N'020603')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (135, 10, 110, N'BAÑOS', N'091002')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (136, 15, 20, N'BARRANCA', N'140901')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (137, 16, 66, N'BARRANCA', N'150701')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (138, 15, 112, N'BARRANCO', N'140125')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (139, 22, 107, N'BARRANQUITA', N'210303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (140, 5, 172, N'BELEN', N'051102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (141, 16, 122, N'BELEN', N'150112')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (142, 4, 36, N'BELLA UNION', N'040405')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (143, 6, 98, N'BELLAVISTA', N'060802')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (144, 20, 173, N'BELLAVISTA', N'190602')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (145, 22, 21, N'BELLAVISTA', N'210701')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (146, 7, 28, N'BELLAVISTA', N'240102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (147, 20, 170, N'BELLAVISTA DE LA UNION', N'190804')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (148, 20, 170, N'BERNAL', N'190803')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (149, 6, 161, N'BOLIVAR', N'061013')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (150, 13, 21, N'BOLIVAR', N'120201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (151, 2, 136, N'BOLOGNESI', N'021002')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (152, 15, 112, N'BREÑA', N'140104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (153, 2, 40, N'BUENA VISTA ALTA', N'020502')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (154, 20, 124, N'BUENOS AIRES', N'190402')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (155, 22, 144, N'BUENOS AIRES', N'210902')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (156, 13, 141, N'BULDIBUYO', N'120602')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (157, 2, 136, N'CABANA', N'021001')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (158, 5, 114, N'CABANA', N'050503')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (159, 21, 162, N'CABANA', N'200902')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (160, 4, 43, N'CABANACONDE', N'040203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (161, 21, 109, N'CABANILLA', N'200602')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (162, 21, 162, N'CABANILLAS', N'200903')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (163, 22, 159, N'CACATACHI', N'210604')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (164, 2, 166, N'CACERES DEL PERU', N'021302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (165, 6, 25, N'CACHACHI', N'060202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (166, 13, 168, N'CACHICADAN', N'120702')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (167, 7, 9, N'CACHIMAYO', N'070308')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (168, 15, 192, N'CACRA', N'140706')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (169, 10, 190, N'CAHUAC', N'091103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (170, 4, 36, N'CAHUACHO', N'040406')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (171, 16, 66, N'CAHUAPANAS', N'150703')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (172, 7, 143, N'CAICAY', N'071102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (173, 23, 32, N'CAIRANI', N'220402')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (174, 9, 2, N'CAJA', N'080204')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (175, 6, 25, N'CAJABAMBA', N'060201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (176, 2, 23, N'CAJACAY', N'020305')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (177, 6, 26, N'CAJAMARCA', N'060101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (178, 2, 128, N'CAJAMARQUILLA', N'022002')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (179, 1, 186, N'CAJARURO', N'010702')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (180, 15, 27, N'CAJATAMBO', N'140201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (181, 2, 90, N'CAJAY', N'020802')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (182, 13, 101, N'CALAMARCA', N'121003')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (183, 23, 174, N'CALANA', N'220102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (184, 15, 35, N'CALANGO', N'140402')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (185, 21, 109, N'CALAPUJA', N'200603')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (186, 7, 28, N'CALCA', N'070401')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (187, 15, 93, N'CALETA DE CARQUIN', N'140504')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (188, 15, 91, N'CALLAHUANCA', N'140603')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (189, 4, 43, N'CALLALLI', N'040205')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (190, 9, 8, N'CALLANMARCA', N'080303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (191, 7, 28, N'CALLAO', N'240101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (192, 6, 64, N'CALLAYUC', N'060502')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (193, 25, 61, N'CALLERIA', N'250101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (194, 6, 161, N'CALQUIS', N'061002')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (195, 22, 126, N'CALZADA', N'210102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (196, 4, 28, N'CAMANA', N'040301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (197, 7, 152, N'CAMANTI', N'071203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (198, 23, 32, N'CAMILACA', N'220406')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (199, 21, 17, N'CAMINACA', N'200205')
GO
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (200, 22, 117, N'CAMPANILLA', N'210402')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (201, 1, 115, N'CAMPORREDONDO', N'010402')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (202, 25, 61, N'CAMPOVERDE', N'250104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (203, 5, 186, N'CANARIA', N'050704')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (204, 10, 76, N'CANCHABAMBA', N'090903')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (205, 20, 81, N'CANCHAQUE', N'190302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (206, 12, 100, N'CANCHAYLLO', N'110305')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (207, 23, 32, N'CANDARAVE', N'220401')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (208, 5, 33, N'CANGALLO', N'050201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (209, 2, 23, N'CANIS', N'020322')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (210, 24, 59, N'CANOAS DE PUNTA SAL', N'230203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (211, 15, 34, N'CANTA', N'140301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (212, 14, 72, N'CAÑARIS', N'130203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (213, 21, 150, N'CAPACHICA', N'200104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (214, 7, 53, N'CAPACMARCA', N'070702')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (215, 21, 69, N'CAPASO', N'201204')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (216, 3, 17, N'CAPAYA', N'030202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (217, 16, 154, N'CAPELO', N'150403')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (218, 9, 42, N'CAPILLAS', N'080405')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (219, 13, 101, N'CARABAMBA', N'121002')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (220, 15, 112, N'CARABAYLLO', N'140105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (221, 21, 162, N'CARACOTO', N'200904')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (222, 15, 91, N'CARAMPOMA', N'140604')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (223, 15, 192, N'CARANIA', N'140707')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (224, 5, 80, N'CARAPO', N'050804')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (225, 4, 36, N'CARAVELI', N'040401')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (226, 3, 17, N'CARAYBAMBA', N'030203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (227, 2, 94, N'CARAZ', N'020701')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (228, 12, 85, N'CARHUACALLANGA', N'110103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (229, 12, 102, N'CARHUAMAYO', N'110402')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (230, 5, 187, N'CARHUANCA', N'050904')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (231, 2, 128, N'CARHUAPAMPA', N'022003')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (232, 2, 38, N'CARHUAZ', N'020401')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (233, 5, 80, N'CARMEN ALTO', N'050103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (234, 7, 28, N'CARMEN DE LA LEGUA-REYNOSO', N'240104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (235, 5, 114, N'CARMEN SALCEDO', N'050504')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (236, 18, 120, N'CARUMAS', N'170102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (237, 13, 13, N'CASA GRANDE', N'120808')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (238, 2, 119, N'CASCA', N'020902')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (239, 2, 193, N'CASCAPARA', N'021502')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (240, 13, 73, N'CASCAS', N'121101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (241, 2, 171, N'CASHAPAMPA', N'021408')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (242, 24, 59, N'CASITAS', N'230202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (243, 2, 40, N'CASMA', N'020501')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (244, 22, 144, N'CASPIZAPA', N'210903')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (245, 15, 91, N'CASTA', N'140605')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (246, 20, 146, N'CASTILLA', N'190103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (247, 9, 42, N'CASTROVIRREYNA', N'080401')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (248, 2, 153, N'CATAC', N'021210')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (249, 20, 146, N'CATACAOS', N'190104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (250, 6, 167, N'CATACHE', N'060902')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (251, 15, 192, N'CATAHUASI', N'140733')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (252, 6, 161, N'CATILLUC', N'061012')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (253, 15, 130, N'CAUJUL', N'141003')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (254, 14, 48, N'CAYALTI', N'130116')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (255, 5, 186, N'CAYARA', N'050706')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (256, 4, 57, N'CAYARANI', N'040603')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (257, 4, 43, N'CAYLLOMA', N'040204')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (258, 4, 12, N'CAYMA', N'040102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (259, 10, 6, N'CAYNA', N'090202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (260, 22, 107, N'CAYNARACHI', N'210304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (261, 7, 139, N'CCAPI', N'071003')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (262, 7, 152, N'CCARHUAYO', N'071204')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (263, 7, 152, N'CCATCA', N'071205')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (264, 9, 8, N'CCOCHACCASA', N'080312')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (265, 7, 63, N'CCORCA', N'070102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (266, 6, 43, N'CELENDIN', N'060301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (267, 15, 35, N'CERRO AZUL', N'140403')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (268, 4, 12, N'CERRO COLORADO', N'040103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (269, 10, 190, N'CHACABAMBA', N'091104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (270, 12, 191, N'CHACAPALPA', N'110602')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (271, 12, 85, N'CHACAPAMPA', N'110106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (272, 2, 13, N'CHACAS', N'021801')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (273, 19, 65, N'CHACAYAN', N'180202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (274, 2, 11, N'CHACCHO', N'021603')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (275, 1, 45, N'CHACHAPOYAS', N'010101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (276, 4, 41, N'CHACHAS', N'040504')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (277, 15, 112, N'CHACLACAYO', N'140107')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (278, 3, 1, N'CHACOCHE', N'030104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (279, 6, 51, N'CHADIN', N'060605')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (280, 10, 133, N'CHAGLLA', N'090702')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (281, 4, 36, N'CHALA', N'040407')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (282, 20, 124, N'CHALACO', N'190403')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (283, 6, 51, N'CHALAMARCA', N'060619')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (284, 5, 172, N'CHALCOS', N'051103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (285, 3, 17, N'CHALHUANCA', N'030201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (286, 7, 143, N'CHALLABAMBA', N'071104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (287, 3, 63, N'CHALLHUAHUACHO', N'030506')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (288, 7, 53, N'CHAMACA', N'070704')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (289, 12, 55, N'CHAMBARA', N'110206')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (290, 6, 159, N'CHANCAY', N'061207')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (291, 15, 88, N'CHANCAY', N'140805')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (292, 6, 167, N'CHANCAYBAÑOS', N'060903')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (293, 12, 46, N'CHANCHAMAYO', N'110801')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (294, 11, 127, N'CHANGUILLO', N'100302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (295, 13, 188, N'CHAO', N'121202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (296, 4, 36, N'CHAPARRA', N'040408')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (297, 3, 17, N'CHAPIMARCA', N'030206')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (298, 4, 12, N'CHARACATO', N'040104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (299, 13, 129, N'CHARAT', N'120403')
GO
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (300, 4, 105, N'CHARCANA', N'040803')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (301, 19, 140, N'CHAUPIMARCA', N'180101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (302, 11, 49, N'CHAVIN', N'100202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (303, 2, 90, N'CHAVIN DE HUANTAR', N'020803')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (304, 10, 78, N'CHAVIN DE PARIARCA', N'090403')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (305, 10, 190, N'CHAVINILLO', N'091101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (306, 5, 114, N'CHAVIÑA', N'050506')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (307, 22, 159, N'CHAZUTA', N'210606')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (308, 7, 31, N'CHECACUPE', N'070603')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (309, 7, 30, N'CHECCA', N'070502')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (310, 15, 93, N'CHECRAS', N'140505')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (311, 13, 46, N'CHEPEN', N'120901')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (312, 6, 26, N'CHETILLA', N'060104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (313, 1, 45, N'CHETO', N'010104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (314, 3, 7, N'CHIARA', N'030303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (315, 5, 80, N'CHIARA', N'050104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (316, 13, 13, N'CHICAMA', N'120802')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (317, 12, 85, N'CHICCHE', N'110107')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (318, 4, 57, N'CHICHAS', N'040604')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (319, 15, 91, N'CHICLA', N'140607')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (320, 14, 48, N'CHICLAYO', N'130101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (321, 4, 12, N'CHIGUATA', N'040105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (322, 6, 51, N'CHIGUIRIP', N'060606')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (323, 12, 85, N'CHILCA', N'110108')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (324, 15, 35, N'CHILCA', N'140405')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (325, 5, 105, N'CHILCAS', N'050404')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (326, 4, 41, N'CHILCAYMARCA', N'040505')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (327, 5, 172, N'CHILCAYOC', N'051110')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (328, 6, 59, N'CHILETE', N'060403')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (329, 1, 45, N'CHILIQUIN', N'010105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (330, 13, 141, N'CHILLIA', N'120603')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (331, 6, 51, N'CHIMBAN', N'060607')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (332, 2, 166, N'CHIMBOTE', N'021301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (333, 11, 49, N'CHINCHA ALTA', N'100201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (334, 11, 49, N'CHINCHA BAJA', N'100203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (335, 10, 86, N'CHINCHAO', N'090102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (336, 7, 9, N'CHINCHAYPUJIO', N'070302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (337, 7, 185, N'CHINCHERO', N'071302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (338, 3, 50, N'CHINCHEROS', N'030701')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (339, 9, 55, N'CHINCHIHUASI', N'080703')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (340, 9, 8, N'CHINCHO', N'080305')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (341, 2, 171, N'CHINGALPO', N'021403')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (342, 2, 11, N'CHINGAS', N'021604')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (343, 5, 114, N'CHIPAO', N'050508')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (344, 22, 159, N'CHIPURANA', N'210607')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (345, 2, 23, N'CHIQUIAN', N'020301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (346, 1, 155, N'CHIRIMOTO', N'010503')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (347, 6, 158, N'CHIRINOS', N'061102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (348, 1, 23, N'CHISQUILLA', N'010304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (349, 4, 43, N'CHIVAY', N'040201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (350, 14, 108, N'CHOCHOPE', N'130302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (351, 4, 41, N'CHOCO', N'040506')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (352, 13, 13, N'CHOCOPE', N'120803')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (353, 15, 192, N'CHOCOS', N'140710')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (354, 18, 72, N'CHOJATA', N'170203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (355, 10, 116, N'CHOLON', N'090502')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (356, 12, 85, N'CHONGOS ALTO', N'110109')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (357, 12, 54, N'CHONGOS BAJO', N'110903')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (358, 14, 48, N'CHONGOYAPE', N'130102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (359, 19, 130, N'CHONTABAMBA', N'180302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (360, 6, 98, N'CHONTALI', N'060804')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (361, 10, 190, N'CHORAS', N'091108')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (362, 6, 51, N'CHOROPAMPA', N'060618')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (363, 6, 64, N'CHOROS', N'060504')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (364, 15, 112, N'CHORRILLOS', N'140108')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (365, 6, 51, N'CHOTA', N'060601')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (366, 21, 150, N'CHUCUITO', N'200106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (367, 13, 163, N'CHUGAY', N'120304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (368, 6, 77, N'CHUGUR', N'060702')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (369, 20, 124, N'CHULUCANAS', N'190401')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (370, 5, 138, N'CHUMPI', N'050605')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (371, 6, 43, N'CHUMUCH', N'060303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (372, 5, 105, N'CHUNGUI', N'050405')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (373, 21, 17, N'CHUPA', N'200206')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (374, 12, 54, N'CHUPACA', N'110901')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (375, 9, 42, N'CHUPAMARCA', N'080408')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (376, 12, 85, N'CHUPURO', N'110112')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (377, 1, 45, N'CHUQUIBAMBA', N'010106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (378, 4, 57, N'CHUQUIBAMBA', N'040601')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (379, 3, 75, N'CHUQUIBAMBILLA', N'030601')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (380, 10, 68, N'CHUQUIS', N'090307')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (381, 9, 55, N'CHURCAMPA', N'080701')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (382, 10, 86, N'CHURUBAMBA', N'090103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (383, 1, 23, N'CHURUJA', N'010305')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (384, 5, 33, N'CHUSCHI', N'050204')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (385, 15, 112, N'CIENEGUILLA', N'140139')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (386, 3, 1, N'CIRCA', N'030102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (387, 23, 174, N'CIUDAD NUEVA', N'220112')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (388, 18, 72, N'COALAQUE', N'170202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (389, 21, 36, N'COASA', N'200304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (390, 21, 150, N'COATA', N'200105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (391, 15, 35, N'COAYLLO', N'140404')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (392, 1, 115, N'COCABAMBA', N'010403')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (393, 4, 98, N'COCACHACRA', N'040702')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (394, 9, 42, N'COCAS', N'080406')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (395, 2, 89, N'COCHABAMBA', N'020103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (396, 6, 51, N'COCHABAMBA', N'060603')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (397, 10, 76, N'COCHABAMBA', N'090904')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (398, 1, 155, N'COCHAMAL', N'010502')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (399, 15, 130, N'COCHAMARCA', N'141006')
GO
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (400, 2, 91, N'COCHAPETI', N'021902')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (401, 3, 50, N'COCHARCAS', N'030704')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (402, 2, 128, N'COCHAS', N'022004')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (403, 12, 55, N'COCHAS', N'110205')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (404, 15, 192, N'COCHAS', N'140708')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (405, 13, 163, N'COCHORCO', N'120302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (406, 10, 149, N'CODO DEL POZUZO', N'090803')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (407, 2, 166, N'COISHCO', N'021308')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (408, 21, 81, N'COJATA', N'200502')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (409, 20, 135, N'COLAN', N'190505')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (410, 6, 98, N'COLASAY', N'060803')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (411, 5, 186, N'COLCA', N'050707')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (412, 12, 85, N'COLCA', N'110104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (413, 2, 89, N'COLCABAMBA', N'020104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (414, 3, 17, N'COLCABAMBA', N'030204')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (415, 9, 180, N'COLCABAMBA', N'080506')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (416, 1, 115, N'COLCAMAR', N'010404')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (417, 7, 139, N'COLCHA', N'071004')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (418, 15, 192, N'COLONIA', N'140709')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (419, 10, 6, N'COLPAS', N'090203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (420, 7, 53, N'COLQUEMARCA', N'070703')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (421, 7, 143, N'COLQUEPATA', N'071103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (422, 2, 23, N'COLQUIOC', N'020323')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (423, 5, 141, N'COLTA', N'051002')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (424, 2, 40, N'COMANDANTE NOEL', N'020503')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (425, 12, 55, N'COMAS', N'110204')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (426, 15, 112, N'COMAS', N'140106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (427, 7, 31, N'COMBAPATA', N'070602')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (428, 9, 84, N'CONAYCA', N'080104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (429, 5, 187, N'CONCEPCION', N'050905')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (430, 12, 55, N'CONCEPCION', N'110201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (431, 10, 6, N'CONCHAMARCA', N'090204')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (432, 6, 51, N'CONCHAN', N'060604')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (433, 2, 136, N'CONCHUCOS', N'021003')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (434, 6, 25, N'CONDEBAMBA', N'060203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (435, 13, 21, N'CONDORMARCA', N'120203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (436, 7, 71, N'CONDOROMA', N'070802')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (437, 21, 69, N'CONDURIRI', N'201205')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (438, 9, 8, N'CONGALLA', N'080304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (439, 2, 128, N'CONGAS', N'022005')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (440, 1, 115, N'CONILA', N'010405')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (441, 21, 124, N'CONIMA', N'201302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (442, 19, 130, N'CONSTITUCION', N'180308')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (443, 16, 184, N'CONTAMANA', N'150501')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (444, 6, 59, N'CONTUMAZA', N'060401')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (445, 15, 27, N'COPA', N'140205')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (446, 1, 19, N'COPALLIN', N'010203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (447, 21, 194, N'COPANI', N'201004')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (448, 4, 43, N'COPORAQUE', N'040206')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (449, 7, 71, N'COPORAQUE', N'070803')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (450, 5, 138, N'CORACORA', N'050601')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (451, 21, 36, N'CORANI', N'200305')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (452, 5, 141, N'CORCULLA', N'051003')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (453, 9, 94, N'CORDOVA', N'080602')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (454, 2, 4, N'CORIS', N'020203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (455, 5, 138, N'CORONEL CASTAÑEDA', N'050604')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (456, 23, 174, N'CORONEL GREGORIO ALBARRACIN L.', N'220113')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (457, 2, 62, N'CORONGO', N'020601')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (458, 1, 23, N'COROSHA', N'010302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (459, 24, 183, N'CORRALES', N'230102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (460, 6, 43, N'CORTEGANA', N'060302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (461, 9, 55, N'COSME', N'080711')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (462, 6, 26, N'COSPAN', N'060103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (463, 3, 63, N'COTABAMBAS', N'030503')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (464, 4, 105, N'COTAHUASI', N'040801')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (465, 2, 153, N'COTAPARACO', N'021202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (466, 3, 17, N'COTARUSE', N'030205')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (467, 12, 169, N'COVIRIALI', N'110702')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (468, 7, 28, N'COYA', N'070402')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (469, 3, 63, N'COYLLURQUI', N'030502')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (470, 20, 170, N'CRISTO NOS VALGA', N'190805')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (471, 21, 36, N'CRUCERO', N'200306')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (472, 18, 120, N'CUCHUMBAYA', N'170103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (473, 9, 84, N'CUENCA', N'080105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (474, 1, 23, N'CUISPES', N'010303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (475, 6, 64, N'CUJILLO', N'060503')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (476, 2, 91, N'CULEBRAS', N'021905')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (477, 12, 85, N'CULLHUAS', N'110105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (478, 1, 186, N'CUMBA', N'010703')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (479, 22, 107, N'CUÑUMBUQUI', N'210305')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (480, 21, 123, N'CUPI', N'200703')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (481, 6, 59, N'CUPISNIQUE', N'060406')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (482, 20, 146, N'CURA MORI', N'190113')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (483, 3, 1, N'CURAHUASI', N'030103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (484, 3, 75, N'CURASCO', N'030614')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (485, 13, 163, N'CURGOS', N'120303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (486, 23, 32, N'CURIBAYA', N'220403')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (487, 12, 100, N'CURICACA', N'110331')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (488, 25, 134, N'CURIMANA', N'250203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (489, 3, 75, N'CURPAHUASI', N'030602')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (490, 2, 62, N'CUSCA', N'020604')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (491, 7, 63, N'CUSCO', N'070101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (492, 7, 152, N'CUSIPATA', N'071206')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (493, 6, 64, N'CUTERVO', N'060501')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (494, 21, 194, N'CUTURAPI', N'201005')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (495, 21, 165, N'CUYOCUYO', N'200803')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (496, 10, 111, N'DANIEL ALOMIA ROBLES', N'090602')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (497, 9, 180, N'DANIEL HERNANDEZ', N'080509')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (498, 4, 98, N'DEAN VALDIVIA', N'040703')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (499, 21, 52, N'DESAGUADERO', N'200402')
GO
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (500, 7, 103, N'ECHARATE', N'070902')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (501, 6, 159, N'EDUARDO VILLANUEVA', N'061205')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (502, 15, 112, N'EL AGUSTINO', N'140135')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (503, 18, 97, N'EL ALGARROBAL', N'170302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (504, 20, 176, N'EL ALTO', N'190702')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (505, 9, 55, N'EL CARMEN', N'080704')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (506, 11, 49, N'EL CARMEN', N'100204')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (507, 20, 81, N'EL CARMEN DE LA FRONTERA', N'190306')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (508, 1, 58, N'EL CENEPA', N'010603')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (509, 22, 78, N'EL ESLABON', N'210206')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (510, 11, 127, N'EL INGENIO', N'100303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (511, 12, 100, N'EL MANTARO', N'110306')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (512, 1, 186, N'EL MILAGRO', N'010704')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (513, 3, 10, N'EL ORO', N'030402')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (514, 1, 19, N'EL PARCO', N'010204')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (515, 13, 181, N'EL PORVENIR', N'120110')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (516, 22, 159, N'EL PORVENIR', N'210608')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (517, 6, 161, N'EL PRADO', N'061009')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (518, 20, 146, N'EL TALLAN', N'190114')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (519, 12, 85, N'EL TAMBO', N'110113')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (520, 2, 119, N'ELEAZAR GUZMAN BARRON', N'020908')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (521, 22, 155, N'ELIAS SOPLIN VARGAS', N'210506')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (522, 16, 154, N'EMILIO SAN MARTIN', N'150404')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (523, 6, 26, N'ENCAÑADA', N'060105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (524, 7, 71, N'ESPINAR', N'070801')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (525, 23, 178, N'ESTIQUE', N'220206')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (526, 23, 178, N'ESTIQUE PAMPA', N'220207')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (527, 14, 48, N'ETEN', N'130103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (528, 14, 48, N'ETEN PUERTO', N'130104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (529, 16, 122, N'FERNANDO LORES', N'150103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (530, 14, 72, N'FERREÑAFE', N'130201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (531, 2, 119, N'FIDEL OLIVAS ESCUDERO', N'020904')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (532, 17, 116, N'FITZCARRALD', N'160202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (533, 13, 181, N'FLORENCIA DE MORA', N'120112')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (534, 1, 23, N'FLORIDA', N'010306')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (535, 20, 16, N'FRIAS', N'190202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (536, 15, 27, N'GORGOR', N'140206')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (537, 19, 65, N'GOYLLARISQUIZGA', N'180203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (538, 1, 45, N'GRANADA', N'010107')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (539, 6, 159, N'GREGORIO PITA', N'061203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (540, 11, 49, N'GROCIO PRADO', N'100205')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (541, 13, 132, N'GUADALUPE', N'120503')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (542, 13, 188, N'GUADALUPITO', N'121203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (543, 6, 59, N'GUZMANGO', N'060404')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (544, 22, 126, N'HABANA', N'210103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (545, 3, 63, N'HAQUIRA', N'030504')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (546, 10, 111, N'HERMILIO VALDIZAN', N'090603')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (547, 23, 178, N'HEROES ALBARRACIN', N'220205')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (548, 12, 55, N'HEROINAS TOLEDO', N'110207')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (549, 15, 192, N'HONGOS', N'140730')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (550, 10, 149, N'HONORIA', N'090801')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (551, 6, 98, N'HUABAL', N'060812')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (552, 2, 90, N'HUACACHI', N'020804')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (553, 5, 172, N'HUACAÑA', N'051109')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (554, 10, 6, N'HUACAR', N'090205')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (555, 2, 136, N'HUACASCHUQUE', N'021004')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (556, 10, 76, N'HUACAYBAMBA', N'090901')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (557, 3, 50, N'HUACCANA', N'030706')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (558, 2, 90, N'HUACCHIS', N'020806')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (559, 12, 54, N'HUACHAC', N'110904')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (560, 2, 90, N'HUACHIS', N'020805')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (561, 15, 93, N'HUACHO', N'140501')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (562, 9, 84, N'HUACHOCOLPA', N'080106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (563, 9, 180, N'HUACHOCOLPA', N'080511')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (564, 19, 140, N'HUACHON', N'180103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (565, 9, 42, N'HUACHOS', N'080409')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (566, 5, 114, N'HUAC-HUAS', N'050510')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (567, 15, 91, N'HUACHUPAMPA', N'140630')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (568, 2, 4, N'HUACLLAN', N'020205')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (569, 10, 116, N'HUACRACHUCO', N'090501')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (570, 12, 85, N'HUACRAPUQUIO', N'110114')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (571, 21, 52, N'HUACULLANI', N'200403')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (572, 3, 75, N'HUAILLATI', N'030603')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (573, 6, 77, N'HUALGAYOC', N'060703')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (574, 12, 85, N'HUALHUAS', N'110116')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (575, 5, 186, N'HUALLA', N'050708')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (576, 22, 21, N'HUALLAGA', N'210705')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (577, 2, 23, N'HUALLANCA', N'020325')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (578, 2, 94, N'HUALLANCA', N'020702')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (579, 9, 8, N'HUALLAY-GRANDE', N'080306')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (580, 15, 93, N'HUALMAY', N'140506')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (581, 13, 163, N'HUAMACHUCO', N'120301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (582, 12, 100, N'HUAMALI', N'110307')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (583, 12, 54, N'HUAMANCACA CHICO', N'110905')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (584, 5, 86, N'HUAMANGUILLA', N'050303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (585, 5, 186, N'HUAMANQUIQUIA', N'050709')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (586, 15, 34, N'HUAMANTANGA', N'140303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (587, 9, 42, N'HUAMATAMBO', N'080410')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (588, 5, 187, N'HUAMBALPA', N'050906')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (589, 1, 155, N'HUAMBO', N'010504')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (590, 4, 43, N'HUAMBO', N'040207')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (591, 6, 51, N'HUAMBOS', N'060608')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (592, 15, 192, N'HUAMPARA', N'140711')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (593, 4, 43, N'HUANCA', N'040208')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (594, 19, 130, N'HUANCABAMBA', N'180303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (595, 20, 81, N'HUANCABAMBA', N'190301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (596, 9, 8, N'HUANCA-HUANCA', N'080307')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (597, 12, 85, N'HUANCAN', N'110118')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (598, 21, 81, N'HUANCANE', N'200501')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (599, 11, 145, N'HUANCANO', N'100402')
GO
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (600, 5, 186, N'HUANCAPI', N'050701')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (601, 15, 27, N'HUANCAPON', N'140207')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (602, 3, 7, N'HUANCARAMA', N'030304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (603, 7, 143, N'HUANCARANI', N'071106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (604, 3, 7, N'HUANCARAY', N'030305')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (605, 5, 186, N'HUANCARAYLLA', N'050710')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (606, 4, 41, N'HUANCARQUI', N'040507')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (607, 1, 45, N'HUANCAS', N'010108')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (608, 13, 141, N'HUANCASPATA', N'120605')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (609, 9, 84, N'HUANCAVELICA', N'080101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (610, 15, 192, N'HUANCAYA', N'140712')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (611, 12, 85, N'HUANCAYO', N'110101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (612, 13, 181, N'HUANCHACO', N'120102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (613, 2, 89, N'HUANCHAY', N'020105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (614, 9, 84, N'HUANDO', N'080120')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (615, 2, 136, N'HUANDOVAL', N'021005')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (616, 15, 192, N'HUANGASCAR', N'140713')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (617, 3, 1, N'HUANIPACA', N'030105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (618, 7, 139, N'HUANOQUITE', N'071005')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (619, 5, 86, N'HUANTA', N'050301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (620, 15, 192, N'HUANTAN', N'140714')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (621, 2, 90, N'HUANTAR', N'020807')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (622, 23, 32, N'HUANUARA', N'220404')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (623, 10, 86, N'HUANUCO', N'090101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (624, 4, 36, N'HUANUHUANU', N'040409')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (625, 15, 91, N'HUANZA', N'140608')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (626, 15, 192, N'HUAÑEC', N'140715')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (627, 3, 10, N'HUAQUIRCA', N'030403')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (628, 15, 88, N'HUARAL', N'140801')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (629, 13, 129, N'HUARANCHAL', N'120404')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (630, 6, 158, N'HUARANGO', N'061103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (631, 2, 89, N'HUARAZ', N'020101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (632, 2, 90, N'HUARI', N'020801')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (633, 19, 140, N'HUARIACA', N'180104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (634, 9, 180, N'HUARIBAMBA', N'080512')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (635, 12, 179, N'HUARICOLCA', N'110503')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (636, 12, 100, N'HUARIPAMPA', N'110308')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (637, 20, 81, N'HUARMACA', N'190303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (638, 2, 91, N'HUARMEY', N'021901')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (639, 7, 152, N'HUARO', N'071207')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (640, 15, 91, N'HUAROCHIRI', N'140609')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (641, 7, 9, N'HUAROCONDO', N'070303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (642, 15, 34, N'HUAROS', N'140304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (643, 12, 179, N'HUASAHUASI', N'110504')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (644, 12, 85, N'HUASICANCHA', N'110119')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (645, 6, 43, N'HUASMIN', N'060304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (646, 13, 101, N'HUASO', N'121004')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (647, 2, 23, N'HUASTA', N'020311')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (648, 2, 94, N'HUATA', N'020703')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (649, 21, 150, N'HUATA', N'200107')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (650, 21, 81, N'HUATASANI', N'200511')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (651, 15, 93, N'HUAURA', N'140507')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (652, 12, 191, N'HUAY HUAY', N'110603')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (653, 9, 94, N'HUAYACUNDO ARMA', N'080603')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (654, 2, 91, N'HUAYAN', N'021903')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (655, 3, 7, N'HUAYANA', N'030317')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (656, 2, 94, N'HUAYLAS', N'020704')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (657, 13, 141, N'HUAYLILLAS', N'120604')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (658, 2, 171, N'HUAYLLABAMBA', N'021404')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (659, 7, 185, N'HUAYLLABAMBA', N'071303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (660, 2, 23, N'HUAYLLACAYAN', N'020310')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (661, 9, 84, N'HUAYLLAHUARA', N'080108')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (662, 2, 147, N'HUAYLLAN', N'021102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (663, 2, 153, N'HUAYLLAPAMPA', N'021203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (664, 19, 140, N'HUAYLLAY', N'180105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (665, 3, 17, N'HUAYLLO', N'030207')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (666, 4, 105, N'HUAYNACOTAS', N'040804')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (667, 13, 141, N'HUAYO', N'120606')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (668, 7, 103, N'HUAYOPATA', N'070903')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (669, 21, 124, N'HUAYRAPATA', N'201304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (670, 9, 94, N'HUAYTARA', N'080604')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (671, 12, 85, N'HUAYUCACHI', N'110120')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (672, 17, 116, N'HUEPETUHE', N'160204')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (673, 12, 100, N'HUERTAS', N'110309')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (674, 22, 117, N'HUICUNGO', N'210403')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (675, 22, 159, N'HUIMBAYOC', N'210609')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (676, 11, 145, N'HUMAY', N'100403')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (677, 17, 175, N'IBERIA', N'160302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (678, 11, 96, N'ICA', N'100101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (679, 6, 159, N'ICHOCAN', N'061202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (680, 18, 72, N'ICHUÑA', N'170204')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (681, 4, 43, N'ICHUPAMPA', N'040209')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (682, 20, 173, N'IGNACIO ESCUDERO', N'190608')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (683, 5, 86, N'IGUAIN', N'050304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (684, 15, 88, N'IHUARI', N'140806')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (685, 23, 101, N'ILABAYA', N'220303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (686, 21, 69, N'ILAVE', N'201201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (687, 14, 108, N'ILLIMO', N'130303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (688, 18, 97, N'ILO', N'170301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (689, 1, 19, N'IMAZA', N'010206')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (690, 15, 35, N'IMPERIAL', N'140406')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (691, 16, 184, N'INAHUAYA', N'150506')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (692, 17, 177, N'INAMBARI', N'160102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (693, 14, 72, N'INCAHUASI', N'130202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (694, 21, 81, N'INCHUPALLA', N'200504')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (695, 23, 174, N'INCLAN', N'220104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (696, 2, 89, N'INDEPENDENCIA', N'020102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (697, 5, 187, N'INDEPENDENCIA', N'050908')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (698, 11, 145, N'INDEPENDENCIA', N'100404')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (699, 15, 112, N'INDEPENDENCIA', N'140134')
GO
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (700, 16, 122, N'INDIANA', N'150110')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (701, 12, 85, N'INGENIO', N'110121')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (702, 1, 115, N'INGUILPATA', N'010406')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (703, 17, 175, N'IÑAPARI', N'160301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (704, 25, 61, N'IPARIA', N'250105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (705, 16, 122, N'IQUITOS', N'150101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (706, 4, 57, N'IRAY', N'040605')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (707, 25, 134, N'IRAZOLA', N'250202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (708, 4, 98, N'ISLAY', N'040704')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (709, 23, 101, N'ITE', N'220302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (710, 21, 36, N'ITUATA', N'200307')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (711, 9, 84, N'IZCUCHACA', N'080109')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (712, 10, 190, N'JACAS CHICO', N'091105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (713, 10, 78, N'JACAS GRANDE', N'090404')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (714, 4, 12, N'JACOBO HUNTER', N'040127')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (715, 6, 98, N'JAEN', N'060801')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (716, 1, 186, N'JAMALCA', N'010705')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (717, 2, 89, N'JANGAS', N'020106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (718, 12, 100, N'JANJAILLO', N'110310')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (719, 4, 36, N'JAQUI', N'040410')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (720, 12, 100, N'JAUJA', N'110301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (721, 14, 108, N'JAYANCA', N'130304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (722, 1, 23, N'JAZAN', N'010312')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (723, 16, 5, N'JEBEROS', N'150205')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (724, 16, 154, N'JENARO HERRERA', N'150410')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (725, 22, 126, N'JEPELACIO', N'210104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (726, 13, 132, N'JEQUETEPEQUE', N'120504')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (727, 6, 26, N'JESUS', N'060106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (728, 10, 110, N'JESUS', N'091001')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (729, 15, 112, N'JESUS MARIA', N'140133')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (730, 5, 80, N'JESUS NAZARENO', N'050115')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (731, 20, 16, N'JILILI', N'190209')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (732, 10, 78, N'JIRCAN', N'090405')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (733, 10, 110, N'JIVIA', N'091007')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (734, 6, 43, N'JORGE CHAVEZ', N'060305')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (735, 10, 111, N'JOSE CRESPO Y CASTILLO', N'090606')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (736, 21, 17, N'JOSE DOMINGO CHOQUEHUANCA', N'200207')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (737, 6, 43, N'JOSE GALVEZ', N'060306')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (738, 14, 48, N'JOSE LEONARDO ORTIZ', N'130112')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (739, 4, 12, N'JOSE LUIS BUSTAMANTE Y RIVERO', N'040129')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (740, 6, 159, N'JOSE MANUEL QUIROZ', N'061204')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (741, 4, 28, N'JOSE MARIA QUIMPER', N'040302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (742, 6, 159, N'JOSE SABOGAL', N'061206')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (743, 3, 10, N'JUAN ESPINOZA MEDRANO', N'030404')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (744, 22, 159, N'JUAN GUERRA', N'210610')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (745, 22, 117, N'JUANJUI', N'210401')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (746, 9, 8, N'JULCAMARCA', N'080308')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (747, 12, 100, N'JULCAN', N'110311')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (748, 13, 101, N'JULCAN', N'121001')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (749, 21, 52, N'JULI', N'200401')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (750, 21, 162, N'JULIACA', N'200901')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (751, 1, 23, N'JUMBILLA', N'010301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (752, 12, 102, N'JUNIN', N'110401')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (753, 3, 17, N'JUSTO APU SAHUARAURA', N'030217')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (754, 3, 7, N'KAQUIABAMBA', N'030319')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (755, 21, 52, N'KELLUYO', N'200412')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (756, 7, 103, N'KIMBIRI', N'070909')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (757, 3, 7, N'KISHUARA', N'030306')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (758, 7, 143, N'KOSÑIPATA', N'071105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (759, 7, 30, N'KUNTURKANKI', N'070503')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (760, 20, 146, N'LA ARENA', N'190105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (761, 22, 159, N'LA BANDA DE SHILCAYO', N'210621')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (762, 20, 176, N'LA BREA', N'190703')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (763, 18, 72, N'LA CAPILLA', N'170205')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (764, 6, 158, N'LA COIPA', N'061105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (765, 24, 183, N'LA CRUZ', N'230103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (766, 13, 129, N'LA CUESTA', N'120405')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (767, 6, 167, N'LA ESPERANZA', N'060904')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (768, 13, 181, N'LA ESPERANZA', N'120111')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (769, 6, 161, N'LA FLORIDA', N'061003')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (770, 20, 135, N'LA HUACA', N'190504')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (771, 1, 45, N'LA JALCA', N'010109')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (772, 4, 12, N'LA JOYA', N'040106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (773, 2, 89, N'LA LIBERTAD', N'020107')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (774, 6, 43, N'LA LIBERTAD DE PALLAN', N'060312')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (775, 20, 124, N'LA MATANZA', N'190408')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (776, 2, 4, N'LA MERCED', N'020206')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (777, 9, 55, N'LA MERCED', N'080705')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (778, 15, 112, N'LA MOLINA', N'140110')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (779, 12, 191, N'LA OROYA', N'110601')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (780, 2, 62, N'LA PAMPA', N'020605')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (781, 1, 19, N'LA PECA', N'010201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (782, 7, 28, N'LA PERLA', N'240105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (783, 2, 23, N'LA PRIMAVERA', N'020324')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (784, 7, 28, N'LA PUNTA', N'240103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (785, 6, 64, N'LA RAMADA', N'060505')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (786, 11, 96, N'LA TINGUIÑA', N'100102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (787, 10, 68, N'LA UNION', N'090301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (788, 12, 179, N'LA UNION', N'110505')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (789, 20, 146, N'LA UNION', N'190106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (790, 14, 48, N'LA VICTORIA', N'130115')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (791, 15, 112, N'LA VICTORIA', N'140109')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (792, 17, 177, N'LABERINTO', N'160104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (793, 2, 136, N'LACABAMBA', N'021006')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (794, 15, 34, N'LACHAQUI', N'140305')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (795, 14, 48, N'LAGUNAS', N'130105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (796, 16, 5, N'LAGUNAS', N'150206')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (797, 20, 16, N'LAGUNAS', N'190203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (798, 15, 91, N'LAHUAYTAMBO', N'140610')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (799, 6, 51, N'LAJAS', N'060609')
GO
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (800, 20, 81, N'LALAQUIZ', N'190308')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (801, 22, 107, N'LAMAS', N'210301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (802, 7, 28, N'LAMAY', N'070403')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (803, 14, 108, N'LAMBAYEQUE', N'130301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (804, 3, 1, N'LAMBRAMA', N'030106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (805, 5, 141, N'LAMPA', N'051004')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (806, 21, 109, N'LAMPA', N'200601')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (807, 15, 88, N'LAMPIAN', N'140807')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (808, 1, 115, N'LAMUD', N'010401')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (809, 20, 173, N'LANCONES', N'190603')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (810, 15, 91, N'LANGA', N'140611')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (811, 7, 30, N'LANGUI', N'070504')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (812, 9, 94, N'LARAMARCA', N'080605')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (813, 5, 114, N'LARAMATE', N'050511')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (814, 15, 91, N'LARAOS', N'140631')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (815, 15, 192, N'LARAOS', N'140716')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (816, 13, 181, N'LAREDO', N'120103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (817, 7, 28, N'LARES', N'070404')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (818, 4, 43, N'LARI', N'040210')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (819, 9, 84, N'LARIA', N'080110')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (820, 16, 122, N'LAS AMAZONAS', N'150104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (821, 20, 146, N'LAS LOMAS', N'190107')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (822, 17, 177, N'LAS PIEDRAS', N'160103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (823, 6, 98, N'LAS PIRIAS', N'060811')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (824, 7, 30, N'LAYO', N'070505')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (825, 1, 45, N'LEIMEBAMBA', N'010110')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (826, 5, 114, N'LEONCIO PRADO', N'050512')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (827, 15, 93, N'LEONCIO PRADO', N'140508')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (828, 12, 100, N'LEONOR ORDOÑEZ', N'110312')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (829, 1, 45, N'LEVANTO', N'010111')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (830, 15, 112, N'LIMA', N'140101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (831, 1, 155, N'LIMABAMBA', N'010505')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (832, 7, 9, N'LIMATAMBO', N'070304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (833, 21, 165, N'LIMBANI', N'200804')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (834, 15, 112, N'LINCE', N'140111')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (835, 15, 192, N'LINCHA', N'140717')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (836, 9, 8, N'LIRCAY', N'080301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (837, 7, 53, N'LIVITACA', N'070705')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (838, 6, 26, N'LLACANORA', N'060108')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (839, 2, 153, N'LLACLLIN', N'021209')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (840, 21, 123, N'LLALLI', N'200704')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (841, 2, 119, N'LLAMA', N'020905')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (842, 6, 51, N'LLAMA', N'060610')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (843, 2, 11, N'LLAMELLIN', N'021601')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (844, 6, 161, N'LLAPA', N'061004')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (845, 2, 136, N'LLAPO', N'021007')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (846, 10, 78, N'LLATA', N'090401')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (847, 5, 114, N'LLAUTA', N'050514')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (848, 12, 169, N'LLAYLLA', N'110703')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (849, 2, 128, N'LLIPA', N'022006')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (850, 11, 137, N'LLIPATA', N'100502')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (851, 5, 86, N'LLOCHEGUA', N'050309')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (852, 12, 100, N'LLOCLLAPAMPA', N'110313')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (853, 18, 72, N'LLOQUE', N'170206')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (854, 2, 119, N'LLUMPA', N'020906')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (855, 7, 53, N'LLUSCO', N'070706')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (856, 4, 43, N'LLUTA', N'040211')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (857, 20, 176, N'LOBITOS', N'190704')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (858, 9, 55, N'LOCROJA', N'080706')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (859, 23, 101, N'LOCUMBA', N'220301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (860, 4, 36, N'LOMAS', N'040411')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (861, 1, 155, N'LONGAR', N'010506')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (862, 13, 21, N'LONGOTEA', N'120204')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (863, 1, 115, N'LONGUITA', N'010407')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (864, 1, 115, N'LONYA CHICO', N'010408')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (865, 1, 186, N'LONYA GRANDE', N'010706')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (866, 11, 96, N'LOS AQUIJES', N'100103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (867, 6, 26, N'LOS BAÑOS DEL INCA', N'060107')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (868, 5, 33, N'LOS MOROCHUCOS', N'050206')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (869, 15, 112, N'LOS OLIVOS', N'140142')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (870, 20, 176, N'LOS ORGANOS', N'190706')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (871, 5, 114, N'LUCANAS', N'050513')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (872, 2, 119, N'LUCMA', N'020903')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (873, 13, 73, N'LUCMA', N'121102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (874, 3, 17, N'LUCRE', N'030208')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (875, 7, 152, N'LUCRE', N'071208')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (876, 5, 105, N'LUIS CARRANZA', N'050407')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (877, 15, 35, N'LUNAHUANA', N'140407')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (878, 5, 86, N'LURICOCHA', N'050305')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (879, 15, 112, N'LURIGANCHO', N'140112')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (880, 15, 112, N'LURIN', N'140113')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (881, 1, 115, N'LUYA', N'010409')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (882, 1, 115, N'LUYA VIEJO', N'010410')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (883, 10, 111, N'LUYANDO', N'090604')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (884, 4, 43, N'MACA', N'040212')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (885, 21, 123, N'MACARI', N'200705')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (886, 2, 166, N'MACATE', N'021303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (887, 4, 41, N'MACHAGUAY', N'040508')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (888, 13, 129, N'MACHE', N'120413')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (889, 7, 185, N'MACHUPICCHU', N'071304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (890, 21, 36, N'MACUSANI', N'200301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (891, 15, 192, N'MADEAN', N'140731')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (892, 17, 116, N'MADRE DE DIOS', N'160203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (893, 4, 43, N'MADRIGAL', N'040213')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (894, 1, 45, N'MAGDALENA', N'010112')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (895, 6, 26, N'MAGDALENA', N'060109')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (896, 13, 13, N'MAGDALENA DE CAO', N'120805')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (897, 15, 112, N'MAGDALENA DEL MAR', N'140114')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (898, 4, 43, N'MAJES', N'040220')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (899, 15, 35, N'MALA', N'140408')
GO
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (900, 2, 91, N'MALVAS', N'021904')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (901, 3, 75, N'MAMARA', N'030604')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (902, 25, 61, N'MANANTAY', N'250107')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (903, 15, 27, N'MANAS', N'140208')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (904, 20, 176, N'MANCORA', N'190705')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (905, 2, 193, N'MANCOS', N'021503')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (906, 2, 23, N'MANGAS', N'020313')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (907, 16, 66, N'MANSERICHE', N'150704')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (908, 9, 84, N'MANTA', N'080111')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (909, 17, 116, N'MANU', N'160201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (910, 14, 72, N'MANUEL ANTONIO MESONES MURO', N'130206')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (911, 12, 55, N'MANZANARES', N'110208')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (912, 21, 150, N'MAÑAZO', N'200108')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (913, 16, 154, N'MAQUIA', N'150405')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (914, 3, 63, N'MARA', N'030505')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (915, 7, 31, N'MARANGANI', N'070604')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (916, 7, 103, N'MARANURA', N'070904')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (917, 7, 185, N'MARAS', N'071305')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (918, 2, 153, N'MARCA', N'021204')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (919, 13, 163, N'MARCABAL', N'120305')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (920, 5, 141, N'MARCABAMBA', N'051005')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (921, 7, 152, N'MARCAPATA', N'071209')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (922, 12, 191, N'MARCAPOMACOCHA', N'110604')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (923, 2, 38, N'MARCARA', N'020406')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (924, 9, 2, N'MARCAS', N'080205')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (925, 20, 173, N'MARCAVELICA', N'190604')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (926, 12, 100, N'MARCO', N'110314')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (927, 11, 127, N'MARCONA', N'100304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (928, 10, 86, N'MARGOS', N'090104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (929, 1, 115, N'MARIA', N'010411')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (930, 5, 33, N'MARIA PARADO DE BELLIDO', N'050211')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (931, 10, 111, N'MARIANO DAMASO BERAUN', N'090605')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (932, 4, 12, N'MARIANO MELGAR', N'040126')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (933, 4, 28, N'MARIANO NICOLAS VALCARCEL', N'040303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (934, 10, 68, N'MARIAS', N'090312')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (935, 15, 91, N'MARIATANA', N'140612')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (936, 1, 155, N'MARISCAL BENAVIDES', N'010508')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (937, 4, 28, N'MARISCAL CACERES', N'040304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (938, 9, 84, N'MARISCAL CACERES', N'080112')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (939, 1, 45, N'MARISCAL CASTILLA', N'010113')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (940, 12, 55, N'MARISCAL CASTILLA', N'110209')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (941, 3, 75, N'MARISCAL GAMARRA', N'030605')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (942, 13, 73, N'MARMOT', N'121103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (943, 2, 90, N'MASIN', N'020808')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (944, 25, 61, N'MASISEA', N'250103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (945, 12, 100, N'MASMA', N'110315')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (946, 12, 100, N'MASMA CHICCHE', N'110332')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (947, 2, 193, N'MATACOTO', N'021504')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (948, 12, 55, N'MATAHUASI', N'110210')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (949, 18, 72, N'MATALAQUE', N'170207')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (950, 24, 195, N'MATAPALO', N'230302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (951, 6, 26, N'MATARA', N'060110')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (952, 2, 94, N'MATO', N'020705')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (953, 15, 91, N'MATUCANA', N'140601')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (954, 12, 169, N'MAZAMARI', N'110704')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (955, 16, 122, N'MAZAN', N'150105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (956, 4, 98, N'MEJIA', N'040705')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (957, 3, 75, N'MICAELA BASTIDAS', N'030606')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (958, 20, 173, N'MIGUEL CHECA', N'190605')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (959, 6, 43, N'MIGUEL IGLESIAS', N'060307')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (960, 1, 155, N'MILPUCC', N'010507')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (961, 6, 51, N'MIRACOSTA', N'060611')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (962, 4, 12, N'MIRAFLORES', N'040107')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (963, 10, 78, N'MIRAFLORES', N'090406')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (964, 15, 112, N'MIRAFLORES', N'140115')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (965, 15, 192, N'MIRAFLORES', N'140718')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (966, 2, 11, N'MIRGAS', N'021605')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (967, 12, 55, N'MITO', N'110211')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (968, 13, 181, N'MOCHE', N'120104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (969, 14, 108, N'MOCHUMI', N'130305')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (970, 21, 124, N'MOHO', N'201301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (971, 10, 133, N'MOLINO', N'090704')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (972, 1, 45, N'MOLINOPAMPA', N'010114')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (973, 12, 100, N'MOLINOS', N'110316')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (974, 13, 168, N'MOLLEBAMBA', N'120703')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (975, 4, 12, N'MOLLEBAYA', N'040108')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (976, 4, 98, N'MOLLENDO', N'040701')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (977, 9, 42, N'MOLLEPAMPA', N'080414')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (978, 7, 9, N'MOLLEPATA', N'070305')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (979, 13, 168, N'MOLLEPATA', N'120704')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (980, 12, 100, N'MONOBAMBA', N'110317')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (981, 14, 48, N'MONSEFU', N'130106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (982, 20, 16, N'MONTERO', N'190204')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (983, 1, 45, N'MONTEVIDEO', N'010115')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (984, 10, 78, N'MONZON', N'090407')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (985, 18, 120, N'MOQUEGUA', N'170101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (986, 22, 159, N'MORALES', N'210611')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (987, 5, 172, N'MORCOLLA', N'051111')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (988, 2, 166, N'MORO', N'021304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (989, 12, 191, N'MOROCOCHA', N'110605')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (990, 16, 66, N'MORONA', N'150705')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (991, 14, 108, N'MORROPE', N'130306')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (992, 20, 124, N'MORROPON', N'190404')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (993, 7, 3, N'MOSOC LLACTA', N'070207')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (994, 14, 108, N'MOTUPE', N'130307')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (995, 9, 84, N'MOYA', N'080113')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (996, 22, 126, N'MOYOBAMBA', N'210101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (997, 21, 17, N'MUÑANI', N'200208')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (998, 12, 100, N'MUQUI', N'110318')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (999, 12, 100, N'MUQUIYAUYO', N'110319')
GO
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1000, 2, 119, N'MUSGA', N'020907')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1001, 6, 158, N'NAMBALLE', N'061104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1002, 6, 26, N'NAMORA', N'060111')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1003, 6, 161, N'NANCHOC', N'061005')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1004, 16, 122, N'NAPO', N'150106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1005, 16, 113, N'NAUTA', N'150301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1006, 15, 130, N'NAVAN', N'141002')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1007, 11, 127, N'NAZCA', N'100301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1008, 2, 166, N'NEPEÑA', N'021305')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1009, 21, 109, N'NICASIO', N'200604')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1010, 4, 28, N'NICOLAS DE PIEROLA', N'040305')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1011, 6, 161, N'NIEPOS', N'061006')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1012, 1, 58, N'NIEVA', N'010601')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1013, 6, 167, N'NINABAMBA', N'060905')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1014, 19, 140, N'NINACACA', N'180106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1015, 14, 48, N'NUEVA ARICA', N'130107')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1016, 22, 155, N'NUEVA CAJAMARCA', N'210505')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1017, 25, 61, N'NUEVA REQUENA', N'250106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1018, 12, 55, N'NUEVE DE JULIO', N'110212')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1019, 2, 166, N'NUEVO CHIMBOTE', N'021309')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1020, 15, 35, N'NUEVO IMPERIAL', N'140409')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1021, 9, 84, N'NUEVO OCCORO', N'080114')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1022, 22, 181, N'NUEVO PROGRESO', N'210802')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1023, 21, 123, N'NUÑOA', N'200706')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1024, 9, 180, N'ÑAHUIMPUQUIO', N'080515')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1025, 10, 190, N'OBAS', N'091106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1026, 1, 115, N'OCALLI', N'010412')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1027, 5, 114, N'OCAÑA', N'050516')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1028, 3, 50, N'OCOBAMBA', N'030703')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1029, 7, 103, N'OCOBAMBA', N'070905')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1030, 7, 152, N'OCONGATE', N'071210')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1031, 4, 28, N'OCOÑA', N'040306')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1032, 7, 71, N'OCORURO', N'070804')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1033, 9, 94, N'OCOYO', N'080606')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1034, 2, 128, N'OCROS', N'022007')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1035, 5, 80, N'OCROS', N'050113')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1036, 11, 96, N'OCUCAJE', N'100114')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1037, 1, 115, N'OCUMAL', N'010413')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1038, 21, 109, N'OCUVIRI', N'200605')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1039, 21, 36, N'OLLACHEA', N'200308')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1040, 7, 185, N'OLLANTAYTAMBO', N'071306')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1041, 21, 194, N'OLLARAYA', N'201006')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1042, 1, 45, N'OLLEROS', N'010116')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1043, 2, 89, N'OLLEROS', N'020108')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1044, 14, 108, N'OLMOS', N'130308')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1045, 7, 139, N'OMACHA', N'071006')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1046, 15, 192, N'OMAS', N'140719')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1047, 18, 72, N'OMATE', N'170201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1048, 1, 155, N'OMIA', N'010509')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1049, 12, 102, N'ONDORES', N'110403')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1050, 13, 141, N'ONGON', N'120607')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1051, 3, 50, N'ONGOY', N'030702')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1052, 4, 41, N'ORCOPAMPA', N'040509')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1053, 12, 55, N'ORCOTUNA', N'110213')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1054, 3, 10, N'OROPESA', N'030405')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1055, 7, 152, N'OROPESA', N'071211')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1056, 21, 123, N'ORURILLO', N'200707')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1057, 5, 114, N'OTOCA', N'050517')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1058, 13, 129, N'OTUZCO', N'120401')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1059, 6, 43, N'OXAMARCA', N'060308')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1060, 19, 130, N'OXAPAMPA', N'180301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1061, 5, 141, N'OYOLO', N'051006')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1062, 15, 130, N'OYON', N'141001')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1063, 14, 48, N'OYOTUN', N'130108')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1064, 12, 100, N'PACA', N'110320')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1065, 20, 16, N'PACAIPAMPA', N'190205')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1066, 13, 46, N'PACANGA', N'120902')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1067, 5, 138, N'PACAPAUSA', N'050608')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1068, 15, 35, N'PACARAN', N'140410')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1069, 15, 88, N'PACARAOS', N'140808')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1070, 13, 132, N'PACASMAYO', N'120506')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1071, 5, 80, N'PACAYCASA', N'050114')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1072, 7, 139, N'PACCARITAMBO', N'071008')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1073, 6, 51, N'PACCHA', N'060612')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1074, 12, 100, N'PACCHA', N'110321')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1075, 12, 191, N'PACCHA', N'110606')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1076, 15, 93, N'PACCHO', N'140509')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1077, 15, 112, N'PACHACAMAC', N'140116')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1078, 3, 10, N'PACHACONAS', N'030406')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1079, 11, 96, N'PACHACUTEC', N'100113')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1080, 9, 55, N'PACHAMARCA', N'080710')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1081, 15, 130, N'PACHANGARA', N'141005')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1082, 10, 68, N'PACHAS', N'090314')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1083, 23, 174, N'PACHIA', N'220107')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1084, 22, 117, N'PACHIZA', N'210404')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1085, 2, 23, N'PACLLON', N'020315')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1086, 3, 7, N'PACOBAMBA', N'030307')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1087, 18, 97, N'PACOCHA', N'170303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1088, 14, 108, N'PACORA', N'130309')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1089, 3, 7, N'PACUCHA', N'030313')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1090, 25, 134, N'PADRE ABAD', N'250201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1091, 16, 184, N'PADRE MARQUEZ', N'150503')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1092, 5, 172, N'PAICO', N'051105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1093, 13, 13, N'PAIJAN', N'120806')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1094, 20, 16, N'PAIMAS', N'190210')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1095, 20, 135, N'PAITA', N'190501')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1096, 22, 117, N'PAJARILLO', N'210405')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1097, 9, 84, N'PALCA', N'080115')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1098, 12, 179, N'PALCA', N'110506')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1099, 21, 109, N'PALCA', N'200606')
GO
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1100, 23, 174, N'PALCA', N'220108')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1101, 12, 179, N'PALCAMAYO', N'110507')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1102, 19, 130, N'PALCAZU', N'180307')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1103, 19, 140, N'PALLANCHACRA', N'180107')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1104, 2, 136, N'PALLASCA', N'021008')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1105, 7, 71, N'PALLPATA', N'070805')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1106, 11, 137, N'PALPA', N'100501')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1107, 12, 169, N'PAMPA HERMOSA', N'110705')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1108, 16, 184, N'PAMPA HERMOSA', N'150504')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1109, 3, 7, N'PAMPACHIRI', N'030308')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1110, 4, 41, N'PAMPACOLCA', N'040510')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1111, 4, 105, N'PAMPAMARCA', N'040805')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1112, 7, 30, N'PAMPAMARCA', N'070506')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1113, 10, 190, N'PAMPAMARCA', N'091107')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1114, 2, 94, N'PAMPAROMAS', N'020706')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1115, 2, 136, N'PAMPAS', N'021009')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1116, 9, 180, N'PAMPAS', N'080501')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1117, 2, 153, N'PAMPAS CHICO', N'021205')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1118, 24, 183, N'PAMPAS DE HOSPITAL', N'230104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1119, 2, 89, N'PAMPAS GRANDE', N'020109')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1120, 10, 133, N'PANAO', N'090701')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1121, 12, 100, N'PANCAN', N'110322')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1122, 12, 169, N'PANGOA', N'110706')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1123, 22, 159, N'PAPAPLAYA', N'210612')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1124, 24, 195, N'PAPAYAL', N'230303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1125, 11, 145, N'PARACAS', N'100405')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1126, 15, 20, N'PARAMONGA', N'140902')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1127, 13, 129, N'PARANDAY', N'120408')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1128, 5, 141, N'PARARCA', N'051007')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1129, 2, 153, N'PARARIN', N'021206')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1130, 5, 33, N'PARAS', N'050207')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1131, 21, 109, N'PARATIA', N'200607')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1132, 12, 100, N'PARCO', N'110323')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1133, 11, 96, N'PARCONA', N'100104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1134, 13, 141, N'PARCOY', N'120608')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1135, 22, 155, N'PARDO MIGUEL', N'210508')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1136, 2, 89, N'PARIACOTO', N'020110')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1137, 2, 38, N'PARIAHUANCA', N'020407')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1138, 12, 85, N'PARIAHUANCA', N'110122')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1139, 16, 113, N'PARINARI', N'150302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1140, 20, 176, N'PARIÑAS', N'190701')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1141, 2, 147, N'PAROBAMBA', N'021103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1142, 7, 139, N'PARURO', N'071001')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1143, 16, 66, N'PASTAZA', N'150706')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1144, 21, 165, N'PATAMBUCO', N'200806')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1145, 14, 48, N'PATAPO', N'130117')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1146, 3, 75, N'PATAYPAMPA', N'030608')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1147, 13, 141, N'PATAZ', N'120609')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1148, 15, 20, N'PATIVILCA', N'140903')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1149, 19, 65, N'PAUCAR', N'180204')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1150, 9, 2, N'PAUCARA', N'080206')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1151, 9, 55, N'PAUCARBAMBA', N'080707')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1152, 21, 150, N'PAUCARCOLLA', N'200109')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1153, 4, 12, N'PAUCARPATA', N'040109')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1154, 7, 143, N'PAUCARTAMBO', N'071101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1155, 19, 140, N'PAUCARTAMBO', N'180108')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1156, 2, 90, N'PAUCAS', N'020809')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1157, 5, 141, N'PAUSA', N'051001')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1158, 9, 180, N'PAZOS', N'080517')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1159, 16, 120, N'PEBAS', N'150602')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1160, 6, 159, N'PEDRO GALVEZ', N'061201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1161, 21, 157, N'PEDRO VILCA APAZA', N'201102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1162, 12, 46, N'PERENE', N'110806')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1163, 21, 165, N'PHARA', N'200805')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1164, 13, 141, N'PIAS', N'120610')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1165, 21, 150, N'PICHACANI', N'200110')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1166, 12, 46, N'PICHANAQUI', N'110805')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1167, 7, 103, N'PICHARI', N'070910')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1168, 7, 71, N'PICHIGUA', N'070806')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1169, 3, 1, N'PICHIRHUA', N'030107')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1170, 22, 144, N'PICOTA', N'210901')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1171, 14, 48, N'PICSI', N'130109')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1172, 9, 84, N'PILCHACA', N'080116')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1173, 12, 85, N'PILCOMAYO', N'110123')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1174, 21, 69, N'PILCUYO', N'201202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1175, 10, 86, N'PILLCO MARCA', N'090111')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1176, 7, 139, N'PILLPINTO', N'071009')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1177, 22, 144, N'PILLUANA', N'210904')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1178, 9, 94, N'PILPICHACA', N'080607')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1179, 14, 48, N'PIMENTEL', N'130110')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1180, 6, 64, N'PIMPINGOS', N'060506')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1181, 10, 76, N'PINRA', N'090902')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1182, 22, 107, N'PINTO RECODO', N'210306')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1183, 6, 51, N'PION', N'060613')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1184, 2, 89, N'PIRA', N'020111')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1185, 7, 28, N'PISAC', N'070405')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1186, 21, 52, N'PISACOMA', N'200406')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1187, 11, 145, N'PISCO', N'100401')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1188, 2, 119, N'PISCOBAMBA', N'020901')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1189, 22, 78, N'PISCOYACU', N'210202')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1190, 1, 115, N'PISUQUIA', N'010414')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1191, 14, 72, N'PITIPO', N'130204')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1192, 7, 31, N'PITUMARCA', N'070605')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1193, 20, 146, N'PIURA', N'190101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1194, 21, 150, N'PLATERIA', N'200114')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1195, 3, 17, N'POCOHUANCA', N'030209')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1196, 23, 174, N'POCOLLAY', N'220109')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1197, 4, 12, N'POCSI', N'040110')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1198, 4, 12, N'POLOBAYA', N'040111')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1199, 22, 181, N'POLVORA', N'210803')
GO
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1200, 2, 147, N'POMABAMBA', N'021101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1201, 12, 100, N'POMACANCHA', N'110324')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1202, 7, 3, N'POMACANCHI', N'070204')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1203, 3, 7, N'POMACOCHA', N'030314')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1204, 9, 2, N'POMACOCHA', N'080207')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1205, 6, 98, N'POMAHUACA', N'060805')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1206, 14, 48, N'POMALCA', N'130118')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1207, 21, 52, N'POMATA', N'200407')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1208, 2, 90, N'PONTO', N'020810')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1209, 13, 181, N'POROTO', N'120109')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1210, 7, 63, N'POROY', N'070103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1211, 22, 155, N'POSIC', N'210502')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1212, 21, 17, N'POTONI', N'200210')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1213, 19, 130, N'POZUZO', N'180306')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1214, 3, 75, N'PROGRESO', N'030607')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1215, 1, 115, N'PROVIDENCIA', N'010423')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1216, 22, 144, N'PUCACACA', N'210905')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1217, 14, 48, N'PUCALA', N'130119')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1218, 6, 98, N'PUCARA', N'060806')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1219, 12, 85, N'PUCARA', N'110124')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1220, 21, 109, N'PUCARA', N'200608')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1221, 15, 112, N'PUCUSANA', N'140118')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1222, 7, 9, N'PUCYURA', N'070306')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1223, 2, 94, N'PUEBLO LIBRE', N'020707')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1224, 15, 112, N'PUEBLO LIBRE', N'140117')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1225, 11, 96, N'PUEBLO NUEVO', N'100105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1226, 11, 49, N'PUEBLO NUEVO', N'100210')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1227, 13, 46, N'PUEBLO NUEVO', N'120903')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1228, 14, 72, N'PUEBLO NUEVO', N'130205')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1229, 15, 112, N'PUENTE PIEDRA', N'140119')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1230, 19, 130, N'PUERTO BERMUDEZ', N'180304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1231, 10, 149, N'PUERTO INCA', N'090802')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1232, 16, 154, N'PUINAHUA', N'150406')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1233, 6, 167, N'PULAN', N'060906')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1234, 5, 138, N'PULLO', N'050611')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1235, 16, 122, N'PUNCHANA', N'150111')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1236, 10, 78, N'PUNCHAO', N'090408')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1237, 21, 150, N'PUNO', N'200101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1238, 4, 98, N'PUNTA DE BOMBON', N'040706')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1239, 15, 112, N'PUNTA HERMOSA', N'140120')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1240, 15, 112, N'PUNTA NEGRA', N'140121')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1241, 10, 78, N'PUÑOS', N'090409')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1242, 18, 72, N'PUQUINA', N'170208')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1243, 5, 114, N'PUQUIO', N'050501')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1244, 25, 150, N'PURUS', N'250401')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1245, 21, 81, N'PUSI', N'200506')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1246, 21, 157, N'PUTINA', N'201101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1247, 15, 192, N'PUTINZA', N'140732')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1248, 16, 122, N'PUTUMAYO', N'150107')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1249, 4, 105, N'PUYCA', N'040806')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1250, 5, 138, N'PUYUSCA', N'050612')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1251, 4, 105, N'QUECHUALLA', N'040807')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1252, 7, 30, N'QUEHUE', N'070507')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1253, 7, 103, N'QUELLOUNO', N'070908')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1254, 4, 12, N'QUEQUEÑA', N'040112')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1255, 9, 94, N'QUERCO', N'080608')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1256, 20, 173, N'QUERECOTILLO', N'190606')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1257, 5, 172, N'QUEROBAMBA', N'051101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1258, 6, 64, N'QUEROCOTILLO', N'060507')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1259, 6, 51, N'QUEROCOTO', N'060614')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1260, 10, 110, N'QUEROPALCA', N'091004')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1261, 21, 165, N'QUIACA', N'200807')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1262, 4, 36, N'QUICACHA', N'040412')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1263, 2, 171, N'QUICHES', N'021405')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1264, 12, 85, N'QUICHUAY', N'110125')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1265, 23, 32, N'QUILAHUANI', N'220405')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1266, 4, 28, N'QUILCA', N'040307')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1267, 21, 157, N'QUILCAPUNCU', N'201103')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1268, 12, 85, N'QUILCAS', N'110126')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1269, 2, 193, N'QUILLO', N'021505')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1270, 15, 35, N'QUILMANA', N'140411')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1271, 15, 192, N'QUINCHES', N'140720')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1272, 18, 72, N'QUINISTAQUILLAS', N'170209')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1273, 1, 45, N'QUINJALCA', N'010117')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1274, 15, 192, N'QUINOCAY', N'140721')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1275, 5, 80, N'QUINUA', N'050105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1276, 2, 147, N'QUINUABAMBA', N'021104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1277, 7, 53, N'QUIÑOTA', N'070707')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1278, 7, 152, N'QUIQUIJANA', N'071212')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1279, 13, 168, N'QUIRUVILCA', N'120705')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1280, 9, 180, N'QUISHUAR', N'080518')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1281, 10, 86, N'QUISQUI', N'090105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1282, 9, 94, N'QUITO ARMA', N'080609')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1283, 10, 68, N'QUIVILLA', N'090316')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1284, 2, 171, N'RAGASH', N'021409')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1285, 2, 90, N'RAHUAPAMPA', N'020811')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1286, 25, 15, N'RAIMONDI', N'250301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1287, 16, 120, N'RAMON CASTILLA', N'150601')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1288, 3, 50, N'RANRACANCHA', N'030708')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1289, 2, 193, N'RANRAHIRCA', N'021506')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1290, 2, 90, N'RAPAYAN', N'020812')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1291, 13, 13, N'RAZURI', N'120807')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1292, 1, 23, N'RECTA', N'010307')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1293, 2, 153, N'RECUAY', N'021201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1294, 14, 48, N'REQUE', N'130111')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1295, 16, 154, N'REQUENA', N'150401')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1296, 15, 91, N'RICARDO PALMA', N'140613')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1297, 12, 100, N'RICRAN', N'110325')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1298, 15, 112, N'RIMAC', N'140122')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1299, 20, 170, N'RINCONADA-LLICUAR', N'190806')
GO
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1300, 4, 57, N'RIO GRANDE', N'040608')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1301, 11, 137, N'RIO GRANDE', N'100503')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1302, 12, 169, N'RIO NEGRO', N'110707')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1303, 1, 58, N'RIO SANTIAGO', N'010602')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1304, 12, 169, N'RIO TAMBO', N'110708')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1305, 22, 155, N'RIOJA', N'210501')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1306, 10, 68, N'RIPAN', N'090317')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1307, 7, 3, N'RONDOCAN', N'070205')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1308, 10, 110, N'RONDOS', N'091006')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1309, 9, 2, N'ROSARIO', N'080208')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1310, 21, 81, N'ROSASPATA', N'200507')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1311, 22, 107, N'RUMISAPA', N'210307')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1312, 10, 111, N'RUPA-RUPA', N'090601')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1313, 3, 10, N'SABAINO', N'030407')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1314, 4, 12, N'SABANDIA', N'040113')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1315, 22, 78, N'SACANCHE', N'210203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1316, 4, 12, N'SACHACA', N'040114')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1317, 5, 80, N'SACSAMARCA', N'050802')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1318, 5, 114, N'SAISA', N'050529')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1319, 4, 57, N'SALAMANCA', N'040606')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1320, 11, 96, N'SALAS', N'100106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1321, 14, 108, N'SALAS', N'130310')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1322, 13, 181, N'SALAVERRY', N'120105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1323, 9, 180, N'SALCABAMBA', N'080519')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1324, 9, 180, N'SALCAHUASI', N'080526')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1325, 20, 124, N'SALITRAL', N'190405')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1326, 20, 173, N'SALITRAL', N'190607')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1327, 6, 98, N'SALLIQUE', N'060807')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1328, 13, 129, N'SALPO', N'120409')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1329, 23, 174, N'SAMA', N'220110')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1330, 21, 17, N'SAMAN', N'200212')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1331, 2, 166, N'SAMANCO', N'021306')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1332, 18, 120, N'SAMEGUA', N'170106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1333, 4, 28, N'SAMUEL PASTOR', N'040308')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1334, 5, 105, N'SAMUGARI', N'050409')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1335, 12, 85, N'SAN AGUSTIN', N'110127')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1336, 11, 145, N'SAN ANDRES', N'100406')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1337, 6, 64, N'SAN ANDRES DE CUTERVO', N'060508')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1338, 15, 91, N'SAN ANDRES DE TUPICOCHA', N'140614')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1339, 21, 17, N'SAN ANTON', N'200213')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1340, 3, 75, N'SAN ANTONIO', N'030609')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1341, 15, 35, N'SAN ANTONIO', N'140412')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1342, 15, 91, N'SAN ANTONIO', N'140615')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1343, 21, 150, N'SAN ANTONIO', N'200111')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1344, 22, 159, N'SAN ANTONIO', N'210616')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1345, 9, 8, N'SAN ANTONIO DE ANTAPARCO', N'080309')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1346, 3, 7, N'SAN ANTONIO DE CACHI', N'030309')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1347, 4, 43, N'SAN ANTONIO DE CHUCA', N'040214')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1348, 9, 94, N'SAN ANTONIO DE CUSICANCHA', N'080610')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1349, 15, 112, N'SAN BARTOLO', N'140123')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1350, 15, 91, N'SAN BARTOLOME', N'140616')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1351, 6, 59, N'SAN BENITO', N'060405')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1352, 6, 162, N'SAN BERNARDINO', N'061302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1353, 15, 112, N'SAN BORJA', N'140140')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1354, 10, 116, N'SAN BUENAVENTURA', N'090505')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1355, 15, 34, N'SAN BUENAVENTURA', N'140306')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1356, 1, 23, N'SAN CARLOS', N'010308')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1357, 11, 145, N'SAN CLEMENTE', N'100407')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1358, 1, 115, N'SAN CRISTOBAL', N'010415')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1359, 5, 114, N'SAN CRISTOBAL', N'050532')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1360, 18, 120, N'SAN CRISTOBAL', N'170104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1361, 22, 144, N'SAN CRISTOBAL', N'210906')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1362, 2, 128, N'SAN CRISTOBAL DE RAJAN', N'022008')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1363, 15, 91, N'SAN DAMIAN', N'140617')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1364, 19, 140, N'SAN FCO DE ASIS DE YARUSYACAN', N'180109')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1365, 6, 98, N'SAN FELIPE', N'060808')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1366, 22, 155, N'SAN FERNANDO', N'210507')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1367, 10, 6, N'SAN FRANCISCO', N'090206')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1368, 10, 110, N'SAN FRANCISCO DE ASIS', N'091003')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1369, 10, 86, N'SAN FRANCISCO DE CAYRAN', N'090106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1370, 1, 45, N'SAN FRANCISCO DE DAGUAS', N'010118')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1371, 5, 138, N'SAN FRANCISCO DE RAVACAYCO', N'050615')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1372, 9, 94, N'SAN FRANCISCO DE SANGAYAICO', N'080611')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1373, 1, 115, N'SAN FRANCISCO DE YESO', N'010416')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1374, 21, 36, N'SAN GABAN', N'200309')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1375, 6, 161, N'SAN GREGORIO', N'061007')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1376, 22, 144, N'SAN HILARION', N'210907')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1377, 6, 158, N'SAN IGNACIO', N'061101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1378, 9, 94, N'SAN ISIDRO', N'080612')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1379, 15, 112, N'SAN ISIDRO', N'140124')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1380, 1, 45, N'SAN ISIDRO DE MAINO', N'010119')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1381, 24, 183, N'SAN JACINTO', N'230105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1382, 5, 141, N'SAN JAVIER DE ALPABAMBA', N'051008')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1383, 1, 115, N'SAN JERONIMO', N'010417')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1384, 3, 7, N'SAN JERONIMO', N'030310')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1385, 7, 63, N'SAN JERONIMO', N'070104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1386, 12, 85, N'SAN JERONIMO DE TUNAN', N'110128')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1387, 15, 192, N'SAN JOAQUIN', N'140722')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1388, 13, 132, N'SAN JOSE', N'120508')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1389, 14, 108, N'SAN JOSE', N'130311')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1390, 21, 17, N'SAN JOSE', N'200214')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1391, 15, 91, N'SAN JOSE DE LOS CHORRILLOS', N'140606')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1392, 11, 96, N'SAN JOSE DE LOS MOLINOS', N'100107')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1393, 6, 158, N'SAN JOSE DE LOURDES', N'061106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1394, 12, 55, N'SAN JOSE DE QUERO', N'110215')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1395, 22, 69, N'SAN JOSE DE SISA', N'211001')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1396, 5, 80, N'SAN JOSE DE TICLLAS', N'050106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1397, 5, 141, N'SAN JOSE DE USHUA', N'051009')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1398, 6, 98, N'SAN JOSE DEL ALTO', N'060809')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1399, 2, 171, N'SAN JUAN', N'021410')
GO
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1400, 5, 114, N'SAN JUAN', N'050521')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1401, 6, 26, N'SAN JUAN', N'060112')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1402, 9, 42, N'SAN JUAN', N'080422')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1403, 5, 80, N'SAN JUAN BAUTISTA', N'050107')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1404, 11, 96, N'SAN JUAN BAUTISTA', N'100108')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1405, 16, 122, N'SAN JUAN BAUTISTA', N'150113')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1406, 20, 124, N'SAN JUAN DE BIGOTE', N'190410')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1407, 3, 17, N'SAN JUAN DE CHACÑA', N'030216')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1408, 6, 64, N'SAN JUAN DE CUTERVO', N'060509')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1409, 15, 91, N'SAN JUAN DE IRIS', N'140632')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1410, 12, 54, N'SAN JUAN DE JARPA', N'110907')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1411, 24, 183, N'SAN JUAN DE LA VIRGEN', N'230106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1412, 6, 51, N'SAN JUAN DE LICUPIS', N'060617')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1413, 1, 115, N'SAN JUAN DE LOPECANCHA', N'010418')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1414, 15, 112, N'SAN JUAN DE LURIGANCHO', N'140137')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1415, 15, 112, N'SAN JUAN DE MIRAFLORES', N'140136')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1416, 2, 11, N'SAN JUAN DE RONTOY', N'021606')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1417, 21, 17, N'SAN JUAN DE SALINAS', N'200215')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1418, 4, 12, N'SAN JUAN DE SIGUAS', N'040115')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1419, 15, 91, N'SAN JUAN DE TANTARANCHE', N'140619')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1420, 4, 12, N'SAN JUAN DE TARUCANI', N'040116')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1421, 11, 49, N'SAN JUAN DE YANAC', N'100211')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1422, 12, 54, N'SAN JUAN DE YSCOS', N'110906')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1423, 21, 165, N'SAN JUAN DEL ORO', N'200808')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1424, 12, 100, N'SAN LORENZO', N'110326')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1425, 15, 91, N'SAN LORENZO DE QUINTI', N'140620')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1426, 2, 38, N'SAN LUIS', N'021701')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1427, 6, 162, N'SAN LUIS', N'061303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1428, 15, 112, N'SAN LUIS', N'140138')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1429, 15, 35, N'SAN LUIS', N'140413')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1430, 6, 64, N'SAN LUIS DE LUCMA', N'060510')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1431, 12, 46, N'SAN LUIS DE SHUARO', N'110804')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1432, 2, 90, N'SAN MARCOS', N'020813')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1433, 9, 180, N'SAN MARCOS DE ROCCHAC', N'080520')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1434, 22, 69, N'SAN MARTIN', N'211004')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1435, 15, 112, N'SAN MARTIN DE PORRES', N'140126')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1436, 15, 91, N'SAN MATEO', N'140621')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1437, 15, 91, N'SAN MATEO DE OTAO', N'140622')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1438, 5, 105, N'SAN MIGUEL', N'050401')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1439, 6, 161, N'SAN MIGUEL', N'061001')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1440, 15, 112, N'SAN MIGUEL', N'140127')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1441, 2, 38, N'SAN MIGUEL DE ACO', N'020408')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1442, 15, 88, N'SAN MIGUEL DE ACOS', N'140809')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1443, 10, 110, N'SAN MIGUEL DE CAURI', N'091005')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1444, 3, 7, N'SAN MIGUEL DE CHACCRAMPA', N'030318')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1445, 2, 23, N'SAN MIGUEL DE CORPANQUI', N'020317')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1446, 20, 81, N'SAN MIGUEL DE EL FAIQUE', N'190307')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1447, 9, 55, N'SAN MIGUEL DE MAYOCC', N'080708')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1448, 1, 155, N'SAN NICOLAS', N'010501')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1449, 2, 38, N'SAN NICOLAS', N'021703')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1450, 6, 162, N'SAN PABLO', N'061301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1451, 7, 31, N'SAN PABLO', N'070606')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1452, 16, 120, N'SAN PABLO', N'150604')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1453, 22, 21, N'SAN PABLO', N'210703')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1454, 2, 128, N'SAN PEDRO', N'022009')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1455, 5, 114, N'SAN PEDRO', N'050522')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1456, 7, 31, N'SAN PEDRO', N'070607')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1457, 3, 1, N'SAN PEDRO DE CACHORA', N'030108')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1458, 12, 179, N'SAN PEDRO DE CAJAS', N'110508')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1459, 2, 90, N'SAN PEDRO DE CHANA', N'020814')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1460, 10, 86, N'SAN PEDRO DE CHAULAN', N'090107')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1461, 12, 100, N'SAN PEDRO DE CHUNAN', N'110327')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1462, 9, 55, N'SAN PEDRO DE CORIS', N'080709')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1463, 11, 49, N'SAN PEDRO DE HUACARPANA', N'100206')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1464, 15, 91, N'SAN PEDRO DE HUANCAYRE', N'140623')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1465, 5, 172, N'SAN PEDRO DE LARCAY', N'051107')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1466, 13, 132, N'SAN PEDRO DE LLOC', N'120501')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1467, 5, 114, N'SAN PEDRO DE PALCO', N'050531')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1468, 15, 192, N'SAN PEDRO DE PILAS', N'140723')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1469, 19, 65, N'SAN PEDRO DE PILLAO', N'180205')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1470, 21, 165, N'SAN PEDRO DE PUTINA PUNCO', N'200812')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1471, 10, 6, N'SAN RAFAEL', N'090207')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1472, 22, 21, N'SAN RAFAEL', N'210702')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1473, 12, 46, N'SAN RAMON', N'110802')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1474, 22, 107, N'SAN ROQUE DE CUMBAZA', N'210316')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1475, 7, 28, N'SAN SALVADOR', N'070406')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1476, 5, 172, N'SAN SALVADOR DE QUIJE', N'051104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1477, 7, 63, N'SAN SEBASTIAN', N'070105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1478, 6, 161, N'SAN SILVESTRE DE COCHAN', N'061008')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1479, 15, 35, N'SAN VICENTE DE CAÑETE', N'140401')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1480, 13, 163, N'SANAGORAN', N'120306')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1481, 5, 114, N'SANCOS', N'050520')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1482, 5, 80, N'SANCOS', N'050801')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1483, 21, 165, N'SANDIA', N'200801')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1484, 15, 91, N'SANGALLAYA', N'140618')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1485, 7, 3, N'SANGARARA', N'070206')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1486, 2, 166, N'SANTA', N'021307')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1487, 7, 103, N'SANTA ANA', N'070901')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1488, 9, 42, N'SANTA ANA', N'080429')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1489, 5, 114, N'SANTA ANA DE HUAYCAHUACHO', N'050524')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1490, 19, 65, N'SANTA ANA DE TUSI', N'180206')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1491, 15, 112, N'SANTA ANITA', N'140143')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1492, 12, 191, N'SANTA BARBARA DE CARHUACAYAN', N'110607')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1493, 1, 115, N'SANTA CATALINA', N'010419')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1494, 20, 124, N'SANTA CATALINA DE MOSSA', N'190406')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1495, 2, 94, N'SANTA CRUZ', N'020708')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1496, 6, 64, N'SANTA CRUZ', N'060511')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1497, 6, 167, N'SANTA CRUZ', N'060901')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1498, 11, 137, N'SANTA CRUZ', N'100504')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1499, 16, 5, N'SANTA CRUZ', N'150210')
GO
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1500, 15, 88, N'SANTA CRUZ DE ANDAMARCA', N'140811')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1501, 13, 168, N'SANTA CRUZ DE CHUCA', N'120706')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1502, 15, 91, N'SANTA CRUZ DE COCACHACRA', N'140624')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1503, 15, 35, N'SANTA CRUZ DE FLORES', N'140414')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1504, 6, 59, N'SANTA CRUZ DE TOLED', N'060409')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1505, 15, 91, N'SANTA EULALIA', N'140625')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1506, 4, 12, N'SANTA ISABEL DE SIGUAS', N'040117')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1507, 15, 93, N'SANTA LEONOR', N'140511')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1508, 5, 114, N'SANTA LUCIA', N'050525')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1509, 21, 109, N'SANTA LUCIA', N'200609')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1510, 15, 93, N'SANTA MARIA', N'140512')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1511, 3, 7, N'SANTA MARIA DE CHICMO', N'030315')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1512, 15, 112, N'SANTA MARIA DEL MAR', N'140128')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1513, 10, 86, N'SANTA MARIA DEL VALLE', N'090108')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1514, 4, 12, N'SANTA RITA DE SIHUAS', N'040118')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1515, 1, 155, N'SANTA ROSA', N'010510')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1516, 2, 136, N'SANTA ROSA', N'021010')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1517, 3, 75, N'SANTA ROSA', N'030613')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1518, 5, 105, N'SANTA ROSA', N'050408')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1519, 6, 98, N'SANTA ROSA', N'060810')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1520, 14, 48, N'SANTA ROSA', N'130113')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1521, 15, 112, N'SANTA ROSA', N'140129')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1522, 21, 123, N'SANTA ROSA', N'200708')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1523, 21, 69, N'SANTA ROSA', N'201203')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1524, 22, 69, N'SANTA ROSA', N'211005')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1525, 12, 55, N'SANTA ROSA DE OCOPA', N'110214')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1526, 15, 34, N'SANTA ROSA DE QUIVES', N'140307')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1527, 12, 191, N'SANTA ROSA DE SACCO', N'110610')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1528, 7, 103, N'SANTA TERESA', N'070906')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1529, 7, 63, N'SANTIAGO', N'070106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1530, 11, 96, N'SANTIAGO', N'100109')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1531, 15, 91, N'SANTIAGO DE ANCHUCAYA', N'140626')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1532, 13, 13, N'SANTIAGO DE CAO', N'120804')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1533, 13, 141, N'SANTIAGO DE CHALLAS', N'120613')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1534, 2, 128, N'SANTIAGO DE CHILCAS', N'022010')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1535, 9, 94, N'SANTIAGO DE CHOCORVOS', N'080613')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1536, 13, 168, N'SANTIAGO DE CHUCO', N'120701')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1537, 5, 80, N'SANTIAGO DE LUCANAMARCA', N'050803')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1538, 5, 172, N'SANTIAGO DE PAUCARAY', N'051106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1539, 5, 80, N'SANTIAGO DE PISCHA', N'050108')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1540, 21, 17, N'SANTIAGO DE PUPUJA', N'200216')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1541, 9, 94, N'SANTIAGO DE QUIRAHUARA', N'080614')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1542, 15, 112, N'SANTIAGO DE SURCO', N'140130')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1543, 15, 91, N'SANTIAGO DE TUNA', N'140627')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1544, 5, 86, N'SANTILLANA', N'050307')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1545, 20, 124, N'SANTO DOMINGO', N'190407')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1546, 12, 85, N'SANTO DOMINGO DE ACOBAMBA', N'110131')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1547, 9, 94, N'SANTO DOMINGO DE CAPILLAS', N'080615')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1548, 6, 64, N'SANTO DOMINGO DE LA CAPILLA', N'060512')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1549, 15, 91, N'SANTO DOMINGO DE LOS OLLEROS', N'140628')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1550, 1, 115, N'SANTO TOMAS', N'010420')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1551, 6, 64, N'SANTO TOMAS', N'060513')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1552, 7, 53, N'SANTO TOMAS', N'070701')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1553, 9, 8, N'SANTO TOMAS DE PATA', N'080310')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1554, 2, 94, N'SANTO TORIBIO', N'020710')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1555, 14, 48, N'SAÑA', N'130114')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1556, 3, 17, N'SAÑAYCA', N'030210')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1557, 12, 85, N'SAÑO', N'110132')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1558, 12, 85, N'SAPALLANGA', N'110133')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1559, 20, 16, N'SAPILLICA', N'190206')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1560, 22, 78, N'SAPOSOA', N'210201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1561, 16, 154, N'SAQUENA', N'150407')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1562, 5, 141, N'SARA SARA', N'051010')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1563, 16, 184, N'SARAYACU', N'150505')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1564, 5, 186, N'SARHUA', N'050713')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1565, 13, 163, N'SARIN', N'120307')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1566, 13, 163, N'SARTIMBAMBA', N'120308')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1567, 12, 169, N'SATIPO', N'110701')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1568, 22, 159, N'SAUCE', N'210619')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1569, 6, 167, N'SAUCEPAMPA', N'060911')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1570, 5, 187, N'SAURAMA', N'050907')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1571, 12, 100, N'SAUSA', N'110333')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1572, 15, 93, N'SAYAN', N'140513')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1573, 13, 73, N'SAYAPULLO', N'121104')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1574, 4, 105, N'SAYLA', N'040808')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1575, 7, 63, N'SAYLLA', N'070107')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1576, 9, 8, N'SECCLLA', N'080311')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1577, 20, 170, N'SECHURA', N'190801')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1578, 25, 15, N'SEPAHUA', N'250304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1579, 6, 167, N'SEXI', N'060907')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1580, 22, 144, N'SHAMBOYACU', N'210910')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1581, 22, 107, N'SHANAO', N'210311')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1582, 22, 159, N'SHAPAJA', N'210620')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1583, 22, 69, N'SHATOJA', N'211003')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1584, 2, 38, N'SHILLA', N'020409')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1585, 1, 23, N'SHIPASBAMBA', N'010309')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1586, 10, 68, N'SHUNQUI', N'090321')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1587, 22, 181, N'SHUNTE', N'210804')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1588, 2, 193, N'SHUPLUY', N'021507')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1589, 4, 43, N'SIBAYO', N'040215')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1590, 12, 85, N'SICAYA', N'110134')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1591, 20, 16, N'SICCHEZ', N'190207')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1592, 2, 171, N'SICSIBAMBA', N'021406')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1593, 7, 31, N'SICUANI', N'070601')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1594, 2, 171, N'SIHUAS', N'021401')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1595, 10, 68, N'SILLAPATA', N'090322')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1596, 13, 181, N'SIMBAL', N'120106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1597, 19, 140, N'SIMON BOLIVAR', N'180110')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1598, 21, 157, N'SINA', N'201105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1599, 12, 100, N'SINCOS', N'110328')
GO
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1600, 10, 78, N'SINGA', N'090410')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1601, 13, 129, N'SINSICAP', N'120410')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1602, 13, 168, N'SITABAMBA', N'120707')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1603, 6, 25, N'SITACOCHA', N'060205')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1604, 23, 178, N'SITAJARA', N'220210')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1605, 5, 86, N'SIVIA', N'050308')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1606, 4, 12, N'SOCABAYA', N'040119')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1607, 5, 80, N'SOCOS', N'050112')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1608, 6, 64, N'SOCOTA', N'060514')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1609, 1, 45, N'SOLOCO', N'010120')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1610, 1, 45, N'SONCHE', N'010121')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1611, 20, 81, N'SONDOR', N'190304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1612, 20, 81, N'SONDORILLO', N'190305')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1613, 16, 154, N'SOPLIN', N'150408')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1614, 5, 172, N'SORAS', N'051108')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1615, 3, 17, N'SORAYA', N'030211')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1616, 22, 126, N'SORITOR', N'210105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1617, 6, 43, N'SOROCHUCO', N'060309')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1618, 11, 96, N'SUBTANJALLA', N'100110')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1619, 2, 4, N'SUCCHA', N'020208')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1620, 6, 43, N'SUCRE', N'060310')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1621, 12, 191, N'SUITUCANCHA', N'110608')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1622, 20, 173, N'SULLANA', N'190601')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1623, 15, 88, N'SUMBILCA', N'140812')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1624, 11, 49, N'SUNAMPE', N'100207')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1625, 15, 20, N'SUPE', N'140904')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1626, 15, 20, N'SUPE PUERTO', N'140905')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1627, 15, 91, N'SURCO', N'140629')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1628, 9, 180, N'SURCUBAMBA', N'080523')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1629, 15, 112, N'SURQUILLO', N'140131')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1630, 23, 178, N'SUSAPAYA', N'220211')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1631, 7, 71, N'SUYCKUTAMBO', N'070807')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1632, 20, 16, N'SUYO', N'190208')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1633, 6, 158, N'TABACONAS', N'061107')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1634, 22, 107, N'TABALOSOS', N'210313')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1635, 6, 51, N'TACABAMBA', N'060615')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1636, 23, 174, N'TACNA', N'220101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1637, 17, 175, N'TAHUAMANU', N'160303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1638, 25, 15, N'TAHUANIA', N'250302')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1639, 3, 7, N'TALAVERA', N'030311')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1640, 20, 135, N'TAMARINDO', N'190506')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1641, 5, 80, N'TAMBILLO', N'050110')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1642, 5, 105, N'TAMBO', N'050406')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1643, 9, 94, N'TAMBO', N'080616')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1644, 11, 49, N'TAMBO DE MORA', N'100208')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1645, 20, 146, N'TAMBO GRANDE', N'190109')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1646, 3, 63, N'TAMBOBAMBA', N'030501')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1647, 17, 177, N'TAMBOPATA', N'160101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1648, 3, 1, N'TAMBURCO', N'030109')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1649, 15, 192, N'TANTA', N'140724')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1650, 10, 78, N'TANTAMAYO', N'090411')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1651, 9, 42, N'TANTARA', N'080427')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1652, 6, 59, N'TANTARICA', N'060407')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1653, 2, 153, N'TAPACOCHA', N'021207')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1654, 3, 17, N'TAPAIRIHUA', N'030212')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1655, 4, 43, N'TAPAY', N'040216')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1656, 16, 154, N'TAPICHE', N'150409')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1657, 12, 179, N'TAPO', N'110509')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1658, 19, 65, N'TAPUC', N'180207')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1659, 21, 81, N'TARACO', N'200508')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1660, 22, 159, N'TARAPOTO', N'210601')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1661, 23, 178, N'TARATA', N'220201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1662, 7, 28, N'TARAY', N'070407')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1663, 2, 89, N'TARICA', N'020112')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1664, 12, 179, N'TARMA', N'110501')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1665, 23, 178, N'TARUCACHI', N'220212')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1666, 11, 96, N'TATE', N'100112')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1667, 2, 136, N'TAUCA', N'021011')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1668, 4, 105, N'TAURIA', N'040809')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1669, 13, 141, N'TAURIJA', N'120611')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1670, 15, 192, N'TAURIPAMPA', N'140725')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1671, 13, 141, N'TAYABAMBA', N'120601')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1672, 16, 5, N'TENIENTE CESAR LOPEZ ROJAS', N'150211')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1673, 16, 122, N'TENIENTE MANUEL CLAVERO', N'150114')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1674, 4, 12, N'TIABAYA', N'040120')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1675, 11, 137, N'TIBILLO', N'100505')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1676, 23, 178, N'TICACO', N'220213')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1677, 2, 153, N'TICAPAMPA', N'021208')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1678, 19, 140, N'TICLACAYAN', N'180111')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1679, 2, 23, N'TICLLOS', N'020320')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1680, 9, 42, N'TICRAPO', N'080428')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1681, 16, 113, N'TIGRE', N'150303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1682, 21, 124, N'TILALI', N'201303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1683, 2, 38, N'TINCO', N'020410')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1684, 1, 115, N'TINGO', N'010421')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1685, 22, 144, N'TINGO DE PONASA', N'210908')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1686, 22, 78, N'TINGO DE SAPOSOA', N'210204')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1687, 21, 194, N'TINICACHI', N'201007')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1688, 7, 31, N'TINTA', N'070608')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1689, 3, 17, N'TINTAY', N'030213')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1690, 9, 180, N'TINTAY PUNCU', N'080525')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1691, 19, 140, N'TINYAHUARCO', N'180112')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1692, 4, 41, N'TIPAN', N'040511')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1693, 21, 150, N'TIQUILLACA', N'200112')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1694, 21, 17, N'TIRAPATA', N'200217')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1695, 4, 43, N'TISCO', N'040217')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1696, 22, 181, N'TOCACHE', N'210801')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1697, 6, 51, N'TOCMOCHE', N'060616')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1698, 15, 192, N'TOMAS', N'140727')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1699, 10, 6, N'TOMAY-KICHWA', N'090208')
GO
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1700, 4, 105, N'TOMEPAMPA', N'040810')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1701, 6, 161, N'TONGOD', N'061011')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1702, 18, 120, N'TORATA', N'170105')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1703, 3, 17, N'TORAYA', N'030214')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1704, 6, 64, N'TORIBIO CASANOVA', N'060515')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1705, 4, 105, N'TORO', N'040811')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1706, 16, 122, N'TORRES CAUSANA', N'150108')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1707, 1, 155, N'TOTORA', N'010511')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1708, 5, 33, N'TOTOS', N'050208')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1709, 10, 149, N'TOURNAVISTA', N'090804')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1710, 12, 54, N'TRES DE DICIEMBRE', N'110908')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1711, 22, 144, N'TRES UNIDOS', N'210909')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1712, 1, 115, N'TRITA', N'010422')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1713, 16, 113, N'TROMPETEROS', N'150305')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1714, 13, 181, N'TRUJILLO', N'120101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1715, 14, 108, N'TUCUME', N'130312')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1716, 14, 48, N'TUMAN', N'130120')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1717, 3, 7, N'TUMAY HUARACA', N'030316')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1718, 6, 162, N'TUMBADEN', N'061304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1719, 24, 183, N'TUMBES', N'230101')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1720, 12, 100, N'TUNAN MARCA', N'110329')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1721, 7, 30, N'TUPAC AMARU', N'070508')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1722, 11, 145, N'TUPAC AMARU INCA', N'100408')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1723, 15, 192, N'TUPE', N'140726')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1724, 3, 75, N'TURPAY', N'030610')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1725, 3, 7, N'TURPO', N'030312')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1726, 4, 43, N'TUTI', N'040218')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1727, 18, 72, N'UBINAS', N'170210')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1728, 22, 181, N'UCHIZA', N'210805')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1729, 13, 21, N'UCHUMARCA', N'120206')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1730, 4, 12, N'UCHUMAYO', N'040121')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1731, 2, 90, N'UCO', N'020815')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1732, 13, 21, N'UCUNCHA', N'120205')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1733, 12, 102, N'ULCUMAYO', N'110404')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1734, 21, 123, N'UMACHIRI', N'200709')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1735, 10, 133, N'UMARI', N'090706')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1736, 21, 194, N'UNICACHI', N'201002')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1737, 6, 161, N'UNION AGUA BLANCA', N'061010')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1738, 4, 41, N'UÑON', N'040513')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1739, 5, 138, N'UPAHUACHO', N'050616')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1740, 4, 41, N'URACA', N'040512')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1741, 3, 50, N'URANMARCA', N'030707')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1742, 16, 113, N'URARINAS', N'150304')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1743, 7, 152, N'URCOS', N'071201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1744, 13, 141, N'URPAY', N'120612')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1745, 7, 185, N'URUBAMBA', N'071301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1746, 21, 36, N'USICAYOS', N'200310')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1747, 13, 129, N'USQUIL', N'120411')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1748, 6, 43, N'UTCO', N'060311')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1749, 6, 167, N'UTICYACU', N'060908')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1750, 1, 23, N'VALERA', N'010310')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1751, 16, 184, N'VARGAS GUERRA', N'150502')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1752, 15, 93, N'VEGUETA', N'140516')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1753, 20, 146, N'VEINTISEIS DE OCTUBRE', N'190115')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1754, 15, 88, N'VEINTISIETE DE NOVIEMBRE', N'140810')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1755, 7, 53, N'VELILLE', N'070708')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1756, 7, 28, N'VENTANILLA', N'240106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1757, 19, 140, N'VICCO', N'180113')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1758, 20, 170, N'VICE', N'190802')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1759, 20, 135, N'VICHAYAL', N'190507')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1760, 13, 181, N'VICTOR LARCO HERRERA', N'120107')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1761, 21, 109, N'VILAVILA', N'200610')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1762, 9, 84, N'VILCA', N'080117')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1763, 3, 75, N'VILCABAMBA', N'030611')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1764, 7, 103, N'VILCABAMBA', N'070907')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1765, 19, 65, N'VILCABAMBA', N'180208')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1766, 5, 186, N'VILCANCHOS', N'050714')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1767, 5, 187, N'VILCAS HUAMAN', N'050901')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1768, 15, 112, N'VILLA EL SALVADOR', N'140141')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1769, 15, 112, N'VILLA MARIA DEL TRIUNFO', N'140132')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1770, 19, 130, N'VILLA RICA', N'180305')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1771, 21, 150, N'VILQUE', N'200113')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1772, 21, 81, N'VILQUE CHICO', N'200509')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1773, 5, 80, N'VINCHOS', N'050109')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1774, 15, 192, N'VIÑAC', N'140728')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1775, 12, 85, N'VIQUES', N'110136')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1776, 4, 41, N'VIRACO', N'040514')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1777, 13, 188, N'VIRU', N'121201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1778, 3, 75, N'VIRUNDO', N'030612')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1779, 5, 187, N'VISCHONGO', N'050902')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1780, 1, 155, N'VISTA ALEGRE', N'010512')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1781, 11, 127, N'VISTA ALEGRE', N'100305')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1782, 15, 192, N'VITIS', N'140729')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1783, 12, 46, N'VITOC', N'110803')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1784, 4, 12, N'VITOR', N'040122')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1785, 7, 63, N'WANCHAQ', N'070108')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1786, 10, 86, N'YACUS', N'090112')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1787, 20, 124, N'YAMANGO', N'190409')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1788, 1, 23, N'YAMBRASBAMBA', N'010311')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1789, 1, 186, N'YAMON', N'010707')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1790, 2, 62, N'YANAC', N'020606')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1791, 3, 17, N'YANACA', N'030215')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1792, 12, 54, N'YANACANCHA', N'110909')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1793, 19, 140, N'YANACANCHA', N'180114')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1794, 19, 65, N'YANAHUANCA', N'180201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1795, 4, 12, N'YANAHUARA', N'040123')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1796, 21, 165, N'YANAHUAYA', N'200810')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1797, 2, 193, N'YANAMA', N'021508')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1798, 7, 30, N'YANAOCA', N'070501')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1799, 4, 57, N'YANAQUIHUA', N'040607')
GO
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1800, 10, 68, N'YANAS', N'090323')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1801, 7, 28, N'YANATILE', N'070408')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1802, 4, 43, N'YANQUE', N'040219')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1803, 22, 126, N'YANTALO', N'210106')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1804, 16, 154, N'YAQUERANA', N'150411')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1805, 4, 12, N'YARABAMBA', N'040124')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1806, 25, 61, N'YARINACOCHA', N'250102')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1807, 10, 86, N'YARUMAYO', N'090109')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1808, 4, 36, N'YAUCA', N'040413')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1809, 11, 96, N'YAUCA DEL ROSARIO', N'100111')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1810, 9, 84, N'YAULI', N'080118')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1811, 12, 100, N'YAULI', N'110330')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1812, 12, 191, N'YAULI', N'110609')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1813, 7, 139, N'YAURISQUE', N'071007')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1814, 2, 40, N'YAUTAN', N'020505')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1815, 2, 38, N'YAUYA', N'021702')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1816, 12, 100, N'YAUYOS', N'110334')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1817, 15, 192, N'YAUYOS', N'140701')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1818, 6, 167, N'YAUYUCAN', N'060909')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1819, 16, 120, N'YAVARI', N'150603')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1820, 6, 59, N'YONAN', N'060408')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1821, 22, 155, N'YORONGOS', N'210503')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1822, 7, 185, N'YUCAY', N'071307')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1823, 18, 72, N'YUNGA', N'170211')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1824, 2, 38, N'YUNGAR', N'020411')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1825, 2, 193, N'YUNGAY', N'021501')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1826, 21, 194, N'YUNGUYO', N'201001')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1827, 2, 62, N'YUPAN', N'020607')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1828, 4, 12, N'YURA', N'040125')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1829, 2, 94, N'YURACMARCA', N'020709')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1830, 22, 155, N'YURACYACU', N'210504')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1831, 16, 5, N'YURIMAGUAS', N'150201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1832, 25, 15, N'YURUA', N'250303')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1833, 10, 149, N'YUYAPICHIS', N'090805')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1834, 22, 107, N'ZAPATERO', N'210314')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1835, 24, 195, N'ZARUMILLA', N'230301')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1836, 21, 52, N'ZEPITA', N'200410')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1837, 24, 59, N'ZORRITOS', N'230201')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1838, 15, 35, N'ZUÑIGA', N'140415')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1839, 7, 9, N'ZURITE', N'070307')
INSERT [dbo].[tbDistrito] ([cnID_Distrito], [cnID_Departamento], [cnID_Provincia], [ctDistrito], [ctCodigoISO]) VALUES (1840, 26, 196, N'OTROS', N'000000')
SET IDENTITY_INSERT [dbo].[tbDistrito] OFF

-- SET IDENTITY_INSERT [dbo].[tbDocIdentidad] ON 
-- INSERT [dbo].[tbDocIdentidad] ([cnID_DocIdentidad], [cnID_Candidato], [cnID_TipoDocumento], [ctNumero]) VALUES (6, 1, 237, N'43970672')
-- INSERT [dbo].[tbDocIdentidad] ([cnID_DocIdentidad], [cnID_Candidato], [cnID_TipoDocumento], [ctNumero]) VALUES (2, 6, 239, N'DFGDFG')
-- SET IDENTITY_INSERT [dbo].[tbDocIdentidad] OFF
-- SET IDENTITY_INSERT [dbo].[tbExperiencia] ON 

-- INSERT [dbo].[tbExperiencia] ([cnID_Experiencia], [cnID_Candidato], [cnID_TipoEntidad], [ctNombreEmpresa], [cnID_Rubro], [cnID_Cargo], [ctCargoOtro], [cnID_Area], [cnID_Pais], [cnID_TipoContrato], [cnSubordinados], [ctDescripcionCargo], [cfFechaInicio], [cfFechaFin], [cnTiempoAnios], [cnTiempoMeses], [ctCertificado], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (1, 1, 242, N'UNIMAR', 1546, 326, NULL, 255, 177, 1620, 12, N'ASDASDASD', CAST(0xA92C0B00 AS Date), CAST(0x163A0B00 AS Date), 9, 5, N'fcecfe11-7104-4492-9312-e3e925bf03b6.pdf', CAST(0x0000A4C4000972F0 AS DateTime), CAST(0x0000A4C6016C2289 AS DateTime), N'::1')
-- SET IDENTITY_INSERT [dbo].[tbExperiencia] OFF
-- SET IDENTITY_INSERT [dbo].[tbIdioma] ON 

-- INSERT [dbo].[tbIdioma] ([cnID_Idioma], [cnID_Candidato], [cnID_Lengua], [ctLenguaOtro], [cnID_Institucion], [cnID_NivelLectura], [cnID_NivelEscritura], [cnID_NivelConversacion], [cnID_Situacion], [ctCertificado], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (1, 1, 208, NULL, 96, 222, 222, 222, 1640, NULL, CAST(0x0000A4C4000824E4 AS DateTime), CAST(0x0000A4C400089CE3 AS DateTime), N'::1')
-- SET IDENTITY_INSERT [dbo].[tbIdioma] OFF

SET IDENTITY_INSERT [dbo].[tbMaestroGrupos] ON 
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (1, N'Area', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (2, N'Cargo', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (3, N'Carrera', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (4, N'Estado de Registro', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (5, N'Grado', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (6, N'Idioma', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (7, N'Instituto', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (8, N'Nivel (Escritura/Lectura/Conversación)', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (9, N'Ofimatica', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (10, N'Rubro', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (11, N'Sexo', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (12, N'Situacion', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (13, N'Tipo de Criterio Laboral', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (14, N'Tipo de Documento', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (15, N'Tipo de Entidad', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (16, N'Tipo de Evaluacion', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (17, N'Tipo de Proceso', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (18, N'Universidad', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (19, N'Tipo de Contrato', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (20, N'Estado Civil', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (21, N'Tipo Estudio', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (22, N'Tipo de Entidad Convocatoria', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (23, N'Estado Proceso', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (24, N'General', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (25, N'Tipo de Resultado', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (26, N'Situacion Educacion', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (27, N'Situacion Tecnico/Especializacion', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (28, N'Situacion Universidad', NULL, 1)
INSERT [dbo].[tbMaestroGrupos] ([cnID_GrupoLista], [ctNombreGrupo], [ctAlias], [cnEstado]) VALUES (29, N'Varios', NULL, 1)
SET IDENTITY_INSERT [dbo].[tbMaestroGrupos] OFF

SET IDENTITY_INSERT [dbo].[tbMaestroListas] ON 
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1, 18, N'Facultad de Teología Pontificia y Civil de Lima', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (2, 18, N'Pontificia Universidad Católica del Perú', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (3, 18, N'Universidad Alas Peruanas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (4, 18, N'Universidad Andina del Cusco', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (5, 18, N'Universidad Andina Néstor Cáceres Velásquez', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (6, 18, N'Universidad Antonio Ruiz de Montoya', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (7, 18, N'Universidad Católica de Santa María', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (8, 18, N'Universidad Católica de Trujillo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (9, 18, N'Universidad Católica Santo Toribio de Mogrovejo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (10, 18, N'Universidad Católica Sedes Sapientiae', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (11, 18, N'Universidad Científica del Sur S.A.C.', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (12, 18, N'Universidad Continental de Ciencias e Ingeniería', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (13, 18, N'Universidad de Administración de Negocios - Esan', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (14, 18, N'Universidad de Chiclayo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (15, 18, N'Universidad de Huánuco', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (16, 18, N'Universidad de Lima', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (17, 18, N'Universidad de Piura', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (18, 18, N'Universidad de San Martín de Porres', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (19, 18, N'Universidad del Pacífico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (20, 18, N'Universidad Femenina del Sagrado Corazón', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (21, 18, N'Universidad Inca Garcilaso de La Vega', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (22, 18, N'Universidad Jose Carlos Mariátegui', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (23, 18, N'Universidad Julio C. Tello', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (24, 18, N'Universidad Los Ángeles de Chimbote', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (25, 18, N'Universidad Nacional Agraria de La Selva', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (26, 18, N'Universidad Nacional Agraria La Molina', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (27, 18, N'Universidad Nacional Amazónica de Madre de Dios', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (28, 18, N'Universidad Nacional Daniel Alcides Carrión', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (29, 18, N'Universidad Nacional de Cajamarca', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (30, 18, N'Universidad Nacional de Educación Enrique Guzmán y Valle', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (31, 18, N'Universidad Nacional de Huancavelica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (32, 18, N'Universidad Nacional de Ingeniería', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (33, 18, N'Universidad Nacional de la Amazonía Peruana', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (34, 18, N'Universidad Nacional de Moquegua', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (35, 18, N'Universidad Nacional de Piura', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (36, 18, N'Universidad Nacional de San Agustín', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (37, 18, N'Universidad Nacional de San Antonio Abad del Cusco', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (38, 18, N'Universidad Nacional de San Martín', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (39, 18, N'Universidad Nacional de Trujillo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (40, 18, N'Universidad Nacional de Tumbes', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (41, 18, N'Universidad Nacional de Ucayali', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (42, 18, N'Universidad Nacional del Altiplano', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (43, 18, N'Universidad Nacional del Callao', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (44, 18, N'Universidad Nacional del Centro del Perú', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (45, 18, N'Universidad Nacional del Santa', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (46, 18, N'Universidad Nacional Federico Villarreal', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (47, 18, N'Universidad Nacional Hermilio Valdizán', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (48, 18, N'Universidad Nacional Intercultural de la Amazonía Peruana', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (49, 18, N'Universidad Nacional Jorge Basadre Grohmann', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (50, 18, N'Universidad Nacional José Faustino Sánchez Carrión', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (51, 18, N'Universidad Nacional José María Arguedas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (52, 18, N'Universidad Nacional Mayor de San Marcos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (53, 18, N'Universidad Nacional Micaela Bastidas de Apurímac', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (54, 18, N'Universidad Nacional Pedro Ruiz Gallo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (55, 18, N'Universidad Nacional San Cristóbal de  Huamanga', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (56, 18, N'Universidad Nacional San Luis Gonzaga de Ica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (57, 18, N'Universidad Nacional Santiago Antúnez de Mayolo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (58, 18, N'Universidad Nacional Tecnológica del Cono Sur', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (59, 18, N'Universidad Nacional Toribio Rodriguez de Mendoza de Amazonas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (60, 18, N'Universidad para el Desarrollo Andino', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (61, 18, N'Universidad Particular de Iquitos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (62, 18, N'Universidad Particular Marcelino Champagnat', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (63, 18, N'Universidad Particular Tecnológica de Los Andes', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (64, 18, N'Universidad Peruana Cayetano Heredia', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (65, 18, N'Universidad Peruana de Ciencias Aplicadas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (66, 18, N'Universidad Peruana de Ciencias e Informática', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (67, 18, N'Universidad Peruana de Integración Global', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (68, 18, N'Universidad Peruana de las Américas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (69, 18, N'Universidad Peruana Simón Bolivar', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (70, 18, N'Universidad Peruana Unión', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (71, 18, N'Universidad Peruana Los Andes', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (72, 18, N'Universidad Privada Abraham Valdelomar', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (73, 18, N'Universidad Privada Ada A. Byron', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (74, 18, N'Universidad Privada Antenor Orrego', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (75, 18, N'Universidad Privada Antonio Guillermo Urrelo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (76, 18, N'Universidad Privada Católica San Pablo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (77, 18, N'Universidad Privada César Vallejo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (78, 18, N'Universidad Privada de Tacna', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (79, 18, N'Universidad Privada del Norte', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (80, 18, N'Universidad Privada del Oriente S.A.C.', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (81, 18, N'Universidad Privada Norbert Wiener', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (82, 18, N'Universidad Privada Samuel Pastor de Camaná', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (83, 18, N'Universidad Privada San Carlos de Puno', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (84, 18, N'Universidad Privada San Ignacio de Loyola', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (85, 18, N'Universidad Privada San Juan Bautista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (86, 18, N'Universidad Privada San Pedro', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (87, 18, N'Universidad Privada Señor de Sipán', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (88, 18, N'Universidad Privada Sergio Bernales S.A.C.', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (89, 18, N'Universidad Privada Telesup S.A.C.', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (90, 18, N'Universidad Ricardo Palma', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (91, 18, N'Universidad Tecnológica del Perú', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (92, 7, N'Alianza Francesa Lima', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (93, 7, N'American British Center', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (94, 7, N'Asociación Cultural Peruano Británica ', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (95, 7, N'Asociación Eductiva Aduni', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (96, 7, N'Avia - Centro de Estudios de Aviacion', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (97, 7, N'British Academy', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (98, 7, N'CAME', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (99, 7, N'Cenfotur', NULL, 1)
GO
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (100, 7, N'Centro de Altos Estudios de la Moda', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (101, 7, N'Centro Cultural de la Pontificia Universidad Católica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (102, 7, N'Centro de Estudios Aeronauticos "Profesional Air"', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (103, 7, N'Centro de Idiomas de la Universidad del Pacífico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (104, 7, N'Centro de Investigación en Estudios de Postgrado', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (105, 7, N'CECITEL - Especialistas en capacitación Telefónica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (106, 7, N'CENFOTUR - Cetro de formacion en Turismo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (107, 7, N'CEPEBAN - Centro Peruano de Estudios Bancarios', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (108, 7, N'CEPREPUC', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (109, 7, N'CESCA', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (110, 7, N'CIBERTEC', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (111, 7, N'Chio Lecca Instituto de Diseño de Moda', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (112, 7, N'CIMAS ', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (113, 7, N'Colegios del Perú Directorio de Paginas web', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (114, 7, N'Columbia - Escuela Hotelera y Alta Cocina', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (115, 7, N'Computec 2000', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (116, 7, N'COMPUTRONIC ', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (117, 7, N'Conservatorio de Lima - Escuela Superior de Musica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (118, 7, N'Coriente Alterna Escuela Superior de Bellas Artes', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (119, 7, N'DGallia - Escuela Superior de Alta Cocina ', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (120, 7, N'Dónde Estudiar y Trabajar', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (121, 7, N'Escuela de Arte Digital', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (122, 7, N'Escuela de Educación Intermedia - CAME', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (123, 7, N'Escuela de Pilotos Aero Vigil', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (124, 7, N'Escuela de Secretariado "Margarita Cabrera" ', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (125, 7, N'Escuela de Seguros Segurtec', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (126, 7, N'Escuela Iberoamericana de Hotelería y Turismo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (127, 7, N'Escuela Internacional de Gerencia EIGER', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (128, 7, N'Escuela Superior de Administración de Negocios - ESAN', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (129, 7, N'Escuela Superior de Telecomunicaciones - ESUTEL', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (130, 7, N'Escuela Superior de Traducción e Interpretación de lima ESIT', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (131, 7, N'Escuela Tecnologica Superior Universidad de Piura', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (132, 7, N'Euroidiomas  ', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (133, 7, N'Gastronomia Institutos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (134, 7, N'Global Institute ', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (135, 7, N'Iberoamericana de Hotelería y Turismo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (136, 7, N'IDAT', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (137, 7, N'IGS Instituto de Salud y Gestion', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (138, 7, N'INFAMILE - Instituto de Aprendisaje Innovaciones Educativas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (139, 7, N'INTECI - Gastronomia, Hoteleria, Turismo, Modas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (140, 7, N'IPAD - Instituto Peruano de Arte y Diseño', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (141, 7, N'IPAE - Escuela de Empresarios', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (142, 7, N'ISAMP Instituto de Administración Turismo u Gastronomia', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (143, 7, N'Institutos Educativos en Lima', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (144, 7, N'Institutos de Gastronomia Lima Peru', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (145, 7, N'Institutos de Idiomas en Perú', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (146, 7, N'Instituto de Investigación de Ciencias Biológicas "Antonio Raimondi"', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (147, 7, N'Instituto de Ciencias de la Comunicación "Charles Chaplin"', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (148, 7, N'Instituto Confucio PUCP - Chino Mandarín', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (149, 7, N'Instituto de Educación Superior "Daniel Alcides Carrión"', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (150, 7, N'Instituto de Educación Superior Leonardo Da Vinci - Trujillo ', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (151, 7, N'Instituto de Educación Superior "San Martín"', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (152, 7, N'Instituto de Educacion Superior Teccen', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (153, 7, N'Instituto de Formación Bancaria IFB', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (154, 7, N'Instituto de Gestion y Salud - IGS', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (155, 7, N'Instituto de Los Andes', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (156, 7, N'Instituto de Idiomas de la Universidad Católica  ', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (157, 7, N'Instituto del Sur (Arequipa)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (158, 7, N'Instituto IDEA', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (159, 7, N'Instituto Leo Design', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (160, 7, N'Instituto Nacional de Investigacion y Capacitación de Telecomunicaciones - INICTEL', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (161, 7, N'Instituto Nina Design - Diseño Moda', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (162, 7, N'Instituto Norbert Wiener', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (163, 7, N'Instituto para el Desarrollo Empresarial y Administrativo - IDEA', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (164, 7, N'Instituto Pedagógico Santo Domingo de Guzman', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (165, 7, N'Instituto Peruano de Administración de Empresas - IPAE', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (166, 7, N'Instituto Peruano de Arte y Diseño IPAD ', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (167, 7, N'Instituto Peruano de Biología Molecular  ', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (168, 7, N'Instituto Peruano de Gastronomia IPG', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (169, 7, N'Instituto Ricardo Palma', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (170, 7, N'Instituto Peruano de Joyeria y Arte', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (171, 7, N'Instituto Peruano de Publicidad - IPP ', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (172, 7, N'Instituto San Ignacio de Loyola - SIL', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (173, 7, N'Instituto Superior Daniel A.Carrión', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (174, 7, N'Instituto Superior de Comunicación y Diseño "Toulouse-Lautrec"', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (175, 7, N'Instituto Superior de Conservacion y Restauracion Yachay Wasi  ', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (176, 7, N'Instituto Superior Expro', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (177, 7, N'Instituto Superior Gastrotur Peru', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (178, 7, N'Instituto Superior MATRIX', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (179, 7, N'Instituto Superior Montessori', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (180, 7, N'Instituto Superior "Orson Welles"', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (181, 7, N'Instituto Superior Peruano Alemán - IPAL', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (182, 7, N'Instituto Superior Poussin', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (183, 7, N'Instituto Superior SISE ', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (184, 7, N'Instituto Superios Tecnológico ABACO', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (185, 7, N'Instituto Superior Tecnológico "Ciro Alegría" (Tarapoto)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (186, 7, N'Instituto Superior Tecnológico COLUMBIA ', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (187, 7, N'Instituto Superior Tecnológico Huando - Huaral', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (188, 7, N'Instituto Superior Tecnológico Público Cutervo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (189, 7, N'Instituto Superior Tecnológico Público "Gral.P.N.P. Oscar Arteta Terzi"', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (190, 7, N'Instituto Toulouse Lautrec', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (191, 7, N'Instituto Superior Pedagógico San Marcos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (192, 7, N'Inteci - Instituto de Profesiones Empresariales ', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (193, 7, N'MAD - Escuela de moda y diseño', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (194, 7, N'Master of the Sky, Escuela de pilotos ', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (195, 7, N'Montecatini Escuela de Administracion y Turismo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (196, 7, N'Montesori - Escuela de Comercio Exterior y Aduanas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (197, 7, N'Profesional Air', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (198, 7, N'SECOMTUR Centro de Estudios Turisticos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (199, 7, N'SENATI', NULL, 1)
GO
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (200, 7, N'SENATI - Programa Nacional de Informática', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (201, 7, N'SENCICO Escuela Superior Tècnica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (202, 7, N'Sociedad Internacional de Telemática SIT', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (203, 7, N'TECSUP', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (204, 7, N'TELESUP', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (205, 7, N'Universidades del Peru - Directorio de paginas web', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (206, 7, N'WIENER', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (207, 6, N'Alemán', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (208, 6, N'Árabe', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (209, 6, N'Ashaninka', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (210, 6, N'Aymara', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (211, 6, N'Chino Mandarín', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (212, 6, N'Francés', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (213, 6, N'Inglés', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (214, 6, N'Italiano', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (215, 6, N'Japonés', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (216, 6, N'Portugués', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (217, 6, N'Quechua', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (218, 6, N'Ruso', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (219, 4, N'Activo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (220, 4, N'Inactivo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (221, 8, N'Básico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (222, 8, N'Intermedio', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (223, 8, N'Avanzado', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (224, 9, N'Word', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (225, 9, N'Excel', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (226, 9, N'PowerPoint', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (227, 11, N'Masculino', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (228, 11, N'Femenino', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (229, 12, N'Colegiado', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (230, 12, N'Titulado', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (231, 12, N'Bachiller', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (232, 12, N'Egresado', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (233, 12, N'En curso', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (234, 12, N'Incompleto', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (235, 13, N'General', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (236, 13, N'Específico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (237, 14, N'DNI', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (238, 14, N'RUC', NULL, 0)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (239, 14, N'Carné de Extranjería', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (240, 14, N'Brevete', NULL, 0)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (241, 15, N'Pública', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (242, 15, N'Privada', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (243, 16, N'Años', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (244, 16, N'Meses', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (246, 17, N'Convocatoria', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (247, 5, N'Doctorado', 6, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (248, 5, N'Maestría', 5, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (249, 5, N'Universitario', 4, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (250, 5, N'Técnico', 3, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (251, 5, N'Secundaria', 2, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (252, 5, N'Primaria', 1, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (253, 5, N'Sin Estudios', 0, 0)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (254, 1, N'Administración / Servicios Generales', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (255, 1, N'Almacén', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (256, 1, N'Atención al Cliente', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (257, 1, N'Auditoría', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (258, 1, N'Banca', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (259, 1, N'Call Center', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (260, 1, N'Comercial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (261, 1, N'Comercio Exterior / Aduanas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (262, 1, N'Compras / Logística', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (263, 1, N'Consultoría', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (264, 1, N'Contabilidad', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (265, 1, N'Finanzas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (266, 1, N'Hotelería / Turismo / Restaurantes', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (267, 1, N'Ingeniería', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (268, 1, N'Investigación y Desarrollo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (269, 1, N'Legal', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (270, 1, N'Mantenimiento', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (271, 1, N'Marketing / Publicidad', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (272, 1, N'Medios Digitales / Internet', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (273, 1, N'Operaciones', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (274, 1, N'Producción', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (275, 1, N'Proyectos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (276, 1, N'Recursos Humanos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (277, 1, N'Sistemas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (278, 1, N'Ventas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (279, 1, N'Otros', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (280, 2, N'Abarrotero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (281, 2, N'Abogado', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (282, 2, N'Acabador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (283, 2, N'Acomodador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (284, 2, N'Acrilero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (285, 2, N'Actor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (286, 2, N'Actriz', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (287, 2, N'Acupunturista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (288, 2, N'Administrador de obra', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (289, 2, N'Administrador de planta', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (290, 2, N'Administrador(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (291, 2, N'Admisionista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (292, 2, N'Aduanero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (293, 2, N'Aerografista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (294, 2, N'Afilador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (295, 2, N'Afinador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (296, 2, N'Agente', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (297, 2, N'Agente aeroportuario', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (298, 2, N'Agente de seguridad', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (299, 2, N'Agricultor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (300, 2, N'Agronomo', NULL, 1)
GO
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (301, 2, N'Aislador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (302, 2, N'Ajedrecista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (303, 2, N'Ajustador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (304, 2, N'AlbaÃ±il', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (305, 2, N'Alfajorero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (306, 2, N'Alineador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (307, 2, N'Almacenero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (308, 2, N'Aluminiero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (309, 2, N'Ama', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (310, 2, N'Ambientalista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (311, 2, N'Amoldador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (312, 2, N'Analista comercial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (313, 2, N'Analista contable', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (314, 2, N'Analista de administracion y finanzas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (315, 2, N'Analista de asuntos regulatorios', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (316, 2, N'Analista de auditoria', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (317, 2, N'Analista de capacitacion', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (318, 2, N'Analista de carteras', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (319, 2, N'Analista de compensaciones', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (320, 2, N'Analista de consumo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (321, 2, N'Analista de control de gestion', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (322, 2, N'Analista de costos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (323, 2, N'Analista de creditos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (324, 2, N'Analista de desarrollo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (325, 2, N'Analista de imagenes', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (326, 2, N'Analista de informatica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (327, 2, N'Analista de inversiones', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (328, 2, N'Analista de laboratorio', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (329, 2, N'Analista de logistica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (330, 2, N'Analista de mantenimiento', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (331, 2, N'Analista de marketing', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (332, 2, N'Analista de minerales', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (333, 2, N'Analista de negocios', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (334, 2, N'Analista de obra', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (335, 2, N'Analista de operaciones', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (336, 2, N'Analista de personal', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (337, 2, N'Analista de procesos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (338, 2, N'Analista de producto', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (339, 2, N'Analista de proyectos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (340, 2, N'Analista de rrhh', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (341, 2, N'Analista de seleccion y reclutamiento de personal', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (342, 2, N'Analista de sistemas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (343, 2, N'Analista de ventas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (344, 2, N'Analista financiero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (345, 2, N'Analista junior crm', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (346, 2, N'Analista legal', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (347, 2, N'Analista planta', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (348, 2, N'Analista predictivo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (349, 2, N'Analista programador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (350, 2, N'Analista senior crm', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (351, 2, N'Analistas de riesgos senior', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (352, 2, N'Andamiero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (353, 2, N'Anfitrion(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (354, 2, N'Anillador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (355, 2, N'Animador(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (356, 2, N'Anticuchero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (357, 2, N'Antropologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (358, 2, N'Anudador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (359, 2, N'Aparador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (360, 2, N'Arbitro', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (361, 2, N'Archivero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (362, 2, N'Arenador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (363, 2, N'Armador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (364, 2, N'Armero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (365, 2, N'Arqueologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (366, 2, N'Arquitecto', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (367, 2, N'Artesano', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (368, 2, N'Artista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (369, 2, N'Asegurador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (370, 2, N'Asesor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (371, 2, N'Asistente', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (372, 2, N'Asistente academico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (373, 2, N'Asistente administrativo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (374, 2, N'Asistente aduanero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (375, 2, N'Asistente auditoria', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (376, 2, N'Asistente bilingue', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (377, 2, N'Asistente comercial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (378, 2, N'Asistente contable', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (379, 2, N'Asistente de administracion y finanzas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (380, 2, N'Asistente de agroexportacion', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (381, 2, N'Asistente de almacen', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (382, 2, N'Asistente de atencion al cliente', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (383, 2, N'Asistente de auditoria', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (384, 2, N'Asistente de campo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (385, 2, N'Asistente de capital', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (386, 2, N'Asistente de cobranzas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (387, 2, N'Asistente de comercio exterior', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (388, 2, N'Asistente de compras', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (389, 2, N'Asistente de control de gestion', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (390, 2, N'Asistente de controller', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (391, 2, N'Asistente de costos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (392, 2, N'Asistente de creditos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (393, 2, N'Asistente de derecho', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (394, 2, N'Asistente de desarrollo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (395, 2, N'Asistente de despacho', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (396, 2, N'Asistente de electrificacion', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (397, 2, N'Asistente de entrenamiento', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (398, 2, N'Asistente de exportacion', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (399, 2, N'Asistente de facturacion', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (400, 2, N'Asistente de finanza', NULL, 1)
GO
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (401, 2, N'Asistente de gerencia', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (402, 2, N'Asistente de gerencia general', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (403, 2, N'Asistente de geriatria', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (404, 2, N'Asistente de importaciones', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (405, 2, N'Asistente de informatica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (406, 2, N'Asistente de ingenieria industrial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (407, 2, N'Asistente de kardex', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (408, 2, N'Asistente de laboratorio', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (409, 2, N'Asistente de licitaciones', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (410, 2, N'Asistente de logistica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (411, 2, N'Asistente de mantenimiento', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (412, 2, N'Asistente de marketing', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (413, 2, N'Asistente de monitoreo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (414, 2, N'Asistente de obra', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (415, 2, N'Asistente de oficina', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (416, 2, N'Asistente de operaciones', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (417, 2, N'Asistente de pedagogia', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (418, 2, N'Asistente de personal', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (419, 2, N'Asistente de planeamiento', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (420, 2, N'Asistente de planilla', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (421, 2, N'Asistente de planta', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (422, 2, N'Asistente de plataforma', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (423, 2, N'Asistente de presupuesto', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (424, 2, N'Asistente de produccion', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (425, 2, N'Asistente de producto', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (426, 2, N'Asistente de proyectos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (427, 2, N'Asistente de rrhh', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (428, 2, N'Asistente de sistemas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (429, 2, N'Asistente de superintendente', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (430, 2, N'Asistente de supervisor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (431, 2, N'Asistente de taller', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (432, 2, N'Asistente de tesoreria', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (433, 2, N'Asistente de ventas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (434, 2, N'Asistente dental', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (435, 2, N'Asistente despacho', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (436, 2, N'Asistente financiero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (437, 2, N'Asistente legal', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (438, 2, N'Asistente social', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (439, 2, N'Atencion al cliente', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (440, 2, N'Au-pair', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (441, 2, N'Audiologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (442, 2, N'Auditor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (443, 2, N'Autocad', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (444, 2, N'Autocadista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (445, 2, N'Autor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (446, 2, N'Auxiliar', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (447, 2, N'Avicultor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (448, 2, N'Ayudante', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (449, 2, N'Ayudante de cocina', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (450, 2, N'Azafata', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (451, 2, N'Bailarin(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (452, 2, N'BaÃ±ador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (453, 2, N'Barbero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (454, 2, N'Barista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (455, 2, N'Barrenador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (456, 2, N'Bartender', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (457, 2, N'Baterista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (458, 2, N'Bibliotecario', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (459, 2, N'Bibliotecologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (460, 2, N'Bicelador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (461, 2, N'Biologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (462, 2, N'Bioquimico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (463, 2, N'Bisuterista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (464, 2, N'Bobinador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (465, 2, N'Bodeguero(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (466, 2, N'Bombero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (467, 2, N'Bordador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (468, 2, N'Botones', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (469, 2, N'Brokers', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (470, 2, N'Bromatologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (471, 2, N'Business analyst', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (472, 2, N'Buzo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (473, 2, N'Cabinero(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (474, 2, N'Cablista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (475, 2, N'Cadista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (476, 2, N'Cafetero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (477, 2, N'Cajero(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (478, 2, N'Calador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (479, 2, N'Calderero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (480, 2, N'Calderista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (481, 2, N'Calibrador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (482, 2, N'Calificador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (483, 2, N'Call center', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (484, 2, N'Camarero(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (485, 2, N'Camarografo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (486, 2, N'Cambiador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (487, 2, N'Cambista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (488, 2, N'Camiseras', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (489, 2, N'Canjeadoras', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (490, 2, N'Cantante', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (491, 2, N'Capacitador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (492, 2, N'Capataz', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (493, 2, N'Capitan', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (494, 2, N'Captador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (495, 2, N'Cardero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (496, 2, N'Cardiologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (497, 2, N'Cargador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (498, 2, N'Caricaturista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (499, 2, N'Carnicero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (500, 2, N'Carpintero', NULL, 1)
GO
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (501, 2, N'Carretilleros', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (502, 2, N'Cartografo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (503, 2, N'Casquero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (504, 2, N'Catador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (505, 2, N'Catedratico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (506, 2, N'Catequista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (507, 2, N'Cauchero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (508, 2, N'Cebichero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (509, 2, N'Censador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (510, 2, N'Cepillador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (511, 2, N'Ceramista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (512, 2, N'Cerista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (513, 2, N'Cerrajero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (514, 2, N'Certificador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (515, 2, N'Chalan', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (516, 2, N'Chaufero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (517, 2, N'Chef', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (518, 2, N'Chequeadores', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (519, 2, N'Chicharronero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (520, 2, N'Chifero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (521, 2, N'Chocolatero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (522, 2, N'Chofer', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (523, 2, N'Chompero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (524, 2, N'Churrero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (525, 2, N'Ciclista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (526, 2, N'Cientifico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (527, 2, N'Cirujano', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (528, 2, N'Clasificador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (529, 2, N'Clavero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (530, 2, N'Cobrador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (531, 2, N'Cocinero(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (532, 2, N'Colchonero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (533, 2, N'Colorista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (534, 2, N'Comediante', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (535, 2, N'Comisionista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (536, 2, N'Compaginador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (537, 2, N'Compositor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (538, 2, N'Composturero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (539, 2, N'Comprador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (540, 2, N'Comprador sennior', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (541, 2, N'Compresorista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (542, 2, N'Comunicador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (543, 2, N'Concesionaria', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (544, 2, N'Concesionario', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (545, 2, N'Conciliador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (546, 2, N'Conductor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (547, 2, N'Conero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (548, 2, N'Confeccionista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (549, 2, N'Confitero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (550, 2, N'Consejero de pares', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (551, 2, N'Conserje', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (552, 2, N'Constructor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (553, 2, N'Consultor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (554, 2, N'Contador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (555, 2, N'Contratista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (556, 2, N'Controlador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (557, 2, N'Convertidor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (558, 2, N'Coordinador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (559, 2, N'Coordinador de calidad', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (560, 2, N'Coordinador de hse', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (561, 2, N'Coordinador de servicios', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (562, 2, N'Coordinador tecnico de flotas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (563, 2, N'Corbatero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (564, 2, N'Coreografo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (565, 2, N'Corrector', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (566, 2, N'Cortador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (567, 2, N'Cortinero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (568, 2, N'Cosmetologa', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (569, 2, N'Cosmiatra', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (570, 2, N'Costurero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (571, 2, N'Cotizador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (572, 2, N'Counter', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (573, 2, N'Criador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (574, 2, N'Criptologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (575, 2, N'Crochetera', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (576, 2, N'Cromador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (577, 2, N'Crucerista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (578, 2, N'Cuartelero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (579, 2, N'Cuellero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (580, 2, N'Cuidador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (581, 2, N'Culatero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (582, 2, N'Danzante', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (583, 2, N'Database administrator', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (584, 2, N'Dealer', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (585, 2, N'Decano', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (586, 2, N'Decorador(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (587, 2, N'Degustadora', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (588, 2, N'Dentista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (589, 2, N'Dermatologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (590, 2, N'Desarrollador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (591, 2, N'Desmanchador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (592, 2, N'Despachador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (593, 2, N'Destajero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (594, 2, N'Detalladores de planos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (595, 2, N'Detective', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (596, 2, N'Devastador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (597, 2, N'Diagramador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (598, 2, N'Diamondrilista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (599, 2, N'Dibujante', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (600, 2, N'Dibujante cad', NULL, 1)
GO
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (601, 2, N'Digitador(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (602, 2, N'Diplomatico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (603, 2, N'Director', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (604, 2, N'Director medico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (605, 2, N'Disc-jockey', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (606, 2, N'DiseÃ±ador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (607, 2, N'DiseÃ±ador de interiores', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (608, 2, N'DiseÃ±ador grafico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (609, 2, N'DiseÃ±adora', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (610, 2, N'Distribuidor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (611, 2, N'Doblador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (612, 2, N'Docente', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (613, 2, N'Doctor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (614, 2, N'Domestica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (615, 2, N'Drapeador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (616, 2, N'Drywallero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (617, 2, N'Ductero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (618, 2, N'Dulcera', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (619, 2, N'Ebanista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (620, 2, N'Ecografista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (621, 2, N'Ecografo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (622, 2, N'Economista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (623, 2, N'Editor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (624, 2, N'Educador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (625, 2, N'Ejecutivo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (626, 2, N'Ejecutivo de ventas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (627, 2, N'Electricista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (628, 2, N'Electromecanico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (629, 2, N'Electronico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (630, 2, N'Electrosanitario', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (631, 2, N'Embalador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (632, 2, N'Embolsador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (633, 2, N'Emolientero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (634, 2, N'Empadronador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (635, 2, N'Empalmador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (636, 2, N'Empaquetador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (637, 2, N'Empastador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (638, 2, N'Emplantillador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (639, 2, N'Empleada', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (640, 2, N'Empresario', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (641, 2, N'Encajador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (642, 2, N'Enchapador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (643, 2, N'Encintador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (644, 2, N'Encofrador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (645, 2, N'Encuadernador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (646, 2, N'Encuestador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (647, 2, N'Endocrinologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (648, 2, N'Endodoncista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (649, 2, N'Enfermero(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (650, 2, N'Enfibrador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (651, 2, N'Enllantador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (652, 2, N'Enmalladoras', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (653, 2, N'Enmarcador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (654, 2, N'Enologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (655, 2, N'Ensaladero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (656, 2, N'Ensamblador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (657, 2, N'Ensobrador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (658, 2, N'Ensuelador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (659, 2, N'Entomologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (660, 2, N'Entrenador(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (661, 2, N'Envasador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (662, 2, N'Epidemiologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (663, 2, N'Escarchador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (664, 2, N'Escritor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (665, 2, N'Escultor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (666, 2, N'Esmerilador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (667, 2, N'Especialista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (668, 2, N'Especialista de costos y presupuesto', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (669, 2, N'Especialista en eficacia cultural', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (670, 2, N'Estadista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (671, 2, N'Estampador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (672, 2, N'Esterilizador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (673, 2, N'Esteticista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (674, 2, N'Estibador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (675, 2, N'Estilista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (676, 2, N'Estucador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (677, 2, N'Estuchero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (678, 2, N'Etiquetador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (679, 2, N'Explorador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (680, 2, N'Expositor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (681, 2, N'Extrusorista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (682, 2, N'Facialista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (683, 2, N'Facturador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (684, 2, N'Farmaceutico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (685, 2, N'Ferretero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (686, 2, N'Fibrero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (687, 2, N'Fileteador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (688, 2, N'Filmador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (689, 2, N'Filosofo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (690, 2, N'Financistas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (691, 2, N'Fisico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (692, 2, N'Fisicoculturista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (693, 2, N'Fisioterapeuta', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (694, 2, N'Fisioterapista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (695, 2, N'Flexografo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (696, 2, N'Florista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (697, 2, N'Focalizador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (698, 2, N'Forjador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (699, 2, N'Forrador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (700, 2, N'Fotocopista', NULL, 1)
GO
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (701, 2, N'Fotograbador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (702, 2, N'Fotografo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (703, 2, N'Fotolitero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (704, 2, N'Fotomecanica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (705, 2, N'Frenero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (706, 2, N'Fresador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (707, 2, N'Fumigador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (708, 2, N'Funcionario', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (709, 2, N'Fundidor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (710, 2, N'Galponero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (711, 2, N'Gasfitero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (712, 2, N'Gastroenterologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (713, 2, N'Geofisico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (714, 2, N'Geologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (715, 2, N'Geomecanico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (716, 2, N'Gerente', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (717, 2, N'Gerente comercial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (718, 2, N'Gerente de administracion', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (719, 2, N'Gerente de administracion y finanzas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (720, 2, N'Gerente de banca personal', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (721, 2, N'Gerente de capacitacion', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (722, 2, N'Gerente de compras', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (723, 2, N'Gerente de creditos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (724, 2, N'Gerente de cultura', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (725, 2, N'Gerente de division', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (726, 2, N'Gerente de exportaciones', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (727, 2, N'Gerente de ingenieros', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (728, 2, N'Gerente de investigacion', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (729, 2, N'Gerente de logistica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (730, 2, N'Gerente de mantenimiento', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (731, 2, N'Gerente de marketing', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (732, 2, N'Gerente de materiales', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (733, 2, N'Gerente de mercadeo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (734, 2, N'Gerente de obras', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (735, 2, N'Gerente de operaciones', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (736, 2, N'Gerente de planta', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (737, 2, N'Gerente de produccion', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (738, 2, N'Gerente de producto', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (739, 2, N'Gerente de proyectos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (740, 2, N'Gerente de proyectos de saneamiento', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (741, 2, N'Gerente de rrhh', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (742, 2, N'Gerente de seguridad planta', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (743, 2, N'Gerente de servicios de salud', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (744, 2, N'Gerente de sistemas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (745, 2, N'Gerente de tienda', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (746, 2, N'Gerente de ventas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (747, 2, N'Gerente general', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (748, 2, N'Gerente multinacional', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (749, 2, N'Gerente regional', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (750, 2, N'Gerente tecnico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (751, 2, N'Gerente zonal', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (752, 2, N'Geriatra', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (753, 2, N'Gerontologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (754, 2, N'Gestor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (755, 2, N'Ginecologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (756, 2, N'Gondolero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (757, 2, N'Graficador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (758, 2, N'Grafologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (759, 2, N'Grifero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (760, 2, N'Groomer', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (761, 2, N'Gruero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (762, 2, N'Guardaespalda', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (763, 2, N'Guardian', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (764, 2, N'Guia', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (765, 2, N'Guia de turismo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (766, 2, N'Guillotinero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (767, 2, N'Guitarrista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (768, 2, N'Habilitador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (769, 2, N'Heladero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (770, 2, N'Hermanador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (771, 2, N'Herrero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (772, 2, N'Hilandero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (773, 2, N'Historiador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (774, 2, N'Hojalatero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (775, 2, N'Hornero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (776, 2, N'Ilustrador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (777, 2, N'Impresor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (778, 2, N'Impulsador(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (779, 2, N'Incautador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (780, 2, N'Informatico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (781, 2, N'Ingeniero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (782, 2, N'Ingeniero agronomo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (783, 2, N'Ingeniero civil', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (784, 2, N'Ingeniero de diseÃ±o', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (785, 2, N'Ingeniero de encofrados', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (786, 2, N'Ingeniero de maquina', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (787, 2, N'Ingeniero de petroleo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (788, 2, N'Ingeniero de planeamiento', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (789, 2, N'Ingeniero de seguridad', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (790, 2, N'Ingeniero electricista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (791, 2, N'Ingeniero electronico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (792, 2, N'Ingeniero especialista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (793, 2, N'Ingeniero industrial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (794, 2, N'Ingeniero mecanico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (795, 2, N'Ingeniero metalurgico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (796, 2, N'Ingeniero minas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (797, 2, N'Ingeniero quimico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (798, 2, N'Ingeniero residente', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (799, 2, N'Ingeniero supervisor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (800, 2, N'Ingeniero zootecnista', NULL, 1)
GO
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (801, 2, N'Injertador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (802, 2, N'Insolador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (803, 2, N'Inspector', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (804, 2, N'Instalador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (805, 2, N'Institutriz', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (806, 2, N'Instructor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (807, 2, N'Instrumentista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (808, 2, N'Interprete', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (809, 2, N'Inventariador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (810, 2, N'Inversionista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (811, 2, N'Investigador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (812, 2, N'Itamae', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (813, 2, N'Jalador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (814, 2, N'Jardinero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (815, 2, N'Jede de arte', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (816, 2, N'Jefe comercial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (817, 2, N'Jefe corporativo de contratos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (818, 2, N'Jefe de administracion', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (819, 2, N'Jefe de administracion y finanzas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (820, 2, N'Jefe de almacen', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (821, 2, N'Jefe de btl', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (822, 2, N'Jefe de calidad', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (823, 2, N'Jefe de call center', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (824, 2, N'Jefe de carga', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (825, 2, N'Jefe de centro', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (826, 2, N'Jefe de cobranzas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (827, 2, N'Jefe de cocina', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (828, 2, N'Jefe de comercio exterior', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (829, 2, N'Jefe de compras', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (830, 2, N'Jefe de contabilidad', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (831, 2, N'Jefe de control de calidad', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (832, 2, N'Jefe de control de calidad y costeo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (833, 2, N'Jefe de costos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (834, 2, N'Jefe de costos e inventarios', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (835, 2, N'Jefe de creditos y cobranzas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (836, 2, N'Jefe de crm', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (837, 2, N'Jefe de delivery', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (838, 2, N'Jefe de deportes', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (839, 2, N'Jefe de despacho', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (840, 2, N'Jefe de diseÃ±o mecanico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (841, 2, N'Jefe de distribucion', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (842, 2, N'Jefe de division comercial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (843, 2, N'Jefe de division de carga', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (844, 2, N'Jefe de equipos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (845, 2, N'Jefe de estacion de servicio', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (846, 2, N'Jefe de eventos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (847, 2, N'Jefe de exportaciones', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (848, 2, N'Jefe de finanzas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (849, 2, N'Jefe de flota', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (850, 2, N'Jefe de fundo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (851, 2, N'Jefe de gestion', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (852, 2, N'Jefe de guardia', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (853, 2, N'Jefe de imagen y publicidad', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (854, 2, N'Jefe de informatica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (855, 2, N'Jefe de ingenieria', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (856, 2, N'Jefe de laboratorio', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (857, 2, N'Jefe de lavanderia', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (858, 2, N'Jefe de linea', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (859, 2, N'Jefe de logistica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (860, 2, N'Jefe de maestranza', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (861, 2, N'Jefe de mantenimiento', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (862, 2, N'Jefe de marca', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (863, 2, N'Jefe de marketing', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (864, 2, N'Jefe de merchandinsing', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (865, 2, N'Jefe de mozos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (866, 2, N'Jefe de obra', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (867, 2, N'Jefe de oficina tecnica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (868, 2, N'Jefe de operaciones', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (869, 2, N'Jefe de personal', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (870, 2, N'Jefe de planeamiento logistico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (871, 2, N'Jefe de planeamiento y desarrollo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (872, 2, N'Jefe de planilla', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (873, 2, N'Jefe de planta', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (874, 2, N'Jefe de produccion', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (875, 2, N'Jefe de producto', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (876, 2, N'Jefe de proyectos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (877, 2, N'Jefe de recepcion', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (878, 2, N'Jefe de riesgos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (879, 2, N'Jefe de rrhh', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (880, 2, N'Jefe de sala', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (881, 2, N'Jefe de seguridad', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (882, 2, N'Jefe de seleccion y reclutamiento de personal', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (883, 2, N'Jefe de servicio al cliente', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (884, 2, N'Jefe de servicio tecnico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (885, 2, N'Jefe de servicios corporativos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (886, 2, N'Jefe de sistemas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (887, 2, N'Jefe de taller', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (888, 2, N'Jefe de tejeduria', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (889, 2, N'Jefe de tesoreria', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (890, 2, N'Jefe de tienda', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (891, 2, N'Jefe de transportes', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (892, 2, N'Jefe de ventas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (893, 2, N'Jefe de ventilacion', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (894, 2, N'Jefe de zona', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (895, 2, N'Jefe quimico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (896, 2, N'Jefe regional', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (897, 2, N'Joyero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (898, 2, N'Juguero(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (899, 2, N'Kardista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (900, 2, N'Kinesiologo', NULL, 1)
GO
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (901, 2, N'Laboratorista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (902, 2, N'Laminador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (903, 2, N'Laqueador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (904, 2, N'Lavacarro', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (905, 2, N'Lavador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (906, 2, N'Lavamuebles', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (907, 2, N'Lavandera', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (908, 2, N'Lavaplatos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (909, 2, N'Lectores', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (910, 2, N'Licitador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (911, 2, N'Lijador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (912, 2, N'Limpiador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (913, 2, N'Linguista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (914, 2, N'Liniero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (915, 2, N'Liquidador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (916, 2, N'Litografo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (917, 2, N'Llamador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (918, 2, N'Llantero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (919, 2, N'Locutor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (920, 2, N'Logistico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (921, 2, N'Lubricador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (922, 2, N'Luchador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (923, 2, N'Lustrabotas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (924, 2, N'Maestro', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (925, 2, N'Maitre', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (926, 2, N'Manager', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (927, 2, N'Mandrilador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (928, 2, N'Manicurista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (929, 2, N'Maniobrista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (930, 2, N'Manualista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (931, 2, N'Maquilladora', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (932, 2, N'Maquinista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (933, 2, N'Marinero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (934, 2, N'Marketero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (935, 2, N'Marmolero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (936, 2, N'Marquero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (937, 2, N'Martillero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (938, 2, N'Masajista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (939, 2, N'Masillador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (940, 2, N'Masoterapista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (941, 2, N'Matematico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (942, 2, N'Matizador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (943, 2, N'Matricero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (944, 2, N'Mayoliquero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (945, 2, N'Mayordomo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (946, 2, N'Mecanico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (947, 2, N'Mecanografo(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (948, 2, N'Mecatronico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (949, 2, N'Medico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (950, 2, N'Medico oncologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (951, 2, N'Medico veterinario', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (952, 2, N'Melaminero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (953, 2, N'Mensajero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (954, 2, N'Mercaderista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (955, 2, N'Mesero(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (956, 2, N'Metalizador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (957, 2, N'Metalmecanica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (958, 2, N'Metalurgista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (959, 2, N'Meteorologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (960, 2, N'Metrador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (961, 2, N'Microbiologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (962, 2, N'Microexportador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (963, 2, N'Milanecero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (964, 2, N'Mimos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (965, 2, N'Minero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (966, 2, N'Mochilero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (967, 2, N'Modelero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (968, 2, N'Modelista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (969, 2, N'Modelo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (970, 2, N'Moldeador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (971, 2, N'Moldista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (972, 2, N'Molinero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (973, 2, N'Monitor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (974, 2, N'Montacarguista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (975, 2, N'Montajista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (976, 2, N'Motociclista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (977, 2, N'Motonivelador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (978, 2, N'Motorista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (979, 2, N'Motorizado', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (980, 2, N'Mozo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (981, 2, N'Muestrista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (982, 2, N'Musico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (983, 2, N'Nana', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (984, 2, N'Narrador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (985, 2, N'Naturista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (986, 2, N'Nefrologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (987, 2, N'Neumologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (988, 2, N'Neurocirujano', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (989, 2, N'Neurologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (990, 2, N'Neuropsicologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (991, 2, N'Neuropsiquiatra', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (992, 2, N'NiÃ±era', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (993, 2, N'Notario', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (994, 2, N'Notificador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (995, 2, N'Numerador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (996, 2, N'Nutricionista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (997, 2, N'Obrero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (998, 2, N'Obstetriz', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (999, 2, N'Oculista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1000, 2, N'Odontologo', NULL, 1)
GO
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1001, 2, N'Odontopediatra', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1002, 2, N'Oficial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1003, 2, N'Oficinista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1004, 2, N'Oftalmologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1005, 2, N'Ojalador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1006, 2, N'Omeopata', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1007, 2, N'Operador(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1008, 2, N'Operario', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1009, 2, N'Optometra', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1010, 2, N'Orfebre', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1011, 2, N'Organista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1012, 2, N'Organizadora de eventos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1013, 2, N'Ortodoncista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1014, 2, N'Otorrino', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1015, 2, N'Otros', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1016, 2, N'Oxigenista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1017, 2, N'Pagador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1018, 2, N'Panadero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1019, 2, N'Panetonero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1020, 2, N'Panificador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1021, 2, N'Pantallero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1022, 2, N'Pantalonero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1023, 2, N'Pantografista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1024, 2, N'Paramedico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1025, 2, N'Parqueador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1026, 2, N'Parquetero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1027, 2, N'Parrillero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1028, 2, N'Pastelero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1029, 2, N'Patologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1030, 2, N'Patron', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1031, 2, N'Patronista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1032, 2, N'Payaso', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1033, 2, N'Pecherista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1034, 2, N'Pedagogo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1035, 2, N'Pediatra', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1036, 2, N'Pedicurista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1037, 2, N'Peinador(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1038, 2, N'Pelador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1039, 2, N'Peletizador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1040, 2, N'Peluquero(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1041, 2, N'Peon', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1042, 2, N'Percherista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1043, 2, N'Percusionista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1044, 2, N'Perforista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1045, 2, N'Periodista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1046, 2, N'Periodoncista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1047, 2, N'Perito', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1048, 2, N'Pescador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1049, 2, N'Pianista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1050, 2, N'Picador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1051, 2, N'Picapiedrero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1052, 2, N'Picaronera', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1053, 2, N'Pigmentador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1054, 2, N'Piloto', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1055, 2, N'PiÃ±atero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1056, 2, N'Pinta caritas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1057, 2, N'Pintor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1058, 2, N'Pizzero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1059, 2, N'Planchador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1060, 2, N'Planificador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1061, 2, N'Planillero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1062, 2, N'Planner', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1063, 2, N'Plantillador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1064, 2, N'Plastificador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1065, 2, N'Platero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1066, 2, N'Platillador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1067, 2, N'Platillero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1068, 2, N'Plegador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1069, 2, N'Plisador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1070, 2, N'Plisadora', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1071, 2, N'Plomero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1072, 2, N'Ploteador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1073, 2, N'Podador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1074, 2, N'Podologo(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1075, 2, N'Polaquero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1076, 2, N'Policia', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1077, 2, N'Poliestador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1078, 2, N'Pollero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1079, 2, N'Ponchador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1080, 2, N'Portero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1081, 2, N'Practicante', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1082, 2, N'Pre-prensista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1083, 2, N'Pre-vendedores', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1084, 2, N'Pre-ventistas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1085, 2, N'Predicador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1086, 2, N'Prensador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1087, 2, N'Prensista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1088, 2, N'Presupuestador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1089, 2, N'Pretinero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1090, 2, N'Prevencionista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1091, 2, N'Previsionista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1092, 2, N'Procurador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1093, 2, N'Productor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1094, 2, N'Profesor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1095, 2, N'Programador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1096, 2, N'Promotor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1097, 2, N'Propagandista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1098, 2, N'Proveedor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1099, 2, N'Proyectista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1100, 2, N'Psicologo(a)', NULL, 1)
GO
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1101, 2, N'Psicopedagogo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1102, 2, N'Psicoterapeuta', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1103, 2, N'Psiquiatra', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1104, 2, N'Publicista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1105, 2, N'Pulidor(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1106, 2, N'Punchador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1107, 2, N'Quimico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1108, 2, N'Quimicos farmaceuticos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1109, 2, N'Quiropractico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1110, 2, N'Radiestesista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1111, 2, N'Radiologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1112, 2, N'Radioperador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1113, 2, N'Raviolero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1114, 2, N'Reactivista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1115, 2, N'Rebobinador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1116, 2, N'Recaudador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1117, 2, N'Recepcionista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1118, 2, N'Recibidor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1119, 2, N'Reclutador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1120, 2, N'Rectificador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1121, 2, N'Rectista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1122, 2, N'Recubridor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1123, 2, N'Recuperador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1124, 2, N'Redactor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1125, 2, N'Reflexologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1126, 2, N'Refractorista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1127, 2, N'Regente', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1128, 2, N'Regulador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1129, 2, N'Rehabilitador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1130, 2, N'Relacionador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1131, 2, N'Relacionista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1132, 2, N'Relacionista publica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1133, 2, N'Relojero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1134, 2, N'Remachador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1135, 2, N'Remallador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1136, 2, N'Repartidor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1137, 2, N'Reponedor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1138, 2, N'Reportero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1139, 2, N'Repostera', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1140, 2, N'Representante', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1141, 2, N'Representante de inversion y desarrollo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1142, 2, N'Representante de ventas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1143, 2, N'Repuestero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1144, 2, N'Repujador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1145, 2, N'Resguardo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1146, 2, N'Residente', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1147, 2, N'Residente de obra', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1148, 2, N'Responsable de comercio exterior', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1149, 2, N'Responsable de registros', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1150, 2, N'Responsable de semillas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1151, 2, N'Restaurador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1152, 2, N'Reumatologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1153, 2, N'Revelador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1154, 2, N'Revisador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1155, 2, N'Ribeteador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1156, 2, N'Rigger', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1157, 2, N'Rolador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1158, 2, N'Rotulador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1159, 2, N'Rotulista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1160, 2, N'Ruteador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1161, 2, N'Sacador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1162, 2, N'Salvavidas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1163, 2, N'Sandwichero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1164, 2, N'Sastre', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1165, 2, N'Saxofonista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1166, 2, N'Secador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1167, 2, N'Secretaria', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1168, 2, N'Secretario', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1169, 2, N'Sectorista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1170, 2, N'Seguridad', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1171, 2, N'Sellador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1172, 2, N'Sereno', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1173, 2, N'Serigrafista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1174, 2, N'Sexologa', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1175, 2, N'Socio', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1176, 2, N'Sociologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1177, 2, N'Soldador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1178, 2, N'Sommelier', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1179, 2, N'Sonidista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1180, 2, N'Soplador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1181, 2, N'Stripper', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1182, 2, N'Sub contador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1183, 2, N'Sub coordinador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1184, 2, N'Sub gerente comercial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1185, 2, N'Sub gerente de administracion y finanzas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1186, 2, N'Sub gerente de colocaciones', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1187, 2, N'Sub gerente de desarrollo de negocios', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1188, 2, N'Sub gerente de informatica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1189, 2, N'Sub gerente de logistica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1190, 2, N'Sub gerente de mantenimiento', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1191, 2, N'Sub gerente de marketing', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1192, 2, N'Sub gerente de obra', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1193, 2, N'Sub gerente de operaciones', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1194, 2, N'Sub gerente de personal', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1195, 2, N'Sub gerente de planta', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1196, 2, N'Sub gerente de producto', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1197, 2, N'Sub gerente de proyectos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1198, 2, N'Sub gerente de rrhh', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1199, 2, N'Sub gerente de seleccion y reclutamiento personal', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1200, 2, N'Sub gerente tecnico', NULL, 1)
GO
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1201, 2, N'Sublimador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1202, 2, N'Superintendente civil', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1203, 2, N'Superintendente de geomecanica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1204, 2, N'Superintendente de mina', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1205, 2, N'Superintendente de planta', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1206, 2, N'Superintendente de recursos humanos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1207, 2, N'Supervisor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1208, 2, N'Supervisor de administracion', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1209, 2, N'Supervisor de compras', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1210, 2, N'Supervisor de hse', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1211, 2, N'Surcidora', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1212, 2, N'Surfer', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1213, 2, N'Systems support supervisor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1214, 2, N'Tabiquero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1215, 2, N'Tallador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1216, 2, N'Tapetero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1217, 2, N'Tapicero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1218, 2, N'Taquimecanografista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1219, 2, N'Tarotista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1220, 2, N'Tasador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1221, 2, N'Tatuador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1222, 2, N'Taxista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1223, 2, N'Teacher', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1224, 2, N'Techador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1225, 2, N'Technical support', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1226, 2, N'Tecladista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1227, 2, N'Tecnico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1228, 2, N'Tecnico en alineamiento y enllante', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1229, 2, N'Tecnico en comunicaciones', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1230, 2, N'Tecnico en refrigeracion', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1231, 2, N'Tecnico maquinarias', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1232, 2, N'Tecnicos en farmacia', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1233, 2, N'Tecnologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1234, 2, N'Tejedor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1235, 2, N'Telecobrador(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1236, 2, N'Telefonista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1237, 2, N'Telegestor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1238, 2, N'Telemarketero(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1239, 2, N'Telemarketers', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1240, 2, N'Teleoperador(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1241, 2, N'Televendedor(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1242, 2, N'Tendedores', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1243, 2, N'TeÃ±idor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1244, 2, N'Terapeuta', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1245, 2, N'Terapista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1246, 2, N'Termofusionista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1247, 2, N'Termosellador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1248, 2, N'Terracero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1249, 2, N'Terramoza', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1250, 2, N'Tesorero(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1251, 2, N'Textilero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1252, 2, N'Tintorero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1253, 2, N'Tipeadora', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1254, 2, N'Tipografo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1255, 2, N'Tizador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1256, 2, N'Toldero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1257, 2, N'Topografo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1258, 2, N'Tornero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1259, 2, N'Trabajador social', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1260, 2, N'Tractorista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1261, 2, N'Trade marketing', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1262, 2, N'Traductor(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1263, 2, N'Trailero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1264, 2, N'Trainer', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1265, 2, N'Training regional', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1266, 2, N'Tramitador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1267, 2, N'Transcriptor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1268, 2, N'Transferencista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1269, 2, N'Translator', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1270, 2, N'Transportista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1271, 2, N'Traumatologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1272, 2, N'Trazadores', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1273, 2, N'Tributarista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1274, 2, N'Trinquero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1275, 2, N'Tripulante', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1276, 2, N'Tripulante de cabina', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1277, 2, N'Tripulante de cubierta', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1278, 2, N'Troquelador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1279, 2, N'Trozador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1280, 2, N'Tubero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1281, 2, N'Tutor(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1282, 2, N'Urdidor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1283, 2, N'Urologo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1284, 2, N'Vajillero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1285, 2, N'Valet', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1286, 2, N'Vaporizador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1287, 2, N'Vendedor(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1288, 2, N'Verificador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1289, 2, N'Veterinario', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1290, 2, N'Vidriero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1291, 2, N'Vigilante', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1292, 2, N'Vinilero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1293, 2, N'Violinista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1294, 2, N'Visitador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1295, 2, N'Visitador medico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1296, 2, N'Vitralista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1297, 2, N'Vocalista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1298, 2, N'Vocero institucional', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1299, 2, N'Volantero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1300, 2, N'Volquetero', NULL, 1)
GO
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1301, 2, N'Voluntario', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1302, 2, N'Vulcanizador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1303, 2, N'Webmaster', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1304, 2, N'Zapatero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1305, 2, N'Zapatillero', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1306, 2, N'Zincador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1307, 2, N'Zonificador', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1308, 2, N'Zootecnista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1309, 2, N'Zurcidora', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1310, 3, N'Actuación', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1311, 3, N'Administración', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1312, 3, N'Administración Bancaria', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1313, 3, N'Administración de Servicios', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1314, 3, N'Administrador Industrial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1315, 3, N'Agrobiotecnología', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1316, 3, N'Agronegocios Internacionales', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1317, 3, N'Agronomía', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1318, 3, N'Aire Acondicionado', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1319, 3, N'Animación y Arte Digital', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1320, 3, N'Antropología', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1321, 3, N'Arqueoarquitectura y Gestión Turística', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1322, 3, N'Arqueología', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1323, 3, N'Arquitectura', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1324, 3, N'Arquitectura de Interiores', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1325, 3, N'Arte', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1326, 3, N'Arte Culinario', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1327, 3, N'Artes Dramáticas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1328, 3, N'Artes Escénicas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1329, 3, N'Asistente del Adulto Mayor', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1330, 3, N'Auditoría', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1331, 3, N'Auditoria Empresarial y del Sector Público', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1332, 3, N'Automatización Industrial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1333, 3, N'Ballet', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1334, 3, N'Bar, Cocteleria y Servicios de Mesa', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1335, 3, N'Bibliotecología', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1336, 3, N'Biología', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1337, 3, N'Bioquímica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1338, 3, N'Biotecnología', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1339, 3, N'Carpintería Industrial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1340, 3, N'Ciencia de la Información', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1341, 3, N'Ciencia de los Alimentos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1342, 3, N'Ciencias Biológicas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1343, 3, N'Ciencias de la Comunicación', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1344, 3, N'Ciencias del Deporte', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1345, 3, N'Ciencias Políticas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1346, 3, N'Ciencias Químicas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1347, 3, N'Cocina', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1348, 3, N'Comercio Internacional', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1349, 3, N'Composición', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1350, 3, N'Computación', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1351, 3, N'Computación Científica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1352, 3, N'Computación e Informática', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1353, 3, N'Comunicación', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1354, 3, N'Comunicación Audiovisual', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1355, 3, N'Comunicacion de Datos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1356, 3, N'Comunicacion para el desarrollo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1357, 3, N'Comunicación social', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1358, 3, N'Confecciones Textiles', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1359, 3, N'Conservación y Restauración', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1360, 3, N'Contabilidad', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1361, 3, N'Control de Máquinas y Procesos Industriales', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1362, 3, N'Cooperativismo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1363, 3, N'Cosmetología', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1364, 3, N'Cosmiatría', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1365, 3, N'Counter Profesional', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1366, 3, N'Danza', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1367, 3, N'Derecho', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1368, 3, N'Derecho del Mar y Servicios Aduaneros', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1369, 3, N'Dibujo Técnico Mecánico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1370, 3, N'Dietética y Nutrición', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1371, 3, N'Dirección Coral', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1372, 3, N'Dirección de Artes Gráficas y Publicitarias', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1373, 3, N'Diseño de Interiores', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1374, 3, N'Diseño de Máquinas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1375, 3, N'Diseño de Modas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1376, 3, N'Diseño Escenográfico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1377, 3, N'Diseño Gráfico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1378, 3, N'Diseño Industrial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1379, 3, N'Diseño Publicitario', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1380, 3, N'Economía', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1381, 3, N'Economía Internacional', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1382, 3, N'Economía Pública', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1383, 3, N'Educación', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1384, 3, N'Educación Artística', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1385, 3, N'Educación en Arte Dramático', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1386, 3, N'Educación Física', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1387, 3, N'Educación Musical', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1388, 3, N'Electricidad Automotríz', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1389, 3, N'Electricidad Industrial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1390, 3, N'Electrónica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1391, 3, N'Electrónica y Automatización Industrial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1392, 3, N'Electrotécnica Industrial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1393, 3, N'Enfermería', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1394, 3, N'Ensamblaje y Reparación de Computadoras', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1395, 3, N'Escenografía', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1396, 3, N'Escultura', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1397, 3, N'Estadística', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1398, 3, N'Estadística e Informática', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1399, 3, N'Estomatología', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1400, 3, N'Farmacia', NULL, 1)
GO
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1401, 3, N'Filosofía', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1402, 3, N'Finanzas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1403, 3, N'Física', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1404, 3, N'Fisioterapia y Rehabilitación', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1405, 3, N'Fotografía', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1406, 3, N'Fotomecánica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1407, 3, N'Gastronomía', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1408, 3, N'Genética', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1409, 3, N'Geografía', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1410, 3, N'Gerontología', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1411, 3, N'Gestión Comercial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1412, 3, N'Gestión de Puertos y Aduanas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1413, 3, N'Gestión Tributaria', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1414, 3, N'Grabado', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1415, 3, N'Historia', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1416, 3, N'Hotelería', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1417, 3, N'Humanidades y Ciencias Sociales', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1418, 3, N'Impresión Offset', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1419, 3, N'Industrias Alimentarias', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1420, 3, N'Informática', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1421, 3, N'Ingeniería Agrícola', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1422, 3, N'Ingeniería Agroindustrial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1423, 3, N'Ingeniería Alimentaría', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1424, 3, N'Ingeniería Ambiental', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1425, 3, N'Ingeniería Biomédica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1426, 3, N'Ingeniería Civil', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1427, 3, N'Ingeniería Comercial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1428, 3, N'Ingeniería de Computación y Sistemas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1429, 3, N'Ingeniería de Desarrollo de Juegos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1430, 3, N'Ingeniería de Higiene y Seguridad Industrial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1431, 3, N'Ingeniería de la Energía', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1432, 3, N'Ingeniería de la Producción e Industrialización de Recursos Hidrobiológicos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1433, 3, N'Ingeniería de Materiales', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1434, 3, N'Ingeniería de Minas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1435, 3, N'Ingeniería de Navegación y Marina Mercante', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1436, 3, N'Ingeniería de Negocios Agro-Forestales', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1437, 3, N'Ingeniería de Petróleo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1438, 3, N'Ingenieria de Redes', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1439, 3, N'Ingeniería de Sistemas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1440, 3, N'Ingeniería de Software', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1441, 3, N'Ingeniería de Telecomunicaciones', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1442, 3, N'Ingeniería de Telecomunicaciones y Telemática', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1443, 3, N'Ingeniería de Transporte Marítimo y Gestión Logística Portuaria', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1444, 3, N'Ingeniería Económica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1445, 3, N'Ingeniería Eléctrica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1446, 3, N'Ingeniería Electrónica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1447, 3, N'Ingeniería en Gestión Empresarial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1448, 3, N'Ingeniería Estadistica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1449, 3, N'Ingeniería Física', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1450, 3, N'Ingenieria Físico Industrial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1451, 3, N'Ingeniería Forestal', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1452, 3, N'Ingeniería Geográfica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1453, 3, N'Ingeniería Geológica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1454, 3, N'Ingeniería Industrial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1455, 3, N'Ingeniería Informática', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1456, 3, N'Ingeniería Mecánica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1457, 3, N'Ingenieria Mecánica de Fluidos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1458, 3, N'Ingeniería Mecatrónica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1459, 3, N'Ingeniería Metalúrgica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1460, 3, N'Ingeniería Naval', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1461, 3, N'Ingeniería Petroquímica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1462, 3, N'Ingeniería Química', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1463, 3, N'Ingeniería Sanitaria', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1464, 3, N'Ingeniería Textil', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1465, 3, N'Interpretación', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1466, 3, N'Investigación Operativa', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1467, 3, N'Joyería Orfebre', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1468, 3, N'Laboratorio Clínico', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1469, 3, N'Lengua y Literatura Hispánicas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1470, 3, N'Lingüística', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1471, 3, N'Literatura', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1472, 3, N'Logística Internacional', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1473, 3, N'Mantenimiento de Maquinaria de Planta', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1474, 3, N'Mantenimiento de Maquinaria Pesada', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1475, 3, N'Marketing', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1476, 3, N'Matemática', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1477, 3, N'Matricería', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1478, 3, N'Mecánica Automotríz', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1479, 3, N'Mecánica de Automotores', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1480, 3, N'Mecánica de Construcciones Metálicas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1481, 3, N'Mecánica de Mantenimiento', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1482, 3, N'Mecánica de Mantenimiento de Máquinas de Confección Textil', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1483, 3, N'Mecánica de Maquinas Herramientas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1484, 3, N'Mecánica de Producción', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1485, 3, N'Mecánica de Refrigeración y Aire acondicionado', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1486, 3, N'Mecánica Textil en Género de punto', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1487, 3, N'Mecánica Textil en Tejeduria Plana', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1488, 3, N'Medicina Humana', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1489, 3, N'Medio Ambiente', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1490, 3, N'Mercadotecnia', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1491, 3, N'Metalurgia', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1492, 3, N'Microbiología', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1493, 3, N'Músico Instrumentista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1494, 3, N'Musicología', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1495, 3, N'Negocios', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1496, 3, N'Negocios Internacionales', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1497, 3, N'Nutrición', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1498, 3, N'Obstetricia', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1499, 3, N'Odontología', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1500, 3, N'Operación de Procesos de la Industria Alimentaria', NULL, 1)
GO
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1501, 3, N'Panadería', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1502, 3, N'Panificación Industrial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1503, 3, N'Parasitología', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1504, 3, N'Pastelería', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1505, 3, N'Periodismo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1506, 3, N'Pesquería', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1507, 3, N'Piloto Comercial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1508, 3, N'Pintura', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1509, 3, N'Políticas Públicas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1510, 3, N'Proceso de Producción', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1511, 3, N'Procesos Industriales', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1512, 3, N'Procesos Químicos y Metalúrgicos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1513, 3, N'Prótesis Dental', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1514, 3, N'Psicología', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1515, 3, N'Publicidad', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1516, 3, N'Química', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1517, 3, N'Química Farmacéutica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1518, 3, N'Química Textil', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1519, 3, N'Recursos Humanos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1520, 3, N'Redes y Comunicación de Datos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1521, 3, N'Relaciones Internacionales', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1522, 3, N'Secretariado Ejecutivo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1523, 3, N'Serigrafía', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1524, 3, N'Sistemas de Información', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1525, 3, N'Sociología', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1526, 3, N'Soldadura Universal', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1527, 3, N'Sommelier', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1528, 3, N'Tecnología Agrícola', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1529, 3, N'Tecnología de la Producción', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1530, 3, N'Tecnología del Vestido', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1531, 3, N'Tecnología Mecánica Eléctrica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1532, 3, N'Tecnología Medica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1533, 3, N'Tecnología Textil', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1534, 3, N'Tecnologías de Información', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1535, 3, N'Tecnologías Electrónicas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1536, 3, N'Teología', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1537, 3, N'Terapia Física Alternativa', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1538, 3, N'Toxicología', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1539, 3, N'Trabajo Social', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1540, 3, N'Traducción', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1541, 3, N'Tripulante Auxiliar', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1542, 3, N'Turismo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1543, 3, N'Urbanismo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1544, 3, N'Veterinaria', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1545, 3, N'Zootecnia', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1546, 10, N'Aeronaves / Astilleros', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1547, 10, N'Agrícola / Ganadera / Agroindustria', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1548, 10, N'Agua y Alcantarillado / Obras Sanitarias', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1549, 10, N'Arquitectura / Diseño / Decoración', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1550, 10, N'Automotriz', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1551, 10, N'Autopistas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1552, 10, N'Bancos / Financieras', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1553, 10, N'Carpintería / Muebles', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1554, 10, N'Científica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1555, 10, N'Combustibles (Gas / Petróleo)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1556, 10, N'Comercial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1557, 10, N'Comercio Mayorista (Intermediac. / Dist.)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1558, 10, N'Comercio Minorista', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1559, 10, N'Compañía de Seguridad', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1560, 10, N'Confecciones', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1561, 10, N'Construcción', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1562, 10, N'Consultor en RRHH', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1563, 10, N'Consultoría / Asesoría', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1564, 10, N'Consumo Masivo (Bebidas / Alimentos / etc.)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1565, 10, N'Cuidado Personal / Cuidado del Hogar / Alimentos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1566, 10, N'Defensa', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1567, 10, N'Educación - Tecnología - IT', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1568, 10, N'Educación / Capacitación', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1569, 10, N'Electricidad', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1570, 10, N'Electricidad (Distribución / Transmisión)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1571, 10, N'Electricidad (Generación)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1572, 10, N'Electrónica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1573, 10, N'Entretenimiento', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1574, 10, N'Estudios Jurídicos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1575, 10, N'Exportación / Importación / Comercio exterior', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1576, 10, N'Farmacéutica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1577, 10, N'Forestal / Papel / Celulosa', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1578, 10, N'Generación de Energía', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1579, 10, N'Gobierno', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1580, 10, N'Head-Hunters', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1581, 10, N'Hotelería / Turismo / Restaurantes', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1582, 10, N'Imprenta / Editoriales', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1583, 10, N'Industrial', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1584, 10, N'Ingeniería', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1585, 10, N'Inmobiliaria / Propiedades', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1586, 10, N'Internet', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1587, 10, N'Inversiones (Soc. / Cías. Holding)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1588, 10, N'Investigación de Mercado', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1589, 10, N'Laboratorio', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1590, 10, N'Logística / Distribución', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1591, 10, N'Manufacturas Varias', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1592, 10, N'MAQUINARIA', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1593, 10, N'Materiales de Construcción', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1594, 10, N'Medicina / Salud', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1595, 10, N'Medios de Comunicación', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1596, 10, N'Metalmecánica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1597, 10, N'Minería', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1598, 10, N'Naviera', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1599, 10, N'Organizaciones sin Fines de Lucro / ONG', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1600, 10, N'Otros', NULL, 1)
GO
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1601, 10, N'Outsourcing', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1602, 10, N'Pesquera / Cultivos Marinos', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1603, 10, N'Previsión', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1604, 10, N'Publicidad / Marketing / RRPP', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1605, 10, N'Química', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1606, 10, N'Retail', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1607, 10, N'Seguros / Previsión', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1608, 10, N'Selección de Personal', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1609, 10, N'Servicios', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1610, 10, N'Servicios de Gestión Operativa', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1611, 10, N'Servicios profesionales', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1612, 10, N'Siderurgia', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1613, 10, N'Sistemas', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1614, 10, N'Telecomunicaciones', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1615, 10, N'Textil', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1616, 10, N'Transporte', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1617, 10, N'Venta por Catálogo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1618, 19, N'Prácticas Pre-Profesionales', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1619, 19, N'Tiempo Completo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1620, 19, N'Medio Tiempo', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1621, 20, N'Soltero(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1622, 20, N'Casado(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1623, 20, N'Viudo(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1624, 20, N'Divorciado(a)', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1625, 21, N'Especializacion', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1626, 21, N'Diplomado', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1627, 21, N'Maestría', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1628, 22, N'Pública', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1629, 22, N'Privada', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1631, 22, N'Publica y Privada', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1632, 23, N'Publicar', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1633, 23, N'No Publicar', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1634, 24, N'Otros', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1635, 25, N'Califica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1636, 25, N'No Califica', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1637, 26, N'Completa', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1638, 26, N'Incompleta', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1639, 27, N'Titulado', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1640, 27, N'Egresado', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1641, 27, N'En curso', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1642, 27, N'Incompleta', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1643, 28, N'Titulado', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1644, 28, N'Bachiller', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1645, 28, N'Egresado', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1646, 28, N'En curso', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1647, 28, N'Incompleta', NULL, 1)
INSERT [dbo].[tbMaestroListas] ([cnID_Lista], [cnID_GrupoLista], [ctElemento], [cnOrden], [cnEstado]) VALUES (1648, 29, N'Ninguno', NULL, 1)
SET IDENTITY_INSERT [dbo].[tbMaestroListas] OFF

SET IDENTITY_INSERT [dbo].[tbPais] ON 
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (1, N'AFGANISTÁN', N'004')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (2, N'ALBANIA', N'008')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (3, N'ALEMANIA', N'276')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (4, N'ANDORRA', N'020')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (5, N'ANGOLA', N'024')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (6, N'ANGUILA', N'660')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (7, N'ANTÁRTIDA', N'010')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (8, N'ANTIGUA Y BARBUDA', N'028')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (9, N'ANTILLAS NEERLANDESAS', N'530')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (10, N'ARABIA SAUDITA', N'682')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (11, N'ARGEL', N'012')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (12, N'ARGENTINA', N'032')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (13, N'ARMENIA', N'051')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (14, N'ARUBA', N'533')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (15, N'AUSTRALIA', N'036')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (16, N'AUSTRIA', N'040')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (17, N'AZERBAIYÁN', N'031')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (18, N'BAHAMAS', N'044')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (19, N'BAHRÉIN', N'048')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (20, N'BANGLADESH', N'050')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (21, N'BARBADOS', N'052')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (22, N'BELARÚS', N'112')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (23, N'BÉLGICA', N'056')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (24, N'BELICE', N'084')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (25, N'BENIN', N'204')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (26, N'BERMUDAS', N'060')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (27, N'BHUTÁN', N'064')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (28, N'BOLIVIA', N'068')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (29, N'BOSNIA Y HERZEGOVINA', N'070')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (30, N'BOTSUANA', N'072')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (31, N'BRASIL', N'076')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (32, N'BRUNÉI', N'096')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (33, N'BULGARIA', N'100')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (34, N'BURKINA FASO', N'854')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (35, N'BURUNDI', N'108')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (36, N'CABO VERDE', N'132')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (37, N'CAMBOYA', N'116')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (38, N'CAMERÚN', N'120')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (39, N'CANADÁ', N'124')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (40, N'CHAD', N'148')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (41, N'CHILE', N'152')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (42, N'CHINA', N'156')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (43, N'CHIPRE', N'196')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (44, N'CIUDAD DEL VATICANO', N'336')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (45, N'COLOMBIA', N'170')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (46, N'COMOROS', N'174')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (47, N'CONGO', N'178')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (48, N'COREA DEL NORTE', N'408')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (49, N'COREA DEL SUR', N'410')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (50, N'COSTA DE MARFIL', N'384')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (51, N'COSTA RICA', N'188')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (52, N'CROACIA', N'191')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (53, N'CUBA', N'192')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (54, N'DINAMARCA', N'208')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (55, N'DOMÍNICA', N'212')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (56, N'ECUADOR', N'218')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (57, N'EGIPTO', N'818')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (58, N'EL SALVADOR', N'222')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (59, N'EMIRATOS ÁRABES UNIDOS', N'784')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (60, N'ERITREA', N'232')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (61, N'ESLOVAQUIA', N'703')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (62, N'ESLOVENIA', N'705')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (63, N'ESPAÑA', N'724')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (64, N'ESTADOS UNIDOS DE AMÉRICA', N'840')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (65, N'ESTONIA', N'233')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (66, N'ETIOPÍA', N'231')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (67, N'FIJI', N'242')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (68, N'FILIPINAS', N'608')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (69, N'FINLANDIA', N'246')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (70, N'FRANCIA', N'250')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (71, N'GABÓN', N'266')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (72, N'GAMBIA', N'270')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (73, N'GEORGIA', N'268')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (74, N'GEORGIA DEL SUR E ISLAS SANDWICH DEL SUR', N'239')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (75, N'GHANA', N'288')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (76, N'GIBRALTAR', N'292')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (77, N'GRANADA', N'308')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (78, N'GRECIA', N'300')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (79, N'GROENLANDIA', N'304')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (80, N'GUADALUPE', N'312')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (81, N'GUAM', N'316')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (82, N'GUATEMALA', N'320')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (83, N'GUAYANA', N'328')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (84, N'GUAYANA FRANCESA', N'254')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (85, N'GUERNSEY', N'831')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (86, N'GUINEA', N'324')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (87, N'GUINEA ECUATORIAL', N'226')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (88, N'GUINEA-BISSAU', N'624')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (89, N'HAITÍ', N'332')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (90, N'HONDURAS', N'340')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (91, N'HONG KONG', N'344')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (92, N'HUNGRÍA', N'348')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (93, N'INDIA', N'356')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (94, N'INDONESIA', N'360')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (95, N'IRAK', N'368')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (96, N'IRÁN', N'364')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (97, N'IRLANDA', N'372')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (98, N'ISLA BOUVET', N'074')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (99, N'ISLA DE MAN', N'833')
GO
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (100, N'ISLANDIA', N'352')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (101, N'ISLAS ÁLAND', N'248')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (102, N'ISLAS CAIMÁN', N'136')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (103, N'ISLAS CHRISTMAS', N'162')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (104, N'ISLAS COCOS', N'166')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (105, N'ISLAS COOK', N'184')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (106, N'ISLAS FAROE', N'234')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (107, N'ISLAS HEARD Y MCDONALD', N'334')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (108, N'ISLAS MALVINAS', N'238')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (109, N'ISLAS MARSHALL', N'584')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (110, N'ISLAS NORKFOLK', N'574')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (111, N'ISLAS PALAOS', N'585')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (112, N'ISLAS PITCAIRN', N'612')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (113, N'ISLAS SOLOMÓN', N'090')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (114, N'ISLAS SVALBARD Y JAN MAYEN', N'744')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (115, N'ISLAS TURCAS Y CAICOS', N'796')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (116, N'ISLAS VÍRGENES BRITÁNICAS', N'092')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (117, N'ISLAS VÍRGENES DE LOS ESTADOS UNIDOS DE AMÉRICA', N'850')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (118, N'ISRAEL', N'376')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (119, N'ITALIA', N'380')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (120, N'JAMAICA', N'388')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (121, N'JAPÓN', N'392')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (122, N'JERSEY', N'832')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (123, N'JORDANIA', N'400')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (124, N'KAZAJSTÁN', N'398')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (125, N'KENIA', N'404')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (126, N'KIRGUISTÁN', N'417')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (127, N'KIRIBATI', N'296')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (128, N'KUWAIT', N'414')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (129, N'LAOS', N'418')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (130, N'LESOTHO', N'426')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (131, N'LETONIA', N'428')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (132, N'LÍBANO', N'422')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (133, N'LIBERIA', N'430')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (134, N'LIBIA', N'434')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (135, N'LIECHTENSTEIN', N'438')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (136, N'LITUANIA', N'440')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (137, N'LUXEMBURGO', N'442')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (138, N'MACAO', N'446')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (139, N'MACEDONIA', N'807')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (140, N'MADAGASCAR', N'450')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (141, N'MALASIA', N'458')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (142, N'MALAWI', N'454')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (143, N'MALDIVAS', N'462')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (144, N'MALI', N'466')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (145, N'MALTA', N'470')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (146, N'MARRUECOS', N'504')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (147, N'MARTINICA', N'474')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (148, N'MAURICIO', N'480')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (149, N'MAURITANIA', N'478')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (150, N'MAYOTTE', N'175')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (151, N'MÉXICO', N'484')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (152, N'MICRONESIA', N'583')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (153, N'MOLDOVA', N'498')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (154, N'MÓNACO', N'492')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (155, N'MONGOLIA', N'496')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (156, N'MONTENEGRO', N'499')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (157, N'MONTSERRAT', N'500')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (158, N'MOZAMBIQUE', N'508')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (159, N'MYANMAR', N'104')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (160, N'NAMIBIA', N'516')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (161, N'NAURU', N'520')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (162, N'NEPAL', N'524')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (163, N'NICARAGUA', N'558')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (164, N'NÍGER', N'562')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (165, N'NIGERIA', N'566')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (166, N'NIUE', N'570')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (167, N'NORUEGA', N'578')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (168, N'NUEVA CALEDONIA', N'540')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (169, N'NUEVA ZELANDA', N'554')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (170, N'OMÁN', N'512')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (171, N'PAÍSES BAJOS', N'528')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (172, N'PAKISTÁN', N'586')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (173, N'PALESTINA', N'275')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (174, N'PANAMÁ', N'591')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (175, N'PAPÚA NUEVA GUINEA', N'598')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (176, N'PARAGUAY', N'600')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (177, N'PERÚ', N'604')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (178, N'POLINESIA FRANCESA', N'258')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (179, N'POLONIA', N'616')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (180, N'PORTUGAL', N'620')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (181, N'PUERTO RICO', N'630')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (182, N'QATAR', N'634')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (183, N'REINO UNIDO', N'826')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (184, N'REPÚBLICA CENTRO-AFRICANA', N'140')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (185, N'REPÚBLICA CHECA', N'203')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (186, N'REPÚBLICA DOMINICANA', N'214')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (187, N'REUNIÓN', N'638')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (188, N'RUANDA', N'646')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (189, N'RUMANÍA', N'642')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (190, N'RUSIA', N'643')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (191, N'SAHARA OCCIDENTAL', N'732')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (192, N'SAMOA', N'882')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (193, N'SAMOA AMERICANA', N'016')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (194, N'SAN BARTOLOMÉ', N'652')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (195, N'SAN CRISTÓBAL Y NIEVES', N'659')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (196, N'SAN MARINO', N'674')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (197, N'SAN PEDRO Y MIQUELÓN', N'666')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (198, N'SAN VICENTE Y LAS GRANADINAS', N'670')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (199, N'SANTA ELENA', N'654')
GO
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (200, N'SANTA LUCÍA', N'662')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (201, N'SANTO TOMÉ Y PRÍNCIPE', N'678')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (202, N'SENEGAL', N'686')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (203, N'SERBIA Y MONTENEGRO', N'688')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (204, N'SEYCHELLES', N'690')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (205, N'SIERRA LEONA', N'694')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (206, N'SINGAPUR', N'702')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (207, N'SIRIA', N'760')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (208, N'SOMALIA', N'706')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (209, N'SRI LANKA', N'144')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (210, N'SUAZILANDIA', N'748')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (211, N'SUDÁFRICA', N'710')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (212, N'SUDÁN', N'736')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (213, N'SUECIA', N'752')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (214, N'SUIZA', N'756')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (215, N'SURINAM', N'740')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (216, N'TAILANDIA', N'764')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (217, N'TAIWÁN', N'158')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (218, N'TANZANIA', N'834')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (219, N'TAYIKISTÁN', N'762')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (220, N'TERRITORIO BRITÁNICO DEL OCÉANO ÍNDICO', N'086')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (221, N'TERRITORIOS AUSTRALES FRANCESES', N'260')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (222, N'TIMOR-LESTE', N'626')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (223, N'TOGO', N'768')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (224, N'TOKELAU', N'772')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (225, N'TONGA', N'776')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (226, N'TRINIDAD Y TOBAGO', N'780')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (227, N'TÚNEZ', N'788')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (228, N'TURKMENISTÁN', N'795')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (229, N'TURQUÍA', N'792')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (230, N'TUVALU', N'798')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (231, N'UCRANIA', N'804')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (232, N'UGANDA', N'800')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (233, N'URUGUAY', N'858')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (234, N'UZBEKISTÁN', N'860')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (235, N'VANUATU', N'548')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (236, N'VENEZUELA', N'862')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (237, N'VIETNAM', N'704')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (238, N'WALLIS Y FUTUNA', N'876')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (239, N'YEMEN', N'887')
INSERT [dbo].[tbPais] ([cnID_Pais], [ctPais], [ctCodigoISO]) VALUES (240, N'YIBUTI', N'262')
SET IDENTITY_INSERT [dbo].[tbPais] OFF

INSERT [dbo].[tbPerfilUsuario] ([cnID_Perfil], [ctDescripcion], [cnEstadoReg]) VALUES (1, N'Candidato', 1)
INSERT [dbo].[tbPerfilUsuario] ([cnID_Perfil], [ctDescripcion], [cnEstadoReg]) VALUES (2, N'Evaluador', 1)

--SET IDENTITY_INSERT [dbo].[tbPostulante] ON 
--INSERT [dbo].[tbPostulante] ([cnID_Postulante], [cnID_Proceso], [cnID_Candidato], [cnPuntosCriterioAcademico], [cnPuntosCriterioLaboral], [cnPuntosCriterioAdicional], [cnEvalAuto], [cnEvalManual], [cnEstadoReg], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (2, 1, 1, 0, 0, 0, 0, 0, NULL, CAST(0x0000A4C5010BDDDE AS DateTime), NULL, N'::1')
--INSERT [dbo].[tbPostulante] ([cnID_Postulante], [cnID_Proceso], [cnID_Candidato], [cnPuntosCriterioAcademico], [cnPuntosCriterioLaboral], [cnPuntosCriterioAdicional], [cnEvalAuto], [cnEvalManual], [cnEstadoReg], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (3, 1, 1, 0, 0, 0, 0, 0, 0, CAST(0x0000A4C5010D80D0 AS DateTime), NULL, N'::1')
--INSERT [dbo].[tbPostulante] ([cnID_Postulante], [cnID_Proceso], [cnID_Candidato], [cnPuntosCriterioAcademico], [cnPuntosCriterioLaboral], [cnPuntosCriterioAdicional], [cnEvalAuto], [cnEvalManual], [cnEstadoReg], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (6, 1, 1, 0, 0, 8, 0, 0, 0, CAST(0x0000A4C5011FB3E2 AS DateTime), NULL, N'::1')
--INSERT [dbo].[tbPostulante] ([cnID_Postulante], [cnID_Proceso], [cnID_Candidato], [cnPuntosCriterioAcademico], [cnPuntosCriterioLaboral], [cnPuntosCriterioAdicional], [cnEvalAuto], [cnEvalManual], [cnEstadoReg], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (7, 1, 1, 0, 0, 0, 0, 0, 0, CAST(0x0000A4C501323769 AS DateTime), NULL, N'::1')
--INSERT [dbo].[tbPostulante] ([cnID_Postulante], [cnID_Proceso], [cnID_Candidato], [cnPuntosCriterioAcademico], [cnPuntosCriterioLaboral], [cnPuntosCriterioAdicional], [cnEvalAuto], [cnEvalManual], [cnEstadoReg], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (8, 1, 1, 0, 0, 0, 0, 0, 0, CAST(0x0000A4C50133CE41 AS DateTime), NULL, N'::1')
--INSERT [dbo].[tbPostulante] ([cnID_Postulante], [cnID_Proceso], [cnID_Candidato], [cnPuntosCriterioAcademico], [cnPuntosCriterioLaboral], [cnPuntosCriterioAdicional], [cnEvalAuto], [cnEvalManual], [cnEstadoReg], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (9, 1, 1, 0, 0, 8, 0, 0, 0, CAST(0x0000A4C5013488E9 AS DateTime), NULL, N'::1')
--INSERT [dbo].[tbPostulante] ([cnID_Postulante], [cnID_Proceso], [cnID_Candidato], [cnPuntosCriterioAcademico], [cnPuntosCriterioLaboral], [cnPuntosCriterioAdicional], [cnEvalAuto], [cnEvalManual], [cnEstadoReg], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (10, 1, 1, 0, 0, 8, 0, 0, 0, CAST(0x0000A4C5013CCDCF AS DateTime), NULL, N'::1')
--INSERT [dbo].[tbPostulante] ([cnID_Postulante], [cnID_Proceso], [cnID_Candidato], [cnPuntosCriterioAcademico], [cnPuntosCriterioLaboral], [cnPuntosCriterioAdicional], [cnEvalAuto], [cnEvalManual], [cnEstadoReg], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (11, 1, 1, 0, 0, 8, 0, 0, 0, CAST(0x0000A4C501435D43 AS DateTime), NULL, N'::1')
--INSERT [dbo].[tbPostulante] ([cnID_Postulante], [cnID_Proceso], [cnID_Candidato], [cnPuntosCriterioAcademico], [cnPuntosCriterioLaboral], [cnPuntosCriterioAdicional], [cnEvalAuto], [cnEvalManual], [cnEstadoReg], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (12, 1, 1, 0, 0, 8, 0, 0, 0, CAST(0x0000A4C501454D2B AS DateTime), NULL, N'::1')
--INSERT [dbo].[tbPostulante] ([cnID_Postulante], [cnID_Proceso], [cnID_Candidato], [cnPuntosCriterioAcademico], [cnPuntosCriterioLaboral], [cnPuntosCriterioAdicional], [cnEvalAuto], [cnEvalManual], [cnEstadoReg], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (13, 1, 1, 0, 0, 8, 0, 0, 0, CAST(0x0000A4C60155EB95 AS DateTime), NULL, N'::1')
--INSERT [dbo].[tbPostulante] ([cnID_Postulante], [cnID_Proceso], [cnID_Candidato], [cnPuntosCriterioAcademico], [cnPuntosCriterioLaboral], [cnPuntosCriterioAdicional], [cnEvalAuto], [cnEvalManual], [cnEstadoReg], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (14, 1, 1, 0, 0, 8, 0, 0, 0, CAST(0x0000A4C60156C2B6 AS DateTime), NULL, N'::1')
--INSERT [dbo].[tbPostulante] ([cnID_Postulante], [cnID_Proceso], [cnID_Candidato], [cnPuntosCriterioAcademico], [cnPuntosCriterioLaboral], [cnPuntosCriterioAdicional], [cnEvalAuto], [cnEvalManual], [cnEstadoReg], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (15, 1, 1, 0, 0, 8, 0, 0, 0, CAST(0x0000A4C601570531 AS DateTime), NULL, N'::1')
--INSERT [dbo].[tbPostulante] ([cnID_Postulante], [cnID_Proceso], [cnID_Candidato], [cnPuntosCriterioAcademico], [cnPuntosCriterioLaboral], [cnPuntosCriterioAdicional], [cnEvalAuto], [cnEvalManual], [cnEstadoReg], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (16, 1, 1, 0, 0, 8, 0, 0, 0, CAST(0x0000A4C6015778AE AS DateTime), NULL, N'::1')
--INSERT [dbo].[tbPostulante] ([cnID_Postulante], [cnID_Proceso], [cnID_Candidato], [cnPuntosCriterioAcademico], [cnPuntosCriterioLaboral], [cnPuntosCriterioAdicional], [cnEvalAuto], [cnEvalManual], [cnEstadoReg], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (17, 1, 1, 0, 0, 8, 0, 0, 0, CAST(0x0000A4C60157EB17 AS DateTime), NULL, N'::1')
--INSERT [dbo].[tbPostulante] ([cnID_Postulante], [cnID_Proceso], [cnID_Candidato], [cnPuntosCriterioAcademico], [cnPuntosCriterioLaboral], [cnPuntosCriterioAdicional], [cnEvalAuto], [cnEvalManual], [cnEstadoReg], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (18, 1, 1, 0, 0, 8, 0, 0, 0, CAST(0x0000A4C6015D9F55 AS DateTime), NULL, N'::1')
--INSERT [dbo].[tbPostulante] ([cnID_Postulante], [cnID_Proceso], [cnID_Candidato], [cnPuntosCriterioAcademico], [cnPuntosCriterioLaboral], [cnPuntosCriterioAdicional], [cnEvalAuto], [cnEvalManual], [cnEstadoReg], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (19, 1, 1, 0, 0, 8, 0, 0, 0, CAST(0x0000A4C6015E31E0 AS DateTime), NULL, N'::1')
--INSERT [dbo].[tbPostulante] ([cnID_Postulante], [cnID_Proceso], [cnID_Candidato], [cnPuntosCriterioAcademico], [cnPuntosCriterioLaboral], [cnPuntosCriterioAdicional], [cnEvalAuto], [cnEvalManual], [cnEstadoReg], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (22, 1, 1, 0, 82, 0, 1, 0, 0, CAST(0x0000A4C6016EFC7D AS DateTime), NULL, N'::1')
--INSERT [dbo].[tbPostulante] ([cnID_Postulante], [cnID_Proceso], [cnID_Candidato], [cnPuntosCriterioAcademico], [cnPuntosCriterioLaboral], [cnPuntosCriterioAdicional], [cnEvalAuto], [cnEvalManual], [cnEstadoReg], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (23, 1, 1, 33, 82, 8, 1, 0, 1, CAST(0x0000A4C601810590 AS DateTime), NULL, N'::1')
--SET IDENTITY_INSERT [dbo].[tbPostulante] OFF
--SET IDENTITY_INSERT [dbo].[tbPostulanteAcad] ON 

--INSERT [dbo].[tbPostulanteAcad] ([cnID_PostulanteAcad], [cnID_Postulante], [cnID_Academico], [cnID_Grado], [cnID_Situacion]) VALUES (2, 2, 1, 247, 229)
--INSERT [dbo].[tbPostulanteAcad] ([cnID_PostulanteAcad], [cnID_Postulante], [cnID_Academico], [cnID_Grado], [cnID_Situacion]) VALUES (3, 3, 1, 247, 229)
--INSERT [dbo].[tbPostulanteAcad] ([cnID_PostulanteAcad], [cnID_Postulante], [cnID_Academico], [cnID_Grado], [cnID_Situacion]) VALUES (5, 7, 1, 247, 229)
--INSERT [dbo].[tbPostulanteAcad] ([cnID_PostulanteAcad], [cnID_Postulante], [cnID_Academico], [cnID_Grado], [cnID_Situacion]) VALUES (6, 8, 1, 248, 229)
--INSERT [dbo].[tbPostulanteAcad] ([cnID_PostulanteAcad], [cnID_Postulante], [cnID_Academico], [cnID_Grado], [cnID_Situacion]) VALUES (7, 9, 1, 247, 229)
--INSERT [dbo].[tbPostulanteAcad] ([cnID_PostulanteAcad], [cnID_Postulante], [cnID_Academico], [cnID_Grado], [cnID_Situacion]) VALUES (8, 10, 1, 247, 229)
--INSERT [dbo].[tbPostulanteAcad] ([cnID_PostulanteAcad], [cnID_Postulante], [cnID_Academico], [cnID_Grado], [cnID_Situacion]) VALUES (9, 11, 1, 247, 229)
--INSERT [dbo].[tbPostulanteAcad] ([cnID_PostulanteAcad], [cnID_Postulante], [cnID_Academico], [cnID_Grado], [cnID_Situacion]) VALUES (10, 12, 1, 248, 229)
--INSERT [dbo].[tbPostulanteAcad] ([cnID_PostulanteAcad], [cnID_Postulante], [cnID_Academico], [cnID_Grado], [cnID_Situacion]) VALUES (11, 13, 1, 248, 229)
--INSERT [dbo].[tbPostulanteAcad] ([cnID_PostulanteAcad], [cnID_Postulante], [cnID_Academico], [cnID_Grado], [cnID_Situacion]) VALUES (12, 14, 1, 248, 229)
--INSERT [dbo].[tbPostulanteAcad] ([cnID_PostulanteAcad], [cnID_Postulante], [cnID_Academico], [cnID_Grado], [cnID_Situacion]) VALUES (13, 15, 1, 248, 229)
--INSERT [dbo].[tbPostulanteAcad] ([cnID_PostulanteAcad], [cnID_Postulante], [cnID_Academico], [cnID_Grado], [cnID_Situacion]) VALUES (14, 16, 1, 248, 229)
--INSERT [dbo].[tbPostulanteAcad] ([cnID_PostulanteAcad], [cnID_Postulante], [cnID_Academico], [cnID_Grado], [cnID_Situacion]) VALUES (15, 17, 1, 248, 229)
--INSERT [dbo].[tbPostulanteAcad] ([cnID_PostulanteAcad], [cnID_Postulante], [cnID_Academico], [cnID_Grado], [cnID_Situacion]) VALUES (16, 18, 1, 248, 229)
--INSERT [dbo].[tbPostulanteAcad] ([cnID_PostulanteAcad], [cnID_Postulante], [cnID_Academico], [cnID_Grado], [cnID_Situacion]) VALUES (17, 19, 1, 248, 229)
--INSERT [dbo].[tbPostulanteAcad] ([cnID_PostulanteAcad], [cnID_Postulante], [cnID_Academico], [cnID_Grado], [cnID_Situacion]) VALUES (18, 22, 1, 248, 229)
--INSERT [dbo].[tbPostulanteAcad] ([cnID_PostulanteAcad], [cnID_Postulante], [cnID_Academico], [cnID_Grado], [cnID_Situacion]) VALUES (19, 23, 2, 251, 1637)
--SET IDENTITY_INSERT [dbo].[tbPostulanteAcad] OFF
--SET IDENTITY_INSERT [dbo].[tbPostulanteCobe] ON 

--INSERT [dbo].[tbPostulanteCobe] ([cnID_PostulanteCobe], [cnID_Postulante], [cnID_Cobertura], [cnID_CoberturaZona]) VALUES (2, 2, 1, 2)
--INSERT [dbo].[tbPostulanteCobe] ([cnID_PostulanteCobe], [cnID_Postulante], [cnID_Cobertura], [cnID_CoberturaZona]) VALUES (3, 3, 1, 2)
--INSERT [dbo].[tbPostulanteCobe] ([cnID_PostulanteCobe], [cnID_Postulante], [cnID_Cobertura], [cnID_CoberturaZona]) VALUES (4, 6, 1, 2)
--INSERT [dbo].[tbPostulanteCobe] ([cnID_PostulanteCobe], [cnID_Postulante], [cnID_Cobertura], [cnID_CoberturaZona]) VALUES (5, 7, 1, 3)
--INSERT [dbo].[tbPostulanteCobe] ([cnID_PostulanteCobe], [cnID_Postulante], [cnID_Cobertura], [cnID_CoberturaZona]) VALUES (6, 8, 1, 2)
--INSERT [dbo].[tbPostulanteCobe] ([cnID_PostulanteCobe], [cnID_Postulante], [cnID_Cobertura], [cnID_CoberturaZona]) VALUES (7, 9, 1, 3)
--INSERT [dbo].[tbPostulanteCobe] ([cnID_PostulanteCobe], [cnID_Postulante], [cnID_Cobertura], [cnID_CoberturaZona]) VALUES (8, 10, 1, 2)
--INSERT [dbo].[tbPostulanteCobe] ([cnID_PostulanteCobe], [cnID_Postulante], [cnID_Cobertura], [cnID_CoberturaZona]) VALUES (9, 11, 1, 3)
--INSERT [dbo].[tbPostulanteCobe] ([cnID_PostulanteCobe], [cnID_Postulante], [cnID_Cobertura], [cnID_CoberturaZona]) VALUES (10, 12, 1, 3)
--INSERT [dbo].[tbPostulanteCobe] ([cnID_PostulanteCobe], [cnID_Postulante], [cnID_Cobertura], [cnID_CoberturaZona]) VALUES (11, 13, 1, 3)
--INSERT [dbo].[tbPostulanteCobe] ([cnID_PostulanteCobe], [cnID_Postulante], [cnID_Cobertura], [cnID_CoberturaZona]) VALUES (12, 14, 1, 3)
--INSERT [dbo].[tbPostulanteCobe] ([cnID_PostulanteCobe], [cnID_Postulante], [cnID_Cobertura], [cnID_CoberturaZona]) VALUES (13, 15, 1, 3)
--INSERT [dbo].[tbPostulanteCobe] ([cnID_PostulanteCobe], [cnID_Postulante], [cnID_Cobertura], [cnID_CoberturaZona]) VALUES (14, 16, 1, 3)
--INSERT [dbo].[tbPostulanteCobe] ([cnID_PostulanteCobe], [cnID_Postulante], [cnID_Cobertura], [cnID_CoberturaZona]) VALUES (15, 17, 1, 3)
--INSERT [dbo].[tbPostulanteCobe] ([cnID_PostulanteCobe], [cnID_Postulante], [cnID_Cobertura], [cnID_CoberturaZona]) VALUES (16, 18, 1, 3)
--INSERT [dbo].[tbPostulanteCobe] ([cnID_PostulanteCobe], [cnID_Postulante], [cnID_Cobertura], [cnID_CoberturaZona]) VALUES (17, 19, 1, 3)
--INSERT [dbo].[tbPostulanteCobe] ([cnID_PostulanteCobe], [cnID_Postulante], [cnID_Cobertura], [cnID_CoberturaZona]) VALUES (20, 22, 1, 3)
--INSERT [dbo].[tbPostulanteCobe] ([cnID_PostulanteCobe], [cnID_Postulante], [cnID_Cobertura], [cnID_CoberturaZona]) VALUES (21, 23, 1, 3)
--SET IDENTITY_INSERT [dbo].[tbPostulanteCobe] OFF
--SET IDENTITY_INSERT [dbo].[tbPostulanteCrit] ON 

--INSERT [dbo].[tbPostulanteCrit] ([cnID_PostulanteCrit], [cnID_Postulante], [cnID_CriterioAdicional]) VALUES (1, 6, 1)
--INSERT [dbo].[tbPostulanteCrit] ([cnID_PostulanteCrit], [cnID_Postulante], [cnID_CriterioAdicional]) VALUES (2, 9, 1)
--INSERT [dbo].[tbPostulanteCrit] ([cnID_PostulanteCrit], [cnID_Postulante], [cnID_CriterioAdicional]) VALUES (3, 10, 1)
--INSERT [dbo].[tbPostulanteCrit] ([cnID_PostulanteCrit], [cnID_Postulante], [cnID_CriterioAdicional]) VALUES (4, 11, 1)
--INSERT [dbo].[tbPostulanteCrit] ([cnID_PostulanteCrit], [cnID_Postulante], [cnID_CriterioAdicional]) VALUES (5, 12, 1)
--INSERT [dbo].[tbPostulanteCrit] ([cnID_PostulanteCrit], [cnID_Postulante], [cnID_CriterioAdicional]) VALUES (6, 13, 1)
--INSERT [dbo].[tbPostulanteCrit] ([cnID_PostulanteCrit], [cnID_Postulante], [cnID_CriterioAdicional]) VALUES (7, 14, 1)
--INSERT [dbo].[tbPostulanteCrit] ([cnID_PostulanteCrit], [cnID_Postulante], [cnID_CriterioAdicional]) VALUES (8, 15, 1)
--INSERT [dbo].[tbPostulanteCrit] ([cnID_PostulanteCrit], [cnID_Postulante], [cnID_CriterioAdicional]) VALUES (9, 16, 1)
--INSERT [dbo].[tbPostulanteCrit] ([cnID_PostulanteCrit], [cnID_Postulante], [cnID_CriterioAdicional]) VALUES (10, 17, 1)
--INSERT [dbo].[tbPostulanteCrit] ([cnID_PostulanteCrit], [cnID_Postulante], [cnID_CriterioAdicional]) VALUES (11, 18, 1)
--INSERT [dbo].[tbPostulanteCrit] ([cnID_PostulanteCrit], [cnID_Postulante], [cnID_CriterioAdicional]) VALUES (12, 19, 1)
--INSERT [dbo].[tbPostulanteCrit] ([cnID_PostulanteCrit], [cnID_Postulante], [cnID_CriterioAdicional]) VALUES (13, 23, 1)
--SET IDENTITY_INSERT [dbo].[tbPostulanteCrit] OFF
--SET IDENTITY_INSERT [dbo].[tbPostulanteDocu] ON 

--INSERT [dbo].[tbPostulanteDocu] ([cnID_PostulanteDocu], [cnID_Postulante], [cnID_Documento], [ctRutaArchivo]) VALUES (1, 6, 1, N'a2ec704b-a2c5-4f1f-892c-a59716a6d1f3.pdf')
--INSERT [dbo].[tbPostulanteDocu] ([cnID_PostulanteDocu], [cnID_Postulante], [cnID_Documento], [ctRutaArchivo]) VALUES (2, 7, 1, N'8c518f17-9078-4035-a32e-2be3f526f502.pdf')
--INSERT [dbo].[tbPostulanteDocu] ([cnID_PostulanteDocu], [cnID_Postulante], [cnID_Documento], [ctRutaArchivo]) VALUES (3, 8, 1, N'80903ba6-b197-4259-a8af-ba6a8215b343.pdf')
--INSERT [dbo].[tbPostulanteDocu] ([cnID_PostulanteDocu], [cnID_Postulante], [cnID_Documento], [ctRutaArchivo]) VALUES (4, 9, 1, N'2ade1e68-fcfb-4130-b5ca-911eaccfe8ad.pdf')
--INSERT [dbo].[tbPostulanteDocu] ([cnID_PostulanteDocu], [cnID_Postulante], [cnID_Documento], [ctRutaArchivo]) VALUES (5, 10, 1, N'66f1f999-5883-4e60-a641-d5ec6a536212.pdf')
--INSERT [dbo].[tbPostulanteDocu] ([cnID_PostulanteDocu], [cnID_Postulante], [cnID_Documento], [ctRutaArchivo]) VALUES (6, 11, 1, N'cb4b2ad5-6d42-4cc4-a37b-834069efa3b8.pdf')
--INSERT [dbo].[tbPostulanteDocu] ([cnID_PostulanteDocu], [cnID_Postulante], [cnID_Documento], [ctRutaArchivo]) VALUES (7, 12, 1, N'51071f92-8981-4e20-892f-73742710ce7b.pdf')
--INSERT [dbo].[tbPostulanteDocu] ([cnID_PostulanteDocu], [cnID_Postulante], [cnID_Documento], [ctRutaArchivo]) VALUES (8, 13, 1, N'f2cc57c2-3bd9-4fd3-9861-89cc9aff1616.pdf')
--INSERT [dbo].[tbPostulanteDocu] ([cnID_PostulanteDocu], [cnID_Postulante], [cnID_Documento], [ctRutaArchivo]) VALUES (9, 14, 1, N'4ca1670a-65d7-4bd0-acc7-3b6056e89ec6.pdf')
--INSERT [dbo].[tbPostulanteDocu] ([cnID_PostulanteDocu], [cnID_Postulante], [cnID_Documento], [ctRutaArchivo]) VALUES (10, 15, 1, N'e920c839-76f4-454c-90b0-3d1f24330bf0.pdf')
--INSERT [dbo].[tbPostulanteDocu] ([cnID_PostulanteDocu], [cnID_Postulante], [cnID_Documento], [ctRutaArchivo]) VALUES (11, 16, 1, N'60cb5432-a8a6-4a00-9868-92308991961a.pdf')
--INSERT [dbo].[tbPostulanteDocu] ([cnID_PostulanteDocu], [cnID_Postulante], [cnID_Documento], [ctRutaArchivo]) VALUES (12, 17, 1, N'e83033ee-4354-4136-816f-96994b26a4ae.pdf')
--INSERT [dbo].[tbPostulanteDocu] ([cnID_PostulanteDocu], [cnID_Postulante], [cnID_Documento], [ctRutaArchivo]) VALUES (13, 18, 1, N'675d04b1-119b-4d58-8e90-6d9ab321c60f.pdf')
--INSERT [dbo].[tbPostulanteDocu] ([cnID_PostulanteDocu], [cnID_Postulante], [cnID_Documento], [ctRutaArchivo]) VALUES (14, 19, 1, N'716e3526-c983-4d3d-8682-edae5298519a.pdf')
--INSERT [dbo].[tbPostulanteDocu] ([cnID_PostulanteDocu], [cnID_Postulante], [cnID_Documento], [ctRutaArchivo]) VALUES (16, 23, 1, N'4059dfe5-387d-49dd-99eb-b8d58e183558.pdf')
--SET IDENTITY_INSERT [dbo].[tbPostulanteDocu] OFF
--SET IDENTITY_INSERT [dbo].[tbPostulanteEvalManual] ON 

--INSERT [dbo].[tbPostulanteEvalManual] ([cnPostulanteEvalManual], [cnID_Postulante], [cnID_TipoEval], [cnEvaluacion], [ctObservacion], [ctUsuarioEval], [cfFechaEval]) VALUES (1, 12, 1, 0, N'ACADEMICO', N'aroque         ', CAST(0x0000A4C501673F63 AS DateTime))
--INSERT [dbo].[tbPostulanteEvalManual] ([cnPostulanteEvalManual], [cnID_Postulante], [cnID_TipoEval], [cnEvaluacion], [ctObservacion], [ctUsuarioEval], [cfFechaEval]) VALUES (2, 12, 2, 1, N'LABORAL', N'aroque         ', CAST(0x0000A4C5015E1DAA AS DateTime))
--INSERT [dbo].[tbPostulanteEvalManual] ([cnPostulanteEvalManual], [cnID_Postulante], [cnID_TipoEval], [cnEvaluacion], [ctObservacion], [ctUsuarioEval], [cfFechaEval]) VALUES (3, 12, 3, 1, N'DOCUMENTOS', N'aroque         ', CAST(0x0000A4C5015E1672 AS DateTime))
--INSERT [dbo].[tbPostulanteEvalManual] ([cnPostulanteEvalManual], [cnID_Postulante], [cnID_TipoEval], [cnEvaluacion], [ctObservacion], [ctUsuarioEval], [cfFechaEval]) VALUES (4, 23, 1, 1, N'', N'aroque         ', CAST(0x0000A4C60184286D AS DateTime))
--INSERT [dbo].[tbPostulanteEvalManual] ([cnPostulanteEvalManual], [cnID_Postulante], [cnID_TipoEval], [cnEvaluacion], [ctObservacion], [ctUsuarioEval], [cfFechaEval]) VALUES (5, 23, 3, 1, N'', N'aroque         ', CAST(0x0000A4C601842E0A AS DateTime))
--INSERT [dbo].[tbPostulanteEvalManual] ([cnPostulanteEvalManual], [cnID_Postulante], [cnID_TipoEval], [cnEvaluacion], [ctObservacion], [ctUsuarioEval], [cfFechaEval]) VALUES (6, 23, 2, 1, N'', N'aroque         ', CAST(0x0000A4C601843067 AS DateTime))
--SET IDENTITY_INSERT [dbo].[tbPostulanteEvalManual] OFF
--SET IDENTITY_INSERT [dbo].[tbPostulanteLabo] ON 

--INSERT [dbo].[tbPostulanteLabo] ([cnID_PostulanteLabo], [cnID_Postulante], [cnID_Experiencia], [cnID_TipoCriterioLabo], [cnTiempoAnios], [cnTiempoMeses]) VALUES (1, 22, 1, 235, 9, NULL)
--INSERT [dbo].[tbPostulanteLabo] ([cnID_PostulanteLabo], [cnID_Postulante], [cnID_Experiencia], [cnID_TipoCriterioLabo], [cnTiempoAnios], [cnTiempoMeses]) VALUES (2, 22, 1, 236, 9, NULL)
--INSERT [dbo].[tbPostulanteLabo] ([cnID_PostulanteLabo], [cnID_Postulante], [cnID_Experiencia], [cnID_TipoCriterioLabo], [cnTiempoAnios], [cnTiempoMeses]) VALUES (3, 23, 1, 235, 9, NULL)
--INSERT [dbo].[tbPostulanteLabo] ([cnID_PostulanteLabo], [cnID_Postulante], [cnID_Experiencia], [cnID_TipoCriterioLabo], [cnTiempoAnios], [cnTiempoMeses]) VALUES (4, 23, 1, 236, 9, NULL)
--SET IDENTITY_INSERT [dbo].[tbPostulanteLabo] OFF
--SET IDENTITY_INSERT [dbo].[tbProceso] ON 

--INSERT [dbo].[tbProceso] ([cnID_Proceso], [ctTitulo], [ctDescripcion], [cfFechaInicio], [cfFechaFin], [cnID_Estado], [ctArchivoTerminos], [ctArchivoProceso], [cnIndEvaluacion], [ctUsuarioCreador], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (1, N'Proceso N°1', N'descripción', CAST(0x0000A4BB0183D140 AS DateTime), CAST(0x0000A4E50183D140 AS DateTime), 1632, NULL, NULL, 1, N'aroque         ', CAST(0x0000A4C60180F016 AS DateTime), CAST(0x0000A4C60180F016 AS DateTime), N'::1')
--SET IDENTITY_INSERT [dbo].[tbProceso] OFF
--SET IDENTITY_INSERT [dbo].[tbProcesoCnfgAcad] ON 

--INSERT [dbo].[tbProcesoCnfgAcad] ([cnID_CnfgAcademica], [cnID_Proceso], [cnID_Grado], [cnID_Situacion], [cnID_OperadorLogico], [cnGrupoLogico], [cnPuntaje]) VALUES (1, 1, 252, 1637, NULL, NULL, 2)
--INSERT [dbo].[tbProcesoCnfgAcad] ([cnID_CnfgAcademica], [cnID_Proceso], [cnID_Grado], [cnID_Situacion], [cnID_OperadorLogico], [cnGrupoLogico], [cnPuntaje]) VALUES (2, 1, 251, 1637, NULL, NULL, 33)
--SET IDENTITY_INSERT [dbo].[tbProcesoCnfgAcad] OFF
--SET IDENTITY_INSERT [dbo].[tbProcesoCnfgLabo] ON 

--INSERT [dbo].[tbProcesoCnfgLabo] ([cnID_CnfgLaboral], [cnID_Proceso], [cnID_TipoEntidad], [cnID_TipoCriterioLabo], [cnID_TipoEvaluacion], [ctDescripcion]) VALUES (2, 1, 1629, 235, 243, N'EXPERIENCIA GENERAL NO MENOR DE 5 AÑOS')
--INSERT [dbo].[tbProcesoCnfgLabo] ([cnID_CnfgLaboral], [cnID_Proceso], [cnID_TipoEntidad], [cnID_TipoCriterioLabo], [cnID_TipoEvaluacion], [ctDescripcion]) VALUES (3, 1, 1629, 236, 243, N'EXPERIENCIA ESPECIFICA NO MENOR DE 1 UN AÑO CONDUCIENDO PERSONAL O GRUPOS EN EL SECTOR PUBLICO O PRIVADO, ACREDITADO.')
--SET IDENTITY_INSERT [dbo].[tbProcesoCnfgLabo] OFF
--SET IDENTITY_INSERT [dbo].[tbProcesoCobertura] ON 

--INSERT [dbo].[tbProcesoCobertura] ([cnID_Cobertura], [cnID_Proceso], [ctNombre]) VALUES (1, 1, N'PUNO')
--INSERT [dbo].[tbProcesoCobertura] ([cnID_Cobertura], [cnID_Proceso], [ctNombre]) VALUES (4, 1, N'APURIMAC')
--SET IDENTITY_INSERT [dbo].[tbProcesoCobertura] OFF
--SET IDENTITY_INSERT [dbo].[tbProcesoCriterioAdic] ON 

--INSERT [dbo].[tbProcesoCriterioAdic] ([cnID_CriterioAdicional], [cnID_Proceso], [ctDescripcion], [cnPuntaje]) VALUES (1, 1, N'CONOCIMIENTO DE LA ZONA Y DEL IDIOMA, LENGUA O DIALECTO DE LA JURISDICCIÓN DE LA UNIDAD TERRITORIAL A LA QUE POSTULA', 8)
--SET IDENTITY_INSERT [dbo].[tbProcesoCriterioAdic] OFF
--SET IDENTITY_INSERT [dbo].[tbProcesoDocumento] ON 

--INSERT [dbo].[tbProcesoDocumento] ([cnID_Documento], [cnID_Proceso], [ctDescripcion], [cnIndObligatorio]) VALUES (1, 1, N'DNI', 0)
--SET IDENTITY_INSERT [dbo].[tbProcesoDocumento] OFF
--SET IDENTITY_INSERT [dbo].[tbProcesoMatrizLabo] ON 

--INSERT [dbo].[tbProcesoMatrizLabo] ([cnID_MatrizLaboral], [cnID_CnfgLaboral], [cnMininmo], [cnMaximo], [cnPuntaje]) VALUES (2, 2, 1, 2, 3)
--INSERT [dbo].[tbProcesoMatrizLabo] ([cnID_MatrizLaboral], [cnID_CnfgLaboral], [cnMininmo], [cnMaximo], [cnPuntaje]) VALUES (3, 3, 1, 2, 66)
--INSERT [dbo].[tbProcesoMatrizLabo] ([cnID_MatrizLaboral], [cnID_CnfgLaboral], [cnMininmo], [cnMaximo], [cnPuntaje]) VALUES (4, 2, 3, 0, 16)
--INSERT [dbo].[tbProcesoMatrizLabo] ([cnID_MatrizLaboral], [cnID_CnfgLaboral], [cnMininmo], [cnMaximo], [cnPuntaje]) VALUES (5, 3, 3, 0, 66)
--SET IDENTITY_INSERT [dbo].[tbProcesoMatrizLabo] OFF

SET IDENTITY_INSERT [dbo].[tbProvincia] ON 
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (1, 3, N'ABANCAY', N'0301')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (2, 9, N'ACOBAMBA', N'0902')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (3, 8, N'ACOMAYO', N'0802')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (4, 2, N'AIJA', N'0202')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (5, 12, N'ALTO AMAZONAS', N'1202')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (6, 10, N'AMBO', N'1002')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (7, 3, N'ANDAHUAYLAS', N'0302')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (8, 9, N'ANGARAES', N'0903')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (9, 8, N'ANTA', N'0803')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (10, 3, N'ANTABAMBA', N'0303')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (11, 2, N'ANTONIO RAIMONDI', N'0203')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (12, 4, N'AREQUIPA', N'0401')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (13, 13, N'ASCOPE', N'1302')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (14, 2, N'ASUNCIÓN', N'0204')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (15, 25, N'ATALAYA', N'2502')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (16, 20, N'AYABACA', N'2002')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (17, 3, N'AYMARAES', N'0304')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (18, 21, N'AZÁNGARO', N'2102')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (19, 1, N'BAGUA', N'0102')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (20, 15, N'BARRANCA', N'1502')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (21, 22, N'BELLAVISTA', N'2202')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (22, 13, N'BOLÍVAR ', N'1303')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (23, 2, N'BOLOGNESI', N'0205')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (24, 1, N'BONGARÁ', N'0103')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (25, 6, N'CAJABAMBA', N'0602')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (26, 6, N'CAJAMARCA', N'0601')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (27, 15, N'CAJATAMBO', N'1503')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (28, 8, N'CALCA', N'0804')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (29, 4, N'CAMANÁ', N'0402')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (30, 8, N'CANAS', N'0805')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (31, 8, N'CANCHIS', N'0806')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (32, 23, N'CANDARAVE', N'2302')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (33, 5, N'CANGALLO', N'0502')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (34, 15, N'CANTA', N'1504')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (35, 15, N'CAÑETE', N'1505')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (36, 21, N'CARABAYA', N'2103')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (37, 4, N'CARAVELÍ', N'0403')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (38, 2, N'CARHUAZ', N'0206')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (39, 2, N'CARLOS FERMÍN FITZCARRALD', N'0207')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (40, 2, N'CASMA', N'0208')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (41, 4, N'CASTILLA', N'0404')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (42, 9, N'CASTROVIRREYNA', N'0904')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (43, 4, N'CAYLLOMA', N'0405')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (44, 6, N'CELENDÍN', N'0603')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (45, 1, N'CHACHAPOYAS', N'0101')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (46, 12, N'CHANCHAMAYO', N'1203')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (47, 13, N'CHEPÉN', N'1304')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (48, 14, N'CHICLAYO', N'1401')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (49, 11, N'CHINCHA', N'1102')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (50, 3, N'CHINCHEROS', N'0306')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (51, 6, N'CHOTA', N'0604')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (52, 21, N'CHUCUITO', N'2104')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (53, 8, N'CHUMBIVILCAS', N'0807')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (54, 12, N'CHUPACA', N'1209')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (55, 9, N'CHURCAMPA', N'0905')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (56, 12, N'CONCEPCIÓN ', N'1202')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (57, 4, N'CONDESUYOS', N'0406')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (58, 1, N'CONDORCANQUI', N'0104')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (59, 24, N'CONTRALMIRANTE VILLAR', N'2402')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (60, 6, N'CONTUMAZÁ', N'0605')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (61, 25, N'CORONEL PORTILLO', N'2501')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (62, 2, N'CORONGO', N'0209')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (63, 3, N'COTABAMBAS', N'0305')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (64, 6, N'CUTERVO', N'0606')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (65, 8, N'CUZCO', N'0801')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (66, 19, N'DANIEL ALCIDES CARRIÓN', N'1902')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (67, 12, N'DATEM DEL MARAÑÓN', N'1207')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (68, 10, N'DOS DE MAYO', N'1003')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (69, 21, N'EL COLLAO', N'2105')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (70, 22, N'EL DORADO ', N'2203')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (71, 8, N'ESPINAR', N'0808')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (72, 14, N'FERREÑAFE', N'1402')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (73, 18, N'GENERAL SÁNCHEZ CERRO', N'1802')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (74, 13, N'GRAN CHIMÚ', N'1311')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (75, 3, N'GRAU', N'0307')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (76, 10, N'HUACAYBAMBA', N'1004')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (77, 6, N'HUALGAYOC', N'0607')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (78, 22, N'HUALLAGA', N'2204')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (79, 10, N'HUAMALÍES', N'1005')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (80, 5, N'HUAMANGA', N'0501')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (81, 20, N'HUANCABAMBA', N'2003')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (82, 21, N'HUANCANÉ', N'2106')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (83, 5, N'HUANCASANCOS', N'0503')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (84, 9, N'HUANCAVELICA', N'0901')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (85, 12, N'HUANCAYO', N'1201')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (86, 5, N'HUANTA', N'0504')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (87, 10, N'HUÁNUCO', N'1001')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (88, 15, N'HUARAL', N'1506')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (89, 2, N'HUARAZ', N'0201')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (90, 2, N'HUARI', N'0210')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (91, 2, N'HUARMEY', N'0211')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (92, 15, N'HUAROCHIRÍ', N'1507')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (93, 15, N'HUAURA', N'1508')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (94, 2, N'HUAYLAS', N'0212')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (95, 9, N'HUAYTARÁ', N'0906')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (96, 11, N'ICA', N'1101')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (97, 18, N'ILO', N'1803')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (98, 4, N'ISLAY', N'0407')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (99, 6, N'JAÉN ', N'0608')
GO
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (100, 12, N'JAUJA', N'1204')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (101, 23, N'JORGE BASADRE', N'2303')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (102, 13, N'JULCÁN', N'1305')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (103, 12, N'JUNÍN', N'1205')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (104, 8, N'LA CONVENCIÓN', N'0809')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (105, 5, N'LA MAR', N'0505')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (106, 4, N'LA UNIÓN', N'0408')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (107, 22, N'LAMAS', N'2205')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (108, 14, N'LAMBAYEQUE', N'1403')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (109, 21, N'LAMPA', N'2107')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (110, 10, N'LAURICOCHA', N'1010')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (111, 10, N'LEONCIO PRADO', N'1006')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (112, 15, N'LIMA', N'1501')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (113, 12, N'LORETO', N'1203')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (114, 5, N'LUCANAS', N'0506')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (115, 1, N'LUYA', N'0105')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (116, 17, N'MANU', N'1702')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (117, 10, N'MARAÑÓN', N'1007')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (118, 22, N'MARISCAL CÁCERES', N'2206')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (119, 2, N'MARISCAL LUZURIAGA', N'0213')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (120, 18, N'MARISCAL NIETO', N'1801')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (121, 12, N'MARISCAL RAMÓN CASTILLA', N'1204')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (122, 12, N'MAYNAS', N'1201')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (123, 21, N'MELGAR', N'2108')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (124, 21, N'MOHO', N'2109')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (125, 20, N'MORROPÓN', N'2004')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (126, 22, N'MOYOBAMBA', N'2201')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (127, 11, N'NAZCA', N'1103')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (128, 2, N'OCROS', N'0214')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (129, 13, N'OTUZCO', N'1306')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (130, 19, N'OXAPAMPA', N'1903')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (131, 15, N'OYÓN', N'1509')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (132, 13, N'PACASMAYO', N'1307')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (133, 10, N'PACHITEA', N'1008')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (134, 25, N'PADRE ABAD', N'2503')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (135, 20, N'PAITA', N'2005')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (136, 2, N'PALLASCA', N'0215')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (137, 11, N'PALPA', N'1104')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (138, 5, N'PARINACOCHAS', N'0507')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (139, 8, N'PARURO', N'0810')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (140, 19, N'PASCO', N'1901')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (141, 13, N'PATAZ', N'1308')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (142, 5, N'PÁUCAR DEL SARASARA', N'0508')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (143, 8, N'PAUCARTAMBO', N'0811')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (144, 22, N'PICOTA', N'2207')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (145, 11, N'PISCO', N'1105')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (146, 20, N'PIURA', N'2001')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (147, 2, N'POMABAMBA', N'0216')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (148, 7, N'PROVINCIA CONSTITUCIONAL DEL CALLAO', N'0701')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (149, 10, N'PUERTO INCA', N'1009')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (150, 21, N'PUNO', N'2101')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (151, 25, N'PURÚS', N'2504')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (152, 8, N'QUISPICANCHI', N'0812')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (153, 2, N'RECUAY', N'0217')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (154, 12, N'REQUENA', N'1205')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (155, 22, N'RIOJA', N'2208')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (156, 1, N'RODRÍGUEZ DE MENDOZA', N'0106')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (157, 21, N'SAN ANTONIO DE PUTINA', N'2110')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (158, 6, N'SAN IGNACIO', N'0609')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (159, 6, N'SAN MARCOS', N'0610')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (160, 22, N'SAN MARTÍN', N'2209')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (161, 6, N'SAN MIGUEL', N'0611')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (162, 6, N'SAN PABLO', N'0612')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (163, 21, N'SAN ROMÁN', N'2111')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (164, 13, N'SÁNCHEZ CARRIÓN', N'1309')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (165, 21, N'SANDIA', N'2112')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (166, 2, N'SANTA', N'0218')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (167, 6, N'SANTA CRUZ', N'0613')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (168, 13, N'SANTIAGO DE CHUCO', N'1310')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (169, 12, N'SATIPO', N'1206')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (170, 20, N'SECHURA', N'2008')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (171, 2, N'SIHUAS', N'0219')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (172, 5, N'SUCRE', N'0509')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (173, 20, N'SULLANA', N'2006')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (174, 23, N'TACNA', N'2301')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (175, 17, N'TAHUAMANU', N'1703')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (176, 20, N'TALARA', N'2007')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (177, 17, N'TAMBOPATA', N'1701')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (178, 23, N'TARATA', N'2304')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (179, 12, N'TARMA', N'1207')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (180, 9, N'TAYACAJA', N'0907')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (181, 22, N'TOCACHE', N'2210')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (182, 13, N'TRUJILLO ', N'1301')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (183, 24, N'TUMBES', N'2401')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (184, 12, N'UCAYALI', N'1206')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (185, 8, N'URUBAMBA', N'0813')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (186, 1, N'UTCUBAMBA', N'0107')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (187, 5, N'VÍCTOR FAJARDO', N'0510')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (188, 5, N'VILCASHUAMÁN', N'0511')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (189, 13, N'VIRÚ', N'1312')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (190, 10, N'YAROWILCA', N'1011')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (191, 12, N'YAULI', N'1208')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (192, 15, N'YAUYOS', N'1510')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (193, 2, N'YUNGAY', N'0220')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (194, 21, N'YUNGUYO', N'2113')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (195, 24, N'ZARUMILLA', N'2403')
INSERT [dbo].[tbProvincia] ([cnID_Provincia], [cnID_Departamento], [ctProvincia], [ctCodigoISO]) VALUES (196, 26, N'OTROS', N'0000')
SET IDENTITY_INSERT [dbo].[tbProvincia] OFF

-- SET IDENTITY_INSERT [dbo].[tbReferencia] ON 
-- INSERT [dbo].[tbReferencia] ([cnID_Referencia], [cnID_Candidato], [ctNombreCompleto], [ctNombreEmpresa], [ctCargo], [ctTelefono], [cfFechaCreacion], [cfFechaActualizacion], [ctIPAcceso]) VALUES (1, 1, N'SAAAAAAAAAAA', N'AAAAAAAAAA', N'AAAAAAAA', N'AAAAAA', CAST(0x0000A4C4000B8447 AS DateTime), CAST(0x0000A4C4000B9359 AS DateTime), N'::1')
-- SET IDENTITY_INSERT [dbo].[tbReferencia] OFF

SET IDENTITY_INSERT [dbo].[tbUsuario] ON 
INSERT [dbo].[tbUsuario] ([cnID_Usuario], [cnID_Perfil], [ctNombres], [ctApellidos], [ctLogin], [ctClave], [cnBloqueado], [cnEstadoReg]) VALUES (1, 2, N'Deyanira', N'Wong', N'dwong', N'e10adc3949ba59abbe56e057f20f883e', 0, 1)
INSERT [dbo].[tbUsuario] ([cnID_Usuario], [cnID_Perfil], [ctNombres], [ctApellidos], [ctLogin], [ctClave], [cnBloqueado], [cnEstadoReg]) VALUES (2, 2, N'Evaluador', N'1', N'evaluador1', N'e10adc3949ba59abbe56e057f20f883e', 0, 1)
INSERT [dbo].[tbUsuario] ([cnID_Usuario], [cnID_Perfil], [ctNombres], [ctApellidos], [ctLogin], [ctClave], [cnBloqueado], [cnEstadoReg]) VALUES (3, 2, N'Evaluador', N'2', N'evaluador2', N'e10adc3949ba59abbe56e057f20f883e', 0, 1)
SET IDENTITY_INSERT [dbo].[tbUsuario] OFF

/****** Object:  Index [idx_tbAcademico_Nilvel]    Script Date: 29/06/2015 23:40:30 ******/
CREATE NONCLUSTERED INDEX [idx_tbAcademico_Nilvel] ON [dbo].[tbAcademico]
(
	[cnID_Grado] ASC,
	[cnID_Situacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Idx_tbCandidato_Email]    Script Date: 29/06/2015 23:40:30 ******/
ALTER TABLE [dbo].[tbCandidato] ADD  CONSTRAINT [Idx_tbCandidato_Email] UNIQUE NONCLUSTERED 
(
	[ctEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Idx_tbCandidato_AMaterno]    Script Date: 29/06/2015 23:40:30 ******/
CREATE NONCLUSTERED INDEX [Idx_tbCandidato_AMaterno] ON [dbo].[tbCandidato]
(
	[ctApellidoMaterno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Idx_tbCandidato_APaterno]    Script Date: 29/06/2015 23:40:30 ******/
CREATE NONCLUSTERED INDEX [Idx_tbCandidato_APaterno] ON [dbo].[tbCandidato]
(
	[ctApellidoPaterno] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Idx_tbCandidato_Nombres]    Script Date: 29/06/2015 23:40:30 ******/
CREATE NONCLUSTERED INDEX [Idx_tbCandidato_Nombres] ON [dbo].[tbCandidato]
(
	[ctNombres] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [idx_tbCnfgSistema_Alias]    Script Date: 29/06/2015 23:40:30 ******/
ALTER TABLE [dbo].[tbCnfgSistema] ADD  CONSTRAINT [idx_tbCnfgSistema_Alias] UNIQUE NONCLUSTERED 
(
	[ctAlias] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [Pk_tbDocIdentidad]    Script Date: 29/06/2015 23:40:30 ******/
ALTER TABLE [dbo].[tbDocIdentidad] ADD  CONSTRAINT [Pk_tbDocIdentidad] PRIMARY KEY NONCLUSTERED 
(
	[cnID_DocIdentidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Idx_tbDocIdentidad_Numero]    Script Date: 29/06/2015 23:40:30 ******/
CREATE NONCLUSTERED INDEX [Idx_tbDocIdentidad_Numero] ON [dbo].[tbDocIdentidad]
(
	[ctNumero] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [idx_tbMaestroGrupos_Codigo]    Script Date: 29/06/2015 23:40:30 ******/
CREATE NONCLUSTERED INDEX [idx_tbMaestroGrupos_Codigo] ON [dbo].[tbMaestroGrupos]
(
	[ctAlias] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [idx_tbMaestroListas_Listar]    Script Date: 29/06/2015 23:40:30 ******/
CREATE NONCLUSTERED INDEX [idx_tbMaestroListas_Listar] ON [dbo].[tbMaestroListas]
(
	[cnID_Lista] ASC,
	[cnID_GrupoLista] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [idx_tbProceso_Fecha]    Script Date: 29/06/2015 23:40:30 ******/
CREATE NONCLUSTERED INDEX [idx_tbProceso_Fecha] ON [dbo].[tbProceso]
(
	[cfFechaInicio] ASC,
	[cfFechaFin] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [idx_tbCriterioAdicional_Descripcion]    Script Date: 29/06/2015 23:40:30 ******/
CREATE NONCLUSTERED INDEX [idx_tbCriterioAdicional_Descripcion] ON [dbo].[tbProcesoCriterioAdic]
(
	[ctDescripcion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [Ak_tbUsuario_Login]    Script Date: 29/06/2015 23:40:30 ******/
ALTER TABLE [dbo].[tbUsuario] ADD  CONSTRAINT [Ak_tbUsuario_Login] UNIQUE NONCLUSTERED 
(
	[ctLogin] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tbAcademico]  WITH CHECK ADD  CONSTRAINT [fk_tbAcademico_tbCandidato] FOREIGN KEY([cnID_Candidato])
REFERENCES [dbo].[tbCandidato] ([cnID_Candidato])
GO
ALTER TABLE [dbo].[tbAcademico] CHECK CONSTRAINT [fk_tbAcademico_tbCandidato]
GO
ALTER TABLE [dbo].[tbAcademico]  WITH CHECK ADD  CONSTRAINT [fk_tbAcademico_tbMaestroListas_Institucion] FOREIGN KEY([cnID_Institucion])
REFERENCES [dbo].[tbMaestroListas] ([cnID_Lista])
GO
ALTER TABLE [dbo].[tbAcademico] CHECK CONSTRAINT [fk_tbAcademico_tbMaestroListas_Institucion]
GO
ALTER TABLE [dbo].[tbAcademico]  WITH CHECK ADD  CONSTRAINT [fk_tbAcademico_tbMaestroListas_Nivel] FOREIGN KEY([cnID_Grado])
REFERENCES [dbo].[tbMaestroListas] ([cnID_Lista])
GO
ALTER TABLE [dbo].[tbAcademico] CHECK CONSTRAINT [fk_tbAcademico_tbMaestroListas_Nivel]
GO
ALTER TABLE [dbo].[tbAcademico]  WITH CHECK ADD  CONSTRAINT [fk_tbAcademico_tbMaestroListas_Profesion] FOREIGN KEY([cnID_Carrera])
REFERENCES [dbo].[tbMaestroListas] ([cnID_Lista])
GO
ALTER TABLE [dbo].[tbAcademico] CHECK CONSTRAINT [fk_tbAcademico_tbMaestroListas_Profesion]
GO
ALTER TABLE [dbo].[tbAcademico]  WITH CHECK ADD  CONSTRAINT [fk_tbAcademico_tbMaestroListas_Situacion] FOREIGN KEY([cnID_Situacion])
REFERENCES [dbo].[tbMaestroListas] ([cnID_Lista])
GO
ALTER TABLE [dbo].[tbAcademico] CHECK CONSTRAINT [fk_tbAcademico_tbMaestroListas_Situacion]
GO
ALTER TABLE [dbo].[tbAcademico]  WITH CHECK ADD  CONSTRAINT [fk_tbAcademico_tbPais] FOREIGN KEY([cnID_Pais])
REFERENCES [dbo].[tbPais] ([cnID_Pais])
GO
ALTER TABLE [dbo].[tbAcademico] CHECK CONSTRAINT [fk_tbAcademico_tbPais]
GO
ALTER TABLE [dbo].[tbCandidato]  WITH CHECK ADD  CONSTRAINT [fk_tbCandidato_tbDepartamento_Direccion] FOREIGN KEY([cnID_Departamento])
REFERENCES [dbo].[tbDepartamento] ([cnID_Departamento])
GO
ALTER TABLE [dbo].[tbCandidato] CHECK CONSTRAINT [fk_tbCandidato_tbDepartamento_Direccion]
GO
ALTER TABLE [dbo].[tbCandidato]  WITH CHECK ADD  CONSTRAINT [fk_tbCandidato_tbDistrito_Direccion] FOREIGN KEY([cnID_Distrito])
REFERENCES [dbo].[tbDistrito] ([cnID_Distrito])
GO
ALTER TABLE [dbo].[tbCandidato] CHECK CONSTRAINT [fk_tbCandidato_tbDistrito_Direccion]
GO
ALTER TABLE [dbo].[tbCandidato]  WITH CHECK ADD  CONSTRAINT [fk_tbCandidato_tbMaestroListas_Sexo] FOREIGN KEY([cnID_Sexo])
REFERENCES [dbo].[tbMaestroListas] ([cnID_Lista])
GO
ALTER TABLE [dbo].[tbCandidato] CHECK CONSTRAINT [fk_tbCandidato_tbMaestroListas_Sexo]
GO
ALTER TABLE [dbo].[tbCandidato]  WITH CHECK ADD  CONSTRAINT [fk_tbCandidato_tbPais_Direccion] FOREIGN KEY([cnID_Pais])
REFERENCES [dbo].[tbPais] ([cnID_Pais])
GO
ALTER TABLE [dbo].[tbCandidato] CHECK CONSTRAINT [fk_tbCandidato_tbPais_Direccion]
GO
ALTER TABLE [dbo].[tbCandidato]  WITH CHECK ADD  CONSTRAINT [fk_tbCandidato_tbPais_Nacimiento] FOREIGN KEY([cnID_PaisNacimiento])
REFERENCES [dbo].[tbPais] ([cnID_Pais])
GO
ALTER TABLE [dbo].[tbCandidato] CHECK CONSTRAINT [fk_tbCandidato_tbPais_Nacimiento]
GO
ALTER TABLE [dbo].[tbCandidato]  WITH CHECK ADD  CONSTRAINT [fk_tbCandidato_tbProvincia_Direccion] FOREIGN KEY([cnID_Provincia])
REFERENCES [dbo].[tbProvincia] ([cnID_Provincia])
GO
ALTER TABLE [dbo].[tbCandidato] CHECK CONSTRAINT [fk_tbCandidato_tbProvincia_Direccion]
GO
ALTER TABLE [dbo].[tbCoberturaZona]  WITH CHECK ADD  CONSTRAINT [fk_tbCoberturaZona_tbProcesoCobertura] FOREIGN KEY([cnID_Cobertura])
REFERENCES [dbo].[tbProcesoCobertura] ([cnID_Cobertura])
GO
ALTER TABLE [dbo].[tbCoberturaZona] CHECK CONSTRAINT [fk_tbCoberturaZona_tbProcesoCobertura]
GO
ALTER TABLE [dbo].[tbComplementario]  WITH CHECK ADD  CONSTRAINT [fk_tbComplementario_tbCandidato] FOREIGN KEY([cnID_Candidato])
REFERENCES [dbo].[tbCandidato] ([cnID_Candidato])
GO
ALTER TABLE [dbo].[tbComplementario] CHECK CONSTRAINT [fk_tbComplementario_tbCandidato]
GO
ALTER TABLE [dbo].[tbComplementario]  WITH CHECK ADD  CONSTRAINT [fk_tbComplementario_tbMaestroListas_Aplicacion] FOREIGN KEY([cnID_TipoEstudio])
REFERENCES [dbo].[tbMaestroListas] ([cnID_Lista])
GO
ALTER TABLE [dbo].[tbComplementario] CHECK CONSTRAINT [fk_tbComplementario_tbMaestroListas_Aplicacion]
GO
ALTER TABLE [dbo].[tbComplementario]  WITH CHECK ADD  CONSTRAINT [fk_tbComplementario_tbPais_Pais] FOREIGN KEY([cnID_Pais])
REFERENCES [dbo].[tbPais] ([cnID_Pais])
GO
ALTER TABLE [dbo].[tbComplementario] CHECK CONSTRAINT [fk_tbComplementario_tbPais_Pais]
GO
ALTER TABLE [dbo].[tbDistrito]  WITH NOCHECK ADD  CONSTRAINT [fk_tbDistrito_tbDepartamento] FOREIGN KEY([cnID_Departamento])
REFERENCES [dbo].[tbDepartamento] ([cnID_Departamento])
GO
ALTER TABLE [dbo].[tbDistrito] CHECK CONSTRAINT [fk_tbDistrito_tbDepartamento]
GO
ALTER TABLE [dbo].[tbDistrito]  WITH NOCHECK ADD  CONSTRAINT [fk_tbDistrito_tbProvincia] FOREIGN KEY([cnID_Provincia])
REFERENCES [dbo].[tbProvincia] ([cnID_Provincia])
GO
ALTER TABLE [dbo].[tbDistrito] CHECK CONSTRAINT [fk_tbDistrito_tbProvincia]
GO
ALTER TABLE [dbo].[tbDocIdentidad]  WITH CHECK ADD  CONSTRAINT [fk_tbDocIdentidad_tbCandidato] FOREIGN KEY([cnID_Candidato])
REFERENCES [dbo].[tbCandidato] ([cnID_Candidato])
GO
ALTER TABLE [dbo].[tbDocIdentidad] CHECK CONSTRAINT [fk_tbDocIdentidad_tbCandidato]
GO
ALTER TABLE [dbo].[tbDocIdentidad]  WITH CHECK ADD  CONSTRAINT [fk_tbDocIdentidad_tbMaestroListas_Tipo] FOREIGN KEY([cnID_TipoDocumento])
REFERENCES [dbo].[tbMaestroListas] ([cnID_Lista])
GO
ALTER TABLE [dbo].[tbDocIdentidad] CHECK CONSTRAINT [fk_tbDocIdentidad_tbMaestroListas_Tipo]
GO
ALTER TABLE [dbo].[tbExperiencia]  WITH CHECK ADD  CONSTRAINT [fk_tbExperiencia_tbCandidato] FOREIGN KEY([cnID_Candidato])
REFERENCES [dbo].[tbCandidato] ([cnID_Candidato])
GO
ALTER TABLE [dbo].[tbExperiencia] CHECK CONSTRAINT [fk_tbExperiencia_tbCandidato]
GO
ALTER TABLE [dbo].[tbExperiencia]  WITH CHECK ADD  CONSTRAINT [fk_tbExperiencia_tbMaestroListas_Area] FOREIGN KEY([cnID_Area])
REFERENCES [dbo].[tbMaestroListas] ([cnID_Lista])
GO
ALTER TABLE [dbo].[tbExperiencia] CHECK CONSTRAINT [fk_tbExperiencia_tbMaestroListas_Area]
GO
ALTER TABLE [dbo].[tbExperiencia]  WITH CHECK ADD  CONSTRAINT [fk_tbExperiencia_tbMaestroListas_Cargo] FOREIGN KEY([cnID_Cargo])
REFERENCES [dbo].[tbMaestroListas] ([cnID_Lista])
GO
ALTER TABLE [dbo].[tbExperiencia] CHECK CONSTRAINT [fk_tbExperiencia_tbMaestroListas_Cargo]
GO
ALTER TABLE [dbo].[tbExperiencia]  WITH CHECK ADD  CONSTRAINT [fk_tbExperiencia_tbMaestroListas_Entidad] FOREIGN KEY([cnID_TipoEntidad])
REFERENCES [dbo].[tbMaestroListas] ([cnID_Lista])
GO
ALTER TABLE [dbo].[tbExperiencia] CHECK CONSTRAINT [fk_tbExperiencia_tbMaestroListas_Entidad]
GO
ALTER TABLE [dbo].[tbExperiencia]  WITH CHECK ADD  CONSTRAINT [fk_tbExperiencia_tbMaestroListas_Rubro] FOREIGN KEY([cnID_Rubro])
REFERENCES [dbo].[tbMaestroListas] ([cnID_Lista])
GO
ALTER TABLE [dbo].[tbExperiencia] CHECK CONSTRAINT [fk_tbExperiencia_tbMaestroListas_Rubro]
GO
ALTER TABLE [dbo].[tbExperiencia]  WITH CHECK ADD  CONSTRAINT [fk_tbExperiencia_tbMaestroListas_TipoContrato] FOREIGN KEY([cnID_TipoContrato])
REFERENCES [dbo].[tbMaestroListas] ([cnID_Lista])
GO
ALTER TABLE [dbo].[tbExperiencia] CHECK CONSTRAINT [fk_tbExperiencia_tbMaestroListas_TipoContrato]
GO
ALTER TABLE [dbo].[tbExperiencia]  WITH CHECK ADD  CONSTRAINT [fk_tbExperiencia_tbPais] FOREIGN KEY([cnID_Pais])
REFERENCES [dbo].[tbPais] ([cnID_Pais])
GO
ALTER TABLE [dbo].[tbExperiencia] CHECK CONSTRAINT [fk_tbExperiencia_tbPais]
GO
ALTER TABLE [dbo].[tbIdioma]  WITH CHECK ADD  CONSTRAINT [fk_tbIdioma_tbCandidato] FOREIGN KEY([cnID_Candidato])
REFERENCES [dbo].[tbCandidato] ([cnID_Candidato])
GO
ALTER TABLE [dbo].[tbIdioma] CHECK CONSTRAINT [fk_tbIdioma_tbCandidato]
GO
ALTER TABLE [dbo].[tbIdioma]  WITH CHECK ADD  CONSTRAINT [fk_tbIdioma_tbMaestroListas_Institucion] FOREIGN KEY([cnID_Institucion])
REFERENCES [dbo].[tbMaestroListas] ([cnID_Lista])
GO
ALTER TABLE [dbo].[tbIdioma] CHECK CONSTRAINT [fk_tbIdioma_tbMaestroListas_Institucion]
GO
ALTER TABLE [dbo].[tbIdioma]  WITH CHECK ADD  CONSTRAINT [fk_tbIdioma_tbMaestroListas_Lengua] FOREIGN KEY([cnID_Lengua])
REFERENCES [dbo].[tbMaestroListas] ([cnID_Lista])
GO
ALTER TABLE [dbo].[tbIdioma] CHECK CONSTRAINT [fk_tbIdioma_tbMaestroListas_Lengua]
GO
ALTER TABLE [dbo].[tbIdioma]  WITH CHECK ADD  CONSTRAINT [fk_tbIdioma_tbMaestroListas_NivConversar] FOREIGN KEY([cnID_NivelConversacion])
REFERENCES [dbo].[tbMaestroListas] ([cnID_Lista])
GO
ALTER TABLE [dbo].[tbIdioma] CHECK CONSTRAINT [fk_tbIdioma_tbMaestroListas_NivConversar]
GO
ALTER TABLE [dbo].[tbIdioma]  WITH CHECK ADD  CONSTRAINT [fk_tbIdioma_tbMaestroListas_NivEscritura] FOREIGN KEY([cnID_NivelEscritura])
REFERENCES [dbo].[tbMaestroListas] ([cnID_Lista])
GO
ALTER TABLE [dbo].[tbIdioma] CHECK CONSTRAINT [fk_tbIdioma_tbMaestroListas_NivEscritura]
GO
ALTER TABLE [dbo].[tbIdioma]  WITH CHECK ADD  CONSTRAINT [fk_tbIdioma_tbMaestroListas_NivLectura] FOREIGN KEY([cnID_NivelLectura])
REFERENCES [dbo].[tbMaestroListas] ([cnID_Lista])
GO
ALTER TABLE [dbo].[tbIdioma] CHECK CONSTRAINT [fk_tbIdioma_tbMaestroListas_NivLectura]
GO
ALTER TABLE [dbo].[tbIdioma]  WITH CHECK ADD  CONSTRAINT [fk_tbIdioma_tbMaestroListas_Situacion] FOREIGN KEY([cnID_Situacion])
REFERENCES [dbo].[tbMaestroListas] ([cnID_Lista])
GO
ALTER TABLE [dbo].[tbIdioma] CHECK CONSTRAINT [fk_tbIdioma_tbMaestroListas_Situacion]
GO
ALTER TABLE [dbo].[tbMaestroListas]  WITH NOCHECK ADD  CONSTRAINT [fk_tbMaestroListas_tbMaestroGrupos] FOREIGN KEY([cnID_GrupoLista])
REFERENCES [dbo].[tbMaestroGrupos] ([cnID_GrupoLista])
GO
ALTER TABLE [dbo].[tbMaestroListas] CHECK CONSTRAINT [fk_tbMaestroListas_tbMaestroGrupos]
GO
ALTER TABLE [dbo].[tbMenu]  WITH CHECK ADD  CONSTRAINT [fk_tbMenu_tbPerfilUsuario] FOREIGN KEY([cnID_Perfil])
REFERENCES [dbo].[tbPerfilUsuario] ([cnID_Perfil])
GO
ALTER TABLE [dbo].[tbMenu] CHECK CONSTRAINT [fk_tbMenu_tbPerfilUsuario]
GO
ALTER TABLE [dbo].[tbPostulante]  WITH CHECK ADD  CONSTRAINT [fk_tbPostulante_tbCandidato] FOREIGN KEY([cnID_Candidato])
REFERENCES [dbo].[tbCandidato] ([cnID_Candidato])
GO
ALTER TABLE [dbo].[tbPostulante] CHECK CONSTRAINT [fk_tbPostulante_tbCandidato]
GO
--ALTER TABLE [dbo].[tbPostulante]  WITH CHECK ADD  CONSTRAINT [fk_tbPostulante_tbProceso] FOREIGN KEY([cnID_Proceso])
--REFERENCES [dbo].[tbProceso] ([cnID_Proceso])
--GO
--ALTER TABLE [dbo].[tbPostulante] CHECK CONSTRAINT [fk_tbPostulante_tbProceso]
--GO
ALTER TABLE [dbo].[tbPostulanteAcad]  WITH CHECK ADD  CONSTRAINT [fk_tbPostulanteAcad_tbPostulante] FOREIGN KEY([cnID_Postulante])
REFERENCES [dbo].[tbPostulante] ([cnID_Postulante])
GO
ALTER TABLE [dbo].[tbPostulanteAcad] CHECK CONSTRAINT [fk_tbPostulanteAcad_tbPostulante]
GO
ALTER TABLE [dbo].[tbPostulanteCobe]  WITH CHECK ADD  CONSTRAINT [fk_tbPostulanteCobe_tbPostulante] FOREIGN KEY([cnID_Postulante])
REFERENCES [dbo].[tbPostulante] ([cnID_Postulante])
GO
ALTER TABLE [dbo].[tbPostulanteCobe] CHECK CONSTRAINT [fk_tbPostulanteCobe_tbPostulante]
GO
ALTER TABLE [dbo].[tbPostulanteCrit]  WITH CHECK ADD  CONSTRAINT [fk_tbPostulanteCrit_tbPostulante] FOREIGN KEY([cnID_Postulante])
REFERENCES [dbo].[tbPostulante] ([cnID_Postulante])
GO
ALTER TABLE [dbo].[tbPostulanteCrit] CHECK CONSTRAINT [fk_tbPostulanteCrit_tbPostulante]
GO
ALTER TABLE [dbo].[tbPostulanteDocu]  WITH CHECK ADD  CONSTRAINT [fk_tbPostulanteDocu_tbPostulante] FOREIGN KEY([cnID_Postulante])
REFERENCES [dbo].[tbPostulante] ([cnID_Postulante])
GO
ALTER TABLE [dbo].[tbPostulanteDocu] CHECK CONSTRAINT [fk_tbPostulanteDocu_tbPostulante]
GO
ALTER TABLE [dbo].[tbPostulanteEvalManual]  WITH CHECK ADD  CONSTRAINT [fk_tbPostulanteEvalManual_tbPostulante] FOREIGN KEY([cnID_Postulante])
REFERENCES [dbo].[tbPostulante] ([cnID_Postulante])
GO
ALTER TABLE [dbo].[tbPostulanteEvalManual] CHECK CONSTRAINT [fk_tbPostulanteEvalManual_tbPostulante]
GO
ALTER TABLE [dbo].[tbPostulanteLabo]  WITH CHECK ADD  CONSTRAINT [fk_tbPostulanteLabo_tbPostulante] FOREIGN KEY([cnID_Postulante])
REFERENCES [dbo].[tbPostulante] ([cnID_Postulante])
GO
ALTER TABLE [dbo].[tbPostulanteLabo] CHECK CONSTRAINT [fk_tbPostulanteLabo_tbPostulante]
GO
ALTER TABLE [dbo].[tbProceso]  WITH CHECK ADD  CONSTRAINT [fk_tbProceso_tbMaestroListas_Estado] FOREIGN KEY([cnID_Estado])
REFERENCES [dbo].[tbMaestroListas] ([cnID_Lista])
GO
ALTER TABLE [dbo].[tbProceso] CHECK CONSTRAINT [fk_tbProceso_tbMaestroListas_Estado]
GO
ALTER TABLE [dbo].[tbProcesoCnfgAcad]  WITH CHECK ADD  CONSTRAINT [fk_tbProcesoCnfgAcad_tbMaestroListas_Grado] FOREIGN KEY([cnID_Grado])
REFERENCES [dbo].[tbMaestroListas] ([cnID_Lista])
GO
ALTER TABLE [dbo].[tbProcesoCnfgAcad] CHECK CONSTRAINT [fk_tbProcesoCnfgAcad_tbMaestroListas_Grado]
GO
ALTER TABLE [dbo].[tbProcesoCnfgAcad]  WITH CHECK ADD  CONSTRAINT [fk_tbProcesoCnfgAcad_tbMaestroListas_Situacion] FOREIGN KEY([cnID_Situacion])
REFERENCES [dbo].[tbMaestroListas] ([cnID_Lista])
GO
ALTER TABLE [dbo].[tbProcesoCnfgAcad] CHECK CONSTRAINT [fk_tbProcesoCnfgAcad_tbMaestroListas_Situacion]
GO
ALTER TABLE [dbo].[tbProcesoCnfgAcad]  WITH CHECK ADD  CONSTRAINT [fk_tbProcesoCnfgAcad_tbProceso] FOREIGN KEY([cnID_Proceso])
REFERENCES [dbo].[tbProceso] ([cnID_Proceso])
GO
ALTER TABLE [dbo].[tbProcesoCnfgAcad] CHECK CONSTRAINT [fk_tbProcesoCnfgAcad_tbProceso]
GO
ALTER TABLE [dbo].[tbProcesoCnfgLabo]  WITH CHECK ADD  CONSTRAINT [fk_tbProcesoCnfgLabo_tbMaestroListas_Tipo] FOREIGN KEY([cnID_TipoEntidad])
REFERENCES [dbo].[tbMaestroListas] ([cnID_Lista])
GO
ALTER TABLE [dbo].[tbProcesoCnfgLabo] CHECK CONSTRAINT [fk_tbProcesoCnfgLabo_tbMaestroListas_Tipo]
GO
ALTER TABLE [dbo].[tbProcesoCnfgLabo]  WITH CHECK ADD  CONSTRAINT [fk_tbProcesoCnfgLabo_tbMaestrosListas] FOREIGN KEY([cnID_TipoEvaluacion])
REFERENCES [dbo].[tbMaestroListas] ([cnID_Lista])
GO
ALTER TABLE [dbo].[tbProcesoCnfgLabo] CHECK CONSTRAINT [fk_tbProcesoCnfgLabo_tbMaestrosListas]
GO
ALTER TABLE [dbo].[tbProcesoCnfgLabo]  WITH CHECK ADD  CONSTRAINT [fk_tbProcesoCnfgLabo_tbProceso] FOREIGN KEY([cnID_Proceso])
REFERENCES [dbo].[tbProceso] ([cnID_Proceso])
GO
ALTER TABLE [dbo].[tbProcesoCnfgLabo] CHECK CONSTRAINT [fk_tbProcesoCnfgLabo_tbProceso]
GO
ALTER TABLE [dbo].[tbProcesoCobertura]  WITH CHECK ADD  CONSTRAINT [fk_tbProcesoCobertura_tbProceso] FOREIGN KEY([cnID_Proceso])
REFERENCES [dbo].[tbProceso] ([cnID_Proceso])
GO
ALTER TABLE [dbo].[tbProcesoCobertura] CHECK CONSTRAINT [fk_tbProcesoCobertura_tbProceso]
GO
ALTER TABLE [dbo].[tbProcesoCriterioAdic]  WITH CHECK ADD  CONSTRAINT [fk_tbProcesoCnfgCriterioAdic_tbProceso] FOREIGN KEY([cnID_Proceso])
REFERENCES [dbo].[tbProceso] ([cnID_Proceso])
GO
ALTER TABLE [dbo].[tbProcesoCriterioAdic] CHECK CONSTRAINT [fk_tbProcesoCnfgCriterioAdic_tbProceso]
GO
ALTER TABLE [dbo].[tbProcesoDocumento]  WITH CHECK ADD  CONSTRAINT [fk_tbProcesoDocumento_tbProceso] FOREIGN KEY([cnID_Proceso])
REFERENCES [dbo].[tbProceso] ([cnID_Proceso])
GO
ALTER TABLE [dbo].[tbProcesoDocumento] CHECK CONSTRAINT [fk_tbProcesoDocumento_tbProceso]
GO
ALTER TABLE [dbo].[tbProcesoMatrizLabo]  WITH CHECK ADD  CONSTRAINT [fk_tbProcesoMatrizLabo_tbProcesoCnfgLabo] FOREIGN KEY([cnID_CnfgLaboral])
REFERENCES [dbo].[tbProcesoCnfgLabo] ([cnID_CnfgLaboral])
GO
ALTER TABLE [dbo].[tbProcesoMatrizLabo] CHECK CONSTRAINT [fk_tbProcesoMatrizLabo_tbProcesoCnfgLabo]
GO
ALTER TABLE [dbo].[tbProvincia]  WITH NOCHECK ADD  CONSTRAINT [fk_tbProvincia_tbDepartamento] FOREIGN KEY([cnID_Departamento])
REFERENCES [dbo].[tbDepartamento] ([cnID_Departamento])
GO
ALTER TABLE [dbo].[tbProvincia] CHECK CONSTRAINT [fk_tbProvincia_tbDepartamento]
GO
ALTER TABLE [dbo].[tbReferencia]  WITH CHECK ADD  CONSTRAINT [fk_tbReferencia_tbCandidato] FOREIGN KEY([cnID_Candidato])
REFERENCES [dbo].[tbCandidato] ([cnID_Candidato])
GO
ALTER TABLE [dbo].[tbReferencia] CHECK CONSTRAINT [fk_tbReferencia_tbCandidato]
GO
ALTER TABLE [dbo].[tbUsuario]  WITH CHECK ADD  CONSTRAINT [fk_tbUsuario_tbPerfilUsuario] FOREIGN KEY([cnID_Perfil])
REFERENCES [dbo].[tbPerfilUsuario] ([cnID_Perfil])
GO
ALTER TABLE [dbo].[tbUsuario] CHECK CONSTRAINT [fk_tbUsuario_tbPerfilUsuario]
GO
USE [master]
GO
ALTER DATABASE [dbSeleccion] SET  READ_WRITE 
GO
