﻿using System.Web.Mvc;
using System.Web.Routing;

namespace mvcSeleccion
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            // name: "Oportunidades",
            // url: "Seleccion/Oportunidades/{a}/{i}");

            //routes.MapRoute(
            //name: "HojaVida",
            //url: "Seleccion/HojaVida/{tlDocumentado}", defaults: new { tlDocumentado = true });


            routes.MapRoute(
              name: "Academica",
              url: "{controller}/{action}/{a}/{i}");

           
            //tlDocumentado

            //routes.MapRoute(
            //name: "Detailsaved",
            //url: "{controller}/{action}/{a}/{id}");

            routes.MapRoute(
              name: "Default_Index",
              url: "{controller}/{action}",
              defaults: new { action = "Index", id = UrlParameter.Optional }
            );

            //Seleccion/GetHojaVida?cnID_Postulante=71&cnID_Candidato=14
            // routes.MapRoute(
            //  name: "Hoja_Vida",
            //  url: "Seleccion/GetHojaVida/{cnID_Postulante}/{cnID_Candidato}",
            //  defaults: new { cnID_Postulante = UrlParameter.Optional, cnID_Candidato = UrlParameter.Optional }
            //);

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Auth", action = "Login", id = UrlParameter.Optional }
            );


        }
    }
}