﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(mvcSeleccion.Startup))]
namespace mvcSeleccion
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
