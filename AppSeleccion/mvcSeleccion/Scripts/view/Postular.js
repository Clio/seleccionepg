﻿$('form').formValidation(

    )
  .on('err.field.fv', function (e, data) {

      if (data.field.indexOf("txtDocumento_") >= 0) return;

      // Hide the messages
      data.element
          .data('fv.messages')
          .find('.help-block[data-fv-for="' + data.field + '"]').hide();
  })
    .on('submit', function (e) {

        if (e.isDefaultPrevented()) {
            // handle the invalid form...


        }
        else {

            if ($('.chkSeleccionarExpEsp:checked').length == 0) {
                bootboxError("Seleccione al menos una experiencia específica.");
                $("#btnGuardar").removeAttr("disabled");
                $("#btnGuardar").removeClass("disabled");
                return false;
            }

            $('#ddlListadoCobertura option').prop('selected', true);

            return true;
        }

    });