﻿var _basePath;
var nroFilaAcademico = 0;
var nroFilaCobertura = 0;
var nroFilaCriterioAdicional = 0;
var nroFilaConfDocum = 0;



$('#txtFechaInicioContainer').datetimepicker({
    sideBySide: true,
    locale: 'es'
})
.on('dp.change', function (e) {
    $('form').formValidation('revalidateField', 'cfFechaInicio');
});

$('#txtFechaFinContainer').datetimepicker({
    sideBySide: true,
    locale: 'es'

})
.on('dp.change', function (e) {
    $('form').formValidation('revalidateField', 'cfFechaFin');
});


$('form').formValidation(
    {
        excluded: [':disabled'], //REVISAR ESTA LINEA
        fields: {
            'tbProcesoCoberturaList_ctNombre[]': {
                validators: {
                    notEmpty: {
                        message: ''
                    }
                }
            },
            'tbProcesoCoberturaList_ctCobertura[]': {
                validators: {
                    notEmpty: {
                        message: ''
                    }
                }
            },
            'tbProcesoCnfgAcadModelList_cnID_Grado[]': {
                validators: {
                    notEmpty: {
                        message: ''
                    }
                }
            },
            'tbProcesoCnfgAcadModelList_cnID_Situacion[]': {
                validators: {
                    notEmpty: {
                        message: ''
                    }
                }
            }
        }
    }
    )
    .on('err.field.fv', function (e, data) {
        // Hide the messages
        data.element
            .data('fv.messages')
            .find('.help-block[data-fv-for="' + data.field + '"]').hide();

        var $tabPane = data.element.parents('.tab-pane'),
               tabId = $tabPane.attr('id');

        $('a[href="#' + tabId + '"][data-toggle="tab"]')
            .parent()
            .find('i')
            .removeClass('fa-check')
            .addClass('fa-times');
    })
    .on('success.field.fv', function (e, data) {
        // data.fv      --> The FormValidation instance
        // data.element --> The field element

        var $tabPane = data.element.parents('.tab-pane'),
            tabId = $tabPane.attr('id'),
            $icon = $('a[href="#' + tabId + '"][data-toggle="tab"]')
                        .parent()
                        .find('i')
                        .removeClass('fa-check fa-times');

        // Check if all fields in tab are valid
        var isValidTab = data.fv.isValidContainer($tabPane);
        if (isValidTab !== null) {
            $icon.addClass(isValidTab ? 'fa-check' : 'fa-times');
        }
    })

    .on('submit', function (e) {


        //$("#divCargarTermRef").removeClass("has-error");

        //if ($("input[name=txtArchivoTerminos]").val() == '') {
        //    $("#divCargarTermRef").addClass("has-error");
        //    return false;
        //}

        //$("#divCargarArchivoProceso").removeClass("has-error");

        //if ($("input[name=txtArchivoProceso]").val() == '') {
        //    $("#divCargarArchivoProceso").addClass("has-error");
        //    return false;
        //}

        if (e.isDefaultPrevented()) {
            // handle the invalid form...

        }
        else {

            if ($("#cnID_Proceso").val() == "0")
                return true;

            $.ajaxSetup({ async: false });

            var _return = false;
            var _message = "";
            $.get($("#hdnValidarProceso").val(),

                    {
                        id: $("#cnID_Proceso").val()
                    },
                function (data) {
                    console.log(data);

                    _return = data.resp;
                    _message = data.message;
                }
            );

            if (_return)
                return true;

            else {
                bootboxError(_message);
            }

            $.ajaxSetup({ async: true });

            $("#btnGuardar").removeAttr("disabled");
            $("#btnGuardar").removeClass("disabled");

            return false;
        }

    });


$(window).load(function () {

    $(".add_row_conf_acad").click(function () {


        var comboPlantillaGrado = $("#tbProcesoCnfgAcadModelList_0_cnID_Grado").html();
        var comboPlantillaSituacion = $("#tbProcesoCnfgAcadModelList_0_cnID_Situacion").html();

        var $template = $("<tr><td class='deleterow' onclick='removeRowAcademico(this,0)'><div class='glyphicon glyphicon-remove'></div></td><td><div class='form-group'><input name='tbProcesoCnfgAcadModelList_cnID_CnfgAcademica[]' type='hidden' value='0' /><select class='form-control tbProcesoCnfgAcadModelList_cnID_Grado text-uppercase' id='tbProcesoCnfgAcadModelList_" + nroFilaAcademico + "_cnID_Grado'  name='tbProcesoCnfgAcadModelList_cnID_Grado[]' ></select></div></td><td><div class='form-group'><select class='form-control text-uppercase tbProcesoCnfgAcadModelList_cnID_Situacion' id='tbProcesoCnfgAcadModelList_" + nroFilaAcademico + "_cnID_Situacion' name='tbProcesoCnfgAcadModelList_cnID_Situacion[]' ></select></div></td><td><div class='form-group'><input class='form-control  text-uppercase' name='tbProcesoCnfgAcadModelList_cnPuntaje[]' required='required' type='number' value='0'></div></td></tr>");

        $('#tab_crit_acad tr:last').after($template);

        $("#tbProcesoCnfgAcadModelList_" + nroFilaAcademico + "_cnID_Grado").append(comboPlantillaGrado);
        //$("#tbProcesoCnfgAcadModelList_" + nroFilaAcademico + "_cnID_Situacion").append(comboPlantillaSituacion);

        //console.log($template.find('[name="tbProcesoCnfgAcadModelList_cnID_TipoDocumento[]"]'));
        $('form').formValidation('addField', $template.find('[name="tbProcesoCnfgAcadModelList_cnID_Grado[]"]'));
        $('form').formValidation('addField', $template.find('[name="tbProcesoCnfgAcadModelList_cnID_Situacion[]"]'));
        $('form').formValidation('addField', $template.find('[name="tbProcesoCnfgAcadModelList_cnPuntaje[]"]'));



        nroFilaAcademico++;
    });

    $(".add_row_cobertura").click(function () {



        var $template = $("<tr><td class='deleterow' onclick='removeRowCobertura(this,0)'><div class='glyphicon glyphicon-remove'></div></td><td><div class='form-group'><div class='input-group'><span class='input-group-btn'><button class='btn btn-success btnZonas' onclick='addZonaCobertura(this);' data-cobertura-id='0' data-cobertura-row-id='" + nroFilaCobertura + "' type='button' >Zonas</button></span><input name='tbProcesoCoberturaList_cnID_Cobertura[]' type='hidden' value='0' id='tbProcesoCoberturaList_" + nroFilaCobertura + "_cnID_Cobertura' /><input class='form-control text-uppercase' id='tbProcesoCoberturaList_" + nroFilaCobertura + "_ctNombre' name='tbProcesoCoberturaList_ctNombre[]' type='text' value=''></div></div></td></tr>");
        $('#tab_cobertura tr:last').after($template);

        $('form').formValidation('addField', $template.find('[name="tbProcesoCoberturaList_ctNombre[]"]'));
        $('form').formValidation('addField', $template.find('[name="tbProcesoCoberturaList_ctCobertura[]"]'));

        nroFilaCobertura++;
    });


    $(".add_row_crit_adicio").click(function () {




        var $template = $("<tr><td class='deleterow' onclick='removeRowAdicional(this,0)'><div class='glyphicon glyphicon-remove'></div></td><td><div class='form-group'><input name='tbProcesoCriterioAdicModelList_cnID_CriterioAdicional[]' type='hidden' value='0' /><input class='form-control text-uppercase' id='tbProcesoCriterioAdicModelList_" + nroFilaCriterioAdicional + "_ctDescripcion' name='tbProcesoCriterioAdicModelList_ctDescripcion[]' type='text' value='' required='required'></div></td><td><div class='form-group'><input class='form-control  text-uppercase' name='tbProcesoCriterioAdicModelList_cnPuntaje[]' required='required' type='number' value='0'></div></td></tr>");
        $('#tab_crit_adicional tr:last').after($template);

        $('form').formValidation('addField', $template.find('[name="tbProcesoCriterioAdicModelList_ctDescripcion[]"]'));
        $('form').formValidation('addField', $template.find('[name="tbProcesoCriterioAdicModelList_cnPuntaje[]"]'));


        //<div class='form-group'><input class='form-control  text-uppercase' name='tbProcesoCriterioAdicModelList_cnPuntaje[]' required='required' type='number' value='0'></div>

        nroFilaCriterioAdicional++;
    });

    $(".add_row_conf_docum").click(function () {

        var $template = $("<tr><td class='deleterow' onclick='removeRowDocumento(this,0)'><div class='glyphicon glyphicon-remove'></div></td><td><div class='form-group'><input name='tbProcesoDocumentoModelList_cnID_Documento[]' type='hidden' value='0' /><input class='form-control text-uppercase' id='tbProcesoDocumentoModelList_" + nroFilaConfDocum + "_ctDescripcion' name='tbProcesoDocumentoModelList_ctDescripcion[]' type='text' value='' ,required='required'></div></td><td><div class='form-group'><select class='form-control' name='tbProcesoDocumentoModelList_cnIndObligatorio[]' required='required' ><option value='true'>Sí</option><option value='false'>No</option></select></div></td></tr>");
        $('#tab_conf_docum tr:last').after($template);

        $('form').formValidation('addField', $template.find('[name="tbProcesoDocumentoModelList_ctDescripcion[]"]'));

        nroFilaConfDocum++;
    });



});


function AddMatriz(obj) {

    //alert( $(obj).attr("data-tipo-criterio"));
    if ($("#cnID_TipoEntidad").val() == "") {
        bootboxError("Seleccione el tipo de entidad para continuar.");
        return;
    }

    var fn = function () {
        $.get($("#hdnMatrizListadoUrl").val(),
            {
                id: $("#cnID_Proceso").val(),
                tipoCriterio: $(obj).attr("data-tipo-criterio"),
                confLaboral: $(obj).attr("data-config-laboral")
            }, function (result) {
                $("#divMatrizLaboralListado").html(result);
            })
    };
    bootboxShowContent($("#hdnMatrizUrl").val(), { tipoCriterio: $(obj).attr("data-tipo-criterio"), id: 0, confLaboral: $(obj).attr("data-config-laboral") }, 'Criterio Laboral', fn);
};

function editRowMatriz(id) {

    bootboxShowContent($("#hdnMatrizUrl").val(), { tipoCriterio: null, id: id }, 'Criterio Laboral', null);
}




$(".lnkDescargar").click(function () {
    var file = $(this).attr("data-filename");

    $.ajax({
        url: $("#hdnDownloadFileUrl").val(),
        type: 'GET',
        data: {
            file: file
        },
        //dataType: 'json',
        //contentType: 'application/json; charset=utf-8',
        success: function (response) {

            if (response.result != undefined) {
                bootboxError(response.result);
                return;
            }
            //location.href = $("#hdnDownloadFileUrl").val() + "?file=" + file;
            window.open($("#hdnDownloadFileUrl").val() + "?file=" + file);

        },
        error: function () {
            bootboxAlert("error");
        }
    });






})

function addZonaCobertura(e) {

    var coberturaId = $(e).attr("data-cobertura-id");
    var _coberturaNombre = $(e).parent().parent().find("input[name='tbProcesoCoberturaList_ctNombre[]']").val();

    if ($.trim(_coberturaNombre) == '') {
        bootboxError("Ingrese el nombre de la cobertura.")
        return;
    }


    if (coberturaId == 0) {


        $.ajaxSetup({ async: false });

        $.post($("#hdnSaveCoberturaUrl").val(),
            {
                idProceso: $("#cnID_Proceso").val(),
                coberturaNombre: _coberturaNombre
            },
            function (result) {
                //alert("result");

                if (result.result == "OK") {
                    coberturaId = result.id;
                }
                else {
                    bootboxError("Ha ocurrido un error al guardar la cobertura.")
                }

                $.ajaxSetup({ async: true });
            });

        $(e).attr("data-cobertura-id", coberturaId);
        //$(e).parent().parent().find("input[name='tbProcesoCoberturaList_cnID_Cobertura[]']").val(coberturaId);
        var rowId = $(e).attr("data-cobertura-row-id");
        $("#tbProcesoCoberturaList_" + rowId + "_cnID_Cobertura").val(coberturaId);
        //$('form').formValidation('revalidateField', 'tbProcesoCoberturaList_cnID_Cobertura[]');
    }

    //alert(coberturaId);

    if (coberturaId > 0)
        bootboxShowContent($("#hdnUrlAddZonaCobertura").val(), { id: coberturaId }, "Zonas : " + _coberturaNombre);
}

$(document).on("change", ".tbProcesoCnfgAcadModelList_cnID_Grado", function () {

    var $ddlSituacion = $(this).parent().parent().parent().find("td").find("div").find(".tbProcesoCnfgAcadModelList_cnID_Situacion");
    console.log($ddlSituacion);
    var _idGrado = $(this).val();
    $ddlSituacion.empty();

    $.ajax({
        cache: false,
        type: "GET",
        url: _basePath + 'Data/GetSituacion',
        data: { "idGrado": _idGrado },
        success: function (data) {

            $ddlSituacion.empty();
            $ddlSituacion.append($('<option></option>').val("").html("Seleccionar"));
            $.each(data, function (id, option) {
                $ddlSituacion.append($('<option></option>').val(option.Value).html(option.Text));
            });
          
        },
        error: function (xhr, ajaxOptions, thrownError) { }
    });
    
})


$(document).ready(function () {
    _basePath = $("#hdBasePath").val();
})