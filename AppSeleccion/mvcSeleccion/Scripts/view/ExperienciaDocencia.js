﻿
$('#txtFechaInicioContainer').datetimepicker({
    format: 'DD/MM/YYYY',
    locale: 'es'
})
.on('dp.change', function (e) {
    $('form').formValidation('revalidateField', 'cfFechaInicio');
});


$("input[name=cnRegistradoDINA]").change(function (e) {

    //$("#btnAgregarDet").hide();
    if ($("input[name=cnRegistradoDINA]:checked").val() == 'true') {
        $("input[name=ctNumeroRegistroDINA]").removeAttr("disabled");
        //$("#btnAgregarDet").show();
      
    }
    else {
        $("input[name=ctNumeroRegistroDINA]").attr("disabled", "disabled");
        $("input[name=ctNumeroRegistroDINA]").val("");
    }

    
        
});



$("#lnkDescargar").click(function () {
    var file = $(this).attr("data-filename");

    $.ajax({
        url: $("#hdnDownloadFileUrl").val(),
        type: 'GET',
        data: {
            file: file
        },
        //dataType: 'json',
        //contentType: 'application/json; charset=utf-8',
        success: function (response) {

            if (response.result != undefined) {
                bootboxError(response.result);
                return;
            }
            //location.href = $("#hdnDownloadFileUrl").val() + "?file=" + file;
            window.open($("#hdnDownloadFileUrl").val() + "?file=" + file, "_blank");
        },
        error: function () {
            bootboxAlert("error");
        }
    });

})