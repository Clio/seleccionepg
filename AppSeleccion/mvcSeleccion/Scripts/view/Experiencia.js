﻿
$('#txtFechaInicioContainer').datetimepicker({
    format: 'DD/MM/YYYY',
    locale: 'es'
})
.on('dp.change', function (e) {
    $('form').formValidation('revalidateField', 'cfFechaInicio');
});

$('#txtFechaFinContainer').datetimepicker({
    format: 'DD/MM/YYYY',
    locale: 'es'
})
.on('dp.change', function (e) {
    $('form').formValidation('revalidateField', 'cfFechaFin');
});








$("#lnkDescargar").click(function () {
    var file = $(this).attr("data-filename");

    $.ajax({
        url: $("#hdnDownloadFileUrl").val(),
        type: 'GET',
        data: {
            file: file
        },
        //dataType: 'json',
        //contentType: 'application/json; charset=utf-8',
        success: function (response) {

            if (response.result != undefined) {
                bootboxError(response.result);
                return;
            }
            //location.href = $("#hdnDownloadFileUrl").val() + "?file=" + file;
            window.open($("#hdnDownloadFileUrl").val() + "?file=" + file, "_blank");
        },
        error: function () {
            bootboxAlert("error");
        }
    });

})