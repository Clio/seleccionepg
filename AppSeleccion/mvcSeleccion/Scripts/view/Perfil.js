﻿var _basePath;

$("#cnID_TipoEmail").change(function () {

    var $ctEmail = $("#ctEmail");
    var $cnID_TipoEmail = $("#cnID_TipoEmail");
    $ctEmail.empty();
    $.ajax({
        cache: false,
        type: "GET",
        url: _basePath + 'Data/GetEmails',
        data: { "tipoEmail": $cnID_TipoEmail.val() },
        success: function (data) {
            
            $ctEmail.append($('<option></option>').val("").html("Seleccionar"));
            
            $.each(data, function (id, option) {
                $ctEmail.append($('<option></option>').val(option.ctMail).html(option.ctMail));
            });

            $('form').formValidation('revalidateField', 'ctEmail');
        },
        error: function (xhr, ajaxOptions, thrownError) { }
    });
});


$('form').formValidation(
    {
        excluded: [':disabled'],
        fields: {
            ctClaveAcceso2: {
                validators: {
                    identical: {
                        field: 'ctClaveAcceso',
                        message: 'Las contraseñas no coinciden.'
                    }
                }
            }
        }
    }
    )
    .on('err.field.fv', function (e, data) {

      
        data.element
            .data('fv.messages')
            .find('.help-block[data-fv-for="' + data.field + '"]').hide();
    })
    .on('submit', function (e) {

       
        if (e.isDefaultPrevented()) {
            // handle the invalid form...

        }
        else {
            return true;
        }

    });


$(document).ready(function () {
    _basePath = $("#hdBasePath").val();
})