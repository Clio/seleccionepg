﻿var _basePath;

$(document).ready(function () {
    _basePath = $("#hdBasePath").val();

    $("#cnID_Mantenimiento").change(function () {

        if ($(this).val() != "")
            $("#btnNuevo").show();
        else
            $("#btnNuevo").hide();

        $("#divListadoItems").html("");

        $("#ddlSubgrupos").empty();
        var value = $(this).val();
        if (value == 50)
            LoadGruposNiveles();
    });

    $("#btnBuscar").click(function () {
        $("#divListadoItems").html("");
        var value = $("#cnID_Mantenimiento").val();
        if (value != 50)
            mtShowGrid(value);
        else {
            value = $("#ddlSubgrupos").val();
            if (value != '')
                mtShowGrid(value);
        }
    });

    function LoadGruposNiveles() {

        $.ajax({
            cache: false,
            type: "GET",
            url: _basePath + 'Data/GetMaestroGrupoList',
            data: { tipo: "TEMATICA" },
            success: function (data) {

                $("#ddlSubgrupos").empty();
                $("#ddlSubgrupos").append($('<option></option>').val("").html("Seleccionar"));
                $.each(data, function (id, option) {
                    $("#ddlSubgrupos").append($('<option></option>').val(option.cnID_GrupoLista).html(option.ctNombreGrupo));
                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });
    }

    $("#btnNuevo").hide();
})

function mtShowGrid(value) {
    var grupopadre = 0;
    grupopadre = $("#cnID_Mantenimiento").val();
    if ($("#ddlSubgrupos").val() == undefined)
        grupopadre = 0;

    $("#divListadoItems").html("Cargando...");
    $.ajax({
        cache: false,
        type: "GET",
        url: _basePath + 'Data/GetMaestroListas',
        data: { group: value, grupopadre: grupopadre },
        success: function (result) {

            // console.log(result);
            $("#divListadoItems").html(result);

        },
        error: function (xhr, ajaxOptions, thrownError) { }
    });
}