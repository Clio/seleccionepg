﻿

$('#frmLogin').formValidation(
        {
            fields: {
                txtEmail: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }
                    }
                },
                txtPassword: {
                    validators: {
                        notEmpty: {
                            message: 'Este campo es requerido'
                        }
                    }
                }
            }
        })
        .on('err.field.fv', function (e, data) {
            // Hide the messages
            //data.element
            //    .data('fv.messages')
            //    .find('.help-block[data-fv-for="' + data.field + '"]').hide();
        })
    .on('submit', function (e) {


        if (e.isDefaultPrevented()) {
            // handle the invalid form...

        }
        else {
            $("#btnIngresar").attr("disabled", "disabled");
            $.post($("#hdnUrlLogin").val(),
                {
                    user: $("#txtEmail").val(),
                    pwd: $("#txtPassword").val(),
                },
                function (resp) {
                    if (resp == "OK" || resp == "OKA") {
                        var title;

                        if (resp == "OK")
                            title = "Sus datos son válidos...";
                        else if(resp == "OKA")
                            title = "Sus datos son válidos, debe realizar el cambio de su contraseña.";

                        bootboxNoButton(title, 'Espere un momento...');
                        setTimeout(function () {
                            location.href = $("#hdnUrlUserPage").val();
                        }, 2000);
                    }
                    else {
                        bootboxError("Ha ocurrido un error al iniciar sesión. Verifique si sus datos son válidos.")
                        $("#btnIngresar").removeAttr("disabled");
                    }
                });
            return false;
        }

    });


$('#frmRecuperar').formValidation(
      {
          excluded: [':disabled'],
          fields: {
              'txtEmail': {
                  validators: {
                      notEmpty: {
                          message: ' '
                      },
                      regexp: {
                          regexp: /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i,
                          message: 'Correo no válido.'
                      }
                  }
              }
          }
      }
    )
 .on('err.field.fv', function (e, data) {
     // Hide the messages
     if (data.field == 'txtEmail') return;

     data.element
         .data('fv.messages')
         .find('.help-block[data-fv-for="' + data.field + '"]').hide();
 })
.on('submit', function (e) {

    if (e.isDefaultPrevented()) {
        // handle the invalid form...

    }
    else {
        $("#btnRecuperar").attr("disabled", "disabled");
        $.post($("#hdnUrRecoverPwd").val(),
            {
                user: $("#txtEmail").val()
            },
            function (resp) {
                if (resp == "OK") {
                    bootboxAlert("Se ha enviado un correo con un enlace para recuperar su contraseña");
                    setTimeout(function () {
                        location.href = $("#hdnUrlLoginPage").val();
                    }, 1000);
                }
                else {
                    bootboxError("Ha ocurrido un error al enviar el enlace para recuperar su contraseña.")
                    $("#btnRecuperar").removeAttr("disabled");
                }

            });
        return false;
    }

});


//frmRecuperar



$('#frmResetPwd').formValidation(
    {
        //excluded: [':disabled'],
        fields: {
            ctClaveAcceso2: {
                validators: {
                    identical: {
                        field: 'ctClaveAcceso',
                        message: 'Las contraseñas no coinciden.'
                    },
                    notEmpty: {
                        message: 'Este campo es obligatorio.'
                    }
                }
            }
        }
    }
    )
.on('err.field.fv', function (e, data) {
    // Hide the messages

    //if (data.field == 'ctClaveAcceso2') return;
    //if (data.field == 'ctClaveAcceso') return;

    //data.element
    //    .data('fv.messages')
    //    .find('.help-block[data-fv-for="' + data.field + '"]').hide();

    //var $tabPane = data.element.parents('.tab-pane'),
    //       tabId = $tabPane.attr('id');

    //$('a[href="#' + tabId + '"][data-toggle="tab"]')
    //    .parent()
    //    .find('i')
    //    .removeClass('fa-check')
    //    .addClass('fa-times');

})
.on('success.field.fv', function (e, data) {
    // data.fv      --> The FormValidation instance
    // data.element --> The field element

    var $tabPane = data.element.parents('.tab-pane'),
        tabId = $tabPane.attr('id'),
        $icon = $('a[href="#' + tabId + '"][data-toggle="tab"]')
                    .parent()
                    .find('i')
                    .removeClass('fa-check fa-times');

    // Check if all fields in tab are valid
    var isValidTab = data.fv.isValidContainer($tabPane);
    if (isValidTab !== null) {
        $icon.addClass(isValidTab ? 'fa-check' : 'fa-times');
    }
})
.on('submit', function (e) {


    if (e.isDefaultPrevented()) {
        // handle the invalid form...

    }
    else {
        $("#btnReset").attr("disabled", "disabled");
        $.post($("#hdnUrResetPwd").val(),
            {
                code: $("#hdnCode").val(),
                pwd: $("#ctClaveAcceso2").val()
            },
            function (resp) {
                if (resp == "OK") {
                    bootboxAlert("Se ha actualizado su contraseña");
                    setTimeout(function () {
                        location.href = $("#hdnUrlLoginPage").val();
                    }, 1000);
                }
                else {
                    bootboxError("Ha ocurrido un error al actualizar su contraseña.")
                    $("#btnReset").removeAttr("disabled");
                }

            });
        return false;
    }

});
