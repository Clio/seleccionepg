﻿var _basePath;

$("#cnID_SubGrupoLista").change(function () {
    var val = $("#cnID_SubGrupoLista option:selected").val();
    $("#cnID_ListaPadre").empty();
    //$("#cnID_SubGrupoLista").empty();
    $.ajax({
        cache: false,
        type: "GET",
        url: _basePath + 'Data/GetAreaTematicaNivelPadre',
        data: { group: val },
        success: function (data) {
           
          
            $.each(data, function (id, option) {
                $("#cnID_ListaPadre").append($('<option></option>').val(option.Value).html(option.Text));
            });

            //$('form').formValidation('revalidateField', 'ctEmail');
        },
        error: function (xhr, ajaxOptions, thrownError) { }
    });
});

$("#cnID_GrupoLista").change(function () {
    $("#cnID_SubGrupoLista").empty();
    if ($(this).val() == 50) {
        $.ajax({
            cache: false,
            type: "GET",
            url: _basePath + 'Data/GetListaAreaTematica',
            data: {},
            success: function (data) {
                var $cnID_SubGrupoLista = $("#cnID_SubGrupoLista");
                $cnID_SubGrupoLista.empty();
                $.each(data, function (id, option) {
                    $cnID_SubGrupoLista.append($('<option></option>').val(option.cnID_GrupoLista).html(option.ctNombreGrupo));
                });

            }
        });
    }
});

$('form').formValidation(
    {
        excluded: [':disabled'],
        fields: {
           
        }
    }
    )
    .on('err.field.fv', function (e, data) {

        //if (data.field == 'txtCertificado') return;
        // Hide the messages
        data.element
            .data('fv.messages')
            .find('.help-block[data-fv-for="' + data.field + '"]').hide();
    })
    .on('submit', function (e) {

        //$("#divCargarArchivo").removeClass("has-error");

        //if ($("input[name=txtCertificado]").val() == '') {
        //    $("#divCargarArchivo").addClass("has-error");
        //    return false;
        //}

        if (e.isDefaultPrevented()) {
            // handle the invalid form...

        }
        else {
            return true;
        }

    });

$(document).ready(function () {
    _basePath = $("#hdBasePath").val();
})