﻿$('form').formValidation()
    .on('err.field.fv', function (e, data) {
        // Hide the messages
        data.element
            .data('fv.messages')
            .find('.help-block[data-fv-for="' + data.field + '"]').hide();
    })
    .on('submit', function (e) {

        if (e.isDefaultPrevented()) {
          
        }
        else {
           

            return true;
        }

    });