﻿$(window).load(function () {


    $(".lnkDescargar").click(function () {
        var file = $(this).attr("data-filename");

        $.ajax({
            url: $("#hdnDownloadFileUrl").val(),
            type: 'GET',
            data: {
                file: file
            },

            success: function (response) {

                if (response.result != undefined) {
                    bootboxError(response.result);
                    return;
                }
                window.open($("#hdnDownloadFileUrl").val() + "?file=" + file);

            },
            error: function () {
                bootboxAlert("error");
            }
        });

    })


    $(".lnkDescargarCertificado").click(function () {
        var file = $(this).attr("data-filename");

        $.ajax({
            url: $("#hdnDownloadFileCertificadoUrl").val(),
            type: 'GET',
            data: {
                file: file
            },
            success: function (response) {

                if (response.result != undefined) {
                    bootboxError(response.result);
                    return;
                }

                window.open($("#hdnDownloadFileCertificadoUrl").val() + "?file=" + file, "_blank");

            },
            error: function () {
                bootboxAlert("error");
            }
        });

    })

    $(".btnGuardar").click(function () {

        var option = $(this).attr("data-option");
        var _tipo = 0;
        var _id = 0;

        

        if ($("#tab_1").hasClass("active")) {
            _tipo = 1;
            _id = $("#hdnIdAcad").val();
        }
        else if ($("#tab_2").hasClass("active")) {
            _tipo = 2;
            _id = $("#hdnIdLab").val();
        }
        else if ($("#tab_3").hasClass("active")) {
            _tipo = 3;
            _id = $("#hdnIdDoc").val();
        }


        if ($("#tab_1").hasClass("active")) {
            $("#hdnObservacionAcad").val($("#txtObservaciones").val());
        }
        else if ($("#tab_2").hasClass("active")) {
            $("#hdnObservacionLab").val($("#txtObservaciones").val());
        }
        else if ($("#tab_3").hasClass("active")) {
            $("#hdnObservacionDoc").val($("#txtObservaciones").val());

        }


        if (option == 'false') {

            if ($.trim($("#txtObservaciones").val()) == '') {
                bootboxError('Ingrese las observaciones.');
                return;
            }

        }
        $.post($("#hdnSaveEvaluacionManualUrl").val(),
            {
                result: option,
                id: _id,
                tipo: _tipo,
                idPostulante: $("#hdnIdPostulante").val(),
                ctObservaciones: $("#txtObservaciones").val()
            },
            function (result) {
                if (_tipo == 1) {
                    $("#hdnIdAcad").val(result.id);
                }
                else if (_tipo == 2) {
                    $("#hdnIdLab").val(result.id);
                }
                else if (_tipo == 3) {
                    $("#hdnIdDoc").val(result.id);
                }





                if ($("#tab_1").hasClass("active")) {
                    $("#lnktab_1").removeClass("text-green");
                    $("#lnktab_1").removeClass("text-red");
                    if (option == 'false') {

                        $("#lnktab_1").addClass("text-red");
                    } else {
                        $("#lnktab_1").addClass("text-green");
                    }
                }
                else if ($("#tab_2").hasClass("active")) {
                    $("#lnktab_2").removeClass("text-green");
                    $("#lnktab_2").removeClass("text-red");
                    if (option == 'false') {

                        $("#lnktab_2").addClass("text-red");
                    } else {
                        $("#lnktab_2").addClass("text-green");
                    }
                }
                else if ($("#tab_3").hasClass("active")) {

                    $("#lnktab_3").removeClass("text-green");
                    $("#lnktab_3").removeClass("text-red");
                    if (option == 'false') {

                        $("#lnktab_3").addClass("text-red");
                    } else {
                        $("#lnktab_3").addClass("text-green");
                    }
                }



                bootboxAlert("Guardado correctamente.");
            })


    });

});

