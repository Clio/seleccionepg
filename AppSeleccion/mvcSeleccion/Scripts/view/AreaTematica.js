﻿var _basePath;
var $ddlNivel1 = $("#ddlNivel_1.form-control.ddlNormal.ddlFirst");
var $ddlNivel2 = $("#ddlNivel2");

var $ddlNormal = $(".ddlNormal");
//var $ddlNivel3 = $("#ddlNivel3");

//$ddlNivel1.change(function () {
//    var val = $("#ddlNivel1 option:selected").val();

//    $.ajax({
//        cache: false,
//        type: "GET",
//        url: _basePath + 'Data/GetAreaTematicaNivel',
//        data: { nivel: 2, id: val },
//        success: function (data) {
//            $ddlNivel2.empty();
//            //$ddlNivel3.empty();
//            $("#chkNivel3List").html("");
//            $.each(data, function (id, option) {
//                $ddlNivel2.append($('<option></option>').val(option.cnID_Lista).html(option.ctElemento));
//            });

//            //$('form').formValidation('revalidateField', 'ctEmail');
//        },
//        error: function (xhr, ajaxOptions, thrownError) { }
//    });
//});

//$ddlNivel2.change(function () {
//    var val = $("#ddlNivel2 option:selected").val();

//    $.ajax({
//        cache: false,
//        type: "GET",
//        url: _basePath + 'Data/GetAreaTematicaNivel',
//        data: { nivel: 3, id: val },
//        success: function (data) {
//            //$ddlNivel3.empty();

//            $.each(data, function (id, option) {
//                //$ddlNivel3.append($('<option></option>').val(option.cnID_Lista).html(option.ctElemento));
//                var checked = 'checked';
//                if (!option.cnChecked)
//                    checked = '';

//                var template = '<div><input  type="checkbox" ' + checked + ' id="chkNivel3List_' + option.cnID_Lista + '" name="chkNivel3List[]" value="' + option.cnID_Lista + '" /> <label for="chkNivel3List_' + option.cnID_Lista + '">' + option.ctElemento + '</label></div>';
//                $("#chkNivel3List").append(template);
//            });

//            //$('form').formValidation('revalidateField', 'ctEmail');
//        },
//        error: function (xhr, ajaxOptions, thrownError) { }
//    });

//});


$ddlNormal.change(function () {
    var $current = $(this);
    var val = $(this).find("option:selected").val();
    var nextLevel = $(this).attr("data-next-level");
    var $nextControl = null;
    var last = false;
    if ($(this).hasClass("ddlLast")) {
        $nextControl = $("#chkLastLevelList");
        last = true;
    }
    else {
        $nextControl = $("#ddlNivel_" + nextLevel);
    }

    //console.log($nextControl);

    $.ajax({
        cache: false,
        type: "GET",
        url: _basePath + 'Data/GetAreaTematicaNivel',
        data: { nivel: nextLevel, id: val, last: last },
        success: function (data) {

            $.each($ddlNormal, function (index, item) {
                //console.log($(this));
                if ($(this).attr("current-level") > $current.attr("current-level")) {
                    $(this).empty();
                }
            });

            $("#chkLastLevelList").html("");

            if (last) {
                $nextControl.html("");

                $.each(data, function (id, option) {
                    var checked = 'checked';
                    if (!option.cnChecked)
                        checked = '';

                    var template = '<div><input  type="checkbox" ' + checked + ' id="chkLastLevelList_' + option.cnID_Lista + '" name="chkLastLevelList[]" value="' + option.cnID_Lista + '" /> <label for="chkLastLevelList_' + option.cnID_Lista + '">' + option.ctElemento + '</label></div>';
                    $nextControl.append(template);
                });
            }
            else {
                $nextControl.empty();
                $.each(data, function (id, option) {
                    $nextControl.append($('<option></option>').val(option.cnID_Lista).html(option.ctElemento));
                });

            }
        },
        error: function (xhr, ajaxOptions, thrownError) { }
    });

});


$("#btnAgregar").click(function () {

    if ($.trim($("#txtAreaEspecifica").val()) != '') {
        $("#ctDominioEspecifico").tagsinput('add', $("#txtAreaEspecifica").val());
        $("#txtAreaEspecifica").val('');
    }

    $.post(_basePath + 'Seleccion/SaveAreaTematica',
           {
               cnID_AreaTematica: $("#cnID_AreaTematica").val(),
               model: JSON.stringify($('form').serializeObject()),
               //chkNivel3List: $("input[name='chkNivel3List[]']").map(function () { return $(this).val(); }).get(),
               ctDominioEspecifico: $("#ctDominioEspecifico").tagsinput('items')

           },
           function (resp) {
               if (resp.result == "OK") {
                   $("#cnID_AreaTematica").val(resp.id);
                   ShowResponse(resp.result);

                   //setTimeout(function () { location.href = _basePath + 'Seleccion/AreaTematica'; }, 1000);

               }
               else
                   bootboxError("Ha ocurrido un error al guardar sus datos.");

           });
});

$("#btnGuardar").click(function () {
    if ($(".ddlLast option:selected").val() == undefined) {
        bootboxError("No ha seleccionado ningún elemento.");
        return;
    }

    $.post(_basePath + 'Seleccion/SaveAreaTematicaDet',
           {
               cnID_AreaTematica: $("#cnID_AreaTematica").val(),
               cnID_AreaTematicaNivel2: $(".ddlLast option:selected").val(),
               chkNivel3List: $("input[name='chkLastLevelList[]']").map(function () {
                   //console.log($(this).is(":checked"));
                   if ($(this).is(":checked") == true)
                       return $(this).val();
               }).get()

           },
           function (resp) {
               if (resp.result == "OK") {
                   $("#cnID_AreaTematica").val(resp.id);
                   ShowResponse(resp.result);

                   //setTimeout(function () { location.href = _basePath + 'Seleccion/AreaTematica'; }, 1000);

               }
               else
                   bootboxError("Ha ocurrido un error al guardar sus datos.");

           });

});

$('#ctDominioEspecifico').on('itemRemoved', function (event) {
    $('#ctDominioEspecifico').tagsinput('refresh');

    $("#btnAgregar").trigger('click');
});

$(document).ready(function () {
    _basePath = $("#hdBasePath").val();
})