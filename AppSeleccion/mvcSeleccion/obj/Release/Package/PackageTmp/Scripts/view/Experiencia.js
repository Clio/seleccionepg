﻿
$('#txtFechaInicioContainer').datetimepicker({
    format: 'DD/MM/YYYY',
    locale: 'es'
})
.on('dp.change', function (e) {
    $('form').formValidation('revalidateField', 'cfFechaInicio');
});

$('#txtFechaFinContainer').datetimepicker({
    format: 'DD/MM/YYYY',
    locale: 'es'
})
.on('dp.change', function (e) {
    $('form').formValidation('revalidateField', 'cfFechaFin');
});


$('form').formValidation(
     {
         excluded: [':disabled'],
         fields: {
             txtCertificado: {
                 validators: {
                     file: {
                         extension: 'pdf',
                         type: 'application/pdf',
                         maxSize: $("#hdnMaxSizeFile").val(),
                         message: 'Sólo se permiten archivos en formato PDF con un tamaño de hasta 3 MB.'
                     }
                     //notEmpty: {
                     //    message: ' '
                     //}
                 }


             }
         }
     }
    )
    .on('err.field.fv', function (e, data) {

        //if (data.field == 'txtCertificado') return;

        // Hide the messages
        data.element
            .data('fv.messages')
            .find('.help-block[data-fv-for="' + data.field + '"]').hide();
    })
    .on('submit', function (e) {

    //$("#divCargarArchivo").removeClass("has-error");

    //if ($("input[name=txtCertificado]").val() == '') {
    //    $("#divCargarArchivo").addClass("has-error");
    //    return false;
    //}

    if (e.isDefaultPrevented()) {
        // handle the invalid form...

    }
    else {
        return true;
    }

    });


$("#lnkDescargar").click(function () {
    var file = $(this).attr("data-filename");

    $.ajax({
        url: $("#hdnDownloadFileUrl").val(),
        type: 'GET',
        data: {
            file: file
        },
        //dataType: 'json',
        //contentType: 'application/json; charset=utf-8',
        success: function (response) {

            if (response.result != undefined) {
                bootboxError(response.result);
                return;
            }
            //location.href = $("#hdnDownloadFileUrl").val() + "?file=" + file;
            window.open($("#hdnDownloadFileUrl").val() + "?file=" + file, "_blank");
        },
        error: function () {
            bootboxAlert("error");
        }
    });

})