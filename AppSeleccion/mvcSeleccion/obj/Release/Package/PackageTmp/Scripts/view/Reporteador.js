﻿var _basePath;

$("#cnID_Table").change(function () {
    var val = $("#cnID_Table option:selected").val();

    $.ajax({
        cache: false,
        type: "GET",
        url: _basePath + 'Data/GetListByGroupId',
        data: { group: val },
        success: function (data) {
            $("#cnID_Columna").empty();

            $.each(data, function (id, option) {
                $("#cnID_Columna").append($('<option></option>').val(option.cnID_Lista).html(option.ctElemento));
            });

        },
        error: function (xhr, ajaxOptions, thrownError) { }
    });
});


$("#cnID_Columna").dblclick(function () {
    var selected = $("#cnID_Columna option:selected");
    if ($.trim($(selected).text()) != '')
        $("#cnID_ColumnaSeleccionada").append($('<option></option>').val($(selected).val()).html($(selected).text()));
});

$("#cnID_ColumnaSeleccionada").dblclick(function () {
    $("#cnID_ColumnaSeleccionada option:selected").remove();

});



$(document).ready(function () {
    _basePath = $("#hdBasePath").val();
})