﻿var _basePath;
var $cnID_Grado = $("#cnID_Grado");
var $ddlSituacion = $("#cnID_Situacion");

$('#txtFechaInicioContainer').datetimepicker({
    format: 'DD/MM/YYYY',
    locale: 'es'
})
 .on('dp.change', function (e) {
     $('form').formValidation('revalidateField', 'cfFechaInicio');
 });

$('#txtFechaFinContainer').datetimepicker({
    format: 'DD/MM/YYYY',
    locale: 'es'
})
.on('dp.change', function (e) {
    $('form').formValidation('revalidateField', 'cfFechaFin');
});

$("#cnID_Grado").change(function (e) {
    $.ajax({
        cache: false,
        type: "GET",
        url: _basePath + 'Data/GetItemByListID',
        data: { "cnID_Lista": $("#cnID_Grado").val() },
        success: function (data) {
            

            if (data.cnObligatorio == true) {
                $("input[name=txtCertificado]").attr("required", "required");
                $('form').formValidation('addField', $('[name="txtCertificado"]'));               
                
            }
            else {
                $("input[name=txtCertificado]").removeAttr("required");
                $('form').formValidation('removeField', $('[name="txtCertificado"]'));
            }

           
        },
        error: function (xhr, ajaxOptions, thrownError) { }
    });
});

$('form').formValidation(
    {
        excluded: [':disabled'],
        fields: {
            txtCertificado: {
                validators: {
                    file: {
                        extension: 'pdf',
                        type: 'application/pdf',
                        maxSize: $("#hdnMaxSizeFile").val(),
                        message: 'Sólo se permiten archivos en formato PDF con un tamaño de hasta 3 MB.'
                    }
                }


            }
        }
    }
    )
    .on('err.field.fv', function (e, data) {
         
        //if (data.field == 'txtCertificado') return;
        // Hide the messages
        data.element
            .data('fv.messages')
            .find('.help-block[data-fv-for="' + data.field + '"]').hide();
    })
    .on('submit', function (e) {

        //$("#divCargarArchivo").removeClass("has-error");

        //if ($("input[name=txtCertificado]").val() == '') {
        //    $("#divCargarArchivo").addClass("has-error");
        //    return false;
        //}

        if (e.isDefaultPrevented()) {
            // handle the invalid form...

        }
        else {
            return true;
        }

    });

$("#lnkDescargar").click(function () {
    var file = $(this).attr("data-filename");

    $.ajax({
        url: $("#hdnDownloadFileUrl").val(),
        type: 'GET',
        data: {
            file: file
        },
        //dataType: 'json',
        //contentType: 'application/json; charset=utf-8',
        success: function (response) {

            if (response.result != undefined) {
                bootboxError(response.result);
                return;
            }
            //location.href = $("#hdnDownloadFileUrl").val() + "?file=" + file;
            window.open($("#hdnDownloadFileUrl").val() + "?file=" + file, "_blank");

        },
        error: function () {
            bootboxAlert("error");
        }
    });

})

$cnID_Grado.change(function () {

    //$ddlSituacion.removeAttr("disabled");
    var _idGrado = $(this).val();
    $ddlSituacion.empty();    

    $.ajax({
        cache: false,
        type: "GET",
        url: _basePath + 'Data/GetSituacion',
        data: { "idGrado": _idGrado },
        success: function (data) {
            $ddlSituacion.empty();
            $ddlSituacion.append($('<option></option>').val("").html("Seleccionar"));
            $.each(data, function (id, option) {
                $ddlSituacion.append($('<option></option>').val(option.Value).html(option.Text));
            });

            if ($("#hdnID_Situacion").val() > 0) {
                $ddlSituacion.val($("#hdnID_Situacion").val());
                //$ddlSituacion.trigger('change');
            }

            $('form').formValidation('revalidateField', 'cnID_Situacion');
        },
        error: function (xhr, ajaxOptions, thrownError) {}
    });

    $ddlSituacion.trigger('change');
});

$(document).ready(function () {
    _basePath = $("#hdBasePath").val();
})