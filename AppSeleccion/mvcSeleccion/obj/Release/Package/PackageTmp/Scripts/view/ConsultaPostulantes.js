﻿$(window).ready(function () {

    var _basePath = $("#hdBasePath").val();
    //hdnConsultaPostulantesUrl




    $("#btnBuscar").click(function () {
        var _cnCobertura = $("#cnCobertura").val();
        if (_cnCobertura == "")
            _cnCobertura = 0;

        $.get($("#hdnConsultaPostulantesUrl").val(),
            {
                cID_Proceso: $("#cID_Proceso").val(),
                cID_Resultado: $("#cID_Resultado").val(),
                ctNumeroDNI: $("#ctNumeroDNI").val(),
                ctEmail: $("#ctEmail").val(),
                cnCobertura: _cnCobertura,
                cnZona: $("#cnZona").val()
            },
            function (result) {
                $("#divResultadoPostulantes").html(result);
            })

    });

    $("#btnExportar").click(function () {
        window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('#divResultadoPostulantes').html()));
        e.preventDefault();
    });


    $("#cID_Proceso").change(function () {

        LoadCoberturas();

    })

    function LoadCoberturas() {
        var _idProceso = $("#cID_Proceso").val();

        $.ajax({
            cache: false,
            type: "GET",
            url: _basePath + 'Data/GetCoberturas',
            data: { "idProceso": _idProceso },
            success: function (data) {

                $("#cnCobertura").empty();

                $.each(data, function (id, option) {
                    $("#cnCobertura").append($('<option></option>').val(option.cnID_Cobertura).html(option.ctNombre));
                });
                LoadZonas();
                
            },
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });
    }

    LoadCoberturas();

    $("#cnCobertura").change(function () {
        LoadZonas();
    })

    function LoadZonas() {
        var $ddlCoberturaZona = $("#cnZona");
        var id = $("#cnCobertura").val();
        $.ajax({
            cache: false,
            type: "GET",
            url: _basePath + 'Data/GetCoberturaZonas',
            data: {
                "idCobertura": id,
                addTodos :true
            },
            success: function (data) {

                $ddlCoberturaZona.empty();                
                $.each(data, function (_id, option) {
                    if ($("#ddlListadoCobertura option[value='" + option.cnID_CoberturaZona + "']").length > 0)
                        $ddlCoberturaZona.append($('<option></option>').val(option.cnID_CoberturaZona).html(option.ctZona).attr("disabled", "disabled"));
                    else
                        $ddlCoberturaZona.append($('<option></option>').val(option.cnID_CoberturaZona).html(option.ctZona));
                });
            },
            error: function (xhr, ajaxOptions, thrownError) {

            }
        });
    }

    setInterval(function () { $("#btnBuscar").click(); }, 180000);

    $("#btnImprimir").click(function () {

        //validar que haya seleccionado al menos un postulante
        var postulantes = [];
        $(".chkSeleccionar").map(function (e, a) {

            if ($(a).is(":checked")) {
                postulantes.push($(a).attr("data-id-postulante"));
            }
        });
        if (postulantes.length == 0) {
            bootboxError("Seleccione al menos un registro");
            return;
        }

        bootboxNoButton("Generando archivos. Espere un momento por favor...", '');

        $.post(_basePath + 'Seleccion/GetHojaVidaMasivo',
            {
                cnID_Proceso: $("#cID_Proceso").val(),
                cnID_Postulante: postulantes
            },
            function (result) {
                bootbox.hideAll();
                if (result.result == "OK") {
                    bootboxAlert("Las hojas de vida de los postulantes seleccionados se han generado correctamente.");
                }
                else
                    bootboxError("Ha ocurrido un error al generar las hojas de vida de los postulantes seleccionados.");
            });

    });


});