﻿$(window).ready(function () {
    var _basePath = $("#hdBasePath").val();

    $("#btnBuscar").click(function () {
        $.get($("#hdnConsultaCandidatosUrl").val(),
            {
                ctNombreCompleto: $("#ctNombreCompleto").val(),
                ctNumeroDNI: $("#ctNumeroDNI").val(),
                ctEmail: $("#ctEmail").val()
            },
            function (result) {
                $("#divResultadoCandidatos").html(result);
            })

    });

    $(document).on("click", ".open-AddBookDialog", function () {
        var data = $(this).data('id').split("/");
        $('#hddIdCandidato').val(data[0]);
        $('#textoEmail').val(data[1]);
        $('#modalActualizarEmail').modal('show');
        LoadForm(_basePath);
    });
});

function LoadForm(_basePath) {

    $('form').formValidation(
    {
        //excluded: [':disabled'],
        fields: {
            textoEmail: {
                validators: {
                    notEmpty: {
                        message: 'Este campo es obligatorio.'
                    },
                    regexp: {
                        regexp: /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i,
                        message: 'Correo no válido.'
                    },
                    callback: {
                        message: 'Este correo ya se encuentra registrado.',
                        callback: function (value, validator, $field) {
                            $.ajaxSetup({ async: false });
                            var res = false;
                            $.get(_basePath + 'Data/ValidarEmail',
                                {
                                    cnID_Candidato: $("#hddIdCandidato").val(),
                                    mail: value
                                },
                                function (result) {
                                    res = result;
                                });
                            $.ajaxSetup({ async: true });
                            return res;
                        }
                    }
                }
            }
        }

    }
)
.on('err.field.fv', function (e, data) {
    // Hide the messages
    //return;
    if (data.field == 'textoEmail') return;

    //console.log(data.field);

    data.element
        .data('fv.messages')
        .find('.help-block[data-fv-for="' + data.field + '"]').hide();

    var $tabPane = data.element.parents('.tab-pane'),
           tabId = $tabPane.attr('id');

    $('a[href="#' + tabId + '"][data-toggle="tab"]')
        .parent()
        .find('i')
        .removeClass('fa-check')
        .addClass('fa-times');

})
 .on('submit', function (e) {


     if (e.isDefaultPrevented()) {
         // handle the invalid form...

     }
     else {
         var newEmail = $("#textoEmail").val();
         var idCandidato = $("#hddIdCandidato").val();
         $.post($("#hddUrlSaveEmail").val(),
             {
                 idCandidato: idCandidato,
                 nuevoEmail: newEmail
             },
             function () {
                 $('#modalActualizarEmail').modal('hide');
                 bootboxAlert("Actualizado correctamente");
                 $("#btnBuscar").click();
             })
         return false;
     }
 });
}
