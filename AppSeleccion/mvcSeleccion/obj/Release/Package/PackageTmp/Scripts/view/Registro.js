﻿
//$(window).load(function () {
var _basePath;

var $ddlDepartamento = $("#cnID_Departamento");
var $cnID_Pais = $("#cnID_Pais");
var $hdnID_PaisPeru = $("#hdnID_PaisPeru");
var $hdnID_Otros = $("#hdnID_Otros");
var $hdComboOtros = $("#hdComboOtros");


var $ddlProvincia = $("#cnID_Provincia");
var $ddlDistrito = $("#cnID_Distrito");


$('#txtFechaNacimientoContainer').datetimepicker({
    format: 'DD/MM/YYYY',
    locale: 'es'
})
.on('dp.change', function (e) {
    $('form').formValidation('revalidateField', 'cfFechaNacimiento');
});




if ($("#cnID_Candidato").val() == 0) {
    $ddlDepartamento.attr("disabled", "disabled");
    $ddlProvincia.attr("disabled", "disabled");
    $ddlDistrito.attr("disabled", "disabled");

    $ddlDepartamento.append($('<option></option>').val($hdnID_Otros.val()).html($hdComboOtros.val()));
    $ddlProvincia.append($('<option></option>').val($hdnID_Otros.val()).html($hdComboOtros.val()));
    $ddlDistrito.append($('<option></option>').val($hdnID_Otros.val()).html($hdComboOtros.val()));

    $ddlDepartamento.val($hdnID_Otros.val());
    $ddlProvincia.val($hdnID_Otros.val());
    $ddlDistrito.val($hdnID_Otros.val());
}

$cnID_Pais.change(function () {

    $ddlDepartamento.removeAttr("disabled");
    $ddlProvincia.removeAttr("disabled");
    $ddlDistrito.removeAttr("disabled");

    var _idPais = $(this).val();
    $ddlDepartamento.empty();
    $ddlProvincia.empty();
    $ddlDistrito.empty();

    if (_idPais != $hdnID_PaisPeru.val()) {


        $ddlDepartamento.append($('<option></option>').val($hdnID_Otros.val()).html($hdComboOtros.val()).attr("selected", "selected"));
        $ddlProvincia.append($('<option></option>').val($hdnID_Otros.val()).html($hdComboOtros.val()).attr("selected", "selected"));
        $ddlDistrito.append($('<option></option>').val($hdnID_Otros.val()).html($hdComboOtros.val()).attr("selected", "selected"));

        $ddlDepartamento.attr("disabled", "disabled");
        $ddlProvincia.attr("disabled", "disabled");
        $ddlDistrito.attr("disabled", "disabled");

        return;
    }

    $.ajax({
        cache: false,
        type: "GET",
        url: _basePath + 'Data/GetDepartamentos',
        data: { "idPais": _idPais },
        success: function (data) {
            $ddlDepartamento.empty();
            $ddlDepartamento.append($('<option></option>').val("").html(""));
            $.each(data, function (id, option) {
                $ddlDepartamento.append($('<option></option>').val(option.cnID_Departamento).html(option.ctDepartamento));
            });

            if ($("#hdID_Departamento").val() > 0) {
                $ddlDepartamento.val($("#hdID_Departamento").val());
                $ddlDepartamento.trigger('change');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });
});

//$cnID_Pais.change(function () {
//    $ddlDepartamento.empty();
//    $ddlProvincia.empty();
//    $ddlDistrito.empty();
//});

$ddlDepartamento.change(function () {
    var _idDepartamento = $(this).val();

    $.ajax({
        cache: false,
        type: "GET",
        url: _basePath + 'Data/GetProvincias',
        data: { "idDepartamento": _idDepartamento },
        success: function (data) {
            $ddlProvincia.empty();
            $ddlDistrito.empty();
            $ddlProvincia.append($('<option></option>').val("").html(""));
            $.each(data, function (id, option) {
                $ddlProvincia.append($('<option></option>').val(option.cnID_Provincia).html(option.ctProvincia));
            });

            if ($("#hdnID_Provincia").val() > 0) {
                $ddlProvincia.val($("#hdnID_Provincia").val());
                $ddlProvincia.trigger('change');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });
});

$ddlProvincia.change(function () {
    var _idProvincia = $(this).val();

    $.ajax({
        cache: false,
        type: "GET",
        url: _basePath + 'Data/GetDistritos',
        data: { "idProvincia": _idProvincia },
        success: function (data) {
            $ddlDistrito.empty();
            $ddlDistrito.append($('<option></option>').val("").html(""));
            $.each(data, function (id, option) {
                $ddlDistrito.append($('<option></option>').val(option.cnID_Distrito).html(option.ctDistrito));
            });

            if ($("#hdnID_Distrito").val() > 0) {
                $ddlDistrito.val($("#hdnID_Distrito").val());
                //$ddlDistrito.trigger('change');
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {

        }
    });
});


$('form').formValidation(
    {
        excluded: [':disabled'],
        fields: {
            'tbDocIdentidadModelList_cnID_TipoDocumento[]': {
                validators: {
                    notEmpty: {
                        message: ''
                    }
                }
            },
            'tbDocIdentidadModelList_ctNumero[]': {
                notEmpty: {
                    message: 'Este campo es obligatorio.'
                },
                validators: {
                    notEmpty: {
                        message: 'Este campo es obligatorio.'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/i,
                        message: 'Sólo se permiten caracteres numéricos.'
                    },
                    callback: {
                        message: 'Este número de documento ya se encuentra registrado',
                        callback: function (value, validator, $field) {
                            var _obj = $field;
                            //console.log($field.parent());
                            var tipDoc = $(_obj).parent().parent().parent().find("select option").filter(":selected").val();
                            //console.log(tipDoc);

                            if (tipDoc == undefined)
                                return false;

                            var result = existeValorDoc($("input[name='tbDocIdentidadModelList_ctNumero[]']"), value, tipDoc);


                            if (result) {
                                return false;
                            } else {

                                $.ajaxSetup({ async: false });
                                var res = false;
                                $.get(_basePath + 'Data/ValidarDocumento',
                                    {
                                        cnID_Candidato: $("#cnID_Candidato").val(),
                                        tipDoc: tipDoc,
                                        nroDoc: value
                                    },
                                    function (result) {
                                        res = result;
                                    });
                                $.ajaxSetup({ async: true });
                                return res;
                            }
                        }
                    }
                }
            },
            'cnID_Sexo': {
                validators: {
                    notEmpty: {
                        message: ''
                    }
                }
            },
            'ctEstadoCivil': {
                validators: {
                    notEmpty: {
                        message: ''
                    }
                }
            },
            ctEmail: {
                validators: {
                    notEmpty: {
                        message: 'Este campo es obligatorio.'
                    },
                    regexp: {
                        regexp: /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i,
                        message: 'Correo no válido.'
                    },
                    callback: {
                        message: 'Este correo ya se encuentra registrado.',
                        callback: function (value, validator, $field) {
                            $.ajaxSetup({ async: false });
                            var res = false;
                            $.get(_basePath + 'Data/ValidarEmail',
                                {
                                    cnID_Candidato: $("#cnID_Candidato").val(),
                                    mail: value
                                },
                                function (result) {
                                    res = result;
                                });
                            $.ajaxSetup({ async: true });
                            return res;
                        }
                    }
                }
            },
            /*telefono: {
                selector: '.telephone',
                validators: {
                    callback: {
                        message: 'Debes ingresar al menos un teléfono',
                        callback: function (value, validator, $field) {
                            var isEmpty = true;
                            // Get the list of fields
                            var $fields = validator.getFieldElements('telefono');
                            for (var i = 0; i < $fields.length; i++) {
                                if ($fields.eq(i).val() !== '') {
                                    isEmpty = false;
                                    break;
                                }
                            }

                            if (!isEmpty) {
                                // Update the status of callback validator for all fields
                                validator.updateStatus('telefono', validator.STATUS_VALID, 'callback');
                                return true;
                            }

                            return false;
                        }
                    },
                    telephone: {
                        message: 'error'
                    }
                }

            },*/
            ctTelefonoCasa:
               {
                   validators: {
                       notEmpty: {
                           message: 'Debes ingresar al menos un teléfono'
                       }
                   }
               },
            ctTelefonoMovil:
                {
                    enabled: false,
                    validators: {
                        notEmpty: {
                            message: 'Debes ingresar al menos un teléfono'
                        }
                    }
                },
            ctClaveAcceso2: {
                validators: {
                    identical: {
                        field: 'ctClaveAcceso',
                        message: 'Las contraseñas no coinciden.'
                    }
                }
            }
        }

    }
    )
     .on('err.field.fv', function (e, data) {
         // Hide the messages
         //return;
         if (data.field == 'tbDocIdentidadModelList_ctNumero[]') return;
         if (data.field == 'ctClaveAcceso2') return;
         if (data.field == 'ctEmail') return;

         //console.log(data.field);

         data.element
             .data('fv.messages')
             .find('.help-block[data-fv-for="' + data.field + '"]').hide();

         var $tabPane = data.element.parents('.tab-pane'),
                tabId = $tabPane.attr('id');

         $('a[href="#' + tabId + '"][data-toggle="tab"]')
             .parent()
             .find('i')
             .removeClass('fa-check')
             .addClass('fa-times');

         if (data.fv.getSubmitButton()) {
             data.fv.disableSubmitButtons(false);
         }

     })
.on('success.field.fv', function (e, data) {
    // data.fv      --> The FormValidation instance
    // data.element --> The field element

    var $tabPane = data.element.parents('.tab-pane'),
        tabId = $tabPane.attr('id'),
        $icon = $('a[href="#' + tabId + '"][data-toggle="tab"]')
                    .parent()
                    .find('i')
                    .removeClass('fa-check fa-times');

    // Check if all fields in tab are valid
    var isValidTab = data.fv.isValidContainer($tabPane);
    if (isValidTab !== null) {
        $icon.addClass(isValidTab ? 'fa-check' : 'fa-times');
    }

    if (data.fv.getSubmitButton()) {
        data.fv.disableSubmitButtons(false);
    }
})
    .on('keyup', '[name="ctTelefonoCasa"], [name="ctTelefonoMovil"]', function (e) {
        var driverLicense = $('form').find('[name="ctTelefonoMovil"]').val(),
            ssn = $('form').find('[name="ctTelefonoCasa"]').val(),
            fv = $('form').data('formValidation');

        switch ($(this).attr('name')) {
            // User is focusing the ssn field
            case 'ctTelefonoCasa':
                fv.enableFieldValidators('ctTelefonoMovil', ssn === '').revalidateField('ctTelefonoMovil');

                if (ssn && fv.getOptions('ctTelefonoCasa', null, 'enabled') === false) {
                    fv.enableFieldValidators('ctTelefonoCasa', true).revalidateField('ctTelefonoCasa');
                } else if (ssn === '' && driverLicense !== '') {
                    fv.enableFieldValidators('ctTelefonoCasa', false).revalidateField('ctTelefonoCasa');
                }
                break;

                // User is focusing the drivers license field
            case 'ctTelefonoMovil':
                if (driverLicense === '') {
                    fv.enableFieldValidators('ctTelefonoCasa', true).revalidateField('ctTelefonoCasa');
                } else if (ssn === '') {
                    fv.enableFieldValidators('ctTelefonoCasa', false).revalidateField('ctTelefonoCasa');
                }

                if (driverLicense && ssn === '' && fv.getOptions('ctTelefonoMovil', null, 'enabled') === false) {
                    fv.enableFieldValidators('ctTelefonoMovil', true).revalidateField('ctTelefonoMovil');
                }
                break;

            default:
                break;
        }
    })
.on('submit', function (e) {

    if (e.isDefaultPrevented()) {
        // handle the invalid form...

    } else {

        //if ($.trim($("#ctTelefonoCasa").val()) == '' || $.trim($("#ctTelefonoMovil").val()) == '') {
        //    bootbox.alert("Ingrese el teléfono de casa o el teléfono móvil.");
        //    return false;
        //}

        $.post(_basePath + 'Seleccion/SaveRegistro',
            {
                model: JSON.stringify($('form').serializeObject()),
                tbDocIdentidadModelList_cnID_TipoDocumento: $("select[name='tbDocIdentidadModelList_cnID_TipoDocumento[]']").map(function () { return $(this).val(); }).get(),
                tbDocIdentidadModelList_ctNumero: $("input[name='tbDocIdentidadModelList_ctNumero[]']").map(function () { return $(this).val(); }).get()
            },
            function (resp) {

                if (resp == "OK") {

                    ShowResponse(resp, "Usted se ha registrado correctamente.");
                    if ($("#hdNewUser").val() == "1") {
                        setTimeout(function () { location.href = $("#hdLoginPage").val(); }, 2000);

                    }
                    else {
                        setTimeout(function () { location.href = _basePath + 'Seleccion/DatosPersonales'; }, 1000);
                    }
                }
                else
                    bootboxError("Ha ocurrido un error al guardar sus datos.");

            });


        return false;
    }
})

//var i = 0;
//$("#add_row").click(function () {

//    if ($("#ddlTipoDocumento").val() == '0' || $.trim($("#txtNroDocumento").val()) == '') {
//        bootbox.alert("Seleccione tipo e ingrese número de documento");
//        return;
//    }

//    $('#addr' + i).html("<td><input id='hdnTipoDocumento" + (i) + "' type='hidden' value='" + $("#ddlTipoDocumento").val() + "'>" + $("#ddlTipoDocumento option:selected").text() + "</td><td>" + $("#txtNroDocumento").val() + "</td>");

//    $('#tab_logic').append('<tr id="addr' + (i + 1) + '"></tr>');
//    i++;
//});

//$("#delete_row").click(function () {
//    if (i > 1) {
//        $("#addr" + (i - 1)).html('');
//        i--;
//    }
//});
//});


$(document).ready(function () {
    _basePath = $("#hdBasePath").val();
    var $cnID_Pais = $("#cnID_Pais option");
    var $cnID_PaisNacimiento = $("#cnID_PaisNacimiento option");
    var $ddlPaisesActual = $("#ddlPaisesActual option");
    var $ddlTipoDocumento = $("#ddlTipoDocumento option");
    var _index_ = 0;

    //$cnID_Pais.eq(_index_).before($('<option></option>').val("").html(""));
    //$cnID_PaisNacimiento.eq(_index_).before($('<option></option>').val("").html(""));
    //$ddlPaisesActual.eq(_index_).before($('<option></option>').val("").html(""));
    //$ddlTipoDocumento.eq(_index_).before($('<option></option>').val("").html(""));
    if ($("#cnID_Candidato").val() == 0) {
        $("#cnID_Pais").val("");
        $("#cnID_PaisNacimiento").val("");
        $("#tbDocIdentidadModelList_0_cnID_TipoDocumento").val("");
        $("#ddlPaisesActual").val("");
    }

    if ($("#hdnID_PaisNacimiento").val() > 0)
        $("#cnID_PaisNacimiento").val($("#hdnID_PaisNacimiento").val());

    if ($("#hdnID_Pais").val() > 0)
        $("#cnID_Pais").val($("#hdnID_Pais").val());

    if ($("#hdnID_Pais").val() > 0) {
        $("#cnID_Pais").trigger('change');
    }

})

function existeValorDoc(lista, valor, tipDoc) {
    var existe = false;
    var i = 0;
    $.each(lista,
            function (index, item) {
                var _tipDoc = $(item).parent().parent().parent().find("select option").filter(":selected").val();
                if ($.trim($(item).val()).toUpperCase() == valor.toUpperCase() && tipDoc == _tipDoc) {

                    if (i > 0)
                        existe = true;

                    i++;
                }
            });

    return existe;
}