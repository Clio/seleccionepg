$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function bootboxAlert(message) {
    bootbox.dialog(
        {
            message: message,
            title: 'Mensaje',
            buttons: {
                success: {
                    label: "Aceptar",
                    className: "btn-success"
                }
            }
        });
}
//bootboxNoButton

function bootboxNoButton(title, message) {
    message += '<div class="progress progress-striped active"> <div class="progress-bar progress-bar-success" style="width: 100%;"></div></div>';
    bootbox.dialog(
        {
            message: message,
            title: (typeof (title) == 'undefined' ? 'Mensaje' : title)

        });
}

function bootboxError(message) {
    bootbox.dialog(
    {
        message: message,
        title: 'Error',
        buttons: {
            error: {
                label: "Aceptar",
                className: "btn-danger"
            }
        }
    });
}

function bootboxConfirm(message, fnContinue) {
    bootbox.dialog(
        {
            message: message,
            title: 'Confirmaci&oacute;n',
            buttons: {
                yes: {
                    label: "S&iacute;",
                    className: "btn-success",
                    callback: function () {
                        if (fnContinue != null)
                            fnContinue();
                    }
                },
                no: {
                    label: "No",
                    className: "btn-danger"
                }
            }
        });
}

function bootboxShowContent(url, data, title, fn) {
    $.get(url, data, function (result) {
        bootbox.dialog({
            title: title,
            message: result
        });

        if (fn != null)
            fn();
    });

}

function ShowResponse(response, msg) {
    if (response == "OK") {
        if (msg == undefined)
            bootboxAlert("Guardado correctamente.");
        else
            bootboxAlert(msg);
        return true;
    }
    else {
        bootboxError("Ha ocurrido un error al guardar.");
        return false;
    }
}

function ShowResponseDelete(response, msgAdicionalError) {

    msgAdicionalError = typeof (msgAdicionalError) == "undefined" ? "" : msgAdicionalError;

    if (response == "OK") {
        bootboxAlert("Eliminado correctamente.");
        return true;
    }
    else {
        bootboxError("Ha ocurrido un error al eliminar el registro." + msgAdicionalError);
        return false;
    }
}


function SelectRow(tableId, rowId) {
    $("#" + rowId).css("background-color", "#00C0EF");

    setTimeout(function () {
        $("#" + rowId).animate({ backgroundColor: '#FFFFFF' }, 'slow');
    }, 5000);
}


function getCaretPosition(ctrl) {
    var CaretPos = 0;    // IE Support
    if (document.selection) {
        ctrl.focus();
        var Sel = document.selection.createRange();
        Sel.moveStart('character', -ctrl.value.length);
        CaretPos = Sel.text.length;
    }
        // Firefox support
    else if (ctrl.selectionStart || ctrl.selectionStart == '0') {
        CaretPos = ctrl.selectionStart;
    }

    return CaretPos;
}

function setCaretPosition(ctrl, pos) {
    if (ctrl.setSelectionRange) {
        ctrl.focus();
        ctrl.setSelectionRange(pos, pos);
    }
    else if (ctrl.createTextRange) {
        var range = ctrl.createTextRange();
        range.collapse(true);
        range.moveEnd('character', pos);
        range.moveStart('character', pos);
        range.select();
    }
}

/*
$("input[type=text],textarea").keyup(function () {
    
    // Remember original caret position
    var caretPosition = getCaretPosition(this);

    if (this.name == "ctEmail" || this.name == "txtEmail") 
        $(this).val($(this).val());
    else
        $(this).val($(this).val().toUpperCase());

    // Reset caret position
    // (we ignore selection length, as typing deselects anyway)
    setCaretPosition(this, caretPosition);
});
*/


$(document).on("keyup", "input[type=text], textarea", function () {
    // Remember original caret position
    var caretPosition = getCaretPosition(this);

    if (this.name == "ctEmail" || this.name == "txtEmail" || this.name == "textoEmail")
        $(this).val($(this).val());
    else
        $(this).val($(this).val().toUpperCase());

    // Reset caret position
    // (we ignore selection length, as typing deselects anyway)
    setCaretPosition(this, caretPosition);
});


function getZero(value) {
    if (value == "")
        return "0";
    return value;
}