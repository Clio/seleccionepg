﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;

namespace mvcSeleccion.Globals
{
    public static class Extensiones
    {
        public static string GetDesc(this System.Enum e)
        {
            FieldInfo field = e.GetType().GetField(e.ToString());
            if (field != null)
            {
                object[] attribs =
                  field.GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attribs.Length > 0)
                    return (attribs[0] as DescriptionAttribute).Description;
            }
            return e.ToString();
        }

    }
}