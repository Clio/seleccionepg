﻿using System.Net;
using System.Net.Mail;

namespace mvcSeleccion
{
    public class BaseEnvioCorreo
    {
        public static bool SendMail(string from_name, string subject, string message, string to)
        {
            SmtpClient smtpClient = new SmtpClient();
            

            NetworkCredential credentials = smtpClient.Credentials as NetworkCredential;
            try
            {
                MailAddress fromAddress = new MailAddress(credentials.UserName, from_name);

                MailMessage mailMessage = new MailMessage();

                mailMessage.From = fromAddress;

                mailMessage.To.Add(to);

                mailMessage.Subject = subject;

                mailMessage.IsBodyHtml = true;

                mailMessage.Body = message;

                smtpClient.Send(mailMessage);

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}