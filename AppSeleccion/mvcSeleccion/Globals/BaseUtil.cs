﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace mvcSeleccion
{
    public class BaseUtil
    {
        public static string GetIPAddress()
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                {
                    return addresses[0];
                }
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        public static bool SaveFile(HttpPostedFileBase file, string directory, out string fileName)
        {
            fileName = string.Empty;
            try
            {
                Guid guid = Guid.NewGuid();
                fileName = string.Format("{0}.{1}", guid.ToString(), Path.GetExtension(file.FileName).Replace(".", string.Empty));
                string path = Path.Combine(directory, fileName);
                file.SaveAs(path);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool SaveFile(byte[] bytes, string directory, string fileName)
        {
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            File.WriteAllBytes(directory + fileName, bytes);

            return true;
        }

        public static bool DeleteFile(string directory, string fileName)
        {
            try
            {
                File.Delete(Path.Combine(directory, fileName));
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static string EncryptPassword(string tcCadena)
        {
            if (tcCadena.Trim().Length == 0)
                return string.Empty;

            MD5 loEncriptarMD5 = MD5CryptoServiceProvider.Create();
            byte[] loCadenaBytes = loEncriptarMD5.ComputeHash(Encoding.Default.GetBytes(tcCadena));

            StringBuilder lcCadenaEncriptada = new StringBuilder();
            for (int i = 0; i < loCadenaBytes.Length; i++)
                lcCadenaEncriptada.AppendFormat("{0:x2}", loCadenaBytes[i]);

            return lcCadenaEncriptada.ToString();
        }

        public static bool mtValidarDireccionesCorreo(string tcDireccionesCorreo)
        {
            char[] lcSeparadores = new char[] { ',', ';' };
            bool llRetorno = true;
            string[] lcDireccionesCorreo = tcDireccionesCorreo.Split(lcSeparadores);
            lcDireccionesCorreo.ToList().ForEach(lcCorreo =>
            {
                if (!mtValidarDireccionCorreo(lcCorreo))
                {
                    llRetorno = false;
                }
            });

            return llRetorno;
        }

        public static bool mtValidarDireccionCorreo(string tcDireccionCorreo)
        {
            tcDireccionCorreo = tcDireccionCorreo.Trim();
            string loExpresionRegular = @"^(([^<>()[\]\\.,;:\s@\""]+"
                        + @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@"
                        + @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
                        + @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+"
                        + @"[a-zA-Z]{2,}))$";

            Regex loRegex = new Regex(loExpresionRegular);

            return loRegex.IsMatch(tcDireccionCorreo);
        }

        /// <summary>
        /// Converts the provided app-relative path into an absolute Url containing the
        /// full host name
        /// </summary>
        /// <param name="relativeUrl">App-Relative path</param>
        /// <returns>Provided relativeUrl parameter as fully qualified Url</returns>
        /// <example>~/path/to/foo to http://www.web.com/path/to/foo</example>
        public static string ToAbsoluteUrl(string relativeUrl)
        {
            if (string.IsNullOrEmpty(relativeUrl))
                return relativeUrl;

            if (HttpContext.Current == null)
                return relativeUrl;

            if (relativeUrl.StartsWith("/"))
                relativeUrl = relativeUrl.Insert(0, "~");
            if (!relativeUrl.StartsWith("~/"))
                relativeUrl = relativeUrl.Insert(0, "~/");

            var url = HttpContext.Current.Request.Url;
            var port = url.Port != 80 ? (":" + url.Port) : String.Empty;

            return String.Format("{0}://{1}{2}{3}",
                url.Scheme, url.Host, port, VirtualPathUtility.ToAbsolute(relativeUrl));
        }

        public static string GetDesc(System.Enum e)
        {
            FieldInfo field = e.GetType().GetField(e.ToString());
            if (field != null)
            {
                object[] attribs =
                  field.GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attribs.Length > 0)
                    return (attribs[0] as DescriptionAttribute).Description;
            }
            return e.ToString();
        }

        public static MemoryStream MergePdfForms(List<byte[]> files)
        {
            if (files.Count > 1)
            {
                PdfReader pdfFile;
                Document doc;
                PdfWriter pCopy;
                MemoryStream msOutput = new MemoryStream();

                pdfFile = new PdfReader(files[0]);

                doc = new Document();
                pCopy = new PdfSmartCopy(doc, msOutput);

                doc.Open();

                for (int k = 0; k < files.Count; k++)
                {
                    pdfFile = new PdfReader(files[k]);
                    PdfReader.unethicalreading = true;
                    for (int i = 1; i < pdfFile.NumberOfPages + 1; i++)
                    {
                        ((PdfSmartCopy)pCopy).AddPage(pCopy.GetImportedPage(pdfFile, i));
                    }
                    pCopy.FreeReader(pdfFile);
                }

                pdfFile.Close();
                pCopy.Close();
                doc.Close();

                return msOutput;
            }
            else if (files.Count == 1)
            {
                return new MemoryStream(files[0]);
            }

            return null;
        }

        public static string GetPasswordDefault()
        {
            return System.Configuration.ConfigurationManager.AppSettings["PasswordDefault"];
        }
    }

    public enum TipoArchivo
    {
        [Description("General")]
        General,
        [Description("Academico")]
        Academico,
        [Description("Laboral")]
        Laboral,
        [Description("Idioma")]
        Idiomas,
        [Description("Est. Compl.")]
        Complementario,
        
        [Description("Doc. Proceso")]
        Documentos
    }

     

    public class ArchivoPostulante
    {
        public long Id { get; set; }

        public string FileName { get; set; }

        public TipoArchivo TipoArchivo { get; set; }

        public ArchivoPostulante(string filename, TipoArchivo tipoArchivo, long id)
        {
            this.FileName = filename;
            this.TipoArchivo = tipoArchivo;
            this.Id = id;
        }
    }
}