﻿using iTextSharp.text;
using Microsoft.Reporting.WebForms;
using mvcSeleccion.dsEvaluacionManualTableAdapters;
using mvcSeleccion.dsPostulanteAcademicoTableAdapters;
using mvcSeleccion.dsPostulanteCoberturaTableAdapters;
using mvcSeleccion.dsPostulanteCritAdicionalTableAdapters;
using mvcSeleccion.dsPostulanteDatosPersonalesTableAdapters;
using mvcSeleccion.dsPostulanteLaboralEspecifTableAdapters;
using mvcSeleccion.dsPostulanteLaboralTableAdapters;
//using PdfSharp.Pdf;
//using PdfSharp.Pdf.IO;
using SalarDb.CodeGen.BLL;
using SalarDb.CodeGen.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mvcSeleccion
{
    public class BaseGlobal
    {
        public static string _Layout = "~/Views/Shared/_Layout.cshtml";
        public static string _ComboSeleccionar = "Seleccionar";
        public static string _ComboOtros = "Otros";
        public static string gcSesionUsuario = "gcSesionUsuario";
        public static string gcSesionCandidato = "gcSesionCandidato";
        public static int Otros = 1634;
        public static int DepartamentoOtros = 26;
        public static int ProvinciaOtros = 196;
        public static int DistritoOtros = 1840;
        public static int LaboralGeneral = 235;
        public static int LaboralEspecifico = 236;
        public static int ConvocatoriaPublicada = 1632;
        public static int TipoEntidadPrivPub = 1631;
        public static int PaisPeru = 177;
        public static int TipoEvaluacionAños = 243;
        public static int Ninguno = 1648;

        public static int TipoMailPersonal = 1649;

        public static int GetTipoEntidadProceso(Int64 lnTipoEntidadExperiencia)
        {
            if (lnTipoEntidadExperiencia == 241)
                return 1628;
            else
                return 1629;
        }

        public static tbUsuarioModel UsuarioActual
        {
            get
            {
                return (tbUsuarioModel)HttpContext.Current.Session[gcSesionUsuario];
            }
            set
            {
                HttpContext.Current.Session[gcSesionUsuario] = value;
            }
        }

        public static tbCandidatoModel CandidatoActual
        {
            get
            {
                return (tbCandidatoModel)HttpContext.Current.Session[gcSesionCandidato];
            }
            set
            {
                HttpContext.Current.Session[gcSesionCandidato] = value;
            }
        }

        public static bool IsLogged
        {
            get
            {
                return BaseGlobal.UsuarioActual != null;
            }
        }

        public enum GruposListas
        {
            Area = 1,
            Cargo = 2,
            Carrera = 3,
            Grado = 5,
            Idioma = 6,
            Instituto = 7,
            NivelLecturaEscrituraConv = 8,
            Rubro = 10,
            Sexo = 11,
            Situacion = 12,
            TipoCriterioLaboral = 13,
            TiposDocumento = 14,
            TipoEntidad = 15,
            TipoEvaluacion = 16,
            Universidad = 18,
            TipoContrato = 19,
            EstadoCivil = 20,
            TipoEstudio = 21,
            TipoEntidadConvocatoria = 22,
            EstadoProceso = 23,
            General = 24,
            TipoResultado = 25,
            SituacionEducativa = 26,
            SituacionTecnica = 27,
            SituacionUniversitaria = 28,
            TipoEmail = 29,

            SituacionPHD = 30,
            SituacionDoctor = 31,
            SituacionMaestro = 32,
            SituacionMaster = 33,
            SituacionTitulo = 34,
            SituacionBachiller = 35,

            CategoriasDocente = 36,
            RegimenDedicacion = 37,

            AreaTematicaNivel1 = 38,
            AreaTematicaNivel2 = 39,
            AreaTematicaNivel3 = 40,

            [Description("ctDatos")]
            TablaDatosPersonales = 41,

            [Description("ctCarrera")]
            TablaCarrera = 42,

            [Description("ctLaboral")]
            TablaExperienciaLaboral = 43,

            [Description("ctDocencia")]
            TablaDocencia = 44,

            [Description("ctIdioma")]
            TablaIdioma = 45,

            [Description("ctTematica")]
            TablaAreaTematica = 51,


            GradoExperienciaInstitucion = 46,
            TipoExperienciaInstitucion = 47,
            UnidadMedidaExperienciaInstitucion = 48,


            AreaTematicaNivel = 50
        }

        public enum Grado
        {
            //Primaria = 252,
            //Secundaria = 251,
            //Tecnico = 250,
            //Doctorado = 247,
            //Maestria = 248,
            //Universitario = 249
            PHD = 1650,
            Doctor = 247,
            Maestro = 248,
            Master_Internacional = 1651,
            Master_Nacional = 1652,
            Titulo_Profesional = 1653,
            Bachiller = 1654,
            Certificación = 1655,
            Diplomado = 1656,
            Curso = 1657
        }

        public enum Parametros
        {
            FileSize
        }

        public enum Perfiles
        {
            Candidato = 1,
            Evaluador = 2
        }

        public static List<SelectListItem> GetPaisListItem()
        {
            tbPaisBLL lotbPaisBLL = new tbPaisBLL();
            var loPaises = lotbPaisBLL.GetAll();

            loPaises.Sort((x, y) => x.ctPais.CompareTo(y.ctPais));

            List<SelectListItem> loPaisesList = new List<SelectListItem>();

            loPaisesList.AddRange((from x in loPaises select new SelectListItem() { Text = x.ctPais, Value = x.cnID_Pais.ToString() }).ToList());

            loPaisesList.Insert(0, new SelectListItem() { Text = _ComboSeleccionar, Value = string.Empty });

            return loPaisesList;
        }

        public static List<tbMaestroListasModel> GetMaestroList(GruposListas grupo, bool sort = true)
        {
            tbMaestroListasBLL tbMaestroListasBLL = new tbMaestroListasBLL();
            var loListado = tbMaestroListasBLL.GetBycnID_GrupoLista((int)grupo);

            if (sort)
                loListado.Sort((x, y) => x.ctElemento.CompareTo(y.ctElemento));

            return loListado;
        }

        public static List<tbMaestroListasModel> GetMaestroListInt(int grupo, bool sort = true)
        {
            tbMaestroListasBLL tbMaestroListasBLL = new tbMaestroListasBLL();
            var loListado = tbMaestroListasBLL.GetBycnID_GrupoLista(grupo);

            if (sort)
                loListado.Sort((x, y) => x.ctElemento.CompareTo(y.ctElemento));

            return loListado;
        }

        public static List<tbMaestroGruposModel> GetMaestroGrupoList(string tipo)
        {
            tbMaestroGruposBLL tbMaestroGrupoBLL = new tbMaestroGruposBLL();
            var loListado = tbMaestroGrupoBLL.GetByctTipoGrupo((tipo));

            return loListado;
        }

        public static List<SelectListItem> GetMaestroListItem(GruposListas grupo, bool addTodos = true, bool addOtros = false, string todosValue = "", string todosTexto = "", bool sort = true)
        {
            var loListado = GetMaestroList(grupo, sort);

            List<SelectListItem> loDocumentosList = new List<SelectListItem>();

            loDocumentosList.AddRange((from x in loListado select new SelectListItem() { Text = x.ctElemento, Value = x.cnID_Lista.ToString() }).ToList());

            if (addTodos)
                loDocumentosList.Insert(0, new SelectListItem() { Text = (todosTexto.Trim().Length == 0) ? _ComboSeleccionar : todosTexto, Value = todosValue });

            if (addOtros)
                loDocumentosList.Insert(1, new SelectListItem() { Text = _ComboOtros, Value = Otros.ToString() });

            return loDocumentosList;
        }

        public static List<SelectListItem> GetMaestroNivelListItem(int grupo, bool addTodos = true, bool addOtros = false, string todosValue = "", string todosTexto = "", bool sort = true)
        {
            var loListado = GetMaestroListInt(grupo, sort);

            List<SelectListItem> loDocumentosList = new List<SelectListItem>();

            loDocumentosList.AddRange((from x in loListado select new SelectListItem() { Text = x.ctElemento, Value = x.cnID_Lista.ToString() }).ToList());

            if (addTodos)
                loDocumentosList.Insert(0, new SelectListItem() { Text = (todosTexto.Trim().Length == 0) ? _ComboSeleccionar : todosTexto, Value = todosValue });

            if (addOtros)
                loDocumentosList.Insert(1, new SelectListItem() { Text = _ComboOtros, Value = Otros.ToString() });

            return loDocumentosList;
        }

        public static List<SelectListItem> GetMaestroList()
        {
            tbMaestroGruposBLL lotbMaestroGruposBLL = new tbMaestroGruposBLL();
            var list = lotbMaestroGruposBLL.GetAll();
            list = list.FindAll(x =>
            {
                return (x.cnID_GrupoLista == (int)GruposListas.TiposDocumento)
                    || (x.cnID_GrupoLista == (int)GruposListas.TipoEmail)
                    || (x.cnID_GrupoLista == (int)GruposListas.CategoriasDocente)
                    || (x.cnID_GrupoLista == (int)GruposListas.RegimenDedicacion)

                    || (x.cnID_GrupoLista == (int)GruposListas.TipoExperienciaInstitucion)
                    //o	Tipo de experiencia en institución
                    //|| (x.cnID_GrupoLista == (int)GruposListas.AreaTematicaNivel1)
                    //|| (x.cnID_GrupoLista == (int)GruposListas.AreaTematicaNivel2)
                    //|| (x.cnID_GrupoLista == (int)GruposListas.AreaTematicaNivel3)                    

                    || (x.cnID_GrupoLista == (int)GruposListas.AreaTematicaNivel)

                    || (x.cnID_GrupoLista == (int)GruposListas.TipoContrato)
                    || (x.cnID_GrupoLista == (int)GruposListas.Cargo)
                    || (x.cnID_GrupoLista == (int)GruposListas.Area)
                    || (x.cnID_GrupoLista == (int)GruposListas.Rubro)

                    || (x.cnID_GrupoLista == (int)GruposListas.Instituto)
                    || (x.cnID_GrupoLista == (int)GruposListas.Universidad)

                    || (x.cnID_GrupoLista == (int)GruposListas.Grado)

                    || (x.cnID_GrupoLista == (int)GruposListas.SituacionPHD)
                    || (x.cnID_GrupoLista == (int)GruposListas.SituacionDoctor)
                    || (x.cnID_GrupoLista == (int)GruposListas.SituacionMaster)
                    || (x.cnID_GrupoLista == (int)GruposListas.SituacionTitulo)
                    || (x.cnID_GrupoLista == (int)GruposListas.SituacionBachiller)


                    || (x.cnID_GrupoLista == (int)GruposListas.Carrera)
                    ;

            });

            List<SelectListItem> loDocumentosList = new List<SelectListItem>();
            loDocumentosList.AddRange((from x in list select new SelectListItem() { Text = x.ctNombreGrupo, Value = x.cnID_GrupoLista.ToString() }).ToList());

            loDocumentosList.Insert(0, new SelectListItem() { Text = _ComboSeleccionar, Value = "" });

            return loDocumentosList;
        }

        public static List<SelectListItem> GetTablasConsulta()
        {
            tbMaestroGruposBLL lotbMaestroGruposBLL = new tbMaestroGruposBLL();
            var list = lotbMaestroGruposBLL.GetAll();
            list = list.FindAll(x =>
            {
                return (x.cnID_GrupoLista == (int)GruposListas.TablaDatosPersonales)
                    || (x.cnID_GrupoLista == (int)GruposListas.TablaExperienciaLaboral)
                    || (x.cnID_GrupoLista == (int)GruposListas.TablaCarrera)
                    || (x.cnID_GrupoLista == (int)GruposListas.TablaIdioma)
                    || (x.cnID_GrupoLista == (int)GruposListas.TablaDocencia)
                    || (x.cnID_GrupoLista == (int)GruposListas.TablaAreaTematica)
                    ;

            });

            List<SelectListItem> loDocumentosList = new List<SelectListItem>();
            loDocumentosList.AddRange((from x in list select new SelectListItem() { Text = x.ctNombreGrupo, Value = x.cnID_GrupoLista.ToString() }).ToList());

            //loDocumentosList.Insert(0, new SelectListItem() { Text = _ComboSeleccionar, Value = "" });

            return loDocumentosList;
        }

        public static List<SelectListItem> GetSituacion(int idGrado)
        {
            List<tbMaestroListasModel> lotbMaestroListasModel = new List<tbMaestroListasModel>();

            switch (idGrado)
            {
                //case (int)BaseGlobal.Grado.Primaria:
                //case (int)BaseGlobal.Grado.Secundaria:
                //    lotbMaestroListasModel = BaseGlobal.GetMaestroList(BaseGlobal.GruposListas.SituacionEducativa, false);
                //    break;

                //case (int)BaseGlobal.Grado.Tecnico:
                //case (int)BaseGlobal.Grado.Doctorado:
                //case (int)BaseGlobal.Grado.Maestria:
                //    lotbMaestroListasModel = BaseGlobal.GetMaestroList(BaseGlobal.GruposListas.SituacionTecnica, false);
                //    break;

                //case (int)BaseGlobal.Grado.Universitario:
                //    lotbMaestroListasModel = BaseGlobal.GetMaestroList(BaseGlobal.GruposListas.SituacionUniversitaria, false);
                //    break;

                case (int)BaseGlobal.Grado.PHD:
                    lotbMaestroListasModel = BaseGlobal.GetMaestroList(BaseGlobal.GruposListas.SituacionPHD, false);
                    break;
                case (int)BaseGlobal.Grado.Doctor:
                    lotbMaestroListasModel = BaseGlobal.GetMaestroList(BaseGlobal.GruposListas.SituacionDoctor, false);
                    break;
                case (int)BaseGlobal.Grado.Maestro:
                    lotbMaestroListasModel = BaseGlobal.GetMaestroList(BaseGlobal.GruposListas.SituacionMaestro, false);
                    break;
                case (int)BaseGlobal.Grado.Master_Internacional:
                case (int)BaseGlobal.Grado.Master_Nacional:
                    lotbMaestroListasModel = BaseGlobal.GetMaestroList(BaseGlobal.GruposListas.SituacionMaestro, false);
                    break;
                case (int)BaseGlobal.Grado.Titulo_Profesional:
                    lotbMaestroListasModel = BaseGlobal.GetMaestroList(BaseGlobal.GruposListas.SituacionTitulo, false);
                    break;
                case (int)BaseGlobal.Grado.Bachiller:
                    lotbMaestroListasModel = BaseGlobal.GetMaestroList(BaseGlobal.GruposListas.SituacionBachiller, false);
                    break;
                default:
                    lotbMaestroListasModel = BaseGlobal.GetMaestroList(BaseGlobal.GruposListas.General, false);
                    break;
            }

            var loSituacion = lotbMaestroListasModel.Select(elem => new SelectListItem()
                {
                    Text = elem.ctElemento,
                    Value = elem.cnID_Lista.ToString()
                });

            return loSituacion.ToList();
        }

        public static List<ArchivoPostulante> GetPostulanteFiles(long tnID_Candidato, long tnID_Postulante)
        {
            List<ArchivoPostulante> files = new List<ArchivoPostulante>();
            tbAcademicoBLL lotbAcademicoBLL = new tbAcademicoBLL();
            tbComplementarioBLL lotbComplementarioBLL = new tbComplementarioBLL();
            tbIdiomaBLL lotbIdiomaBLL = new tbIdiomaBLL();
            tbExperienciaBLL lotbExperienciaBLL = new tbExperienciaBLL();
            tbPostulanteDocuBLL lotbPostulanteDocuBLL = new tbPostulanteDocuBLL();

            tbAcademicoBLL lotbAcademicoBLLMasivo = new tbAcademicoBLL();

            var tbAcademicoModelList = lotbAcademicoBLL.GetBycnID_Candidato(tnID_Candidato);

            var tbAcademicoMasivoModelList = lotbAcademicoBLL.GetBycnID_CandidatoMasivo(tnID_Candidato);

            var tbComplementarioModelList = lotbComplementarioBLL.GetBycnID_Candidato(tnID_Candidato);
            var tbIdiomaModelList = lotbIdiomaBLL.GetBycnID_Candidato(tnID_Candidato);
            var tbExperienciaModelList = lotbExperienciaBLL.GetBycnID_Candidato(tnID_Candidato);
            List<tbPostulanteDocuModel> tbtbPostulanteDocuList = new List<tbPostulanteDocuModel>();

            if (tnID_Postulante > 0)
                tbtbPostulanteDocuList = lotbPostulanteDocuBLL.GetBycnID_Postulante(tnID_Postulante);

            Func<string, string> fnAgregarCarpetaCert = (a) => { return a.Trim().Length == 0 ? string.Empty : BaseVars.CARPETA_CERTIFICADOS + "/" + a; };
            Func<string, string> fnAgregarCarpetaPost = (a) => { return a.Trim().Length == 0 ? string.Empty : BaseVars.CARPETA_POSTULACION + "/" + a; };

            if (tbAcademicoMasivoModelList != null)
            {
                tbAcademicoMasivoModelList.ForEach(acadmas =>
                {
                    if (acadmas.ctCertificado != null)
                        files.Add(new ArchivoPostulante(fnAgregarCarpetaCert(acadmas.ctCertificado), TipoArchivo.Academico, acadmas.cnID_Academico));
                });
            }

            if (tbAcademicoModelList != null)
            {
                tbAcademicoModelList.ForEach(acad =>
                {
                    if (acad.ctCertificado != null)
                        files.Add(new ArchivoPostulante(fnAgregarCarpetaCert(acad.ctCertificado), TipoArchivo.Academico, acad.cnID_Academico));
                });
            }

            if (tbComplementarioModelList != null)
            {
                tbComplementarioModelList.ForEach(comp =>
                {
                    if (comp.ctCertificado != null)
                        files.Add(new ArchivoPostulante(fnAgregarCarpetaCert(comp.ctCertificado), TipoArchivo.Complementario, comp.cnID_Complementario));
                });
            }

            if (tbIdiomaModelList != null)
            {
                tbIdiomaModelList.ForEach(idioma =>
                {
                    if (idioma.ctCertificado != null)
                        files.Add(new ArchivoPostulante(fnAgregarCarpetaCert(idioma.ctCertificado), TipoArchivo.Idiomas, idioma.cnID_Idioma));
                });
            }

            if (tbExperienciaModelList != null)
            {
                tbExperienciaModelList.ForEach(experiencia =>
                {
                    if (experiencia.ctCertificado != null)
                        files.Add(new ArchivoPostulante(fnAgregarCarpetaCert(experiencia.ctCertificado), TipoArchivo.Laboral, experiencia.cnID_Experiencia));
                });
            }

            if (tbtbPostulanteDocuList != null)
            {
                tbtbPostulanteDocuList.ForEach(archivo =>
                {
                    if (archivo.ctRutaArchivo != null)
                        files.Add(new ArchivoPostulante(fnAgregarCarpetaPost(archivo.ctRutaArchivo), TipoArchivo.Documentos, archivo.cnID_Documento));
                });
            }

            if (files != null)
            {
                files.RemoveAll(f => f.FileName.Trim().Length == 0);
                files.RemoveAll(f => !System.IO.File.Exists(f.FileName));
            }

            return files;
        }

        public static string GetParameter(Parametros p)
        {
            tbCnfgSistemaBLL lotbCnfgSistemaBLL = new tbCnfgSistemaBLL();
            tbCnfgSistemaModel tbCnfgSistema = lotbCnfgSistemaBLL.GetByctCodigo(p.ToString());

            if (tbCnfgSistema != null)
            {
                return tbCnfgSistema.ctValor;
            }

            return string.Empty;
        }

        public static int GetPuntajeAcademico(long cnID_Proceso, out long cnID_Academico)
        {
            cnID_Academico = 0;

            var listadoGradosMaestro = BaseGlobal.GetMaestroList(BaseGlobal.GruposListas.Grado);
            tbAcademicoBLL lotbAcademicoBLL = new tbAcademicoBLL();
            var tbAcademicoModelList = lotbAcademicoBLL.GetBycnID_Candidato(BaseGlobal.CandidatoActual.cnID_Candidato);

            tbProcesoCnfgAcadBLL lotbProcesoCnfgAcadBLL = new tbProcesoCnfgAcadBLL();
            var tbProcesoCnfgAcadModelList = lotbProcesoCnfgAcadBLL.GetBycnID_Proceso(cnID_Proceso);

            var loGradosPostulante = (from x in listadoGradosMaestro
                                      join y in tbAcademicoModelList on x.cnID_Lista equals y.cnID_Grado
                                      select new { x.cnID_Lista, x.cnOrden, y.cnID_Situacion, y.cnID_Academico }).ToList();

            loGradosPostulante.Sort((gp1, gp2) => gp1.cnOrden.Value.ToString().CompareTo(gp2.cnOrden.Value.ToString()));

            int lnPuntajeAcademico = 0;
            if (loGradosPostulante != null)
            {
                if (loGradosPostulante.Count > 0)
                {
                    var loGradoObtenido = tbProcesoCnfgAcadModelList.Find(x => x.cnID_Grado == loGradosPostulante.Last().cnID_Lista && x.cnID_Situacion == loGradosPostulante.Last().cnID_Situacion);
                    if (loGradoObtenido != null)
                    {
                        lnPuntajeAcademico = loGradoObtenido.cnPuntaje;
                        cnID_Academico = loGradosPostulante.Last().cnID_Academico;
                    }
                }
            }

            if (cnID_Academico == 0)
            {
                if (loGradosPostulante.Count > 0)
                    cnID_Academico = loGradosPostulante.First().cnID_Academico;
            }

            return lnPuntajeAcademico;
        }

        public static bool CumpleNivelAcademico(long cnID_Proceso)
        {
            bool tieneNivel = false;

            var listadoGradosMaestro = BaseGlobal.GetMaestroList(BaseGlobal.GruposListas.Grado);
            tbAcademicoBLL lotbAcademicoBLL = new tbAcademicoBLL();
            var tbAcademicoModelList = lotbAcademicoBLL.GetBycnID_Candidato(BaseGlobal.CandidatoActual.cnID_Candidato);

            tbProcesoCnfgAcadBLL lotbProcesoCnfgAcadBLL = new tbProcesoCnfgAcadBLL();
            var tbProcesoCnfgAcadModelList = lotbProcesoCnfgAcadBLL.GetBycnID_Proceso(cnID_Proceso);

            var loGradosPostulante = (from x in listadoGradosMaestro
                                      join y in tbAcademicoModelList on x.cnID_Lista equals y.cnID_Grado
                                      select new { x.cnID_Lista, x.cnOrden, y.cnID_Situacion }).ToList();

            loGradosPostulante.Sort((gp1, gp2) => gp1.cnOrden.Value.ToString().CompareTo(gp2.cnOrden.Value.ToString()));

            var loGradosProceso = (from x in listadoGradosMaestro
                                   join y in tbProcesoCnfgAcadModelList on x.cnID_Lista equals y.cnID_Grado
                                   select new { x.cnID_Lista, x.cnOrden, y.cnID_Situacion }).ToList();

            loGradosProceso.Sort((gp1, gp2) => gp1.cnOrden.Value.ToString().CompareTo(gp2.cnOrden.Value.ToString()));

            if (loGradosPostulante != null && loGradosProceso != null)
            {
                if (loGradosPostulante.Count > 0 && loGradosProceso.Count > 0)
                {
                    var llRetorno = loGradosProceso.Find(x => loGradosPostulante.Last().cnID_Lista == x.cnID_Lista && loGradosPostulante.Last().cnID_Situacion == x.cnID_Situacion) != null;

                    //Sólo si el grado del postulante es mayor al del proceso se debe retornar true
                    if (!llRetorno)
                    {
                        if (loGradosProceso.Find(x => loGradosPostulante.Find(y => y.cnID_Lista == x.cnID_Lista) != null) == null)
                            llRetorno = loGradosPostulante.Max(y => y.cnOrden) > loGradosProceso.Max(x => x.cnOrden);
                    }

                    return llRetorno;
                }
            }

            return tieneNivel;
        }

        public static int GetTiempoExperienciaLaboralGeneral(long cnID_Proceso)
        {
            tbExperienciaBLL lotbExperienciaBLL = new tbExperienciaBLL();
            var tbExperienciaModelList = lotbExperienciaBLL.GetBycnID_Candidato(BaseGlobal.CandidatoActual.cnID_Candidato);

            List<tbProcesoMatrizLaboModel> lotbProcesoMatrizLaboModelList = new List<tbProcesoMatrizLaboModel>();
            tbProcesoMatrizLaboBLL lotbProcesoMatrizLaboBLL = new tbProcesoMatrizLaboBLL();
            lotbProcesoMatrizLaboModelList = lotbProcesoMatrizLaboBLL.GetBycnID_Proceso(cnID_Proceso);

            tbProcesoCnfgLaboBLL lotbProcesoCnfgLaboBLL = new tbProcesoCnfgLaboBLL();
            var tbProcesoCnfgLaboList = lotbProcesoCnfgLaboBLL.GetBycnID_Proceso(cnID_Proceso);

            int lnTiempoExperienciaGeneral = 0;

            long lnTipoEntidad = tbProcesoCnfgLaboList.First().cnID_TipoEntidad;
            var loExperienciaEntidad = tbExperienciaModelList.FindAll(x => BaseGlobal.GetTipoEntidadProceso(x.cnID_TipoEntidad) == lnTipoEntidad || lnTipoEntidad == BaseGlobal.TipoEntidadPrivPub);

            if (loExperienciaEntidad != null)
            {
                if (loExperienciaEntidad.Count > 0)
                {
                    loExperienciaEntidad.RemoveAll(x => !x.cnTiempoAnios.HasValue);
                    lnTiempoExperienciaGeneral = loExperienciaEntidad.Sum(x => x.cnTiempoAnios.Value);
                }
            }

            return lnTiempoExperienciaGeneral;
        }

        public static bool CumpleNivelLaboralGeneral(long cnID_Proceso)
        {
            List<tbProcesoMatrizLaboModel> lotbProcesoMatrizLaboModelList = new List<tbProcesoMatrizLaboModel>();
            tbProcesoMatrizLaboBLL lotbProcesoMatrizLaboBLL = new tbProcesoMatrizLaboBLL();
            lotbProcesoMatrizLaboModelList = lotbProcesoMatrizLaboBLL.GetBycnID_Proceso(cnID_Proceso);

            int lnTiempoExperienciaGeneral = BaseGlobal.GetTiempoExperienciaLaboralGeneral(cnID_Proceso);
            int lnTiempoMinimoGeneral = lotbProcesoMatrizLaboModelList.Min(x => x.cnMininmo);

            return lnTiempoExperienciaGeneral >= lnTiempoMinimoGeneral;
        }

        public static int GetPuntajeLaboralGeneral(long cnID_Proceso)
        {
            int lnPuntaje = 0;
            if (!BaseGlobal.CumpleNivelLaboralGeneral(cnID_Proceso))
                return 0;

            List<tbProcesoMatrizLaboModel> lotbProcesoMatrizLaboModelList = new List<tbProcesoMatrizLaboModel>();
            tbProcesoMatrizLaboBLL lotbProcesoMatrizLaboBLL = new tbProcesoMatrizLaboBLL();
            lotbProcesoMatrizLaboModelList = lotbProcesoMatrizLaboBLL.GetBycnID_Proceso(cnID_Proceso);

            int lnTiempoLaboralGeneral = BaseGlobal.GetTiempoExperienciaLaboralGeneral(cnID_Proceso);
            var _matriz = lotbProcesoMatrizLaboModelList.Find(x => (lnTiempoLaboralGeneral >= x.cnMininmo && lnTiempoLaboralGeneral <= x.cnMaximo) || (lnTiempoLaboralGeneral >= x.cnMininmo && x.cnMaximo == 0));

            if (_matriz != null)
                lnPuntaje = _matriz.cnPuntaje;

            return lnPuntaje;
        }

        public static int GetPuntajeLaboral(long cnID_Proceso, List<string> criterioLaboralesEspecificoTmp, out List<object[]> loExperienciaLaboral)
        {
            tbExperienciaBLL lotbExperienciaBLL = new tbExperienciaBLL();
            var tbExperienciaModelList = lotbExperienciaBLL.GetBycnID_Candidato(BaseGlobal.CandidatoActual.cnID_Candidato);

            List<tbProcesoMatrizLaboModel> lotbProcesoMatrizLaboModelList = new List<tbProcesoMatrizLaboModel>();
            tbProcesoMatrizLaboBLL lotbProcesoMatrizLaboBLL = new tbProcesoMatrizLaboBLL();
            lotbProcesoMatrizLaboModelList = lotbProcesoMatrizLaboBLL.GetBycnID_Proceso(cnID_Proceso);

            tbProcesoCnfgLaboBLL lotbProcesoCnfgLaboBLL = new tbProcesoCnfgLaboBLL();
            var tbProcesoCnfgLaboList = lotbProcesoCnfgLaboBLL.GetBycnID_Proceso(cnID_Proceso);

            int lnPuntajeLaboral = 0;
            loExperienciaLaboral = new List<object[]>();

            var loExperienciaLaboralTmp = new List<object[]>();

            lotbProcesoMatrizLaboModelList.ForEach(x =>
            {
                if (tbProcesoCnfgLaboList != null)
                {
                    var loConfGeneral = tbProcesoCnfgLaboList.Find(f => f.cnID_TipoCriterioLabo == BaseGlobal.LaboralGeneral);
                    var loConfEsp = tbProcesoCnfgLaboList.Find(f => f.cnID_TipoCriterioLabo == BaseGlobal.LaboralEspecifico);

                    if (loConfGeneral != null)
                    {
                        if (x.cnID_CnfgLaboral == loConfGeneral.cnID_CnfgLaboral)
                        {
                            tbExperienciaModelList.ForEach(y =>
                            {
                                if (BaseGlobal.GetTipoEntidadProceso(y.cnID_TipoEntidad) == loConfGeneral.cnID_TipoEntidad || loConfGeneral.cnID_TipoEntidad == BaseGlobal.TipoEntidadPrivPub)
                                {
                                    if ((y.cnTiempoAnios >= x.cnMininmo && y.cnTiempoAnios <= x.cnMaximo) || (y.cnTiempoAnios >= x.cnMininmo && x.cnMaximo == 0))
                                    {
                                        loExperienciaLaboralTmp.Add(new object[] { y.cnID_Experiencia, y.cnTiempoAnios, loConfGeneral.cnID_TipoCriterioLabo });
                                    }
                                }
                            });
                        }
                    }

                    if (loConfEsp != null)
                    {
                        if (x.cnID_CnfgLaboral == loConfEsp.cnID_CnfgLaboral)
                        {
                            if (criterioLaboralesEspecificoTmp != null)
                            {
                                var loExperienciaEspecifica = tbExperienciaModelList.FindAll(a => criterioLaboralesEspecificoTmp.Find(_b => int.Parse(_b) == a.cnID_Experiencia) != null);
                                loExperienciaEspecifica.ForEach(y =>
                                {
                                    if (BaseGlobal.GetTipoEntidadProceso(y.cnID_TipoEntidad) == loConfGeneral.cnID_TipoEntidad || loConfGeneral.cnID_TipoEntidad == BaseGlobal.TipoEntidadPrivPub)
                                    {
                                        if ((y.cnTiempoAnios >= x.cnMininmo && y.cnTiempoAnios <= x.cnMaximo) || (y.cnTiempoAnios >= x.cnMininmo && x.cnMaximo == 0))
                                        {
                                            lnPuntajeLaboral += x.cnPuntaje;
                                            loExperienciaLaboralTmp.Add(new object[] { y.cnID_Experiencia, y.cnTiempoAnios, loConfEsp.cnID_TipoCriterioLabo });
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            });

            lnPuntajeLaboral += BaseGlobal.GetPuntajeLaboralGeneral(cnID_Proceso);

            loExperienciaLaboral = loExperienciaLaboralTmp;

            return lnPuntajeLaboral;
        }

        public static bool CumpleNivelLaboralEspecif(long cnID_Proceso, int[] cnID_Experiencia)
        {
            tbExperienciaBLL lotbExperienciaBLL = new tbExperienciaBLL();
            var tbExperienciaModelList = lotbExperienciaBLL.GetBycnID_Candidato(BaseGlobal.CandidatoActual.cnID_Candidato);

            List<tbProcesoMatrizLaboModel> lotbProcesoMatrizLaboModelList = new List<tbProcesoMatrizLaboModel>();
            tbProcesoMatrizLaboBLL lotbProcesoMatrizLaboBLL = new tbProcesoMatrizLaboBLL();
            lotbProcesoMatrizLaboModelList = lotbProcesoMatrizLaboBLL.GetBycnID_Proceso(cnID_Proceso);

            tbProcesoCnfgLaboBLL lotbProcesoCnfgLaboBLL = new tbProcesoCnfgLaboBLL();
            var tbProcesoCnfgLaboList = lotbProcesoCnfgLaboBLL.GetBycnID_Proceso(cnID_Proceso);

            int lnPuntajeLaboral = 0;

            lotbProcesoMatrizLaboModelList.ForEach(x =>
            {
                if (tbProcesoCnfgLaboList != null)
                {
                    var loConfEsp = tbProcesoCnfgLaboList.Find(f => f.cnID_TipoCriterioLabo == BaseGlobal.LaboralEspecifico);

                    if (loConfEsp != null)
                    {
                        if (x.cnID_CnfgLaboral == loConfEsp.cnID_CnfgLaboral)
                        {
                            if (cnID_Experiencia != null)
                            {
                                var loExperienciaEspecifica = tbExperienciaModelList.FindAll(a => cnID_Experiencia.ToList().Find(_b => _b == a.cnID_Experiencia) != 0);
                                loExperienciaEspecifica.ForEach(y =>
                                {
                                    if (BaseGlobal.GetTipoEntidadProceso(y.cnID_TipoEntidad) == loConfEsp.cnID_TipoEntidad || loConfEsp.cnID_TipoEntidad == BaseGlobal.TipoEntidadPrivPub)
                                    {
                                        if ((y.cnTiempoAnios >= x.cnMininmo && y.cnTiempoAnios <= x.cnMaximo) || (y.cnTiempoAnios >= x.cnMininmo && x.cnMaximo == 0))
                                        {
                                            lnPuntajeLaboral += x.cnPuntaje;
                                        }
                                    }
                                });
                            }
                        }
                    }
                }
            });

            return lnPuntajeLaboral > 0;
        }

        public static byte[] GetHojaVida(long? cnID_Postulante = null, long? cnID_Candidato = null, bool tlEliminar = true, bool? tlDocumentado = true, string reportType = "PDF")
        {
            long lnID_Candidato = 0;
            tbProcesoModel loProceso = new tbProcesoModel();
            if (BaseGlobal.CandidatoActual != null)
                lnID_Candidato = BaseGlobal.CandidatoActual.cnID_Candidato;
            else
                lnID_Candidato = cnID_Candidato.Value;

            var localReport = new LocalReport { ReportPath = HttpContext.Current.Server.MapPath("~/Reports/rpthojaVida.rdlc") };
            localReport.SetParameters(new ReportParameter("cnID_Candidato", lnID_Candidato.ToString()));
            if (cnID_Postulante.HasValue)
            {
                localReport.SetParameters(new ReportParameter("cnID_Postulante", cnID_Postulante.Value.ToString()));

                tbPostulanteBLL lotbPostulanteBLL = new tbPostulanteBLL();
                tbProcesoBLL lotbProcesoBLL = new tbProcesoBLL();
                var loPostulante = lotbPostulanteBLL.GetBycnID_Postulante(cnID_Postulante.Value);
                loProceso = lotbProcesoBLL.GetBycnID_Proceso(loPostulante.cnID_Proceso);

                localReport.SetParameters(new ReportParameter("ctProcesoTitulo", loProceso.ctTitulo.ToUpper()));
            }

            if (cnID_Postulante.HasValue)
            {
                tbPostulanteBLL lotbPostulanteBLL = new tbPostulanteBLL();
                tbPostulanteModel lotbPostulanteModel = lotbPostulanteBLL.GetBycnID_Postulante(cnID_Postulante.Value);
                localReport.SetParameters(new ReportParameter("cnCalifica", lotbPostulanteModel.cnEvalAuto.ToString()));
            }

            localReport.SubreportProcessing += (e, b) =>
            {
                if (b.ReportPath == "rptDatosPesonales")
                {
                    dsDatosPersonalesTableAdapters.tbCandidato_GetHojaVidaDatosPersonalesTableAdapter lotbCandidato_GetHojaVidaDatosPersonalesTableAdapter = new dsDatosPersonalesTableAdapters.tbCandidato_GetHojaVidaDatosPersonalesTableAdapter();
                    dsDatosPersonales.tbCandidato_GetHojaVidaDatosPersonalesDataTable dttbCandidato_GetHojaVidaDatosPersonalesDataTable = new dsDatosPersonales.tbCandidato_GetHojaVidaDatosPersonalesDataTable();
                    lotbCandidato_GetHojaVidaDatosPersonalesTableAdapter.Fill(dttbCandidato_GetHojaVidaDatosPersonalesDataTable, Convert.ToInt32(lnID_Candidato));
                    ReportDataSource rdstbCandidato_GetHojaVidaDatosPersonalesDataTable = new ReportDataSource();
                    rdstbCandidato_GetHojaVidaDatosPersonalesDataTable.Name = "dstDatosPersonales";
                    rdstbCandidato_GetHojaVidaDatosPersonalesDataTable.Value = dttbCandidato_GetHojaVidaDatosPersonalesDataTable;
                    b.DataSources.Add(rdstbCandidato_GetHojaVidaDatosPersonalesDataTable);
                }

                if (b.ReportPath == "rptFormacionAcademica")
                {
                    dsDatosAcademicosTableAdapters.tbCandidato_GetHojaVidaAcademicoTableAdapter lotbCandidato_GetHojaVidaAcademicoTableAdapter = new dsDatosAcademicosTableAdapters.tbCandidato_GetHojaVidaAcademicoTableAdapter();
                    dsDatosAcademicos.tbCandidato_GetHojaVidaAcademicoDataTable lotbCandidato_GetHojaVidaAcademicoDataTable = new dsDatosAcademicos.tbCandidato_GetHojaVidaAcademicoDataTable();
                    lotbCandidato_GetHojaVidaAcademicoTableAdapter.Fill(lotbCandidato_GetHojaVidaAcademicoDataTable, Convert.ToInt32(lnID_Candidato));
                    ReportDataSource rdstbCandidato_GetHojaVidaAcademicoDataTable = new ReportDataSource();
                    rdstbCandidato_GetHojaVidaAcademicoDataTable.Name = "dstDatosAcademicos";
                    rdstbCandidato_GetHojaVidaAcademicoDataTable.Value = lotbCandidato_GetHojaVidaAcademicoDataTable;
                    b.DataSources.Add(rdstbCandidato_GetHojaVidaAcademicoDataTable);
                }

                if (b.ReportPath == "rptEstudiosComplementarios")
                {
                    dsDatosComplementariosTableAdapters.tbCandidato_GetHojaVidaComplementarioTableAdapter lotbCandidato_GetHojaVidaComplementarioTableAdapter = new dsDatosComplementariosTableAdapters.tbCandidato_GetHojaVidaComplementarioTableAdapter();
                    dsDatosComplementarios.tbCandidato_GetHojaVidaComplementarioDataTable lotbCandidato_GetHojaVidaComplementarioDataTable = new dsDatosComplementarios.tbCandidato_GetHojaVidaComplementarioDataTable();
                    lotbCandidato_GetHojaVidaComplementarioTableAdapter.Fill(lotbCandidato_GetHojaVidaComplementarioDataTable, Convert.ToInt32(lnID_Candidato));
                    ReportDataSource rdstbCandidato_GetHojaVidaComplementarioDataTable = new ReportDataSource();
                    rdstbCandidato_GetHojaVidaComplementarioDataTable.Name = "dstDatosComplementarios";
                    rdstbCandidato_GetHojaVidaComplementarioDataTable.Value = lotbCandidato_GetHojaVidaComplementarioDataTable;
                    b.DataSources.Add(rdstbCandidato_GetHojaVidaComplementarioDataTable);
                }
                if (b.ReportPath == "rptIdiomas")
                {
                    dsIdiomasTableAdapters.tbCandidato_GetHojaVidaIdiomaTableAdapter lotbCandidato_GetHojaVidaIdiomaTableAdapter = new dsIdiomasTableAdapters.tbCandidato_GetHojaVidaIdiomaTableAdapter();
                    dsIdiomas.tbCandidato_GetHojaVidaIdiomaDataTable lotbCandidato_GetHojaVidaIdiomaDataTable = new dsIdiomas.tbCandidato_GetHojaVidaIdiomaDataTable();

                    lotbCandidato_GetHojaVidaIdiomaTableAdapter.Fill(lotbCandidato_GetHojaVidaIdiomaDataTable, Convert.ToInt32(lnID_Candidato));
                    ReportDataSource rdslotbCandidato_GetHojaVidaIdiomaDataTable = new ReportDataSource();
                    rdslotbCandidato_GetHojaVidaIdiomaDataTable.Name = "dstIdiomas";
                    rdslotbCandidato_GetHojaVidaIdiomaDataTable.Value = lotbCandidato_GetHojaVidaIdiomaDataTable;
                    b.DataSources.Add(rdslotbCandidato_GetHojaVidaIdiomaDataTable);
                }

                if (b.ReportPath == "rptExperienciaLaboral")
                {
                    dsExperienciaLaboralTableAdapters.tbCandidato_GetHojaVidaExperienciaTableAdapter lotbCandidato_GetHojaVidaExperienciaTableAdapter = new dsExperienciaLaboralTableAdapters.tbCandidato_GetHojaVidaExperienciaTableAdapter();
                    dsExperienciaLaboral.tbCandidato_GetHojaVidaExperienciaDataTable lotbCandidato_GetHojaVidaExperienciaDataTable = new dsExperienciaLaboral.tbCandidato_GetHojaVidaExperienciaDataTable();

                    lotbCandidato_GetHojaVidaExperienciaTableAdapter.Fill(lotbCandidato_GetHojaVidaExperienciaDataTable, Convert.ToInt32(lnID_Candidato));
                    ReportDataSource rdslotbCandidato_GetHojaVidaExperienciaDataTable = new ReportDataSource();
                    rdslotbCandidato_GetHojaVidaExperienciaDataTable.Name = "dstExperienciaLaboral";
                    rdslotbCandidato_GetHojaVidaExperienciaDataTable.Value = lotbCandidato_GetHojaVidaExperienciaDataTable;
                    b.DataSources.Add(rdslotbCandidato_GetHojaVidaExperienciaDataTable);
                }

                if (b.ReportPath == "rptReferencias")
                {
                    dsReferenciasTableAdapters.tbCandidato_GetHojaVidaReferenciaTableAdapter lotbCandidato_GetHojaVidaReferenciaTableAdapter = new dsReferenciasTableAdapters.tbCandidato_GetHojaVidaReferenciaTableAdapter();
                    dsReferencias.tbCandidato_GetHojaVidaReferenciaDataTable lotbCandidato_GetHojaVidaReferenciaDataTable = new dsReferencias.tbCandidato_GetHojaVidaReferenciaDataTable();

                    lotbCandidato_GetHojaVidaReferenciaTableAdapter.Fill(lotbCandidato_GetHojaVidaReferenciaDataTable, Convert.ToInt32(lnID_Candidato));
                    ReportDataSource rdslotbCandidato_GetHojaVidaReferenciaDataTable = new ReportDataSource();
                    rdslotbCandidato_GetHojaVidaReferenciaDataTable.Name = "dstReferencias";
                    rdslotbCandidato_GetHojaVidaReferenciaDataTable.Value = lotbCandidato_GetHojaVidaReferenciaDataTable;
                    b.DataSources.Add(rdslotbCandidato_GetHojaVidaReferenciaDataTable);
                }

                if (cnID_Postulante.HasValue)
                {
                    if (b.ReportPath == "rptPostulanteDatosPersonales")
                    {
                        tbPostulante_GetHojaVidaDatosPersonalesTableAdapter lotbPostulante_GetHojaVidaDatosPersonalesTableAdapter = new tbPostulante_GetHojaVidaDatosPersonalesTableAdapter();
                        mvcSeleccion.dsPostulanteDatosPersonales.tbPostulante_GetHojaVidaDatosPersonalesDataTable lotbPostulante_GetHojaVidaDatosPersonalesDataTable = new mvcSeleccion.dsPostulanteDatosPersonales.tbPostulante_GetHojaVidaDatosPersonalesDataTable();

                        lotbPostulante_GetHojaVidaDatosPersonalesTableAdapter.Fill(lotbPostulante_GetHojaVidaDatosPersonalesDataTable, cnID_Postulante);
                        ReportDataSource rslotbPostulante_GetHojaVidaDatosPersonalesDataTable = new ReportDataSource();
                        rslotbPostulante_GetHojaVidaDatosPersonalesDataTable.Name = "dstPostulanteDatosPersonales";
                        rslotbPostulante_GetHojaVidaDatosPersonalesDataTable.Value = lotbPostulante_GetHojaVidaDatosPersonalesDataTable;
                        b.DataSources.Add(rslotbPostulante_GetHojaVidaDatosPersonalesDataTable);
                    }

                    if (b.ReportPath == "rptPostulanteAcademico")
                    {
                        tbPostulante_GetHojaVidaAcademicoTableAdapter lotbPostulante_GetHojaVidaAcademicoTableAdapter = new tbPostulante_GetHojaVidaAcademicoTableAdapter();
                        mvcSeleccion.dsPostulanteAcademico.tbPostulante_GetHojaVidaAcademicoDataTable lotbPostulante_GetHojaVidaAcademicoDataTable = new dsPostulanteAcademico.tbPostulante_GetHojaVidaAcademicoDataTable();

                        lotbPostulante_GetHojaVidaAcademicoTableAdapter.Fill(lotbPostulante_GetHojaVidaAcademicoDataTable, cnID_Postulante);
                        ReportDataSource rslotbPostulante_GetHojaVidaAcademicoDataTable = new ReportDataSource();
                        rslotbPostulante_GetHojaVidaAcademicoDataTable.Name = "dstPostulanteAcademico";
                        rslotbPostulante_GetHojaVidaAcademicoDataTable.Value = lotbPostulante_GetHojaVidaAcademicoDataTable;
                        b.DataSources.Add(rslotbPostulante_GetHojaVidaAcademicoDataTable);
                    }

                    if (b.ReportPath == "rptPostulanteCobertura")
                    {
                        tbPostulante_GetHojaVidaCoberturaTableAdapter lotbPostulante_GetHojaVidaCoberturaTableAdapter = new tbPostulante_GetHojaVidaCoberturaTableAdapter();
                        mvcSeleccion.dsPostulanteCobertura.tbPostulante_GetHojaVidaCoberturaDataTable lotbPostulante_GetHojaVidaCoberturaDataTable = new dsPostulanteCobertura.tbPostulante_GetHojaVidaCoberturaDataTable();

                        lotbPostulante_GetHojaVidaCoberturaTableAdapter.Fill(lotbPostulante_GetHojaVidaCoberturaDataTable, cnID_Postulante);
                        ReportDataSource dslotbPostulante_GetHojaVidaCoberturaDataTable = new ReportDataSource();
                        dslotbPostulante_GetHojaVidaCoberturaDataTable.Name = "dstPostulanteCobertura";
                        dslotbPostulante_GetHojaVidaCoberturaDataTable.Value = lotbPostulante_GetHojaVidaCoberturaDataTable;
                        b.DataSources.Add(dslotbPostulante_GetHojaVidaCoberturaDataTable);
                    }

                    if (b.ReportPath == "rptPostulanteLaboral")
                    {
                        tbPostulante_GetHojaVidaLaboralTableAdapter lotbPostulante_GetHojaVidaLaboralTableAdapter = new tbPostulante_GetHojaVidaLaboralTableAdapter();
                        mvcSeleccion.dsPostulanteLaboral.tbPostulante_GetHojaVidaLaboralDataTable lotbPostulante_GetHojaVidaLaboralDataTable = new mvcSeleccion.dsPostulanteLaboral.tbPostulante_GetHojaVidaLaboralDataTable();

                        lotbPostulante_GetHojaVidaLaboralTableAdapter.Fill(lotbPostulante_GetHojaVidaLaboralDataTable, cnID_Postulante);
                        ReportDataSource dslotbPostulante_GetHojaVidaLaboralDataTable = new ReportDataSource();
                        dslotbPostulante_GetHojaVidaLaboralDataTable.Name = "dstPostulanteLaboral";
                        dslotbPostulante_GetHojaVidaLaboralDataTable.Value = lotbPostulante_GetHojaVidaLaboralDataTable;
                        b.DataSources.Add(dslotbPostulante_GetHojaVidaLaboralDataTable);
                    }

                    if (b.ReportPath == "rptPostulanteLaboralEspecif")
                    {
                        tbPostulante_GetHojaVidaLaboralEspecifTableAdapter lotbPostulante_GetHojaVidaLaboralEspecifTableAdapter = new tbPostulante_GetHojaVidaLaboralEspecifTableAdapter();
                        mvcSeleccion.dsPostulanteLaboralEspecif.tbPostulante_GetHojaVidaLaboralEspecifDataTable lotbPostulante_GetHojaVidaLaboralEspecifDataTable = new dsPostulanteLaboralEspecif.tbPostulante_GetHojaVidaLaboralEspecifDataTable();

                        lotbPostulante_GetHojaVidaLaboralEspecifTableAdapter.Fill(lotbPostulante_GetHojaVidaLaboralEspecifDataTable, cnID_Postulante);
                        ReportDataSource dslotbPostulante_GetHojaVidaLaboralEspecifDataTable = new ReportDataSource();
                        dslotbPostulante_GetHojaVidaLaboralEspecifDataTable.Name = "dstPostulanteLaboralEspecif";
                        dslotbPostulante_GetHojaVidaLaboralEspecifDataTable.Value = lotbPostulante_GetHojaVidaLaboralEspecifDataTable;
                        b.DataSources.Add(dslotbPostulante_GetHojaVidaLaboralEspecifDataTable);
                    }

                    if (b.ReportPath == "rptPostulanteCritAdicional")
                    {
                        tbPostulante_GetHojaVidaAdicionalTableAdapter lotbPostulante_GetHojaVidaAdicionalTableAdapter = new tbPostulante_GetHojaVidaAdicionalTableAdapter();
                        mvcSeleccion.dsPostulanteCritAdicional.tbPostulante_GetHojaVidaAdicionalDataTable lotbPostulante_GetHojaVidaAdicionalDataTable = new dsPostulanteCritAdicional.tbPostulante_GetHojaVidaAdicionalDataTable();

                        lotbPostulante_GetHojaVidaAdicionalTableAdapter.Fill(lotbPostulante_GetHojaVidaAdicionalDataTable, cnID_Postulante);
                        ReportDataSource dslotbPostulante_GetHojaVidaAdicionalDataTable = new ReportDataSource();
                        dslotbPostulante_GetHojaVidaAdicionalDataTable.Name = "dstPostulanteCritAdicional";
                        dslotbPostulante_GetHojaVidaAdicionalDataTable.Value = lotbPostulante_GetHojaVidaAdicionalDataTable;
                        b.DataSources.Add(dslotbPostulante_GetHojaVidaAdicionalDataTable);
                    }

                    if (b.ReportPath == "rptEvaluacionManual")
                    {
                        tbPostulante_GetEvaluacionManualTableAdapter lotbPostulante_GetEvaluacionManualTableAdapter = new tbPostulante_GetEvaluacionManualTableAdapter();
                        mvcSeleccion.dsEvaluacionManual.tbPostulante_GetEvaluacionManualDataTable lotbPostulante_GetEvaluacionManualDataTable = new dsEvaluacionManual.tbPostulante_GetEvaluacionManualDataTable();

                        lotbPostulante_GetEvaluacionManualTableAdapter.Fill(lotbPostulante_GetEvaluacionManualDataTable, cnID_Postulante);
                        ReportDataSource dslotbPostulante_GetEvaluacionManuallDataTable = new ReportDataSource();
                        dslotbPostulante_GetEvaluacionManuallDataTable.Name = "dstEvaluacionManual";
                        dslotbPostulante_GetEvaluacionManuallDataTable.Value = lotbPostulante_GetEvaluacionManualDataTable;
                        b.DataSources.Add(dslotbPostulante_GetEvaluacionManuallDataTable);
                    }
                }
            };
            //Give the collection a name (EmployeeCollection) so that we can reference it in our report designer
            //var reportDataSource = new ReportDataSource("NiceData", Model);
            //localReport.DataSources.Add(reportDataSource);

            //var reportType = _reportType;
            string mimeType;
            string encoding;
            string fileNameExtension;

            //The DeviceInfo settings should be changed based on the reportType
            //http://msdn2.microsoft.com/en-us/library/ms155397.aspx
            var deviceInfo =
                string.Format(@"<DeviceInfo><OutputFormat>{0}</OutputFormat>
                                    <PageWidth>8.5in</PageWidth>
                                    <PageHeight>11in</PageHeight>
                                    <MarginTop>0in</MarginTop>
                                    <MarginLeft>0.5in</MarginLeft>
                                    <MarginRight>0in</MarginRight>
                                    <MarginBottom>0in</MarginBottom>
                                    <HumanReadablePDF>True</HumanReadablePDF>
                                </DeviceInfo>", reportType);

            Warning[] warnings;
            string[] streams;

            byte[] _bytesReturn = null;
            //Render the report
            byte[] renderedBytes = null;
            if (reportType == "PDF")
            {
                renderedBytes = localReport.Render(
                  reportType,
                  deviceInfo,
                  out mimeType,
                  out encoding,
                  out fileNameExtension,
                  out streams,
                  out warnings);
            }
            else
            {
                renderedBytes = localReport.Render(reportType);
            }

            _bytesReturn = renderedBytes;

            //if (cnID_Postulante.HasValue)
            if (tlDocumentado != null)
            {
                if (tlDocumentado.Value)
                {
                    string fileName = string.Empty;
                    fileName = string.Format("{0}\\tmp_cv_{1}.pdf", BaseVars.CARPETA_TEMP, lnID_Candidato);
                    System.IO.File.WriteAllBytes(fileName, renderedBytes);

                    //Unir con archivos de postulante

                    var files = BaseGlobal.GetPostulanteFiles(lnID_Candidato, (cnID_Postulante.HasValue) ? cnID_Postulante.Value : 0);
                    if (files != null)
                    {
                        files.Insert(0, new ArchivoPostulante(fileName, TipoArchivo.General, 0));

                        List<byte[]> loFiles = new List<byte[]>();

                        foreach (ArchivoPostulante file in files)
                        {
                            loFiles.Add(File.ReadAllBytes(file.FileName));
                        }

                        MemoryStream stream = BaseUtil.MergePdfForms(loFiles);
                        if (tlEliminar)
                            BaseUtil.DeleteFile(string.Empty, fileName);

                        _bytesReturn = stream.ToArray();
                    }
                }
            }

            return _bytesReturn;
        }
    }
}