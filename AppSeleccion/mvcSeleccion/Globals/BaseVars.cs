﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mvcSeleccion
{
    public class BaseVars
    {

        

        public static string CARPETA_CERTIFICADOS
        {
            get
            {
                return HttpContext.Current.Server.MapPath("~/Uploads/Certificados");
            }
        }

        public static string CARPETA_PROCESOS
        {
            get
            {
                return HttpContext.Current.Server.MapPath("~/Uploads/Procesos");
            }
        }

        public static string CARPETA_POSTULACION
        {
            get
            {
                return HttpContext.Current.Server.MapPath("~/Uploads/Postulacion");
            }
        }

        public static string CARPETA_TEMP
        {
            get
            {
                return HttpContext.Current.Server.MapPath("~/Uploads/Temp");
            }
        }

        public static string CARPETA_HOJA_VIDA_POSTULANTES
        {
            get
            {
                return HttpContext.Current.Server.MapPath("~/Uploads/HojaVidaPostulantes");
            }
        }

        public static string CARPETA_PERFIL
        {
            get
            {
                return HttpContext.Current.Server.MapPath("~/Uploads/Perfil");
            }
        }
    }
}