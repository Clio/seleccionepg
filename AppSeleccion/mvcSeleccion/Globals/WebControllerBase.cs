﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mvcSeleccion
{
    public class WebControllerBase : Controller
    {
        public ActionResult RedirectToLogin()
        {
            return RedirectToAction("Login", "Auth");
        }
    }
}