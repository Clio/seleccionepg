﻿using SalarDb.CodeGen.BLL;
using SalarDb.CodeGen.Model;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace mvcSeleccion.Controllers
{
    public class FormacionController : WebControllerBase
    {
        public ActionResult Academica(int? id, string a = null, int? i = null)
        {
            if (!BaseGlobal.IsLogged)
                return this.RedirectToLogin();

            long cnID_Candidato = BaseGlobal.CandidatoActual.cnID_Candidato;
            ViewBag.Grados = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.Grado, sort: false);
            ViewBag.Carreras = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.Carrera, addOtros: true);

            var instituciones = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.Instituto);
            instituciones.AddRange(BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.Universidad));
            instituciones.Sort((x, y) => x.Text.CompareTo(y.Text));
            instituciones.RemoveAll(x => x.Value.Trim() == string.Empty && x.Text == BaseGlobal._ComboSeleccionar);
            instituciones.Insert(0, new SelectListItem() { Text = BaseGlobal._ComboSeleccionar, Value = string.Empty });
            instituciones.Insert(1, new SelectListItem() { Text = BaseGlobal._ComboOtros, Value = BaseGlobal.Otros.ToString() });
            ViewBag.Instituciones = instituciones;

            ViewBag.Paises = BaseGlobal.GetPaisListItem();

            tbAcademicoBLL lotbAcademicoBLL = new tbAcademicoBLL();
            tbAcademicoModel lotbAcademicoModel = new tbAcademicoModel();
            lotbAcademicoModel.cnID_Candidato = cnID_Candidato;
            lotbAcademicoModel.cfFechaInicio = DateTime.Now;
            lotbAcademicoModel.cfFechaFin = DateTime.Now;
            lotbAcademicoModel.tbAcademicoModelList = new List<tbAcademicoModel>();

            if (id.HasValue)
            {
                if (id.Value > 0)
                {
                    lotbAcademicoModel = lotbAcademicoBLL.GetBycnID_Academico(id.Value);
                    ViewBag.Situaciones = BaseGlobal.GetSituacion(Convert.ToInt32(lotbAcademicoModel.cnID_Grado));
                }
            }

            //--------------------
            ViewBag.action = a;
            ViewBag.id = i;

            int IdCandidato = Convert.ToInt32(BaseGlobal.CandidatoActual.cnID_Candidato);
            ViewBag.document = lotbAcademicoBLL.ListDoceumnts(IdCandidato, 1);
            //--------------------

            lotbAcademicoModel.tbAcademicoModelList = lotbAcademicoBLL.GetBycnID_Candidato(cnID_Candidato);

            return View(lotbAcademicoModel);
        }

        [HttpPost]
        public ActionResult SaveAcademica(tbAcademicoModel model, HttpPostedFileBase txtCertificado)
        {
            try
            {
                string fileName = string.Empty;
                string oldFile = model.ctCertificado;
                bool isnewFile = false;
                //if (txtCertificado != null)
                //{
                    if (BaseUtil.SaveFile(txtCertificado, BaseVars.CARPETA_CERTIFICADOS, out fileName))
                    {
                        model.ctCertificado = fileName;
                        isnewFile = true;
                    }
                //}

                tbAcademicoBLL lotbAcademicoBLL = new tbAcademicoBLL();

                model.cfFechaCreacion = DateTime.Now;
                model.ctIPAcceso = BaseUtil.GetIPAddress();
                if (model.cnID_Academico == 0)
                    model.cnID_Academico = lotbAcademicoBLL.Insert(model);
                else
                {
                    if (isnewFile)
                        BaseUtil.DeleteFile(BaseVars.CARPETA_CERTIFICADOS, oldFile);

                    lotbAcademicoBLL.Update(model);

                }

            }
            catch
            {
            }

            return RedirectToAction("Academica", new { a = "saved", i = model.cnID_Academico });
        }

        [HttpPost]
        public ActionResult SaveAcademicaBloque(tbAcademicoModel model, HttpPostedFileBase txtCertificado, int idbloque = 1)
        {
            try
            {
                string fileName = string.Empty;
                string oldFile = model.ctCertificado;
                bool isnewFile = false;
                //if (txtCertificado != null)
                //{
                if (BaseUtil.SaveFile(txtCertificado, BaseVars.CARPETA_CERTIFICADOS, out fileName))
                {
                    model.ctCertificado = fileName;
                    isnewFile = true;
                }
                //}

                tbAcademicoBLL lotbAcademicoBLL = new tbAcademicoBLL();

                //model.cfFechaCreacion = DateTime.Now;
                model.ctIPAcceso = BaseUtil.GetIPAddress();
                if (model.cnID_Academico == 0)
                    model.cnID_Academico = lotbAcademicoBLL.InsertBloque(model, idbloque);
                else
                {
                    if (isnewFile)
                        BaseUtil.DeleteFile(BaseVars.CARPETA_CERTIFICADOS, oldFile);

                    lotbAcademicoBLL.Update(model);

                }

            }
            catch
            {
            }

            return RedirectToAction("Academica", new { a = "saved", i = model.cnID_Academico });
        }

        [HttpPost]
        public ActionResult DeleteFormacion(int id)
        {
            try
            {
                tbAcademicoBLL lotbAcademicoBLL = new tbAcademicoBLL();
                tbAcademicoModel lotbAcademicoModel = lotbAcademicoBLL.GetBycnID_Academico(id);
                lotbAcademicoBLL.Delete(id);
                BaseUtil.DeleteFile(BaseVars.CARPETA_CERTIFICADOS, lotbAcademicoModel.ctCertificado);
                return Json("OK");
            }
            catch
            {
                return Json("KO");
            }
        }

        [HttpPost]
        public ActionResult DeleteFormacionTemporal(int id)
        {
            try
            {
                tbAcademicoBLL lotbAcademicoBLL = new tbAcademicoBLL();
                //tbAcademicoModel lotbAcademicoModel = lotbAcademicoBLL.GetBycnID_Academico(id);
                lotbAcademicoBLL.DeleteByCandidato(id);
                //BaseUtil.DeleteFile(BaseVars.CARPETA_CERTIFICADOS, lotbAcademicoModel.ctCertificado);
                return Json("OK");
            }
            catch
            {
                return Json("KO");
            }
        }

        public ActionResult Complementarios(int? id, string a = null, int? i = null)
        {
            if (!BaseGlobal.IsLogged)
                return this.RedirectToLogin();

            long cnID_Candidato = BaseGlobal.CandidatoActual.cnID_Candidato;
            ViewBag.Grados = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.Grado);
            ViewBag.Carreras = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.Carrera);

            var instituciones = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.Instituto);
            instituciones.AddRange(BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.Universidad));
            instituciones.Sort((x, y) => x.Text.CompareTo(y.Text));
            instituciones.RemoveAll(x => x.Value.Trim() == string.Empty && x.Text == BaseGlobal._ComboSeleccionar);
            instituciones.Insert(0, new SelectListItem() { Text = BaseGlobal._ComboSeleccionar, Value = string.Empty });
            instituciones.Insert(1, new SelectListItem() { Text = BaseGlobal._ComboOtros, Value = BaseGlobal.Otros.ToString() });
            ViewBag.Instituciones = instituciones;

            ViewBag.Situaciones = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.Situacion);
            ViewBag.Paises = BaseGlobal.GetPaisListItem();
            ViewBag.TipoEstudios = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.TipoEstudio);

            tbComplementarioBLL lotbComplementarioBLL = new tbComplementarioBLL();
            tbComplementarioModel lotbComplementarioModel = new tbComplementarioModel();
            lotbComplementarioModel.cfFechaInicio = DateTime.Now;
            lotbComplementarioModel.cfFechaFin = DateTime.Now;
            lotbComplementarioModel.cnID_Candidato = cnID_Candidato;
            lotbComplementarioModel.tbComplementarioModelList = new List<tbComplementarioModel>();

            if (id.HasValue)
            {
                if (id.Value > 0)
                {
                    lotbComplementarioModel = lotbComplementarioBLL.GetBycnID_Complementario(id.Value);
                }
            }

            lotbComplementarioModel.tbComplementarioModelList = lotbComplementarioBLL.GetBycnID_Candidato(cnID_Candidato);

            //--------------------
            ViewBag.action = a;
            ViewBag.id = i;
            //--------------------

            return View(lotbComplementarioModel);
        }

        [HttpPost]
        public ActionResult SaveComplementario(tbComplementarioModel model, HttpPostedFileBase txtCertificado)
        {
            try
            {
                string fileName = string.Empty;
                string oldFile = model.ctCertificado;
                bool isnewFile = false;
                if (txtCertificado != null)
                {
                    if (BaseUtil.SaveFile(txtCertificado, BaseVars.CARPETA_CERTIFICADOS, out fileName))
                    {
                        model.ctCertificado = fileName;
                        isnewFile = true;
                    }
                }

                tbComplementarioBLL lotbComplementarioBLL = new tbComplementarioBLL();

                model.cfFechaCreacion = DateTime.Now;
                model.ctIPAcceso = BaseUtil.GetIPAddress();
                if (model.cnID_Complementario == 0)
                    model.cnID_Complementario = lotbComplementarioBLL.Insert(model);
                else
                {
                    if (isnewFile)
                        BaseUtil.DeleteFile(BaseVars.CARPETA_CERTIFICADOS, oldFile);

                    lotbComplementarioBLL.Update(model);
                }

            }
            catch
            {
            }

            return RedirectToAction("Complementarios", new { a = "saved", i = model.cnID_Complementario });
        }


        [HttpPost]
        public ActionResult DeleteComplementario(int id)
        {
            try
            {
                tbComplementarioBLL lotbComplementarioBLL = new tbComplementarioBLL();
                tbComplementarioModel lotbComplementarioModel = new tbComplementarioModel();
                lotbComplementarioBLL.Delete(id);
                BaseUtil.DeleteFile(BaseVars.CARPETA_CERTIFICADOS, lotbComplementarioModel.ctCertificado);
                return Json("OK");
            }
            catch
            {
                return Json("KO");
            }
        }

        public ActionResult Idiomas(int? id, string a = null, int? i = null)
        {
            if (!BaseGlobal.IsLogged)
                return this.RedirectToLogin();

            long cnID_Candidato = BaseGlobal.CandidatoActual.cnID_Candidato;
            ViewBag.Idiomas = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.Idioma, addOtros: true);
            ViewBag.NivelLecturaEscrituraConv = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.NivelLecturaEscrituraConv);

            var instituciones = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.Instituto);
            instituciones.AddRange(BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.Universidad));
            instituciones.Sort((x, y) => x.Text.CompareTo(y.Text));
            instituciones.RemoveAll(x => x.Value.Trim() == string.Empty && x.Text == BaseGlobal._ComboSeleccionar);
            instituciones.Insert(0, new SelectListItem() { Text = BaseGlobal._ComboSeleccionar, Value = string.Empty });
            ViewBag.Instituciones = instituciones;

            ViewBag.Situaciones = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.SituacionTecnica);

            tbIdiomaBLL lotbIdiomaBLL = new tbIdiomaBLL();
            tbIdiomaModel lotbIdiomaModel = new tbIdiomaModel();
            lotbIdiomaModel.cnID_Candidato = cnID_Candidato;

            if (id.HasValue)
            {
                if (id.Value > 0)
                {
                    lotbIdiomaModel = lotbIdiomaBLL.GetBycnID_Idioma(id.Value);
                }
            }

            lotbIdiomaModel.tbIdiomaModelList = new List<tbIdiomaModel>();
            lotbIdiomaModel.tbIdiomaModelList = lotbIdiomaBLL.GetBycnID_Candidato(cnID_Candidato);

            //--------------------
            ViewBag.action = a;
            ViewBag.id = i;
            //--------------------

            return View(lotbIdiomaModel);
        }

        [HttpPost]
        public ActionResult SaveIdioma(tbIdiomaModel model, HttpPostedFileBase txtCertificado)
        {
            try
            {
                string fileName = string.Empty;
                string oldFile = model.ctCertificado;
                bool isnewFile = false;

                if (txtCertificado != null)
                {
                    if (BaseUtil.SaveFile(txtCertificado, BaseVars.CARPETA_CERTIFICADOS, out fileName))
                    {
                        model.ctCertificado = fileName;
                        isnewFile = true;
                    }
                }

                tbIdiomaBLL lotbIdiomaBLL = new tbIdiomaBLL();
                model.ctIPAcceso = BaseUtil.GetIPAddress();

                if (model.cnID_Institucion == 0)
                    model.cnID_Institucion = BaseGlobal.Ninguno;
                if (model.cnID_Situacion == 0)
                    model.cnID_Situacion = BaseGlobal.Ninguno;

                if (model.cnID_Idioma == 0)
                {
                    model.cfFechaCreacion = DateTime.Now;
                    model.cnID_Idioma = lotbIdiomaBLL.Insert(model);
                }
                else
                {
                    model.cfFechaActualizacion = DateTime.Now;
                    if (isnewFile)
                        BaseUtil.DeleteFile(BaseVars.CARPETA_CERTIFICADOS, oldFile);

                    lotbIdiomaBLL.Update(model);
                }
            }
            catch
            {
            }

            return RedirectToAction("Idiomas", new { a = "saved", i = model.cnID_Idioma });
        }

        [HttpPost]
        public ActionResult DeleteIdioma(int id)
        {
            try
            {
                tbIdiomaBLL lotbIdiomaBLL = new tbIdiomaBLL();
                tbIdiomaModel lotbIdiomaModel = lotbIdiomaBLL.GetBycnID_Idioma(id);
                lotbIdiomaBLL.Delete(id);
                BaseUtil.DeleteFile(BaseVars.CARPETA_CERTIFICADOS, lotbIdiomaModel.ctCertificado);
                return Json("OK");
            }
            catch
            {
                return Json("KO");
            }
        }
    }
}