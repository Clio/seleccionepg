﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using SalarDb.CodeGen.BLL;
using SalarDb.CodeGen.Common;
using SalarDb.CodeGen.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mvcSeleccion.Controllers
{
    public class SeleccionController : WebControllerBase
    {
        // GET: Seleccion
        public ActionResult Index()
        {
            if (!BaseGlobal.IsLogged)
                return this.RedirectToLogin();

            return View();
        }

        public ActionResult Registro(string a)
        {
            bool newUser = Request.Params.Get("a") == "new";
            if (!newUser && BaseGlobal.UsuarioActual == null)
            {
                return this.RedirectToLogin();
            }

            tbCandidatoBLL lotbCandidatoBLL = new tbCandidatoBLL();
            tbCandidatoModel lotbCandidatoModel = new tbCandidatoModel();

            if (a != null)
            {
                lotbCandidatoModel = new tbCandidatoModel();
                lotbCandidatoModel.cfFechaNacimiento = DateTime.Now;
                lotbCandidatoModel.tbDocIdentidadModelList = new List<tbDocIdentidadModel>();
                lotbCandidatoModel.tbDocIdentidadModelList.Add(new tbDocIdentidadModel());
            }
            else
            {
                long cnID_Candidato = BaseGlobal.CandidatoActual.cnID_Candidato;
                lotbCandidatoModel = lotbCandidatoBLL.GetBycnID_Candidato(cnID_Candidato);
                lotbCandidatoModel.ctEmail = lotbCandidatoModel.ctEmail.Trim(); //BGuerra 30-05-2015
                BaseGlobal.CandidatoActual = lotbCandidatoModel;
                lotbCandidatoModel.tbDocIdentidadModelList = new List<tbDocIdentidadModel>();
                if (cnID_Candidato == 0)
                {
                    lotbCandidatoModel.tbDocIdentidadModelList.Add(new tbDocIdentidadModel());
                }
                else
                {
                    tbDocIdentidadBLL lotbDocIdentidadBLL = new tbDocIdentidadBLL();
                    var docs = lotbDocIdentidadBLL.GetBycnID_CandidatoList(cnID_Candidato);
                    lotbCandidatoModel.tbDocIdentidadModelList = docs;
                }
            }

            ViewBag.Paises = BaseGlobal.GetPaisListItem();
            ViewBag.Documentos = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.TiposDocumento);

            var sexos = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.Sexo);
            var estadosCiviles = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.EstadoCivil);

            sexos.RemoveAll(x => x.Value.Trim() == string.Empty && x.Text == BaseGlobal._ComboSeleccionar);
            estadosCiviles.RemoveAll(x => x.Value.Trim() == string.Empty && x.Text == BaseGlobal._ComboSeleccionar);

            ViewBag.SexoListado = sexos;
            ViewBag.EstadoCivilListado = estadosCiviles;

            return View(lotbCandidatoModel);
        }

        [HttpPost]
        public ActionResult SaveRegistro(string model, int[] tbDocIdentidadModelList_cnID_TipoDocumento, string[] tbDocIdentidadModelList_ctNumero)
        {
            IDbConnection con = dbSeleccionDbConnection.GetNewConnection();
            dbSeleccionDbTransaction tran = dbSeleccionDbTransaction.BeginTransaction();
            tran.Connection = con;

            try
            {
                tbCandidatoModel loCandidato = JsonConvert.DeserializeObject<tbCandidatoModel>(model, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
                if (loCandidato.cnID_Departamento == 0)
                    loCandidato.cnID_Departamento = BaseGlobal.DepartamentoOtros;
                if (loCandidato.cnID_Provincia == 0)
                    loCandidato.cnID_Provincia = BaseGlobal.ProvinciaOtros;
                if (loCandidato.cnID_Distrito == 0)
                    loCandidato.cnID_Distrito = BaseGlobal.DistritoOtros;

                loCandidato.tbDocIdentidadModelList = new List<tbDocIdentidadModel>();
                for (int i = 0; i < tbDocIdentidadModelList_cnID_TipoDocumento.Count(); i++)
                {
                    loCandidato.tbDocIdentidadModelList.Add(new tbDocIdentidadModel() { cnID_TipoDocumento = tbDocIdentidadModelList_cnID_TipoDocumento[i], ctNumero = tbDocIdentidadModelList_ctNumero[i] });
                }

                loCandidato.ctIPAcceso = BaseUtil.GetIPAddress();

                tbCandidatoBLL lotbCandidatoBLL = new tbCandidatoBLL();

                lotbCandidatoBLL.Transaction = tran;
                lotbCandidatoBLL.Transaction.Connection = con;

                loCandidato.ctEmail = loCandidato.ctEmail.Trim();
                loCandidato.ctClaveAcceso = BaseUtil.EncryptPassword(loCandidato.ctClaveAcceso);
                if (loCandidato.cnID_Candidato > 0)
                {
                    lotbCandidatoBLL.Update(loCandidato);
                }
                else
                {
                    loCandidato.cnID_Candidato = lotbCandidatoBLL.Insert(loCandidato);
                }
                /*
                tbUsuarioModel lotbUsuarioModel = new tbUsuarioModel();
                tbUsuarioBLL lotbUsuarioBLL = new tbUsuarioBLL();

                lotbUsuarioBLL.Transaction = tran;
                lotbUsuarioBLL.Transaction.Connection = con;

                lotbUsuarioModel = lotbUsuarioBLL.GetByctLogin(loCandidato.ctEmail);
                if (lotbUsuarioModel != null)
                {
                    lotbUsuarioModel.ctClave = BaseUtil.EncryptPassword(loCandidato.ctClaveAcceso);
                    lotbUsuarioModel.ctNombres = loCandidato.ctNombres;
                    lotbUsuarioModel.ctApellidos = loCandidato.ctApellidoPaterno;
                    lotbUsuarioModel.cnEstadoReg = true;
                    lotbUsuarioBLL.Update(lotbUsuarioModel);
                }
                else
                {
                    lotbUsuarioModel = new tbUsuarioModel();
                    lotbUsuarioModel.cnID_Perfil = 1;
                    lotbUsuarioModel.ctLogin = loCandidato.ctEmail;
                    lotbUsuarioModel.ctClave = BaseUtil.EncryptPassword(loCandidato.ctClaveAcceso);
                    lotbUsuarioModel.ctNombres = loCandidato.ctNombres;
                    lotbUsuarioModel.ctApellidos = loCandidato.ctApellidoPaterno;
                    lotbUsuarioModel.cnEstadoReg = true;
                    //lotbUsuarioBLL.Insert(lotbUsuarioModel);
                }
                */

                tbDocIdentidadBLL lotbDocIdentidadBLL = new tbDocIdentidadBLL();

                lotbDocIdentidadBLL.Transaction = tran;
                lotbDocIdentidadBLL.Transaction.Connection = con;

                lotbDocIdentidadBLL.Delete(loCandidato.cnID_Candidato);
                loCandidato.tbDocIdentidadModelList.ForEach(x =>
                {
                    x.cnID_Candidato = loCandidato.cnID_Candidato;
                    lotbDocIdentidadBLL.Insert(x);
                });


                tbMailBLL lotbMaildBLL = new tbMailBLL();

                lotbMaildBLL.Transaction = tran;
                lotbMaildBLL.Transaction.Connection = con;

                var mail = new tbMailModel();
                mail.cnID_TipoEmail = BaseGlobal.TipoMailPersonal;
                mail.ctMail = loCandidato.ctEmail;
                mail.cnID_Candidato = loCandidato.cnID_Candidato;
                lotbMaildBLL.Insert(mail);

                tran.Commit();

                return Json("OK");
            }
            catch
            {
                tran.Rollback();
                return Json("KO");
            }
        }

        public ActionResult DatosPersonales(string a)
        {
            bool newUser = Request.Params.Get("a") == "new";
            if (!newUser && BaseGlobal.UsuarioActual == null)
            {
                return this.RedirectToLogin();
            }

            tbCandidatoBLL lotbCandidatoBLL = new tbCandidatoBLL();
            tbCandidatoModel lotbCandidatoModel = new tbCandidatoModel();

            if (a != null)
            {
                lotbCandidatoModel = new tbCandidatoModel();
                lotbCandidatoModel.cfFechaNacimiento = DateTime.Now;

                lotbCandidatoModel.tbDocIdentidadModelList = new List<tbDocIdentidadModel>();
                lotbCandidatoModel.tbDocIdentidadModelList.Add(new tbDocIdentidadModel());

                lotbCandidatoModel.tbMailModelList = new List<tbMailModel>();
                lotbCandidatoModel.tbMailModelList.Add(new tbMailModel());
            }
            else
            {
                long cnID_Candidato = BaseGlobal.CandidatoActual.cnID_Candidato;
                lotbCandidatoModel = lotbCandidatoBLL.GetBycnID_Candidato(cnID_Candidato);
                lotbCandidatoModel.ctEmail = lotbCandidatoModel.ctEmail.Trim(); //BGuerra 30-05-2015
                BaseGlobal.CandidatoActual = lotbCandidatoModel;

                lotbCandidatoModel.tbDocIdentidadModelList = new List<tbDocIdentidadModel>();
                lotbCandidatoModel.tbMailModelList = new List<tbMailModel>();

                if (cnID_Candidato == 0)
                {
                    lotbCandidatoModel.tbDocIdentidadModelList.Add(new tbDocIdentidadModel());
                    lotbCandidatoModel.tbMailModelList.Add(new tbMailModel());
                }
                else
                {
                    tbDocIdentidadBLL lotbDocIdentidadBLL = new tbDocIdentidadBLL();
                    var docs = lotbDocIdentidadBLL.GetBycnID_CandidatoList(cnID_Candidato);
                    lotbCandidatoModel.tbDocIdentidadModelList = docs;

                    tbMailBLL lotbMaildBLL = new tbMailBLL();
                    var mails = lotbMaildBLL.GetBycnID_CandidatoList(cnID_Candidato);
                    lotbCandidatoModel.tbMailModelList = mails;

                    //agregar mails
                }
            }

            ViewBag.Paises = BaseGlobal.GetPaisListItem();
            ViewBag.Documentos = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.TiposDocumento);
            ViewBag.TipoEmail = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.TipoEmail);

            var sexos = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.Sexo);
            var estadosCiviles = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.EstadoCivil);

            sexos.RemoveAll(x => x.Value.Trim() == string.Empty && x.Text == BaseGlobal._ComboSeleccionar);
            estadosCiviles.RemoveAll(x => x.Value.Trim() == string.Empty && x.Text == BaseGlobal._ComboSeleccionar);

            ViewBag.SexoListado = sexos;
            ViewBag.EstadoCivilListado = estadosCiviles;

            return View(lotbCandidatoModel);
        }

        [HttpPost]
        public ActionResult SaveDatosPersonales(string model, int[] tbDocIdentidadModelList_cnID_TipoDocumento, string[] tbDocIdentidadModelList_ctNumero,
            int[] tbMailModelList_cnID_TipoEmail, string[] tbMailModelList_ctMail)
        {
            IDbConnection con = dbSeleccionDbConnection.GetNewConnection();
            dbSeleccionDbTransaction tran = dbSeleccionDbTransaction.BeginTransaction();
            tran.Connection = con;

            try
            {
                tbCandidatoModel loCandidato = JsonConvert.DeserializeObject<tbCandidatoModel>(model, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
                if (loCandidato.cnID_Departamento == 0)
                    loCandidato.cnID_Departamento = BaseGlobal.DepartamentoOtros;
                if (loCandidato.cnID_Provincia == 0)
                    loCandidato.cnID_Provincia = BaseGlobal.ProvinciaOtros;
                if (loCandidato.cnID_Distrito == 0)
                    loCandidato.cnID_Distrito = BaseGlobal.DistritoOtros;

                loCandidato.tbDocIdentidadModelList = new List<tbDocIdentidadModel>();
                for (int i = 0; i < tbDocIdentidadModelList_cnID_TipoDocumento.Count(); i++)
                {
                    loCandidato.tbDocIdentidadModelList.Add(new tbDocIdentidadModel() { cnID_TipoDocumento = tbDocIdentidadModelList_cnID_TipoDocumento[i], ctNumero = tbDocIdentidadModelList_ctNumero[i] });
                }

                loCandidato.tbMailModelList = new List<tbMailModel>();
                for (int i = 0; i < tbMailModelList_cnID_TipoEmail.Count(); i++)
                {
                    loCandidato.tbMailModelList.Add(new tbMailModel() { cnID_TipoEmail = tbMailModelList_cnID_TipoEmail[i], ctMail = tbMailModelList_ctMail[i] });
                }

                loCandidato.ctIPAcceso = BaseUtil.GetIPAddress();

                tbCandidatoBLL lotbCandidatoBLL = new tbCandidatoBLL();

                lotbCandidatoBLL.Transaction = tran;
                lotbCandidatoBLL.Transaction.Connection = con;

                //loCandidato.ctEmail = loCandidato.ctEmail.Trim();
                //loCandidato.ctClaveAcceso = BaseUtil.EncryptPassword(loCandidato.ctClaveAcceso);
                if (loCandidato.cnID_Candidato > 0)
                {
                    lotbCandidatoBLL.Update(loCandidato);
                }
                else
                {
                    loCandidato.cnID_Candidato = lotbCandidatoBLL.Insert(loCandidato);
                }
                /*
                tbUsuarioModel lotbUsuarioModel = new tbUsuarioModel();
                tbUsuarioBLL lotbUsuarioBLL = new tbUsuarioBLL();

                lotbUsuarioBLL.Transaction = tran;
                lotbUsuarioBLL.Transaction.Connection = con;

                lotbUsuarioModel = lotbUsuarioBLL.GetByctLogin(loCandidato.ctEmail);
                if (lotbUsuarioModel != null)
                {
                    lotbUsuarioModel.ctClave = BaseUtil.EncryptPassword(loCandidato.ctClaveAcceso);
                    lotbUsuarioModel.ctNombres = loCandidato.ctNombres;
                    lotbUsuarioModel.ctApellidos = loCandidato.ctApellidoPaterno;
                    lotbUsuarioModel.cnEstadoReg = true;
                    lotbUsuarioBLL.Update(lotbUsuarioModel);
                }
                else
                {
                    lotbUsuarioModel = new tbUsuarioModel();
                    lotbUsuarioModel.cnID_Perfil = 1;
                    lotbUsuarioModel.ctLogin = loCandidato.ctEmail;
                    lotbUsuarioModel.ctClave = BaseUtil.EncryptPassword(loCandidato.ctClaveAcceso);
                    lotbUsuarioModel.ctNombres = loCandidato.ctNombres;
                    lotbUsuarioModel.ctApellidos = loCandidato.ctApellidoPaterno;
                    lotbUsuarioModel.cnEstadoReg = true;
                    //lotbUsuarioBLL.Insert(lotbUsuarioModel);
                }
                */

                tbDocIdentidadBLL lotbDocIdentidadBLL = new tbDocIdentidadBLL();

                lotbDocIdentidadBLL.Transaction = tran;
                lotbDocIdentidadBLL.Transaction.Connection = con;

                lotbDocIdentidadBLL.Delete(loCandidato.cnID_Candidato);
                loCandidato.tbDocIdentidadModelList.ForEach(x =>
                {
                    x.cnID_Candidato = loCandidato.cnID_Candidato;
                    lotbDocIdentidadBLL.Insert(x);
                });

                tbMailBLL lotbMaildBLL = new tbMailBLL();

                lotbMaildBLL.Transaction = tran;
                lotbMaildBLL.Transaction.Connection = con;

                lotbMaildBLL.Delete(loCandidato.cnID_Candidato);
                loCandidato.tbMailModelList.ForEach(x =>
                {
                    x.cnID_Candidato = loCandidato.cnID_Candidato;
                    lotbMaildBLL.Insert(x);
                });

                tran.Commit();

                return Json("OK");
            }
            catch
            {
                tran.Rollback();
                return Json("KO");
            }
        }

        public ActionResult GetHojaVida(int? cnID_Postulante = null, long? cnID_Candidato = null, bool? doc = true, string reportType = "PDF")
        {
            if (!BaseGlobal.IsLogged)
                return this.RedirectToLogin();

            byte[] loHojaVida = BaseGlobal.GetHojaVida(cnID_Postulante, cnID_Candidato, tlDocumentado: doc, reportType: reportType);

            MemoryStream stream = new MemoryStream(loHojaVida);
            //Stream _st = stream;

            if (reportType == "PDF")
                return new FileStreamResult(stream, "application/pdf");
            else
                return new FileStreamResult(stream, "application/msword");
            //return File(_st, "application/pdf", "HojaVida.pdf");
        }

        [HttpPost]
        public ActionResult GetHojaVidaMasivo(long cnID_Proceso, int[] cnID_Postulante)
        {
            try
            {
                tbProcesoBLL lotbProcesoBLL = new tbProcesoBLL();
                var loProceso = lotbProcesoBLL.GetBycnID_Proceso(cnID_Proceso);

                if (cnID_Postulante != null)
                {
                    for (int i = 0; i < cnID_Postulante.Length; i++)
                    {
                        tbPostulanteBLL lotbPostulanteBLL = new tbPostulanteBLL();
                        var loPostulante = lotbPostulanteBLL.GetBycnID_Postulante(cnID_Postulante[i]);

                        tbCandidatoBLL lotbCandidatoBLL = new tbCandidatoBLL();
                        var loCandidato = lotbCandidatoBLL.GetBycnID_Candidato(loPostulante.cnID_Candidato);

                        var loHojaVida = BaseGlobal.GetHojaVida(loPostulante.cnID_Postulante, loPostulante.cnID_Candidato, false);
                        //guardar en carpeta con nombre de archivo;

                        string _filename = string.Format("{0} {1} {2}.pdf", loCandidato.ctApellidoPaterno, loCandidato.ctApellidoMaterno, loCandidato.ctNombres);

                        BaseUtil.SaveFile(loHojaVida, BaseVars.CARPETA_HOJA_VIDA_POSTULANTES + "/" + loProceso.ctTitulo + "/", _filename);
                    }
                }
                return Json(new { result = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
            }

            return Json(new { result = "KO" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DownloadFile(string file)
        {
            if (!BaseGlobal.IsLogged)
                return this.RedirectToLogin();

            string _file = BaseVars.CARPETA_CERTIFICADOS + "/" + file;
            if (System.IO.File.Exists(_file))
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(_file);
                string fileName = file;
                MemoryStream stream = new MemoryStream(fileBytes);

                return new FileStreamResult(stream, "application/pdf");

                //return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
            }
            return Json(new { result = "Archivo no encontrado" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult DownloadFilePostulacion(string file)
        {
            if (!BaseGlobal.IsLogged)
                return this.RedirectToLogin();

            string _file = BaseVars.CARPETA_POSTULACION + "/" + file;
            if (System.IO.File.Exists(_file))
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(_file);
                string fileName = file;
                MemoryStream stream = new MemoryStream(fileBytes);

                return new FileStreamResult(stream, "application/pdf");

                //return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
            }
            return Json(new { result = "Archivo no encontrado" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Oportunidades(string a = null, long? i = null)
        {
            if (!BaseGlobal.IsLogged)
                return this.RedirectToLogin();

            List<tbProcesoModel> lotbProcesoModelList = new List<tbProcesoModel>();
            tbProcesoBLL lotbProcesoBLL = new tbProcesoBLL();

            lotbProcesoModelList = lotbProcesoBLL.GetProcesosVigentes();

            List<tbPostulanteModel> tbPostulanteModelList = new List<tbPostulanteModel>();
            tbPostulanteBLL lotbPostulanteBLL = new tbPostulanteBLL();

            tbPostulanteModelList = lotbPostulanteBLL.GetBycnID_Candidato(BaseGlobal.CandidatoActual.cnID_Candidato);

            lotbProcesoModelList.ForEach(proceso =>
            {
                proceso.EsPostulante = tbPostulanteModelList.Find(x => x.cnID_Proceso == proceso.cnID_Proceso && x.cnEstadoReg == 1) != null;
            });

            ViewBag.action = a;

            return View(lotbProcesoModelList);
        }

        public ActionResult Postular(long id)
        {
            if (!BaseGlobal.IsLogged)
                return this.RedirectToLogin();

            tbProcesoBLL lotbProcesoBLL = new tbProcesoBLL();
            tbProcesoModel lotbProcesoModel = lotbProcesoBLL.GetBycnID_Proceso(id);

            tbProcesoCoberturaBLL lotbProcesoCoberturaBLL = new tbProcesoCoberturaBLL();
            lotbProcesoModel.tbProcesoCoberturaList = lotbProcesoCoberturaBLL.GetBycnID_Proceso(id);

            tbProcesoCnfgAcadBLL lotbProcesoCnfgAcadBLL = new tbProcesoCnfgAcadBLL();
            lotbProcesoModel.tbProcesoCnfgAcadModelList = lotbProcesoCnfgAcadBLL.GetBycnID_Proceso(id);

            tbProcesoCnfgLaboBLL lotbProcesoCnfgLaboBLL = new tbProcesoCnfgLaboBLL();
            lotbProcesoModel.tbProcesoCnfgLaboList = lotbProcesoCnfgLaboBLL.GetBycnID_Proceso(id);

            tbProcesoCriterioAdicBLL lotbProcesoCriterioAdicBLL = new tbProcesoCriterioAdicBLL();
            lotbProcesoModel.tbProcesoCriterioAdicModelList = lotbProcesoCriterioAdicBLL.GetBycnID_Proceso(id);

            tbProcesoDocumentoBLL LOtbProcesoDocumentoBLL = new tbProcesoDocumentoBLL();
            lotbProcesoModel.tbProcesoDocumentoModelList = LOtbProcesoDocumentoBLL.GetBycnID_Proceso(id);

            tbProcesoMatrizLaboBLL lotbProcesoMatrizLaboBLL = new tbProcesoMatrizLaboBLL();
            lotbProcesoModel.tbProcesoMatrizLaboModelList = lotbProcesoMatrizLaboBLL.GetBycnID_Proceso(id);

            var loTipoEntidad = lotbProcesoModel.tbProcesoCnfgLaboList.First();

            tbExperienciaBLL lotbExperienciaBLL = new tbExperienciaBLL();
            var loExperienciaCandidatoList = lotbExperienciaBLL.GetBycnID_Candidato(BaseGlobal.CandidatoActual.cnID_Candidato);

            ViewBag.ExperienciaCandidatoList = new List<tbExperienciaModel>();
            if (loExperienciaCandidatoList != null)
                ViewBag.ExperienciaCandidatoList = loExperienciaCandidatoList.FindAll(x => BaseGlobal.GetTipoEntidadProceso(x.cnID_TipoEntidad) == loTipoEntidad.cnID_TipoEntidad || (loTipoEntidad.cnID_TipoEntidad == BaseGlobal.TipoEntidadPrivPub));

            return View(lotbProcesoModel);
        }

        [HttpPost]
        public ActionResult ValidarExpLabEsp(int cnID_Proceso, int[] cnID_Experiencia)
        {
            bool cumpleExpLabEsp = BaseGlobal.CumpleNivelLaboralEspecif(cnID_Proceso, cnID_Experiencia);
            if (cumpleExpLabEsp)
                return Json(new { res = "OK", message = string.Empty });
            else
                return Json(new { res = "KO", message = "Usted no cumple con el criterio específico solicitado." });
        }

        [HttpPost]
        public ActionResult SavePostulacion(tbProcesoModel model, int[] tbPostulanteDocuModelList, ICollection<string> tbProcesoCoberturaZonaList)
        {
            long lnID_Postulante = 0;
            var critAdicional = Request.Form["tbProcesoCriterioAdicModelList[]"];
            bool b = false;
            List<string> criterioAdicionalesTmp = new List<string>();
            if (critAdicional != null)
                criterioAdicionalesTmp = critAdicional.Split(',').ToList().FindAll(x => !bool.TryParse(x, out b));

            var critLaboral = Request.Form["chkExperienciaList[]"];
            var criterioLaboralesEspecificoTmp = critLaboral.Split(',').ToList().FindAll(x => !bool.TryParse(x, out b));

            if (model.cnID_Proceso > 0)
            {
                tbProcesoCriterioAdicBLL lotbProcesoCriterioAdicBLL = new tbProcesoCriterioAdicBLL();
                var tbProcesoCriterioAdicModelList = lotbProcesoCriterioAdicBLL.GetBycnID_Proceso(model.cnID_Proceso);
                var criteriosAdicionales = tbProcesoCriterioAdicModelList.FindAll(x => criterioAdicionalesTmp.Find(y => int.Parse(y) == x.cnID_CriterioAdicional) != null);

                long cnID_Academico = 0;
                int lnPuntajeAcademico = BaseGlobal.GetPuntajeAcademico(model.cnID_Proceso, out cnID_Academico);

                List<object[]> loExperienciaLaboral = new List<object[]>();
                int lnPuntajeLaboral = BaseGlobal.GetPuntajeLaboral(model.cnID_Proceso, criterioLaboralesEspecificoTmp, out loExperienciaLaboral);

                IDbConnection con = dbSeleccionDbConnection.GetNewConnection();
                dbSeleccionDbTransaction tran = dbSeleccionDbTransaction.BeginTransaction();
                tran.Connection = con;

                int[] cnID_Experiencia = new int[criterioLaboralesEspecificoTmp.Count];
                int index = 0;
                criterioLaboralesEspecificoTmp.ForEach(x =>
                {
                    cnID_Experiencia[index] = int.Parse(x);
                    index++;
                });

                try
                {
                    tbPostulanteBLL lotbPostulanteBLL = new tbPostulanteBLL();
                    tbPostulanteDocuBLL lotbPostulanteDocuBLL = new tbPostulanteDocuBLL();
                    tbPostulanteCobeBLL lotbPostulanteCobeBLL = new tbPostulanteCobeBLL();
                    tbPostulanteLaboBLL lotbPostulanteLaboBLL = new tbPostulanteLaboBLL();
                    tbPostulanteCritBLL lotbPostulanteCritBLL = new tbPostulanteCritBLL();

                    tbPostulanteAcadBLL lotbPostulanteAcadBLL = new tbPostulanteAcadBLL();
                    tbAcademicoBLL lotbAcademicoBLL = new tbAcademicoBLL();

                    lotbPostulanteBLL.Transaction = tran;
                    lotbPostulanteBLL.Transaction.Connection = con;

                    lotbPostulanteDocuBLL.Transaction = tran;
                    lotbPostulanteDocuBLL.Transaction.Connection = con;

                    lotbPostulanteCobeBLL.Transaction = tran;
                    lotbPostulanteCobeBLL.Transaction.Connection = con;

                    lotbPostulanteLaboBLL.Transaction = tran;
                    lotbPostulanteLaboBLL.Transaction.Connection = con;

                    lotbPostulanteCritBLL.Transaction = tran;
                    lotbPostulanteCritBLL.Transaction.Connection = con;

                    lotbPostulanteAcadBLL.Transaction = tran;
                    lotbPostulanteAcadBLL.Transaction.Connection = con;

                    lotbAcademicoBLL.Transaction = tran;
                    lotbAcademicoBLL.Transaction.Connection = con;

                    tbPostulanteModel lotbPostulanteModel = new tbPostulanteModel();
                    lotbPostulanteModel.cnID_Candidato = BaseGlobal.CandidatoActual.cnID_Candidato;
                    lotbPostulanteModel.cnID_Proceso = model.cnID_Proceso;
                    lotbPostulanteModel.cnPuntosCriterioAcademico = lnPuntajeAcademico;
                    lotbPostulanteModel.cnPuntosCriterioAdicional = criteriosAdicionales.Sum(p => p.cnPuntaje);
                    lotbPostulanteModel.cnPuntosCriterioLaboral = lnPuntajeLaboral;
                    lotbPostulanteModel.cfFechaCreacion = DateTime.Now;
                    lotbPostulanteModel.ctIPAcceso = BaseUtil.GetIPAddress();
                    lotbPostulanteModel.cnEstadoReg = 1;
                    //lotbPostulanteModel.cnID_Grado = cnGrado;
                    lotbPostulanteModel.cnEvalAuto = BaseGlobal.CumpleNivelAcademico(model.cnID_Proceso) && BaseGlobal.CumpleNivelLaboralGeneral(model.cnID_Proceso) && BaseGlobal.CumpleNivelLaboralEspecif(model.cnID_Proceso, cnID_Experiencia);
                    lotbPostulanteModel.cnEvalManual = false;

                    lnID_Postulante = lotbPostulanteBLL.Insert(lotbPostulanteModel);

                    List<tbPostulanteDocuModel> lotbPostulanteDocuModelList = new List<tbPostulanteDocuModel>();

                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        var file = Request.Files[i];
                        if (file != null)
                        {
                            if (file.InputStream != null)
                            {
                                if (file.ContentLength > 0)
                                {
                                    string fileName = string.Empty;
                                    BaseUtil.SaveFile(file, BaseVars.CARPETA_POSTULACION, out fileName);
                                    lotbPostulanteDocuModelList.Add(new tbPostulanteDocuModel()
                                    {
                                        cnID_Postulante = lnID_Postulante,
                                        cnID_Documento = tbPostulanteDocuModelList[i],
                                        cfFechaCreacion = DateTime.Now,
                                        ctIPAcceso = BaseUtil.GetIPAddress(),
                                        ctRutaArchivo = fileName
                                    });
                                }
                            }
                        }
                    }

                    lotbPostulanteDocuModelList.ForEach(doc =>
                    {
                        lotbPostulanteDocuBLL.Insert(doc);
                    });

                    List<tbPostulanteCobeModel> tbPostulanteCobeModelList = new List<tbPostulanteCobeModel>();
                    tbProcesoCoberturaZonaList.ToList().ForEach(cob =>
                    {
                        tbCoberturaZonaBLL lotbCoberturaZonaBLL = new tbCoberturaZonaBLL();
                        var loCoberturaZonaModel = lotbCoberturaZonaBLL.GetBycnID_CoberturaZona(long.Parse(cob));

                        tbPostulanteCobeModelList.Add(new tbPostulanteCobeModel() { cnID_Postulante = lnID_Postulante, cnID_Cobertura = loCoberturaZonaModel.cnID_Cobertura, cnID_CoberturaZona = long.Parse(cob) });
                    });

                    tbPostulanteCobeModelList.ForEach(cob =>
                    {
                        lotbPostulanteCobeBLL.Insert(cob);
                    });

                    List<tbPostulanteLaboModel> tbPostulanteLaboList = new List<tbPostulanteLaboModel>();
                    loExperienciaLaboral.ForEach(exp =>
                    {
                        tbPostulanteLaboList.Add(new tbPostulanteLaboModel() { cnID_Postulante = lnID_Postulante, cnID_Experiencia = long.Parse(exp[0].ToString()), cnTiempo = int.Parse(exp[1].ToString()), cnID_TipoCriterioLabo = int.Parse(exp[2].ToString()) });
                    });

                    tbPostulanteLaboList.ForEach(lab =>
                    {
                        lotbPostulanteLaboBLL.Insert(lab);
                    });

                    criteriosAdicionales.ForEach(crit =>
                    {
                        lotbPostulanteCritBLL.Insert(new tbPostulanteCritModel() { cnID_Postulante = lnID_Postulante, cnID_CriterioAdicional = crit.cnID_CriterioAdicional });
                    });

                    tbPostulanteAcadModel lotbPostulanteAcadModel = new tbPostulanteAcadModel();
                    tbAcademicoModel lotbAcademicoModel = new tbAcademicoModel();
                    lotbAcademicoModel = lotbAcademicoBLL.GetBycnID_Academico(cnID_Academico);
                    if (lotbAcademicoModel != null)
                    {
                        lotbPostulanteAcadModel = new tbPostulanteAcadModel() { cnID_Postulante = lnID_Postulante, cnID_Academico = lotbAcademicoModel.cnID_Academico, cnID_Grado = lotbAcademicoModel.cnID_Grado, cnID_Situacion = lotbAcademicoModel.cnID_Situacion };
                        lotbPostulanteAcadBLL.Insert(lotbPostulanteAcadModel);
                    }

                    tran.Commit();
                }
                catch
                {
                    tran.Rollback();
                }
            }

            return RedirectToAction("Oportunidades", new { a = "saved", i = lnID_Postulante });
        }

        public ActionResult Anular(long id)
        {
            tbPostulanteBLL lotbPostulanteBLL = new tbPostulanteBLL();
            lotbPostulanteBLL.AnularPostulacion(BaseGlobal.CandidatoActual.cnID_Candidato, id);

            return RedirectToAction("Oportunidades", new { a = "deleted" });
        }

        public ActionResult Perfil()
        {
            tbCandidatoBLL lotbCandidatoBLL = new tbCandidatoBLL();
            tbCandidatoModel lotbCandidatoModel = new tbCandidatoModel();
            long cnID_Candidato = BaseGlobal.CandidatoActual.cnID_Candidato;
            lotbCandidatoModel = lotbCandidatoBLL.GetBycnID_Candidato(cnID_Candidato);

            tbMailBLL lotbMailBLL = new tbMailBLL();
            var mails = lotbMailBLL.GetBycnID_CandidatoList(cnID_Candidato);

            lotbCandidatoModel.cnID_TipoEmail = mails.Find(x => x.ctMail.ToUpper().Trim() == lotbCandidatoModel.ctEmail.ToUpper().Trim()).cnID_TipoEmail;

            ViewBag.Emails = new List<SelectListItem>() { new SelectListItem() { Text = lotbCandidatoModel.ctEmail, Value = lotbCandidatoModel.ctEmail } };
            ViewBag.TipoEmail = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.TipoEmail);

            return View(lotbCandidatoModel);
        }

        [HttpPost]
        public ActionResult SavePerfil(tbCandidatoModel model, HttpPostedFileBase txtImagen)
        {
            try
            {
                string fileName = string.Empty;
                string oldFile = model.ctImagenPerfil;
                bool isnewFile = false;
                if (txtImagen != null)
                {
                    if (BaseUtil.SaveFile(txtImagen, BaseVars.CARPETA_PERFIL, out fileName))
                    {
                        model.ctImagenPerfil = fileName;
                        isnewFile = true;
                    }
                }

                if (isnewFile)
                    BaseUtil.DeleteFile(BaseVars.CARPETA_PERFIL, oldFile);

                tbCandidatoBLL lotbCandidatoBLL = new tbCandidatoBLL();
                var lotbCandidatoModel = new tbCandidatoModel();
                lotbCandidatoModel = lotbCandidatoBLL.GetBycnID_Candidato(BaseGlobal.CandidatoActual.cnID_Candidato);
                lotbCandidatoModel.ctEmail = model.ctEmail;

                lotbCandidatoModel.ctDescripcion = model.ctDescripcion;

                lotbCandidatoModel.ctImagenPerfil = model.ctImagenPerfil;
                if (model.ctClaveAcceso != null)
                    if (model.ctClaveAcceso.Trim().Length > 0)
                        lotbCandidatoModel.ctClaveAcceso = BaseUtil.EncryptPassword(model.ctClaveAcceso);

                lotbCandidatoBLL.UpdateProfile(lotbCandidatoModel);

                BaseGlobal.CandidatoActual = lotbCandidatoBLL.GetByctEmail(model.ctEmail);
            }
            catch
            {
            }
            return RedirectToAction("Perfil");
        }

        public ActionResult AreaTematica()
      {
            //var listaGrupos = BaseGlobal.GetMaestroGrupoList("TEMATICA");
            //var firstLevel = listaGrupos.Find(x => x.cnGrupoPadre == 0);
            //ViewBag.Nivel1List = BaseGlobal.GetMaestroNivelListItem(firstLevel.cnID_GrupoLista, addTodos: false);
            //ViewBag.Niveles = listaGrupos;

            //tbAreaTematicaBLL lotbAreaTematicaBLL = new tbAreaTematicaBLL();
            //tbAreaTematicaModel lotbAreaTematicaModel = lotbAreaTematicaBLL.GetBycnID_Candidato(BaseGlobal.CandidatoActual.cnID_Candidato);

            //lotbAreaTematicaModel = lotbAreaTematicaModel ?? new tbAreaTematicaModel();

            //return View(lotbAreaTematicaModel);


            var listaGrupos = BaseGlobal.GetMaestroGrupoList("TEMATICA");
            var firstLevel = listaGrupos.Find(x => x.cnGrupoPadre == 0);
            var fathers = listaGrupos.Where(x => x.cnGrupoPadre == 0).ToList();

            List<SelectListItem> fatherSelectList = new List<SelectListItem>();


            fatherSelectList.AddRange((from x in fathers select new SelectListItem() { Text = x.ctNombreGrupo, Value = x.cnID_GrupoLista.ToString() }).ToList());

            ViewBag.Nivel1List = fatherSelectList;
            // ViewBag.Nivel1List = BaseGlobal.GetMaestroNivelListItem(firstLevel.cnID_GrupoLista, addTodos: false);
            ViewBag.Niveles = listaGrupos;

            tbAreaTematicaBLL lotbAreaTematicaBLL = new tbAreaTematicaBLL();
            tbAreaTematicaModel lotbAreaTematicaModel = lotbAreaTematicaBLL.GetBycnID_Candidato(BaseGlobal.CandidatoActual.cnID_Candidato);

            lotbAreaTematicaModel = lotbAreaTematicaModel ?? new tbAreaTematicaModel();

            return View(lotbAreaTematicaModel);
        }

        [HttpPost]
        public ActionResult SaveAreaTematica(int cnID_AreaTematica, string model, /*int[] chkNivel3List, */string[] ctDominioEspecifico)
        {
            try
            {
                var objModel = JsonConvert.DeserializeObject<tbAreaTematicaModel>(model);
                if (ctDominioEspecifico != null)
                    objModel.ctDominioEspecifico = string.Join(",", ctDominioEspecifico);
                else
                    objModel.ctDominioEspecifico = string.Empty;

                objModel.cnID_AreaTematica = cnID_AreaTematica;
                objModel.cnID_Candidato = BaseGlobal.CandidatoActual.cnID_Candidato;

                tbAreaTematicaBLL lotbAreaTematicaBLL = new tbAreaTematicaBLL();
                objModel.ctIPAcceso = BaseUtil.GetIPAddress();

                if (objModel.cnID_AreaTematica == 0)
                {
                    objModel.cnID_AreaTematica = lotbAreaTematicaBLL.Insert(objModel);
                }
                else
                {
                    lotbAreaTematicaBLL.Update(objModel);
                }

                return Json(new { result = "OK", id = objModel.cnID_AreaTematica });
            }
            catch (Exception)
            {
                return Json(new { result = "KO", id = 0 });
            }
        }

        //
        [HttpPost]
        public ActionResult SaveAreaTematicaDet(long cnID_AreaTematica, int cnID_AreaTematicaNivel2, int[] chkNivel3List)
        {
            try
            {
                tbAreaTematicaDetBLL lotbAreaTematicaDetBLL = new tbAreaTematicaDetBLL();

                lotbAreaTematicaDetBLL.DeleteByNivel(BaseGlobal.CandidatoActual.cnID_Candidato, cnID_AreaTematicaNivel2);
                if (chkNivel3List != null)
                {
                    if (cnID_AreaTematica == 0)
                    {
                        tbAreaTematicaBLL lotbAreaTematicaBLL = new tbAreaTematicaBLL();
                        var objModel = new tbAreaTematicaModel();
                        objModel.ctIPAcceso = BaseUtil.GetIPAddress();
                        objModel.cnID_Candidato = BaseGlobal.CandidatoActual.cnID_Candidato;
                        cnID_AreaTematica = lotbAreaTematicaBLL.Insert(objModel);
                    }

                    chkNivel3List.ToList().ForEach(x =>
                    {
                        lotbAreaTematicaDetBLL.Insert(new tbAreaTematicaDetModel()
                        {
                            cnID_AreaTematica = cnID_AreaTematica,
                            cnID_DominioCurso = x,
                            ctIPAcceso = BaseUtil.GetIPAddress()
                        });
                    });
                }

                return Json(new { result = "OK", id = cnID_AreaTematica });
            }
            catch (Exception)
            {
                return Json(new { result = "KO", id = 0 });
            }
        }
    }
}