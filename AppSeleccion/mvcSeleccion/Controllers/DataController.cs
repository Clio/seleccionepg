﻿using SalarDb.CodeGen.BLL;
using SalarDb.CodeGen.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace mvcSeleccion.Controllers
{
    public class DataController : Controller
    {
        [HttpGet]
        public ActionResult GetDepartamentos(int idPais)
        {
            tbDepartamentoBLL lotbDepartamentoBLL = new tbDepartamentoBLL();
            var loDepartamentoList = lotbDepartamentoBLL.GetAll();

            //loDepartamentoList.Insert(0, new SalarDb.CodeGen.Model.tbDepartamentoModel() { ctDepartamento = BaseGlobal._ComboSeleccionar });

            return Json(loDepartamentoList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetProvincias(int idDepartamento)
        {
            tbProvinciaBLL lotbProvinciaBLL = new tbProvinciaBLL();
            var loProvinciaList = lotbProvinciaBLL.GetBycnID_Departamento(idDepartamento);

            //loProvinciaList.Insert(0, new SalarDb.CodeGen.Model.tbProvinciaModel() { ctProvincia = BaseGlobal._ComboSeleccionar });

            return Json(loProvinciaList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetDistritos(int idProvincia)
        {
            tbDistritoBLL lotbDistritoBLL = new tbDistritoBLL();
            var loDistritosList = lotbDistritoBLL.GetBycnID_Provincia(idProvincia);

            //loDistritosList.Insert(0, new SalarDb.CodeGen.Model.tbDistritoModel() { ctDistrito = BaseGlobal._ComboSeleccionar });

            return Json(loDistritosList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetCoberturas(long idProceso)
        {
            tbProcesoCoberturaBLL lotbProcesoCoberturaBLL = new tbProcesoCoberturaBLL();
            var loCoberturasList = lotbProcesoCoberturaBLL.GetBycnID_Proceso(idProceso);

            loCoberturasList.Insert(0, new SalarDb.CodeGen.Model.tbProcesoCoberturaModel() { cnID_Cobertura = 0, ctNombre = "Todos" });

            return Json(loCoberturasList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetSituacion(int idGrado)
        {
            var loSituacionList = BaseGlobal.GetSituacion(idGrado);
            return Json(loSituacionList, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetEmails(int tipoEmail)
        {
            tbMailBLL lotbMaildBLL = new tbMailBLL();
            var loEmails = lotbMaildBLL.GetBycnID_CandidatoList(BaseGlobal.CandidatoActual.cnID_Candidato).FindAll(x => x.cnID_TipoEmail == tipoEmail);
            return Json(loEmails, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetItemByListID(long cnID_Lista)
        {
            tbMaestroListasBLL lotbMaestroListasBLL = new tbMaestroListasBLL();
            var lista = lotbMaestroListasBLL.GetBycnID_Lista(cnID_Lista);
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ValidarDocumento(int cnID_Candidato, int tipDoc, string nroDoc)
        {
            tbDocIdentidadBLL lotbDocIdentidadBLL = new tbDocIdentidadBLL();
            var docs = lotbDocIdentidadBLL.GetByctNumero(nroDoc);
            if (docs.Find(x => x.ctNumero.Trim() == nroDoc.Trim() && x.cnID_TipoDocumento == tipDoc && x.cnID_Candidato != cnID_Candidato) != null)
                return Json(false, JsonRequestBehavior.AllowGet);

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ValidarEmailTipo(int cnID_Candidato, int tipDoc, string mail)
        {
            tbCandidatoBLL lotbCandidatoBLL = new tbCandidatoBLL();
            var emails = lotbCandidatoBLL.GetByctEmail(mail);
            if (emails != null)
            {
                if (emails.cnID_Candidato != cnID_Candidato)
                    return Json(false, JsonRequestBehavior.AllowGet);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ValidarEmail(int cnID_Candidato, string mail)
        {
            tbCandidatoBLL lotbCandidatoBLL = new tbCandidatoBLL();
            var emails = lotbCandidatoBLL.GetByctEmail(mail);
            if (emails != null)
            {
                if (emails.cnID_Candidato != cnID_Candidato)
                    return Json(false, JsonRequestBehavior.AllowGet);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetCoberturaZonas(int idCobertura, bool? addTodos)
        {
            tbCoberturaZonaBLL lotbCoberturaZonaBLL = new tbCoberturaZonaBLL();
            List<tbCoberturaZonaModel> lotbCoberturaZonaModel = new List<tbCoberturaZonaModel>();
            lotbCoberturaZonaModel = lotbCoberturaZonaBLL.GetBycnID_Cobertura(idCobertura);

            if (addTodos.HasValue)
                if (addTodos.Value)
                {
                    lotbCoberturaZonaModel.Insert(0, new SalarDb.CodeGen.Model.tbCoberturaZonaModel() { cnID_CoberturaZona = 0, ctZona = "Todos" });
                }

            return Json(lotbCoberturaZonaModel, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetAreaTematicaNivel(int nivel, int id, bool last)
        {
            tbMaestroListasBLL lotbMaestroListasBLL = new tbMaestroListasBLL();
            List<tbMaestroListasModel> list = new List<tbMaestroListasModel>();
            if (!last)
                list = lotbMaestroListasBLL.GetBycnID_ListaPadre(id);
            else if (last)
                list = lotbMaestroListasBLL.GetAreaTematicaNivelUltimo(id, BaseGlobal.CandidatoActual.cnID_Candidato);
                //list = lotbMaestroListasBLL.GetBycnID_ListaPadre(id);


            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetAreaTematicaNivelPadre(int group)
        {
            tbMaestroListasBLL lotbMaestroListasBLL = new tbMaestroListasBLL();

            List<tbMaestroListasModel> list = new List<tbMaestroListasModel>();

            List<SelectListItem> listaSelect = new List<SelectListItem>();
            tbMaestroGruposBLL lotbMaestroGruposBLL = new tbMaestroGruposBLL();
            var objGrupo = lotbMaestroGruposBLL.GetBycnID_GrupoLista(group);
            //if (objGrupo.cnID_GrupoLista == (int)mvcSeleccion.BaseGlobal.GruposListas.AreaTematicaNivel)
            {
                //tbMaestroGruposBLL lotbMaestroGruposBLL = new tbMaestroGruposBLL();
                //var grupo = lotbMaestroGruposBLL.GetBycnID_GrupoLista(objGrupo.cnID_GrupoLista);
                if (objGrupo.cnGrupoPadre != 0)//== (int)mvcSeleccion.BaseGlobal.GruposListas.AreaTematicaNivel)
                {
                    list = lotbMaestroListasBLL.GetBycnID_GrupoLista(objGrupo.cnGrupoPadre);
                }
            }
            //if ((int)mvcSeleccion.BaseGlobal.GruposListas.AreaTematicaNivel1 != group)
            //{
            //    if ((int)mvcSeleccion.BaseGlobal.GruposListas.AreaTematicaNivel2 == group)
            //        list = lotbMaestroListasBLL.GetBycnID_GrupoLista((int)mvcSeleccion.BaseGlobal.GruposListas.AreaTematicaNivel1);
            //    if ((int)mvcSeleccion.BaseGlobal.GruposListas.AreaTematicaNivel3 == group)
            //        list = lotbMaestroListasBLL.GetBycnID_GrupoLista((int)mvcSeleccion.BaseGlobal.GruposListas.AreaTematicaNivel2);
            //}

            listaSelect.AddRange((from x in list select new SelectListItem() { Text = x.ctElemento, Value = x.cnID_Lista.ToString() }).ToList());

            return Json(listaSelect, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetMaestroListas(int group, int grupopadre)
        {
            tbMaestroListasBLL lotbMaestroListasBLL = new tbMaestroListasBLL();
            List<tbMaestroListasModel> lista = new List<tbMaestroListasModel>();
            if (group == (int)mvcSeleccion.BaseGlobal.GruposListas.AreaTematicaNivel)
            {

            }
            else
            {
                lista = lotbMaestroListasBLL.GetBycnID_GrupoLista(group);
            }
            ViewBag.GrupoPadre = grupopadre;
            return PartialView("MaestroListas", lista);
        }

        [HttpGet]
        public ActionResult GetListaAreaTematica()
        {
            var listaGrupos = BaseGlobal.GetMaestroGrupoList("TEMATICA");
            return Json(listaGrupos, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        public ActionResult GetListByGroupId(int group)
        {
            var list = BaseGlobal.GetMaestroList((BaseGlobal.GruposListas)Enum.Parse(typeof(BaseGlobal.GruposListas), group.ToString()), false);
            //list.Insert(1, new tbMaestroListasModel() { ctElemento = "Área Temática",  });
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetMaestroGrupoList(string tipo)
        {
            var list = BaseGlobal.GetMaestroGrupoList(tipo);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
    }
}