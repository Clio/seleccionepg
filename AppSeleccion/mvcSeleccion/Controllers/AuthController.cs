﻿using SalarDb.CodeGen.BLL;
using SalarDb.CodeGen.Model;
using System;
using System.Web.Mvc;

namespace mvcSeleccion.Controllers
{
    public class AuthController : Controller
    {
        // GET: Auth
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ValidUser(string user, string pwd)
        {
            try
            {
                if (!BaseUtil.mtValidarDireccionCorreo(user)) //Evaluador
                {
                    tbUsuarioBLL lotbUsuarioBLL = new tbUsuarioBLL();
                    tbUsuarioModel lotbUsuarioModel = new tbUsuarioModel();

                    lotbUsuarioModel = lotbUsuarioBLL.GetByctLogin(user);
                    if (lotbUsuarioModel != null)
                    {
                        string passw = BaseUtil.EncryptPassword(pwd);

                        if (lotbUsuarioModel.ctClave == passw)//Usuario autenticado
                        {
                            BaseGlobal.UsuarioActual = lotbUsuarioModel;
                            BaseGlobal.UsuarioActual.cnID_Perfil = (int)BaseGlobal.Perfiles.Evaluador;

                            string result = "OK";
                            if (passw == BaseUtil.EncryptPassword(BaseUtil.GetPasswordDefault()))
                                result = "OKA";

                            return Json(result);
                        }
                    }
                }
                else//Usuario
                {
                    tbCandidatoModel lotbCandidatoModel = new tbCandidatoModel();
                    tbCandidatoBLL lotbCandidatoBLL = new tbCandidatoBLL();

                    lotbCandidatoModel = lotbCandidatoBLL.GetByctEmail(user);

                    if (lotbCandidatoModel.ctClaveAcceso == BaseUtil.EncryptPassword(pwd))//Usuario autenticado
                    {
                        BaseGlobal.UsuarioActual = new tbUsuarioModel() { ctNombres = lotbCandidatoModel.ctNombres, ctApellidos = lotbCandidatoModel.ctApellidoPaterno + " " + lotbCandidatoModel.ctApellidoMaterno };

                        BaseGlobal.UsuarioActual.cnID_Perfil = (int)BaseGlobal.Perfiles.Candidato;
                        BaseGlobal.CandidatoActual = lotbCandidatoModel;
                        return Json("OK");
                    }
                }
            }
            catch (Exception ex)
            {
                //Enviar mensaje al usuario
            }

            return Json("KO");
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            BaseGlobal.UsuarioActual = null;
            return RedirectToAction("Login");
        }

        public ActionResult Welcome()
        {
            return View();
        }

        public ActionResult Recuperar()
        {
            return View();
        }

        public JsonResult RecoverPwd(string user)
        {          
            

            tbCandidatoModel lotbCandidatoModel = new tbCandidatoModel();
            tbCandidatoBLL lotbCandidatoBLL = new tbCandidatoBLL();

            lotbCandidatoModel = lotbCandidatoBLL.GetByctEmail(user);


            if (lotbCandidatoModel != null)
            {
                Guid guid = Guid.NewGuid();
                lotbCandidatoModel.cnCodigoRecuperacion = guid;
                lotbCandidatoBLL.Update(lotbCandidatoModel);

                string mensajeCorreo = "Ingrese al siguiente enlace para recuperar el acceso a su cuenta: ";
                mensajeCorreo += "<br/>";

                string url = BaseUtil.ToAbsoluteUrl("~/Auth/ResetPwd?code=" + guid.ToString());

                mensajeCorreo += string.Format("<a href='{0}'>{0}</a>", url);

                if (BaseEnvioCorreo.SendMail("Pensión 65", "Recuperación de contraseña", mensajeCorreo, user))
                    return Json("OK");
            }

            return Json("KO");
        }

        public ActionResult ResetPwd(string code)
        {
            return View();
        }

        [HttpPost]
        public ActionResult ResetPassw(string code, string pwd)
        {

            tbCandidatoModel lotbCandidatoModel = new tbCandidatoModel();
            tbCandidatoBLL lotbCandidatoBLL = new tbCandidatoBLL();


            lotbCandidatoModel = lotbCandidatoBLL.GetByctCodigoRecuperacion(code);
            if (lotbCandidatoModel != null)
            {
                lotbCandidatoModel.ctClaveAcceso = BaseUtil.EncryptPassword(pwd);
                lotbCandidatoModel.cnCodigoRecuperacion = null;

                lotbCandidatoBLL.Update(lotbCandidatoModel);

                return Json("OK");
            }

            return Json("KO");
        }
    }
}