﻿using SalarDb.CodeGen.BLL;
using SalarDb.CodeGen.Common;
using SalarDb.CodeGen.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using mvcSeleccion.Globals;
using OfficeOpenXml;

namespace mvcSeleccion.Controllers
{
    [OutputCacheAttribute(VaryByParam = "*", Duration = 0, NoStore = true)]
    public class ConvocatoriaController : WebControllerBase
    {
        // GET: Convocatoria
        public ActionResult Index(string a = null, int? i = null)
        {
            if (!BaseGlobal.IsLogged)
                return this.RedirectToLogin();

            List<tbProcesoModel> lotbProcesoModelList = new List<tbProcesoModel>();
            tbProcesoBLL lotbProcesoBLL = new tbProcesoBLL();

            lotbProcesoModelList = lotbProcesoBLL.GetAll();

            //--------------------
            ViewBag.action = a;
            ViewBag.id = i;
            //--------------------

            return View(lotbProcesoModelList);
        }

        public ActionResult Detalle(string a = null, long? i = null)
        {
            if (!BaseGlobal.IsLogged)
                return this.RedirectToLogin();

            tbProcesoModel lotbProcesoModel = new tbProcesoModel();
            tbProcesoBLL lotbProcesoBLL = new tbProcesoBLL();

            lotbProcesoModel.cfFechaFin = DateTime.Now;
            lotbProcesoModel.cfFechaInicio = DateTime.Now;

            ViewBag.action = a;

            if (i > 0)
            {
                lotbProcesoModel = lotbProcesoBLL.GetBycnID_Proceso(i.Value);

                tbProcesoCoberturaBLL lotbProcesoCoberturaBLL = new tbProcesoCoberturaBLL();
                lotbProcesoModel.tbProcesoCoberturaList = lotbProcesoCoberturaBLL.GetBycnID_Proceso(i.Value);

                tbProcesoCnfgAcadBLL lotbProcesoCnfgAcadBLL = new tbProcesoCnfgAcadBLL();
                lotbProcesoModel.tbProcesoCnfgAcadModelList = lotbProcesoCnfgAcadBLL.GetBycnID_Proceso(i.Value);

                tbProcesoCnfgLaboBLL lotbProcesoCnfgLaboBLL = new tbProcesoCnfgLaboBLL();
                var confLabo = lotbProcesoCnfgLaboBLL.GetBycnID_Proceso(i.Value);
                if (confLabo.Count > 0)
                    lotbProcesoModel.tbProcesoCnfgLabo = confLabo.First();
                else
                    lotbProcesoModel.tbProcesoCnfgLabo = new tbProcesoCnfgLaboModel();

                tbProcesoCriterioAdicBLL lotbProcesoCriterioAdicBLL = new tbProcesoCriterioAdicBLL();
                lotbProcesoModel.tbProcesoCriterioAdicModelList = lotbProcesoCriterioAdicBLL.GetBycnID_Proceso(i.Value);

                tbProcesoDocumentoBLL LOtbProcesoDocumentoBLL = new tbProcesoDocumentoBLL();
                lotbProcesoModel.tbProcesoDocumentoModelList = LOtbProcesoDocumentoBLL.GetBycnID_Proceso(i.Value);
            }

            lotbProcesoModel.tbProcesoCoberturaList = (lotbProcesoModel.tbProcesoCoberturaList == null) ? new List<tbProcesoCoberturaModel>() : lotbProcesoModel.tbProcesoCoberturaList;
            if (lotbProcesoModel.tbProcesoCoberturaList.Count == 0)
                lotbProcesoModel.tbProcesoCoberturaList.Add(new tbProcesoCoberturaModel());

            lotbProcesoModel.tbProcesoCnfgAcadModelList = (lotbProcesoModel.tbProcesoCnfgAcadModelList == null) ? new List<tbProcesoCnfgAcadModel>() : lotbProcesoModel.tbProcesoCnfgAcadModelList;
            if (lotbProcesoModel.tbProcesoCnfgAcadModelList.Count == 0)
                lotbProcesoModel.tbProcesoCnfgAcadModelList.Add(new tbProcesoCnfgAcadModel());

            if (lotbProcesoModel.tbProcesoCnfgLabo == null)
                lotbProcesoModel.tbProcesoCnfgLabo = new tbProcesoCnfgLaboModel();

            lotbProcesoModel.tbProcesoCriterioAdicModelList = (lotbProcesoModel.tbProcesoCriterioAdicModelList == null) ? new List<tbProcesoCriterioAdicModel>() : lotbProcesoModel.tbProcesoCriterioAdicModelList;
            //if (lotbProcesoModel.tbProcesoCriterioAdicModelList.Count == 0)
            //    lotbProcesoModel.tbProcesoCriterioAdicModelList.Add(new tbProcesoCriterioAdicModel());

            lotbProcesoModel.tbProcesoDocumentoModelList = (lotbProcesoModel.tbProcesoDocumentoModelList == null) ? new List<tbProcesoDocumentoModel>() : lotbProcesoModel.tbProcesoDocumentoModelList;
            //if (lotbProcesoModel.tbProcesoDocumentoModelList.Count == 0)
            //    lotbProcesoModel.tbProcesoDocumentoModelList.Add(new tbProcesoDocumentoModel());

            ViewBag.Grados = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.Grado);
            ViewBag.Situaciones = new List<SelectListItem>();
            ViewBag.TipoEntidadListado = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.TipoEntidadConvocatoria);
            ViewBag.EstadosProcesoListado = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.EstadoProceso);

            return View(lotbProcesoModel);
        }

        [HttpGet]
        public ActionResult CriterioLaboralListado(long id)
        {
            tbProcesoCnfgLaboBLL lotbProcesoCnfgLaboBLL = new tbProcesoCnfgLaboBLL();
            List<tbProcesoCnfgLaboModel> tbProcesoCnfgLaboModelList = lotbProcesoCnfgLaboBLL.GetBycnID_Proceso(id);
            return PartialView("ConfigLaboralListado", tbProcesoCnfgLaboModelList);
        }

        [HttpGet]
        public PartialViewResult CriterioLaboralMatriz(int? tipoCriterio, int id, int confLaboral)
        {
            tbProcesoMatrizLaboModel lotbProcesoMatrizLaboModel = new tbProcesoMatrizLaboModel();
            lotbProcesoMatrizLaboModel.tbProcesoMatrizLaboModelList = new List<tbProcesoMatrizLaboModel>();
            lotbProcesoMatrizLaboModel.tbProcesoMatrizLaboModelList.Add(new tbProcesoMatrizLaboModel());

            ViewBag.TipoEvaluacionListado = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.TipoEvaluacion, false);
            ViewBag.TipoCriterioLaboralListado = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.TipoCriterioLaboral);

            //if (tipoCriterio.HasValue)
            //    lotbProcesoMatrizLaboModel.cnID_TipoCriterioLabo = tipoCriterio.Value;

            if (id > 0)
            {
                tbProcesoMatrizLaboBLL lotbProcesoMatrizLaboBLL = new tbProcesoMatrizLaboBLL();
                lotbProcesoMatrizLaboModel = lotbProcesoMatrizLaboBLL.GetBycnID_MatrizLaboral(id);
            }
            lotbProcesoMatrizLaboModel.cnID_CnfgLaboral = confLaboral;

            tbProcesoCnfgLaboBLL lotbProcesoCnfgLaboBLL = new tbProcesoCnfgLaboBLL();
            tbProcesoCnfgLaboModel tbProcesoCnfgLaboModelTmp = new tbProcesoCnfgLaboModel();

            if (confLaboral > 0)
                tbProcesoCnfgLaboModelTmp = lotbProcesoCnfgLaboBLL.GetBycnID_CnfgLaboral(confLaboral);

            if (tipoCriterio.HasValue)
                tbProcesoCnfgLaboModelTmp.cnID_TipoCriterioLabo = tipoCriterio.Value;

            ViewBag.tbProcesoCnfgLaboModel = tbProcesoCnfgLaboModelTmp;

            return PartialView("_CriterioLaboralMatriz", lotbProcesoMatrizLaboModel);
        }

        [HttpGet]
        public PartialViewResult MatrizLaboralListado(long id, int tipoCriterio = 0, int confLaboral = 0)
        {
            List<tbProcesoMatrizLaboModel> lotbProcesoMatrizLaboModelList = new List<tbProcesoMatrizLaboModel>();
            tbProcesoMatrizLaboBLL lotbProcesoMatrizLaboBLL = new tbProcesoMatrizLaboBLL();
            lotbProcesoMatrizLaboModelList = lotbProcesoMatrizLaboBLL.GetBycnID_Proceso(id);
            ViewBag.TipoCriterio = tipoCriterio;

            lotbProcesoMatrizLaboModelList.ForEach(x =>
            {
                tbProcesoCnfgLaboBLL lotbProcesoCnfgLaboBLL = new tbProcesoCnfgLaboBLL();
                x.tbProcesoCnfgLaboModel = lotbProcesoCnfgLaboBLL.GetBycnID_CnfgLaboral(x.cnID_CnfgLaboral);
            });

            return PartialView("_MatrizLaboralListado", lotbProcesoMatrizLaboModelList);
        }

        [HttpPost]
        public ActionResult SaveMatriz(int cnID_MatrizLaboral, int cnID_CnfgLaboral, Int64 cnID_Proceso, Int64 cnID_TipoEntidad, /*Int64 cnID_TipoEvaluacion, */int cnMininmo, int cnMaximo, int cnPuntaje, string ctDescripcion, Int32 cnID_TipoCriterioLabo, string model = null)
        {
            string message = string.Empty;
            try
            {
                tbProcesoMatrizLaboModel lotbProcesoMatrizLaboModel = new tbProcesoMatrizLaboModel();
                lotbProcesoMatrizLaboModel.cnID_MatrizLaboral = cnID_MatrizLaboral;

                //lotbProcesoMatrizLaboModel.cnID_Proceso = cnID_Proceso;
                //lotbProcesoMatrizLaboModel.cnID_TipoEvaluacion = cnID_TipoEvaluacion;
                lotbProcesoMatrizLaboModel.cnMininmo = cnMininmo;
                lotbProcesoMatrizLaboModel.cnMaximo = cnMaximo;
                lotbProcesoMatrizLaboModel.cnPuntaje = cnPuntaje;
                //lotbProcesoMatrizLaboModel.ctDescripcion = ctDescripcion;
                //lotbProcesoMatrizLaboModel.cnID_TipoCriterioLabo = cnID_TipoCriterioLabo;

                IDbConnection con = dbSeleccionDbConnection.GetNewConnection();
                dbSeleccionDbTransaction tran = dbSeleccionDbTransaction.BeginTransaction();
                tran.Connection = con;
                try
                {
                    tbProcesoBLL lotbProcesoBLL = new tbProcesoBLL();
                    tbProcesoMatrizLaboBLL lotbProcesoMatrizLaboBLL = new tbProcesoMatrizLaboBLL();
                    tbProcesoCnfgLaboBLL lotbProcesoCnfgLaboBLL = new tbProcesoCnfgLaboBLL();

                    lotbProcesoMatrizLaboBLL.Transaction = tran;
                    lotbProcesoMatrizLaboBLL.Transaction.Connection = con;

                    lotbProcesoCnfgLaboBLL.Transaction = tran;
                    lotbProcesoCnfgLaboBLL.Transaction.Connection = con;

                    lotbProcesoBLL.Transaction = tran;
                    lotbProcesoBLL.Transaction.Connection = con;

                    if (cnID_Proceso == 0)
                    {
                        tbProcesoModel lotbProcesoModel = new tbProcesoModel();
                        cnID_Proceso = lotbProcesoBLL.Insert(lotbProcesoModel);
                    }

                    tbProcesoCnfgLaboModel lotbProcesoCnfgLaboModel = new tbProcesoCnfgLaboModel();

                    if (cnID_CnfgLaboral > 0)
                        lotbProcesoCnfgLaboModel = lotbProcesoCnfgLaboBLL.GetBycnID_CnfgLaboral(cnID_CnfgLaboral);

                    lotbProcesoCnfgLaboModel.ctDescripcion = ctDescripcion;
                    lotbProcesoCnfgLaboModel.cnID_Proceso = cnID_Proceso;
                    lotbProcesoCnfgLaboModel.cnID_TipoEntidad = cnID_TipoEntidad;
                    lotbProcesoCnfgLaboModel.cnID_TipoCriterioLabo = cnID_TipoCriterioLabo;
                    lotbProcesoCnfgLaboModel.cnID_TipoEvaluacion = BaseGlobal.TipoEvaluacionAños;

                    if (cnID_CnfgLaboral == 0)
                    {
                        cnID_CnfgLaboral = lotbProcesoCnfgLaboBLL.Insert(lotbProcesoCnfgLaboModel);
                    }
                    else
                    {
                        lotbProcesoCnfgLaboBLL.Update(lotbProcesoCnfgLaboModel);
                    }

                    lotbProcesoMatrizLaboModel.cnID_CnfgLaboral = cnID_CnfgLaboral;

                    if (this.mtEsMatrizValida(cnID_MatrizLaboral, lotbProcesoMatrizLaboModel.cnID_CnfgLaboral, cnMininmo, cnMaximo, out message))
                    {
                        if (cnID_MatrizLaboral == 0)
                            lotbProcesoMatrizLaboBLL.Insert(lotbProcesoMatrizLaboModel);
                        else
                            lotbProcesoMatrizLaboBLL.Update(lotbProcesoMatrizLaboModel);

                        tran.Commit();

                        return Json(new { result = "OK", cnID_CnfgLaboral = cnID_CnfgLaboral }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        tran.Rollback();
                }
                catch
                {
                    tran.Rollback();
                }
            }
            catch
            {
            }

            return Json(new { result = "KO", message = message }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveMatrizDetalle(int cnID_MatrizLaboral,/* Int64 cnID_TipoEvaluacion, */int cnMininmo, int cnMaximo, int cnPuntaje)
        {
            string message = string.Empty;
            try
            {
                tbProcesoMatrizLaboBLL lotbProcesoMatrizLaboBLL = new tbProcesoMatrizLaboBLL();
                tbProcesoMatrizLaboModel lotbProcesoMatrizLaboModel = new tbProcesoMatrizLaboModel();
                lotbProcesoMatrizLaboModel = lotbProcesoMatrizLaboBLL.GetBycnID_MatrizLaboral(cnID_MatrizLaboral);

                if (this.mtEsMatrizValida(cnID_MatrizLaboral, lotbProcesoMatrizLaboModel.cnID_CnfgLaboral, cnMininmo, cnMaximo, out message))
                {
                    try
                    {
                        //lotbProcesoMatrizLaboModel.cnID_TipoEvaluacion = cnID_TipoEvaluacion;
                        lotbProcesoMatrizLaboModel.cnMininmo = cnMininmo;
                        lotbProcesoMatrizLaboModel.cnMaximo = cnMaximo;
                        lotbProcesoMatrizLaboModel.cnPuntaje = cnPuntaje;
                        lotbProcesoMatrizLaboBLL.Update(lotbProcesoMatrizLaboModel);

                        return Json(new { result = "OK", message = message }, JsonRequestBehavior.AllowGet);
                    }
                    catch
                    {
                    }
                }
            }
            catch
            {
            }

            return Json(new { result = "KO", message = message }, JsonRequestBehavior.AllowGet);
        }

        private bool mtEsMatrizValida(int cnID_MatrizLaboral, int cnID_CnfgLaboral, int cnMininmo, int cnMaximo, out string message)
        {
            message = string.Empty;

            if (cnID_CnfgLaboral == 0)
                return true;

            tbProcesoMatrizLaboBLL lotbProcesoMatrizLaboBLL = new tbProcesoMatrizLaboBLL();
            var loMatrizProcesoList = lotbProcesoMatrizLaboBLL.GetBycnID_CnfgLaboral(cnID_CnfgLaboral);
            if (loMatrizProcesoList.Find(x => x.cnMaximo == cnMininmo && x.cnID_MatrizLaboral != cnID_MatrizLaboral) != null)
            {
                message = "El límite mínimo debe ser mayor a uno de los límites máximos ingresados.";
            }
            else if (loMatrizProcesoList.Find(x => x.cnMininmo == cnMaximo && x.cnID_MatrizLaboral != cnID_MatrizLaboral) != null)
            {
                message = "El límite máximo debe ser menor a uno de los límites mínimos ingresados.";
            }

            return message.Trim().Length == 0;
        }

        [HttpPost]
        public ActionResult SaveConfiguracionLaboral(int cnID_MatrizLaboral, int cnID_CnfgLaboral, Int64 cnID_Proceso, Int64 cnID_TipoEntidad, string ctDescripcion, Int32 cnID_TipoCriterioLabo, string model = null)
        {
            try
            {
                IDbConnection con = dbSeleccionDbConnection.GetNewConnection();
                dbSeleccionDbTransaction tran = dbSeleccionDbTransaction.BeginTransaction();
                tran.Connection = con;
                try
                {
                    tbProcesoBLL lotbProcesoBLL = new tbProcesoBLL();
                    tbProcesoMatrizLaboBLL lotbProcesoMatrizLaboBLL = new tbProcesoMatrizLaboBLL();
                    tbProcesoCnfgLaboBLL lotbProcesoCnfgLaboBLL = new tbProcesoCnfgLaboBLL();

                    lotbProcesoBLL.Transaction = tran;
                    lotbProcesoBLL.Transaction.Connection = con;

                    lotbProcesoMatrizLaboBLL.Transaction = tran;
                    lotbProcesoMatrizLaboBLL.Transaction.Connection = con;

                    lotbProcesoCnfgLaboBLL.Transaction = tran;
                    lotbProcesoCnfgLaboBLL.Transaction.Connection = con;

                    if (cnID_Proceso == 0)
                    {
                        tbProcesoModel lotbProcesoModel = new tbProcesoModel();
                        //cnID_Proceso = lotbProcesoBLL.Insert(lotbProcesoModel);
                    }

                    tbProcesoCnfgLaboModel lotbProcesoCnfgLaboModel = new tbProcesoCnfgLaboModel();

                    if (cnID_CnfgLaboral > 0)
                        lotbProcesoCnfgLaboModel = lotbProcesoCnfgLaboBLL.GetBycnID_CnfgLaboral(cnID_CnfgLaboral);

                    lotbProcesoCnfgLaboModel.ctDescripcion = ctDescripcion;
                    lotbProcesoCnfgLaboModel.cnID_Proceso = cnID_Proceso;
                    lotbProcesoCnfgLaboModel.cnID_TipoEntidad = cnID_TipoEntidad;
                    lotbProcesoCnfgLaboModel.cnID_TipoCriterioLabo = cnID_TipoCriterioLabo;

                    if (cnID_CnfgLaboral == 0)
                    {
                        cnID_CnfgLaboral = lotbProcesoCnfgLaboBLL.Insert(lotbProcesoCnfgLaboModel);
                    }
                    else
                    {
                        lotbProcesoCnfgLaboBLL.Update(lotbProcesoCnfgLaboModel);
                    }

                    tran.Commit();

                    return Json(new { result = "OK", cnID_CnfgLaboral = cnID_CnfgLaboral, cnID_Proceso = cnID_Proceso }, JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    tran.Rollback();
                }
            }
            catch
            {
            }

            return Json(new { result = "KO" }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult EditMatrizItem(int id)
        {
            tbProcesoMatrizLaboBLL lotbProcesoMatrizLaboBLL = new tbProcesoMatrizLaboBLL();
            tbProcesoMatrizLaboModel lotbProcesoMatrizLaboModel = lotbProcesoMatrizLaboBLL.GetBycnID_MatrizLaboral(id);

            ViewBag.TipoEvaluacionListado = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.TipoEvaluacion, false);

            return PartialView("_EditMatrizItem", lotbProcesoMatrizLaboModel);
        }

        [HttpPost]
        public ActionResult DeleteMatrizLabo(int id)
        {
            try
            {
                tbProcesoMatrizLaboBLL lotbProcesoMatrizLaboBLL = new tbProcesoMatrizLaboBLL();
                lotbProcesoMatrizLaboBLL.Delete(id);

                return Json("OK");
            }
            catch
            {
                return Json("KO");
            }
        }

        [HttpPost]
        public ActionResult DeleteCobertura(int id)
        {
            try
            {
                tbProcesoCoberturaBLL lotbProcesoCoberturaBLL = new tbProcesoCoberturaBLL();
                lotbProcesoCoberturaBLL.Delete(id);

                return Json("OK");
            }
            catch
            {
                return Json("KO");
            }
        }

        [HttpPost]
        public ActionResult DeleteAcademico(int id)
        {
            try
            {
                tbProcesoCnfgAcadBLL lotbProcesoCnfgAcadBLL = new tbProcesoCnfgAcadBLL();
                lotbProcesoCnfgAcadBLL.Delete(id);

                return Json("OK");
            }
            catch
            {
                return Json("KO");
            }
        }

        [HttpPost]
        public ActionResult DeleteAdicional(int id)
        {
            try
            {
                tbProcesoCriterioAdicBLL lotbProcesoCriterioAdicBLL = new tbProcesoCriterioAdicBLL();
                lotbProcesoCriterioAdicBLL.Delete(id);

                return Json("OK");
            }
            catch
            {
                return Json("KO");
            }
        }

        [HttpPost]
        public ActionResult DeleteDocumento(int id)
        {
            try
            {
                tbProcesoDocumentoBLL lotbProcesoDocumentoBLL = new tbProcesoDocumentoBLL();
                lotbProcesoDocumentoBLL.Delete(id);

                return Json("OK");
            }
            catch
            {
                return Json("KO");
            }
        }

        [HttpPost]
        public ActionResult SaveProceso(tbProcesoModel model, HttpPostedFileBase txtArchivoTerminos, HttpPostedFileBase txtArchivoProceso,
            int[] tbProcesoCoberturaList_cnID_Cobertura, string[] tbProcesoCoberturaList_ctNombre, string[] tbProcesoCoberturaList_ctCobertura,
            int[] tbProcesoCnfgAcadModelList_cnID_CnfgAcademica, int[] tbProcesoCnfgAcadModelList_cnID_Grado, int[] tbProcesoCnfgAcadModelList_cnID_Situacion, int[] tbProcesoCnfgAcadModelList_cnPuntaje,
            int[] tbProcesoCriterioAdicModelList_cnID_CriterioAdicional, string[] tbProcesoCriterioAdicModelList_ctDescripcion, int[] tbProcesoCriterioAdicModelList_cnPuntaje,
            int[] tbProcesoDocumentoModelList_cnID_Documento, string[] tbProcesoDocumentoModelList_ctDescripcion, bool[] tbProcesoDocumentoModelList_cnIndObligatorio
            )
        {
            bool isNewProceso = (model.cnID_Proceso == 0);
            int cnTipoEntidad = 0;
            if (Request.Form["cnID_TipoEntidad"] != null)
                cnTipoEntidad = int.Parse(Request.Form["cnID_TipoEntidad"].ToString()); ;

            long id = 0;
            string[] archivos = new string[2];
            if (model.cnID_Proceso > 0)
            {
                tbProcesoBLL lotbProcesoBLLTmp = new tbProcesoBLL();
                tbProcesoModel lotbProcesoModelTmp = lotbProcesoBLLTmp.GetBycnID_Proceso(model.cnID_Proceso);
                archivos[0] = lotbProcesoModelTmp.ctArchivoTerminos;
                archivos[1] = lotbProcesoModelTmp.ctArchivoProceso;

                model.ctArchivoTerminos = lotbProcesoModelTmp.ctArchivoTerminos;
                model.ctArchivoProceso = lotbProcesoModelTmp.ctArchivoProceso;
            }


            string fileName = string.Empty;
            bool newArchivoTerminos = false;
            bool newArchivoProceso = false;

            if (txtArchivoTerminos != null && txtArchivoTerminos.ContentLength > 0)
            {
                if (BaseUtil.SaveFile(txtArchivoTerminos, BaseVars.CARPETA_PROCESOS, out fileName))
                {
                    model.ctArchivoTerminos = fileName;
                    newArchivoTerminos = true;
                }
            }

            if (txtArchivoProceso != null && txtArchivoProceso.ContentLength > 0)
            {
                if (BaseUtil.SaveFile(txtArchivoProceso, BaseVars.CARPETA_PROCESOS, out fileName))
                {
                    model.ctArchivoProceso = fileName;
                    newArchivoProceso = true;
                }
            }

            IDbConnection con = dbSeleccionDbConnection.GetNewConnection();
            dbSeleccionDbTransaction tran = dbSeleccionDbTransaction.BeginTransaction();
            tran.Connection = con;

            try
            {
                tbProcesoBLL lotbProcesoBLL = new tbProcesoBLL();
                lotbProcesoBLL.Transaction = tran;
                lotbProcesoBLL.Transaction.Connection = con;

                model.cnIndEvaluacion = true;
                //model.cnID_Tipo = 246;//Tipo de Proceso

                model.cfFechaCreacion = DateTime.Now;
                model.cfFechaActualizacion = DateTime.Now;
                model.ctUsuarioCreador = BaseGlobal.UsuarioActual.ctLogin;
                model.ctIPAcceso = BaseUtil.GetIPAddress();

                if (model.cnID_Proceso == 0)
                    id = lotbProcesoBLL.Insert(model);
                else
                {
                    id = model.cnID_Proceso;
                    lotbProcesoBLL.Update(model);
                }

                int index = 0;
                List<tbProcesoCoberturaModel> tbProcesoCoberturaList = new List<tbProcesoCoberturaModel>();
                if (tbProcesoCoberturaList_ctNombre != null)
                {
                    tbProcesoCoberturaList_ctNombre.ToList().ForEach(nombre =>
                    {
                        tbProcesoCoberturaList.Add(new tbProcesoCoberturaModel() { cnID_Cobertura = tbProcesoCoberturaList_cnID_Cobertura[index], cnID_Proceso = id, ctNombre = nombre });
                        index++;
                    });
                }

                index = 0;

                List<tbProcesoCnfgAcadModel> tbProcesoCnfgAcadModelList = new List<tbProcesoCnfgAcadModel>();

                if (tbProcesoCnfgAcadModelList_cnID_Grado != null)
                {
                    tbProcesoCnfgAcadModelList_cnID_Grado.ToList().ForEach(grado =>
                    {
                        tbProcesoCnfgAcadModelList.Add(new tbProcesoCnfgAcadModel() { cnID_CnfgAcademica = tbProcesoCnfgAcadModelList_cnID_CnfgAcademica[index], cnID_Proceso = id, cnID_Grado = grado, cnID_Situacion = tbProcesoCnfgAcadModelList_cnID_Situacion[index], cnPuntaje = tbProcesoCnfgAcadModelList_cnPuntaje[index] });
                        index++;
                    });
                }

                index = 0;
                List<tbProcesoCriterioAdicModel> tbProcesoCriterioAdicModelList = new List<tbProcesoCriterioAdicModel>();
                if (tbProcesoCriterioAdicModelList_ctDescripcion != null)
                {
                    tbProcesoCriterioAdicModelList_ctDescripcion.ToList().ForEach(desc =>
                    {
                        tbProcesoCriterioAdicModelList.Add(new tbProcesoCriterioAdicModel() { cnID_CriterioAdicional = tbProcesoCriterioAdicModelList_cnID_CriterioAdicional[index], ctDescripcion = desc, cnID_Proceso = id, cnPuntaje = tbProcesoCriterioAdicModelList_cnPuntaje[index] });
                        index++;
                    });
                }

                index = 0;
                List<tbProcesoDocumentoModel> tbProcesoDocumentoModelList = new List<tbProcesoDocumentoModel>();
                if (tbProcesoDocumentoModelList_ctDescripcion != null)
                {
                    tbProcesoDocumentoModelList_ctDescripcion.ToList().ForEach(docum =>
                    {
                        tbProcesoDocumentoModelList.Add(new tbProcesoDocumentoModel() { cnID_Documento = tbProcesoDocumentoModelList_cnID_Documento[index], cnID_Proceso = id, ctDescripcion = docum, cnIndObligatorio = tbProcesoDocumentoModelList_cnIndObligatorio[index] });
                        index++;
                    });
                }

                //Insertar o actualizar
                tbProcesoCoberturaBLL lotbProcesoCoberturaBLL = new tbProcesoCoberturaBLL();
                tbProcesoCnfgAcadBLL lotbProcesoCnfgAcadBLL = new tbProcesoCnfgAcadBLL();
                tbProcesoCriterioAdicBLL lotbProcesoCriterioAdicBLL = new tbProcesoCriterioAdicBLL();
                tbProcesoDocumentoBLL lotbProcesoDocumentoBLL = new tbProcesoDocumentoBLL();

                lotbProcesoCoberturaBLL.Transaction = tran;
                lotbProcesoCoberturaBLL.Transaction.Connection = con;

                lotbProcesoCnfgAcadBLL.Transaction = tran;
                lotbProcesoCnfgAcadBLL.Transaction.Connection = con;

                lotbProcesoCriterioAdicBLL.Transaction = tran;
                lotbProcesoCriterioAdicBLL.Transaction.Connection = con;

                lotbProcesoDocumentoBLL.Transaction = tran;
                lotbProcesoDocumentoBLL.Transaction.Connection = con;

                tbProcesoCoberturaList.ForEach(cobertura =>
                {
                    if (cobertura.cnID_Cobertura == 0)
                        lotbProcesoCoberturaBLL.Insert(cobertura);
                    else
                        lotbProcesoCoberturaBLL.Update(cobertura);
                });

                tbProcesoCnfgAcadModelList.ForEach(academica =>
                {
                    if (academica.cnID_CnfgAcademica == 0)
                        lotbProcesoCnfgAcadBLL.Insert(academica);
                    else
                        lotbProcesoCnfgAcadBLL.Update(academica);
                });

                tbProcesoCriterioAdicModelList.ForEach(adicion =>
                {
                    if (adicion.cnID_CriterioAdicional == 0)
                        lotbProcesoCriterioAdicBLL.Insert(adicion);
                    else
                        lotbProcesoCriterioAdicBLL.Update(adicion);
                });

                tbProcesoDocumentoModelList.ForEach(docum =>
                {
                    if (docum.cnID_Documento == 0)
                        lotbProcesoDocumentoBLL.Insert(docum);
                    else
                        lotbProcesoDocumentoBLL.Update(docum);
                });

                tbProcesoCnfgLaboBLL lotbProcesoCnfgLaboBLL = new tbProcesoCnfgLaboBLL();
                tbProcesoCnfgLaboModel lotbProcesoCnfgLaboModel = new tbProcesoCnfgLaboModel();

                lotbProcesoCnfgLaboBLL.Transaction = tran;
                lotbProcesoCnfgLaboBLL.Transaction.Connection = con;

                lotbProcesoCnfgLaboModel.cnID_Proceso = id;
                lotbProcesoCnfgLaboModel.ctDescripcion = model.ctDescripcion;
                lotbProcesoCnfgLaboModel.cnID_TipoEntidad = cnTipoEntidad;

                //if (cnID_CnfgLaboral == 0)
                //{
                //    lotbProcesoCnfgLaboBLL.Insert(lotbProcesoCnfgLaboModel);
                //}
                //else
                //{
                //    lotbProcesoCnfgLaboModel.cnID_CnfgLaboral = cnID_CnfgLaboral;
                //    lotbProcesoCnfgLaboBLL.Update(lotbProcesoCnfgLaboModel);
                //}

                List<tbProcesoCnfgLaboModel> tbProcesoCnfgLaboModelList = lotbProcesoCnfgLaboBLL.GetBycnID_Proceso(id);
                tbProcesoCnfgLaboModelList.ForEach(conf =>
                {
                    conf.cnID_TipoEntidad = cnTipoEntidad;
                    lotbProcesoCnfgLaboBLL.Update(conf);
                });

                tran.Commit();

                if (archivos != null)
                {
                    if (newArchivoTerminos && archivos.Length > 0)
                        BaseUtil.DeleteFile(BaseVars.CARPETA_PROCESOS, archivos[0]);

                    if (newArchivoProceso && archivos.Length > 1)
                        BaseUtil.DeleteFile(BaseVars.CARPETA_PROCESOS, archivos[1]);
                }
            }
            catch
            {
                tran.Rollback();
            }

            if (id > 0)
            {
                if (!isNewProceso)
                    return RedirectToAction("Index", new { a = "saved", i = id });
                else
                    return RedirectToAction("Detalle", new { a = "saved", i = id });
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult DownloadFile(string file)
        {
            if (!BaseGlobal.IsLogged)
                return this.RedirectToLogin();

            //return new FilePathResult(BaseVars.CARPETA_PROCESOS + "/" + file, "content-disposition");
            string _file = BaseVars.CARPETA_PROCESOS + "/" + file;
            if (System.IO.File.Exists(_file))
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(_file);
                string fileName = file;

                MemoryStream stream = new MemoryStream(fileBytes);

                return new FileStreamResult(stream, "application/pdf");
            }
            return Json(new { result = "Archivo no encontrado" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ConsultaPostulantes()
        {
            if (!BaseGlobal.IsLogged)
                return this.RedirectToLogin();

            List<tbProcesoModel> lotbProcesoModelList = new List<tbProcesoModel>();
            tbProcesoBLL lotbProcesoBLL = new tbProcesoBLL();
            List<SelectListItem> SelectListadoProcesos = new List<SelectListItem>();

            var listadoProcesos = lotbProcesoBLL.GetAll();
            listadoProcesos.ForEach(proceso =>
            {
                SelectListadoProcesos.Add(new SelectListItem() { Text = proceso.ctTitulo, Value = proceso.cnID_Proceso.ToString() });
            });

            ViewBag.TipoResultadoList = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.TipoResultado, true, todosValue: "0", todosTexto: "Todos");

            ViewBag.ListadoProcesos = SelectListadoProcesos;

            return View();
        }

        [HttpGet]
        public ActionResult ListarPostulantes(int cID_Proceso, int cID_Resultado, string ctNumeroDNI, string ctEmail, Int64 cnCobertura, Int64 cnZona)
        {
            List<tbPostulanteModel> postulantesListado = new List<tbPostulanteModel>();
            tbPostulanteBLL lotbPostulanteBLL = new tbPostulanteBLL();

            postulantesListado = lotbPostulanteBLL.GetConsultaPostulantes(cID_Proceso, cID_Resultado, ctNumeroDNI, ctEmail, cnCobertura, cnZona);

            return PartialView("_PostulantesListado", postulantesListado);
        }

        //[HttpGet]
        //public ActionResult _ConsultaPostulantes(int cID_Proceso, int cID_Resultado, string ctNumeroDNI, string ctEmail)
        //{
        //    List<tbPostulanteModel> postulantesListado = new List<tbPostulanteModel>();
        //    tbPostulanteBLL lotbPostulanteBLL = new tbPostulanteBLL();
        //    postulantesListado = lotbPostulanteBLL.GetConsultaPostulantes(cID_Proceso, cID_Resultado, ctNumeroDNI, ctEmail);

        //    return Json(postulantesListado, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult ConsultaCandidatos()
        {
            if (!BaseGlobal.IsLogged)
                return this.RedirectToLogin();

            return View();
        }

        [HttpGet]
        public ActionResult ListarCandidatos(string ctNombreCompleto, string ctNumeroDNI, string ctEmail)
        {
            List<tbCandidatoModel> candidatosListado = new List<tbCandidatoModel>();
            tbCandidatoBLL lotbCandidatoBLL = new tbCandidatoBLL();

            candidatosListado = lotbCandidatoBLL.GetConsultaCandidatos(ctNombreCompleto, ctNumeroDNI, ctEmail);

            return PartialView("_CandidatosListado", candidatosListado);
        }

        [HttpPost]
        public ActionResult DeleteProceso(long id)
        {
            string message = string.Empty;
            List<tbPostulanteModel> postulantesListado = new List<tbPostulanteModel>();
            tbPostulanteBLL lotbPostulanteBLL = new tbPostulanteBLL();
            postulantesListado = lotbPostulanteBLL.GetConsultaPostulantes(id, 0, string.Empty, string.Empty, 0, 0);
            if (postulantesListado.Count > 0)
            {
                message = "Ya existen postulantes registrados para este proceso. No se puede continuar.";
            }
            else
            {
                IDbConnection con = dbSeleccionDbConnection.GetNewConnection();
                dbSeleccionDbTransaction tran = dbSeleccionDbTransaction.BeginTransaction();
                tran.Connection = con;
                try
                {
                    tbProcesoBLL lotbProcesoBLL = new tbProcesoBLL();
                    tbProcesoCnfgAcadBLL lotbProcesoCnfgAcadBLL = new tbProcesoCnfgAcadBLL();
                    tbProcesoCoberturaBLL lotbProcesoCoberturaBLL = new tbProcesoCoberturaBLL();
                    tbCoberturaZonaBLL lotbCoberturaZonaBLL = new tbCoberturaZonaBLL();
                    tbProcesoCriterioAdicBLL lotbProcesoCriterioAdicBLL = new tbProcesoCriterioAdicBLL();
                    tbProcesoDocumentoBLL lotbProcesoDocumentoBLL = new tbProcesoDocumentoBLL();
                    tbProcesoMatrizLaboBLL lotbProcesoMatrizLaboBLL = new tbProcesoMatrizLaboBLL();
                    tbProcesoCnfgLaboBLL lotbProcesoCnfgLaboBLL = new tbProcesoCnfgLaboBLL();

                    lotbProcesoBLL.Transaction = tran;
                    lotbProcesoBLL.Transaction.Connection = con;

                    lotbProcesoCnfgAcadBLL.Transaction = tran;
                    lotbProcesoCnfgAcadBLL.Transaction.Connection = con;

                    lotbProcesoCoberturaBLL.Transaction = tran;
                    lotbProcesoCoberturaBLL.Transaction.Connection = con;

                    lotbCoberturaZonaBLL.Transaction = tran;
                    lotbCoberturaZonaBLL.Transaction.Connection = con;

                    lotbProcesoCriterioAdicBLL.Transaction = tran;
                    lotbProcesoCriterioAdicBLL.Transaction.Connection = con;

                    lotbProcesoDocumentoBLL.Transaction = tran;
                    lotbProcesoDocumentoBLL.Transaction.Connection = con;

                    lotbProcesoMatrizLaboBLL.Transaction = tran;
                    lotbProcesoMatrizLaboBLL.Transaction.Connection = con;

                    lotbProcesoCnfgLaboBLL.Transaction = tran;
                    lotbProcesoCnfgLaboBLL.Transaction.Connection = con;

                    var academicos = lotbProcesoCnfgAcadBLL.GetBycnID_Proceso(id);
                    var coberturas = lotbProcesoCoberturaBLL.GetBycnID_Proceso(id);
                    var criteriosAdiconales = lotbProcesoCriterioAdicBLL.GetBycnID_Proceso(id);
                    var documentos = lotbProcesoDocumentoBLL.GetBycnID_Proceso(id);
                    var matriz = lotbProcesoMatrizLaboBLL.GetBycnID_Proceso(id);
                    var laboral = lotbProcesoCnfgLaboBLL.GetBycnID_Proceso(id);

                    if (academicos != null)
                    {
                        academicos.ForEach(acad =>
                           {
                               lotbProcesoCnfgAcadBLL.Delete(acad.cnID_CnfgAcademica);
                           });
                    }

                    if (coberturas != null)
                    {
                        coberturas.ForEach(cob =>
                        {
                            var zonas = lotbCoberturaZonaBLL.GetBycnID_Cobertura(cob.cnID_Cobertura);
                            zonas.ForEach(zona =>
                            {
                                lotbCoberturaZonaBLL.Delete(zona.cnID_CoberturaZona);
                            });
                            lotbProcesoCoberturaBLL.Delete(cob.cnID_Cobertura);
                        });
                    }

                    if (criteriosAdiconales != null)
                    {
                        criteriosAdiconales.ForEach(adicional =>
                        {
                            lotbProcesoCriterioAdicBLL.Delete(adicional.cnID_CriterioAdicional);
                        });
                    }

                    if (documentos != null)
                    {
                        documentos.ForEach(doc =>
                        {
                            lotbProcesoDocumentoBLL.Delete(doc.cnID_Documento);
                        });
                    }

                    if (matriz != null)
                    {
                        matriz.ForEach(m =>
                        {
                            lotbProcesoMatrizLaboBLL.Delete(m.cnID_MatrizLaboral);
                        });
                    }

                    if (laboral != null)
                    {
                        laboral.ForEach(lab =>
                        {
                            lotbProcesoCnfgLaboBLL.Delete(lab.cnID_CnfgLaboral);
                        });
                    }

                    lotbProcesoBLL.Delete(id);
                    tran.Commit();
                }
                catch
                {
                    message = "Ha ocurrido un error al eliminar el proceso. Intente nuevamente.";
                    tran.Rollback();
                }
            }
            return Json(new { result = "OK", message = message });
        }

        [HttpGet]
        public JsonResult ValidarProceso(long id)
        {
            bool result = true;
            string message = string.Empty;
            tbProcesoCnfgLaboBLL lotbProcesoCnfgLaboBLL = new tbProcesoCnfgLaboBLL();
            var laboral = lotbProcesoCnfgLaboBLL.GetBycnID_Proceso(id);
            if (laboral.Count < 2)
            {
                result = false;
                message = "Ingrese al menos un criterio laboral general y uno específico.";
            }

            return Json(new { resp = result, message = message }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddZonaCobertura(long id)
        {
            tbCoberturaZonaBLL lotbCoberturaZonaBLL = new tbCoberturaZonaBLL();
            List<tbCoberturaZonaModel> lotbCoberturaZonaModel = new List<tbCoberturaZonaModel>();

            lotbCoberturaZonaModel = lotbCoberturaZonaBLL.GetBycnID_Cobertura(id);

            if (lotbCoberturaZonaModel.Count == 0)
                lotbCoberturaZonaModel.Add(new tbCoberturaZonaModel() { cnID_Cobertura = id });

            ViewBag.CoberturaId = id;
            return PartialView(lotbCoberturaZonaModel);
        }

        [HttpPost]
        public ActionResult SaveZonas(long id, long[] tbCoberturaZonaModelList_cnID_CoberturaZona, string[] tbCoberturaZonaModelList_ctZona)
        {
            try
            {
                int i = 0;
                if (tbCoberturaZonaModelList_cnID_CoberturaZona != null && tbCoberturaZonaModelList_ctZona != null)
                {
                    List<tbCoberturaZonaModel> tbCoberturaZonaModelList = new List<tbCoberturaZonaModel>();
                    tbCoberturaZonaModelList_cnID_CoberturaZona.ToList().ForEach(x =>
                    {
                        tbCoberturaZonaModelList.Add(new tbCoberturaZonaModel() { cnID_Cobertura = id, cnID_CoberturaZona = x, ctZona = tbCoberturaZonaModelList_ctZona[i] });
                        i++;
                    });

                    tbCoberturaZonaBLL lotbCoberturaZonaBLL = new tbCoberturaZonaBLL();

                    tbCoberturaZonaModelList.ForEach(x =>
                    {
                        if (x.cnID_CoberturaZona == 0)
                            lotbCoberturaZonaBLL.Insert(x);
                        else
                            lotbCoberturaZonaBLL.Update(x);
                    });
                }
            }
            catch
            {
                return Json(new { result = "OK" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { result = "OK" }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteZona(int id)
        {
            try
            {
                tbCoberturaZonaBLL lotbCoberturaZonaBLL = new tbCoberturaZonaBLL();
                lotbCoberturaZonaBLL.Delete(id);

                return Json("OK");
            }
            catch
            {
                return Json("KO");
            }
        }

        [HttpPost]
        public ActionResult SaveCobertura(long idProceso, string coberturaNombre)
        {
            try
            {
                tbProcesoCoberturaBLL lotbProcesoCoberturaBLL = new tbProcesoCoberturaBLL();
                tbProcesoCoberturaModel lotbProcesoCoberturaModel = new tbProcesoCoberturaModel() { cnID_Proceso = idProceso, ctNombre = coberturaNombre };
                var id = lotbProcesoCoberturaBLL.Insert(lotbProcesoCoberturaModel);

                return Json(new { result = "OK", id = id }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { result = "KO", id = "0" }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EvaluacionManual(long id)
        {
            if (!BaseGlobal.IsLogged)
                return this.RedirectToLogin();

            tbPostulanteBLL lotbPostulanteBLL = new tbPostulanteBLL();
            tbPostulanteModel lotbPostulanteModel = new tbPostulanteModel();
            lotbPostulanteModel = lotbPostulanteBLL.GetBycnID_Postulante(id);

            tbProcesoBLL lotbProcesoBLL = new tbProcesoBLL();
            lotbPostulanteModel.tbProcesoModel = lotbProcesoBLL.GetBycnID_Proceso(lotbPostulanteModel.cnID_Proceso);

            tbCandidatoBLL lotbCandidatoBLL = new tbCandidatoBLL();
            lotbPostulanteModel.tbCandidatoModel = lotbCandidatoBLL.GetBycnID_Candidato(lotbPostulanteModel.cnID_Candidato);

            tbPostulanteCobeBLL lotbPostulanteCobeBLL = new tbPostulanteCobeBLL();
            var tbPostulanteCobeModel = lotbPostulanteCobeBLL.GetBycnID_Postulante(id).First();

            tbCoberturaZonaBLL lotbCoberturaZonaBLL = new tbCoberturaZonaBLL();
            var tbCoberturaZonaModel = lotbCoberturaZonaBLL.GetBycnID_CoberturaZona(tbPostulanteCobeModel.cnID_CoberturaZona);

            tbProcesoCoberturaBLL lotbProcesoCoberturaBLL = new tbProcesoCoberturaBLL();
            var tbProcesoCoberturaModel = lotbProcesoCoberturaBLL.GetBycnID_Cobertura((int)tbPostulanteCobeModel.cnID_Cobertura);

            ViewBag.ProcesoCobertura = string.Format("{0} / {1}", tbProcesoCoberturaModel.ctNombre, tbCoberturaZonaModel.ctZona);


            tbProcesoDocumentoBLL lotbProcesoDocumentoBLL = new tbProcesoDocumentoBLL();
            tbPostulanteDocuBLL lotbPostulanteDocuBLL = new tbPostulanteDocuBLL();
            var tbPostulanteDocuList = lotbPostulanteDocuBLL.GetBycnID_Postulante(id);
            lotbPostulanteModel.tbPostulanteDocuList = tbPostulanteDocuList;
            lotbPostulanteModel.tbPostulanteDocuList.ForEach(doc =>
            {
                var documento = lotbProcesoDocumentoBLL.GetBycnID_Documento(doc.cnID_Documento);
                doc.ctDescripcion = documento.ctDescripcion;
            });

            tbProcesoCnfgAcadBLL lotbProcesoCnfgAcadBLL = new tbProcesoCnfgAcadBLL();
            var lotbProcesoCnfgAcadModelList = lotbProcesoCnfgAcadBLL.GetBycnID_Proceso(lotbPostulanteModel.cnID_Proceso);

            tbAcademicoBLL lotbAcademicoBLL = new tbAcademicoBLL();

            lotbPostulanteModel.tbAcademicoModelList = lotbAcademicoBLL.GetBycnID_Candidato(lotbPostulanteModel.cnID_Candidato);
            lotbPostulanteModel.tbAcademicoModelList.ForEach(x =>
            {
                x.Cumple = lotbProcesoCnfgAcadModelList.Find(y => x.cnID_Grado == y.cnID_Grado && x.cnID_Situacion == y.cnID_Situacion) != null;
            });


            tbExperienciaBLL lotbExperienciaBLL = new tbExperienciaBLL();
            lotbPostulanteModel.tbExperienciaModelList = lotbExperienciaBLL.GetBycnID_Candidato(lotbPostulanteModel.cnID_Candidato);

            tbPostulanteLaboBLL lotbPostulanteLaboBLL = new tbPostulanteLaboBLL();
            var tbPostulanteLaboModelList = lotbPostulanteLaboBLL.GetBycnID_Postulante(id);

            lotbPostulanteModel.tbExperienciaModelList.ForEach(x =>
            {
                x.Cumple = tbPostulanteLaboModelList.Find(y => x.cnID_Experiencia == y.cnID_Experiencia) != null;
            });


            tbPostulanteEvalManualBLL lotbPostulanteEvalManualBLL = new tbPostulanteEvalManualBLL();
            List<tbPostulanteEvalManualModel> lotbPostulanteEvalManualModelList = new List<tbPostulanteEvalManualModel>();
            lotbPostulanteEvalManualModelList = lotbPostulanteEvalManualBLL.GetBycnPostulanteEvalManual_GetBycnID_Postulante(id);


            var loEvalAcad = lotbPostulanteEvalManualModelList.Find(x => x.cnID_TipoEval == 1);
            var loEvalLab = lotbPostulanteEvalManualModelList.Find(x => x.cnID_TipoEval == 2);
            var loEvalDoc = lotbPostulanteEvalManualModelList.Find(x => x.cnID_TipoEval == 3);

            ViewBag.tbPostulanteEvalManualModelAcad = new tbPostulanteEvalManualModel();
            ViewBag.tbPostulanteEvalManualModelLab = new tbPostulanteEvalManualModel();
            ViewBag.tbPostulanteEvalManualModelDoc = new tbPostulanteEvalManualModel();

            if (loEvalAcad != null)
                ViewBag.tbPostulanteEvalManualModelAcad = loEvalAcad;

            if (loEvalLab != null)
                ViewBag.tbPostulanteEvalManualModelLab = loEvalLab;

            if (loEvalDoc != null)
                ViewBag.tbPostulanteEvalManualModelDoc = loEvalDoc;

            return View(lotbPostulanteModel);
        }

        [HttpPost]
        public JsonResult SaveEvaluacionManual(bool result, long id, int tipo, long idPostulante, string ctObservaciones)
        {

            tbPostulanteEvalManualBLL lotbPostulanteEvalManualBLL = new tbPostulanteEvalManualBLL();

            tbPostulanteEvalManualModel lotbPostulanteEvalManualModel = new tbPostulanteEvalManualModel()
            {
                cnPostulanteEvalManual = id,
                cnID_Postulante = idPostulante,
                ctObservacion = ctObservaciones,
                ctUsuarioEval = BaseGlobal.UsuarioActual.ctLogin,
                cnEvaluacion = result,
                cnID_TipoEval = tipo
            };

            if (id > 0)
            {
                lotbPostulanteEvalManualBLL.Update(lotbPostulanteEvalManualModel);
            }
            else
            {
                id = lotbPostulanteEvalManualBLL.Insert(lotbPostulanteEvalManualModel);
            }


            return Json(new { result = "OK", id = id }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveCorreo(long idCandidato, string nuevoEmail)
        {
            try
            {
                tbCandidatoBLL lotbCandidatoBLL = new tbCandidatoBLL();
                lotbCandidatoBLL.UpdateEmail(idCandidato, nuevoEmail);
                return Json(new { result = "OK" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { result = "KO" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ResetPassw(string id)
        {
            tbCandidatoBLL lotbCandidatoBLL = new tbCandidatoBLL();

            if (id != null)
            {
                string claveAcceso = BaseUtil.EncryptPassword(BaseUtil.GetPasswordDefault());
                lotbCandidatoBLL.UpdateClaveAcceso(Convert.ToInt64(id), claveAcceso);
            }

            return Json(new { result = "OK" });
        }

        public ActionResult Reporteador()
        {
            ViewBag.ListadoTablas = BaseGlobal.GetTablasConsulta();

            return View();
        }


        public void ExportarExcelReporteador()
        {
            string accion = Request.Params.Get("accion");
            string txtOperador1 = Request.Params.Get("txtOperador1[]");
            string txtColumna = Request.Params.Get("txtColumna[]");
            string hdnColumna = Request.Params.Get("hdnColumna[]");
            string txtOperador2 = Request.Params.Get("txtOperador2[]");
            string txtValor1 = Request.Params.Get("txtValor1[]");
            string txtValor2 = Request.Params.Get("txtValor2[]");

            string columnasTexto = Request.Params.Get("columnasTexto");
            string columnasValor = Request.Params.Get("columnasValor");

            tbMaestroListasBLL lotbMaestroListasBLL = new tbMaestroListasBLL();
            StringBuilder sb = new StringBuilder();
            int i = 0;
            if (txtOperador1 != null)
            {
                string[] _txtOperador1 = txtOperador1.Split(',');
                string[] _txtColumna = txtColumna.Split(',');
                string[] _hdnColumna = hdnColumna.Split(',');
                string[] _txtOperador2 = txtOperador2.Split(',');
                string[] _txtValor1 = txtValor1.Split(',');
                string[] _txtValor2 = txtValor2.Split(',');




                _hdnColumna.ToList().ForEach(x =>
                {
                    string line = string.Empty;
                    var obj = lotbMaestroListasBLL.GetBycnID_Lista(Convert.ToInt32(x));
                    BaseGlobal.GruposListas grupo = (BaseGlobal.GruposListas)Enum.Parse(typeof(BaseGlobal.GruposListas), obj.cnID_GrupoLista.ToString());

                    var tablaPrefijo = grupo.GetDesc();

                    //AND CAMPO BETWEEN 1 AND 2
                    line = string.Format("{0} {1} {2} '{3}' {4} {5}",
                        _txtOperador1[i],
                        tablaPrefijo + _txtColumna[i],
                        _txtOperador2[i],
                        _txtValor1[i],
                        _txtOperador2[i] == "BETWEEN" ? "AND" : string.Empty,
                        _txtValor2[i]
                        );

                    i++;
                    sb.Append(line);
                });
            }

            string columnas = string.Empty;
            i = 0;
            string[] columnasTexto2 = columnasTexto.Split(',');
            columnasValor.Split(',').ToList().ForEach(col =>
            {
                if (col.Trim().Length > 0)
                {
                    var obj = lotbMaestroListasBLL.GetBycnID_Lista(Convert.ToInt32(col));
                    BaseGlobal.GruposListas grupo = (BaseGlobal.GruposListas)Enum.Parse(typeof(BaseGlobal.GruposListas), obj.cnID_GrupoLista.ToString());
                    columnas += grupo.GetDesc() + columnasTexto2[i] + ",";
                }
                i++;
            });

            string where = sb.ToString();
            tbCandidatoBLL lotbCandidatoBLL = new tbCandidatoBLL();

            DataTable dt = new DataTable();
            if (where.ToUpper().Contains("DELETE") || where.ToUpper().Contains("UPDATE") || where.ToUpper().Contains("DROP") || where.ToUpper().Contains("ALTER") || where.ToUpper().Contains("CREATE"))
            {
                dt = new DataTable();
            }
            else
            {
                dt = lotbCandidatoBLL.GetConsultaQuery(where, columnas);
            }



            GridView gv = new GridView();
            gv.DataSource = dt;
            gv.DataBind();

            if (accion == "excel")
            {
                if (dt != null)
                {
                    //if (dt.Rows.Count > 0)
                    {
                        //Response.ClearContent();
                        //Response.Buffer = true;
                        //Response.AddHeader("content-disposition", "attachment; filename=Consulta.xls");
                        //Response.ContentType = "application/ms-excel";
                        //Response.Charset = "";
                        Response.Clear();
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment;filename=" + HttpUtility.UrlEncode("Datos.xlsx", System.Text.Encoding.UTF8));

                        using (ExcelPackage pck = new ExcelPackage())
                        {
                            ExcelWorksheet ws = pck.Workbook.Worksheets.Add("Datos");
                            ws.Cells["A1"].LoadFromDataTable(dt, true);
                            var ms = new System.IO.MemoryStream();
                            pck.SaveAs(ms);
                            ms.WriteTo(Response.OutputStream);
                        }


                        return;
                    }
                }
            }

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();

        }
    }
}