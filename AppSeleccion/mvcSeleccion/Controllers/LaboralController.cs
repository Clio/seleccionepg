﻿using SalarDb.CodeGen.BLL;
using SalarDb.CodeGen.Common;
using SalarDb.CodeGen.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mvcSeleccion.Controllers
{
    public class LaboralController : WebControllerBase
    {
        // GET: Laboral
        public ActionResult Experiencia(int? id, string a = null, int? i = null)
        {
            if (!BaseGlobal.IsLogged)
                return this.RedirectToLogin();

            long cnID_Candidato = BaseGlobal.CandidatoActual.cnID_Candidato;

            ViewBag.TipoEntidades = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.TipoEntidad);
            ViewBag.TipoContratos = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.TipoContrato);
            ViewBag.Rubros = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.Rubro);
            ViewBag.Cargos = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.Cargo);
            ViewBag.Areas = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.Area);
            ViewBag.Paises = BaseGlobal.GetPaisListItem();

            tbExperienciaModel lotbExperienciaModel = new tbExperienciaModel();
            tbExperienciaBLL lotbExperienciaBLL = new tbExperienciaBLL();
            lotbExperienciaModel.cnID_Candidato = cnID_Candidato;
            lotbExperienciaModel.cfFechaInicio = DateTime.Now;
            lotbExperienciaModel.cfFechaFin = DateTime.Now;

            if (id.HasValue)
            {
                if (id.Value > 0)
                {
                    lotbExperienciaModel = lotbExperienciaBLL.GetBycnID_Experiencia(id.Value);
                }
            }

            lotbExperienciaModel.tbExperienciaModelList = new List<tbExperienciaModel>();
            lotbExperienciaModel.tbExperienciaModelList = lotbExperienciaBLL.GetBycnID_Candidato(cnID_Candidato);

            //--------------------
            ViewBag.action = a;
            ViewBag.id = i;

            tbAcademicoBLL lotbAcademicoBLL = new tbAcademicoBLL();
            int IdCandidato = Convert.ToInt32(BaseGlobal.CandidatoActual.cnID_Candidato);
            ViewBag.document = lotbAcademicoBLL.ListDoceumnts(IdCandidato, 2);
            //--------------------

            return View(lotbExperienciaModel);
        }

        [HttpPost]
        public ActionResult SaveExperiencia(tbExperienciaModel model, HttpPostedFileBase txtCertificado)
        {
            //if (txtCertificado != null && txtCertificado.ContentLength > 0 || model.cnID_Experiencia > 0)
            //{
            try
            {
                string oldFile = model.ctCertificado;
                string fileName = string.Empty;
                bool isnewFile = false;
                if (txtCertificado != null)
                {
                    if (BaseUtil.SaveFile(txtCertificado, BaseVars.CARPETA_CERTIFICADOS, out fileName))
                    {
                        model.ctCertificado = fileName;
                        isnewFile = true;
                    }
                }
                tbExperienciaBLL lotbExperienciaBLL = new tbExperienciaBLL();

                model.cfFechaCreacion = DateTime.Now;
                model.ctIPAcceso = BaseUtil.GetIPAddress();

                if (model.cfFechaFin.Month >= model.cfFechaInicio.Month)
                {
                    model.cnTiempoAnios = model.cfFechaFin.Year - model.cfFechaInicio.Year;
                    model.cnTiempoMeses = Math.Abs(model.cfFechaFin.Month - model.cfFechaInicio.Month);
                }
                else if (model.cfFechaFin.Month < model.cfFechaInicio.Month)
                {
                    model.cnTiempoAnios = model.cfFechaFin.Year - model.cfFechaInicio.Year - 1;
                    model.cnTiempoMeses = Math.Abs(model.cfFechaFin.Month + 12 - model.cfFechaInicio.Month);
                }
                else
                {
                    model.cnTiempoAnios = 0;
                    model.cnTiempoMeses = 0;
                }

                if (model.cnID_Experiencia == 0)
                    model.cnID_Experiencia = lotbExperienciaBLL.Insert(model);
                else
                {
                    if (isnewFile)
                        BaseUtil.DeleteFile(BaseVars.CARPETA_CERTIFICADOS, oldFile);

                    lotbExperienciaBLL.Update(model);
                }
            }
            catch
            {
            }
            //}

            return RedirectToAction("Experiencia", new { a = "saved", i = model.cnID_Experiencia });
        }

        public ActionResult SaveExperienciaBloque(tbExperienciaModel model, HttpPostedFileBase txtCertificado, int idbloque=2)
        {
            //if (txtCertificado != null && txtCertificado.ContentLength > 0 || model.cnID_Experiencia > 0)
            //{
            try
            {
                string oldFile = model.ctCertificado;
                string fileName = string.Empty;
                bool isnewFile = false;
                if (txtCertificado != null)
                {
                    if (BaseUtil.SaveFile(txtCertificado, BaseVars.CARPETA_CERTIFICADOS, out fileName))
                    {
                        model.ctCertificado = fileName;
                        isnewFile = true;
                    }
                }
                tbExperienciaBLL lotbExperienciaBLL = new tbExperienciaBLL();

                model.cfFechaCreacion = DateTime.Now;
                model.ctIPAcceso = BaseUtil.GetIPAddress();



                if (model.cnID_Experiencia == 0)
                    model.cnID_Experiencia = lotbExperienciaBLL.InsertBloque(model,idbloque);
                else
                {
                    if (isnewFile)
                        BaseUtil.DeleteFile(BaseVars.CARPETA_CERTIFICADOS, oldFile);

                }
            }
            catch
            {
            }
            //}

            return RedirectToAction("Experiencia", new { a = "saved", i = model.cnID_Experiencia });
        }

        [HttpPost]
        public ActionResult DeleteExperiencia(int id)
        {
            try
            {
                tbExperienciaBLL lotbExperienciaBLL = new tbExperienciaBLL();
                tbExperienciaModel lotbExperienciaModel = lotbExperienciaBLL.GetBycnID_Experiencia(id);
                lotbExperienciaBLL.Delete(id);
                BaseUtil.DeleteFile(BaseVars.CARPETA_CERTIFICADOS, lotbExperienciaModel.ctCertificado);
                return Json("OK");
            }
            catch
            {
                return Json("KO");
            }
        }

        [HttpPost]
        public ActionResult DeleteExperienciaTemporal(int id)
        {
            try
            {
                tbExperienciaBLL lotbExperienciaBLL = new tbExperienciaBLL();
                //tbExperienciaModel lotbExperienciaModel = lotbExperienciaBLL.GetBycnID_Experiencia(id);
                lotbExperienciaBLL.DeleteByCandidato(id);
                //BaseUtil.DeleteFile(BaseVars.CARPETA_CERTIFICADOS, lotbExperienciaModel.ctCertificado);
                return Json("OK");
            }
            catch
            {
                return Json("KO");
            }
        }

        // GET: Laboral
        public ActionResult Referencias(int? id, string a = null, int? i = null)
        {
            if (!BaseGlobal.IsLogged)
                return this.RedirectToLogin();

            long cnID_Candidato = BaseGlobal.CandidatoActual.cnID_Candidato;

            ViewBag.Cargos = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.Cargo);

            tbReferenciaModel lotbReferenciaModel = new tbReferenciaModel();
            tbReferenciaBLL lotbReferenciaBLL = new tbReferenciaBLL();
            lotbReferenciaModel.cnID_Candidato = cnID_Candidato;

            if (id.HasValue)
            {
                if (id.Value > 0)
                {
                    lotbReferenciaModel = lotbReferenciaBLL.GetBycnID_Referencia(id.Value);
                }
            }

            lotbReferenciaModel.tbReferenciaModelList = new List<tbReferenciaModel>();
            lotbReferenciaModel.tbReferenciaModelList = lotbReferenciaBLL.GetBycnID_Candidato(cnID_Candidato);

            //--------------------
            ViewBag.action = a;
            ViewBag.id = i;
            //--------------------

            return View(lotbReferenciaModel);
        }

        [HttpPost]
        public ActionResult SaveReferencia(tbReferenciaModel model)
        {
            try
            {
                tbReferenciaBLL lotbReferenciaBLL = new tbReferenciaBLL();
                model.cfFechaCreacion = DateTime.Now;
                model.ctIPAcceso = BaseUtil.GetIPAddress();
                if (model.cnID_Referencia == 0)
                    model.cnID_Referencia = lotbReferenciaBLL.Insert(model);
                else
                {
                    lotbReferenciaBLL.Update(model);
                }
            }
            catch
            {
            }

            return RedirectToAction("Referencias", new { a = "saved", i = model.cnID_Referencia });
        }

        [HttpPost]
        public ActionResult DeleteReferencia(int id)
        {
            try
            {
                tbReferenciaBLL lotbReferenciaBLL = new tbReferenciaBLL();
                tbReferenciaModel lotbReferenciaModel = lotbReferenciaBLL.GetBycnID_Referencia(id);
                lotbReferenciaBLL.Delete(id);
                return Json("OK");
            }
            catch
            {
                return Json("KO");
            }
        }

        // GET: Laboral
        public ActionResult Docencia(int? id, string a = null, int? i = null)
        {
            if (!BaseGlobal.IsLogged)
                return this.RedirectToLogin();

            long cnID_Candidato = BaseGlobal.CandidatoActual.cnID_Candidato;

            ViewBag.TipoEntidades = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.TipoEntidad);
            ViewBag.CategoriasDocente = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.CategoriasDocente);
            ViewBag.RegimenesDedicacion = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.RegimenDedicacion);

            ViewBag.GradosExperiencia = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.GradoExperienciaInstitucion);
            ViewBag.TiposExperiencia = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.TipoExperienciaInstitucion);
            ViewBag.UnidadesMedidaExperiencai = BaseGlobal.GetMaestroListItem(BaseGlobal.GruposListas.UnidadMedidaExperienciaInstitucion);

            ViewBag.DocenteInvListado = new List<SelectListItem>() { new SelectListItem() { Text = "Sí", Value = "true" }, new SelectListItem() { Text = "No", Value = "false" } };
            ViewBag.RegistradoDINA = new List<SelectListItem>() { new SelectListItem() { Text = "Sí", Value = "true" }, new SelectListItem() { Text = "No", Value = "false" } };

            tbExperienciaDocenciaModel lotbExperienciaDocenciaModel = new tbExperienciaDocenciaModel();
            tbExperienciaDocenciaBLL lotbExperienciaDocenciaBLL = new tbExperienciaDocenciaBLL();
            lotbExperienciaDocenciaModel.cnID_Candidato = cnID_Candidato;
            lotbExperienciaDocenciaModel.cfFechaInicio = DateTime.Now;


            if (id.HasValue)
            {
                if (id.Value > 0)
                {
                    lotbExperienciaDocenciaModel = lotbExperienciaDocenciaBLL.GetBycnID_ExperienciaDocencia(id.Value);

                    lotbExperienciaDocenciaModel.tbExperienciaDocenciaModelList = new List<tbExperienciaDocenciaModel>();
                    lotbExperienciaDocenciaModel.tbExperienciaDocenciaModelList = lotbExperienciaDocenciaBLL.GetBycnID_Candidato(cnID_Candidato);


                    //lotbExperienciaDocenciaModel.tbExperienciaDocenciaDetModelList = new List<tbExperienciaDocenciaDetModel>();

                    tbExperienciaDocenciaDetBLL lotbExperienciaDocenciaDetBLL = new tbExperienciaDocenciaDetBLL();
                    lotbExperienciaDocenciaModel.tbExperienciaDocenciaDetModelList = new List<tbExperienciaDocenciaDetModel>();
                    lotbExperienciaDocenciaModel.tbExperienciaDocenciaModelList.ForEach(x =>
                    {
                        if (x.cnID_ExperienciaDocencia == id.Value)
                        {
                            var list = lotbExperienciaDocenciaDetBLL.GetBycnID_ExperienciaDocencia(x.cnID_ExperienciaDocencia);
                            lotbExperienciaDocenciaModel.tbExperienciaDocenciaDetModelList.AddRange(list);
                        }
                    });
                }
            }

            else
            {
                lotbExperienciaDocenciaModel.tbExperienciaDocenciaModelList = new List<tbExperienciaDocenciaModel>();
                lotbExperienciaDocenciaModel.tbExperienciaDocenciaModelList = lotbExperienciaDocenciaBLL.GetBycnID_Candidato(cnID_Candidato);

            }





            //--------------------
            ViewBag.action = a;
            ViewBag.id = i;

            tbAcademicoBLL lotbAcademicoBLL = new tbAcademicoBLL();
            int IdCandidato = Convert.ToInt32(BaseGlobal.CandidatoActual.cnID_Candidato);
            ViewBag.document = lotbAcademicoBLL.ListDoceumnts(IdCandidato, 3);
            //--------------------

            return View(lotbExperienciaDocenciaModel);
        }

        [HttpPost]
        public ActionResult SaveExperienciaDocencia(tbExperienciaDocenciaModel model)
        {
            IDbConnection con = dbSeleccionDbConnection.GetNewConnection();
            dbSeleccionDbTransaction tran = dbSeleccionDbTransaction.BeginTransaction();
            try
            {
                string experienciaDocenciaDet = Request.Form["cnID_ExperienciaDocenciaDet[]"];
                string grado = Request.Form["cnID_Grado[]"];
                string tipoExperiencia = Request.Form["cnID_TipoExperiencia[]"];
                string descripcion = Request.Form["ctDescripcion[]"];
                string tiempoExperiencia = Request.Form["cnTiempoExperiencia[]"];
                string unidadMedida = Request.Form["cnID_UnidadMedida[]"];
                //string ctCerfificado = Request.Form["ctCerfificado[]"];

                var cerfificados = Request.Files;

                List<tbExperienciaDocenciaDetModel> list = new List<tbExperienciaDocenciaDetModel>();

                if (grado != null)
                {
                    string[] _grado = grado.Split(',');
                    string[] _tipoExperiencia = tipoExperiencia.Split(',');
                    string[] _descripcion = descripcion.Split(',');
                    string[] _tiempoExperiencia = tiempoExperiencia.Split(',');
                    string[] _unidadMedida = unidadMedida.Split(',');
                    //string[] _ctCerfificado = ctCerfificado.Split(',');

                    string[] _experienciaDocenciaDet = experienciaDocenciaDet.Split(',');

                    int i = 0;
                    _grado.ToList().ForEach(x =>
                    {
                        //string oldFile = _ctCerfificado[i];
                        string fileName = string.Empty;
                        bool isnewFile = false;

                        //if (cerfificados[i] != null)
                        //{
                        //    if (cerfificados[i].ContentLength > 0)
                        //    {
                        //        if (BaseUtil.SaveFile(cerfificados[i], BaseVars.CARPETA_CERTIFICADOS, out fileName))
                        //        {
                        //            isnewFile = true;
                        //        }
                        //    }
                        //}

                        list.Add(new tbExperienciaDocenciaDetModel()
                        {
                            cnID_ExperienciaDocenciaDet = Convert.ToInt64(_experienciaDocenciaDet[i]),
                            cnID_Grado = Convert.ToInt64(_grado[i]),
                            cnID_TipoExperiencia = Convert.ToInt64(_tipoExperiencia[i]),
                            ctDescripcion = _descripcion[i],
                            cnTiempoExperiencia = Convert.ToInt32(_tiempoExperiencia[i]),
                            cnID_UnidadMedida = Convert.ToInt64(_unidadMedida[i]),
                            //ctCerfificado = "0"
                        });
                        i++;
                    });
                }

                tbExperienciaDocenciaBLL lotbExperienciaDocenciaBLL = new tbExperienciaDocenciaBLL();
                tbExperienciaDocenciaDetBLL lotbExperienciaDocenciaDetBLL = new tbExperienciaDocenciaDetBLL();

                model.cfFechaCreacion = DateTime.Now;
                model.ctIPAcceso = BaseUtil.GetIPAddress();

                tran.Connection = con;

                lotbExperienciaDocenciaBLL.Transaction = tran;
                lotbExperienciaDocenciaBLL.Transaction.Connection = con;

                lotbExperienciaDocenciaDetBLL.Transaction = tran;
                lotbExperienciaDocenciaDetBLL.Transaction.Connection = con;

                if (model.cnID_ExperienciaDocencia == 0)
                {
                    model.cnID_ExperienciaDocencia = lotbExperienciaDocenciaBLL.Insert(model);
                }
                else
                {
                    lotbExperienciaDocenciaBLL.Update(model);
                }

                if (list != null)
                {
                    list.ForEach(x =>
                    {
                        x.cnID_ExperienciaDocencia = model.cnID_ExperienciaDocencia;
                        x.ctIPAcceso = BaseUtil.GetIPAddress();
                        if (x.cnID_ExperienciaDocenciaDet == 0)
                        {
                            lotbExperienciaDocenciaDetBLL.Insert(x);
                        }
                        else
                            lotbExperienciaDocenciaDetBLL.Update(x);
                    });
                }

                tran.Commit();
            }
            catch
            {
                tran.Rollback();
            }

            return RedirectToAction("Docencia", new { a = "saved", i = model.cnID_ExperienciaDocencia });
        }

        public ActionResult SaveExperienciaDocenciaBloque(tbExperienciaDocenciaModel model, HttpPostedFileBase txtCertificado, int idbloque=3)
        {
            try
            {
                
                string fileName = string.Empty;
                string archivo = string.Empty;
                bool isnewFile = false;
                if (txtCertificado != null)
                {
                    if (BaseUtil.SaveFile(txtCertificado, BaseVars.CARPETA_CERTIFICADOS, out fileName))
                    {
                        archivo = fileName;
                        isnewFile = true;
                    }
                }
                //tbExperienciaBLL lotbExperienciaBLL = new tbExperienciaBLL();
                tbExperienciaDocenciaBLL lotbExperienciaDocenciaBLL = new tbExperienciaDocenciaBLL();

                model.cfFechaCreacion = DateTime.Now;
                model.ctIPAcceso = BaseUtil.GetIPAddress();



                if (model.cnID_ExperienciaDocencia == 0)
                    model.cnID_ExperienciaDocencia = lotbExperienciaDocenciaBLL.InsertBloque(model,archivo,idbloque);
                else
                {
                    if (isnewFile) { }
                        //BaseUtil.DeleteFile(BaseVars.CARPETA_CERTIFICADOS, oldFile);

                }
            }
            catch
            {
            }
            //}

            return RedirectToAction("Docencia", new { a = "saved", i = model.cnID_ExperienciaDocencia });

            
        }

        [HttpPost]
        public ActionResult DeleteExperienciaDocencia(int id)
        {
            try
            {
                tbExperienciaDocenciaBLL lotbExperienciaDocenciaBLL = new tbExperienciaDocenciaBLL();
                tbExperienciaDocenciaModel lotbExperienciaDocenciaModel = lotbExperienciaDocenciaBLL.GetBycnID_ExperienciaDocencia(id);
                lotbExperienciaDocenciaBLL.Delete(id);
                //BaseUtil.DeleteFile(BaseVars.CARPETA_CERTIFICADOS, lotbExperienciaDocenciaModel.ctCertificado);
                return Json("OK");
            }
            catch
            {
                return Json("KO");
            }
        }

        [HttpPost]
        public ActionResult DeleteExperienciaDocenciaTemporal(int id)
        {
            try
            {
                tbExperienciaDocenciaBLL lotbExperienciaDocenciaBLL = new tbExperienciaDocenciaBLL();
                //tbExperienciaDocenciaModel lotbExperienciaDocenciaModel = lotbExperienciaDocenciaBLL.GetBycnID_ExperienciaDocencia(id);
                lotbExperienciaDocenciaBLL.DeleteByCandidato(id);
                //BaseUtil.DeleteFile(BaseVars.CARPETA_CERTIFICADOS, lotbExperienciaDocenciaModel.ctCertificado);
                return Json("OK");
            }
            catch
            {
                return Json("KO");
            }
        }

        [HttpPost]
        public ActionResult DeleteExperienciaDocenciaDet(int id)
        {
            try
            {
                tbExperienciaDocenciaDetBLL lotbExperienciaDocenciaDetBLL = new tbExperienciaDocenciaDetBLL();

                lotbExperienciaDocenciaDetBLL.Delete(id);
                //BaseUtil.DeleteFile(BaseVars.CARPETA_CERTIFICADOS, lotbExperienciaDocenciaModel.ctCertificado);
                return Json("OK");
            }
            catch
            {
                return Json("KO");
            }
        }
    }
}