﻿using SalarDb.CodeGen.BLL;
using SalarDb.CodeGen.Model;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace mvcSeleccion.Controllers
{
    public class MantenimientoController : Controller
    {
        // GET: Mantenimiento
        public ActionResult Index(string a = null, string type = null)
        {
            ViewBag.ListadoMantenimientos = BaseGlobal.GetMaestroList();
            ViewBag.action = a;
            return View();
        }

        public ActionResult AreaTematica(string a = null)
        {
            var listSubgrupos = BaseGlobal.GetMaestroGrupoList("TEMATICA");
            ViewBag.action = a;
            return View(listSubgrupos);
        }

        public ActionResult Edit(int? id, int padre)
        {
            int grupoLista = 0;
            ViewBag.ListadoMantenimientos = BaseGlobal.GetMaestroList();
            tbMaestroListasModel lista = new tbMaestroListasModel();

            tbMaestroListasBLL lotbMaestroListasBLL = new tbMaestroListasBLL();
            List<SelectListItem> listaSelect = new List<SelectListItem>();
            ViewBag.ListadoSubGrupos = new List<SelectListItem>();
            if (id.HasValue)
            {
                List<tbMaestroListasModel> list = new List<tbMaestroListasModel>();
                lista = lotbMaestroListasBLL.GetBycnID_Lista(id.Value);
                if (padre == (int)mvcSeleccion.BaseGlobal.GruposListas.AreaTematicaNivel)
                {
                    tbMaestroGruposBLL lotbMaestroGruposBLL = new tbMaestroGruposBLL();
                    var grupo = lotbMaestroGruposBLL.GetBycnID_GrupoLista(lista.cnID_GrupoLista);
                    if (grupo.cnGrupoPadre != 0)
                    {
                        list = lotbMaestroListasBLL.GetBycnID_GrupoLista(grupo.cnGrupoPadre);
                    }

                    grupoLista = lista.cnID_GrupoLista;
                    lista.cnID_GrupoLista = (int)BaseGlobal.GruposListas.AreaTematicaNivel;
                    var listSubgrupos = BaseGlobal.GetMaestroGrupoList("TEMATICA");
                    ViewBag.ListadoSubGrupos = listSubgrupos.Select(x => new SelectListItem() { Text = x.ctNombreGrupo, Value = x.cnID_GrupoLista.ToString() }).ToList();
                    lista.cnID_SubGrupoLista = grupoLista;
                }

                listaSelect.AddRange((from x in list select new SelectListItem() { Text = x.ctElemento, Value = x.cnID_Lista.ToString() }).ToList());
            }

            lista.cnOrden = lista.cnOrden ?? 1;

            ViewBag.ListadoPadres = listaSelect;

            return View("Edit", lista);
        }

        public ActionResult EditAreaTematica(int? id)
        {
            tbMaestroGruposBLL lotbMaestroGruposBLL = new tbMaestroGruposBLL();
            tbMaestroGruposModel lotbMaestroGruposModel = new tbMaestroGruposModel();
            if (id.HasValue)
            {
                lotbMaestroGruposModel = lotbMaestroGruposBLL.GetBycnID_GrupoLista(id.Value);
            }

            var listSubgrupos = BaseGlobal.GetMaestroGrupoList("TEMATICA");

            var list = listSubgrupos.Select(x => new SelectListItem() { Text = x.ctNombreGrupo, Value = x.cnID_GrupoLista.ToString() }).ToList();
            list.Insert(0, new SelectListItem() { Value = "0", Text = "Seleccionar" });
            ViewBag.ListadoAreasTematicas = list;

            return View(lotbMaestroGruposModel);
        }

        [HttpPost]
        public ActionResult Save(tbMaestroListasModel model)
        {
            try
            {
                if (model.cnID_GrupoLista == (int)BaseGlobal.GruposListas.AreaTematicaNivel)
                    model.cnID_GrupoLista = model.cnID_SubGrupoLista;

                tbMaestroListasBLL lotbMaestroListasBLL = new tbMaestroListasBLL();
                model.cnEstado = 1;
                if (model.cnID_Lista == 0)
                    model.cnID_Lista = lotbMaestroListasBLL.Insert(model);
                else
                {
                    lotbMaestroListasBLL.Update(model);
                }
            }
            catch
            {
            }

            return RedirectToAction("Index", new { a = "saved", i = model.cnID_Lista });
        }

        [HttpPost]
        public ActionResult SaveAreaTematica(tbMaestroGruposModel model)
        {
            try
            {
                tbMaestroGruposBLL lotbMaestroGruposBLL = new tbMaestroGruposBLL();
                model.cnEstado = 1;
                model.ctTipoGrupo = "TEMATICA";
                if (model.cnID_GrupoLista == 0)
                    model.cnID_GrupoLista = lotbMaestroGruposBLL.Insert(model);
                else
                {
                    lotbMaestroGruposBLL.Update(model);
                }
            }
            catch
            {
            }

            return RedirectToAction("AreaTematica", new { a = "saved", i = model.cnID_GrupoLista });
        }

        [HttpPost]
        public ActionResult DeleteList(int id)
        {
            try
            {
                tbMaestroListasBLL lotbMaestroListasBLL = new tbMaestroListasBLL();
                lotbMaestroListasBLL.Delete(id);

                return Json("OK");
            }
            catch
            {
                return Json("KO");
            }
        }

        [HttpPost]
        public ActionResult DeleteGrupo(int id)
        {
            try
            {
                tbMaestroGruposBLL lotbMaestroGruposBLL = new tbMaestroGruposBLL();
                lotbMaestroGruposBLL.Delete(id);

                return Json("OK");
            }
            catch
            {
                return Json("KO");
            }
        }
    }
}